/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.unfulfilled
import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.iterators.emptyImmutableIterator
import cc.persch.stardustx.collections.lists.UnmodifiableListView
import cc.persch.stardustx.collections.lists.__emptyUnmodifiableList

/**
 * Defines an empty [UnmodifiableIndexSetView] implementation.
 *
 * @since 5.0
 */
@ThreadSafe
public object EmptyUnmodifiableIndexSet: UnmodifiableIndexSetView<Nothing>, RandomAccess {
	
	override val size: Int
		get() = 0
	
	override fun isEmpty(): Boolean = true
	
	override fun iterator(): UnmodifiableIteratorView<Nothing> = emptyImmutableIterator()
	
	@Pure
	override fun equals(other: Any?): Boolean = other is IndexSet<*> && other.isEmpty()
	
	@Pure
	override fun getAt(index: Int): Nothing = unfulfilled("index")
	
	@Pure
	override fun indexOf(value: Nothing): Int = -1
	
	@Pure
	override fun indexOfReference(value: Nothing): Int = -1
	
	@Pure
	override fun contains(element: Nothing): Boolean = false
	
	@Pure
	override fun containsAll(elements: Collection<Nothing>): Boolean = elements.isEmpty()
	
	@Pure
	override fun asList(): UnmodifiableListView<Nothing> = __emptyUnmodifiableList()
}

