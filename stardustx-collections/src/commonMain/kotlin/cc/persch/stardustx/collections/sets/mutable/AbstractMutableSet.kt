/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper

/**
 * Defines the base class of a [MutableSet].
 *
 * @since 5.0
 */
public abstract class AbstractMutableSet<E>: AbstractSet<E>(), MutableSet<E> {
	
	abstract override val set: MutableSet<E>
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun add(element: E): Boolean {
		
		checkMutability()
		
		return set.add(element)
	}
	
	override fun addAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		return elements !== this && set.addAll(elements)
	}
	
	override fun clear() {
		
		checkMutability()
		
		set.clear()
	}
	
	override fun remove(element: E): Boolean {
		
		checkMutability()
		
		return set.remove(element)
	}
	
	override fun removeAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		if(elements === this) {
			
			val wasNotEmpty = isNotEmpty()
			
			clear()
			
			return wasNotEmpty
		}
		
		@Suppress("ConvertArgumentToSet")
		return set.removeAll(elements)
	}
	
	override fun retainAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		if(elements === this)
			return false
		
		@Suppress("ConvertArgumentToSet")
		return set.retainAll(elements)
	}
	
	override fun iterator(): MutableIterator<E> = MutableIteratorWrapper(set.iterator(), this::checkMutability)
}
