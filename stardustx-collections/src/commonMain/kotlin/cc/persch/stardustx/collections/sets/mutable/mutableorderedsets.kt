/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardustx.collections.DEFAULT_CAPACITY

/**
 * Creates an empty [MutableIndexSet].
 *
 * @since 4.0
 */
@Pure
public fun <V> mutableOrderedSetOf(): MutableIndexSet<V> = MutableHashIndexSet()

/**
 * Creates a [MutableIndexSet] containing the given [items].
 *
 * @since 4.0
 */
@Pure
public fun <V> mutableOrderedSetOf(vararg items: V): MutableIndexSet<V> =
	items.asList().toMutableOrderedSet()

/**
 * Creates a [MutableIndexSet].
 *
 * @since 4.0
 */
@Pure
public fun <V> Sequence<V>.toMutableOrderedSet(): MutableIndexSet<V> = asIterable().toMutableOrderedSet()

/**
 * Creates a [MutableIndexSet].
 *
 * @since 4.0
 */
@Pure
public fun <V> Iterable<V>.toMutableOrderedSet(): MutableIndexSet<V> =
	MutableHashIndexSet<V>(if(this is Collection<*>) size else DEFAULT_CAPACITY) exert { it.addAll(this) }
