/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches.mutable

import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardust.exert
import cc.persch.stardustx.collections.bunches.Bunch
import cc.persch.stardustx.collections.bunches.MutableBunch

/**
 * Tries to get the value that is assigned to the given [key]. If the key does not exist, the value of the given
 * [defaultValueSupplier] is put into this bunch and returned.
 *
 * @since 4.0
 * @return The value, found by the given [key]; otherwise, the crated value of [defaultValueSupplier].
 * @see Bunch.get
 * @see MutableBunch.put
 */
@ShouldUse
public inline fun <K, V> MutableBunch<K, V>.getOrPut(key: K, defaultValueSupplier: (K) -> V): V =
	this.get(key) ?: defaultValueSupplier(key).exert(::put)
