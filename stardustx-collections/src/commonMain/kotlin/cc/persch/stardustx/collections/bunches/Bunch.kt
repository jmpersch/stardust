/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure

/**
 * Defines an interface for a bunch that is a map, where the [key][K] is an element of the [value][V].
 * Due to this, a bunch is provided as a [set][Set]
 * of values.
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 3.0
 */
public interface Bunch<K, V>: Set<V> {
	
	/**
	 * The key extractor function.
	 */
	public val keyExtractor: (V) -> K
	
	/**
	 * Get a set of the keys.
	 *
	 * @since 5.0
	 */
	public val keys: Set<K>
	
	/**
	 * Tries to get the value that is assigned to the supplied key.
	 *
	 * @param key The key.
	 * @return The value, found by the key; otherwise, `null`.
	 */
	@Pure
	public operator fun get(key: K): V?
	
	/**
	 * Tries to get the value that is assigned to the key of the supplied value.
	 *
	 * @param value The value to get the key from.
	 * @return The value to get.
	 */
	@Pure
	public fun getByKeyOf(value: V): V? = get(keyExtractor(value))
	
	/**
	 * Checks whether this collection contains the specified key.
	 *
	 * @param key The key.
	 * @return `true`, if this collection contains the specified key; otherwise, `false`.
	 */
	@Pure
	public fun containsKey(key: K): Boolean
	
	/**
	 * Checks whether this collection contains the specified value.
	 *
	 * @param value The value.
	 * @return `true`, if this collection contains the specified value; otherwise, `false`.
	 */
	@Pure
	public fun containsValue(value: V): Boolean
	
	/**
	 * Checks whether this collection contains the key of the specified value.
	 *
	 * @param value The value.
	 * @return `true`, if this collection contains the key of the specified value; otherwise, `false`.
	 */
	@Pure
	public fun containsKeyOf(value: V): Boolean = containsKey(keyExtractor(value))
	
	/**
	 * Checks whether this collection contains all specified keys.
	 *
	 * @param keys The keys.
	 * @return `true`, if this collection contains all keys; otherwise, `false`.
	 */
	@Pure
	public fun containsAllKeys(keys: Iterable<K>): Boolean = keys.all(this::containsKey)
	
	/**
	 * Checks whether this collection contains all specified values.
	 *
	 * @param values The values.
	 * @return `true`, if this collection contains all values; otherwise, `false`.
	 */
	@Pure
	public fun containsAllValues(values: Iterable<V>): Boolean = values === this || values.all(this::containsValue)
	
	/**
	 * Checks whether this collection contains the keys of all specified values.
	 *
	 * @param values The values.
	 * @return `true`, if this collection contains the keys of all specified values; otherwise, `false`.
	 */
	@Pure
	public fun containsAllKeysOf(values: Iterable<V>): Boolean = values === this || values.all(this::containsKeyOf)
	
	/**
	 * Checks whether this collection contains the specified value.
	 *
	 * **Info:** Better use [containsValue].
	 *
	 * @param element The value.
	 * @return `true`, if this collection contains the specified value; otherwise, `false`.
	 * @see containsValue
	 */
	@Pure
	override fun contains(element: V): Boolean = containsValue(element)
	
	/**
	 * Checks whether this collection contains all specified values.
	 *
	 * **Info:** Better use [containsAllValues].
	 *
	 * @param values The values.
	 * @return `true`, if this collection contains all specified values; otherwise, `false`.
	 * @see containsAllValues
	 */
	@Pure
	public fun containsAll(values: Iterable<V>): Boolean = containsAllValues(values)
	
	@Pure
	override fun containsAll(elements: Collection<V>): Boolean = containsAllValues(elements)
	
	/**
	 * Returns a mutable copy.
	 */
	@Pure
	public fun mutableCopy(): MutableBunch<K, V> = toMutableBunch()
	
	/**
	 * Returns an unmodifiable view.
	 */
	@Pure
	public fun asUnmodifiableView(): UnmodifiableBunchView<K, V> = when(this) {
		
		is UnmodifiableBunchView<K, V> -> this
		
		else -> UnmodifiableBunchWrapper(this)
	}
}
