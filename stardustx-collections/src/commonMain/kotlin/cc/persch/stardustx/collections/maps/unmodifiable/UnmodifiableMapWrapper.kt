/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.getOrInitialize
import cc.persch.stardustx.collections.UnmodifiableCollectionView
import cc.persch.stardustx.collections.UnmodifiableCollectionWrapper
import cc.persch.stardustx.collections.sets.UnmodifiableSetView
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import kotlinx.serialization.Transient

/**
 * Defines a wrapper that provides an immutable view of a [Map].
 *
 * @since 4.2
 * 
 * @constructor Creates a new instance wrapping the given [map].
 */
public open class UnmodifiableMapWrapper<K, V>(
	protected open val map: Map<K, V>
) : UnmodifiableMapView<K, V> {
	
	@Transient
	private var _entries: UnmodifiableEntrySetWrapper<K, V>? = null
	
	@Transient
	private var _keys: UnmodifiableSetWrapper<K>? = null
	
	@Transient
	private var _values: UnmodifiableCollectionWrapper<V>? = null
	
	override val entries: UnmodifiableSetView<UnmodifiableMapView.UnmodifiableEntryView<K, V>>
		get() = this::_entries.getOrInitialize { UnmodifiableEntrySetWrapper(map.entries) }
	
	override val keys: UnmodifiableSetView<K>
		get() = this::_keys.getOrInitialize { UnmodifiableSetWrapper(map.keys) }
	
	override val values: UnmodifiableCollectionView<V>
		get() = this::_values.getOrInitialize { UnmodifiableCollectionWrapper(map.values) }
	
	override val size: Int
		get() = map.size
	
	override fun containsKey(key: K): Boolean = map.containsKey(key)
	
	override fun containsValue(value: V): Boolean = map.containsValue(value)
	
	override operator fun get(key: K): V? = map.get(key)
	
	override fun isEmpty(): Boolean = map.isEmpty()
	
	override fun hashCode(): Int = map.hashCode()
	
	override fun equals(other: Any?): Boolean = map == other
	
	override fun toString(): String = map.toString()
}
