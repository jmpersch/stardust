/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper
import cc.persch.stardustx.collections.maps.__putIfAbsent
import cc.persch.stardustx.collections.maps.__remove

/**
 * Defines base class for [mutable bunches][MutableBunch].
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 4.0
 *
 * @constructor Create a new instance.
 * @param keyExtractor The key extractor.
 */
public abstract class AbstractMutableBunch<K, V>: AbstractBunch<K, V>(), MutableBunch<K, V> {
	
	abstract override val map: MutableMap<K, V>
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun put(value: V): V? {
		
		checkMutability()
		
		return map.put(extractKey(value), value)
	}
	
	override fun putIfAbsent(value: V): V? {
		
		checkMutability()
		
		return map.__putIfAbsent(extractKey(value), value)
	}
	
	override fun removeKey(key: K): Boolean {
		
		checkMutability()
		
		return map.remove(key) != null
	}
	
	override fun removeValue(value: V): Boolean {
		
		checkMutability()
		
		return map.__remove(extractKey(value), value)
	}
	
	override fun removeByKeyOf(value: V): Boolean {
		
		checkMutability()
		
		return removeKey(extractKey(value))
	}
	
	override fun clear() {
		
		checkMutability()
		
		map.clear()
	}
	
	override fun iterator(): MutableIterator<V> = MutableIteratorWrapper(map.values.iterator(), this::checkMutability)
}
