/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.lists.mutable

import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper
import cc.persch.stardustx.collections.iterators.MutableListIteratorWrapper
import cc.persch.stardustx.collections.lists.AbstractList

/**
 * Defines the [AbstractMutableList] class.
 *
 * @since 5.0
 */
public abstract class AbstractMutableList<E>: AbstractList<E>(), MutableList<E> {
	
	abstract override val list: MutableList<E>
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun add(element: E): Boolean {
		
		checkMutability()
		
		return list.add(element)
	}
	
	override fun add(index: Int, element: E) {
		
		checkMutability()
		
		list.add(index, element)
	}
	
	override fun addAll(index: Int, elements: Collection<E>) : Boolean {
		
		checkMutability()
		
		return list.addAll(index, elements)
	}
	
	override fun addAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		return list.addAll(elements)
	}
	
	override fun clear() {
		
		checkMutability()
		
		list.clear()
	}
	
	override fun remove(element: E): Boolean {
		
		checkMutability()
		
		return list.remove(element)
	}
	
	override fun removeAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		return list.removeAll(elements)
	}
	
	override fun removeAt(index: Int): E {
		
		checkMutability()
		
		return list.removeAt(index)
	}
	
	override fun retainAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		return list.retainAll(elements)
	}
	
	override fun set(index: Int, element: E): E {
		
		checkMutability()
		
		return list.set(index, element)
	}
	
	override fun iterator(): MutableIterator<E> = MutableIteratorWrapper(list.iterator(), this::checkMutability)
	
	override fun listIterator(): MutableListIterator<E> = listIterator(0)
	
	override fun listIterator(index: Int): MutableListIterator<E> =
		MutableListIteratorWrapper(list.listIterator(index), this::checkMutability)
	
	override fun subList(fromIndex: Int, toIndex: Int): MutableList<E> =
		MutableSubList(list.subList(fromIndex, toIndex))
	
	/**
	 * Defines mutable sub-list.
	 *
	 * @constructor Creates a new instance using the given mutable [sub-list][list].
	 */
	protected open inner class MutableSubList<E>(
		override val list: MutableList<E>
	) : AbstractMutableList<E>() {
		
		override fun checkMutability(): Unit = this@AbstractMutableList.checkMutability()
		
		override fun subList(fromIndex: Int, toIndex: Int): MutableList<E> =
			MutableSubList(list.subList(fromIndex, toIndex))
	}
}
