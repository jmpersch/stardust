/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.IndexCollection

/**
 * Defines an interface for an ordered [Bunch].
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 3.0
 */
public interface IndexBunch<K, V>: Bunch<K, V>, IndexCollection<V> {
	
	/**
	 * Tries to find the index of the specified key.
	 *
	 * @param key The key.
	 * @return The found index of the specified key; otherwise, `null`.
	 */
	@Pure
	public fun indexOfKey(key: K): Int
	
	/**
	 * Tries to find the index of the specified value.
	 *
	 * @param value The value.
	 * @return The found index of the specified value; otherwise, `null`.
	 */
	@Pure
	public fun indexOfValue(value: V): Int
	
	/**
	 * Tries to find the index of the key of the specified value.
	 *
	 * @param value The value.
	 * @return The found index of the key of the specified value; otherwise, `null`.
	 * @since 5.0
	 */
	@Pure
	public fun indexByKeyOf(value: V): Int = indexOfKey(keyExtractor(value))
	
	/**
	 * Tries to find the index of the key of the specified value.
	 *
	 * @param value The value.
	 * @return The found index of the key of the specified value; otherwise, `-1`.
	 */
	@Pure
	public fun indexOfValueReference(value: V): Int
	
	/**
	 * Returns a [List] view of this collection. The returned view is backed by this collection, so all changes in
	 * this collection are reflected by the returned view.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun asList(): List<V>
	
	/**
	 * Returns a mutable copy.
	 */
	@Pure
	override fun mutableCopy(): MutableIndexBunch<K, V> = toMutableIndexBunch()
	
	/**
	 * Returns an unmodifiable view of this ordered bunch.
	 */
	@Pure
	override fun asUnmodifiableView(): UnmodifiableIndexBunchView<K, V> = when(this) {
		
		is UnmodifiableIndexBunchView<K, V> -> this
		
		else -> UnmodifiableIndexBunchWrapper(this)
	}
}
