/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure

/**
 * Defines the base class for a [Map].
 *
 */
public abstract class AbstractMap<K, out V>: Map<K, V> {
	
	protected abstract val map: Map<K, V>
	
	override val size: Int
		get() = map.size
	
	@Pure
	override fun containsKey(key: K): Boolean = map.containsKey(key)
	
	@Pure
	override fun containsValue(value: @UnsafeVariance V): Boolean = map.containsValue(value)
	
	@Pure
	override fun get(key: K): V? = map.get(key)
	
	@Pure
	override fun isEmpty(): Boolean = map.isEmpty()
	
	@Pure
	override fun equals(other: Any?): Boolean = map == other
	
	@Pure
	override fun hashCode(): Int = map.hashCode()
	
	@Pure
	override fun toString(): String = map.toString()
}
