/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.lists

import cc.persch.stardust.annotations.Pure

/**
 * Defines the base class for a [List].
 *
 * @since 5.0
 */
public abstract class AbstractList<out E>: List<E> {
	
	protected abstract val list: List<E>
	
	override val size: Int
		get() = list.size
	
	@Pure
	override fun contains(element: @UnsafeVariance E): Boolean = list.contains(element)
	
	@Pure
	override fun containsAll(elements: Collection<@UnsafeVariance E>): Boolean = list.containsAll(elements)
	
	@Pure
	override fun get(index: Int): E = list.get(index)
	
	@Pure
	override fun indexOf(element: @UnsafeVariance E): Int = list.indexOf(element)
	
	@Pure
	override fun isEmpty(): Boolean = list.isEmpty()
	
	@Pure
	override fun lastIndexOf(element: @UnsafeVariance E): Int = list.lastIndexOf(element)
	
	@Pure
	override fun listIterator(): ListIterator<E> = listIterator(0)
	
	@Pure
	override fun equals(other: Any?): Boolean = list == other
	
	@Pure
	override fun hashCode(): Int = list.hashCode()
	
	@Pure
	override fun toString(): String = list.toString()
}
