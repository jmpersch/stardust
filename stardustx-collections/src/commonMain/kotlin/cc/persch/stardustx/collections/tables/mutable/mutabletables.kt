/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.tables

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import kotlin.jvm.JvmName

/**
 * Gets the value of a mutable table. If the value does not exist, the value created by [defaultValue] is added.
 *
 * @since 5.0
 */
@ShouldUse
public inline fun <R, C, V> MutableTable<R, C, V>.getOrPut(
	rowKey: R,
	columnKey: C,
	defaultValue: () -> V
) : V = getOrPutEmpty(rowKey).getOrPut(columnKey, defaultValue)

/**
 * Puts the given [elements] into this table.
 * 
 * @since 5.0
 */
public fun <R, C, V> MutableTable<R, C, V>.putAll(elements: Iterable<Map.Entry<R, Iterable<Map.Entry<C, V>>>>): Unit =
	elements.forEach { (rowKey, columns) -> putAll(rowKey, columns) }

/**
 * Creates an empty [MutableTable].
 *
 * @since 4.6
 * @see toMutableTable
 */
@Pure
public fun <R, C, V> mutableTableOf(): MutableTable<R, C, V> = MutableLinkedHashTable()

/**
 * Creates a [MutableTable] of the given [rowKey], [columnKey], and [value].
 *
 * @since 4.6
 * @see toMutableTable
 */
@Pure
public fun <R, C, V> mutableTableOf(rowKey: R, columnKey: C, value: V): MutableTable<R, C, V> =
	buildMutableTable { put(rowKey, columnKey, value) }

/**
 * Creates a [MutableTable] of the given [rowKey] and [columns].
 *
 * @since 4.6
 * @see toMutableTable
 */
@Pure
public fun <R, C, V> mutableTableOf(rowKey: R, vararg columns: Map.Entry<C, V>): MutableTable<R, C, V> =
	mutableTableOf(rowKey, columns.asList())

/**
 * Creates a [MutableTable] of the given [rowKey] and [columns].
 *
 * @since 4.6
 * @see toMutableTable
 */
@Pure
public fun <R, C, V> mutableTableOf(rowKey: R, columns: Iterable<Map.Entry<C, V>>): MutableTable<R, C, V> =
	buildMutableTable { putAll(rowKey, columns) }

/**
 * Creates a [MutableTable] of the given [triples of row keys, column keys, and values][triples].
 *
 * @since 4.6
 * @see toMutableTable
 */
@Pure
public fun <R, C, V> mutableTableOf(vararg triples: Triple<R, C, V>): MutableTable<R, C, V> =
	triples.asList().toMutableTable()

/**
 * Creates a [MutableTable] of the given [triples of row keys, column keys, and values][this@mutableTableOfTriples].
 *
 * @since 5.0
 * @see mutableTableOf
 */
@JvmName("triplesToMutableTable")
@Pure
public fun <R, C, V> Sequence<Triple<R, C, V>>.toMutableTable(): MutableTable<R, C, V> = asIterable().toMutableTable()

/**
 * Creates a [MutableTable] of the given [triples of row keys, column keys, and values][this@mutableTableOfTriples].
 *
 * @since 5.0
 * @see mutableTableOf
 */
@JvmName("triplesToMutableTable")
@Pure
public fun <R, C, V> Iterable<Triple<R, C, V>>.toMutableTable(): MutableTable<R, C, V> =
	buildMutableTable { putAll(this@toMutableTable) }

/**
 * Returns a [MutableTable].
 *
 * @since 5.0
 * @see mutableTableOf
 */
@Pure
public fun <R, C, V> Sequence<Map.Entry<R, Iterable<Map.Entry<C, V>>>>.toMutableTable(): MutableTable<R, C, V> =
	asIterable().toMutableTable()

/**
 * Returns a [MutableTable].
 *
 * @since 5.0
 * @see mutableTableOf
 */
@Pure
public fun <R, C, V> Iterable<Map.Entry<R, Iterable<Map.Entry<C, V>>>>.toMutableTable(): MutableTable<R, C, V> =
	buildMutableTable<R, C, V> { putAll(this@toMutableTable) }

/**
 * Returns a mutable copy.
 *
 * @since 4.6
 * @see mutableTableOf
 */
@Pure
public fun <R, C, V> Table<R, C, V>.toMutableTable(): MutableTable<R, C, V> =
	buildMutableTable(size) { putAll(this@toMutableTable) }
