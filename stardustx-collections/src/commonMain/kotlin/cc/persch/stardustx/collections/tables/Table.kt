/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.tables

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.rope.RopeIndentable

/**
 * Defines a table.
 *
 * @param R The type of the row keys
 * @param C The type of the column keys
 * @param V The type of the values
 *
 * @since 4.6
 */
public interface Table<R, C, out V>: Iterable<Map.Entry<R, Map<C, V>>>, RopeIndentable {
	
	/**
	 * Gets the row count.
	 */
	public val size: Int
	
	/**
	 * Gets a set of all row keys.
	 */
	public val rowKeys: Set<R>
	
	/**
	 * Gets a value indicating whether this collection is empty.
	 * 
	 * @see isNotEmpty
	 * @see isNullOrEmpty
	 */
	@Pure
	public fun isEmpty(): Boolean = size == 0
	
	/**
	 * Gets a value indicating whether this collection is *not* empty.
	 * 
	 * @since 5.0
	 * @see isEmpty
	 * @see isNullOrEmpty
	 */
	@Pure
	public fun isNotEmpty(): Boolean = size != 0
	
	/**
	 * Computes the size over all rows and columns.
	 */
	@Pure
	public fun overAllSize(): Int = asSequence().sumOf { it.value.size }
	
	/**
	 * Gets the column map of the given [rowKey]; or `null`, if the key was not found.
	 *
	 * Note: The column map may be empty.
	 *
	 * @see getNonEmpty
	 */
	@Pure
	public fun get(rowKey: R): Map<C, V>?
	
	/**
	 * Gets the column map of the given [rowKey].
	 * If the key was not found or the column map is empty, `null` is returned.
	 *
	 * @see get
	 */
	@Pure
	public fun getNonEmpty(rowKey: R, returnNullOnEmpty: Boolean): Map<C, V>? {
		
		val columns = get(rowKey)
		
		return if(columns.isNullOrEmpty()) null else columns
	}
	
	/**
	 * Gets the value of the cell identified by the given [rowKey] and [columnKey];
	 * or `null`, if the cell was not found.
	 */
	@Pure
	public fun get(rowKey: R, columnKey: C): V?
	
	/**
	 * Gets a value indicating whether this table contains the given [rowKey]. The associated row might be empty.
	 */
	@Pure
	public fun containsKey(rowKey: R): Boolean
	
	/**
	 * Gets a value indicating whether this table contains the given [rowKey] and the associated row is *not* empty.
	 */
	@Pure
	public fun containsKeyNonEmpty(rowKey: R): Boolean
	
	/**
	 * Gets a value indicating whether this table contains the given combination of [rowKey] and [columnKey].
	 */
	@Pure
	public fun containsKey(rowKey: R, columnKey: C): Boolean = get(rowKey)?.containsKey(columnKey) ?: false
	
	/**
	 * Returns `true`, if this table contains the given [value].
	 *
	 * Note: This is an expensive operation, because it iterates through all values of the whole table.
	 */
	@Pure
	public fun containsValue(value: @UnsafeVariance V): Boolean = asValueSequence().any { it == value }
	
	/**
	 * Returns `true`, if the column of the given [rowKey] contains the given [value].
	 *
	 * Note: This is a relatively expensive operation, because it iterates through all values of a certain row.
	 */
	@Pure
	public fun containsValue(rowKey: R, value: @UnsafeVariance V): Boolean = get(rowKey)?.containsValue(value) == true
	
	/**
	 * Returns `true`, if the cell of the given [rowKey] and [columnKey] contains the given [value].
	 */
	@Pure
	public fun containsValue(rowKey: R, columnKey: C, value: @UnsafeVariance V): Boolean =
		get(rowKey)?.get(columnKey) == value
	
	@Pure
	override fun iterator(): Iterator<Map.Entry<R, Map<C, V>>>
	
	/**
	 * Returns a sequence of [entries][Map.Entry].
	 */
	@Pure
	public fun asSequence(): Sequence<Map.Entry<R, Map<C, V>>>
	
	/**
	 * Returns a sequence of row keys. This is a convenience method for `rowKeys.asSequence()`.
	 * 
	 * @see rowKeys
	 */
	@Pure
	public fun asRowKeySequence(): Sequence<R> = rowKeys.asSequence()
	
	/**
	 * Returns a sequence of column keys.
	 */
	@Pure
	public fun asColumnKeySequence(): Sequence<C> = asColumnSequence().map { it.key }
	
	/**
	 * Returns a sequence of column [entries][Map.Entry].
	 */
	@Pure
	public fun asColumnSequence(): Sequence<Map.Entry<C, V>>
	
	/**
	 * Returns a sequence of cells as [triples of row key, column key, and value][Triple].
	 */
	@Pure
	public fun asCellSequence(): Sequence<Triple<R, C, V>> = asSequence().flatMap { (rowKey, column) ->
		
		column.asSequence().map { (columnKey, value) -> Triple(rowKey, columnKey, value) }
	}
	
	/**
	 * Returns a sequence of cell keys as [pairs of row key and column key][Pair].
	 */
	@Pure
	public fun asCellKeySequence(): Sequence<Pair<R, C>> =
		asSequence().flatMap { (rowKey, column) -> column.keys.asSequence().map { rowKey to it } }
	
	/**
	 * Gets a sequence of all values.
	 */
	@Pure
	public fun asValueSequence(): Sequence<V>
	
	/**
	 * Creates a mutable copy.
	 */
	@Pure
	public fun mutableCopy(): MutableTable<R, C, @UnsafeVariance V> = toMutableTable()
	
	/**
	 * Returns an unmodifiable view.
	 */
	@Pure
	public fun asUnmodifiableView(): UnmodifiableTableView<R, C, V> = when(this) {
		
		is UnmodifiableTableView -> this
		
		else -> UnmodifiableTableWrapper(this)
	}
}
