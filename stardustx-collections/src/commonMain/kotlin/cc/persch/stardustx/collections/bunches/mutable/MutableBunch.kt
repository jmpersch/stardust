/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.pipe
import cc.persch.stardustx.collections.__accumulate

/**
 * Defines an interface for a mutable [Bunch].
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 3.0
 */
public interface MutableBunch<K, V>: Bunch<K, V>, MutableIterable<V> {
	
	/**
	 * Puts a value into this collection or replaces the existing value by the specified one,
	 * if the key is already present.
	 *
	 * @param value The value to put.
	 * @return The previous set value; or `null`, if there is no mapping the key of the specified value.
	 * @see get
	 */
	public fun put(value: V): V?
	
	/**
	 * Puts the given [value] into this map, if its key is not already associated with a value or the value is `null`.
	 *
	 * @param value The value to put.
	 * @return The previous set value; or `null`, if there is no previous value set.
	 */
	public fun putIfAbsent(value: V): V?
	
	/**
	 * Puts any of the specified values into this collection.
	 *
	 * @param values The values to put.
	 * @return `true`, if at least one item was putted; otherwise, `false`.
	 * @see put
	 * @see putAll
	 */
	public fun putAll(values: Iterable<V>): Boolean = values !== this && values.__accumulate { put(it) !== it }
	
	/**
	 * Puts any of the specified values that are absent, into this collection.
	 *
	 * @param values The values to put.
	 * @return `true`, if at least one item was putted; otherwise, `false`.
	 * @see putIfAbsent
	 * @see putAllAbsent
	 */
	public fun putAllAbsent(values: Iterable<V>): Boolean =
		values !== this && values.__accumulate { putIfAbsent(it) !== it }
	
	/**
	 * Removes the value that is associated to the specified key.
	 *
	 * @param key The key to remove.
	 * @return `true`, if the element has been removed; otherwise, `false`.
	 */
	public fun removeKey(key: K): Boolean
	
	/**
	 * Removes the specified value from this collection. Key and value must match; otherwise, the element is not removed.
	 *
	 * @param value The value to remove.
	 * @return `true`, if the value has been removed; otherwise, `false`.
	 */
	public fun removeValue(value: V): Boolean
	
	/**
	 * Removes the value that is mapped to the key of the specified value.
	 *
	 * @param value The value, containing the key to remove.
	 * @return `true`, if the value has been removed; otherwise, `false`.
	 */
	public fun removeByKeyOf(value: V): Boolean
	
	/**
	 * Removes all values that is associated to the specified keys.
	 *
	 * @param keys The keys to remove.
	 * @return `true`, if at least one item was removed; otherwise, `false`.
	 * @see removeAllKeys
	 */
	public fun removeAllKeys(keys: Iterable<K>): Boolean = keys.__accumulate(this::removeKey)
	
	/**
	 * Removes the specified values from this collection. Key and value of an element must match;
	 * otherwise, the element is not removed.
	 *
	 * @param values The values to remove.
	 * @return `true`, if at least one item was removed; otherwise, `false`.
	 */
	public fun removeAllValues(values: Iterable<V>): Boolean =
		if(values !== this) values.__accumulate(this::removeValue)
		else if(isEmpty()) false
		else { clear(); true }
	
	/**
	 * Removes the values that are mapped to the keys of the specified values.
	 *
	 * @param values The values, containing the keys to remove.
	 * @return `true`, if at least one item was removed; otherwise, `false`.
	 */
	public fun removeAllByKeyOf(values: Iterable<V>): Boolean =
		if(values !== this) removeAllKeys(values.asSequence().map(this.keyExtractor).asIterable())
		else if(isEmpty()) false
		else { clear(); true }
	
	/**
	 * Retains all keys that are contained in the specified collection.
	 *
	 * @param keys The keys to retain.
	 * @return `true`, if at leas one element has been removed; otherwise, `false`.
	 */
	public fun retainAllKeys(keys: Iterable<K>): Boolean =
		keys.toSet() pipe { set -> this.retainAll { set.contains(this.keyExtractor(it)) } }
	
	/**
	 * Retains all values that are contained in the specified collection.
	 *
	 * @param values The values to retain.
	 * @return `true`, if at leas one element has been removed; otherwise, `false`.
	 */
	public fun retainAllValues(values: Iterable<V>): Boolean =
		values !== this && values.toSet() pipe { this.retainAll(it::contains) }
	
	/**
	 * Retains all key of the values that are contained in the specified collection.
	 *
	 * @param values The values to retain.
	 * @return `true`, if at leas one element has been removed; otherwise, `false`.
	 */
	public fun retainAllByKeyOf(values: Iterable<V>): Boolean =
		values !== this && retainAllKeys(values.asSequence().map(this.keyExtractor).asIterable())
	
	/**
	 * Removes all elements of this collection.
	 */
	public fun clear()
}
