/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import cc.persch.stardust.getOrInitialize
import cc.persch.stardustx.collections.UnmodifiableCollectionWrapper
import cc.persch.stardustx.collections.sets.UnmodifiableSetView
import kotlinx.serialization.Transient

/**
 * Defines a wrapper that provides an immutable view of a [Bunch].
 *
 * @since 3.0
 */
public open class UnmodifiableBunchWrapper<K, V>(
	override val collection: Bunch<K, V>
) : UnmodifiableCollectionWrapper<V>(collection), UnmodifiableBunchView<K, V> {
	
	override val keyExtractor: (V) -> K
		get() = collection.keyExtractor
	
	@Transient
	private var _keys: UnmodifiableSetWrapper<K>? = null
	
	override val keys: UnmodifiableSetView<K>
		get() = this::_keys.getOrInitialize { UnmodifiableSetWrapper(collection.keys) }
	
	override fun contains(element: V): Boolean = collection.contains(element)
	
	override fun containsAll(elements: Collection<V>): Boolean = collection.containsAll(elements)
	
	override fun get(key: K): V? = collection.get(key)
	
	override fun containsKey(key: K): Boolean = collection.containsKey(key)
	
	override fun containsValue(value: V): Boolean = collection.containsValue(value)
}
