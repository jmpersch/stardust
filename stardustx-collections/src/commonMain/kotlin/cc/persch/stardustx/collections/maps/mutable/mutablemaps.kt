/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.DEFAULT_CAPACITY
import cc.persch.stardustx.collections.__accumulate
import cc.persch.stardustx.collections.lists.buildMutableList
import kotlin.jvm.JvmName

/**
 * Puts the given [value] into this map, if the given [key] is not already associated with a value
 * or the value is `null`.
 *
 * **Important:** Note that the operation is not guaranteed to be atomic if the map is being modified concurrently.
 *
 * @param key The key.
 * @param value The value.
 * @return The current value, if the key already exists; otherwise, `null`, if the given value was put into this map.
 *
 * @since 4.0
 */
internal expect fun <K, V> MutableMap<K, V>.__putIfAbsent(key: K, value: V): V?

/**
 * Removes the entry for the given [key] only if it is currently mapped to the given [value] (checked with equality).
 *
 * **Important:** Note that the operation is not guaranteed to be atomic if the map is being modified concurrently.
 *
 * @since 4.0
 */
internal expect fun <K, V> MutableMap<K, V>.__remove(key: K, value: V): Boolean

/**
 * Puts all entries from the given iterable to this map.
 *
 * @since 5.0
 * @see putAllAbsent
 */
public fun <K, V> MutableMap<K, V>.putAll(entries: Iterable<Map.Entry<K, V>>): Unit =
	entries.forEach { (key, entry) -> put(key, entry) }

/**
 * Puts all absent entries from the given iterable to this map.
 *
 * @return `true` if at least one value was added.
 *
 * @since 5.0
 * @see putAll
 */
public fun <K, V> MutableMap<K, V>.putAllAbsent(entries: Iterable<Map.Entry<K, V>>): Boolean =
	entries.__accumulate { (key, entry) -> __putIfAbsent(key, entry) == null }

/**
 * Creates a [MutableMap] of this sequence.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Sequence<Pair<K, V>>.toMutableMap(): MutableMap<K, V> = asIterable().toMutableMap()

/**
 * Creates a [MutableMap] of this [key-value pairs][Pair].
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Iterable<Pair<K, V>>.toMutableMap(): MutableMap<K, V> = buildMutableMap { putAll(this@toMutableMap) }

/**
 * Returns a [MutableMap].
 *
 * @since 5.0
 */
@JvmName("entriesToMutableMap")
@Pure
public fun <K, V> Sequence<Map.Entry<K, V>>.toMutableMap(): MutableMap<K, V> = asIterable().toMutableMap()

/**
 * Returns a [MutableMap]
 *
 * @since 5.0
 */
@JvmName("entriesToMutableMap")
@Pure
public fun <K, V> Iterable<Map.Entry<K, V>>.toMutableMap(): MutableMap<K, V> =
	buildMutableMap { putAll(this@toMutableMap) }

/**
 * Converts this map into a [MutableList] containing all entries as key-value pairs.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Map<out K, V>.toMutableList(): MutableList<Pair<K, V>> =
	buildMutableList(size + DEFAULT_CAPACITY) { this@toMutableList.forEach { add(it.toPair()) } }
