/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.lists

import cc.persch.stardustx.collections.iterators.UnmodifiableListIteratorView
import cc.persch.stardustx.collections.UnmodifiableCollectionView

/**
 * Defines an interface for an immutable view of a [List].
 *
 * @since 4.2
 */
public interface UnmodifiableListView<out E>: UnmodifiableCollectionView<E>, List<E> {
	
	override fun listIterator(): UnmodifiableListIteratorView<E>
	
	override fun listIterator(index: Int): UnmodifiableListIteratorView<E>
	
	override fun subList(fromIndex: Int, toIndex: Int): UnmodifiableListView<E>
}
