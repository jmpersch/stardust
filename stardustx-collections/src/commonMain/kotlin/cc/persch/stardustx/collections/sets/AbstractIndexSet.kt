/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.sets

import cc.persch.stardustx.collections.bunches.IndexBunch

/**
 * Defines the base class of an [IndexSet].
 *
 * @since 5.0
 */
public abstract class AbstractIndexSet<E>: IndexSet<E> {
	
	protected abstract val bunch: IndexBunch<E, E>
	
	override val size: Int
		get() = bunch.size
	
	override fun contains(element: E): Boolean = bunch.contains(element)
	
	override fun containsAll(elements: Collection<E>): Boolean = bunch.containsAll(elements)
	
	override fun isEmpty(): Boolean = bunch.isEmpty()
	
	override fun iterator(): Iterator<E> = bunch.iterator()
	
	override fun getAt(index: Int): E = bunch.getAt(index)
	
	override fun indexOf(value: E): Int = bunch.indexOfKey(value)
	
	override fun indexOfReference(value: E): Int = bunch.indexOfValueReference(value)
	
	override fun asList(): List<E> = bunch.asList()
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other)
			return true
		
		if(other !is IndexSet<*> || size != other.size)
			return false
		
		@Suppress("UNCHECKED_CAST")
		return bunch.containsAll(other as IndexSet<E>)
	}
	
	override fun hashCode(): Int = bunch.hashCode()
	
	override fun toString(): String = joinToString(", ", "[", "]")
}
