/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.collections.contentHashCode
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.ifNegative
import cc.persch.stardust.require
import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorWrapper
import cc.persch.stardustx.collections.iterators.UnmodifiableListIteratorView
import cc.persch.stardustx.collections.lists.UnmodifiableListView
import kotlinx.serialization.Transient

/**
 * Defines the base class for an [IndexBunch] implementation.
 *
 * @since 4.0
 */
public abstract class AbstractIndexBunch<K, V>: IndexBunch<K, V> {
	
	protected abstract val map: Map<K, Entry<K, V>>
	
	protected abstract val list: List<Entry<K, V>>
	
	/**
	 * Specifies the starting index for an index refresh.
	 *
	 * A value less than zero means that no refresh is required.
	 */
	protected abstract var refreshIndex: Int
	
	protected var modCount: Int = 0
	
	override val size: Int
		get() = list.size
	
	@Transient
	private var _keys: UnmodifiableSetWrapper<K>? = null
	
	override val keys: Set<K>
		get() = this::_keys.getOrInitialize { UnmodifiableSetWrapper(map.keys) }
	
	/**
	 * Extracts the key from the supplied value.
	 *
	 * @param value The value to extract the key from.
	 * @return The extracted key.
	 */
	@Pure
	protected fun extractKey(value: V): K = keyExtractor.invoke(value)
	
	protected fun optimizedIndexRefresh(entry: Entry<*, *>): Boolean {
		if(refreshIndex < 0 || entry.index < refreshIndex)
			return false
		
		for(i in refreshIndex..<list.size)
			list.get(i).index = i
		
		refreshIndex = -1
		
		return true
	}
	
	override fun isEmpty(): Boolean = map.isEmpty()
	
	@Pure
	override operator fun get(key: K): V? = map.get(key)?.value
	
	override fun getAt(index: Int): V = list.get(requireIndex(index)).value
	
	override fun indexOfKey(key: K): Int {
		val entry = map.get(key) ?: return -1
		
		optimizedIndexRefresh(entry)
		
		return entry.index
	}
	
	override fun indexOfValue(value: V): Int {
		
		val i = indexOfKey(extractKey(value)) ifNegative { return it }
		
		return if(getAt(i) == value) i else -1
	}
	
	override fun indexOfValueReference(value: V): Int {
		
		val i = indexOfKey(extractKey(value)) ifNegative { return it }
		
		return if(getAt(i) === value) i else -1
	}
	
	@Pure
	override fun containsKey(key: K): Boolean = map.containsKey(key)
	
	@Pure
	override fun containsValue(value: V): Boolean = this.get(extractKey(value)) == value
	
	override fun asList(): List<V> = ListView(list)
	
	protected fun requireIndex(index: Int): Int {
		
		require("index", index in list.indices) { "The index is out of bounds." }
		
		return index
	}
	
	override fun hashCode(): Int = list.contentHashCode(Entry<*, *>::value)
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other)
			return true
		
		if(other !is IndexBunch<*, *>)
			return false
		
		if(list.size != other.size)
			return false
		
		for(i in list.indices) {
			
			if(getAt(i) != other.getAt(i))
				return false
		}
		
		return true
	}
	
	override fun toString(): String = joinToString(", ", "[", "]")
	
	protected abstract class Entry<K, V>(
		public var index: Int,
		public val key: K,
		public open val value: V
	) {
		override fun equals(other: Any?): Boolean {
			
			if(this === other)
				return true
			
			if(other !is Entry<*, *>)
				return false
			
			return index == other.index && key == other.key && value == other.value
		}
		
		override fun hashCode(): Int {
			
			var result = index
			result = 31 * result + key.hashCode()
			result = 31 * result + value.hashCode()
			
			return result
		}
		
		override fun toString(): String = "$value"
	}
	
	private class ListView<K, V>(
		val subList: List<Entry<K, V>>
	) : UnmodifiableListView<V> {
		
		override val size
			get() = subList.size
		
		override fun isEmpty() = size == 0
		
		override fun contains(element: V) = subList.any { it.value == element }
		
		override fun containsAll(elements: Collection<V>) = elements.all(this::contains)
		
		override fun get(index: Int) = subList.get(index).value
		
		override fun indexOf(element: V) = subList.indexOfFirst { it.value == element }
		
		override fun iterator() = UnmodifiableIteratorWrapper(subList.asSequence().map(Entry<K, V>::value).iterator())
		
		override fun lastIndexOf(element: V) = subList.indexOfLast { it.value == element }
		
		override fun listIterator(): UnmodifiableListIteratorView<V> = UnmodifiableListViewIterator(subList.listIterator())
		
		override fun listIterator(index: Int): UnmodifiableListIteratorView<V> =
			UnmodifiableListViewIterator(subList.listIterator(index))
		
		override fun subList(fromIndex: Int, toIndex: Int): UnmodifiableListView<V> =
			ListView(subList.subList(fromIndex, toIndex))
		
		private class UnmodifiableListViewIterator<V>(
			private val iterator: ListIterator<Entry<*, V>>
		) : UnmodifiableListIteratorView<V> {
			
			override operator fun hasNext() = iterator.hasNext()
			
			override fun hasPrevious() = iterator.hasPrevious()
			
			override operator fun next() = iterator.next().value
			
			override fun nextIndex() = iterator.nextIndex()
			
			override fun previous() = iterator.previous().value
			
			override fun previousIndex() = iterator.previousIndex()
		}
	}
}
