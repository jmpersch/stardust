/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.lists

// InsertAt ////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Inserts an [element] into this list at the given [index].
 *
 * @since 4.0
 * @throws IndexOutOfBoundsException [index] is out of bounds.
 * @see MutableList.add
 */
public fun <E> MutableList<E>.insertAt(index: Int, element: E): Unit = add(index, element)

/**
 * Inserts all [elements] of the specified collection into this list at the given [index].
 *
 * @return `true`, if at least one element was added; otherwise, `false`.
 * @since 4.0
 * @throws IndexOutOfBoundsException [index] is out of bounds.
 * @see MutableList.addAll
 */
public fun <E> MutableList<E>.insertAllAt(index: Int, elements: Collection<E>): Boolean = addAll(index, elements)

/**
 * Inserts all [elements] of the specified collection into this list at the given [index].
 *
 * @return `true`, if at least one element was added; otherwise, `false`.
 * @since 4.0
 * @throws IndexOutOfBoundsException [index] is out of bounds.
 * @see MutableList.addAll
 */
public fun <E> MutableList<E>.insertAllAt(index: Int, elements: Iterable<E>): Boolean =
	addAll(index, if(elements is Collection<E>) elements else elements.toMutableList())
