/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps

import cc.persch.stardustx.collections.__accumulate

public interface MutableIndexMap<K, V>: IndexMap<K, V>, MutableMap<K, V> {
	
	/**
	 * Puts the given [value] into this map, if its key is not already associated with a value or the value is `null`.
	 *
	 * @param value The value to put.
	 * @return The previous set value; or `null`, if there is no previous value set.
	 */
	public fun putIfAbsent(key: K, value: V): V?
	
	/**
	 * Puts any of the specified values that are absent, into this collection.
	 *
	 * @param values The values to put.
	 * @return `true`, if at least one item was putted; otherwise, `false`.
	 * @see putIfAbsent
	 * @see putAllAbsent
	 */
	public fun putAllAbsent(values: Iterable<Pair<K, V>>): Boolean =
		values !== this && values.__accumulate { putIfAbsent(it.first, it.second) !== it }
	
	/**
	 * Puts any of the specified values that are absent, into this collection.
	 *
	 * @param values The values to put.
	 * @return `true`, if at least one item was putted; otherwise, `false`.
	 * @see putIfAbsent
	 * @see putAllAbsent
	 */
	public fun putAllAbsentEntries(values: Iterable<Map.Entry<K, V>>): Boolean =
		values !== this && values.__accumulate { putIfAbsent(it.key, it.value) !== it }
	
	/**
	 * Removes the specified value from this collection. Key and value must match; otherwise, the element is not removed.
	 *
	 * @param value The value to remove.
	 * @return `true`, if the value has been removed; otherwise, `false`.
	 */
	public fun remove(key: K, value: V): Boolean
	
	/**
	 * Removes all values that is associated to the specified keys.
	 *
	 * @param keys The keys to remove.
	 * @return `true`, if at least one item was removed; otherwise, `false`.
	 */
	public fun removeAll(keys: Iterable<K>): Boolean = keys.__accumulate { remove(it) != null }
	
	/**
	 * Removes the specified values from this collection. Key and value of an element must match;
	 * otherwise, the element is not removed.
	 *
	 * @param values The values to remove.
	 * @return `true`, if at least one item was removed; otherwise, `false`.
	 */
	public fun removeAllValues(values: Iterable<Pair<K, V>>): Boolean =
		if(values !== this) values.__accumulate { remove(it.first, it.second) }
		else if(isEmpty()) false
		else { clear(); true }
	
	/**
	 * Removes the specified values from this collection. Key and value of an element must match;
	 * otherwise, the element is not removed.
	 *
	 * @param values The values to remove.
	 * @return `true`, if at least one item was removed; otherwise, `false`.
	 */
	public fun removeAllEntries(values: Iterable<Map.Entry<K, V>>): Boolean =
		if(values !== this) values.__accumulate { remove(it.key, it.value) }
		else if(isEmpty()) false
		else { clear(); true }
	
	// TODO: Add `retain…` functions
//	/**
//	 * Retains all keys that are contained in the specified collection.
//	 *
//	 * @param keys The keys to retain.
//	 * @return `true`, if at leas one element has been removed; otherwise, `false`.
//	 */
//	public fun retainAllKeys(keys: Iterable<K>): Boolean
//
//	/**
//	 * Retains all values that are contained in the specified collection.
//	 *
//	 * @param values The values to retain.
//	 * @return `true`, if at leas one element has been removed; otherwise, `false`.
//	 */
//	public fun retainAllValues(values: Iterable<V>): Boolean
	
	/**
	 * Removes all elements of this collection.
	 */
	public override fun clear()
}
