/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a [MutableMap] using the given [builderAction].
 *
 * @since 5.0
 * @see mutableMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableMap(
	@BuilderInference builderAction: MutableMap<K, V>.() -> Unit
) : MutableMap<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return LinkedHashMap<K, V>().apply(builderAction)
}

/**
 * Creates a [MutableMap] with the given [initialCapacity] using the given [builderAction].
 *
 * @since 5.0
 * @see mutableMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableMap(
	initialCapacity: Int,
	@BuilderInference builderAction: MutableMap<K, V>.() -> Unit
) : MutableMap<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return LinkedHashMap<K, V>(initialCapacity).apply(builderAction)
}
