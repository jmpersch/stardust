/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.lists.UnmodifiableListView
import cc.persch.stardustx.collections.lists.asUnmodifiableView

/**
 * Defines a wrapper that provides an immutable view of an [IndexBunch].
 *
 * @since 3.0
 */
public open class UnmodifiableIndexBunchWrapper<K, V>(
	override val collection: IndexBunch<K, V>
) : UnmodifiableBunchWrapper<K, V>(collection), UnmodifiableIndexBunchView<K, V> {
	
	@Pure
	override fun getAt(index: Int): V = collection.getAt(index)
	
	@Pure
	override fun indexOfKey(key: K): Int = collection.indexOfKey(key)
	
	@Pure
	override fun indexOfValue(value: V): Int = collection.indexOfValue(value)
	
	@Pure
	override fun indexOfValueReference(value: V): Int = collection.indexOfValueReference(value)
	
	@Pure
	override fun asList(): UnmodifiableListView<V> = collection.asList().asUnmodifiableView()
}
