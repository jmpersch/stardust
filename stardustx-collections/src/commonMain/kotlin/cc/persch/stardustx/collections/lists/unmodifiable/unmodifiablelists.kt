/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.lists

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure

private val EMPTY_UNMODIFIABLE_LIST = UnmodifiableListWrapper<Any?>(emptyList())

@Suppress("UNCHECKED_CAST")
@Cached
@Pure
internal fun <E> __emptyUnmodifiableList() = EMPTY_UNMODIFIABLE_LIST as UnmodifiableListView<E>

/**
 * Returns an immutable view of this list.
 *
 * @since 4.0
 */
@Pure
public fun <E> List<E>.asUnmodifiableView(): UnmodifiableListView<E> = when(this) {
	
	is UnmodifiableListView<E> -> this
	
	else -> UnmodifiableListWrapper(this)
}

/**
 * Returns this; or an empty [UnmodifiableListView], if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentList
 */
@Cached
@Pure
public fun <E> UnmodifiableListView<E>?.orEmpty(): UnmodifiableListView<E> = this ?: __emptyUnmodifiableList()
