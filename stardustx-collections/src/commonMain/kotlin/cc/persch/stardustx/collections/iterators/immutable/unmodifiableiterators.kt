/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.iterators

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.iterators.particular.EmptyUnmodifiableIterator
import cc.persch.stardustx.collections.iterators.particular.EmptyUnmodifiableListIterator

/**
 * Gets an empty [UnmodifiableIteratorView].
 *
 * @since 5.0
 * @see emptyImmutableListIterator
 * @see emptyListIterator
 * @see emptyIterator
 * @see Iterator.asImmutable
 */
@Cached
@Pure
public fun emptyImmutableIterator(): UnmodifiableIteratorView<Nothing> = EmptyUnmodifiableIterator

/**
 * Gets an empty [UnmodifiableListIteratorView].
 *
 * @since 5.0
 * @see emptyImmutableIterator
 * @see emptyIterator
 * @see emptyListIterator
 * @see Iterator.asImmutable
 */
@Cached
@Pure
public fun emptyImmutableListIterator(): UnmodifiableListIteratorView<Nothing> = EmptyUnmodifiableListIterator

/**
 * Returns an immutable view of this iterator.
 *
 * @since 5.0
 */
@Pure
public fun <T> Iterator<T>.asImmutable(): UnmodifiableIteratorView<T> = when(this) {
	
	is UnmodifiableIteratorView -> this
	
	else -> UnmodifiableIteratorWrapper(this)
}

/**
 * Returns an immutable view of this list iterator.
 *
 * @since 5.0
 */
@Pure
public fun <T> ListIterator<T>.asImmutable(): UnmodifiableListIteratorView<T> = when(this) {
	
	is UnmodifiableListIteratorView -> this
	
	else -> UnmodifiableListIteratorWrapper(this)
}
