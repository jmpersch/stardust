/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.require
import cc.persch.stardustx.collections.DEFAULT_CAPACITY

/**
 * The default load factor of maps.
 *
 * @see DEFAULT_CAPACITY
 */
public const val DEFAULT_LOAD_FACTOR: Float = 0.75f

internal fun checkInitialCapacity(initialCapacity: Int, param: String = "initialCapacity"): Int {
	
	require(param, initialCapacity >= 0)
	
	return initialCapacity
}

internal fun checkLoadFactor(loadFactor: Float, param: String = "loadFactor"): Float {
	
	require(param, loadFactor > 0 && !loadFactor.isNaN())
	
	return loadFactor
}

/**
 * Creates an empty [UnmodifiableMapView] implementation.
 *
 * @since 5.0
 * @see orEmpty
 */
@Suppress("UNCHECKED_CAST")
@Pure
public fun <K, V> emptyUnmodifiableMap(): UnmodifiableMapView<K, V> = EmptyUnmodifiableMap as UnmodifiableMapView<K, V>

/**
 * Creates a [Map.Entry] with this as the entry's key and the given [value] as the entry's value.
 *
 * @since 4.0
 * @see MapEntry
 */
@Pure
public infix fun <K, V> K.mapTo(value: V): Map.Entry<K, V> = MapEntry(this, value)

/**
 * Creates a mutable copy.
 *
 * @since 4.0
 */
@Pure
public fun <K, V> Map<K, V>.mutableCopy(): MutableMap<K, V> = toMutableMap()
