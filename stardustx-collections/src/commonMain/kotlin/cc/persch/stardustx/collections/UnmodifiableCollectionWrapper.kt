/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections

import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.iterators.asImmutable

/**
 * Defines a wrapper that provides an immutable view of a [Collection].
 *
 * @since 4.2
 */
public open class UnmodifiableCollectionWrapper<out E>(
	protected open val collection: Collection<E>
) : UnmodifiableCollectionView<E>
{
	override val size: Int
		get() = collection.size
	
	override fun contains(element: @UnsafeVariance E): Boolean = collection.contains(element)
	
	override fun containsAll(elements: Collection<@UnsafeVariance E>): Boolean = collection.containsAll(elements)
	
	override fun isEmpty(): Boolean = collection.isEmpty()
	
	override fun iterator(): UnmodifiableIteratorView<E> = collection.iterator().asImmutable()
	
	override fun hashCode(): Int = collection.hashCode()
	
	override fun equals(other: Any?): Boolean = collection == other
	
	override fun toString(): String = collection.toString()
}
