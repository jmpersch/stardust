/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.rope.RopeIndentable

/**
 * Defines a multi-map.
 *
 * @since 4.6
 */
public interface MultiMap<K, out V>: Iterable<Map.Entry<K, Set<V>>>, RopeIndentable {
	
	/**
	 * Gets the size of this collection (the count of keys)
	 */
	public val size: Int
	
	/**
	 * Gets a read-only set of all keys in this collection.
	 */
	public val keys: Set<K>
	
	/**
	 * Gets a value indicating whether this collection is empty.
	 * 
	 * @see isNotEmpty
	 * @see isNullOrEmpty
	 */
	@Pure
	public fun isEmpty(): Boolean = size == 0
	
	/**
	 * Gets a value indicating whether this collection is *not* empty.
	 * 
	 * @since 5.0
	 * @see isEmpty
	 * @see isNullOrEmpty
	 */
	@Pure
	public fun isNotEmpty(): Boolean = size != 0
	
	/**
	 * Calculates the over-all size of this collection.
	 */
	@Pure
	public fun overAllSize(): Int = asSequence().sumOf { it.value.size }
	
	/**
	 * Gets the sub-set of the given [key]; or `null`, if the key was not found.
	 *
	 * Note: The sub-set may be empty.
	 *
	 * @see getNonEmpty
	 */
	@Pure
	public fun get(key: K): Set<V>?
	
	/**
	 * Gets the sub-set of the given [key]. If the key was not found or the sub-set is empty, `null` is returned.
	 *
	 * @see get
	 */
	@Pure
	public fun getNonEmpty(key: K, returnNullOnEmpty: Boolean): Set<V>? {
		
		val set = get(key)
		
		return if(set.isNullOrEmpty()) null else set
	}
	
	/**
	 * Gets a value indicating whether this collection contains the given [key]. The associated sub-set might be empty.
	 *
	 * @see containsKeyNonEmpty
	 */
	@Pure
	public fun containsKey(key: K): Boolean
	
	/**
	 * Gets a value indicating whether this collection contains the given [key]
	 * and the associated sub-set is *not* empty.
	 *
	 * @see containsKey
	 * @since 6.0
	 */
	@Pure
	public fun containsKeyNonEmpty(key: K): Boolean
	
	/**
	 * Returns `true`, if this multi-map contains the given [value].
	 *
	 * Note: This is an expensive operation, because it iterates through all values of all subsets.
	 */
	@Pure
	public fun containsValue(value: @UnsafeVariance V): Boolean = asValueSequence().any { it == value }
	
	/**
	 * Returns `true`, if the set of the given [key] contains the given [value].
	 */
	@Pure
	public fun containsValue(key: K, value: @UnsafeVariance V): Boolean = get(key)?.contains(value) ?: false
	
	@Pure
	override fun iterator(): Iterator<Map.Entry<K, Set<V>>>
	
	@Pure
	public fun asSequence(): Sequence<Map.Entry<K, Set<V>>>
	
	@Pure
	public fun asValueSequence(): Sequence<V>
	
	/**
	 * Creates a mutable copy.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun <K, V> MultiMap<K, V>.mutableCopy(): MutableMultiMap<K, V> = toMutableMultiMap()
	
	/**
	 * Returns an unmodifiable view.
	 */
	@Pure
	public fun asUnmodifiableView(): UnmodifiableMultiMapView<K, V> = when(this) {
		
		is UnmodifiableMultiMapView -> this
		
		else -> UnmodifiableMultiMapWrapper(this)
	}
}
