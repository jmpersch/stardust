/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

// MutableSet Builders /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a [MutableSet] using the given [builderAction].
 *
 * @since 4.0
 * @see mutableSetOf
 */
@Pure
@TypeSafeBuilder
public inline fun <T> buildMutableSet(
	@BuilderInference builderAction: MutableSet<T>.() -> Unit
) : MutableSet<T> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return LinkedHashSet<T>().apply(builderAction)
}

/**
 * Creates a [MutableSet] with the [initialCapacity] using the given [builderAction].
 *
 * @since 5.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see mutableSetOf
 */
@Pure
@TypeSafeBuilder
public inline fun <T> buildMutableSet(
	initialCapacity: Int,
	@BuilderInference builderAction: MutableSet<T>.() -> Unit
) : MutableSet<T> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return LinkedHashSet<T>(initialCapacity).apply(builderAction)
}

// MutableOrderedSet Builders //////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a [MutableIndexSet] using the given [builderAction].
 *
 * @since 4.0
 * @see mutableOrderedSetOf
 */
@Pure
@TypeSafeBuilder
public inline fun <T> buildMutableIndexSet(
	@BuilderInference builderAction: MutableIndexSet<T>.() -> Unit
) : MutableIndexSet<T> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return MutableHashIndexSet<T>().apply(builderAction)
}

/**
 * Creates a [MutableIndexSet] with the [initialCapacity] using the given [builderAction].
 *
 * @since 5.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see mutableOrderedSetOf
 */
@Pure
@TypeSafeBuilder
public inline fun <T> buildMutableIndexSet(
	initialCapacity: Int,
	@BuilderInference builderAction: MutableIndexSet<T>.() -> Unit
) : MutableIndexSet<T> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return MutableHashIndexSet<T>(initialCapacity).apply(builderAction)
}

