/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

// Bunch Builders //////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a [Bunch] using the given [builderAction].
 *
 * @since 5.0
 * @see bunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildBunch(
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableBunch<K, V>.() -> Unit
) : Bunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return buildMutableBunch(keyExtractor, builderAction)
}

/**
 * Creates a [Bunch] with the given [initialCapacity], [keyExtractor], and [builderAction].
 *
 * @since 5.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see bunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildBunch(
	initialCapacity: Int,
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableBunch<K, V>.() -> Unit
) : Bunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return buildMutableBunch(initialCapacity, keyExtractor, builderAction)
}

// Ordered Bunch Builders //////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates an [IndexBunch] using the given [keyExtractor] and [builderAction].
 *
 * @since 5.0
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildIndexBunch(
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableIndexBunch<K, V>.() -> Unit
) : IndexBunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return buildMutableIndexBunch(keyExtractor, builderAction)
}

/**
 * Creates an [IndexBunch] with the given [initialCapacity], [keyExtractor], and [builderAction].
 *
 * @since 5.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildIndexBunch(
	initialCapacity: Int,
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableIndexBunch<K, V>.() -> Unit
) : IndexBunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return buildMutableIndexBunch(initialCapacity, keyExtractor, builderAction)
}
