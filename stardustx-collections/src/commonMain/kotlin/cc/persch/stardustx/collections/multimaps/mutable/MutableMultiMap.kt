/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardustx.collections.__accumulate
import kotlin.jvm.JvmName

/**
 * Defines a mutable [MultiMap].
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 * 
 * @since 4.6
 */
public interface MutableMultiMap<K, V>: MultiMap<K, V>, MutableIterable<Map.Entry<K, Set<V>>> {
		
	@Pure
	override fun get(key: K): MutableSet<V>?
	
	/**
	 * Gets the associated sub-set of the given [key].
	 * If the key is absent, an empty sub-set is put into this collection and returned.
	 */
	@ShouldUse
	public fun getOrPutEmpty(key: K): MutableSet<V>
	
	@ShouldUse
	public fun getOrPutEmpty(key: K, initialSetCapacity: Int): MutableSet<V>
	
	@ShouldUse
	public fun getOrPutEmpty(key: K, initialSetCapacity: Int, initialSetLoadFactor: Float): MutableSet<V>
	
	/**
	 * Associates an empty sub-set with the given [key], if the key is absent.
	 */
	public fun putEmptyIfAbsent(key: K): Boolean
	
	public fun putEmptyIfAbsent(key: K, initialSetCapacity: Int): Boolean
	
	public fun putEmptyIfAbsent(key: K, initialSetCapacity: Int, initialSetLoadFactor: Float): Boolean
	
	/**
	 * Associates the given [value] with the given [key].
	 */
	public fun put(key: K, value: V): Boolean = getOrPutEmpty(key).add(value)
	
	/**
	 * Associates all given [values] with the given [key].
	 */
	public fun putAll(key: K, vararg values: V): Boolean = putAll(key, values.asList())
	
	/**
	 * Associates all given [values] with the given [key].
	 */
	public fun putAll(key: K, values: Iterable<V>): Boolean = getOrPutEmpty(key).addAll(values)
	
	/**
	 * Puts all given [pairs] into this collection.
	 */
	public fun putAll(vararg pairs: Pair<K, V>): Boolean = putAll(pairs.asList())
	
	/**
	 * Puts all given [pairs] into this collection.
	 */
	public fun putAll(pairs: Iterable<Pair<K, V>>): Boolean = pairs.__accumulate { (key, value) -> put(key, value) }
	
	/**
	 * Puts the elements of the given [collection] into this collection.
	 */
	public fun putAllEntries(collection: Iterable<Map.Entry<K, Set<V>>>): Boolean {
		
		if(this === collection)
			return false
		
		return collection.__accumulate { (key, values) -> putAll(key, values) }
	}
	
	/**
	 * Puts the elements of the [other] multimap into this collection.
	 */
	public fun putAll(other: MultiMap<K, V>): Boolean = putAllEntries(other)
	
	/**
	 * Clears the sub-set of the given [key]. The empty sub-set remains in this collection.
	 */
	public fun remove(key: K): Boolean = remove(key, prune = false)
	
	/**
	 * If [prune] is set to `true`, the sub-set of the given [key] is removed. If [prune] is set to `false`, the
	 * sub-set of the given [key] is cleared.
	 */
	public fun remove(key: K, prune: Boolean): Boolean
	
	/**
	 * Removes the given [value] of the sub-set of the given [key]. The sub-set may remain empty.
	 */
	public fun remove(key: K, value: V): Boolean = remove(key, value, prune = false)
	
	/**
	 * Removes the given [value] of the sub-set of the given [key]. If [prune] is set to `true` and the remaining
	 * sub-set is empty, it will be removed.
	 */
	public fun remove(key: K, value: V, prune: Boolean): Boolean
	
	/**
	 * Removes all empty sub-sets of this collection.
	 */
	public fun pruneAll()
	
	/**
	 * Removes all elements of this collection.
	 */
	public fun clear()
	
	@Pure
	override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, MutableSet<V>>>
	
	/**
	 * Creates a mutable copy of this collection.
	 */
	@Pure
	public fun mutableCopy(): MutableMultiMap<K, V> = toMutableMultiMap()
}
