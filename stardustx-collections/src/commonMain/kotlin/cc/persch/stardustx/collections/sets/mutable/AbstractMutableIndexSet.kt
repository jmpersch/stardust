/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardustx.collections.bunches.MutableIndexBunch
import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper

/**
 * Defines the base class of a [MutableIndexSet].
 *
 * @since 5.0
 */
public abstract class AbstractMutableIndexSet<E>: AbstractIndexSet<E>(), MutableIndexSet<E> {
	
	abstract override val bunch: MutableIndexBunch<E, E>
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun removeAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		if(elements === this) {
			
			val wasNotEmpty = isNotEmpty()
			
			clear()
			
			return wasNotEmpty
		}
		
		return bunch.removeAllKeys(elements)
	}
	
	override fun retainAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		if(elements === this)
			return false
		
		return bunch.retainAllKeys(elements)
	}
	
	override fun setAt(index: Int, element: E): Boolean {
		
		checkMutability()
		
		return bunch.setAt(index, element)
	}
	
	override fun add(element: E): Boolean {
		
		checkMutability()
		
		return bunch.add(element)
	}
	
	override fun addAll(elements: Iterable<E>): Boolean {
		
		checkMutability()
		
		return elements !== this && bunch.addAll(elements)
	}
	
	override fun insertAt(index: Int, element: E): Boolean {
		
		checkMutability()
		
		return bunch.insertAt(index, element)
	}
	
	override fun insertAllAt(index: Int, elements: Iterable<E>): Boolean {
		
		checkMutability()
		
		return elements !== this && bunch.insertAllAt(index, elements)
	}
	
	override fun remove(element: E): Boolean {
		
		checkMutability()
		
		return bunch.removeKey(element)
	}
	
	override fun removeAt(index: Int) {
		
		checkMutability()
		
		return bunch.removeAt(index)
	}
	
	override fun sort(comparator: Comparator<in E>) {
		
		checkMutability()
		
		return bunch.sortWith(comparator)
	}
	
	override fun clear() {
		
		checkMutability()
		
		return bunch.clear()
	}
	
	override fun reverse() {
		
		checkMutability()
		
		return bunch.reverse()
	}
	
	override fun iterator(): MutableIterator<E> = MutableIteratorWrapper(bunch.iterator(), this::checkMutability)
}
