/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exerting
import kotlin.jvm.JvmName

/**
 * Creates an empty [MutableMultiMap].
 *
 * @since 5.0
 */
@Pure
public fun <K, V> mutableHashMultiMapOf(): MutableMultiMap<K, V> = MutableHashMultiMap()

/**
 * Creates a [MutableMultiMap] of the given [key] and [value].
 *
 * @since 5.0
 */
@Pure
public fun <K, V> mutableHashMultiMapOf(key: K, value: V): MutableMultiMap<K, V> =
	mutableHashMultiMapOf<K, V>() exerting { put(key, value) }

/**
 * Creates a [MutableMultiMap] of the given key-value [pairs].
 *
 * @since 5.0
 * @see toMutableMultiMap
 */
@Pure
public fun <K, V> mutableHashMultiMapOf(vararg pairs: Pair<K, V>): MutableMultiMap<K, V> =
	pairs.asList().toMutableHashMultiMap()

/**
 * Creates a [MutableMultiMap] of the given [key] and its associated [values].
 *
 * @since 4.6
 */
@Pure
public fun <K, V> mutableHashMultiMapOf(key: K, vararg values: V): MutableMultiMap<K, V> =
	mutableHashMultiMapOf(key, values.asList())

/**
 * Creates a [MutableMultiMap] of the given [key] and its associated [values].
 *
 * @since 4.6
 */
@Pure
public fun <K, V> mutableHashMultiMapOf(key: K, values: Iterable<V>): MutableMultiMap<K, V> =
	mutableHashMultiMapOf<K, V>() exerting { putAll(key, values) }

/**
 * Creates a [MutableMultiMap].
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Sequence<Pair<K, V>>.toMutableHashMultiMap(): MutableMultiMap<K, V> = asIterable().toMutableHashMultiMap()

/**
 * Creates a [MutableMultiMap].
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Iterable<Pair<K, V>>.toMutableHashMultiMap(): MutableMultiMap<K, V> =
	mutableHashMultiMapOf<K, V>() exerting { putAll(this@toMutableHashMultiMap) }

/**
 * Returns a [MutableMultiMap].
 *
 * @since 5.0
 */
@JvmName("multiEntriesToMutableHashMultiMap")
@Pure
public fun <K, V> Sequence<Map.Entry<K, Iterable<V>>>.toMutableHashMultiMap(): MutableMultiMap<K, V> =
	asIterable().toMutableHashMultiMap()

/**
 * Returns a [MutableMultiMap].
 *
 * @since 4.6
 */
@JvmName("multiEntriesToMutableMultiMap")
@Pure
public fun <K, V> Iterable<Map.Entry<K, Iterable<V>>>.toMutableHashMultiMap(): MutableMultiMap<K, V> =
	buildMutableHashMultiMap { putAll(this@toMutableHashMultiMap) }
