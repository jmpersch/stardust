/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardustx.collections.UnmodifiableCollectionView
import cc.persch.stardustx.collections.lists.__emptyUnmodifiableList
import cc.persch.stardustx.collections.sets.EmptyUnmodifiableSet
import cc.persch.stardustx.collections.sets.UnmodifiableSetView

/**
 * Defines an empty [UnmodifiableMapView] implementation.
 *
 * @since 5.0
 */
@ThreadSafe
public object EmptyUnmodifiableMap: UnmodifiableMapView<Nothing, Nothing> {
	
	override val entries: UnmodifiableSetView<UnmodifiableMapView.UnmodifiableEntryView<Nothing, Nothing>>
		get() = EmptyUnmodifiableSet
	
	override val keys: UnmodifiableSetView<Nothing>
		get() = EmptyUnmodifiableSet
	
	override val size: Int
		get() = 0
	
	override val values: UnmodifiableCollectionView<Nothing>
		get() = __emptyUnmodifiableList()
	
	@Pure
	override fun equals(other: Any?): Boolean = other is Map<*, *> && other.isEmpty()
	
	@Pure
	override fun hashCode(): Int = 1
	
	@Pure
	override fun toString(): String = "{}"
	
	@Pure
	override fun containsKey(key: Nothing): Boolean = false
	
	@Pure
	override fun containsValue(value: Nothing): Boolean = false
	
	@Pure
	override fun get(key: Nothing): Nothing? = null
	
	@Pure
	override fun isEmpty(): Boolean = true
}
