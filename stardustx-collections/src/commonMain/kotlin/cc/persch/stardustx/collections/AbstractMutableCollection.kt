/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections

import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper

/**
 * Defines the base class for a [MutableCollection].
 *
 * @since 5.0
 */
public abstract class AbstractMutableCollection<E>: MutableCollection<E> {
	
	protected abstract val collection: MutableCollection<E>
	
	override val size: Int
		get() = collection.size
	
	override fun isEmpty(): Boolean = collection.isEmpty()
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun contains(element: E): Boolean = collection.contains(element)
	
	override fun containsAll(elements: Collection<E>): Boolean = collection.containsAll(elements)
	
	override fun iterator(): MutableIterator<E> =
		MutableIteratorWrapper(collection.iterator(), this::checkMutability)
	
	override fun equals(other: Any?): Boolean = collection == other
	
	override fun hashCode(): Int = collection.hashCode()
	
	override fun toString(): String = collection.toString()
	
	override fun add(element: E): Boolean {
		
		checkMutability()
		
		return collection.add(element)
	}
	
	override fun addAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		return collection.addAll(elements)
	}
	
	override fun clear() {
		
		checkMutability()
		
		return collection.clear()
	}
	
	override fun remove(element: E): Boolean {
		
		checkMutability()
		
		return collection.remove(element)
	}
	
	override fun removeAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		@Suppress("ConvertArgumentToSet")
		return collection.removeAll(elements)
	}
	
	override fun retainAll(elements: Collection<E>): Boolean {
		
		checkMutability()
		
		@Suppress("ConvertArgumentToSet")
		return collection.retainAll(elements)
	}
}
