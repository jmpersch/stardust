/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper
import cc.persch.stardustx.collections.maps.checkInitialCapacity
import cc.persch.stardustx.collections.maps.checkLoadFactor

/**
 * Defines the base class for a [MutableMultiMap].
 *
 * @since 5.0
 */
public abstract class AbstractMutableMultiMap<K, V>(
	private val initialSetCapacity: Int,
	private val setLoadFactor: Float
) : AbstractMultiMap<K, V>(), MutableMultiMap<K, V> {
	
	init {
		
		checkInitialCapacity(initialSetCapacity, "initialSetCapacity")
		checkLoadFactor(setLoadFactor, "setLoadFactor")
	}
	
	abstract override val map: MutableMap<K, MutableSet<V>>
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	@Pure
	protected abstract fun createMutableSet(initialSetCapacity: Int, setLoadFactor: Float): MutableSet<V>
	
	@Pure
	override fun get(key: K): MutableSet<V>? = map.get(key)
	
	@ShouldUse
	override fun getOrPutEmpty(key: K): MutableSet<V> = getOrPutEmpty(key, initialSetCapacity, setLoadFactor)
	
	@ShouldUse
	public override fun getOrPutEmpty(key: K, initialSetCapacity: Int): MutableSet<V> =
		getOrPutEmpty(key, initialSetCapacity, setLoadFactor)
	
	@ShouldUse
	public override fun getOrPutEmpty(key: K, initialSetCapacity: Int, initialSetLoadFactor: Float): MutableSet<V> =
		map.getOrPut(key) { createMutableSet(initialSetCapacity, initialSetLoadFactor) }
	
	override fun putEmptyIfAbsent(key: K): Boolean = putEmptyIfAbsent(key, initialSetCapacity, setLoadFactor)
	
	public override fun putEmptyIfAbsent(key: K, initialSetCapacity: Int): Boolean =
		putEmptyIfAbsent(key, initialSetCapacity, setLoadFactor)
	
	public override fun putEmptyIfAbsent(key: K, initialSetCapacity: Int, initialSetLoadFactor: Float): Boolean {
		
		checkMutability()
		
		if(map.containsKey(key))
			return false
		
		map.put(key, createMutableSet(initialSetCapacity, initialSetLoadFactor))
		
		return true
	}
	
	override fun remove(key: K, prune: Boolean): Boolean {
		
		checkMutability()
		
		if(prune)
			return map.remove(key) != null
		
		val entry = get(key) ?: return false
		
		entry.clear()
		
		return true
	}
	
	override fun remove(key: K, value: V, prune: Boolean): Boolean {
		
		checkMutability()
		
		val subMap = get(key) ?: return false
		
		if(!subMap.remove(value))
			return false
		
		if(prune && subMap.isEmpty())
			map.remove(key)
		
		return true
	}
	
	override fun pruneAll() {
		
		checkMutability()
		
		val iterator = map.iterator()
		
		while(iterator.hasNext()) {
			
			val next = iterator.next()
			
			if(next.value.isEmpty())
				iterator.remove()
		}
	}
	
	override fun clear() {
		
		checkMutability()
		
		map.clear()
	}
	
	@Pure
	override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, MutableSet<V>>> =
		MutableIteratorWrapper(map.iterator(), this::checkMutability)
}
