/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.indenters.indentMap
import cc.persch.stardust.text.rope.indenters.orSimpleNameOf
import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.iterators.particular.EmptyUnmodifiableIterator
import cc.persch.stardustx.collections.sets.EmptyUnmodifiableSet
import cc.persch.stardustx.collections.sets.UnmodifiableSetView

/**
 * Defines an empty [UnmodifiableMultiMapView] implementation.
 *
 * @since 5.0
*/
@ThreadSafe
public object EmptyPersistentMultiMap: UnmodifiableMultiMapView<Nothing, Nothing> {
	
	override val size: Int
		get() = 0
	
	override fun get(key: Nothing): Nothing? = null
	
	override fun containsKey(key: Nothing): Boolean = false
	
	override fun containsKeyNonEmpty(key: Nothing): Boolean = false
	
	override fun hashCode(): Int = 1
	
	override fun equals(other: Any?): Boolean = other is MultiMap<*, *> && other.size == 0
	
	override fun asSequence(): Sequence<Nothing> = emptySequence()
	
	override fun asValueSequence(): Sequence<Nothing> = emptySequence()
	
	override val keys: UnmodifiableSetView<Nothing>
		get() = EmptyUnmodifiableSet
	
	override fun iterator(): UnmodifiableIteratorView<Nothing> = EmptyUnmodifiableIterator
	
	override fun ropeIndented(builder: RopeBuilder, name: CharSequence?, bodyOption: BodyOption): Unit =
		builder.indentMap(emptyMap<Nothing, Nothing>(), name.orSimpleNameOf(this::class), bodyOption)
}
