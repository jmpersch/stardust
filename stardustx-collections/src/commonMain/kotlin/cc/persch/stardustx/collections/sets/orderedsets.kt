/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure

/**
 * Gets an empty [IndexSet].
 *
 * @see orEmpty
 */
@Cached
@Pure
public fun <E> emptyOrderedSet(): IndexSet<E> = EmptyUnmodifiableIndexSet

/**
 * Gets an empty [UnmodifiableIndexSetView] implementation.
 *
 * @see orEmpty
 */
@Cached
@Pure
public fun <E> emptyUnmodifiableOrderedSet(): UnmodifiableIndexSetView<E> = EmptyUnmodifiableIndexSet

/**
 * Gets an empty ordered set.
 * 
 * Better use [emptyOrderedSet].
 *
 * @see emptyOrderedSet
 * @see orEmpty
 */
@Cached
@Pure
public fun <E> orderedSetOf(): IndexSet<E> = emptyOrderedSet()

/**
 * Creates a singleton [IndexSet] containing the given [item].
 *
 * @since 4.0
 * @see buildIndexSet
 */
@Pure
public fun <E> orderedSetOf(item: E): IndexSet<E> = mutableOrderedSetOf(item)

/**
 * Creates an [IndexSet] containing the given [items].
 *
 * @since 5.0
 * @see buildIndexSet
 */
@Pure
public fun <E> orderedSetOf(vararg items: E): IndexSet<E> = mutableOrderedSetOf(*items)

/**
 * Returns this; or an empty [IndexSet], if this is `null`.
 *
 * @since 5.0
 * @see emptyOrderedSet
 */
@Cached
@Pure
public fun <E> IndexSet<E>?.orEmpty(): IndexSet<E> = this ?: emptyOrderedSet()

/**
 * Returns a [IndexSet].
 *
 * @since 5.0
 * @see orderedSetOf
 */
@Pure
public fun <E> Sequence<E>.toOrderedSet(): IndexSet<E> = toMutableOrderedSet()

/**
 * Returns a [IndexSet].
 *
 * @since 5.0
 * @see orderedSetOf
 */
@Pure
public fun <E> Iterable<E>.toOrderedSet(): IndexSet<E> = toMutableOrderedSet()
