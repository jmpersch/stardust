/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps.mutable

import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.pipe
import cc.persch.stardustx.collections.AbstractMutableCollection
import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper
import cc.persch.stardustx.collections.maps.AbstractMap
import cc.persch.stardustx.collections.sets.AbstractMutableSet

/**
 * Defines the base class for a [MutableMap].
 *
 * @since 5.0
 */
public abstract class AbstractMutableMap<K, V>: AbstractMap<K, V>(), MutableMap<K, V> {
	
	abstract override val map: MutableMap<K, V>
	
	private var _entries: InnerEntrySet? = null
	
	override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
		get() = this::_entries.getOrInitialize { InnerEntrySet(map.entries) }
	
	private var _keys: InnerSet? = null
	
	override val keys: MutableSet<K>
		get() = this::_keys.getOrInitialize { InnerSet(map.keys) }
	
	private var _values: InnerCollection? = null
	
	override val values: MutableCollection<V>
		get() = this::_values.getOrInitialize { InnerCollection(map.values) }
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun clear() {
		
		checkMutability()
		
		map.clear()
	}
	
	override fun put(key: K, value: V): V? {
		
		checkMutability()
		
		return map.put(key, value)
	}
	
	override fun putAll(from: Map<out K, V>) {
		
		checkMutability()
		
		if(from !== this)
			map.putAll(from)
	}
	
	override fun remove(key: K): V? {
		
		checkMutability()
		
		return map.remove(key)
	}
	
	private inner class InnerEntrySet(
		override val set: MutableSet<MutableMap.MutableEntry<K, V>>,
	) : AbstractMutableSet<MutableMap.MutableEntry<K, V>>() {
		
		override fun checkMutability() = this@AbstractMutableMap.checkMutability()
		
		override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, V>> =
			object : MutableIteratorWrapper<MutableMap.MutableEntry<K, V>>(
				set.iterator(),
				this@AbstractMutableMap::checkMutability
			) {
				override fun next(): MutableMap.MutableEntry<K, V> = super.next() pipe { InnerEntry(it) }
			}
		
		private inner class InnerEntry<K, V>(
			private val entry: MutableMap.MutableEntry<K, V>
		) : MutableMap.MutableEntry<K, V> {
			
			override val key: K
				get() = entry.key
			
			override val value: V
				get() = entry.value
			
			override fun setValue(newValue: V): V {
				
				checkMutability()
				
				return entry.setValue(newValue)
			}
			
			override fun equals(other: Any?) = entry == other
			
			override fun hashCode() = entry.hashCode()
			
			override fun toString() = entry.toString()
		}
	}
	
	private inner class InnerSet(
		override val set: MutableSet<K>,
	) : AbstractMutableSet<K>() {
		
		override fun checkMutability() = this@AbstractMutableMap.checkMutability()
	}
	
	private inner class InnerCollection(
		override val collection: MutableCollection<V>
	) : AbstractMutableCollection<V>() {
		
		override fun checkMutability() = this@AbstractMutableMap.checkMutability()
	}
}
