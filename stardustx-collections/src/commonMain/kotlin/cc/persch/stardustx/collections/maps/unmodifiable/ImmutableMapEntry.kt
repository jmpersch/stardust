/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.threading.ThreadSafe

/**
 * Defines an open default implementation of the [UnmodifiableMapView.UnmodifiableEntryView] interface with the following features:
 *
 * * Implementation of the [hashCode], [equals], and [toString] methods
 * * Destructuring declarations, e.g. `val (key, value) = entry`
 *     * [component1] → [key]
 *     * [component2] → [value]
 * * [Copy][copy] method
 *
 * Use [mapTo] to create a map entry: `val entry = key mapTo value`
 *
 * @since 5.0
 * @see MapEntry
 * @see mapTo
 * @see UnmodifiableMapEntryWrapper
 * @see asUnmodifiableView
 *
 * @constructor Creates a new instance containing the given [key] and [value].
 */
@ThreadSafe
public open class ImmutableMapEntry<K, out V>(
	override val key: K,
	override val value: V
) : AbstractMapEntry<K, V>(), UnmodifiableMapView.UnmodifiableEntryView<K, V> {
	
	override fun copy(key: K, value: @UnsafeVariance V): Map.Entry<K, V> = ImmutableMapEntry(key, value)
}
