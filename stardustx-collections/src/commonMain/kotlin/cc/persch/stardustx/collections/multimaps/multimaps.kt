/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract
import kotlin.jvm.JvmName

/**
 * Gets an empty [MultiMap].
 *
 * @see orEmpty
 */
@Cached
@Pure
public fun <K, V> emptyMultiMap(): MultiMap<K, V> = emptyUnmodifiableMultiMap()

/**
 * Gets an empty [UnmodifiableMultiMapView] implementation.
 *
 * @see orEmpty
 */
@Suppress("UNCHECKED_CAST")
@Cached
@Pure
public fun <K, V> emptyUnmodifiableMultiMap(): UnmodifiableMultiMapView<K, V> =
	EmptyPersistentMultiMap as UnmodifiableMultiMapView<K, V>

/**
 * Gets an empty [MultiMap].
 * 
 * Better use [emptyMultiMap].
 *
 * @see emptyMultiMap
 * @see orEmpty
 */
@Cached
@Pure
public fun <K, V> multiMapOf(): MultiMap<K, V> = emptyUnmodifiableMultiMap()

/**
 * Gets a singleton [MultiMap] of the given key-value [pair].
 *
 * @since 5.0
 * @see buildMultiMap
 */
@Pure
public fun <K, V> multiMapOf(pair: Pair<K, V>): MultiMap<K, V> = mutableMultiMapOf(pair)

/**
 * Gets a singleton [MultiMap] of the given key-value [pair].
 *
 * @since 5.0
 * @see buildMultiMap
 */
@Pure
public fun <K, V> multiMapOf(vararg pairs: Pair<K, V>): MultiMap<K, V> = mutableMultiMapOf(*pairs)

/**
 * Returns this; or an empty [MultiMap], if this is `null`.
 *
 * @since 5.0
 * @see emptyMultiMap
 */
@Cached
@Pure
public fun <K, V> MultiMap<K, V>?.orEmpty(): MultiMap<K, V> = this ?: emptyUnmodifiableMultiMap()

///**
// * Gets the mapped value to the given [key], or `null` if the key was not found.
// */
//@Suppress("UNCHECKED_CAST")
//operator fun <K, V> MultiMap<out K, V>.get(key: K) = (this as MultiMap<K, V>).get(key)

/**
 * Gets a value indicating whether this [MultiMap] is `null` or empty.
 *
 * @since 5.0
 */
@Pure
public fun MultiMap<*, *>?.isNullOrEmpty(): Boolean {
	
    contract { returns(false) implies (this@isNullOrEmpty != null) }

    return this == null || isEmpty()
}

/**
 * Returns this [MultiMap], if it is not `null` and not empty. Otherwise, the result of [defaultValue] is returned.
 *
 * @since 5.0
 */
@Pure
public inline fun <C, R: MultiMap<*, *>> C?.ifNullOrEmpty(defaultValue: () -> R): R where C : R {
	
	contract { callsInPlace(defaultValue, AT_MOST_ONCE) }
	
	return if (isNullOrEmpty()) defaultValue() else this
}

/**
 * Returns a [MultiMap].
 *
 * @since 5.0
 * @see multiMapOf
 */
@Pure
public fun <K, V> Sequence<Pair<K, V>>.toMultiMap(): MultiMap<K, V> = toMutableMultiMap()

/**
 * Returns a [MultiMap].
 *
 * @since 5.0
 * @see multiMapOf
 */
@Pure
public fun <K, V> Iterable<Pair<K, V>>.toMultiMap(): MultiMap<K, V> = toMutableMultiMap()

/**
 * Returns a [MultiMap].
 *
 * @since 5.0
 * @see multiMapOf
 */
@JvmName("multiEntriesToMultiMap")
@Pure
public fun <K, V> Sequence<Map.Entry<K, Iterable<V>>>.toMultiMap(): MultiMap<K, V> = toMutableMultiMap()

/**
 * Returns a [MultiMap].
 *
 * @since 5.0
 * @see multiMapOf
 */
@JvmName("multiEntriesToMultiMap")
@Pure
public fun <K, V> Iterable<Map.Entry<K, Iterable<V>>>.toMultiMap(): MultiMap<K, V> = toMutableMultiMap()

/**
 * Performs the given [action] for each entry in this [MultiMap].
 */
public inline fun <K, V> MultiMap<K, V>.forEach(action: (key: K, value: V) -> Unit) {
	
	for((key, values) in this) {
		
		for(value in values)
			action(key, value)
	}
}
