/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.iterators

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.iterators.particular.EmptyUnmodifiableIterator
import cc.persch.stardustx.collections.iterators.particular.EmptyUnmodifiableListIterator

/**
 * Gets an empty [Iterator].
 *
 * @since 5.0
 * @see emptyListIterator
 * @see emptyImmutableIterator
 * @see emptyImmutableListIterator
 */
@Cached
@Pure
public fun emptyIterator(): UnmodifiableIteratorView<Nothing> = EmptyUnmodifiableIterator

/**
 * Gets an empty [ListIterator].
 *
 * @since 5.0
 * @see emptyIterator
 * @see emptyImmutableIterator
 * @see emptyImmutableListIterator
 */
@Cached
@Pure
public fun emptyListIterator(): ListIterator<Nothing> = EmptyUnmodifiableListIterator
