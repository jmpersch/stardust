/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.bunches.IndexBunch
import cc.persch.stardustx.collections.sets.IndexSet

/**
 * Defines an interface for index collections.
 *
 * @since 5.0
 * @see IndexBunch
 * @see IndexSet
 */
public interface IndexCollection<out E>: Collection<E> {
	
	/**
	 * Returns the index of the last item; or `-1`, if this collection is empty.
	 *
	 * @since 5.0
	 */
	public val lastIndex: Int
		get() = size - 1
	
	/**
	 * Gets the value at the specified index.
	 *
	 * @param index The index of the value to get.
	 * @return The value at the supplied index.
	 * @throws IllegalArgumentException [index] is out of range.
	 */
	@Pure
	public fun getAt(index: Int): E
	
	/**
	 * Returns the first element.
	 *
	 * @throws NoSuchElementException The collection is empty.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun first(): E =
		if(isEmpty()) throw NoSuchElementException("Collection is empty.") else getAt(0)
	
	/**
	 * Returns the first element; or `null`, if the collection is empty.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun firstOrNull(): E? = if(isEmpty()) null else getAt(0)
	
	/**
	 * Returns the last element.
	 *
	 * @throws NoSuchElementException The collection is empty.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun last(): E =
		if(isEmpty()) throw NoSuchElementException("Collection is empty.") else getAt(size - 1)
	
	/**
	 * Returns the last element; or `null`, if the collection is empty.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun lastOrNull(): E? = if(isEmpty()) null else getAt(size - 1)
}
