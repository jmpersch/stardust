/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.makeStringOf

/**
 * Defines an open default implementation of the [Map.Entry] interface with the following features:
 *
 * * Implementation of the [hashCode], [equals], and [toString] methods
 * * Destructuring declarations, e.g. `val (key, value) = entry`
 *     * [component1] → [key]
 *     * [component2] → [value]
 * * [Copy][copy] method
 *
 * Use [mapTo] to create a map entry: `val entry = key mapTo value`
 *
 * @since 4.0
 * @see mapTo
 *
 * @constructor Creates a new instance containing the given [key] and [value].
 */
public abstract class AbstractMapEntry<K, out V>: Map.Entry<K, V> {
	
	/**
	 * Gets the [key] as destructuring component 1.
	 */
	@Pure
	public open fun component1(): K = key
	
	/**
	 * Gets the [value] as destructuring component 2.
	 */
	@Pure
	public open fun component2(): V = value
	
	/**
	 * Creates a copy of this class providing the option to exchange items (key or value).
	 */
	@Pure
	public abstract fun copy(key: K = this.key, value: @UnsafeVariance V = this.value): Map.Entry<K, V>
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other)
			return true
		
		if(other !is Map.Entry<*, *>)
			return false
		
		return key == other.key && value == other.value
	}
	
	override fun hashCode(): Int {
		
		var result = key.hashCode()
		result = 31 * result + value.hashCode()
		return result
	}
	
	override fun toString(): String = makeStringOf(this, AbstractMapEntry<*, *>::key, AbstractMapEntry<*, *>::value)
}
