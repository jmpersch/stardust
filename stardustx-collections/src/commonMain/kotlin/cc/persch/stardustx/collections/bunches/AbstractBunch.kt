/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import cc.persch.stardust.getOrInitialize
import kotlinx.serialization.Transient

/**
 * Defines the [AbstractBunch] class.
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 3.0
 */
public abstract class AbstractBunch<K, V>: Bunch<K, V> {
	
	protected abstract val map: Map<K, V>
	
	override val size: Int
		get() = map.size
	
	@Transient
	private var _keys: UnmodifiableSetWrapper<K>? = null
	
	override val keys: Set<K>
		get() = this::_keys.getOrInitialize { UnmodifiableSetWrapper(map.keys) }
	
	/**
	 * Extracts the key from the supplied value.
	 *
	 * @param value The value to extract the key from.
	 * @return The extracted key.
	 */
	@Pure
	protected fun extractKey(value: V): K = keyExtractor(value)
	
	override fun isEmpty(): Boolean = map.isEmpty()
	
	@Pure
	override operator fun get(key: K): V? = map.get(key)
	
	@Pure
	override fun hashCode(): Int = map.hashCode()
	
	@Pure
	override fun equals(other: Any?): Boolean {
		
		if(this === other)
			return true
		
		if(other is AbstractBunch<*, *>)
			return map == other.map
		
		@Suppress("UNCHECKED_CAST")
		return other is Collection<*> && size == other.size && containsAllValues(other as Iterable<V>)
	}
	
	@Pure
	override fun containsKey(key: K): Boolean = map.containsKey(key)
	
	@Pure
	override fun containsValue(value: V): Boolean = get(extractKey(value)) == value
	
	@Pure
	override fun toString(): String = joinToString(", ", "{", "}")
}
