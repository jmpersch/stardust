/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardustx.collections.DEFAULT_CAPACITY
import cc.persch.stardustx.collections.maps.DEFAULT_LOAD_FACTOR
import cc.persch.stardustx.collections.maps.checkInitialCapacity
import cc.persch.stardustx.collections.maps.checkLoadFactor

/**
 * Defines a [MutableMultiMap] that is backed by a [HashMap].
 *
 * @since 4.6
 */
public open class MutableHashMultiMap<K, V>(
	initialCapacity: Int = DEFAULT_CAPACITY,
	loadFactor: Float = DEFAULT_LOAD_FACTOR,
	initialSetCapacity: Int = DEFAULT_CAPACITY,
	setLoadFactor: Float = DEFAULT_LOAD_FACTOR
) : AbstractMutableMultiMap<K, V>(initialSetCapacity, setLoadFactor) {
	
	init {
		
		checkInitialCapacity(initialCapacity)
		checkLoadFactor(loadFactor)
	}
	
	override val map: MutableMap<K, MutableSet<V>> = HashMap(initialCapacity, loadFactor)
	
	override fun checkMutability() { /* Nothing to check */ }
	
	override fun createMutableSet(initialSetCapacity: Int, setLoadFactor: Float): MutableSet<V> =
		HashSet(initialSetCapacity, setLoadFactor)
}
