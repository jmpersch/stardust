/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardustx.collections.maps.UnmodifiableMapView
import cc.persch.stardustx.collections.sets.UnmodifiableSetView
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import cc.persch.stardust.getOrInitialize

/**
 * Defines a wrapper that provides an immutable view of a multi-map entry.
 *
 * @since 5.0
 * 
 * @constructor Creates a new instance wrapping the given [entry].
 */
internal class UnmodifiableMultiMapEntryWrapper<K, V>(
	private val entry: Map.Entry<K, Set<V>>
) : UnmodifiableMapView.UnmodifiableEntryView<K, UnmodifiableSetView<V>>
{
	override val key: K get() = entry.key
	
	private var _value: UnmodifiableSetWrapper<V>? = null
	
	override val value: UnmodifiableSetView<V> get() = this::_value.getOrInitialize { UnmodifiableSetWrapper(entry.value) }
	
	override fun equals(other: Any?) = entry == other
	
	override fun hashCode() = entry.hashCode()
	
	override fun toString() = entry.toString()
}
