/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.maps.UnmodifiableMapView
import cc.persch.stardustx.collections.multimaps.iterators.ImmutableMultiMapIteratorWrapper
import cc.persch.stardustx.collections.sets.UnmodifiableSetView
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.pipe
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.RopeBuilder
import kotlinx.serialization.Transient

/**
 * Defines a wrapper that provides an immutable view o a [MultiMap].
 *
 * @since 4.6
 * @see asUnmodifiableView
 *
 * @constructor Creates a new instance wrapping the given [multi-map][multiMap].
 */
public open class UnmodifiableMultiMapWrapper<K, out V>(protected val multiMap: MultiMap<K, V>): UnmodifiableMultiMapView<K, V> {
	
	override val size: Int
		get() = multiMap.size
	
	@Transient
	private var _keys: UnmodifiableSetWrapper<K>? = null
	
	override val keys: UnmodifiableSetView<K>
		get() = this::_keys.getOrInitialize { UnmodifiableSetWrapper(multiMap.keys) }
	
	override fun get(key: K): UnmodifiableSetView<V>? = multiMap.get(key)?.pipe(::UnmodifiableSetWrapper)
	
	override fun containsKey(key: K): Boolean = multiMap.containsKey(key)
	
	override fun containsKeyNonEmpty(key: K): Boolean = multiMap.containsKeyNonEmpty(key)
	
	override fun iterator(): UnmodifiableIteratorView<UnmodifiableMapView.UnmodifiableEntryView<K, UnmodifiableSetView<V>>> =
		ImmutableMultiMapIteratorWrapper(multiMap.iterator())
	
	override fun asSequence(): Sequence<UnmodifiableMapView.UnmodifiableEntryView<K, UnmodifiableSetView<V>>> = iterator().asSequence()
	
	override fun asValueSequence(): Sequence<V> = multiMap.asValueSequence()
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = multiMap.ropeIndented(builder, name, bodyOption)
	
	override fun hashCode(): Int = multiMap.hashCode()
	
	override fun equals(other: Any?): Boolean = multiMap == other
	
	override fun toString(): String = multiMap.toString()
}
