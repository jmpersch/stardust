/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure

/**
 * Creates an empty [Bunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see orEmpty
 * @see bunchOf
 */
@Pure
public fun <K, V> emptyBunchOf(keyExtractor: (V) -> K): Bunch<K, V> = EmptyUnmodifiableBunch(keyExtractor)

/**
 * Creates an empty [UnmodifiableBunchView] implementation using the given [keyExtractor].
 *
 * @since 5.0
 * @see orEmpty
 * @see bunchOf
 */
@Pure
public fun <K, V> emptyUnmodifiableBunchOf(keyExtractor: (V) -> K): UnmodifiableBunchView<K, V> =
	EmptyUnmodifiableBunch(keyExtractor)

/**
 * Creates an empty [Bunch] using the given [keyExtractor].
 * 
 * Better use [emptyBunchOf].
 *
 * @since 5.0
 * @see emptyBunchOf
 * @see orEmpty
 */
@Pure
public fun <K, V> bunchOf(keyExtractor: (V) -> K): Bunch<K, V> = emptyBunchOf(keyExtractor)

/**
 * Creates a [Bunch] using the given [keyExtractor] and [item].
 *
 * @since 5.0
 * @see buildBunch
 */
@Pure
public fun <K, V> bunchOf(keyExtractor: (V) -> K, item: V): Bunch<K, V> = mutableBunchOf(keyExtractor, item)

/**
 * Creates a [Bunch] using the given [keyExtractor] and [items].
 *
 * @since 5.0
 * @see buildBunch
 */
@Pure
public fun <K, V> bunchOf(keyExtractor: (V) -> K, vararg items: V): Bunch<K, V> =
	mutableBunchOf(keyExtractor, *items)

/**
 * Returns a [Bunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see bunchOf
 */
@Pure
public fun <K, V> Sequence<V>.toBunch(keyExtractor: (V) -> K): Bunch<K, V> = toMutableBunch(keyExtractor)

/**
 * Returns a [Bunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see bunchOf
 */
@Pure
public fun <K, V> Iterable<V>.toBunch(keyExtractor: (V) -> K): Bunch<K, V> = toMutableBunch(keyExtractor)

/**
 * Returns this; or creates empty [Bunch] using the given [keyExtractor], if this is `null`.
 *
 * @since 5.0
 * @see emptyBunchOf
 */
@Pure
public fun <K, V> Bunch<K, V>?.orEmpty(keyExtractor: (V) -> K): Bunch<K, V> =
	this ?: emptyBunchOf(keyExtractor)
