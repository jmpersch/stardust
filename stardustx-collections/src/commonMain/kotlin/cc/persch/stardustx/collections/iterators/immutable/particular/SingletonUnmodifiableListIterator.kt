/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.iterators.particular

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardustx.collections.iterators.UnmodifiableListIteratorView

/**
 * Defines singleton [UnmodifiableListIteratorView].
 *
 * @since 4.0
 *
 * @constructor Creates a new instance.
 */
@ThreadSafe
public class SingletonUnmodifiableListIterator<E>(
	element: E
) : AbstractSingletonUnmodifiableIterator<E>(element), UnmodifiableListIteratorView<E> {
	
	@Pure
	override fun hasPrevious(): Boolean = !hasNext()
	
	@Pure
	override fun previous(): E = if(hasPrevious()) element else throw NoSuchElementException()
	
	@Pure
	override fun nextIndex(): Int = if(hasNext()) 0 else -1
	
	@Pure
	override fun previousIndex(): Int = if(hasPrevious()) 0 else -1
}
