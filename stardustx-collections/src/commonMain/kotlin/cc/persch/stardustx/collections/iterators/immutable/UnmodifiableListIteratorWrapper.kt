/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.iterators

import cc.persch.stardust.annotations.Pure

/**
 * Defines an immutable [ListIterator] wrapper.
 *
 * @constructor Creates a new instance using the given [iterator]
 *
 * @since 1.0
 */
public open class UnmodifiableListIteratorWrapper<out E>(
	override val iterator: ListIterator<E>
) : IteratorWrapper<E>(), UnmodifiableListIteratorView<E> {
	
	@Pure
	override fun hasPrevious(): Boolean = iterator.hasPrevious()
	
	@Pure
	override fun nextIndex(): Int = iterator.nextIndex()
	
	@Pure
	override fun previous(): E = iterator.previous()
	
	@Pure
	override fun previousIndex(): Int = iterator.previousIndex()
}
