/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a [MultiMap] using the given [builder].
 *
 * @since 5.0
 * @see multiMapOf
 */
@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
@Pure
public inline fun <K, V> buildMultiMap(
	@BuilderInference builder: MutableMultiMap<K, V>.() -> Unit
) : MultiMap<K, V> {
	
	contract { callsInPlace(builder, EXACTLY_ONCE) }
	
	return buildMutableMultiMap(builder)
}

/**
 * Creates a [MultiMap] with the given [initialCapacity] using the given [builder].
 *
 * @since 5.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see multiMapOf
 */
@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
@Pure
public inline fun <K, V> buildMultiMap(
	initialCapacity: Int,
	@BuilderInference builder: MutableMultiMap<K, V>.() -> Unit
) : MultiMap<K, V> {
	
	contract { callsInPlace(builder, EXACTLY_ONCE) }
	
	return buildMutableMultiMap(initialCapacity, builder)
}
