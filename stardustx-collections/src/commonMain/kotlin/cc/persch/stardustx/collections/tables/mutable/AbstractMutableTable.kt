/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.tables.mutable

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardustx.collections.iterators.MutableIteratorWrapper
import cc.persch.stardustx.collections.maps.__remove
import cc.persch.stardustx.collections.maps.checkInitialCapacity
import cc.persch.stardustx.collections.maps.checkLoadFactor
import cc.persch.stardustx.collections.tables.AbstractTable
import cc.persch.stardustx.collections.tables.MutableTable

/**
 * Defines the base class for a [MutableTable].
 *
 * @since 5.0
 */
public abstract class AbstractMutableTable<R, C, V>(
	private val initialColumnCapacity: Int,
	private val columnLoadFactor: Float
) : AbstractTable<R, C, V>(), MutableTable<R, C, V> {
	
	init {
		
		checkInitialCapacity(initialColumnCapacity, "initialColumnCapacity")
		checkLoadFactor(columnLoadFactor, "columnLoadFactor")
	}
	
	abstract override val map: MutableMap<R, MutableMap<C, V>>
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	@Pure
	protected abstract fun createHashMap(initialColumnCapacity: Int, columnLoadFactor: Float): MutableMap<C, V>
	
	@Pure
	override fun get(rowKey: R): MutableMap<C, V>? = map.get(rowKey)
	
	@ShouldUse
	override fun getOrPutEmpty(rowKey: R): MutableMap<C, V> =
		getOrPutEmpty(rowKey, initialColumnCapacity, columnLoadFactor)
	
	@ShouldUse
	override fun getOrPutEmpty(rowKey: R, initialColumnCapacity: Int): MutableMap<C, V> =
		getOrPutEmpty(rowKey, initialColumnCapacity, columnLoadFactor)
	
	@ShouldUse
	override fun getOrPutEmpty(rowKey: R, initialColumnCapacity: Int, columnLoadFactor: Float): MutableMap<C, V> {
		
		checkMutability()
		
		return map.getOrPut(rowKey) { createHashMap(initialColumnCapacity, columnLoadFactor)}
	}
	
	override fun putEmptyIfAbsent(rowKey: R): Boolean {
		
		return putEmptyIfAbsent(rowKey, initialColumnCapacity, columnLoadFactor)
	}
	
	override fun putEmptyIfAbsent(rowKey: R, initialColumnCapacity: Int): Boolean {
		
		return putEmptyIfAbsent(rowKey, initialColumnCapacity, columnLoadFactor)
	}
	
	override fun putEmptyIfAbsent(rowKey: R, initialColumnCapacity: Int, columnLoadFactor: Float): Boolean {
		
		checkMutability()
		
		if(map.containsKey(rowKey))
			return false
		
		map.put(rowKey, createHashMap(initialColumnCapacity, columnLoadFactor))
		
		return true
	}
	
	override fun removeRow(rowKey: R, prune: Boolean): Boolean {
		
		checkMutability()
		
		if(prune)
			return map.remove(rowKey) != null
		
		val entry = get(rowKey) ?: return false
		
		entry.clear()
		
		return true
	}
	
	override fun removeCell(rowKey: R, columnKey: C, prune: Boolean): V? {
		
		checkMutability()
		
		val subMap = get(rowKey) ?: return null
		
		val removedValue = subMap.remove(columnKey) ?: return null
		
		if(prune && subMap.isEmpty())
			map.remove(rowKey)
		
		return removedValue
	}
	
	override fun removeCell(rowKey: R, columnKey: C, value: V, prune: Boolean): Boolean {
		
		checkMutability()
		
		val subMap = get(rowKey) ?: return false
		
		if(!subMap.__remove(columnKey, value))
			return false
		
		if(prune && subMap.isEmpty())
			map.remove(rowKey)
		
		return true
	}
	
	override fun pruneAll() {
		
		checkMutability()
		
		val iterator = map.iterator()
		
		while(iterator.hasNext()) {
			
			val next = iterator.next()
			
			if(next.value.isEmpty())
				iterator.remove()
		}
	}
	
	override fun clear() {
		
		checkMutability()
		
		map.clear()
	}
	
	@Pure
	override fun iterator(): MutableIterator<MutableMap.MutableEntry<R, MutableMap<C, V>>> =
		MutableIteratorWrapper(map.iterator(), this::checkMutability)
}
