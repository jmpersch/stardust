/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.exert
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

// MutableBunch Builders ///////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a [MutableBunch] using the given [keyExtractor] and [builderAction].
 *
 * @since 4.0
 * @see mutableBunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableBunch(
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableBunch<K, V>.() -> Unit
) : MutableBunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return mutableBunchOf(keyExtractor).apply(builderAction)
}

/**
 * Creates a [MutableBunch] with the given [initialCapacity] using the given [keyExtractor] and [builderAction].
 *
 * @since 5.0
 * @see mutableBunchOf
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableBunch(
	initialCapacity: Int,
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableBunch<K, V>.() -> Unit
) : MutableBunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return MutableOrderedBunch(initialCapacity, keyExtractor).apply(builderAction)
}

// MutableBunch Builders ///////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a [MutableIndexBunch] using the given [keyExtractor] and [builderAction].
 *
 * @since 4.0
 * @see mutableBunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableIndexBunch(
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableIndexBunch<K, V>.() -> Unit
) : MutableIndexBunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return mutableIndexBunchOf(keyExtractor).exert(builderAction)
}
/**
 * Creates a [MutableIndexBunch] using the given [keyExtractor] and [builderAction].
 *
 * @since 5.0
 * @see mutableBunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableIndexBunch(
	initialCapacity: Int,
	noinline keyExtractor: (V) -> K,
	@BuilderInference builderAction: MutableIndexBunch<K, V>.() -> Unit
) : MutableIndexBunch<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return MutableHashIndexBunch(initialCapacity, keyExtractor).exert(builderAction)
}
