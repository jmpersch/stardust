/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardustx.collections.DEFAULT_CAPACITY

/**
 * Creates a [MutableIndexBunch], using the given [keyExtractor].
 *
 * @since 3.0
 */
@Pure
public fun <K, V> mutableIndexBunchOf(keyExtractor: (V) -> K): MutableIndexBunch<K, V> =
	MutableHashIndexBunch(keyExtractor)

/**
 * Creates a [MutableIndexBunch] containing the specified [items], using the given [keyExtractor].
 *
 * @since 3.0
 */
@Pure
public fun <K, V> mutableIndexBunchOf(keyExtractor: (V) -> K, vararg items: V): MutableIndexBunch<K, V> =
	items.asList().toMutableIndexBunch(keyExtractor)

/**
 * Creates a [MutableIndexBunch] from this sequence using the given [keyExtractor].
 *
 * @since 4.0
 */
@Pure
public fun <K, V> Sequence<V>.toMutableIndexBunch(keyExtractor: (V) -> K): MutableIndexBunch<K, V> =
	asIterable().toMutableIndexBunch(keyExtractor)

/**
 * Creates a [MutableIndexBunch] from this iterable using the given [keyExtractor].
 *
 * @since 4.0
 */
@Pure
public fun <K, V> Iterable<V>.toMutableIndexBunch(
	keyExtractor: (V) -> K
) : MutableIndexBunch<K, V> = MutableHashIndexBunch(
	if(this is Collection<*>) size else DEFAULT_CAPACITY,
	keyExtractor
) exert { it.putAll(this) }

/**
 * Creates a new [MutableBunch] containing the values of this instance.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Bunch<K, V>.toMutableIndexBunch(): MutableIndexBunch<K, V> =
	MutableHashIndexBunch(keyExtractor) exert { it.putAll(this) }

///**
// * Adds a value to this [MutableIndexBunch].
// * If an entry with the key of this value already exists, an [IllegalStateException] is thrown.
// * 
// * @throws IllegalStateException An entry with the key of the given [value] already exists.
// * @since 6.0
// */
//public fun <K, V> MutableIndexBunch<K, V>.addOrThrow(value: V) {
//	
//	addOrThrow(value) { IllegalStateException("Failed adding value to ${MutableIndexBunch::class.getSimpleName()}")}
//}
//
///**
// * Adds a value to this [MutableIndexBunch].
// * If an entry with the key of this value already exists, the exception created by [exceptionSupplier] is thrown.
// *
// * @throws E An entry with the key of the given [value] already exists.
// * @since 6.0
// */
//@Throws(Throwable::class)
//public inline fun <K, V, E: Throwable> MutableIndexBunch<K, V>.addOrThrow(value: V, exceptionSupplier: (V) -> E) {
//	
//	if(!add(value))
//		throw exceptionSupplier(value)
//}
