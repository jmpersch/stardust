/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardustx.collections.__accumulate

/**
 * Defines an interface for mutable [ordered bunches][IndexBunch].
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 3.0
 */
public interface MutableIndexBunch<K, V>: IndexBunch<K, V>, MutableBunch<K, V> {
	
	/**
	 * Tries to set a value at the specified index.
	 *
	 * @param index The index to place the specified value.
	 * @param value The value to place at the specified index.
	 * @return `true`, if the [value] was set; otherwise, `false`.
	 * @throws IllegalArgumentException [index] is out of range.
	 */
	public fun setAt(index: Int, value: V): Boolean
	
	/**
	 * Tries to add a value to this set.
	 *
	 * Note: This function is identical with the call of `insertAt(size, value)`.
	 *
	 * @param value The value to add.
	 * @return `true`, if the [value] was added; otherwise, if the key already exists, `false`.
	 */
	public fun add(value: V): Boolean = insertAt(this.size, value)
	
	/**
	 * Tries to add any value of the specified collection to this set.
	 *
	 * @param values The values to add.
	 * @return `true`, if at least one value was added; otherwise, `false`.
	 */
	public fun addAll(values: Iterable<V>): Boolean = values !== this && insertAllAt(this.size, values)
	
	/**
	 * Tries to insert a value at the specified index.
	 *
	 * @param index The index to insert the specified value.
	 * @param value The value to insert at the specified index.
	 * @return `true`, if the [value] was inserted; otherwise, if the key already exists, `false`.
	 */
	public fun insertAt(index: Int, value: V): Boolean
	
	/**
	 * Tries to insert any value of the specified collection to this set at the specified index.
	 *
	 * @param index The index to insert the specified values
	 * @param values The values to add.
	 * @return `true`, if at least one value was added; otherwise, `false`.
	 */
	public fun insertAllAt(index: Int, values: Iterable<V>): Boolean
	
	/**
	 * Removes an element at the specified index.
	 *
	 * @param index The index of the element to remove.
	 */
	public fun removeAt(index: Int)
	
	/**
	 * Sorts this collection according to the order, induced by the specified comparator.
	 *
	 * @param comparator A comparator.
	 */
	public fun sortWith(comparator: Comparator<in V>)
	
	/**
	 * Reverses the elements in this collection.
	 *
	 * @since 5.0
	 */
	public fun reverse()
	
	/**
	 * Replaces the given value according to its key, independent of the index.
	 *
	 * @return The old value; or `null`, if there was no value assigned to the key.
	 * @since 5.0
	 */
	public fun replace(value: V): V?
	
	/**
	 * Replaces the given value according to its key, independent of the index.
	 *
	 * @return `true`, if at least one value was replaced, otherwise, `false`.
	 * @since 5.0
	 */
	public fun replaceAll(values: Iterable<V>): Boolean = values !== this && values.__accumulate { replace(it) != null }
	
	/**
	 * Adds the given value to this collection.
	 * If the key already exists, the existing value is replaced by the given one.
	 *
	 * @return The old value; or `null`, if there was no value assigned to the key.
	 * @since 5.0
	 */
	public fun addOrReplace(value: V): V?
	
	/**
	 * Add or replaces the given values, independent of the indices.
	 *
	 * @since 5.0
	 */
	public fun addOrReplaceAll(values: Iterable<V>) {
		
		if(values !== this)
			values.forEach(::addOrReplace)
	}
	
	// region MutableKeySet Implementation
	
	/**
	 * Implementation of [MutableBunch]: Tries to add a value to this collection.
	 *
	 * @param value The value to put.
	 * @return The previous value (independent of the index); or `null`, if there is no mapping for the key
	 *   of the specified value.
	 * @see add
	 * @see replace
	 * @see addOrReplace
	 */
	override fun put(value: V): V? = putIfAbsent(value)
	
	/**
	 * Implementation of [MutableBunch]: Tries to add a value to this collection.
	 *
	 * **Better use [add].**
	 *
	 * @param value The value to add.
	 * @return The previous set value (independent of the index); or `null`, if there is no mapping for the key
	 *   of the specified value.
	 * @see add
	 * @see replace
	 * @see addOrReplace
	 */
	override fun putIfAbsent(value: V): V? = if(add(value)) null else getByKeyOf(value)
	
	// endregion /MutableKeySet Implementation/
}
