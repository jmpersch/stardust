/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.lists.UnmodifiableListView
import cc.persch.stardustx.collections.lists.asUnmodifiableView

/**
 * Defines a wrapper that provides an immutable view of an [IndexSet].
 *
 * @since 4.2
 * 
 * @constructor Creates a new instance wrapping the given [collection].
 */
public open class UnmodifiableIndexSetWrapper<out E>(
	override val collection: IndexSet<E>
) : UnmodifiableSetWrapper<E>(collection), UnmodifiableIndexSetView<E> {
	
	@Pure
	override fun isEmpty(): Boolean = collection.isEmpty()
	
	@Pure
	override fun getAt(index: Int): E = collection.getAt(index)
	
	@Pure
	override fun indexOf(value: @UnsafeVariance E): Int = collection.indexOf(value)
	
	@Pure
	override fun indexOfReference(value: @UnsafeVariance E): Int = collection.indexOfReference(value)
	
	@Pure
	override fun asList(): UnmodifiableListView<E> = collection.asList().asUnmodifiableView()
}
