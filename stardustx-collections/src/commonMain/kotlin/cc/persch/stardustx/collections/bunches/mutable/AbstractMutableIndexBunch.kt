/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.Unchecked
import cc.persch.stardustx.collections.maps.__putIfAbsent
import cc.persch.stardustx.collections.maps.__remove
import cc.persch.stardust.require

/**
 * Defines the base class for a [MutableIndexBunch] implementation.
 *
 * @param K The type of the keys of this collection—should be immutable!
 * @param V The type of the values of this collection.
 *
 * @since 4.0
 */
public abstract class AbstractMutableIndexBunch<K, V>: AbstractIndexBunch<K, V>(), MutableIndexBunch<K, V> {
	
	abstract override val map: MutableMap<K, InnerEntry<K, V>>
	
	abstract override val list: MutableList<InnerEntry<K, V>>
	
	override var refreshIndex: Int = -1
	
	private fun touch() {
		
		modCount++
	}
	
	/**
	 * Sets the optimal refresh index.
	 */
	private fun setOptimalRefreshIndex(@Unchecked index: Int) {
		
		if(refreshIndex < 0)
			refreshIndex = index
		else if(index < refreshIndex)
			refreshIndex = index
	}
	
	private fun requireInsertionIndex(index: Int) {
		
		require("index", index in 0..list.size) { "The index is out of bounds." }
	}
	
	/**
	 * Checks for mutability. Prevents modification by throwing an exception.
	 *
	 * @throws IllegalStateException The collection is immutable.
	 */
	protected abstract fun checkMutability()
	
	override fun setAt(index: Int, value: V): Boolean {
		
		checkMutability()
		
		requireIndex(index)
		
		val key = extractKey(value)
		
		val currentEntry = list.get(index)
		
		// The key belongs to the current entry
		if(currentEntry.key == key) {
			currentEntry.value = value
			
			return true
		}
		
		touch()
		
		val newEntry = InnerEntry(index, key, value)
		
		// The key already exists
		if(map.__putIfAbsent(key, newEntry) != null)
			return false
		
		list.set(index, newEntry)
		
		__forceRemovalFromMap(currentEntry)
		
		return true
	}
	
	private fun __removeFromMap(entry: InnerEntry<K, V>) = map.__remove(entry.key, entry)
	
	private fun __forceRemovalFromMap(entry: InnerEntry<K, V>) {
		
		var isRegularRemoval = false
		
		try {
			isRegularRemoval = __removeFromMap(entry)
		}
		finally {
			if(!isRegularRemoval) {
				// A failed normal removal of the entry indicates a wrong implementation
				// of the `equals` and/or `hashCode` methods of the `Key` class.
				// In this case, the entry can only be removed by reference check.
				
				val keyIterator = map.values.iterator()
				
				while(keyIterator.hasNext()) {
					val next = keyIterator.next()
					
					if(next === entry) {
						keyIterator.remove()
						
						break
					}
				}
			}
		}
	}
	
	override fun insertAt(index: Int, value: V): Boolean {
		
		checkMutability()
		
		requireInsertionIndex(index)
		
		touch()
		
		val entry = InnerEntry(index, extractKey(value), value)
		
		if(map.__putIfAbsent(entry.key, entry) != null)
			return false
		
		list.add(index, entry)
		
		setOptimalRefreshIndex(index)
		
		return true
	}
	
	override fun insertAllAt(index: Int, values: Iterable<V>): Boolean {
		
		checkMutability()
		
		requireInsertionIndex(index)
		
		if(values === this)
			return false
		
		touch()
		
		val buffer = ArrayList<InnerEntry<K, V>>()
		
		try {
			
			values.forEach { value ->
				
				val entry = InnerEntry(index, extractKey(value), value)
				
				if(map.__putIfAbsent(entry.key, entry) == null)
					buffer.add(entry)
			}
		}
		finally {
			
			if(buffer.isNotEmpty()) {
				
				list.addAll(index, buffer)
				
				setOptimalRefreshIndex(index)
			}
		}
		
		return buffer.isNotEmpty()
	}
	
	override fun removeKey(key: K): Boolean {
		
		checkMutability()
		
		touch()
		
		val entry = map.remove(key) ?: return false
		
		optimizedIndexRefresh(entry)
		
		list.removeAt(entry.index)
		
		setOptimalRefreshIndex(entry.index)
		
		return true
	}
	
	override fun removeValue(value: V): Boolean {
		
		checkMutability()
		
		val entry = map.get(extractKey(value))
		
		if(entry == null || entry.value != value || !__removeFromMap(entry))
			return false
		
		touch()
		
		optimizedIndexRefresh(entry)
		
		list.removeAt(entry.index)
		
		setOptimalRefreshIndex(entry.index)
		
		return true
	}
	
	override fun removeByKeyOf(value: V): Boolean {
		
		return removeKey(extractKey(value))
	}
	
	override fun removeAt(index: Int) {
		
		checkMutability()
		
		requireIndex(index)
		
		val entry = list.get(index)
		
		touch()
		
		setOptimalRefreshIndex(index)
		
		list.removeAt(index)
		
		__forceRemovalFromMap(entry)
	}
	
	private inline fun <X> _removeAllBy(items: Iterable<X>, mapRemover: (X) -> Entry<K, V>?): Boolean {
		
		if(list.isEmpty())
			return false
		
		val firstEntry = list.get(0)
		
		optimizedIndexRefresh(firstEntry)
		
		val removedEntries = ArrayList<Entry<*, *>>()
		
		try {
			
			for(item in items) {
				
				val removedEntry = mapRemover(item) ?: continue
				
				setOptimalRefreshIndex(removedEntry.index)
				
				removedEntries.add(removedEntry)
			}
		}
		finally {
			
			if(removedEntries.isNotEmpty()) {
				touch()
				
				removedEntries.sortByDescending(Entry<*, *>::index)
				
				for(i in removedEntries.indices)
					list.removeAt(removedEntries[i].index)
			}
		}
		
		return removedEntries.isNotEmpty()
	}
	
	override fun removeAllKeys(keys: Iterable<K>): Boolean {
		
		checkMutability()
		
		return _removeAllBy(keys, map::remove)
	}
	
	override fun removeAllValues(values: Iterable<V>): Boolean {
		
		checkMutability()
		
		return _removeAllBy(values) {
			
			val entry = map.get(extractKey(it))
			
			if(entry != null && entry.value == it && __removeFromMap(entry)) entry
			else null
		}
	}
	
	override fun removeAllByKeyOf(values: Iterable<V>): Boolean {
		
		checkMutability()
		
		return _removeAllBy(values) { map.remove(extractKey(it)) }
	}
	
	private inline fun <X> _retainAll(
		itemsToRetain: Iterable<X>,
		crossinline entryConverter: (Entry<K, V>) -> X,
		resultMaker: (Iterable<X>) -> Boolean
	) : Boolean {
		
		if(list.isEmpty())
			return false
		
		val itemsToRetainSet = itemsToRetain.toSet()
		
		val itemsToRemove = list.asSequence()
			.map { entryConverter(it) }
			.filter { !itemsToRetainSet.contains(it) }
			.asIterable()
		
		return resultMaker(itemsToRemove)
	}
	
	override fun retainAllKeys(keys: Iterable<K>): Boolean {
		
		checkMutability()
		
		return _retainAll(keys, Entry<K, *>::key, this::removeAllKeys)
	}
	
	override fun retainAllValues(values: Iterable<V>): Boolean {
		
		checkMutability()
		
		return _retainAll(values, Entry<*, V>::value, this::removeAllValues)
	}
	
	override fun retainAllByKeyOf(values: Iterable<V>): Boolean {
		
		checkMutability()
		
		return _retainAll(values, Entry<*, V>::value, this::removeAllByKeyOf)
	}
	
	override fun clear() {
		
		checkMutability()
		
		if(isEmpty())
			return
		
		touch()
		
		map.clear()
		list.clear()
		
		refreshIndex = -1
	}
	
	override fun sortWith(comparator: Comparator<in V>) {
		
		checkMutability()
		
		if(isEmpty())
			return
		
		touch()
		
		setOptimalRefreshIndex(0)
		
		list.sortWith { o1, o2 -> comparator.compare(o1.value, o2.value) }
	}
	
	override fun reverse() {
		
		checkMutability()
		
		if(isEmpty())
			return
		
		touch()
		
		setOptimalRefreshIndex(0)
		
		list.reverse()
	}
	
	override fun replace(value: V): V? = __replace(value, addIfAbsent = false)
	
	override fun addOrReplace(value: V): V? = __replace(value, addIfAbsent = true)
	
	private fun __replace(value: V, addIfAbsent: Boolean): V? {
		
		checkMutability()
		
		if(isEmpty())
			return null
		
		val entry = map.get(extractKey(value))
		
		if(entry == null) {
			if(addIfAbsent)
				add(value)
			
			return null
		}
		
		val oldValue = entry.value
		
		entry.value = value
		
		return oldValue
	}
	
	private companion object {
		
		private val iteratorNilValue = Any()
	}
	
	protected class InnerEntry<K, V> internal constructor(
		index: Int,
		key: K,
		value: V
	) : Entry<K, V>(index, key, value) {
		
		override var value: V = value
			internal set
	}
	
	@Pure
	override fun iterator(): MutableIterator<V> = MutableBunchIterator()
	
	private inner class MutableBunchIterator: MutableIterator<V> {
		
		private var modCount = this@AbstractMutableIndexBunch.modCount
		
		private var index = 0
		
		@Suppress("UNCHECKED_CAST", "PrivatePropertyName")
		private val DUMMY = iteratorNilValue as V
		
		private var current: V = DUMMY
		
		override operator fun next(): V {
			
			check(hasNext()) { "No next element available." }
			
			current = getAt(index++)
			
			return current
		}
		
		override operator fun hasNext(): Boolean {
			
			ensureModCount()
			
			return index < list.size
		}
		
		override fun remove() {
			
			checkMutability()
			
			ensureModCount()
			
			check(current != DUMMY) { "Nothing to remove. Iterate to the next element to remove." }
			
			current = DUMMY
			
			removeAt(--index)
			
			refreshModCount()
		}
		
		private fun ensureModCount() {
			
			if(modCount != this@AbstractMutableIndexBunch.modCount)
				throw ConcurrentModificationException()
		}
		
		private fun refreshModCount() {
			
			modCount = this@AbstractMutableIndexBunch.modCount
		}
	}
}
