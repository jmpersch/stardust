/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.tables

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.multimaps.orEmpty
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract
import kotlin.jvm.JvmName

/**
 * Gets an empty [Table].
 * 
 * @see orEmpty
 */
@Cached
@Pure
public fun <R, C, V> emptyTable(): Table<R, C, V> = emptyUnmodifiableTable()

/**
 * Gets an empty [UnmodifiableTableView] implementation.
 *
 * @see orEmpty
 */
@Suppress("UNCHECKED_CAST")
@Cached
@Pure
public fun <R, C, V> emptyUnmodifiableTable(): UnmodifiableTableView<R, C, V> =
	EmptyUnmodifiableTable as UnmodifiableTableView<R, C, V>

/**
 * Gets an empty [Table].
 *
 * Better use [emptyTable].
 *
 * @since 5.0
 * @see emptyTable
 * @see orEmpty
 */
@Cached
@Pure
public fun <R, C, V> tableOf(): Table<R, C, V> = emptyUnmodifiableTable()

/**
 * Creates a singleton [Table] of the given [rowKey], [columnKey], and [value].
 *
 * @since 5.0
 * @see buildTable
 */
@Pure
public fun <R, C, V> tableOf(rowKey: R, columnKey: C, value: V): Table<R, C, V> =
	mutableTableOf(rowKey, columnKey, value)

/**
 * Creates a singleton [Table] of the given [rowKey] and [columns].
 *
 * @since 5.0
 * @see buildTable
 */
@Pure
public fun <R, C, V> tableOf(rowKey: R, vararg columns: Map.Entry<C, V>): Table<R, C, V> =
	mutableTableOf(rowKey, columns.asList())

/**
 * Creates a singleton [Table] of the given [rowKey] and [columns].
 *
 * @since 5.0
 * @see buildTable
 */
@Pure
public fun <R, C, V> tableOf(rowKey: R, columns: Iterable<Map.Entry<C, V>>): Table<R, C, V> =
	mutableTableOf(rowKey, columns)

/**
 * Creates a singleton [Table] of the given [triples of row keys, column keys, and values][triples].
 *
 * @since 5.0
 * @see buildTable
 */
@Pure
public fun <R, C, V> tableOf(vararg triples: Triple<R, C, V>): Table<R, C, V> = mutableTableOf(*triples)

/**
 * Returns this table; or an empty table, if this is `null`.
 *
 * @since 5.0
 * @see emptyTable
 * @see emptyPersistentTable
 */
@Cached
@Pure
public fun <R, C, V> Table<R, C, V>?.orEmpty(): Table<R, C, V> = this ?: emptyUnmodifiableTable()

///**
// * Gets the value mapped to the given [key], or `null` if the key was not found.
// */
//@Suppress("UNCHECKED_CAST")
//operator fun <R, C, V> Table<out R, C, V>.get(key: R) = (this as Table<R, C, V>).get(key)
//
///**
// * Gets the value mapped to the given [key] and [columnKey], or `null` if the column was not found.
// */
//@Suppress("UNCHECKED_CAST")
//operator fun <R, C, V> Table<out R, out C, V>.get(key: R, columnKey: C) = (this as Table<R, C, V>).get(key, columnKey)

/**
 * Gets a value indicating whether this [Table] is `null` or empty.
 *
 * @since 5.0
 */
@Pure
public fun Table<*, *, *>?.isNullOrEmpty(): Boolean {
	
    contract { returns(false) implies (this@isNullOrEmpty != null) }

    return this == null || isEmpty()
}

/**
 * Returns this [Table], if it is not `null` and not empty. Otherwise, the result of [defaultValue] is returned.
 *
 * @since 5.0
 */
@Pure
public inline fun <C, R: Table<*, *, *>> C?.ifNullOrEmpty(defaultValue: () -> R): R where C : R {
	
	contract { callsInPlace(defaultValue, AT_MOST_ONCE) }
	
	return if (isNullOrEmpty()) defaultValue() else this
}
/**
 * Returns a [Table] of the given [triples of row keys, column keys, and values][this@persistentTableOf].
 *
 * @since 5.0
 */
@JvmName("triplesToTable")
@Pure
public fun <R, C, V> Sequence<Triple<R, C, V>>.toTable(): Table<R, C, V> = toMutableTable()

/**
 * Returns a [Table] of the given [triples of row keys, column keys, and values][this@persistentTableOf].
 *
 * @since 5.0
 */
@JvmName("triplesToTable")
@Pure
public fun <R, C, V> Iterable<Triple<R, C, V>>.toTable(): Table<R, C, V> = toMutableTable()

/**
 * Returns a [Table].
 *
 * @since 5.0
 */
@Pure
public fun <R, C, V> Sequence<Map.Entry<R, Iterable<Map.Entry<C, V>>>>.toTable(): Table<R, C, V> = toMutableTable()

/**
 * Returns a [Table].
 *
 * @since 5.0
 * @see tableOf
 */
@Pure
public fun <R, C, V> Iterable<Map.Entry<R, Iterable<Map.Entry<C, V>>>>.toTable(): Table<R, C, V> = toMutableTable()

/**
 * Performs the given [action] for each entry in this [Table].
 */
public inline fun <R, C, V> Table<R, C, V>.forEach(action: (rowKey: R, columnKey: C, value: V) -> Unit) {
	
	for((rowKey, columns) in this) {
		
		for((columnKey, value) in columns)
			action(rowKey, columnKey, value)
	}
}
