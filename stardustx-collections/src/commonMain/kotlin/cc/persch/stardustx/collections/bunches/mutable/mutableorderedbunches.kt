/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardust.exert

/**
 * Creates an empty [MutableBunch] using the given [keyExtractor].
 *
 * @since 3.0
 */
@Pure
public fun <K, V> mutableBunchOf(keyExtractor: (V) -> K): MutableBunch<K, V> =
	MutableOrderedBunch(keyExtractor)

/**
 * Creates a [MutableBunch] containing the specified [items], using the given [keyExtractor].
 *
 * @since 3.0
 */
@Pure
public fun <K, V> mutableBunchOf(keyExtractor: (V) -> K, vararg items: V): MutableBunch<K, V> =
	items.asList().toMutableBunch(keyExtractor)

/**
 * Creates a [MutableBunch] from this sequence using the given [keyExtractor].
 *
 * @since 4.0
 */
@Pure
public fun <K, V> Sequence<V>.toMutableBunch(keyExtractor: (V) -> K): MutableBunch<K, V> =
	asIterable().toMutableBunch(keyExtractor)

/**
 * Creates a new [MutableBunch] from this iterable using the given [keyExtractor].
 *
 * @since 4.0
 */
@Pure
public fun <K, V> Iterable<V>.toMutableBunch(keyExtractor: (V) -> K): MutableBunch<K, V> =
	MutableOrderedBunch(keyExtractor) exert { it.putAll(this) }

/**
 * Creates a new [MutableBunch] containing the values of this instance.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Bunch<K, V>.toMutableBunch(): MutableBunch<K, V> =
	MutableOrderedBunch(keyExtractor) exert { it.putAll(this) }



