/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps.iterators

import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.maps.UnmodifiableMapView
import cc.persch.stardustx.collections.maps.UnmodifiableMapEntryWrapper
import cc.persch.stardust.pipe

/**
 * Defines an immutable entry iterator wrapper.
 * 
 * Each next entry is wrapped into an [UnmodifiableMapEntryWrapper].
 *
 * @since 5.0
 * @see UnmodifiableMapEntryWrapper
 * 
 * @constructor Creates a new instance using the given [iterator].
 */
public open class ImmutableEntryIteratorWrapper<K, out V>(
	protected val iterator: Iterator<Map.Entry<K, V>>
) : UnmodifiableIteratorView<UnmodifiableMapView.UnmodifiableEntryView<K, V>> {
	
	override operator fun hasNext(): Boolean = iterator.hasNext()
	
	override operator fun next(): UnmodifiableMapView.UnmodifiableEntryView<K, V> =
		iterator.next() pipe { UnmodifiableMapEntryWrapper(it) }
}
