/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps

/**
 * Defines an open default implementation of the [Map.Entry] interface with the following features:
 *
 * * Implementation of the [hashCode], [equals], and [toString] methods
 * * Destructuring declarations, e.g. `val (key, value) = entry`
 *     * [component1] → [key]
 *     * [component2] → [value]
 * * [Copy][copy] method
 *
 * @since 4.0
 * @see ImmutableMapEntry
 *
 * @constructor Creates a new instance containing the given [key] and [value].
 */
public open class MapEntry<K, out V>(
	override val key: K,
	override val value: V
) : AbstractMapEntry<K, V>() {
	
	override fun copy(key: K, value: @UnsafeVariance V): Map.Entry<K, V> = MapEntry(key, value)
}
