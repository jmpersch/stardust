/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.maps.DEFAULT_LOAD_FACTOR

/**
 * The default capacity of collections.
 *
 * @see DEFAULT_LOAD_FACTOR
 */
public const val DEFAULT_CAPACITY: Int = 16

/**
 * Accumulates over *all* elements of this collection.
 * A `false` value of the predicate does *not* stop the accumulation.
 *
 * @since 4.0
 */
@PublishedApi
@Pure
internal inline fun <V> Iterable<V>.__accumulate(predicate: (V) -> Boolean): Boolean
{
	var done = false
	
	for(element in this) {
		
		done = done or predicate(element)
	}
	
	return done
}

///**
// * Returns the first index of the given [element]; or `null`, if the collection does not contain the element.
// *
// * @since 4.0
// * @see indexOf
// * @see lastIndexOrNullOf
// */
//@Pure
//public fun <E> Iterable<E>.indexOrNullOf(element: E): Int? = indexOf(element) pipe { if(it < 0) null else it }
//
///**
// * Returns the last index of the given [element]; or `null`, if the collection does not contain the element.
// *
// * @since 4.0
// * @see lastIndexOf
// * @see indexOrNullOf
// */
//@Pure
//public fun <E> Iterable<E>.lastIndexOrNullOf(element: E): Int? = lastIndexOf(element) pipe { if(it < 0) null else it }

/**
 * Return an unmodifiable view.
 *
 * @since 5.0
 */
@Pure
public fun <T> Collection<T>.asUnmodifiableView(): UnmodifiableCollectionView<T> = when(this) {
	
	is UnmodifiableCollectionView -> this
	
	else -> UnmodifiableCollectionWrapper(this)
}
