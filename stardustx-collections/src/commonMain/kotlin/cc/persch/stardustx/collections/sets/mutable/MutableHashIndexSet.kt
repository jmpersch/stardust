/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

import cc.persch.stardustx.collections.DEFAULT_CAPACITY
import cc.persch.stardustx.collections.maps.DEFAULT_LOAD_FACTOR
import cc.persch.stardustx.collections.maps.checkInitialCapacity
import cc.persch.stardustx.collections.maps.checkLoadFactor
import cc.persch.stardustx.collections.bunches.MutableIndexBunch
import cc.persch.stardustx.collections.bunches.MutableHashIndexBunch

/**
 * Defines a mutable [IndexSet].
 *
 * @param E The type of the values of this collection.
 *
 * @since 4.0
 * 
 * @constructor Creates a new instance with the given [initialCapacity], [loadFactor], and [keyExtractor].
 * @throws IllegalArgumentException [initialCapacity] or [loadFactor] is less than zero;
 *   or [loadFactor] is not a number.
 */
public open class MutableHashIndexSet<E>(
	initialCapacity: Int,
	loadFactor: Float
) : AbstractMutableIndexSet<E>(), RandomAccess {
	
	override val bunch: MutableIndexBunch<E, E> = MutableHashIndexBunch(
		checkInitialCapacity(initialCapacity),
		checkLoadFactor(loadFactor)
	) { it }
	
	/**
	 * Creates a new instance.
	 */
	public constructor(): this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR)
	
	/**
	 * Creates a new instance with the given [initialCapacity].
	 *
	 * @throws IllegalArgumentException [initialCapacity] is less than zero.
	 */
	public constructor(initialCapacity: Int): this(initialCapacity, DEFAULT_LOAD_FACTOR)
	
	override fun checkMutability() { }
}
