/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.tables

import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.maps.UnmodifiableMapView
import cc.persch.stardustx.collections.maps.UnmodifiableMapEntryWrapper
import cc.persch.stardustx.collections.maps.UnmodifiableMapWrapper
import cc.persch.stardustx.collections.sets.UnmodifiableSetView
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import cc.persch.stardustx.collections.tables.iterators.ImmutableTableIteratorWrapper
import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.pipe
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.RopeBuilder

/**
 * Provides an immutable view of a [Table].
 *
 * @since 4.6
 * 
 * @constructor Creates a new instance wrapping the given [Table].
 */
public open class UnmodifiableTableWrapper<R, C, out V>(
	protected val table: Table<R, C, V>
) : UnmodifiableTableView<R, C, V> {
	
	override val size: Int
		get() = table.size
	
	override fun get(rowKey: R): UnmodifiableMapView<C, V>? = table.get(rowKey)?.pipe(::UnmodifiableMapWrapper)
	
	override fun get(rowKey: R, columnKey: C): V? = table.get(rowKey, columnKey)
	
	override fun containsKey(rowKey: R): Boolean = table.containsKey(rowKey)
	
	override fun containsKeyNonEmpty(rowKey: R): Boolean = table.containsKeyNonEmpty(rowKey)
	
	override fun asSequence(): Sequence<UnmodifiableMapView.UnmodifiableEntryView<R, UnmodifiableMapView<C, V>>> =
		table.asSequence().map { UnmodifiableTableEntryWrapper(it) }
	
	override fun asColumnSequence(): Sequence<UnmodifiableMapView.UnmodifiableEntryView<C, V>> =
		table.asColumnSequence().map { UnmodifiableMapEntryWrapper(it) }
	
	override fun asValueSequence(): Sequence<V> = table.asValueSequence()
	
	private var _rowKeys: UnmodifiableSetWrapper<R>? = null
	
	override val rowKeys: UnmodifiableSetView<R>
		get() = this::_rowKeys.getOrInitialize { UnmodifiableSetWrapper(table.rowKeys) }
	
	override fun iterator(): UnmodifiableIteratorView<UnmodifiableMapView.UnmodifiableEntryView<R, UnmodifiableMapView<C, V>>> =
		ImmutableTableIteratorWrapper(table.iterator())
	
	override fun ropeIndented(builder: RopeBuilder, name: CharSequence?, bodyOption: BodyOption): Unit =
		table.ropeIndented(builder, name, bodyOption)
}
