/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.IndexCollection

/**
 * Defines an ordered [set][Set].
 *
 * @param E The type of the values of this collection.
 *
 * @since 4.0
 */
public interface IndexSet<out E>: Set<E>, IndexCollection<E> {
	
	/**
	 * Gets the value at the specified index.
	 *
	 * @param index The index of the value to get.
	 * @return The value at the supplied index.
	 * @throws IllegalArgumentException [index] is out of range.
	 * @see getAt
	 */
	@Pure
	public operator fun get(index: Int): E = this.getAt(index)
	
	/**
	 * Tries to find the index of the specified value.
	 *
	 * @param value The value.
	 * @return The found index of the specified value; otherwise, `null`.
	 */
	@Pure
	public fun indexOf(value: @UnsafeVariance E): Int
	
	/**
	 * Tries to find the index of the key of the specified value.
	 *
	 * @param value The value.
	 * @return The found index of the key of the specified value; otherwise, `null`.
	 */
	@Pure
	public fun indexOfReference(value: @UnsafeVariance E): Int
	
	/**
	 * Returns a [List] view of this collection. The returned list is backed by this collection; hence, all changes in
	 * this collection are reflected by the returned list view.
	 *
	 * @since 5.0
	 */
	@Pure
	public fun asList(): List<E>
	
	/**
	 * Creates a mutable copy of this ordered set.
	 */
	@Pure
	public fun mutableCopy(): MutableIndexSet<@UnsafeVariance E> = toMutableOrderedSet()
	
	/**
	 * Returns an unmodifiable view.
	 */
	@Pure
	public fun asUnmodifiableView(): UnmodifiableIndexSetView<E> = when(this) {
		
		is UnmodifiableIndexSetView -> this
		
		else -> UnmodifiableIndexSetWrapper(this)
	}
}
