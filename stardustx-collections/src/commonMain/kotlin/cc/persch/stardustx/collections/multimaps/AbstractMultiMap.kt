/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.rope.*
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentMap
import cc.persch.stardust.text.rope.indenters.orSimpleNameOf
import cc.persch.stardust.text.rope.indenters.toIndentedStringWithoutBody

/**
 * Defines the base class for a [MultiMap].
 *
 * @since 5.0
 */
public abstract class AbstractMultiMap<K, V>: MultiMap<K, V> {
	
	protected abstract val map: Map<K, Set<V>>
	
	override val size: Int
		get() = map.size
	
	override val keys: Set<K>
		get() = map.keys
	
	@Pure
	override fun get(key: K): Set<V>? = map.get(key)
	
	@Pure
	override fun containsKey(key: K): Boolean = map.containsKey(key)
	
	@Pure
	override fun containsKeyNonEmpty(key: K): Boolean = !map.get(key).isNullOrEmpty()
	
	@Pure
	override fun iterator(): Iterator<Map.Entry<K, Set<V>>> = map.iterator()
	
	@Pure
	override fun asSequence(): Sequence<Map.Entry<K, Set<V>>> = map.asSequence()
	
	@Pure
	override fun asValueSequence(): Sequence<V> = map.values.asSequence().flatMap { it }
	
	@Pure
	override fun equals(other: Any?): Boolean {
		
		if(this === other)
			return true
		
		if(other is AbstractMultiMap<*, *>)
			return map == other.map
		
		if(other !is MultiMap<*, *> || size != other.size)
			return false
		
		@Suppress("UNCHECKED_CAST")
		other as MultiMap<K, V>
		
		for(entry in map) {
			
			val otherValue = other.get(entry.key) ?: return false
			
			if(entry.value != otherValue)
				return false
		}
		
		return true
	}
	
	@Pure
	override fun hashCode(): Int = map.hashCode()
	
	@Pure
	override fun toString(): String = toIndentedStringWithoutBody()
	
	override fun ropeIndented(builder: RopeBuilder, name: CharSequence?, bodyOption: BodyOption): Unit =
		builder.indentMap(map, name.orSimpleNameOf(this::class), bodyOption)
}
