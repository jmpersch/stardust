/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.lists

import cc.persch.stardustx.collections.iterators.UnmodifiableListIteratorView
import cc.persch.stardustx.collections.iterators.UnmodifiableListIteratorWrapper
import cc.persch.stardustx.collections.UnmodifiableCollectionWrapper

/**
 * Defines a wrapper to provide an immutable view of a [List].
 *
 * @since 4.2
 *
 * @constructor Creates a new instance using the given [collection].
 */
public open class UnmodifiableListWrapper<out E>(
	override val collection: List<E>
): UnmodifiableCollectionWrapper<E>(collection), UnmodifiableListView<E> {
	
	override operator fun get(index: Int): E = collection.get(index)
	
	override fun indexOf(element: @UnsafeVariance E): Int = collection.indexOf(element)
	
	override fun lastIndexOf(element: @UnsafeVariance E): Int = collection.lastIndexOf(element)
	
	override fun listIterator(): UnmodifiableListIteratorView<E> = UnmodifiableListIteratorWrapper(collection.listIterator())
	
	override fun listIterator(index: Int): UnmodifiableListIteratorView<E> =
		UnmodifiableListIteratorWrapper(collection.listIterator(index))
	
	override fun subList(fromIndex: Int, toIndex: Int): UnmodifiableListView<E> =
		UnmodifiableListWrapper(collection.subList(fromIndex, toIndex))
}
