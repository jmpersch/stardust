/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.sets

import cc.persch.stardust.annotations.Pure

/**
 * Defines the base class of a [Set].
 *
 * @since 5.0
 */
public abstract class AbstractSet<out E>: Set<E>
{
	protected abstract val set: Set<E>
	
	override val size: Int
		get() = set.size
	
	@Pure
	override fun contains(element: @UnsafeVariance E): Boolean = set.contains(element)
	
	@Pure
	override fun containsAll(elements: Collection<@UnsafeVariance E>): Boolean = set.containsAll(elements)
	
	@Pure
	override fun isEmpty(): Boolean = set.isEmpty()
	
	@Pure
	override fun equals(other: Any?): Boolean = set == other
	
	@Pure
	override fun hashCode(): Int = set.hashCode()
	
	@Pure
	override fun toString(): String = set.toString()
}
