/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.tables

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardustx.collections.__accumulate
import cc.persch.stardustx.collections.maps.__putIfAbsent
import cc.persch.stardustx.collections.maps.putAll
import cc.persch.stardustx.collections.maps.putAllAbsent

/**
 * Defines a mutable [Table].
 *
 * @param R The type of the row keys
 * @param C The type of the column keys
 * @param V The type of the values
 *
 * @since 4.6
 */
public interface MutableTable<R, C, V>: Table<R, C, V>, MutableIterable<Map.Entry<R, Map<C, V>>> {
	
	@Pure
	override fun get(rowKey: R): MutableMap<C, V>?
	
	/**
	 * Gets the columns of the row of the given [rowKey]. If the row does not exist, an empty row is put into this
	 * collection and returned.
	 */
	@ShouldUse
	public fun getOrPutEmpty(rowKey: R): MutableMap<C, V>
	
	@ShouldUse
	public fun getOrPutEmpty(rowKey: R, initialColumnCapacity: Int): MutableMap<C, V>
	
	@ShouldUse
	public fun getOrPutEmpty(rowKey: R, initialColumnCapacity: Int, columnLoadFactor: Float): MutableMap<C, V>
	
	/**
	 * Associates an empty row with the given [rowKey], if the key is absent.
	 */
	public fun putEmptyIfAbsent(rowKey: R): Boolean
	
	public fun putEmptyIfAbsent(rowKey: R, initialColumnCapacity: Int): Boolean
	
	public fun putEmptyIfAbsent(rowKey: R, initialColumnCapacity: Int, columnLoadFactor: Float): Boolean
	
	/**
	 * Puts the given [value] into the cell defined by the given [rowKey] and [columnKey] combination.
	 *
	 * @return The previous value of the cell; or `null`, if there was no value associated to the cell.
	 */
	public fun put(rowKey: R, columnKey: C, value: V): V? = getOrPutEmpty(rowKey).put(columnKey, value)
	
	/**
	 * Puts the given [value] into the cell defined by the given [rowKey] and [columnKey] combination, if it is absent.
	 *
	 * @return The previous value of the cell; or `null`, if there was no value associated to the cell.
	 */
	public fun putIfAbsent(rowKey: R, columnKey: C, value: V): V? =
		getOrPutEmpty(rowKey).__putIfAbsent(columnKey, value)
	
	/**
	 * Puts the given columns into the row of the given [rowKey].
	 */
	public fun putAllEntries(rowKey: R, vararg columns: Map.Entry<C, V>): Unit = putAllEntries(rowKey, columns.asList())
	
	/**
	 * Puts the given columns into the row of the given [rowKey].
	 */
	public fun putAllEntries(rowKey: R, columns: Iterable<Map.Entry<C, V>>) {
		
		getOrPutEmpty(rowKey).putAll(columns)
	}
	
	/**
	 * Puts the absent cells of the given columns into the row of the given [rowKey].
	 *
	 * @return `true` if at least one value was added.
	 */
	public fun putAllAbsentEntries(rowKey: R, vararg columns: Map.Entry<C, V>): Boolean =
		putAllAbsentEntries(rowKey, columns.asList())
	
	/**
	 * Puts the absent cells of the given columns into the row of the given [rowKey].
	 *
	 * @return `true` if at least one value was added.
	 */
	public fun putAllAbsentEntries(rowKey: R, columns: Iterable<Map.Entry<C, V>>): Boolean =
		getOrPutEmpty(rowKey).putAllAbsent(columns)
	
	/**
	 * Puts the elements of [other] into this table.
	 */
	public fun putAll(other: Table<R, C, V>) {
		
		if(this === other)
			return
		
		other.forEach { (rowKey, columns) -> getOrPutEmpty(rowKey).putAll(columns) }
	}
	
	/**
	 * Puts the given columns into the row of the given [rowKey].
	 */
	public fun putAll(rowKey: R, vararg columns: Map.Entry<C, V>): Unit = putAll(rowKey, columns.asList())
	
	/**
	 * Puts the given columns into the row of the given [rowKey].
	 */
	public fun putAll(rowKey: R, columns: Iterable<Map.Entry<C, V>>) {
		
		getOrPutEmpty(rowKey).putAll(columns)
	}
	
	/**
	 * Puts the absent cells of the given columns into the row of the given [rowKey].
	 *
	 * @return `true` if at least one value was added.
	 */
	public fun putAllAbsent(rowKey: R, vararg columns: Map.Entry<C, V>): Boolean =
		putAllAbsent(rowKey, columns.asList())
	
	/**
	 * Puts the absent cells of the given columns into the row of the given [rowKey].
	 *
	 * @return `true` if at least one value was added.
	 */
	public fun putAllAbsent(rowKey: R, columns: Iterable<Map.Entry<C, V>>): Boolean =
		getOrPutEmpty(rowKey).putAllAbsent(columns)
	
	/**
	 * Puts the given [triples of row keys, column keys, and values][triples] into this collection.
	 */
	public fun putAll(vararg triples: Triple<R, C, V>): Unit = putAll(triples.asList())
	
	/**
	 * Puts the absent cells of the given [triples of row keys, column keys, and values][triples] into this collection.
	 *
	 * @return `true` if at least one value was added.
	 */
	public fun putAllAbsent(vararg triples: Triple<R, C, V>): Boolean = putAllAbsent(triples.asList())
	
	/**
	 * Puts the given [triples of row keys, column keys, and values][triples] into this collection.
	 */
	public fun putAll(triples: Iterable<Triple<R, C, V>>): Unit =
		triples.forEach { (rowKey, columnKey, value) -> put(rowKey, columnKey, value) }
	
	/**
	 * Puts the absent cells of the given [triples of row keys, column keys, and values][triples] into this collection.
	 *
	 * @return `true` if at least one value was added.
	 */
	public fun putAllAbsent(triples: Iterable<Triple<R, C, V>>): Boolean =
		triples.__accumulate { (rowKey, columnKey, value) -> putIfAbsent(rowKey, columnKey, value) == null }
	
	/**
	 * Clears the row of the given [rowKey]. The empty row remains in this collection.
	 */
	public fun removeRow(rowKey: R): Boolean = removeRow(rowKey, false)
	
	/**
	 * If [prune] is set to `true`, the row of the given [rowKey] is removed. If [prune] is set to `false`, the
	 * row of the given [rowKey] is cleared.
	 */
	public fun removeRow(rowKey: R, prune: Boolean): Boolean
	
	/**
	 * Removes the column defined by the given [rowKey] and [columnKey] combination. The row may remain empty.
	 *
	 * If [prune] is set to `true`, a remaining empty row is removed.
	 *
	 * @return The removed value; or `null`, if there was no value associated to the cell.
	 */
	public fun removeCell(rowKey: R, columnKey: C, prune: Boolean = false): V?
	
	/**
	 * Removes value of the cell defined by the given [rowKey] and [columnKey] combination. The row may remain empty.
	 *
	 * If [prune] is set to `true`, a remaining empty row is removed.
	 */
	public fun removeCell(rowKey: R, columnKey: C, value: V, prune: Boolean = false): Boolean
	
	/**
	 * Removes all empty rows of this collection.
	 */
	public fun pruneAll()
	
	/**
	 * Removes all elements of this collection.
	 */
	public fun clear()
	
	@Pure
	override fun iterator(): MutableIterator<MutableMap.MutableEntry<R, MutableMap<C, V>>>
}
