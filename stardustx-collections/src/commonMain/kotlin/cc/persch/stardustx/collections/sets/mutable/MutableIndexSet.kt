/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.sets

/**
 * Defines an interface for mutable [IndexSet].
 *
 * @param E The type of the values of this collection should be immutable!
 *
 * @since 4.0
 */
public interface MutableIndexSet<E>: IndexSet<E>, MutableSet<E> {
	
	/**
	 * Tries to set a value at the specified index.
	 *
	 * @param index The index to place the specified value.
	 * @param element The element to place at the specified index.
	 * @return `true`, if the *value* was set; otherwise, `false`.
	 * @throws IllegalArgumentException *index* is out of range.
	 */
	public fun setAt(index: Int, element: E): Boolean
	
	/**
	 * Tries to add any value of the specified collection to this set.
	 *
	 * @param elements The elements to add.
	 * @return `true`, if at least one *value* was added; otherwise, `false`.
	 */
	override fun addAll(elements: Collection<E>): Boolean = addAll(elements as Iterable<E>)
	
	/**
	 * Tries to add any value of the specified collection to this set.
	 *
	 * @param elements The elements to add.
	 * @return `true`, if at least one *value* was added; otherwise, `false`.
	 */
	public fun addAll(elements: Iterable<E>): Boolean
	
	/**
	 * Tries to add any value of the specified collection to this set.
	 *
	 * @param elements The elements to add.
	 * @return `true`, if at least one *value* was added; otherwise, `false`.
	 */
	public fun addAll(elements: Sequence<E>): Boolean = this.addAll(elements.asIterable())
	
	/**
	 * Tries to insert a value at the specified index.
	 *
	 * @param index The index to insert the specified value.
	 * @param element The element to insert at the specified index.
	 * @return `true`, if the *value* was inserted; otherwise, if the key already exists, `false`.
	 */
	public fun insertAt(index: Int, element: E): Boolean
	
	/**
	 * Tries to insert any value of the specified collection to this set at the specified index.
	 *
	 * @param index The index to insert the specified values
	 * @param elements The elements to add.
	 * @return `true`, if at least one *value* was added; otherwise, `false`.
	 */
	public fun insertAllAt(index: Int, elements: Iterable<E>): Boolean
	
	/**
	 * Tries to insert any value of the specified collection to this set at the specified index.
	 *
	 * @param index The index to insert the specified values
	 * @param elements The elements to add.
	 * @return `true`, if at least one *value* was added; otherwise, `false`.
	 */
	public fun insertAllAt(index: Int, elements: Sequence<E>): Boolean = this.insertAllAt(index, elements.asIterable())
	
	/**
	 * Removes an element at the specified index.
	 *
	 * @param index The index of the element to remove.
	 */
	public fun removeAt(index: Int)
	
	/**
	 * Sorts this collection according to the order, induced by the specified comparator.
	 *
	 * @param comparator A comparator.
	 */
	public fun sort(comparator: Comparator<in E>)
	
	/**
	 * Removes all elements from this set.
	 */
	override fun clear()
	
	/**
	 * Reverses the elements in this set.
	 *
	 * @since 5.0
	 */
	public fun reverse()
}
