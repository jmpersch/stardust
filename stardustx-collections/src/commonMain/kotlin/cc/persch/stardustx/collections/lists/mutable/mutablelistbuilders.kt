/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.lists

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.exert
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a [MutableList] using the given [builderAction].
 *
 * @since 4.0
 * @see mutableListOf
 */
@Pure
@TypeSafeBuilder
public inline fun <T> buildMutableList(
	@BuilderInference builderAction: MutableList<T>.() -> Unit
) : MutableList<T> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return mutableListOf<T>().exert(builderAction)
}

/**
 * Creates a [MutableList] with the given [initialCapacity] and [builderAction].
 *
 * @since 4.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see mutableListOf
 */
@Pure
@TypeSafeBuilder
public inline fun <T> buildMutableList(
	initialCapacity: Int,
	@BuilderInference builderAction: MutableList<T>.() -> Unit
) : MutableList<T> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return ArrayList<T>(initialCapacity).exert(builderAction)
}
