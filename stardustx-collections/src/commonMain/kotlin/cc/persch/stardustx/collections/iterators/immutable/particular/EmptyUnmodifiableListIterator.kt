/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.iterators.particular

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardustx.collections.iterators.UnmodifiableListIteratorView

/**
 * Defines an empty [ListIterator].
 *
 * @since 4.0
 *
 * @constructor Creates a new instance.
 */
@ThreadSafe
public object EmptyUnmodifiableListIterator: AbstractEmptyUnmodifiableIterator(), UnmodifiableListIteratorView<Nothing> {
	
	@Pure
	override fun hasPrevious(): Boolean = false
	
	@Pure
	override fun nextIndex(): Int = -1
	
	@Pure
	override fun previousIndex(): Int = -1
	
	@Pure
	override fun previous(): Nothing = throw NoSuchElementException()
}
