/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.maps.UnmodifiableMapView.UnmodifiableEntryView

/**
 * Returns an immutable view.
 *
 * @since 4.0
 */
@Pure
public fun <K, V> Map<K, V>.asUnmodifiableView(): UnmodifiableMapView<K, V> = when(this) {
		
	is UnmodifiableMapView<K, V> -> this
	
	else -> UnmodifiableMapWrapper(this)
}

/**
 * Returns an immutable view.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> Map.Entry<K, V>.asUnmodifiableView(): UnmodifiableEntryView<K, V> = when(this) {
	
	is UnmodifiableEntryView -> this

	else -> UnmodifiableMapEntryWrapper(this)
}

/**
 * Creates an iterator that returns all [entries][UnmodifiableMapView.entries] from this collection.
 * 
 * This is a convenient function for `entries.iterator()`.
 *
 * @since 5.0
 */
public fun <K, V> UnmodifiableMapView<K, V>.iterator(): UnmodifiableIteratorView<UnmodifiableEntryView<K, V>> = entries.iterator()

/**
 * Creates a sequence that returns all [entries][UnmodifiableMapView.entries] from this collection.
 *
 * This is a convenient function for `entries.iterator().asSequence()`.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> UnmodifiableMapView<K, V>.asSequence(): Sequence<UnmodifiableEntryView<K, V>> = entries.iterator().asSequence()

/**
 * Returns this; or an empty [UnmodifiableMapView], if this is `null`.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> UnmodifiableMapView<K, V>?.orEmpty(): UnmodifiableMapView<K, V> = this ?: emptyUnmodifiableMap()

/**
 * Creates a [UnmodifiableMapView.UnmodifiableEntryView] with this as the entry's key and the given [value] as the entry's value.
 *
 * @since 4.0
 * @see ImmutableMapEntry
 * @see mapTo
 */
@Pure
public infix fun <K, V> K.mapImmutableTo(value: V): UnmodifiableEntryView<K, V> = ImmutableMapEntry(this, value)
