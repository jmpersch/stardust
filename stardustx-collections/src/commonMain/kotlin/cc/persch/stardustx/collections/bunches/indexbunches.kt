/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure

/**
 * Creates an empty [IndexBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see orEmpty
 * @see indexBunchOf
 */
@Pure
public fun <K, V> emptyIndexBunchOf(keyExtractor: (V) -> K): IndexBunch<K, V> =
	EmptyUnmodifiableIndexBunch(keyExtractor)

///**
// * Creates an empty [UnmodifiableIndexBunchView] implementation using the given [keyExtractor].
// *
// * @since 5.0
// * @see orEmpty
// * @see indexBunchOf
// */
//@Pure
//internal fun <K, V> emptyUnmodifiableIndexBunchOf(keyExtractor: (V) -> K): UnmodifiableIndexBunchView<K, V> =
//	EmptyUnmodifiableIndexBunch(keyExtractor)

/**
 * Creates an empty [IndexBunch] using the given [keyExtractor].
 *
 * Better use [emptyIndexBunchOf].
 *
 * @since 5.0
 * @see emptyIndexBunchOf
 * @see orEmpty
 */
@Pure
public fun <K, V> indexBunchOf(
	keyExtractor: (V) -> K,
) : IndexBunch<K, V> = emptyIndexBunchOf(keyExtractor)

/**
 * Creates a singleton [IndexBunch] using the given [keyExtractor] and [item].
 *
 * @since 5.0
 * @see buildIndexBunch
 */
@Pure
public fun <K, V> indexBunchOf(
	keyExtractor: (V) -> K,
	item: V
) : IndexBunch<K, V> = mutableIndexBunchOf(keyExtractor, item)

/**
 * Creates an [IndexBunch] using the given [keyExtractor] and [items].
 *
 * @since 5.0
 * @see buildIndexBunch
 */
@Pure
public fun <K, V> indexBunchOf(
	keyExtractor: (V) -> K,
	vararg items: V
) : IndexBunch<K, V> = mutableIndexBunchOf(keyExtractor, *items)


/**
 * Returns a [IndexBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see indexBunchOf
 */
@Pure
public fun <K, V> Sequence<V>.toIndexBunch(
	keyExtractor: (V) -> K
) : IndexBunch<K, V> = toMutableIndexBunch(keyExtractor)

/**
 * Returns a [IndexBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see indexBunchOf
 */
@Pure
public fun <K, V> Iterable<V>.toIndexBunch(
	keyExtractor: (V) -> K
) : IndexBunch<K, V> = toMutableIndexBunch(keyExtractor)

/**
 * Returns this; or creates an empty [IndexBunch] using the given [keyExtractor],
 * if this is `null`.
 *
 * @since 5.0
 */
@Pure
public fun <K, V> IndexBunch<K, V>?.orEmpty(keyExtractor: (V) -> K): IndexBunch<K, V> =
	this ?: emptyIndexBunchOf(keyExtractor)
