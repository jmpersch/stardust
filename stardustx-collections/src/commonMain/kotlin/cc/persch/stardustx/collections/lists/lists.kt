/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.lists

import cc.persch.stardust.annotations.Pure

/**
 * Returns a view of the portion of this list starting at the given [range][from].
 * The returned list is backed by this list, so non-structural changes in the returned list are reflected in this list,
 * and vice-versa.
 *
 * Structural changes in the base list make the behavior of the view undefined.
 */
@Pure
public fun <E> List<E>.subList(from: Int): List<E> = subList(from, size)

/**
 * Returns a view of the portion of this list of the given [range].
 * The returned list is backed by this list, so non-structural changes in the returned list are reflected in this list,
 * and vice-versa.
 *
 * Structural changes in the base list make the behavior of the view undefined.
 */
@Pure
public fun <E> List<E>.subList(range: IntRange): List<E> = subList(range.first, range.last + 1)

/**
 * Creates a mutable copy.
 *
 * @since 4.0
 */
@Pure
public fun <E> List<E>.mutableCopy(): MutableList<E> = toMutableList()
