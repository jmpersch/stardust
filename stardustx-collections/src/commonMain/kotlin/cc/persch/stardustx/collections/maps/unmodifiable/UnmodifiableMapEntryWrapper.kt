/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

/**
 * Defines an immutable wrapper for a map entry.
 *
 * Use [asUnmodifiableView] to avoid nesting of multiple immutable instances.
 *
 * @since 5.0
 * @see ImmutableMapEntry
 * @see mapTo
 * @see asUnmodifiableView
 *
 * @constructor Creates a new instance containing the given [entry].
 */
public open class UnmodifiableMapEntryWrapper<K, out V>(
	protected val entry: Map.Entry<K, V>
) : AbstractMapEntry<K, V>(), UnmodifiableMapView.UnmodifiableEntryView<K, V> {
	
	override val key: K get() = entry.key
	
	override val value: V get() = entry.value
	
	override fun copy(key: K, value: @UnsafeVariance V): Map.Entry<K, V> = ImmutableMapEntry(key, value)
	
	override fun equals(other: Any?): Boolean = entry == other
	
	override fun hashCode(): Int = entry.hashCode()
	
	override fun toString(): String = entry.toString()
}
