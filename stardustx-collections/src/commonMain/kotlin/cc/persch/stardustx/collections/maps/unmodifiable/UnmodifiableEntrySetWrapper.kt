/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.maps

import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.maps.iterators.ImmutableEntryIteratorWrapper
import cc.persch.stardustx.collections.sets.UnmodifiableSetView

/**
 * Defines an immutable set iterator wrapper.
 *
 * @since 5.0
 *
 * @constructor Creates a new instance using the given [set].
 */
public open class UnmodifiableEntrySetWrapper<K, V>(
	protected val set: Set<Map.Entry<K, V>>
) : UnmodifiableSetView<UnmodifiableMapView.UnmodifiableEntryView<K, V>> {
	
	override val size: Int
		get() = set.size
	
	override fun contains(element: UnmodifiableMapView.UnmodifiableEntryView<K, V>): Boolean =
		set.contains(element)
	
	override fun containsAll(elements: Collection<UnmodifiableMapView.UnmodifiableEntryView<K, V>>): Boolean =
		set.containsAll(elements)
	
	override fun isEmpty(): Boolean = set.isEmpty()
	
	override fun iterator(): UnmodifiableIteratorView<UnmodifiableMapView.UnmodifiableEntryView<K, V>> =
		ImmutableEntryIteratorWrapper(set.iterator())
	
	override fun equals(other: Any?): Boolean = set == other
	
	override fun hashCode(): Int = set.hashCode()
	
	override fun toString(): String = set.toString()
}
