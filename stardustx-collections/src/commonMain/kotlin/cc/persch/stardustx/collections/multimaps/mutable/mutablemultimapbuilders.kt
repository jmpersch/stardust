/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.exert

/**
 * Creates a [MutableMultiMap] using the given [builderAction].
 *
 * @since 4.6
 * @see mutableMultiMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableMultiMap(
	@BuilderInference builderAction: MutableMultiMap<K, V>.() -> Unit
) : MutableMultiMap<K, V> = mutableMultiMapOf<K, V>().exert(builderAction)


/**
 * Creates a [MutableMultiMap] with the given [initialCapacity] using the given [builderAction].
 *
 * @since 5.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see mutableMultiMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableMultiMap(
	initialCapacity: Int, 
	@BuilderInference builderAction: MutableMultiMap<K, V>.() -> Unit
) : MutableMultiMap<K, V> = MutableLinkedHashMultiMap<K, V>(initialCapacity).exert(builderAction)


/**
 * Creates a [MutableHashMultiMap] using the given [builderAction].
 *
 * @since 6.0
 * @see mutableHashMultiMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableHashMultiMap(
	@BuilderInference builderAction: MutableMultiMap<K, V>.() -> Unit
) : MutableMultiMap<K, V> = mutableHashMultiMapOf<K, V>().exert(builderAction)


/**
 * Creates a [MutableHashMultiMap] with the given [initialCapacity] using the given [builderAction].
 *
 * @since 6.0
 * @throws IllegalArgumentException [initialCapacity] is less than zero.
 * @see mutableMultiMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildMutableHashMultiMap(
	initialCapacity: Int,
	@BuilderInference builderAction: MutableMultiMap<K, V>.() -> Unit
) : MutableMultiMap<K, V> = MutableHashMultiMap<K, V>(initialCapacity).exert(builderAction)
