/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.collections.tables

import cc.persch.stardustx.collections.DEFAULT_CAPACITY
import cc.persch.stardustx.collections.maps.DEFAULT_LOAD_FACTOR
import cc.persch.stardustx.collections.maps.checkInitialCapacity
import cc.persch.stardustx.collections.maps.checkLoadFactor
import cc.persch.stardustx.collections.tables.mutable.AbstractMutableTable

/**
 * Defines a [MutableTable] backed by a [LinkedHashMap].
 *
 * @since 4.6
 *
 * @constructor Creates a new instance with the given [initialCapacity] and [loadFactor].
 * @throws IllegalArgumentException [initialCapacity] or [loadFactor] is less than zero;
 *   or [loadFactor] is not a number.
 */
public open class MutableHashTable<R, C, V>(
	initialCapacity: Int = DEFAULT_CAPACITY,
	loadFactor: Float = DEFAULT_LOAD_FACTOR,
	initialColumnCapacity: Int = DEFAULT_CAPACITY,
	columnLoadFactor: Float = DEFAULT_LOAD_FACTOR
) : AbstractMutableTable<R, C, V>(initialColumnCapacity, columnLoadFactor) {
	
	override val map: HashMap<R, MutableMap<C, V>> =
		HashMap(checkInitialCapacity(initialCapacity), checkLoadFactor(loadFactor))
	
	override fun checkMutability() { /* Nothing to check */ }
	
	override fun createHashMap(initialColumnCapacity: Int, columnLoadFactor: Float): MutableMap<C, V> =
		HashMap(initialColumnCapacity, columnLoadFactor)
}
