/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.maps

/**
 * Puts the given [value] into this map, if the given [key] is not already associated with a value
 * or the value is `null`.
 *
 * **Important:** Note that the operation is not guaranteed to be atomic if the map is being modified concurrently.
 *
 * @param key The key.
 * @param value The value.
 * @return The current value, if the key already exists; otherwise, `null`, if the given value was put into this map.
 *
 * @since 4.0
 */
internal actual fun <K, V> MutableMap<K, V>.__putIfAbsent(key: K, value: V) = putIfAbsent(key, value)

/**
 * Removes the entry for the given [key] only if it is currently mapped to the given [value] (checked with equality).
 *
 * **Important:** Note that the operation is not guaranteed to be atomic if the map is being modified concurrently.
 *
 * @since 4.0
 */
internal actual fun <K, V> MutableMap<K, V>.__remove(key: K, value: V) = remove(key, value)
