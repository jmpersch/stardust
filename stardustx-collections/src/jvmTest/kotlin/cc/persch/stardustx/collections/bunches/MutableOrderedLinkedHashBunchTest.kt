/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardust.makeStringOf
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.collections.ArrayList
import kotlin.test.*

/**
 * Defines tests for [MutableHashIndexBunch].
 *
 * @since 4.0
 */
class MutableOrderedLinkedHashBunchTest {
	
	private var bunch: MutableHashIndexBunch<Entry.Key, Entry> = MutableHashIndexBunch(Entry::key)
	
	private var list: ArrayList<Entry> = ArrayList()
	
	private val dummyEntry = Entry(Int.MAX_VALUE)
	
	open class Entry(open val key: Key, val value: Int): Comparable<Entry> {
		
		constructor(value: Int): this(Key(value), value)
		
		@Pure
		override fun compareTo(other: Entry): Int {
			
			val res = key.compareTo(other.key)
			
			if(res != 0)
				return res
			
			return value.compareTo(other.value)
		}
		
		override fun equals(other: Any?): Boolean {
			
			if(this === other) return true
			if(other !is Entry) return false
			
			if(key != other.key) return false
			if(value != other.value) return false
			
			return true
		}
		
		override fun hashCode(): Int {
			
			var result = key.hashCode()
			result = 31 * result + value
			return result
		}
		
		override fun toString() = makeStringOf(this, Entry::key, Entry::value)
		
		open class Key(val value: Int): Comparable<Key> {
			
			@Pure
			override fun compareTo(other: Key) = value.compareTo(other.value)
			
			@Pure
			override fun equals(other: Any?) = other is Key && value == other.value
			
			@Pure
			override fun hashCode() = value
			
			override fun toString() = value.toString()
		}
	}
	
	private class FaultyEntry(key: FaultyKey, value: Int): Entry(key, value) {
		
		class FaultyKey(value: Int): Key(value) {
			
			var faultinessEnabled = false
			
			override fun equals(other: Any?) = if(faultinessEnabled) false else super.equals(other)
			
			@Pure
			override fun hashCode() = if(faultinessEnabled) -value else super.hashCode()
		}
	}
	
	@Pure
	fun createList(startIndex: Int): ArrayList<Entry> {
		
		val list = ArrayList<Entry>()
		
		for(i in startIndex..<startIndex + 6)
			list.add(Entry(i))
		
		return list
	}
	
	private fun fillSet(set: MutableIndexBunch<Entry.Key, Entry>) {
		
		list.forEach { assertTrue(set.add(it)) }
	}
	
	private fun ensureEquality(set: MutableIndexBunch<Entry.Key, Entry>, list: List<Entry>) {
		
		assertEquals(list.size.toLong(), set.size.toLong())
		
		for(i in list.indices) {
			
			val e = list.get(i)
			
			assertSame(set.getAt(i), e)
			assertSame(set.get(e.key), e)
		}
	}
	
	@BeforeEach
	fun setUp() {
		
		list = createList(0)
		
		bunch = MutableHashIndexBunch { obj: Entry -> obj.key }
		
		fillSet(bunch)
		
		ensureEquality(bunch, list)
	}
	
	///////////////////////////////////////////////////////////////////////////
	
	fun getSize() {
		
		assertEquals(list.size, bunch.size)
	}
	
	@Test
	fun get() {
		
		val e = list.get(1)
		
		assertSame(bunch[e.key], e)
		
		assertNull(bunch[dummyEntry.key])
	}
	
	@Test
	fun tryGetByKeyOf() {
		
		val e = list.get(2)
		
		assertSame(bunch.getByKeyOf(e), e)
		
		assertNull(bunch.getByKeyOf(dummyEntry))
	}
	
	@Test
	fun getAt() {
		
		assertSame(bunch.getAt(1), list.get(1))
	}
	
	@Test
	fun setAt1_newEntry() {
		
		assertTrue(bunch.setAt(2, dummyEntry))
		assertSame(bunch.getAt(2), dummyEntry)
		
		list.set(2, dummyEntry)
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun setAt2_existingKeyButNewValue() {
		
		val e = Entry(list.get(1).key, 1000)
		
		assertFalse(bunch.setAt(2, e))
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun setAt3_existingEntry() {
		
		assertFalse(bunch.setAt(2, list.get(1)))
		
		ensureEquality(bunch, list)
	}
	
	/**
	 * This test tests also the forced removal hardening implementation of the bunch.
	 */
	@Test
	fun setAt4_faultyEntry() {
		
		val index = 2
		val faultyKey1 = FaultyEntry.FaultyKey(1000)
		val faultyEntry1 = FaultyEntry(faultyKey1, 1000)
		
		assertTrue(bunch.insertAt(index, faultyEntry1))
		
		val faultyKey2 = FaultyEntry.FaultyKey(1001)
		val faultyEntry2 = FaultyEntry(faultyKey2, 1001)
		
		faultyKey1.faultinessEnabled = true
		faultyKey2.faultinessEnabled = true
		
		assertTrue(bunch.setAt(index, faultyEntry2))
		
		faultyKey1.faultinessEnabled = false
		faultyKey2.faultinessEnabled = false
		
		assertEquals(faultyEntry2, bunch.getAt(index))
	}
	
	@Test
	fun testHashCode() {
		
		assertEquals(bunch.hashCode(), list.hashCode())
	}
	
	@Test
	fun testEquals1() {
		assertEquals(bunch, bunch.mutableCopy())
	}
	
	@Test
	fun testEquals2() {
		
		assertNotEquals(bunch, bunch.mutableCopy().apply { assertTrue(add(dummyEntry)) })
	}
	
	@Test
	fun testEquals3() {
		
		assertNotEquals<Any>(bunch, list)
	}
	
	@Test
	fun indexOf1() {
		
		val index = 3
		
		assertSame(bunch.indexOfKey(list[index].key), index)
	}
	
	@Test
	fun indexOf2() {
		
		assertEquals(-1, bunch.indexOfKey(dummyEntry.key))
	}
	
	@Test
	fun indexOfValue1() {
		
		val index = 3
		
		assertSame(bunch.indexOfValue(list[index]), index)
	}
	
	@Test
	fun indexOfValue2() {
		
		assertEquals(-1, bunch.indexOfValue(dummyEntry))
	}
	
	@Test
	fun containsKey1() {
		
		assertTrue(bunch.containsKey(list.get(1).key))
	}
	
	@Test
	fun containsKey2() {
		
		assertFalse(bunch.containsKey(dummyEntry.key))
	}
	
	@Test
	fun containsKeyOf1() {
		
		assertTrue(bunch.containsKeyOf(list.get(1)))
	}
	
	@Test
	fun containsKeyOf2() {
		
		assertFalse(bunch.containsKeyOf(dummyEntry))
	}
	
	@Test
	fun containsValue1() {
		
		assertTrue(bunch.containsValue(list.get(2)))
	}
	
	@Test
	fun containsValue2() {
		
		assertFalse(bunch.containsValue(dummyEntry))
	}
	
	@Test
	fun iteratorTest() {
		
		var i = 0
		
		for(e in bunch)
			assertSame(e, list.get(i++))
	}
	
	@Test
	fun testToString() {
		
		assertEquals(bunch.toString(), list.toString())
	}
	
	@Test
	fun tryAdd1() {
		
		assertTrue(bunch.add(dummyEntry))
		list.add(dummyEntry)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryAdd2() {
		
		assertFalse(bunch.add(list.get(0)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryAddAll1() {
		
		val moreEntries = createList(list.size)
		
		assertTrue(bunch.addAll(moreEntries))
		list.addAll(moreEntries)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryAddAll2() {
		
		assertFalse(bunch.addAll(list))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryAddAll3() {
		
		list.addAll(createList(list.size))
		
		assertTrue(bunch.addAll(list))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryAddAll4() {
		
		assertFalse(bunch.addAll(bunch))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryInsertAt1() {
		
		val index = 2
		
		assertTrue(bunch.insertAt(index, dummyEntry))
		list.add(index, dummyEntry)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryInsertAt2() {
		
		assertFalse(bunch.insertAt(2, list.get(0)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryInsertAllAt1() {
		
		val index = 2
		
		val moreEntries = createList(list.size)
		
		assertTrue(bunch.insertAllAt(index, moreEntries))
		list.addAll(index, moreEntries)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryInsertAllAt2() {
		
		val index = 2
		
		assertFalse(bunch.insertAllAt(index, list))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryInsertAllAt3() {
		
		val index = 2
		
		list.addAll(index, createList(list.size))
		
		assertTrue(bunch.insertAllAt(index, list))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryInsertAllAt4() {
		
		val index = 2
		
		assertFalse(bunch.insertAllAt(index, bunch))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryRemoveKey1() {
		
		assertTrue(bunch.removeKey(list.removeAt(2).key))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryRemoveKey2() {
		
		assertFalse(bunch.removeKey(dummyEntry.key))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryRemoveValue1() {
		
		assertTrue(bunch.removeValue(list.removeAt(2)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryRemoveValue2() {
		
		assertFalse(bunch.removeValue(dummyEntry))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryRemoveByKeyOf1() {
		
		assertTrue(bunch.removeByKeyOf(list.removeAt(2)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun tryRemoveByKeyOf2() {
		
		assertFalse(bunch.removeByKeyOf(dummyEntry))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAt() {
		
		val index = 2
		
		bunch.removeAt(index)
		list.removeAt(index)
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllKeys1() {
		
		val subList = list.subList(1, 3)
		
		val copy = (subList.toMutableList() exert { it.add(dummyEntry) }).map(Entry::key)
		
		assertTrue(bunch.removeAllKeys(copy))
		subList.clear()
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllKeys2() {
		
		assertFalse(bunch.removeAllKeys(listOf(dummyEntry).map(Entry::key)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllValues1() {
		
		val subList = list.subList(1, 3)
		
		val copy = subList.toMutableList() exert { it.add(dummyEntry) }
		
		assertTrue(bunch.removeAllValues(copy))
		subList.clear()
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllValues2() {
		
		assertFalse(bunch.removeAllValues(listOf(dummyEntry)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllValues3() {
		
		assertTrue(bunch.removeAllValues(bunch))
		list.clear()
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllByKeyOf1() {
		
		val subList = list.subList(1, 3)
		
		val copy = subList.toMutableList() exert { it.add(dummyEntry) }
		
		assertTrue(bunch.removeAllByKeyOf(copy))
		subList.clear()
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllByKeyOf2() {
		
		assertFalse(bunch.removeAllByKeyOf(listOf(dummyEntry)))
		ensureEquality(bunch, list)
	}
	
	@Test
	fun removeAllByKeyOf3() {
		
		assertTrue(bunch.removeAllByKeyOf(bunch))
		list.clear()
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllKeys1() {
		
		val subList = list.subList(1, 3)
		
		val copy = subList.toMutableList() exert { it.add(dummyEntry) }
		
		assertTrue(bunch.retainAllKeys(copy.map(Entry::key)))
		list.retainAll(copy)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllKeys2() {
		
		assertFalse(bunch.retainAllKeys(list.map(Entry::key)))
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllKeys3() {
		
		val list = listOf(dummyEntry.key)
		
		assertTrue(bunch.retainAllKeys(list))
		ensureEquality(bunch, listOf())
	}
	
	@Test
	fun retainAllValues1() {
		
		val subList = list.subList(1, 3)
		
		val copy = subList.toMutableList() exert { it.add(dummyEntry) }
		
		assertTrue(bunch.retainAllValues(copy))
		list.retainAll(copy)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllValues2() {
		
		assertFalse(bunch.retainAllValues(list))
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllValues3() {
		
		val list = listOf(dummyEntry)
		
		assertTrue(bunch.retainAllValues(list))
		ensureEquality(bunch, listOf())
	}
	
	@Test
	fun retainAllByKeyOf1() {
		
		val subList = list.subList(1, 3)
		
		val copy = subList.toMutableList() exert { it.add(dummyEntry) }
		
		assertTrue(bunch.retainAllByKeyOf(copy))
		list.retainAll(copy)
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllByKeyOf2() {
		
		assertFalse(bunch.retainAllByKeyOf(list))
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun retainAllByKeyOf3() {
		
		val list = listOf(dummyEntry)
		
		assertTrue(bunch.retainAllByKeyOf(list))
		ensureEquality(bunch, listOf())
	}
	
	@Test
	fun clear() {
		
		list.clear()
		bunch.clear()
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun sort() {
		
		bunch.sortWith(Comparator.reverseOrder())
		list.sortWith(Comparator.reverseOrder())
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun mutableCopy() {
		
		val copy = bunch.mutableCopy()
		
		ensureEquality(copy, list)
		assertEquals(copy, bunch)
	}
	
	@Test
	fun removeViaIterator() {
		
		val setIterator = bunch.iterator()
		val listIterator = list.iterator()
		
		for(i in 0..<2) {
			setIterator.next()
			listIterator.next()
		}
		
		setIterator.remove()
		listIterator.remove()
		
		ensureEquality(bunch, list)
	}
	
	@Test
	fun testFirst() {
		
		assertEquals(bunch.getAt(0), bunch.first())
	}
	
	@Test
	fun testLast() {
		
		assertEquals(bunch.getAt(bunch.size - 1), bunch.last())
	}
	
	@Test
	fun testReverse() {
		
		bunch.reverse()
		
		for(i in list.indices)
			assertEquals(list.get(i), bunch.getAt(list.size - i - 1))
		
		for(i in list.indices)
			assertEquals(i, bunch.indexOfKey(list.get(list.size - i - 1).key))
	}
}
