/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.base

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

interface ListTestBase<E: List<*>, C: List<*>>: CollectionTestBase<E, C> {
	
	@Test
	fun getTest() = createCollections { expected, actual ->
		
		for(i in expected.indices)
			assertEquals(expected[i], actual[i])
	}
	
	@Test
	fun indexOfTest() = createCollections { expected, actual ->
		
		expected.indices.forEach { 
			assertEquals(it, actual.indexOf(expected[it]))
		}
	}
	
	@Test
	fun lastIndexOfTest() = createCollections { expected, actual ->
		
		expected.indices.forEach { 
			assertEquals(it, actual.lastIndexOf(expected[it]))
		}
	}
	
	@Test
	fun listIteratorTest() = createCollections { expected, actual ->
		
		val eiter = expected.listIterator()
		val aiter = actual.listIterator()
		
		while(aiter.hasNext() && eiter.hasNext())
			assertEquals(aiter.next(), eiter.next())
		
		assert(!aiter.hasNext() && !eiter.hasNext())
	}
	
	@Test
	fun sublistTest() = createCollections { expected, actual ->
		
		if(actual.isEmpty()) {
			
			assertEquals(expected.subList(0, 0), actual.subList(0, 0))
			
			return@createCollections
		}
		
		if(actual.size == 1) {
			
			assertEquals(expected.subList(0, 1), actual.subList(0, 1))
			
			assertEquals(expected.subList(1, 1), actual.subList(1, 1))
			
			return@createCollections
		}
		
		val from = 1.coerceAtMost(actual.size)
		val to = (actual.size - 1).coerceAtLeast(from)
		
		assertEquals(expected.subList(from, to), actual.subList(from, to))
	}
}
