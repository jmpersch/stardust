/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.base

import cc.persch.stardust.annotations.Pure
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

interface IterableTestBase<E: Iterable<*>, C: Iterable<*>> {
	
	@Pure
	fun createExpected(): E
	
	@Pure
	fun createActual(): C
	
	@Test
	fun iteratorTest() = createCollections { expected, actual ->
		
		assertIterableEquals(expected, actual)
	}
	
	fun assertIterableEquals(expected: Iterable<*>, actual: Iterable<*>) {
		
		val expIter = expected.iterator()
		val currIter = actual.iterator()
		
		while(currIter.hasNext() && expIter.hasNext())
			assertEquals(currIter.next(), expIter.next())
		
		assert(!currIter.hasNext() && !expIter.hasNext())
	}
	
	@Pure
	fun createCollections(tester: (E, C) -> Unit) = tester(createExpected(), createActual())
}
