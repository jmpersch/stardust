/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime.formatters

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardustx.datetime.TimeSpan
import cc.persch.stardustx.datetime.UnitOfFractionOfSeconds
import java.time.Duration
import java.time.format.DateTimeParseException

/**
 * Defines a formatter for formatting and parsing a [TimeSpan].
 *
 * @see timeLike
 * @see iso8601
 * @since 5.1
 */
public sealed class TimeSpanFormatter {
	
	/**
	 * Converts the [timeSpan] to a string representation.
	 */
	public abstract fun format(timeSpan: TimeSpan): String
	
	/**
	 * Parses the [input] producing a [TimeSpan].
	 *
	 * @throws DateTimeParseException [input] is invalid.
	 */
	public abstract fun parse(input: String): Conclude<TimeSpan, DateTimeParseException>
	
	public companion object {
		
		/**
		 * Creates a formatter for a time span of time-like format defined as `-?D+.HH:mm:ss.(SSS|u{6}|n{9})`,
		 * e.g., `"15.04:03:02.102300405"` for a positive timestamp or `"-15.04:03:02.102300405"`
		 * for a negative timestamp.
		 *
		 * The given [unitOfFractionOfSeconds] sets the unit of the fraction of the second to either milliseconds,
		 * microseconds, or nanoseconds. Specify `null` (default) to automatically chose the unit.
		 */
		@Pure
		public fun timeLike(unitOfFractionOfSeconds: UnitOfFractionOfSeconds? = null): TimeSpanFormatter =
			TimeLikeTimeSpanFormatter(unitOfFractionOfSeconds)
		
		/**
		 * Creates a formatter for a time span of the ISO 8601 standard, e.g., `"PT123H56M29.789S"`.
		 * For more information about the supported input, see [Duration.parse].
		 *
		 * This formatter uses [TimeSpan.toString] for formatting and [TimeSpan.parse] for parsing of a time span.
		 *
		 * @see TimeSpan.toString
		 * @see TimeSpan.parse
		 */
		@Cached
		@Pure
		public fun iso8601(): TimeSpanFormatter = Iso8601TimeSpanFormatter
	}
}
