/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import java.time.*
import java.util.*
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Converts a this [ZonedDateTime] into a [DateTime].
 *
 * @since 5.1
 */
@Pure
public fun ZonedDateTime.toDateTime(): DateTime = DateTime(this)

/**
 * Converts a this [LocalDate] into a [Date].
 *
 * @since 5.1
 */
@Pure
public fun LocalDate.toDate(): Date = Date(this)

/**
 * Converts a this [Duration] into a [TimeSpan].
 *
 * @since 5.1
 */
@Pure
public fun Duration.toTimeSpan(): TimeSpan = TimeSpan(this)

/**
 * Creates a new instance of the class [DateTime] using this [GregorianCalendar].
 *
 * @since 5.1
 */
@Pure
public fun GregorianCalendar.toDateTime(): DateTime = DateTime(toZonedDateTime())
