/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.conclude.*
import cc.persch.stardust.math.plusExact
import cc.persch.stardust.math.timesExact
import cc.persch.stardust.pipe
import cc.persch.stardust.piping
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofSeconds
import cc.persch.stardustx.datetime.formatters.TimeSpanFormatter
import cc.persch.stardustx.datetime.UnitOfFractionOfSeconds.*
import cc.persch.stardustx.datetime.serializers.TimeSpanSerializer
import cc.persch.stardustx.datetime.timespans.units.*
import kotlinx.serialization.Serializable
import java.time.Duration
import java.time.format.DateTimeParseException
import java.time.temporal.TemporalAmount
import kotlin.math.absoluteValue

/**
 * Defines a time span with nanoseconds resolution.
 *
 * This class is immutable and thread-safe.
 *
 * ## Components
 *
 * The amount of time is modeled in terms of [seconds][inWholeSeconds] and [nanoseconds].
 *
 * The seconds are in the range of [Long.MIN_VALUE]..[Long.MAX_VALUE] and the nanoseconds are in the range of
 * `-999,999,999..999,999,999`.
 *
 * Both values combined are restricted to the range of [TimeSpan.MIN_VALUE]..[TimeSpan.MAX_VALUE].
 *
 * A negative time span of, e.g., -1.5 seconds is represented as -1 second and -5,000,000 nanoseconds.
 *
 * ## Decomposition and Recomposition
 *
 * Both components of the time span (seconds and nanoseconds) can also be retrieved via decomposition,
 * for example:
 *
 * ```
 * val (inWholeSeconds, nanosecond) = timeSpan
 * ```
 *
 * Use [ofSeconds] with nano adjustment to recompose the time span from the components, for example:
 *
 * ```
 * TimeSpan.ofSeconds(inWholeSeconds, nanosecond)
 * ```
 *
 * @see DateTime
 * @see Date
 * @see toTimeSpan
 * @see TimeSpanFormatter
 * @since 1.0
 *
 * @constructor Creates a new instance of the [TimeSpan] class using the given [duration].
 * @since 5.1
 */
@Serializable(with = TimeSpanSerializer::class)
@ThreadSafe
public class TimeSpan internal constructor(
	private val duration: Duration
) : Comparable<TimeSpan>, TemporalAmount by duration {
	
	/**
	 * Gets a value indicating whether this [TimeSpan] is zero.
	 *
	 * @since 5.1
	 */
	public val isZero: Boolean
		get() = duration piping { seconds or nano.toLong() } == 0L
	
	/**
	 * Gets a value indicating whether this [TimeSpan] is negative.
	 *
	 * @since 5.1
	 */
	public val isNegative: Boolean
		get() = duration.seconds < 0L
	
	// PARTIAL VALUES //////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Gets the days part of this timestamp. Same as [inWholeDays].
	 *
	 * @see inWholeDays
	 */
	public val days: Long
		get() = inWholeSeconds() / SECONDS_PER_DAY

	/**
	 * Gets the hours part of this timestamp (`-24..24`).
	 */
	public val hours: Int
		get() = ((inWholeSeconds() / SECONDS_PER_HOUR) % 24L).toInt()

	/**
	 * Gets the minutes part of this timestamp (`-59..59`).
	 */
	public val minutes: Int
		get() = ((inWholeSeconds() / SECONDS_PER_MINUTE) % 60L).toInt()

	/**
	 * Gets the seconds part of this timestamp (`-59..59`).
	 */
	public val seconds: Int
		get() = (inWholeSeconds() % 60L).toInt()

	/**
	 * Gets the milliseconds part of this timestamp (`-999..999`).
	 */
	public val milliseconds: Int
		get() = nanoseconds / NANOSECONDS_PER_MILLISECOND

	/**
	 * Gets the microseconds part of this timestamp (`-999,999..999,999`).
	 *
	 * @since 5.1
	 */
	public val microseconds: Int
		get() = nanoseconds / NANOSECONDS_PER_MICROSECOND
	
	/**
	 * Gets the nanoseconds part of this timestamp (`-999,999,999..999,999,999`).
	 *
	 * @since 5.1
	 * @see component2
	 */
	public val nanoseconds: Int
		get() = duration.nano pipe { nan -> when {
			
			nan == 0 -> 0
			isNegative -> -(NANOSECONDS_PER_SECOND - nan)
			else -> nan
		}}

	// IN WHOLE VALUES /////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Gets the time span in whole days. Same as [days].
	 *
	 * @since 5.1
	 * @see days
	 */
	public fun inWholeDays(): Long = inWholeSeconds() / SECONDS_PER_DAY
	
	/**
	 * Gets the time span in whole hours.
	 *
	 * @since 5.1
	 */
	public fun inWholeHours(): Long = inWholeSeconds() / SECONDS_PER_HOUR
	
	/**
	 * Gets the time span in whole minutes.
	 *
	 * @since 5.1
	 */
	public fun inWholeMinutes(): Long = inWholeSeconds() / SECONDS_PER_MINUTE
	
	/**
	 * Gets the time span in whole seconds.
	 *
	 * @since 5.1
	 * @see component1
	 */
	public fun inWholeSeconds(): Long =
		duration.seconds pipe { seconds -> (if(seconds < 0L && duration.nano > 0) seconds + 1L else seconds) }
	
	/**
	 * Gets the time span in whole milliseconds.
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	public fun inWholeMilliseconds(): ConcludeLong<ArithmeticException> = catchLong { duration.toMillis() }
	
	/**
	 * Gets the time span in whole microseconds.
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	public fun inWholeMicroseconds(): ConcludeLong<ArithmeticException> = encloseLong {
		
		(inWholeSeconds() timesExact MICROSECONDS_PER_SECOND.toLong()) + (nanoseconds / NANOSECONDS_PER_MICROSECOND)
	}
	
	/**
	 * Gets the time span in whole nanoseconds.
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	public fun getInWholeNanoseconds(): ConcludeLong<ArithmeticException> = catchLong { duration.toNanos() }
	
	// FUNCTIONS ///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Gets the [inWholeSeconds] as the first component of this [TimeSpan].
	 *
	 * @see inWholeSeconds
	 * @since 5.1
	 */
	public operator fun component1(): Long = inWholeSeconds()
	
	/**
	 * Gets the [nanoseconds] as the second component of this [TimeSpan].
	 *
	 * @see nanoseconds
	 * @since 5.1
	 */
	public operator fun component2(): Int = nanoseconds
	
	@Pure
	override fun compareTo(other: TimeSpan): Int = duration.compareTo(other.duration)
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(javaClass != other?.javaClass) return false
		
		other as TimeSpan
		
		return duration == other.duration
	}
	
	override fun hashCode(): Int = duration.hashCode()
	
	/**
	 * Gets the absolute value of this [TimeSpan].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see negate
	 * @see unaryMinus
	 * @since 5.1
	 */
	public fun absoluteValue(): Conclude<TimeSpan, ArithmeticException> = enclose(::absoluteValue)
	
	/**
	 * Gets the absolute value of this [TimeSpan].
	 *
	 * @see negate
	 * @see unaryMinus
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	public fun absoluteValue(): TimeSpan = if(isNegative) TimeSpan(duration.negated()) else this
	
	/**
	 * Adds [other] to this [TimeSpan].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 */
	@Pure
	public operator fun plus(other: TimeSpan): Conclude<TimeSpan, ArithmeticException> = enclose { plus(other) }
	
	/**
	 * Adds [other] to this [TimeSpan].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun plus(other: TimeSpan): TimeSpan =
		if(other.duration.isZero) this else TimeSpan(duration.plus(other.duration))
	
	/**
	 * Subtracts [other] from this [TimeSpan].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 */
	@Pure
	public operator fun minus(other: TimeSpan): Conclude<TimeSpan, ArithmeticException> = enclose { minus(other) }
	
	/**
	 * Subtracts [other] from this [TimeSpan].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun minus(other: TimeSpan): TimeSpan =
		if(other.duration.isZero) this else TimeSpan(duration.minus(other.duration))
	
	/**
	 * Multiplies this [TimeSpan] by given [value].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	@Pure
	public operator fun times(value: Int): Conclude<TimeSpan, ArithmeticException> = times(value.toLong())
	
	/**
	 * Multiplies this [TimeSpan] by given [value].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun times(value: Int): TimeSpan = times(value.toLong())
	
	/**
	 * Multiplies this [TimeSpan] by given [value].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	@Pure
	public operator fun times(value: Long): Conclude<TimeSpan, ArithmeticException> = enclose { times(value) }
	
	/**
	 * Multiplies this [TimeSpan] by given [value].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun times(value: Long): TimeSpan = when(value) {
	
		0L -> ZERO
		1L -> this
		else -> TimeSpan(duration.multipliedBy(value))
	}
	
	/**
	 * Divides this [TimeSpan] by given [value].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	@Pure
	public operator fun div(value: Int): Conclude<TimeSpan, ArithmeticException> = div(value.toLong())
	
	/**
	 * Divides this [TimeSpan] by given [value].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun div(value: Int): TimeSpan = div(value.toLong())
	
	/**
	 * Divides this [TimeSpan] by given [value].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	@Pure
	public operator fun div(value: Long): Conclude<TimeSpan, ArithmeticException> = enclose { div(value) }
	
	/**
	 * Divides this [TimeSpan] by given [value].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun div(value: Long): TimeSpan = if(value == 1L) this else TimeSpan(duration.dividedBy(value))
	
	/**
	 * Calculates the number of whole times the [other][other] [TimeSpan] occurs within this [TimeSpan].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	@Pure
	public operator fun div(other: TimeSpan): Conclude<Long, ArithmeticException> = enclose { div(other) }
	
	/**
	 * Calculates the number of whole times the [other][other] [TimeSpan] occurs within this [TimeSpan].
	 *
	 * @since 6.0
	 * @throws ArithmeticException A numeric overflow occurred.
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun div(other: TimeSpan): Long = duration.dividedBy(other.duration)

	/**
	 * Inverts the value of this [TimeSpan]. Same as [negate].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see negate
	 * @see absoluteValue
	 */
	@Pure
	public operator fun unaryMinus(): Conclude<TimeSpan, ArithmeticException> = enclose(::unaryMinus)
	
	/**
	 * Inverts the value of this [TimeSpan]. Same as [negate].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @see negate
	 * @see absoluteValue
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun unaryMinus(): TimeSpan = TimeSpan(duration.negated())

	/**
	 * Inverts the value of this [TimeSpan]. Same as [unaryMinus].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see unaryMinus
	 * @see absoluteValue
	 * @since 5.1
	 */
	@Pure
	public fun negate(): Conclude<TimeSpan, ArithmeticException> = enclose(::negate)
	
	/**
	 * Inverts the value of this [TimeSpan]. Same as [unaryMinus].
	 *
	 * @return A new [TimeSpan] with the inverted value of this [TimeSpan].
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @see unaryMinus
	 * @see absoluteValue
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public fun negate(): TimeSpan = TimeSpan(duration.negated())
	
	/**
	 * Converts this [TimeSpan] to a [Duration].
	 *
	 * @since 5.1
	 */
	@Cached
	@Pure
	public fun toDuration(): Duration = duration
	
	/**
	 * Gets the string representation of this timestamp of the ISO 8601 format, e.g., `"PT123H56M29.789S"`.
	 */
	@Pure
	override fun toString(): String = duration.toString()
	
	public companion object {
		
		internal const val NANOSECONDS_PER_MICROSECOND = 1_000
		internal const val NANOSECONDS_PER_MILLISECOND = 1_000_000
		internal const val NANOSECONDS_PER_SECOND = 1_000_000_000
		internal const val NANOSECONDS_PER_MINUTE = 60L * NANOSECONDS_PER_SECOND
		internal const val NANOSECONDS_PER_HOUR = 60L * NANOSECONDS_PER_MINUTE
		internal const val NANOSECONDS_PER_DAY = 24L * NANOSECONDS_PER_HOUR
		
		internal const val MICROSECONDS_PER_SECOND = 1_000_000
		
		internal const val MILLISECONDS_PER_SECOND = 1000
		internal const val MILLISECONDS_PER_MINUTE = 60 * MILLISECONDS_PER_SECOND
		internal const val MILLISECONDS_PER_HOUR = 60 * MILLISECONDS_PER_MINUTE
		internal const val MILLISECONDS_PER_DAY = 24 * MILLISECONDS_PER_HOUR
		
		internal const val SECONDS_PER_MINUTE = 60L
		internal const val SECONDS_PER_HOUR = 60 * SECONDS_PER_MINUTE
		internal const val SECONDS_PER_DAY = 24 * SECONDS_PER_HOUR
		
		/**
		 * The maximum nanoseconds.
		 *
		 * @since 5.1
		 */
		internal const val NANOSECONDS_MAX_VALUE: Int = 999_999_999
		
		/**
		 * The maximum microseconds.
		 *
		 * @since 5.1
		 */
		internal const val MICROSECONDS_MAX_VALUE: Int = 999_999
		
		/**
		 * The maximum milliseconds.
		 *
		 * @since 5.1
		 */
		internal const val MILLISECONDS_MAX_VALUE: Int = 999
		
		/**
		 * A zero time span.
		 */
		public val ZERO: TimeSpan = TimeSpan(Duration.ZERO)

		/**
		 * The minimum time span.
		 */
		public val MIN_VALUE: TimeSpan = TimeSpan(Duration.ofSeconds(Long.MIN_VALUE, 0))
		
		/**
		 * The maximum time span.
		 */
		public val MAX_VALUE: TimeSpan = TimeSpan(Duration.ofSeconds(Long.MAX_VALUE, NANOSECONDS_MAX_VALUE.toLong()))
		
		// OF //////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Creates a new [TimeSpan] of the given [value] of the given [unit].
		 *
		 * @return [Failure] if a numeric overflow occurred.
		 * @since 5.1
		 * @see ofNanoseconds
		 * @see ofMicroseconds
		 * @see ofMilliseconds
		 */
		@Pure
		public fun of(value: Int, unit: UnitOfFractionOfSeconds): TimeSpan = of(value.toLong(), unit)
		
		/**
		 * Creates a new [TimeSpan] of the given [value] of the given [unit].
		 *
		 * @return [Failure] if a numeric overflow occurred.
		 * @since 5.1
		 * @see ofMilliseconds
		 * @see ofMicroseconds
		 * @see ofNanoseconds
		 */
		@Pure
		public fun of(value: Long, unit: UnitOfFractionOfSeconds): TimeSpan = when(unit) {
			
			MILLISECONDS -> ofMilliseconds(value)
			MICROSECONDS -> ofMicroseconds(value)
			NANOSECONDS -> ofNanoseconds(value)
		}
		
		/**
		 * Creates a new instance of the [TimeSpan] class.
		 *
		 * All given values can be arbitrary, since they are altered during conversion.
		 *
		 * @param hours The hours.
		 * @param minutes The minutes.
		 * @param seconds The seconds.
		 * @param nanoAdjustment The adjustment of fraction of a second.
		 * @return [Failure] if a numeric overflow occurred.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			hours: Int,
			minutes: Int,
			seconds: Int,
			nanoAdjustment: Int = 0
		) : Conclude<TimeSpan, ArithmeticException> = of(
			0L,
			hours.toLong(),
			minutes.toLong(),
			seconds.toLong(),
			nanoAdjustment.toLong()
		)
		
		/**
		 * Creates a new instance of the [TimeSpan] class.
		 *
		 * All given values can be arbitrary, since they are altered during conversion.
		 *
		 * @param hours The hours.
		 * @param minutes The minutes.
		 * @param seconds The seconds.
		 * @param nanoAdjustment The adjustment of fraction of a second.
		 * @return [Failure] if a numeric overflow occurred.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			hours: Long,
			minutes: Long,
			seconds: Long,
			nanoAdjustment: Long = 0L
		) : Conclude<TimeSpan, ArithmeticException> = of(0L, hours, minutes, seconds, nanoAdjustment)
		
		/**
		 * Creates a new instance of the [TimeSpan] class.
		 *
		 * All given values can be arbitrary, since they are altered during conversion.
		 *
		 * @param days The days.
		 * @param hours The hours.
		 * @param minutes The minutes.
		 * @param seconds The seconds.
		 * @param nanoAdjustment The adjustment of fraction of a second.
		 * @return [Failure] if a numeric overflow occurred.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			days: Int,
			hours: Int,
			minutes: Int,
			seconds: Int,
			nanoAdjustment: Int = 0
		) : Conclude<TimeSpan, ArithmeticException> = of(
			days.toLong(),
			hours.toLong(),
			minutes.toLong(),
			seconds.toLong(),
			nanoAdjustment.toLong()
		)
		
		/**
		 * Creates a new instance of the [TimeSpan] class.
		 *
		 * All given values can be arbitrary, since they are altered during conversion.
		 *
		 * @param days The days.
		 * @param hours The hours.
		 * @param minutes The minutes.
		 * @param seconds The seconds.
		 * @param nanoAdjustment The adjustment of fraction of a second.
		 * @return [Failure] if a numeric overflow occurred.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			days: Long,
			hours: Long,
			minutes: Long,
			seconds: Long,
			nanoAdjustment: Long = 0L,
		) : Conclude<TimeSpan, ArithmeticException> =
			durationOf(days, hours, minutes, seconds, nanoAdjustment).map(::TimeSpan)
		
		@Pure
		private fun durationOf(
			days: Long, hours: Long, minutes: Long, seconds: Long, nanoseconds: Long
		) : Conclude<Duration, ArithmeticException> = enclose {
			
			val sDays = days timesExact SECONDS_PER_DAY
			val sHours = hours timesExact SECONDS_PER_HOUR
			val sMin = minutes timesExact SECONDS_PER_MINUTE
			
			val sum = sDays plusExact sHours plusExact sMin plusExact seconds
			
			Duration.ofSeconds(sum, nanoseconds)
		}
		
		// OF CERTAIN UNIT /////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Creates a new [TimeSpan] of the given number of [days].
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 * @see Int.days
		 */
		@Pure
		public fun ofDays(days: Int): Conclude<TimeSpan, ArithmeticException> = ofDays(days.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [days].
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 * @see Int.days
		 */
		@Pure
		public fun ofDays(days: Long): Conclude<TimeSpan, ArithmeticException> =
			`catch` { TimeSpan(Duration.ofDays(days)) }
		
		/**
		 * Creates a new [TimeSpan] of given number of [hours].
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 * @see Int.hours
		 */
		@Pure
		public fun ofHours(hours: Int): Conclude<TimeSpan, ArithmeticException> = ofHours(hours.toLong())
		
		/**
		 * Creates a new [TimeSpan] of given number of [hours].
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 * @see Int.hours
		 */
		@Pure
		public fun ofHours(hours: Long): Conclude<TimeSpan, ArithmeticException> =
			`catch` { TimeSpan(Duration.ofHours(hours)) }
		
		/**
		 * Creates a new [TimeSpan] of given number of [minutes].
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 * @see Int.minutes
		 */
		@Pure
		public fun ofMinutes(minutes: Int): Conclude<TimeSpan, ArithmeticException> = ofMinutes(minutes.toLong())
		
		/**
		 * Creates a new [TimeSpan] of given number of [minutes].
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 * @see Int.minutes
		 */
		@Pure
		public fun ofMinutes(minutes: Long): Conclude<TimeSpan, ArithmeticException> =
			`catch` { TimeSpan(Duration.ofMinutes(minutes)) }
		
		/**
		 * Creates a new [TimeSpan] of the given number of [seconds].
		 *
		 * @since 5.1
		 * @see Int.seconds
		 */
		@Pure
		public fun ofSeconds(seconds: Int): TimeSpan = ofSeconds(seconds.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [seconds].
		 *
		 * @since 5.1
		 * @see Int.seconds
		 */
		@Pure
		public fun ofSeconds(seconds: Long): TimeSpan = TimeSpan(Duration.ofSeconds(seconds))
		
		/**
		 * Creates a new [TimeSpan] of the given number of [seconds] and the given [nanoAdjustment].
		 *
		 * An arbitrary value can be given for [nanoAdjustment], since the value is altered during conversion.
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 */
		@Pure
		public fun ofSeconds(seconds: Int, nanoAdjustment: Int): Conclude<TimeSpan, ArithmeticException> =
			ofSeconds(seconds.toLong(), nanoAdjustment.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [seconds] and the given [nanoAdjustment].
		 *
		 * An arbitrary value can be given for [nanoAdjustment], since the value is altered during conversion.
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 */
		@Pure
		public fun ofSeconds(seconds: Int, nanoAdjustment: Long): Conclude<TimeSpan, ArithmeticException> =
			ofSeconds(seconds.toLong(), nanoAdjustment)
		
		/**
		 * Creates a new [TimeSpan] of the given number of [seconds] and the given [nanoAdjustment].
		 *
		 * An arbitrary value can be given for [nanoAdjustment], since the value is altered during conversion.
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 */
		@Pure
		public fun ofSeconds(seconds: Long, nanoAdjustment: Int): Conclude<TimeSpan, ArithmeticException> =
			ofSeconds(seconds, nanoAdjustment.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [seconds] and the given [nanoAdjustment].
		 *
		 * An arbitrary value can be given for [nanoAdjustment], since the value is altered during conversion.
		 *
		 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
		 * @since 5.1
		 */
		@Pure
		public fun ofSeconds(seconds: Long, nanoAdjustment: Long): Conclude<TimeSpan, ArithmeticException> =
			`catch` { TimeSpan(Duration.ofSeconds(seconds, nanoAdjustment)) }
		
		/**
		 * Creates a new [TimeSpan] of the given number of [milliseconds].
		 *
		 * @since 5.1
		 * @see Int.ms
		 */
		@Pure
		public fun ofMilliseconds(milliseconds: Int): TimeSpan = ofMilliseconds(milliseconds.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [milliseconds].
		 *
		 * @since 5.1
		 * @see Int.ms
		 */
		@Pure
		public fun ofMilliseconds(milliseconds: Long): TimeSpan = TimeSpan(Duration.ofMillis(milliseconds))
		
		/**
		 * Creates a new [TimeSpan] of the given number of [microseconds].
		 *
		 * @since 5.1
		 * @see Int.us
		 */
		@Pure
		public fun ofMicroseconds(microseconds: Int): TimeSpan = ofMicroseconds(microseconds.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [microseconds].
		 *
		 * @since 5.1
		 * @see Int.us
		 */
		@Pure
		public fun ofMicroseconds(microseconds: Long): TimeSpan {
			
			val seconds = microseconds / MICROSECONDS_PER_SECOND
			val micros = microseconds % MICROSECONDS_PER_SECOND
			
			// Cannot overflow
			return TimeSpan(Duration.ofSeconds(seconds, micros * NANOSECONDS_PER_MICROSECOND))
		}
		
		/**
		 * Creates a new [TimeSpan] of the given number of [nanoseconds].
		 *
		 * @since 5.1
		 * @see Int.ns
		 */
		@Pure
		public fun ofNanoseconds(nanoseconds: Int): TimeSpan = ofNanoseconds(nanoseconds.toLong())
		
		/**
		 * Creates a new [TimeSpan] of the given number of [nanoseconds].
		 *
		 * @since 5.1
		 * @see Int.ns
		 */
		@Pure
		public fun ofNanoseconds(nanoseconds: Long): TimeSpan = TimeSpan(Duration.ofNanos(nanoseconds))
		
		/**
		 * Parses the given [input] of the ISO 8601 format into a [TimeSpan].
		 * For more information about the supported input, see [Duration.parse].
		 *
		 * @return [Failure] if the input is invalid.
		 * @since 5.1
		 */
		@Pure
		public fun parse(input: String): Conclude<TimeSpan, DateTimeParseException> =
			`catch` { TimeSpan(Duration.parse(input)) }
	}
}
