/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime.formatters

import cc.persch.stardust.*
import cc.persch.stardust.conclude.*
import cc.persch.stardust.parsing.parseLong
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.rope
import cc.persch.stardustx.datetime.TimeSpan
import cc.persch.stardustx.datetime.TimeSpan.Companion.MICROSECONDS_MAX_VALUE
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_MICROSECOND
import cc.persch.stardustx.datetime.TimeSpan.Companion.MILLISECONDS_MAX_VALUE
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_MAX_VALUE
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_MILLISECOND
import cc.persch.stardustx.datetime.UnitOfFractionOfSeconds
import cc.persch.stardustx.datetime.UnitOfFractionOfSeconds.*
import java.time.format.DateTimeParseException
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract
import kotlin.math.absoluteValue

/**
 * @since 5.1
 */
internal class TimeLikeTimeSpanFormatter(
	val unitOfFractionOfSeconds: UnitOfFractionOfSeconds? = null
) : TimeSpanFormatter() {
	
	private inline fun <T: Any> __requireNotNull(input: String, value: T?, lazyMessage: RopeBuilder.() -> Unit) {
		
		contract { returns() implies (value != null); callsInPlace(lazyMessage, AT_MOST_ONCE) }
		
		if(value == null)
			__unfulfilled(input, rope(lazyMessage))
	}
	
	private inline fun __require(input: String, condition: Boolean, lazyMessage: RopeBuilder.() -> Unit) {
		
		contract { returns() implies condition; callsInPlace(lazyMessage, AT_MOST_ONCE) }
		
		if(!condition)
			__unfulfilled(input, rope(lazyMessage))
	}
	
	private fun __unfulfilled(input: String, message: String, cause: RuntimeException? = null): Nothing =
		throw DateTimeParseException(message, input, 0, cause)
	
	override fun format(timeSpan: TimeSpan): String = timeSpan piping { rope {
		
		+days.toString()
		+'.'
		align(hours.absoluteValue.toString(), 2, '0')
		+':'
		align(minutes.absoluteValue.toString(), 2, '0')
		+':'
		align(seconds.absoluteValue.toString(), 2, '0')
		+'.'
		
		when(unitOfFractionOfSeconds.orInferVia(timeSpan)) {
			
			MILLISECONDS -> align(milliseconds.absoluteValue.toString(), 3, '0')
			MICROSECONDS -> align(microseconds.absoluteValue.toString(), 6, '0')
			NANOSECONDS -> align(nanoseconds.absoluteValue.toString(), 9, '0')
		}
	}}
	
	private fun UnitOfFractionOfSeconds?.orInferVia(timeSpan: TimeSpan) =
		this ?: timeSpan.nanoseconds.pipe { nanos -> when {
			(nanos % NANOSECONDS_PER_MILLISECOND) == 0 -> MILLISECONDS
			(nanos % NANOSECONDS_PER_MICROSECOND) == 0 -> MICROSECONDS
			else -> NANOSECONDS
		}}
	
	override fun parse(input: String): Conclude<TimeSpan, DateTimeParseException> {
		
		val result = PATTERN.matchEntire(input)?.groupValues
		
		val days: Long?
		val hours: Long?
		val minutes: Long?
		val seconds: Long?
		val nanos: Long?
		
		try {
			
			// TODO: Refactor this into conclusion
			__require(input, result != null && result.size == 6) {
				
				-"Failed parsing timespan: Invalid timespan format."
				-"Expected format: -?D+.HH:mm:ss.(SSS|u{6}|n{9})"
			}
			
			days = result.get(1).parseLong().orNull()
			hours = result.get(2).parseLong().orNull()
			minutes = result.get(3).parseLong().orNull()
			seconds = result.get(4).parseLong().orNull()
			
			__requireNotNull(input, days) { -"Failed parsing timespan: Invalid number of days." }
			
			__require(input, hours != null && hours in 0..24) {
				
				-"Failed parsing timespan: Invalid number of hours."
				-"Expected range: 0..24"
			}
			
			__require(input, minutes != null && minutes in 0..59) {
				
				-"Failed parsing timespan: Invalid number of minutes."
				-"Expected range: 0..59"
			}
			
			__require(input, seconds != null && seconds in 0..59) {
				
				-"Failed parsing timespan: Invalid number of seconds.\nExpected range: 0..59"
			}
			
			val fractionOfSecondString = result.get(5)
			
			when(fractionOfSecondString.length) {
				
				3 -> {
					
					val millis = fractionOfSecondString.parseLong().orNull()
					
					__require(input, millis != null && millis in 0..MILLISECONDS_MAX_VALUE) {
						
						-"Failed parsing timespan: Invalid number of milliseconds."
						-"Expected range: 0..$MILLISECONDS_MAX_VALUE"
					}
					
					nanos = millis * NANOSECONDS_PER_MILLISECOND
				}
				
				6 -> {
					
					val micros = fractionOfSecondString.parseLong().orNull()
					
					__require(input, micros != null && micros in 0..MICROSECONDS_MAX_VALUE) {
						
						-"Failed parsing timespan: Invalid number of microseconds."
						-"Expected range: 0..$MICROSECONDS_MAX_VALUE"
					}
					
					nanos = micros * NANOSECONDS_PER_MICROSECOND
				}
				
				9 -> {
					
					nanos = fractionOfSecondString.parseLong().orNull()
					
					__require(
						input,
						nanos != null && nanos in 0..NANOSECONDS_MAX_VALUE
					) {
						
						-"Failed parsing timespan: Invalid number of nanoseconds."
						-"Expected range: 0..$NANOSECONDS_MAX_VALUE"
					}
				}
				
				else -> __unfulfilled(
					input,
					"Failed parsing timespan: Invalid timespan format.\nExpected format: -?D+.HH:mm:ss.(SSS|n{9})"
				)
			}
		}
		catch(e: DateTimeParseException) {
			
			return failureOf(e)
		}
		
		val factor = if(days < 0) -1L else 1L
		
		return TimeSpan.of(days, factor * hours, factor * minutes, factor * seconds, factor * nanos).mapError {
			
			__unfulfilled(input, "Failed parsing timespan: The given timestamp is out of range.", it)
		}
	}
	
	private companion object {
		
		private val PATTERN by lazy { Regex("""^([+-]?\d+)\.(\d{2}):(\d{2}):(\d{2})\.(\d{3,9})$""") }
	}
}
