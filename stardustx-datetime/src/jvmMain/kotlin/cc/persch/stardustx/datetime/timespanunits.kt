/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.datetime.timespans.units

import cc.persch.stardust.conclude.*
import cc.persch.stardustx.datetime.TimeSpan
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofDays
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofHours
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofMicroseconds
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofMilliseconds
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofMinutes
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofNanoseconds
import cc.persch.stardustx.datetime.TimeSpan.Companion.ofSeconds

// UNITS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [TimeSpan] of this number of days.
 *
 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofDays
 */
public val Int.days: Conclude<TimeSpan, ArithmeticException>
	get() = ofDays(toLong())

/**
 * Creates a new [TimeSpan] of this number of days.
 *
 * @throws ArithmeticException The input exceeds the capacity of the [TimeSpan].
 * @since 6.0
 * @see ofDays
 */
context(EnclosingContext<ArithmeticException>)
public val Int.days: TimeSpan
	get() = ofDays(toLong()) or { throw ArithmeticException("long overflow") }

/**
 * Creates a new [TimeSpan] of this number of days.
 *
 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofDays
 */
public val Long.days: Conclude<TimeSpan, ArithmeticException> get() = ofDays(this)

/**
 * Creates a new [TimeSpan] of this number of days.
 *
 * @throws ArithmeticException The input exceeds the capacity of the [TimeSpan].
 * @since 6.0
 * @see ofDays
 */
context(EnclosingContext<ArithmeticException>)
public val Long.days: TimeSpan
	get() = ofDays(this) or { throw ArithmeticException("long overflow") }


/**
 * Creates a new [TimeSpan] of this number of hours.
 *
 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofHours
 */
public val Int.hours: Conclude<TimeSpan, ArithmeticException>
	get() = ofHours(toLong())

/**
 * Creates a new [TimeSpan] of this number of hours.
 *
 * @throws ArithmeticException The input exceeds the capacity of the [TimeSpan].
 * @since 6.0
 * @see ofHours
 */
context(EnclosingContext<ArithmeticException>)
public val Int.hours: TimeSpan
	get() = ofHours(toLong()) or { throw ArithmeticException("long overflow") }

/**
 * Creates a new [TimeSpan] of this number of hours.
 *
 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofHours
 */
public val Long.hours: Conclude<TimeSpan, ArithmeticException>
	get() = ofHours(this)

/**
 * Creates a new [TimeSpan] of this number of hours.
 *
 * @throws ArithmeticException The input exceeds the capacity of the [TimeSpan].
 * @since 6.0
 * @see ofHours
 */
context(EnclosingContext<ArithmeticException>)
public val Long.hours: TimeSpan
	get() = ofHours(this) or { throw ArithmeticException("long overflow") }


/**
 * Creates a new [TimeSpan] of this number of minutes.
 *
 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofMinutes
 */
public val Int.minutes: Conclude<TimeSpan, ArithmeticException> get() =
	ofMinutes(toLong())

/**
 * Creates a new [TimeSpan] of this number of minutes.
 *
 * @throws ArithmeticException The input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofMinutes
 */
context(EnclosingContext<ArithmeticException>)
public val Int.minutes: TimeSpan
	get() = ofMinutes(toLong()) or { throw ArithmeticException("long overflow") }

/**
 * Creates a new [TimeSpan] of this number of minutes.
 *
 * @return [Failure] if the input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofMinutes
 */
public val Long.minutes: Conclude<TimeSpan, ArithmeticException>
	get() = ofMinutes(this)

/**
 * Creates a new [TimeSpan] of this number of minutes.
 *
 * @throws ArithmeticException The input exceeds the capacity of the [TimeSpan].
 * @since 5.1
 * @see ofMinutes
 */
context(EnclosingContext<ArithmeticException>)
public val Long.minutes: TimeSpan
	get() = ofMinutes(this) or { throw ArithmeticException("long overflow") }


/**
 * Creates a new [TimeSpan] of this number of seconds.
 * @since 5.1
 * @see ofSeconds
 */
public val Int.seconds: TimeSpan
	get() = ofSeconds(toLong())

/**
 * Creates a new [TimeSpan] of this number of seconds.
 *
 * @since 5.1
 * @see ofSeconds
 */
public val Long.seconds: TimeSpan
	get() = ofSeconds(this)


/**
 * Creates a new [TimeSpan] of this number of milliseconds.
 *
 * @since 5.1
 * @see ofMilliseconds
 */
public val Int.ms: TimeSpan
	get() = ofMilliseconds(toLong())

/**
 * Creates a new [TimeSpan] of this number of milliseconds.
 *
 * @since 5.1
 * @see ofMilliseconds
 */
public val Long.ms: TimeSpan
	get() = ofMilliseconds(this)


/**
 * Creates a new [TimeSpan] of this number of microseconds.
 *
 * @since 5.1
 * @see ofMicroseconds
 */
public val Int.us: TimeSpan
	get() = ofMicroseconds(toLong())

/**
 * Creates a new [TimeSpan] of this number of microseconds.
 * @since 5.1
 * @see ofMicroseconds
 */
public val Long.us: TimeSpan
	get() = ofMicroseconds(this)


/**
 * Creates a new [TimeSpan] of this number of nanoseconds.
 *
 * @since 5.1
 * @see ofNanoseconds
 */
public val Int.ns: TimeSpan
	get() = ofNanoseconds(toLong())

/**
 * Creates a new [TimeSpan] of this number of nanoseconds.
 *
 * @since 5.1
 * @see ofNanoseconds
 */
public val Long.ns: TimeSpan
	get() = ofNanoseconds(this)
