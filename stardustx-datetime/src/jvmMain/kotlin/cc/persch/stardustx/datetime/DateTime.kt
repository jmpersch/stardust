/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.conclude.*
import cc.persch.stardust.datetime.DateUtils
import cc.persch.stardust.datetime.ZoneIds
import cc.persch.stardust.getOrInitialize
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_MILLISECOND
import cc.persch.stardustx.datetime.serializers.DateTimeSerializer
import kotlinx.serialization.Serializable
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.*
import java.util.*

/**
 * Defines a date-time—a date with a time and a time zone.
 *
 * This class is immutable and thread-safe.
 * 
 * @since 1.0
 * @see Date
 * @see TimeSpan
 *
 * @constructor Creates a new instance of the [DateTime] class using the given [zonedDateTime].
 */
@Serializable(with = DateTimeSerializer::class)
@ThreadSafe
public class DateTime
internal constructor(
	private val zonedDateTime: ZonedDateTime = ZonedDateTime.now()
) : Comparable<DateTime>, TemporalAccessor by zonedDateTime {
	
	@Transient
	private var cachedDate: DateTime? = null
	
	/**
	 * Gets the date component that means the current date at 00:00:00.000000000 a.m.
	 *
	 * @return The value to get.
	 * @see toDate
	 */
	public val date: DateTime
		get() = this::cachedDate.getOrInitialize {
		
		of(year, monthValue, day, 0, 0, 0, 0, zonedDateTime.zone).orThrow()
	}

	/**
	 * Gets the time zone.
	 */
	public val zone: ZoneId
		get() = zonedDateTime.zone

	/**
	 * Gets the time zone offset in hours.
	 */
	public val zoneOffset: ZoneOffset
		get() = zonedDateTime.offset

	/**
	 * Gets the year.
	 */
	public val year: Int
		get() = zonedDateTime.year

	/**
	 * Gets the enum [Month] for the month.
	 */
	public val month: Month
		get() = zonedDateTime.month

	/**
	 * Gets the month as `int`.
	 */
	public val monthValue: Int
		get() = zonedDateTime.monthValue

	/**
	 * Gets the day.
	 */
	public val day: Int
		get() = zonedDateTime.dayOfMonth

	/**
	 * Gets the day of the week.
	 */
	public val dayOfWeek: DayOfWeek
		get() = zonedDateTime.dayOfWeek

	/**
	 * Gets the day of the year.
	 */
	public val dayOfYear: Int
		get() = zonedDateTime.dayOfYear
	
	/**
	 * Gets the week of the year as known from organizers. The year starts with the first week and may end with week 53.
	 */
	public val weekOfYear: Int
		get() = zonedDateTime.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR)

	/**
	 * Gets the raw week of the year. January 1 to 7 is week one, 8 to 14 is week two and so on, up to week 52.
	 */
	public val rawWeekOfYear: Int
		get() = zonedDateTime.get(ChronoField.ALIGNED_WEEK_OF_YEAR)

	/**
	 * Gets the week of the month.
	 */
	public val weekOfMonth: Int
		get() = zonedDateTime.get(ChronoField.ALIGNED_WEEK_OF_MONTH)

	/**
	 * Gets the hour.
	 */
	public val hour: Int
		get() = zonedDateTime.hour

	/**
	 * Gets the minute.
	 */
	public val minute: Int
		get() = zonedDateTime.minute

	/**
	 * Gets the second.
	 */
	public val second: Int
		get() = zonedDateTime.second

	/**
	 * Gets the millisecond.
	 */
	public val millisecond: Int
		get() = zonedDateTime.nano / NANOSECONDS_PER_MILLISECOND

	/**
	 * Gets the nanosecond.
	 *
	 * @since 5.1
	 */
	public val nanosecond: Int
		get() = zonedDateTime.nano

	/**
	 * Gets a value, indicating whether the year of this instance is a leap year.
	 *
	 * @return The value to get.
	 */
	public val isLeapYear: Boolean
		get() = DateUtils.isLeapYear(year)

	// endregion /Properties/

	// region Methods

	// region CEH Computation Members
	
	/**
	 * Compares this to the given [DateTime] *independent of the time zone*.
	 *
	 * **Example:**
	 *
	 * ```
	 * // 2013-12-07 12:41:35:563 CET
	 * val cet = DateTime.now(ZoneIds.cet);
	 *
	 * // 2013-12-07 03:41:35:563 PST
	 * val pdt = cet.toZone(ZoneIds.america_losangeles);
	 *
	 * // Compare `DateTime` instances based on the offset date-time and the zone,
	 * // that means that both represent the same time
	 * cet.compareTo(pdt); // 0
	 *
	 * // Compare `DateTime` instances for identical date-time and zone,
	 * // that means that both dates have to be defined identically
	 * cet.compareExactTo(pdt) // 1
	 * ```
	 *
	 * @see compareExactTo
	 */
	@Pure
	override fun compareTo(other: DateTime): Int = toInstant().compareTo(other.toInstant())
	
	/**
	 * Compares this to the given [DateTime] with the time zone taken into account.
	 *
	 * **Example:**
	 *
	 * ```
	 * // 2013-12-07 12:41:35:563 CET
	 * val cet = DateTime.now(ZoneIds.cet);
	 *
	 * // 2013-12-07 03:41:35:563 PST
	 * val pdt = cet.toZone(ZoneIds.america_losangeles);
	 *
	 * // Compare `DateTime` instances based on the offset date-time and the zone,
	 * // that means that both represent the same time
	 * cet.compareTo(pdt); // 0
	 *
	 * // Compare `DateTime` instances for identical date-time and zone,
	 * // that means that both dates have to be defined identically
	 * cet.compareExactTo(pdt) // 1
	 * ```
	 *
	 * @see compareTo
	 * @since 5.1
	 */
	@Pure
	public fun compareExactTo(other: DateTime): Int = zonedDateTime.compareTo(other.zonedDateTime)

	/**
	 * Indicates whether some other [DateTime] is equal to this one, *independent of the time zone*.
	 *
	 * **Example:**
	 * 
	 * ```
	 * // 2013-12-07 12:41:35:563 CET
	 * val cet = DateTime.now(ZoneIds.cet);
	 *
	 * // 2013-12-07 03:41:35:563 PST
	 * val pdt = cet.toZone(ZoneIds.america_losangeles);
	 *
	 * // Compare `DateTime` instances based on the offset date-time and the zone,
	 * // that means that both represent the same time
	 * cet == pdt; // true
	 *
	 * // Compare `DateTime` instances for identical date-time and zone,
	 * // that means that both dates have to be defined identically
	 * cet equalsExact pdt // false
	 * ```
	 *
	 * @param other The reference object with which to compare.
	 * @return `true` if this object is the same as [other]; `false` otherwise.
	 * @see equalsExact
	 */
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(javaClass != other?.javaClass) return false
		
		other as DateTime
		
		return zonedDateTime.isEqual(other.zonedDateTime)
	}

	/**
	 * Indicates whether some other [DateTime] is *exactly equal* to this one that means,
	 * the time zone has to be the same.
	 *
	 * **Example:**
	 * 
	 * ```
	 * // 2013-12-07 12:41:35:563 CET
	 * val cet = DateTime.now(ZoneIds.cet);
	 *
	 * // 2013-12-07 03:41:35:563 PST
	 * val pdt = cet.toZone(ZoneIds.america_losangeles);
	 *
	 * // Compare `DateTime` instances based on the offset date-time and the zone,
	 * // that means that both represent the same time
	 * cet == pdt; // true
	 *
	 * // Compare `DateTime` instances for identical date-time and zone,
	 * // that means that both dates have to be defined identically
	 * cet equalsExact pdt // false
	 * ```
	 *
	 * @param other The reference object with which to compare.
	 * @return `true` if this object is the same as [other]; `false` otherwise.
	 * @see equals
	 */
	@Pure
	public infix fun equalsExact(other: DateTime?): Boolean = other != null && zonedDateTime == other.zonedDateTime

	/**
	 * Indicates whether some other [DateTime] is *not exactly equal* to this one that means,
	 * the time zone has to be the same.
	 *
	 * @see equalsExact
	 * @since 5.1
	 */
	@Pure
	public infix fun equalsNotExact(other: DateTime?): Boolean = !equalsExact(other)
	
	/**
	 * Gets the hash code *independent of the time zone*.
	 *
	 * @see hashCodeExact
	 */
	override fun hashCode(): Int = toInstant().hashCode()
	
	/**
	 * Gets the hash code *considering the time zone*.
	 *
	 * @see hashCode
	 * @since 5.1
	 */
	@Pure
	public fun hashCodeExact(): Int = zonedDateTime.hashCode()

	/**
	 * Adds the given [timeSpan] from this [DateTime].
	 *
	 * @return [Failure] if the operation failed: Either an [ArithmeticException] on a numeric overflow
	 *   or a [DateTimeException], if the addition failed.
	 */
	@Pure
	public operator fun plus(timeSpan: TimeSpan): Conclude<DateTime, RuntimeException> = enclose { plus(timeSpan) }

	/**
	 * Adds the given [timeSpan] from this [DateTime].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @throws DateTimeException The addition failed.
	 * @since 6.0
	 */
	context(EnclosingContext<RuntimeException>)
	@Pure
	public operator fun plus(timeSpan: TimeSpan): DateTime =
		if(timeSpan.isZero) this else DateTime(zonedDateTime.plus(timeSpan.toDuration()))

	/**
	 * Subtracts the given [timeSpan] from this [DateTime].
	 *
	 * @return [Failure] if the operation failed: Either an [ArithmeticException] on a numeric overflow
	 *   or a [DateTimeException], if the subtraction failed.
	 */
	@Pure
	public operator fun minus(timeSpan: TimeSpan): Conclude<DateTime, RuntimeException> = enclose { minus(timeSpan) }

	/**
	 * Subtracts the given [timeSpan] from this [DateTime].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @throws DateTimeException The subtraction failed.
	 * @since 6.0
	 */
	context(EnclosingContext<RuntimeException>)
	@Pure
	public operator fun minus(timeSpan: TimeSpan): DateTime =
		if(timeSpan.isZero) this else DateTime(zonedDateTime.minus(timeSpan.toDuration()))

	/**
	 * Subtracts the [other] from this [DateTime].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28T17:19:16.919639635Z - 2023-12-30T05:19:16.919639635Z = -1.12 days
	 * 2023-12-30T05:19:16.919639635Z - 2023-12-28T17:19:16.919639635Z = +1.12 days
	 * ```
	 * This function produces the inverse result of [between].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see between
	 */
	@Pure
	public operator fun minus(other: DateTime): Conclude<TimeSpan, ArithmeticException> = enclose { minus(other) }

	/**
	 * Subtracts the [other] from this [DateTime].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28T17:19:16.919639635Z - 2023-12-30T05:19:16.919639635Z = -1.12 days
	 * 2023-12-30T05:19:16.919639635Z - 2023-12-28T17:19:16.919639635Z = +1.12 days
	 * ```
	 * This function produces the inverse result of [between].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @see between
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun minus(other: DateTime): TimeSpan =
		TimeSpan(Duration.between(other.zonedDateTime, zonedDateTime))

	/**
	 * Gets the time span between this and [other].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28T17:19:16.919639635Z - 2023-12-30T05:19:16.919639635Z = +1.12 days
	 * 2023-12-30T05:19:16.919639635Z - 2023-12-28T17:19:16.919639635Z = -1.12 days
	 * ```
	 *
	 * This function produces the inverse result of [minus].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see minus
	 * @since 5.1
	 */
	@Pure
	public infix fun between(other: DateTime): Conclude<TimeSpan, ArithmeticException> = enclose { between(other) }

	/**
	 * Gets the time span between this and [other].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28T17:19:16.919639635Z - 2023-12-30T05:19:16.919639635Z = +1.12 days
	 * 2023-12-30T05:19:16.919639635Z - 2023-12-28T17:19:16.919639635Z = -1.12 days
	 * ```
	 *
	 * This function produces the inverse result of [minus].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @see minus
	 * @since 5.1
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public infix fun between(other: DateTime): TimeSpan =
		TimeSpan(Duration.between(zonedDateTime, other.zonedDateTime))

	/**
	 * Converts this instance into a [Date].
	 */
	@Pure
	public fun toDate(): Date = Date.of(year, monthValue, day).orThrow()

	/**
	 * Converts this instance into a [ZonedDateTime].
	 */
	@Cached
	@Pure
	public fun toZonedDateTime(): ZonedDateTime = zonedDateTime

	@Transient
	private var cachedInstant: Instant? = null
	
	/**
	 * Converts this instance into an [Instant].
	 */
	@Cached
	@Pure
	public fun toInstant(): Instant = this::cachedInstant.getOrInitialize(zonedDateTime::toInstant)
	
	/**
	 * Gets the epoch timestamp in seconds.
	 *
	 * @since 5.1
	 */
	public fun toEpochSeconds(): Long = zonedDateTime.toEpochSecond()
	
	/**
	 * Gets the epoch timestamp in milliseconds.
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @since 5.1
	 */
	public fun toEpochMilli(): ConcludeLong<ArithmeticException> = catchLong { toInstant().toEpochMilli() }

	/**
	 * Gets a new [DateTime] instance representing the same date and time, but in another time zone.
	 *
	 *
	 * **Example:**
	 * 
	 * ```
	 * // 2013-12-07 12:41:35:563 CET
	 * DateTime cest = DateTime.now();
	 *
	 * // 2013-12-07 11:41:35:563 UTC
	 * DateTime utc = cest.toUniversalTime(); // same as `cest.toZone(ZoneIds.utc)`
	 *
	 * // 2013-12-07 03:41:35:563 PST
	 * DateTime pdt = utc.toZone(ZoneIds.america_losangeles);
	 *
	 * System.out.println(cest == pdt); // true
	 * ```
	 * 
	 * @param zoneId The new time zone.
	 * @return [Failure] if the result exceeds the supported date range.
	 * @see toUniversalTime
	 * @see toLocalTime
	 */
	@Pure
	public fun toZone(zoneId: ZoneId): Conclude<DateTime, DateTimeException> = `catch` {
		
		val otherZonedDateTime: ZonedDateTime = zonedDateTime.withZoneSameInstant(zoneId)

		// Info: If `otherZonedDateTime` is same instance as `zonedDateTime`, nothing has changed.
		if(otherZonedDateTime === zonedDateTime) this else DateTime(otherZonedDateTime)
	}

	/**
	 * Gets a new [DateTime] representing the same date and time, but in the [local][ZoneId.systemDefault] time zone.
	 *
	 * This is a convenience function for `toZone(ZoneId.systemDefault())`.
	 *
	 * @return [Failure] if the result exceeds the supported date range.
	 * @see toZone
	 * @see toUniversalTime
	 */
	@Pure
	public fun toLocalTime(): Conclude<DateTime, DateTimeException> = toZone(ZoneId.systemDefault())
	
	/**
	 * Gets a new [DateTime] representing the same date and time, but in the universal time code.
	 *
	 * This is a convenience function for `toZone(ZoneIds.utc)`.
	 *
	 * @return [Failure] if the result exceeds the supported date range.
	 * @see toZone
	 * @see toLocalTime
	 */
	@Pure
	public fun toUniversalTime(): Conclude<DateTime, DateTimeException> = toZone(ZoneIds.utc)
	
	@Transient
	private var cachedToString: String? = null
	
	/**
	 * Gets the string-representation of this [DateTime], e.g., `"2014-03-17 19:27:31.590584210+1:00[Europe/Berlin]"`,
	 * using [DateTimeFormatter.ISO_ZONED_DATE_TIME].
	 */
	@Cached
	@Pure
	override fun toString(): String = this::cachedToString.getOrInitialize {
		
		DateTimeFormatter.ISO_ZONED_DATE_TIME.format(zonedDateTime)
	}
	
	@Transient
	private var cachedToComparableString: Conclude<String, DateTimeException>? = null
	
	/**
	 * Gets the comparable string-representation of this [DateTime], e.g., `"2014-03-17T18:27:31.590584210Z"`.
	 */
	@Cached
	@Pure
	public fun toComparableString(): Conclude<String, DateTimeException> =
		this::cachedToComparableString.getOrInitialize {
		
		toUniversalTime().map { COMPARABLE_FORMAT.format(it.zonedDateTime) }
	}
	
	public companion object {
		
		private val COMPARABLE_FORMAT by lazy { DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.nnnnnnnnn'Z'") }
		
		// OF //////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Creates a new instance of the class [DateTime].
		 *
		 * @param localDateTime The [LocalDateTime].
		 * @param zone The time zone.
		 * @param preferredOffset The preferred [zone offset][ZoneOffset].
		 *
		 * @since 5.1
		 */
		@Pure
		public fun of(localDateTime: LocalDateTime, zone: ZoneId, preferredOffset: ZoneOffset): DateTime =
			DateTime(ZonedDateTime.ofLocal(localDateTime, zone, preferredOffset))
		
		/**
		 * Creates a new instance of the class [DateTime].
		 *
		 * @param year The year to represent, from `-999,999,999` to `999,999,999`.
		 * @param month The month-of-year.
		 * @param dayOfMonth The day-of-month to represent, from `1` to `31`.
		 * @param hour The hour-of-day to represent, from `0` to `23`.
		 * @param minute The minute-of-hour to represent, from `0` to `59`.
		 * @param second The second-of-minute to represent, from `0` to `59`.
		 * @param zone The time zone.
		 * @throws DateTimeException The value of any field is out of range, or if the day-of-month is invalid for the
		 *   month-year.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			year: Int,
			month: Month,
			dayOfMonth: Int,
			hour: Int,
			minute: Int,
			second: Int,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, DateTimeException> = of(year, month.value, dayOfMonth, hour, minute, second, 0, zone)
		
		/**
		 * Creates a new instance of the class [DateTime].
		 *
		 * @param year The year to represent, from `-999,999,999` to `999,999,999`.
		 * @param month The month-of-year.
		 * @param dayOfMonth The day-of-month to represent, from `1` to `31`.
		 * @param hour The hour-of-day to represent, from `0` to `23`.
		 * @param minute The minute-of-hour to represent, from `0` to `59`.
		 * @param second The second-of-minute to represent, from `0` to `59`.
		 * @param nanoseconds Nanos of a second, from `0` to `999,999,999`.
		 * @param zone The time zone.
		 * @throws DateTimeException The value of any field is out of range, or if the day-of-month is invalid for the
		 *   month-year.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			year: Int,
			month: Month,
			dayOfMonth: Int,
			hour: Int,
			minute: Int,
			second: Int,
			nanoseconds: Int,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, DateTimeException> =
			of(year, month.value, dayOfMonth, hour, minute, second, nanoseconds, zone)
		
		/**
		 * Creates a new instance of the class [DateTime].
		 *
		 * @param year The year to represent, from `-999,999,999` to `999,999,999`.
		 * @param month The month-of-year to represent, from `1` (January) to `12` (December).
		 * @param dayOfMonth The day-of-month to represent, from `1` to `31`.
		 * @param hour The hour-of-day to represent, from `0` to `23`.
		 * @param minute The minute-of-hour to represent, from `0` to `59`.
		 * @param second The second-of-minute to represent, from `0` to `59`.
		 * @param zone The time zone.
		 * @throws DateTimeException The value of any field is out of range, or if the day-of-month is invalid for the
		 *   month-year.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			year: Int,
			month: Int,
			dayOfMonth: Int,
			hour: Int,
			minute: Int,
			second: Int,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, DateTimeException> = of(year, month, dayOfMonth, hour, minute, second, 0, zone)
		
		/**
		 * Creates a new instance of the class [DateTime].
		 *
		 * @param year The year to represent, from `-999,999,999` to `999,999,999`.
		 * @param month The month-of-year to represent, from `1` (January) to `12` (December).
		 * @param dayOfMonth The day-of-month to represent, from `1` to `31`.
		 * @param hour The hour-of-day to represent, from `0` to `23`.
		 * @param minute The minute-of-hour to represent, from `0` to `59`.
		 * @param second The second-of-minute to represent, from `0` to `59`.
		 * @param nanoseconds Nanos of a second, from `0` to `999,999,999`.
		 * @param zone The time zone.
		 * @throws DateTimeException The value of any field is out of range, or if the day-of-month is invalid for the
		 *   month-year.
		 * @since 5.1
		 */
		@Pure
		public fun of(
			year: Int,
			month: Int,
			dayOfMonth: Int,
			hour: Int,
			minute: Int,
			second: Int,
			nanoseconds: Int,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, DateTimeException> = `catch` {
			
			DateTime(ZonedDateTime.of(year, month, dayOfMonth, hour, minute, second, nanoseconds, zone))
		}
		
		// NOW /////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Gets the current [DateTime] from the [default time zone][ZoneIds.systemDefault].
		 */
		@Pure
		public fun now(): DateTime = DateTime()
		
		/**
		 * Gets the current [Date] from the specified [time zone][zone].
		 */
		@Pure
		public fun now(zone: ZoneId): DateTime = DateTime(ZonedDateTime.now(zone))
		
		/**
		 * Gets the current [DateTime] from the [UTC time zone][ZoneIds.utc].
		 */
		@Pure
		public fun utcNow(): DateTime = now(ZoneIds.utc)
		
		// OF CERTAIN UNIT /////////////////////////////////////////////////////////////////////////////////////////////
	
		/**
		 * Creates a [DateTime] of [epochSecond] (Unix timestamp) and [times zone][zone].
		 *
		 * @return [Failure] if the maximum or minimum is exceeded.
		 * @since 5.1
		 */
		public fun ofEpochSecond(
			epochSecond: Long,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, DateTimeException> = `catch` {
			
			DateTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(epochSecond), zone))
		}
	
		/**
		 * Creates a [DateTime] of [epochSecond] (Unix timestamp), [nanoAdjustment], and [times zone][zone].
		 *
		 * An arbitrary value can be given for [nanoAdjustment], since the value is altered during conversion.
		 *
		 * @return [Failure] if either a numeric overflow occurred ([ArithmeticException]) or the result exceeds the
		 *   supported range ([DateTimeException]).
		 * @throws ArithmeticException Numeric overflow.
		 * @since 5.1
		 */
		public fun ofEpochSecond(
			epochSecond: Long,
			nanoAdjustment: Long,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, RuntimeException> = `catch` {
			
			DateTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(epochSecond, nanoAdjustment), zone))
		}
	
		/**
		 * Creates a [DateTime] of [epoch milliseconds][epochMilli] (Unix timestamp) and [times zone][zone].
		 *
		 * @return [Failure] if the maximum or minimum is exceeded.
		 */
		public fun ofEpochMilli(
			epochMilli: Long,
			zone: ZoneId = ZoneId.systemDefault()
		) : Conclude<DateTime, DateTimeException> = `catch` {
			
			DateTime(ZonedDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), zone))
		}
		
		// PARSE ///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Parses a [DateTime] from the given [text], using [DateTimeFormatter.ISO_ZONED_DATE_TIME].
		 *
		 * @param text The text to parse.
		 * @return [Failure] if the input is invalid.
		 */
		@Pure
		public fun parse(text: CharSequence): Conclude<DateTime, DateTimeParseException> =
			catch<_, DateTimeParseException> { DateTime(ZonedDateTime.parse(text)) }
		
		/**
		 * Parses a [DateTime] from the given [text]  using the given [formatter].
		 *
		 * @param text The text to parse.
		 * @param formatter The formatter to use.
		 * @return [Failure] if the input is invalid.
		 */
		@Pure
		public fun parse(text: CharSequence, formatter: DateTimeFormatter): Conclude<DateTime, DateTimeParseException> =
			`catch`<_, DateTimeParseException> {  DateTime(ZonedDateTime.parse(text, formatter)) }
		
		/**
		 * Parses a [DateTime] from the given [text], [zone], and [preferredOffset],
		 * using [DateTimeFormatter.ISO_LOCAL_DATE_TIME].
		 *
		 * @param text The text to parse.
		 * @param zone The time zone.
		 * @param preferredOffset The preferred [zone offset][ZoneOffset].
		 * @return [Failure] if the input is invalid.
		 */
		@Pure
		public fun parse(
			text: CharSequence,
			zone: ZoneId,
			preferredOffset: ZoneOffset
		) : Conclude<DateTime, DateTimeParseException> = `catch`<_, DateTimeParseException> {
			
			DateTime(ZonedDateTime.ofLocal(LocalDateTime.parse(text), zone, preferredOffset))
		}
		
		/**
		 * Parses a [DateTime] from the given [text], [zone], and [preferredOffset], using the given [formatter].
		 *
		 * @return [Failure] if the input is invalid.
		 */
		@Pure
		public fun parse(
			text: CharSequence,
			formatter: DateTimeFormatter,
			zone: ZoneId,
			preferredOffset: ZoneOffset
		) : Conclude<DateTime, DateTimeParseException> = `catch`<_, DateTimeParseException> {
			
			DateTime(ZonedDateTime.ofLocal(LocalDateTime.parse(text, formatter), zone, preferredOffset))
		}
	}
}
