/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.require
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.conclude.*
import cc.persch.stardust.datetime.DateUtils
import cc.persch.stardust.datetime.ZoneIds
import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.math.timesExact
import cc.persch.stardust.pipe
import cc.persch.stardustx.datetime.TimeSpan.Companion.MILLISECONDS_PER_DAY
import cc.persch.stardustx.datetime.TimeSpan.Companion.SECONDS_PER_DAY
import cc.persch.stardustx.datetime.serializers.DateSerializer
import kotlinx.serialization.Serializable
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.*

/**
 * Defines a date (without time and time zone).
 *
 * This class is immutable and thus thread-safe.
 *
 * @since 1.0
 * @see DateTime
 * @see TimeSpan
 *
 * @constructor Creates a new instance of the [Date] class using the given [localDate].
 */
@Serializable(with = DateSerializer::class)
@ThreadSafe
public class Date internal constructor(
	private val localDate: LocalDate = LocalDate.now()
) : Comparable<Date>, TemporalAccessor by localDate {
	
	/**
	 * Gets the year.
	 */
	public val year: Int
		get() = localDate.year
	
	/**
	 * Gets the month.
	 */
	public val month: Month
		get() = localDate.month
	
	/**
	 * Gets the month.
	 */
	public val monthValue: Int
		get() = localDate.monthValue
	
	/**
	 * Gets the day.
	 */
	public val day: Int
		get() = localDate.dayOfMonth
	
	/**
	 * Gets the day of week.
	 */
	public val dayOfWeek: DayOfWeek
		get() = localDate.dayOfWeek
	
	/**
	 * Gets the day of year.
	 */
	public val dayOfYear: Int
		get() = localDate.dayOfYear
	
	/**
	 * Gets the week of the year as known from organizers. The year starts with the first week and may end with week 53.
	 */
	public val weekOfYear: Int
		get() = toUtcDateTime().weekOfYear
	
	/**
	 * Gets the raw week of the year. January 1 to January 7 is week one, January 8 to January 14 is week two and so
	 * on, up to week 52.
	 */
	public val rawWeekOfYear: Int
		get() = localDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR)
	
	/**
	 * Gets the week of the year.
	 */
	public val weekOfMonth: Int
		get() = localDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH)
	
	/**
	 * Gets a value, indicating whether the year of this instance is a leap year.
	 */
	public val isLeapYear: Boolean
		get() = DateUtils.isLeapYear(year)
	
	/**
	 * Gets the number of days since the epoch, where day `0` is 1970-01-01.
	 *
	 * @since 5.1
	 */
	public fun toEpochDays(): Long = localDate.toEpochDay()
	
	/**
	 * Gets the epoch timestamp in seconds.
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	public fun toEpochSeconds(): ConcludeLong<ArithmeticException> =
		localDate.toEpochDay() timesExact SECONDS_PER_DAY
	
	/**
	 * Gets the epoch timestamp in milliseconds.
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @since 5.1
	 */
	public fun toEpochMillis(): ConcludeLong<ArithmeticException> =
		localDate.toEpochDay() timesExact MILLISECONDS_PER_DAY.toLong()
	
	/**
	 * Gets the date of the first day of the month.
	 *
	 * Example: `Date(2023, 07, 25).toFirstOfMonth()` gets `2023-07-01`.
	 */
	public fun toFirstOfMonth(): Date = if(day == 1) this else of(year, month, 1).orThrow()
	
	/**
	 * Gets the date of the last day of the month.
	 *
	 * Example: `Date(2023, 07, 25).toFirstOfMonth()` gets `2023-07-31`.
	 *
	 * @since 5.1
	 */
	public fun toLastOfMonth(): Date {
		
		val lastDay = month.length(DateUtils.isLeapYear(year))
		
		return if(day == lastDay) this else of(year, month, lastDay).orThrow()
	}

	/**
	 * Converts this instance into a [LocalDate].
	 *
	 * @since 5.1
	 */
	@Cached
	@Pure
	public fun toLocalDate(): LocalDate = localDate
	
	@Pure
	override fun compareTo(other: Date): Int = localDate.compareTo(other.localDate)
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(javaClass != other?.javaClass) return false
		
		other as Date
		
		return localDate == other.localDate
	}
	
	override fun hashCode(): Int = localDate.hashCode()
	
	/**
	 * Gets a new [Date] with specified [TimeSpan] added. The given time span must represent whole days.
	 *
	 * @return [Failure] if [timeSpan] does not represent whole days ([IllegalArgumentException]] or
	 *   the result exceeds the supported date range ([DateTimeException]).
	 * @see plusDays
	 */
	@Pure
	public operator fun plus(timeSpan: TimeSpan): Conclude<Date, RuntimeException> = enclose { plus(timeSpan) }
	
	/**
	 * Gets a new [Date] with specified [TimeSpan] added. The given time span must represent whole days.
	 *
	 * @throws IllegalArgumentException [timeSpan] does not represent whole days.
	 * @throws DateTimeException The result exceeds the supported date range
	 * @see plusDays
	 * @since 6.0
	 */
	context(EnclosingContext<RuntimeException>)
	@Pure
	public operator fun plus(timeSpan: TimeSpan): Date = plusMinus(timeSpan, LocalDate::plusDays)
	
	/**
	 * Gets a new [Date] with specified [TimeSpan] subtracted. The given time span must represent whole days.
	 *
	 * @return [Failure] if [timeSpan] does not represent whole days ([IllegalArgumentException]] or
	 *   the result exceeds the supported date range ([DateTimeException]).
	 * @see minusDays
	 */
	@Pure
	public operator fun minus(timeSpan: TimeSpan): Conclude<Date, RuntimeException> = enclose { minus(timeSpan) }
	
	/**
	 * Gets a new [Date] with specified [TimeSpan] subtracted. The given time span must represent whole days.
	 *
	 * @throws IllegalArgumentException [timeSpan] does not represent whole days.
	 * @throws DateTimeException The result exceeds the supported date range
	 * @see minusDays
	 * @since 6.0
	 */
	context(EnclosingContext<RuntimeException>)
	@Pure
	public operator fun minus(timeSpan: TimeSpan): Date = plusMinus(timeSpan, LocalDate::minusDays)
	
	/**
	 * @throws IllegalArgumentException
	 * @throws DateTimeException
	 */
	@Pure
	private fun plusMinus(timeSpan: TimeSpan, action: (LocalDate, Long) -> LocalDate): Date {
		
		require(
			"timeSpan",
			timeSpan.hours == 0 && timeSpan.minutes == 0 && timeSpan.seconds == 0 && timeSpan.milliseconds == 0
		)
		
		return timeSpan.inWholeDays() pipe { days ->
		
			if(days == 0L) this else Date(action(localDate, days))
		}
	}
	
	/**
	 * Add the given [days] to this [Date]. This is a convenience function for `date + TimeSpan.ofDays(days)`.
	 *
	 * @return [Failure] if the result exceeds the supported date range.
	 * @since 5.1
	 */
	@Pure
	public infix fun plusDays(days: Long): Conclude<Date, DateTimeException> = enclose { plusDays(days) }
	
	/**
	 * Add the given [days] to this [Date]. This is a convenience function for `date + TimeSpan.ofDays(days)`.
	 *
	 * @throws DateTimeException The result exceeds the supported date range.
	 * @since 6.0
	 */
	context(EnclosingContext<DateTimeException>)
	@Pure
	public infix fun plusDays(days: Long): Date = if(days == 0L) this else Date(localDate.plusDays(days))
	
	/**
	 * Subtracts the given [days] from this [Date]. This is a convenience function for `date - TimeSpan.ofDays(days)`.
	 *
	 * @return [Failure] if the result exceeds the supported date range.
	 * @since 5.1
	 */
	@Pure
	public infix fun minusDays(days: Long): Conclude<Date, DateTimeException> = enclose { minusDays(days) }
	
	/**
	 * Subtracts the given [days] from this [Date]. This is a convenience function for `date - TimeSpan.ofDays(days)`.
	 *
	 * @throws DateTimeException The result exceeds the supported date range.
	 * @since 6.0
	 */
	context(EnclosingContext<DateTimeException>)
	@Pure
	public infix fun minusDays(days: Long): Date = if(days == 0L) this else Date(localDate.minusDays(days))
	
	/**
	 * Subtracts [other] from this [Date].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28 - 2023-12-29 = -1 day
	 * 2023-12-29 - 2023-12-28 = +1 day
	 * ```
	 * This function produces the inverse result of [between].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see between
	 */
	@Pure
	public operator fun minus(other: Date): Conclude<TimeSpan, ArithmeticException> =
		toUtcDateTime().minus(other.toUtcDateTime())
	
	/**
	 * Subtracts [other] from this [Date].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28 - 2023-12-29 = -1 day
	 * 2023-12-29 - 2023-12-28 = +1 day
	 * ```
	 * This function produces the inverse result of [between].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @see between
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	@Pure
	public operator fun minus(other: Date): TimeSpan = toUtcDateTime().minus(other.toUtcDateTime())
	
	/**
	 * Gets the time span between this and the given [DateTime].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28 - 2023-12-29 = +1 day
	 * 2023-12-29 - 2023-12-28 = -1 day
	 * ```
	 *
	 * This function produces the inverse result of [minus].
	 *
	 * @return [Failure] if a numeric overflow occurred.
	 * @see minus
	 * @since 5.1
	 */
	public infix fun between(other: Date): Conclude<TimeSpan, ArithmeticException> =
		toUtcDateTime().between(other.toUtcDateTime())
	
	/**
	 * Gets the time span between this and the given [DateTime].
	 *
	 * Examples:
	 *
	 * ```text
	 * 2023-12-28 - 2023-12-29 = +1 day
	 * 2023-12-29 - 2023-12-28 = -1 day
	 * ```
	 *
	 * This function produces the inverse result of [minus].
	 *
	 * @throws ArithmeticException A numeric overflow occurred.
	 * @see minus
	 * @since 6.0
	 */
	context(EnclosingContext<ArithmeticException>)
	public infix fun between(other: Date): TimeSpan = toUtcDateTime().between(other.toUtcDateTime())
	
	/**
	 * Converts this [Date] to an [DateTime] of the given [time zone][zone].
	 */
	@Pure
	public fun toDateTime(zone: ZoneId): DateTime = DateTime.of(year, monthValue, day, 0, 0, 0, 0, zone).orThrow()
	
	@Transient
	private var cachedUtcDateTime: DateTime? = null
	
	/**
	 * Converts this [Date] to an [DateTime] of the [UTC time zone][ZoneIds.utc].
	 */
	@Cached
	@Pure
	public fun toUtcDateTime(): DateTime = this::cachedUtcDateTime.getOrInitialize { toDateTime(ZoneIds.utc) }
	
	@Transient
	private var cachedInstant: Instant? = null
	
	/**
	 * Converts this [Date] to an [Instant].
	 */
	@Pure
	public fun toInstant(): Instant = this::cachedInstant.getOrInitialize { toUtcDateTime().toInstant() }
	
	@Transient
	private var cachedToString: String? = null
	
	/**
	 * Gets the string-representation of this [Date], e.g. `2014-03-17`, using [DateTimeFormatter.ISO_LOCAL_DATE].
	 */
	@Cached
	@Pure
	override fun toString(): String = this::cachedToString.getOrInitialize {
		
		DateTimeFormatter.ISO_LOCAL_DATE.format(localDate)
	}
	
	public companion object {
		
		// OF //////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Creates a new instance of the class [Date].
		 *
		 * @param year The year to represent, from `-999,999,999` to `999,999,999`.
		 * @param month The month.
		 * @param dayOfMonth The day-of-month to represent, from `1` to `31`.
		 * @return [Failure] if the value of any field is out of range, or if the day-of-month is invalid for the
		 *   month-year.
		 * @since 5.1
		 */
		@Pure
		public fun of(year: Int, month: Month, dayOfMonth: Int): Conclude<Date, DateTimeException> =
			`catch` { Date(LocalDate.of(year, month, dayOfMonth)) }
		
		/**
		 * Creates a new instance of the class [Date].
		 *
		 * @param year The year to represent, from `-999,999,999` to `999,999,999`.
		 * @param month The month-of-year to represent, from `1` (January) to `12` (December).
		 * @param dayOfMonth The day-of-month to represent, from `1` to `31`.
		 * @return [Failure] if the value of any field is out of range, or if the day-of-month is invalid for the
		 *   month-of-year.
		 * @since 5.1
		 */
		@Pure
		public fun of(year: Int, month: Int, dayOfMonth: Int): Conclude<Date, DateTimeException> =
			`catch` { Date(LocalDate.of(year, month, dayOfMonth)) }
		
		// CURRENT /////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * Gets the current [Date] from the [default time zone][ZoneIds.systemDefault].
		 */
		@Pure
		public fun current(): Date = Date()
		
		/**
		 * Gets the current [Date] from the [UTC time zone][ZoneIds.utc].
		 */
		@Pure
		public fun utcCurrent(): Date = current(ZoneIds.utc)
		
		/**
		 * Gets the current [Date] from the specified [time zone][zone].
		 */
		@Pure
		public fun current(zone: ZoneId): Date = Date(LocalDate.now(zone))
		
		/**
		 * Gets the date of the first day of the specified month in the specified year.
		 *
		 * Example: `Dates.firstOf(NOVEMBER, 2018)` gets the date `2018-11-01`.
		 *
		 * @return [Failure] if the specified year is invalid.
		 */
		@Pure
		public fun firstOf(month: Month, year: Int): Conclude<Date, DateTimeException> = of(year, month, 1)
		
		/**
		 * Gets the date of the last day of the specified month in the specified year.
		 *
		 * Example: `Dates.lastOf(NOVEMBER, 2018)` gets the date `2018-11-30`.
		 *
		 * @return [Failure] if the specified year is invalid.
		 */
		@Pure
		public fun lastOf(month: Month, year: Int): Conclude<Date, DateTimeException> =
			of(year, month, month.length(DateUtils.isLeapYear(year)))
		
		/**
		 * Parses a [Date] from a string, using [DateTimeFormatter.ISO_LOCAL_DATE].
		 *
		 * @return [Failure] if the input is invalid.
		 */
		@Pure
		public fun parse(text: CharSequence): Conclude<Date, DateTimeParseException> =
			`catch`<_, DateTimeParseException> { Date(LocalDate.parse(text)) }
		
		/**
		 * Parses a [Date] from the given [text] using the given [formatter].
		 *
		 * @return [Failure] if the input is invalid.
		 */
		@Pure
		public fun parse(text: CharSequence, formatter: DateTimeFormatter): Conclude<Date, DateTimeParseException> =
			`catch`<_, DateTimeParseException> { Date(LocalDate.parse(text, formatter)) }
	}
}
