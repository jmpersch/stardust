/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardustx.datetime.timespans

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardustx.datetime.TimeSpan
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_DAY
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_HOUR
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_MICROSECOND
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_MILLISECOND
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_MINUTE
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_SECOND
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Multiplies the given [TimeSpan] by this value.
 *
 * @throws ArithmeticException A numeric overflow occurred.
 * @since 5.1
 */
@Pure
public operator fun Int.times(timeSpan: TimeSpan): Conclude<TimeSpan, ArithmeticException> = timeSpan * this

/**
 * Multiplies the given [TimeSpan] by this value.
 *
 * @throws ArithmeticException A numeric overflow occurred.
 * @since 5.1
 */
@Pure
public operator fun Long.times(timeSpan: TimeSpan): Conclude<TimeSpan, ArithmeticException> = timeSpan * this

/**
 * Invokes the given [block] and returns elapsed time in nanoseconds resolution using [System.nanoTime].
 *
 * @since 5.1
 */
public inline fun measureNanoTime(block: () -> Unit): TimeSpan {
	
	contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
	
	val startTime = System.nanoTime()
	
	block()
	
	val elapsedNanos = System.nanoTime() - startTime
	
	return TimeSpan.ofNanoseconds(elapsedNanos)
}

// IN APPROXIMATED VALUES //////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Gets the time span in approximated days as a floating point number.
 *
 * @see TimeSpan.inWholeDays
 * @since 5.1
 */
public fun TimeSpan.inDays(): Double = inNanoseconds() / NANOSECONDS_PER_DAY

/**
 * Gets the time span in approximated hours as a floating point number.
 *
 * @see TimeSpan.inWholeHours
 * @since 5.1
 */
public fun TimeSpan.inHours(): Double = inNanoseconds() / NANOSECONDS_PER_HOUR

/**
 * Gets the time span in approximated minutes as a floating point number.
 *
 * @see TimeSpan.inWholeMinutes
 * @since 5.1
 */
public fun TimeSpan.inMinutes(): Double = inNanoseconds() / NANOSECONDS_PER_MINUTE

/**
 * Gets the time span in approximated seconds as a floating point number.
 *
 * @see TimeSpan.inWholeSeconds
 * @since 5.1
 */
public fun TimeSpan.inSeconds(): Double = inNanoseconds() / NANOSECONDS_PER_SECOND

/**
 * Gets the time span in approximated milliseconds as a floating point number.
 *
 * @see TimeSpan.inWholeMilliseconds
 * @since 5.1
 */
public fun TimeSpan.inMilliseconds(): Double = inNanoseconds() / NANOSECONDS_PER_MILLISECOND

/**
 * Gets the time span in approximated milliseconds as a floating point number.
 *
 * @see TimeSpan.inWholeMicroseconds
 * @since 5.1
 */
public fun TimeSpan.inMicroseconds(): Double = inNanoseconds() / NANOSECONDS_PER_MICROSECOND

/**
 * Gets the time span in approximated nanoseconds as a floating point number.
 *
 * @see TimeSpan.inWholeNanoseconds
 * @since 5.1
 */
public fun TimeSpan.inNanoseconds(): Double = inWholeSeconds().toDouble() * NANOSECONDS_PER_SECOND + nanoseconds
