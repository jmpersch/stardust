/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime.serializers

import cc.persch.stardust.conclude.orElse
import cc.persch.stardustx.datetime.DateTime
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.descriptors.PrimitiveKind.STRING
import kotlinx.serialization.encoding.*
import java.time.format.DateTimeFormatter

/**
 * Defines a serializer for [DateTime] via [DateTimeFormatter.ISO_LOCAL_DATE_TIME].
 *
 * Uses [DateTime.toString] for serialization and [DateTime.parse] for deserialization.
 *
 * @since 5.1
 * @see DateTime.toString
 * @see DateTime.parse
 */
public object DateTimeSerializer: KSerializer<DateTime> {
	
	override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(DateTime::class.qualifiedName!!, STRING)
	
	override fun serialize(encoder: Encoder, value: DateTime): Unit = encoder.encodeString(value.toString())
	
	override fun deserialize(decoder: Decoder): DateTime {
		
		val input = decoder.decodeString()
		
		return DateTime.parse(input) orElse {
			
			throw SerializationException("Failed decoding date-time.", it)
		}
	}
}
