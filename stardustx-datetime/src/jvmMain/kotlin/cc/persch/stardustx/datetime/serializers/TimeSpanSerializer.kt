/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime.serializers

import cc.persch.stardust.conclude.orElse
import cc.persch.stardustx.datetime.TimeSpan
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.descriptors.PrimitiveKind.STRING
import kotlinx.serialization.encoding.*
import java.time.Duration

/**
 * Defines a formatter for a time span of the ISO 8601 standard, e.g., `"PT123H56M29.789S"`.
 * For more information about the supported input, see [Duration.parse].
 *
 * Uses [TimeSpan.toString] for serialization and [TimeSpan.parse] for deserialization.
 *
 * @since 5.1
 * @see TimeSpan.toString
 * @see TimeSpan.parse
 */
public object TimeSpanSerializer: KSerializer<TimeSpan> {
	
	override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(TimeSpan::class.qualifiedName!!, STRING)
	
	override fun serialize(encoder: Encoder, value: TimeSpan): Unit = encoder.encodeString(value.toString())
	
	override fun deserialize(decoder: Decoder): TimeSpan {
		
		val input = decoder.decodeString()
		
		 return TimeSpan.parse(input) orElse {
			 
			 throw SerializationException("Failed decoding timespan.", it)
		 }
	}
}
