/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.asSuccess
import cc.persch.stardust.conclude.orThrow
import cc.persch.stardust.datetime.ZoneIds
import cc.persch.stardustx.datetime.timespans.units.hours
import cc.persch.stardustx.test.expectSuccess
import cc.persch.stardustx.test.assertSuccessEquals
import java.time.ZoneId
import kotlin.test.*

class DateTimeTest {

	@Test
	fun equalsTest1() = assertEquals(createDateTime(), createDateTime())

	@Test
	fun equalsTest2() = assertEquals(createDateTime().toZone(NEW_YORK).orThrow(), createDateTime())
	
	@Test
	fun equalsNotTest() = assertNotEquals(createDateTime(hour = 18), createDateTime(hour = 19))
	
	@Test
	fun equalsExactTest1() = assertTrue(createDateTime() equalsExact createDateTime())
	
	@Test
	fun equalsExactTest2() = assertFalse(createDateTime().toZone(NEW_YORK).orThrow() equalsExact createDateTime())
	
	@Test
	fun equalsNotExactTest1() = assertFalse(createDateTime() equalsNotExact createDateTime())
	
	@Test
	fun equalsNotExactTest2() =
		assertTrue(createDateTime().toZone(NEW_YORK).orThrow() equalsNotExact createDateTime())
	
	@Test
	fun hashCodeTest() = assertEquals(1423566840, createDateTime().hashCode())
	
	@Test
	fun hashCodeExactTest() = assertEquals(-1225443025, createDateTime().hashCodeExact())
	
	@Test
	fun compareTest1() = assertEquals(0, createDateTime().compareTo(createDateTime()))
	
	@Test
	fun compareTest2() = assertEquals(-1, createDateTime(hour = 18).compareTo(createDateTime(hour = 19)))
	
	@Test
	fun compareTest3() = assertEquals(1, createDateTime(hour = 19).compareTo(createDateTime(hour = 18)))
	
	@Test
	fun compareTest4() = assertEquals(0, createDateTime().toZone(NEW_YORK).expectSuccess().compareTo(createDateTime()))
	
	@Test
	fun compareTest5() = assertEquals(-1, createDateTime(hour = 18).toZone(NEW_YORK).expectSuccess().compareTo(createDateTime(hour = 19)))
	
	@Test
	fun compareTest6() = assertEquals(1, createDateTime(hour = 19).toZone(NEW_YORK).expectSuccess().compareTo(createDateTime(hour = 18)))
	
	@Test
	fun compareExactTest1() = assertEquals(0, createDateTime().compareExactTo(createDateTime()))
	
	@Test
	fun compareExactTest2() = assertEquals(-1, createDateTime(hour = 18).compareExactTo(createDateTime(hour = 19)))
	
	@Test
	fun compareExactTest3() = assertEquals(1, createDateTime(hour = 19).compareExactTo(createDateTime(hour = 18)))
	
	@Test
	fun compareExactTest4() = assertEquals(-1, createDateTime().toZone(NEW_YORK).expectSuccess().compareExactTo(createDateTime()))
	
	@Test
	fun compareExactTest5() = assertEquals(-1, createDateTime(hour = 18).toZone(NEW_YORK).expectSuccess().compareExactTo(createDateTime(hour = 19)))
	
	@Test
	fun compareExactTest6() = assertEquals(1, createDateTime(hour = 19).toZone(NEW_YORK).expectSuccess().compareExactTo(createDateTime(hour = 18)))
	
	@Test
	fun compareExactTest7() = assertEquals(1, createDateTime(hour = 19).toZone(NEW_YORK).expectSuccess().compareExactTo(createDateTime(hour = 17)))
	
	@Test
	fun plusTimeSpanTest() {
		
		assertSuccessEquals(createDateTime(hour = 19), createDateTime(hour = 18) + 1.hours.orThrow())
	}
	
	@Test
	fun minusTimeSpanTest() {
		
		assertSuccessEquals(createDateTime(hour = 18), createDateTime(hour = 19) - 1.hours.orThrow())
	}
	
	@Test
	fun minusDateTimeTest1() {
		
		assertSuccessEquals(1.hours.orThrow(), createDateTime(hour = 19) - createDateTime(hour = 18))
	}
	
	@Test
	fun minusDateTimeTest2() {
		
		assertSuccessEquals((-1).hours.orThrow(), createDateTime(hour = 18) - createDateTime(hour = 19))
	}
	
	@Test
	fun betweenTest1() {
		
		assertSuccessEquals((-1).hours.orThrow(), createDateTime(hour = 19) between createDateTime(hour = 18))
	}
	
	@Test
	fun betweenTest2() {
		
		assertSuccessEquals(1.hours.orThrow(), createDateTime(hour = 18) between createDateTime(hour = 19))
	}
	
	@Test
	fun toStringTest() {
		
		assertEquals(DATE_TIME_STRING, createDateTime().toString())
	}
	
	@Test
	fun toComparableStringTest() {
		
		assertSuccessEquals("2023-07-31T16:19:21.500050005Z", createDateTime().toComparableString())
	}
	
	@Test
	fun parseTest() {
		
		assertEquals(createDateTime().asSuccess(), DateTime.parse(DATE_TIME_STRING))
	}
	
	@Test
	fun dateTest() {
		
		assertEquals(createDateTime(hour = 0, minute = 0, second = 0, nanos = 0), createDateTime().date)
	}
	
	@Test
	fun millisecondTest() {
		
		assertEquals(500, createDateTime().millisecond)
	}
	
	@Test
	fun toDateTest() {
		
		assertEquals(Date.of(YEAR, MONTH, DAY).orThrow(), createDateTime().toDate())
	}
	
	@Test
	fun toUtcTest() {
		
		assertSuccessEquals(createDateTime(hour = 16, zone = ZoneIds.utc), createDateTime().toUniversalTime())
	}
	
	private companion object {
		
		private const val YEAR = 2023
		private const val MONTH = 7
		private const val DAY = 31
		private const val HOUR = 18
		private const val MINUTE = 19
		private const val SECOND = 21
		private const val NANOS = 500_050_005
		
		private val BERLIN = ZoneIds.europe_berlin
		private val NEW_YORK = ZoneIds.america_newyork
		
		private const val DATE_TIME_STRING = "2023-07-31T18:19:21.500050005+02:00[Europe/Berlin]"
		
		@Pure
		private fun createDateTime(
			year: Int = YEAR,
			month: Int = MONTH,
			day: Int = DAY,
			hour: Int = HOUR,
			minute: Int = MINUTE,
			second: Int = SECOND,
			nanos: Int = NANOS,
			zone: ZoneId = BERLIN
		) = DateTime.of(year, month, day, hour, minute, second, nanos, zone) orThrow {
			
			"Failed creating DateTime for DateTimeTest."
		}
	}
}
