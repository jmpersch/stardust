/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime.serializers

import cc.persch.stardust.conclude.orThrow
import cc.persch.stardustx.datetime.TimeSpan
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.intellij.lang.annotations.Language
import kotlin.test.Test
import kotlin.test.assertEquals

class TimeSpanSerializerTest {
	
	@Test
	fun encodeTest() = assertEquals(JSON_TIMESPAN_STRING, Json.encodeToString(TIMESPAN))

	@Test
	fun decodeTest() = assertEquals(TIMESPAN, Json.decodeFromString<TimeSpan>(JSON_TIMESPAN_STRING))

	companion object {

		@Language("JSON")
		private const val JSON_TIMESPAN_STRING = "\"PT124H3M2.123456789S\""

		private val TIMESPAN = TimeSpan.of(5, 4, 3, 2, 123456789).orThrow()
	}
}
