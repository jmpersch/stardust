/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_MAX_VALUE
import cc.persch.stardustx.datetime.TimeSpan.Companion.NANOSECONDS_PER_SECOND
import cc.persch.stardustx.datetime.timespans.*
import cc.persch.stardustx.test.*
import kotlin.test.*

class TimeSpanTest {
	
	@Test
	 fun testDecomposition() {
		
		assertComponents(SECONDS, NANOS, createTimeSpan())
	}
	
	@Test
	 fun testNegation() {
		
		assertComponents(-SECONDS, -NANOS, createTimeSpan().negate())
	}
	
	@Test
	 fun testEquals() {
		
		assertEquals(createTimeSpan(), createTimeSpan())
	}
	
	@Test
	 fun testEqualsNot() {
		
		assertSuccessNotEquals(createTimeSpan(), createTimeSpan().negate())
	}
	
	@Test
	 fun testHashCode() {
		
		assertEquals(51446633, createTimeSpan().hashCode())
	}
	
	@Test
	 fun testDecompositionAndRecompositionPositive() {
		
		val t1 = createTimeSpan()
		val (s, n) = t1
		val t2 = TimeSpan.ofSeconds(s, n)
		
		assertSuccessEquals(t1, t2)
	}
	
	@Test
	 fun testDecompositionAndRecompositionNegative() {
		
		val t1 = createTimeSpan().negate().orThrow()
		val (s, n) = t1
		val t2 = TimeSpan.ofSeconds(s, n)
		
		assertSuccessEquals(t1, t2)
	}
	
	@Test
	 fun testConstructorAndComponentsWithNanosPositive() {
		
		runConstructorTest(isPositive = true)
	}
	
	@Test
	 fun testConstructorAndComponentsWithNanosNegative() {
		
		runConstructorTest(isPositive = false)
	}
	
	private  fun runConstructorTest(isPositive: Boolean) {
		
		val factor = if(isPositive) 1 else -1
		
		val timeSpan = TimeSpan.of(
			factor * 1L,
			factor * 25L,
			factor * (25L * 60L + 1L),
			factor * (25L * 60L * 60L + 61L),
			factor * (25L * 60L * 60L * NANOSECONDS_PER_SECOND + 61L * NANOSECONDS_PER_SECOND + 1_000_001L),
		).expectSuccess("Failed crating TimeSpan using `TimeSpan.of`.")
		
		assertComponents(factor * SECONDS, factor * NANOS, timeSpan)
		
		assertEquals(
			if(isPositive) STRING_POSITIVE_NANOS else STRING_NEGATIVE_NANOS,
			"$timeSpan",
			"String representation"
		)
		
		assertEquals(factor * 5L, timeSpan.days, "Days")
		assertEquals(factor * 4, timeSpan.hours, "Hours")
		assertEquals(factor * 3, timeSpan.minutes, "Minutes")
		assertEquals(factor * 2, timeSpan.seconds, "Seconds")
		assertEquals(factor * 1, timeSpan.milliseconds, "Milliseconds")
		assertEquals(factor * NANOS, timeSpan.nanoseconds, "Nanoseconds")
		
		assertEquals(factor * 5L, timeSpan.inWholeDays(), "Total whole days")
		assertEquals(factor * 124L, timeSpan.inWholeHours(), "Total whole hours")
		assertEquals(factor * 7443L, timeSpan.inWholeMinutes(), "Total whole minutes")
		assertEquals(factor * SECONDS, timeSpan.inWholeSeconds(), "Total whole seconds")
		// TODO: Replace `orThrow` with `assertIsSuccessLong` when available
		assertEquals(factor * 446582001L, timeSpan.inWholeMilliseconds().orThrow(), "Total whole milliseconds")
		assertEquals(factor * 446582001000L, timeSpan.inWholeMicroseconds().orThrow(), "Total whole microseconds")
		assertEquals(factor * 446582001000001L, timeSpan.getInWholeNanoseconds().orThrow(), "Total whole nanoseconds")
		
		assertEquals(factor * 5.16, timeSpan.inDays(), 0.01, "Total days")
		assertEquals(factor * 124.05, timeSpan.inHours(), 0.01, "Total hours")
		assertEquals(factor * 7443.03, timeSpan.inMinutes(), 0.01, "Total minutes")
		assertEquals(factor * 446582.001, timeSpan.inSeconds(), 0.0001, "Total seconds")
		assertEquals(factor * 4.46582001000001E8, timeSpan.inMilliseconds(), "Total milliseconds")
		assertEquals(factor * 4.46582001000001E11, timeSpan.inMicroseconds(), "Total microseconds")
		assertEquals(factor * 4.46582001000001E14, timeSpan.inNanoseconds(), "Total nanoseconds")
	}
	
	@Test
	 fun testComparison1() {
		
		assertEquals(0, createTimeSpan().compareTo(createTimeSpan()))
	}
	
	@Test
	 fun testComparison2() {
		
		assertEquals(1, createTimeSpan().compareTo(createTimeSpan().negate().orThrow()))
	}
	
	@Test
	 fun testComparison3() {
		
		assertEquals(-1, createTimeSpan().negate().orThrow().compareTo(createTimeSpan()))
	}
	
	@Test
	 fun testComparison4() {
		
		assertEquals(0, createTimeSpan().negate().orThrow().compareTo(createTimeSpan().negate().orThrow()))
	}
	
	@Test
	 fun testRawUnit1() {
		
		assertComponents(SECONDS, NANOS, createTimeSpan())
	}
	
	@Test
	 fun testRawUnit2() {
		
		assertComponents(-SECONDS, -NANOS, -createTimeSpan())
	}
	
	@Test
	 fun testZeroValue() {
		
		assertComponents(0L, 0, TimeSpan.ZERO)
	}
	
	@Test
	 fun testMaxValue() {
		
		assertComponents(Long.MAX_VALUE, NANOSECONDS_MAX_VALUE, TimeSpan.MAX_VALUE)
	}
	
	/**
	 * Checks [TimeSpan.MIN_VALUE] and also covers the adjustment of negative seconds with zero nanos.
	 */
	@Test
	 fun testMinValueAndNegativeSeconds() {
		
		assertComponents(Long.MIN_VALUE, 0, TimeSpan.MIN_VALUE)
	}
	
	@Test
	 fun testOverflow1() {
		
		assertIs<Failure<*>>(TimeSpan.ofSeconds(Long.MAX_VALUE, NANOSECONDS_PER_SECOND))
	}
	
	@Test
	 fun testOverflow2() {
		
		assertIs<Failure<*>>(TimeSpan.ofSeconds(Long.MIN_VALUE, -1))
	}
	
	@Test
	 fun testPlus() {
		
		assertComponents(2L * SECONDS, 2 * NANOS, createTimeSpan() + createTimeSpan())
	}
	
	@Test
	 fun testMinus1() {
		
		assertComponents(0L, 0, createTimeSpan() - createTimeSpan())
	}
	
	@Test
	 fun testMinus2() {
		
		assertComponents(-2L * SECONDS, -2 * NANOS, enclose { createTimeSpan().negate() - createTimeSpan() })
	}
	
	@Test
	 fun testTimes1() {
		
		assertComponents(0L, 0, createTimeSpan() * 0L)
	}
	
	@Test
	 fun testTimes2() {
		
		assertComponents(SECONDS, NANOS, createTimeSpan() * 1L)
	}
	
	@Test
	 fun testTimes3() {
		
		assertComponents(2L * SECONDS, 2 * NANOS, createTimeSpan() * 2L)
	}
	
	@Test
	 fun testTimes4() {
		
		assertComponents(-2L * SECONDS, -2 * NANOS, enclose { createTimeSpan().negate() * 2L })
	}
	
	@Test
	 fun testDiv1() {
		
		assertComponents(SECONDS, NANOS, createTimeSpan() / 1L)
	}
	
	@Test
	 fun testDiv2() {
		
		assertComponents(SECONDS / 2L, NANOS / 2, createTimeSpan() / 2L)
	}
	
	@Test
	 fun testDiv3() {
		
		assertComponents(SECONDS / -2L, NANOS / -2, createTimeSpan() / -2L)
	}
	
	@Test
	 fun testDiv4() {
		
		assertIs<Failure<*>>(createTimeSpan() / 0L )
	}
	
	@Test
	 fun testAbsoluteValue1() {
		
		assertComponents(SECONDS, NANOS, createTimeSpan().absoluteValue())
	}
	
	@Test
	 fun testAbsoluteValue2() {
		
		assertComponents(SECONDS, NANOS, enclose { createTimeSpan().negate().absoluteValue() })
	}
	
	@Test
	 fun testIsZero1() {
		
		assertTrue(TimeSpan.ofSeconds(0L).isZero)
	}
	
	@Test
	 fun testIsZero2() {
		
		assertFalse(createTimeSpan().isZero)
	}
	
	@Test
	 fun testIsNegative1() {
		
		assertFalse(createTimeSpan().isNegative)
	}
	
	@Test
	 fun testIsNegative2() {
		
		assertFalse(TimeSpan.ofSeconds(0L).isNegative)
	}
	
	@Test
	 fun testIsNegative3() {
		
		assertTrue(createTimeSpan().negate().expectSuccess().isNegative)
	}
	
	@Test
	 fun testToStringPositive() {
		
		assertEquals(STRING_POSITIVE_NANOS, createTimeSpan().toString())
	}
	
	@Test
	 fun testToStringNegative() {
		
		assertEquals(STRING_NEGATIVE_NANOS, createTimeSpan().negate().expectSuccess().toString())
	}
	
	@Test
	 fun testParsePositiveNanos() {
		
		assertSuccessEquals(createTimeSpan(), TimeSpan.parse(STRING_POSITIVE_NANOS))
	}
	
	@Test
	 fun testParseNegativeNanos() {
		
		assertConcludeEquals(createTimeSpan().negate(), TimeSpan.parse(STRING_NEGATIVE_NANOS))
	}
	
	@Test
	 fun testOfNanoseconds() {
		
		assertComponents(2L, 500_050_005, TimeSpan.ofNanoseconds(2_500_050_005L))
	}
	
	@Test
	 fun testOfMicroseconds() {
		
		assertComponents(2L, 500_005_000, TimeSpan.ofMicroseconds(2_500_005L))
	}
	
	@Test
	fun testOfMilliseconds() {
		
		assertComponents(2L, 505_000_000, TimeSpan.ofMilliseconds(2_505L))
	}
	
	private companion object {
		
		private const val SECONDS = 446_582L
		private const val NANOS = 1_000_001
		
		private const val STRING_POSITIVE_NANOS = "PT124H3M2.001000001S"
		private const val STRING_NEGATIVE_NANOS = "PT-124H-3M-2.001000001S"
		
		@Pure
		private fun createTimeSpan() =
			TimeSpan.ofSeconds(SECONDS, NANOS.toLong()).orThrow { "Failed creating TimeSpan in TimeSpanTest" }
		
		/**
		 * Asserts that the expected seconds and nanoseconds are equal to the components of the given [TimeSpan].
		 */
		private fun assertComponents(
			expectedSeconds: Long,
			expectedNanoseconds: Int,
			actualTimeSpan: Conclude<TimeSpan, *>
		) = assertComponents(expectedSeconds, expectedNanoseconds, assertIsSuccess(actualTimeSpan, "Timespan"))
		
		/**
		 * Asserts that the expected seconds and nanoseconds are equal to the components of the given [TimeSpan].
		 */
		private fun assertComponents(expectedSeconds: Long, expectedNanoseconds: Int, actualTimeSpan: TimeSpan) {
			
//			println(actualTimeSpan)
			
			val (actualSeconds, actualNanoseconds) = actualTimeSpan
			
			assertEquals(expectedSeconds, actualSeconds, "Component Seconds")
			assertEquals(expectedNanoseconds, actualNanoseconds, "Component Nanoseconds")
		}
	}
}
