/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.orThrow
import cc.persch.stardust.datetime.ZoneIds
import cc.persch.stardustx.datetime.timespans.units.days
import cc.persch.stardustx.datetime.timespans.units.hours
import cc.persch.stardustx.test.assertIsFailure
import cc.persch.stardustx.test.assertIsFailureOf
import cc.persch.stardustx.test.assertSuccessEquals
import java.time.DateTimeException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals

class DateTest {

	@Test
	fun equalsTest() {
		
		assertEquals(createDate(), createDate())
	}

	@Test
	fun equalsNotTest() {
		
		assertNotEquals(createDate(day = 18), createDate(day = 19))
	}
	
	@Test
	fun compareTest1() {
		
		assertEquals(0, createDate().compareTo(createDate()))
	}
	
	@Test
	fun compareTest2() {
		
		assertEquals(-1, createDate(day = 18).compareTo(createDate(day = 19)))
	}
	
	@Test
	fun compareTest3() {
		
		assertEquals(1, createDate(day = 19).compareTo(createDate(day = 18)))
	}
	
	@Test
	fun plusTimeSpanTest1() {
		
		assertSuccessEquals(createDate(day = 19), createDate(day = 18) + 1.days.orThrow())
	}
	
	@Test
	fun plusTimeSpanTest2() {
		
		assertIsFailure(createDate() + 1.hours.orThrow())
	}
	
	@Test
	fun minusTimeSpanTest1() {
		
		assertSuccessEquals(createDate(day = 18), createDate(day = 19) - 1.days.orThrow())
	}
	
	@Test
	fun minusTimeSpanTest2() {
		
		assertIsFailure(createDate() - 1.hours.orThrow())
	}
	
	@Test
	fun minusDateTimeTest1() {
		
		assertSuccessEquals(1.days.orThrow(), createDate(day = 19) - createDate(day = 18))
	}
	
	@Test
	fun minusDateTimeTest2() {
		
		assertSuccessEquals((-1).days.orThrow(), createDate(day = 18) - createDate(day = 19))
	}
	
	@Test
	fun plusDaysTest() {
		
		assertSuccessEquals(createDate(day = 19), createDate(day = 18) plusDays 1)
	}
	
	@Test
	fun minusDaysTest() {
		
		assertSuccessEquals(createDate(day = 18), createDate(day = 19) minusDays 1)
	}
	
	@Test
	fun betweenTest1() {
		
		assertSuccessEquals((-1).days.orThrow(), createDate(day = 19) between createDate(day = 18))
	}
	
	@Test
	fun betweenTest2() {
		
		assertSuccessEquals(1.days.orThrow(), createDate(day = 18) between createDate(day = 19))
	}
	
	@Test
	fun toStringTest() {
		
		assertEquals(DATE_STRING, createDate().toString())
	}
	
	@Test
	fun parseTest() {
		
		assertSuccessEquals(createDate(), Date.parse(DATE_STRING))
	}
	
	@Test
	fun toDateTimeTest() {
		
		assertEquals(DateTime.of(YEAR, MONTH, DAY, 0, 0, 0, BERLIN).orThrow(), createDate().toDateTime(BERLIN))
	}
	
	@Test
	fun toUtcTest() {
		
		assertEquals(DateTime.of(YEAR, MONTH, DAY, 0, 0, 0, ZoneIds.utc).orThrow(), createDate().toUtcDateTime())
	}
	
	private companion object {
		
		private const val YEAR = 2023
		private const val MONTH = 7
		private const val DAY = 31
		
		private val BERLIN = ZoneIds.europe_berlin
		
		private const val DATE_STRING = "2023-07-31"
		
		@Pure
		private fun createDate(
			year: Int = YEAR,
			month: Int = MONTH,
			day: Int = DAY,
		) = Date.of(year, month, day).orThrow { "Failed creating Date in DateTest." }
	}
}
