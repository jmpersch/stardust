/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.datetime.serializers

import cc.persch.stardust.conclude.orThrow
import cc.persch.stardust.datetime.ZoneIds
import cc.persch.stardustx.datetime.DateTime
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.intellij.lang.annotations.Language
import kotlin.test.Test
import kotlin.test.assertEquals

class DateTimeSerializerTest {
	
	@Test
	fun encodeTest() {
		
		assertEquals(JSON_DATE_TIME_STRING, Json.encodeToString(DATE_TIME))
	}

	@Test
	fun decodeTest() {
		
		assertEquals(DATE_TIME, Json.decodeFromString<DateTime>(JSON_DATE_TIME_STRING))
	}

	companion object {

		@Language("JSON")
		private const val JSON_DATE_TIME_STRING = "\"2023-12-30T15:16:17.123456789+01:00[Europe/Berlin]\""

		private val DATE_TIME = DateTime.of(2023, 12, 30, 15, 16, 17, 123456789, ZoneIds.europe_berlin) orThrow {
			
			"Failed creating DateTime for DateTimeSerializerTest."
		}
	}
}
