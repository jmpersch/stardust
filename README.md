    _______ _______ _______  ______ ______  _     _ _______ _______
    |______    |    |_____| |_____/ |     \ |     | |______    |   
    ______|    |    |     | |    \_ |_____/ |_____| ______|    |   
                                                                   

Stardust is a powerful extension to Kotlin's standard API.
With version 6, Stardust introduces new programming concepts concerning the handling of exceptions and results
with the goal to make software more reliable.

This project is set up as a multi-platform project targeting the JVM and JavaScript platforms.
The Gradle build system is used to compile the code and to create the artifacts.

## Project Structure

The project consists of the core module and several extension modules.

### stardust

The `stardust` module contains the core features of the Stardust library.

### stardustx

The `stardustx` modules contain extensions to the Stardust library.

* `stardustx-cehc`: Utils for comparison, equality, and hash code computation
* `stardustx-collections`: Mutable collections and unmodifiable collection views
* `stardustx-collections-immutable`: Immutable and persistent collections
* `stardustx-datetime`: Date and time
* `stardustx-test`: Testing utilities
* `stardustx-text`: Text utilities
* `stardustx-uuid`: UUID utilities

## Include Stardust into Your Software Project

```kotlin
repositories {
	
    // The URL to the Maven package registry to retrieve the artifacts of the Stardust library.
    // Find the GitLab repository under: <https://gitlab.com/jmpersch/registry>
    maven("https://gitlab.com/api/v4/projects/41417498/packages/maven")
}

dependencies {
	
    // Main module
    implementation("cc.persch:stardust-jvm:$stardustVersion")
	
    // Extension modules
    implementation("cc.persch:stardustx-cehc-jvm:$stardustVersion")
    implementation("cc.persch:stardustx-collections-jvm:$stardustVersion")
    implementation("cc.persch:stardustx-collections-immutable-jvm:$stardustVersion")
    implementation("cc.persch:stardustx-datetime-jvm:$stardustVersion")
    implementation("cc.persch:stardustx-test-jvm:$stardustVersion")
    implementation("cc.persch:stardustx-text-jvm:$stardustVersion")
    implementation("cc.persch:stardustx-uuid-jvm:$stardustVersion")
}

kotlin {
    
    compilerOptions {
        
        // Enable experimental context receivers to use enclosed overloads.
        freeCompilerArgs.add("-Xcontext-receivers")
    }
}
```

## Build and Test the Project

### Build and run tests as Gradle task in IntelliJ IDEA

```
clean build testClasses test --tests "*" --continue
```

*(Note: The `--tests "*"` argument forces showing up IntelliJ’s test window.)*

### Build and run tests via the Gradle wrapper in the terminal

```sh
./gradlew clean build testClasses test --continue
```

## Copyright and License

**Copyright © 2008–2025 [Jan Martin Persch](https://persch.cc/)**

This project is licensed under the terms of the [Apache License, Version 2.0](LICENSE.txt).
