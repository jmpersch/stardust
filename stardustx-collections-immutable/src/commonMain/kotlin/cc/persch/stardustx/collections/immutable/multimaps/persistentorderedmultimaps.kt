/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.multimaps

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentMapOf

private val EMPTY_PERSISTENT_ORDERED_MULTIMAP = PersistentOrderedMultiMap(persistentMapOf<Any?, PersistentSet<Any?>>())

@Suppress("UNCHECKED_CAST")
@Cached
@Pure
public fun <K, V> emptyPersistentMultiMap(): PersistentMultiMap<K, V> =
	EMPTY_PERSISTENT_ORDERED_MULTIMAP as PersistentMultiMap<K, V>

@Cached
@Pure
public fun <K, V> persistentMultiMapOf(): PersistentMultiMap<K, V> = emptyPersistentMultiMap()


@Pure
public fun <K, V> persistentMultiMapOf(key: K, value: V): PersistentMultiMap<K, V> =
	emptyPersistentMultiMap<K, V>().put(key, value)

@Pure
public fun <K, V> persistentMultiMapOf(key: K, values: Iterable<V>): PersistentMultiMap<K, V> =
	emptyPersistentMultiMap<K, V>().putAll(key, values)
