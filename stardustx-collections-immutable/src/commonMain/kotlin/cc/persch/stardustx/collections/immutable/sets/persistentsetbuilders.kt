/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.sets

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.mutate
import kotlinx.collections.immutable.persistentHashSetOf
import kotlinx.collections.immutable.persistentSetOf
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a persistent ordered set using the given [builderAction].
 * 
 * @since 5.0
 * @see persistentSetOf
 */
@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
@Pure
public inline fun <E> buildPersistentSet(
	@BuilderInference builderAction: MutableSet<E>.() -> Unit
) : PersistentSet<E> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return persistentSetOf<E>().mutate(builderAction)
}

/**
 * Creates a persistent hash set using the given [builderAction].
 *
 * @since 5.0
 * @see persistentHashSetOf
 */
@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
@Pure
public inline fun <E> buildPersistentHashSet(
	@BuilderInference builderAction: MutableSet<E>.() -> Unit
) : PersistentSet<E> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return persistentHashSetOf<E>().mutate(builderAction)
}
