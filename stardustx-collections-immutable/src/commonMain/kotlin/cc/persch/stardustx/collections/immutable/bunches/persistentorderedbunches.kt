/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardustx.collections.bunches.Bunch
import cc.persch.stardustx.collections.bunches.MutableBunch
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates an empty [PersistentOrderedBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see orEmpty
 */
@Pure
public fun <K, V> emptyPersistentBunchOf(keyExtractor: (V) -> K): PersistentBunch<K, V> =
	PersistentOrderedBunch(keyExtractor)

/**
 * Creates an empty [PersistentBunch] using the given [keyExtractor].
 *
 * Better use [emptyPersistentBunchOf]
 *
 * @since 5.0
 * @see emptyPersistentBunchOf
 * @see orEmpty
 */
@Pure
public fun <K, V> persistentBunchOf(keyExtractor: (V) -> K): PersistentBunch<K, V> =
	emptyPersistentBunchOf(keyExtractor)

/**
 * Creates a singleton [PersistentBunch] using the given [keyExtractor] and [item].
 *
 * @since 5.0
 * @see buildPersistentBunch
 */
@Pure
public fun <K, V> persistentBunchOf(
	keyExtractor: (V) -> K,
	item: V
) : PersistentBunch<K, V> = emptyPersistentBunchOf(keyExtractor).put(item)

/**
 * Creates an [PersistentBunch] using the given [keyExtractor] and [items].
 *
 * @since 5.0
 * @see buildPersistentBunch
 */
@Pure
public fun <K, V> persistentBunchOf(
	keyExtractor: (V) -> K,
	vararg items: V
) : PersistentBunch<K, V> = emptyPersistentBunchOf(keyExtractor).putAll(items.asList())

/**
 * Returns a [PersistentBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see persistentBunchOf
 */
@Pure
public fun <K, V> Sequence<V>.toPersistentBunch(
	keyExtractor: (V) -> K
) : PersistentBunch<K, V> = asIterable().toPersistentBunch(keyExtractor)

/**
 * Creates a [PersistentBunch] from this collection.
 *
 * @since 5.0
 * @see persistentBunchOf
 */
@Pure
public fun <K, V> Bunch<K, V>.toPersistentBunch(
) : PersistentBunch<K, V> = (this as Iterable<V>).toPersistentBunch(keyExtractor)

/**
 * Creates a [PersistentBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see persistentBunchOf
 */
@Suppress("UNCHECKED_CAST")
@Pure
public fun <K, V> Iterable<V>.toPersistentBunch(
	keyExtractor: (V) -> K
) : PersistentBunch<K, V> = when(this) {
	
	is PersistentBunch<*, V> -> this as PersistentBunch<K, V>
	
	else -> buildPersistentBunch(keyExtractor) { putAll(this@toPersistentBunch) }
}

/**
 * Returns this; or an empty [PersistentOrderedBunch], if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentBunchOf
 */
@Pure
public fun <K, V> PersistentBunch<K, V>?.orEmpty(keyExtractor: (V) -> K): PersistentBunch<K, V> =
	this ?: emptyPersistentBunchOf(keyExtractor)
