/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.multimaps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardustx.collections.multimaps.MultiMap
import kotlinx.collections.immutable.ImmutableSet
import kotlinx.collections.immutable.PersistentSet

@ThreadSafe
public interface PersistentMultiMap<K, out V>: ImmutableMultiMap<K, V> {
	
	@Pure
	public fun put(key: K, value: @UnsafeVariance V): PersistentMultiMap<K, V>
	
	@Pure
	public fun putEmptyIfAbsent(key: K): PersistentMultiMap<K, V>
	
	@Pure
	public fun putAll(key: K, values: Iterable<@UnsafeVariance V>): PersistentMultiMap<K, V>
	
	@Pure
	public fun remove(key: K, prune: Boolean): PersistentMultiMap<K, V>
	
	@Pure
	public fun remove(key: K, value: @UnsafeVariance V, prune: Boolean): PersistentMultiMap<K, V>
	
	@Pure
	public fun pruneAll(): PersistentMultiMap<K, V>
	
	@Pure
	public fun clear(): PersistentMultiMap<K, V>
	
	@Pure
	public fun builder(): Builder<K, @UnsafeVariance V>
	
	public interface Builder<K, V>: MultiMap<K, V> {
		
		@ShouldUse
		public fun getOrPutEmpty(key: K): ImmutableSet<V>
		
		public fun put(key: K, value: V): Boolean
		
		public fun putEmptyIfAbsent(key: K): Boolean
		
		@Pure
		public fun putAll(key: K, values: Iterable<V>): Boolean
		
		public fun remove(key: K, prune: Boolean): Boolean
		
		public fun remove(key: K, value: V, prune: Boolean): Boolean
		
		public fun pruneAll()
		
		public fun clear()
		
		override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, PersistentSet<V>>>
		
		@Pure
        public fun build(): PersistentMultiMap<K, V>
    }
}
