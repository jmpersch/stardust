/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.lists

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.collections.lists.*
import cc.persch.stardustx.collections.multimaps.orEmpty
import kotlinx.collections.immutable.*

/**
 * Gets an empty persistent array list.
 *
 * @since 5.0
 * @see orEmpty
 * @see persistentListOf
 */
@Cached
@Pure
public fun <E> emptyPersistentList(): PersistentList<E> = persistentListOf()

@Pure
public fun <E> persistentListOf(element: E): PersistentList<E> = persistentListOf<E>().add(element)

/**
 * Returns a [PersistentList].
 *
 * @since 5.0
 * @see persistentListOf
 */
@Pure
public fun <E> List<E>.toPersistent(): PersistentList<E> = (this as Iterable<E>).toPersistentList()

/**
 * Returns this; or an empty immutable list, if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentList
 */
@Cached
@Pure
public fun <E> ImmutableList<E>?.orEmpty(): ImmutableList<E> = this ?: persistentListOf()


/**
 * Returns this; or an empty persistent list, if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentList
 */
@Cached
@Pure
public fun <E> PersistentList<E>?.orEmpty(): PersistentList<E> = this ?: persistentListOf()
