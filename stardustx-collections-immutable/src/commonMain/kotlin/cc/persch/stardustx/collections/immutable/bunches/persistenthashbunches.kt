/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardustx.collections.bunches.Bunch
import cc.persch.stardustx.collections.bunches.MutableBunch
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates an empty [PersistentHashBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see orEmpty
 */
@Pure
public fun <K, V> emptyPersistentHashBunchOf(keyExtractor: (V) -> K): PersistentBunch<K, V> =
	PersistentHashBunch(keyExtractor)

/**
 * Creates an empty [PersistentHashBunch] using the given [keyExtractor].
 *
 * Better use [emptyPersistentHashBunchOf]
 *
 * @since 5.0
 * @see emptyPersistentBunchOf
 * @see orEmpty
 */
@Pure
public fun <K, V> persistentHashBunchOf(keyExtractor: (V) -> K): PersistentBunch<K, V> =
	emptyPersistentHashBunchOf(keyExtractor)

/**
 * Creates a singleton [PersistentHashBunch] using the given [keyExtractor] and [item].
 *
 * @since 5.0
 * @see buildPersistentBunch
 */
@Pure
public fun <K, V> persistentHashBunchOf(
	keyExtractor: (V) -> K,
	item: V
) : PersistentBunch<K, V> = emptyPersistentHashBunchOf(keyExtractor).put(item)

/**
 * Creates an [PersistentHashBunch] using the given [keyExtractor] and [items].
 *
 * @since 5.0
 * @see buildPersistentHashBunch
 */
@Pure
public fun <K, V> persistentHashBunchOf(
	keyExtractor: (V) -> K,
	vararg items: V
) : PersistentBunch<K, V> = emptyPersistentHashBunchOf(keyExtractor).putAll(items.asList())

/**
 * Creates a [PersistentHashBunch] from this collection.
 *
 * @since 5.0
 * @see persistentHashBunchOf
 */
@Pure
public fun <K, V> Bunch<K, V>.toPersistentHashBunch(
) : PersistentBunch<K, V> = (this as Iterable<V>).toPersistentHashBunch(keyExtractor)

/**
 * Returns a [PersistentHashBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see persistentHashBunchOf
 */
@Pure
public fun <K, V> Sequence<V>.toPersistentHashBunch(
	keyExtractor: (V) -> K
) : PersistentBunch<K, V> = asIterable().toPersistentHashBunch(keyExtractor)

/**
 * Creates a [PersistentBunch] using the given [keyExtractor].
 *
 * @since 5.0
 * @see persistentHashBunchOf
 */
@Suppress("UNCHECKED_CAST")
@Pure
public fun <K, V> Iterable<V>.toPersistentHashBunch(
	keyExtractor: (V) -> K
) : PersistentBunch<K, V> = when(this) {
	
	is PersistentBunch<*, V> -> this as PersistentBunch<K, V>
	
	else -> buildPersistentHashBunch(keyExtractor) { putAll(this@toPersistentHashBunch) }
}

/**
 * Returns this; or an empty [PersistentHashBunch], if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentHashBunchOf
 */
@Pure
public fun <K, V> PersistentBunch<K, V>?.orEmptyHashBunch(keyExtractor: (V) -> K): PersistentBunch<K, V> =
	this ?: emptyPersistentHashBunchOf(keyExtractor)
