/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.sets

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import kotlinx.collections.immutable.*

/**
 * Returns an empty persistent ordered set.
 */
@Cached
@Pure
public fun <E> emptyPersistentSet(): PersistentSet<E> = persistentSetOf()

/**
 * Returns an empty persistent hash set.
 */
@Cached
@Pure
public fun <E> emptyPersistentHashSet(): PersistentSet<E> = persistentHashSetOf()

@Pure
public fun <E> persistentSetOf(element: E): PersistentSet<E> = persistentSetOf<E>().add(element)

@Pure
public fun <E> persistentHashSetOf(element: E): PersistentSet<E> = persistentHashSetOf<E>().add(element)

/**
 * Returns this; or an empty immutable ordered set, if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentSet
 */
@Cached
@Pure
public fun <E> ImmutableSet<E>?.orEmpty(): ImmutableSet<E> = this ?: persistentSetOf()

/**
 * Returns this; or an empty persistent ordered set, if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentSet
 */
@Cached
@Pure
public fun <E> PersistentSet<E>?.orEmpty(): PersistentSet<E> = this ?: persistentSetOf()

/**
 * Returns this; or an empty immutable hash set, if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentHashSet
 */
@Cached
@Pure
public fun <E> ImmutableSet<E>?.orEmptyHashSet(): ImmutableSet<E> = this ?: persistentHashSetOf()

/**
 * Returns this; or an empty persistent hash set, if this is `null`.
 *
 * @since 5.0
 * @see emptyPersistentHashSet
 */
@Cached
@Pure
public fun <E> PersistentSet<E>?.orEmptyHashSet(): PersistentSet<E> = this ?: persistentHashSetOf()
