/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardustx.collections.bunches.MutableBunch
import kotlinx.collections.immutable.PersistentSet

/**
 * Defines an interface for a *fully immutable* and *thread-safe* bunch implementation.
 *
 * **Important:** If you create a class that implements this interface, it must be *fully immutable* and *thread-safe*!
 * @param K The type of the keys of this collection—should be persistent!
 * @param V The type of the values of this collection.
 *
 * @since 5.0
 */
@ThreadSafe
public interface PersistentBunch<K, V>: ImmutableBunch<K, V>, PersistentSet<V> {
	
	@Pure
	public fun put(value: V): PersistentBunch<K, V>
	
	@Pure
	public fun putIfAbsent(value: V): PersistentBunch<K, V>
	
	@Pure
	public fun putAll(values: Iterable<V>): PersistentBunch<K, V>
	
	@Pure
	public fun putAllAbsent(values: Iterable<V>): PersistentBunch<K, V>
	
	@Pure
	public fun removeKey(key: K): PersistentBunch<K, V>
	
	@Pure
	public fun removeValue(value: V): PersistentBunch<K, V>
	
	@Pure
	public fun removeAllKeys(keys: Iterable<K>): PersistentBunch<K, V>
	
	@Pure
	public fun removeAllValues(values: Iterable<V>): PersistentBunch<K, V>
	
	@Pure
	public fun retainAllKeys(keys: Iterable<K>): PersistentBunch<K, V>
	
	@Pure
	public fun retainAllValues(values: Iterable<V>): PersistentBunch<K, V>
	
	@Pure
	public override fun clear(): PersistentBunch<K, V>
	
	// region PersistentSet
	
	@Pure
	override fun add(element: V): PersistentBunch<K, V> = put(element)
	
	@Pure
	override fun addAll(elements: Collection<V>): PersistentBunch<K, V> = putAll(elements)
	
	@Pure
	override fun remove(element: V): PersistentBunch<K, V> = removeValue(element)
	
	@Pure
	override fun removeAll(elements: Collection<V>): PersistentBunch<K, V> = removeAllValues(elements)
	
	@Pure
	override fun removeAll(predicate: (V) -> Boolean): PersistentBunch<K, V>
	
	@Pure
	override fun retainAll(elements: Collection<V>): PersistentBunch<K, V> = retainAllValues(elements)
	
	// endregion /PersistentSet/
	
	@Pure
	public override fun builder(): Builder<K, V>

	/**
	 * Defines a [PersistentBunch] builder.
	 */
	public interface Builder<K, V>: PersistentSet.Builder<V>, MutableBunch<K, V> {

		/**
		 * Returns a [PersistentBunch].
		 */
		@Pure
		public override fun build(): PersistentBunch<K, V>
	}
}
