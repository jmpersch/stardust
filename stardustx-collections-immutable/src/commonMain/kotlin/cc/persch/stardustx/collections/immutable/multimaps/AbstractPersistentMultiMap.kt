/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.multimaps

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardust.exerting
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.orSimpleNameOf
import cc.persch.stardust.text.rope.indenters.ropeIndented
import cc.persch.stardustx.collections.multimaps.AbstractMultiMap
import kotlinx.collections.immutable.ImmutableSet
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet

public abstract class AbstractPersistentMultiMap<K, out V>(
	override val map: PersistentMap<K, PersistentSet<V>>
) : AbstractMultiMap<K, @UnsafeVariance V>(), PersistentMultiMap<K, V> {
	
	// region Immutable Functions
	
	override val keys: ImmutableSet<K>
		get() = map.keys
	
	override fun asSequence(): Sequence<Map.Entry<K, ImmutableSet<V>>> = map.asSequence()
	
	override fun asValueSequence(): Sequence<V> = map.asSequence().flatMap(Map.Entry<K, Set<V>>::value)
	
	override fun get(key: K): ImmutableSet<V>? = map.get(key)
	
	override fun iterator(): Iterator<Map.Entry<K, ImmutableSet<V>>> = map.iterator()
	
	override fun builder(): PersistentMultiMap.Builder<K, @UnsafeVariance V> = Builder(this)
	
	override fun ropeIndented(builder: RopeBuilder, name: CharSequence?, bodyOption: BodyOption): Unit =
		map.ropeIndented(builder, name orSimpleNameOf this::class, bodyOption)
	
	// endregion /Immutable Functions/
	
	// region Mutable Functions
	
	@Cached
	@Pure
	protected abstract fun getEmptyPersistentMultiMap(): PersistentMultiMap<K, V>
	
	@Cached
	@Pure
	protected abstract fun getEmptyPersistentSet(): PersistentSet<V>
	
	@Pure
	protected abstract fun wrap(map: PersistentMap<K, PersistentSet<@UnsafeVariance V>>): AbstractPersistentMultiMap<K, V>
	
//	@Pure
//	private inline fun mutating(block: () -> PersistentMap<K, PersistentSet<V>>): PersistentMultiMap<K, V> =
//		block() pipe { if(it === map) this else wrap(it) }
	
	@Pure
	private inline fun building(block: (PersistentMultiMap.Builder<K, V>) -> Unit): PersistentMultiMap<K, V> =
		builder().exert(block).build()
	
	@Pure
	override fun put(key: K, value: @UnsafeVariance V): PersistentMultiMap<K, V> = building { it.put(key, value) }
	
	override fun putEmptyIfAbsent(key: K): PersistentMultiMap<K, V> = building { it.putEmptyIfAbsent(key) }
	
	@Pure
	override fun putAll(key: K, values: Iterable<@UnsafeVariance V>): PersistentMultiMap<K, V> =
		building { it.putAll(key, values) }
	
	override fun remove(key: K, prune: Boolean): PersistentMultiMap<K, V> = building { it.remove(key, prune) }
	
	override fun remove(key: K, value: @UnsafeVariance V, prune: Boolean): PersistentMultiMap<K, V> =
		building { it.remove(key, value, prune) }
	
	override fun pruneAll(): PersistentMultiMap<K, V> = building { it.pruneAll() }
	
	// FIXME: In AbstractPersistentBunch.empty returns wrong empty persistent type!
	
	override fun clear(): PersistentMultiMap<K, V> = getEmptyPersistentMultiMap()
	
	// endregion /Mutable Functions/
	
	public class Builder<K, V>(
		private val sourceMultiMap: AbstractPersistentMultiMap<K, V>
	) : AbstractMultiMap<K, V>(), PersistentMultiMap.Builder<K, V> {
		
		override val map: PersistentMap.Builder<K, PersistentSet<V>> = sourceMultiMap.map.builder()
		
		override val keys: MutableSet<K>
			get() = map.keys
		
		override val size: Int
			get() = map.size
		
		override fun asSequence(): Sequence<Map.Entry<K, ImmutableSet<V>>> = map.asSequence()
		
		override fun asValueSequence(): Sequence<V> = map.asSequence().flatMap(Map.Entry<K, PersistentSet<V>>::value)
		
		override fun containsKey(key: K): Boolean = map.containsKey(key)
		
		override fun containsKeyNonEmpty(key: K): Boolean = !map.get(key).isNullOrEmpty()
		
		override fun get(key: K): ImmutableSet<V>? = map.get(key)
		
		override fun clear(): Unit = map.clear()
		
		override fun getOrPutEmpty(key: K): ImmutableSet<V> = map.getOrPut(key, sourceMultiMap::getEmptyPersistentSet)
		
		override fun put(key: K, value: V): Boolean {
			
			val oldSet = map.get(key) ?: sourceMultiMap.getEmptyPersistentSet()
			
			val newSet = oldSet.add(value)
			
			if(newSet === oldSet)
				return false
			
			map.put(key, newSet)
			
			return true
		}
		
		override fun putEmptyIfAbsent(key: K): Boolean {
			
			if(map.containsKey(key))
				return false
			
			map.put(key, sourceMultiMap.getEmptyPersistentSet())
			
			return true
		}
		
		override fun putAll(key: K, values: Iterable<V>): Boolean {
			
			val oldSet = map.get(key)
			
			var newSet = oldSet ?: sourceMultiMap.getEmptyPersistentSet()
			
			newSet = when(values) {
				
				is Collection -> newSet.addAll(values)
				
				else -> newSet.builder().exerting { addAll(values) }.build()
			}
			
			if(newSet === oldSet)
				return false
			
			map.put(key, newSet)
			
			return true
		}
		
		override fun pruneAll() {
			
			val iterator = map.iterator()
			
			while(iterator.hasNext()) {
				
				val next = iterator.next()
				
				if(next.value.isEmpty())
					iterator.remove()
			}
		}
		
		override fun remove(key: K, prune: Boolean): Boolean {
			
			if(prune)
				return map.remove(key) != null
			
			val oldSet = map.get(key)
			
			if(oldSet.isNullOrEmpty())
				return false
			
			map.put(key, sourceMultiMap.getEmptyPersistentSet())
			
			return true
		}
		
		override fun remove(key: K, value: V, prune: Boolean): Boolean {
			
			val oldSet = map.get(key) ?: return false
			
			val newSet = oldSet.remove(value)
			
			if(newSet === oldSet)
				return false
			
			if(prune && oldSet.isEmpty())
				map.remove(key)
			
			map.put(key, newSet)
			
			return true
		}
		
		override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, PersistentSet<V>>> = map.iterator()
		
		override fun build(): PersistentMultiMap<K, V> {
			
			val sourceMultiMap = sourceMultiMap
			
			val newMap = map.build()
			
			return if(newMap === sourceMultiMap.map) sourceMultiMap else sourceMultiMap.wrap(newMap) 
		}
		
		override fun ropeIndented(builder: RopeBuilder, name: CharSequence?, bodyOption: BodyOption): Unit =
			map.ropeIndented(builder, name orSimpleNameOf this::class, bodyOption)
	}
}
