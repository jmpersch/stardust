/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.maps

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.exert
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentMapOf
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a persistent ordered map using the given [builderAction].
 *
 * @since 5.0
 * @see persistentMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildPersistentMap(
	@BuilderInference builderAction: MutableMap<K, V>.() -> Unit
) : PersistentMap<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return persistentMapOf<K, V>().builder().exert(builderAction).build()
}


/**
 * Creates a persistent hash map using the given [builderAction].
 *
 * @since 5.0
 * @see persistentMapOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildPersistentHashMap(
	@BuilderInference builderAction: MutableMap<K, V>.() -> Unit
) : PersistentMap<K, V> {
	
	contract { callsInPlace(builderAction, EXACTLY_ONCE) }
	
	return persistentHashMapOf<K, V>().builder().exert(builderAction).build()
}
