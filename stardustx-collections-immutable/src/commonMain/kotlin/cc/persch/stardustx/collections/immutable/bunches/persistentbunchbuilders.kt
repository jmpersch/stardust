/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.exert
import cc.persch.stardustx.collections.bunches.MutableBunch
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a [PersistentBunch] using the given [keyExtractor] and [builder].
 *
 * @since 5.0
 * @see persistentBunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildPersistentBunch(
	noinline keyExtractor: (V) -> K,
	@BuilderInference builder: MutableBunch<K, V>.() -> Unit
) : PersistentBunch<K, V> {
	
	contract { callsInPlace(builder, EXACTLY_ONCE) }
	
	return emptyPersistentBunchOf(keyExtractor).builder().exert(builder).build()
}

/**
 * Creates a [PersistentHashBunch] using the given [keyExtractor] and [builder].
 *
 * @since 5.0
 * @see persistentHashBunchOf
 */
@Pure
@TypeSafeBuilder
public inline fun <K, V> buildPersistentHashBunch(
	noinline keyExtractor: (V) -> K,
	@BuilderInference builder: MutableBunch<K, V>.() -> Unit
) : PersistentBunch<K, V> {
	
	contract { callsInPlace(builder, EXACTLY_ONCE) }
	
	return emptyPersistentHashBunchOf(keyExtractor).builder().exert(builder).build()
}
