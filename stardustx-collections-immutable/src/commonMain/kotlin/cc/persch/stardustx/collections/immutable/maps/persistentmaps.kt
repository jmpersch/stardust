/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.maps

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import kotlinx.collections.immutable.*
import kotlinx.collections.immutable.persistentHashMapOf
import kotlinx.collections.immutable.persistentMapOf

/**
 * Puts the given [value] into this map, if the given [key] is not already associated with a value
 * or the value is `null`.
 *
 * **Important:** Note that the operation is not guaranteed to be atomic if the map is being modified concurrently.
 *
 * @param key The key.
 * @param value The value.
 * @return The current value, if the key already exists; otherwise, `null`, if the given value was put into this map.
 *
 * @since 4.0
 */
internal expect fun <K, V> MutableMap<K, V>.__putIfAbsent(key: K, value: V): V?

/**
 * @since 6.0
 */
internal expect fun <K, V> PersistentMap<K, V>.__putIfAbsent(key: K, value: V): PersistentMap<K, V>

/**
 * Removes the entry for the given [key] only if it is currently mapped to the given [value] (checked with equality).
 *
 * **Important:** Note that the operation is not guaranteed to be atomic if the map is being modified concurrently.
 *
 * @since 4.0
 */
internal expect fun <K, V> MutableMap<K, V>.__remove(key: K, value: V): Boolean

/**
 * Returns an empty persistent map.
 */
@Cached
@Pure
public fun <K, V> emptyPersistentMap(): PersistentMap<K, V> = persistentMapOf()

/**
 * Returns an empty persistent hash map.
 */
@Cached
@Pure
public fun <K, V> emptyPersistentHashMap(): PersistentMap<K, V> = persistentHashMapOf()

@Pure
public fun <K, V> persistentMapOf(key: K, value: V): PersistentMap<K, V> =
	persistentMapOf<K, V>().put(key, value)

@Pure
public fun <K, V> persistentHashMapOf(key: K, value: V): PersistentMap<K, V> =
	persistentHashMapOf<K, V>().put(key, value)

@Pure
public fun <K, V> persistentMapOf(vararg entries: Map.Entry<K, V>): PersistentMap<K, V> =
	persistentMapOf<K,V>().mutate { map -> entries.forEach { (k, v) -> map.put(k, v) } }

@Pure
public fun <K, V> persistentHashMapOf(vararg entries: Map.Entry<K, V>): PersistentMap<K, V> =
	persistentHashMapOf<K,V>().mutate { map -> entries.forEach { (k, v) -> map.put(k, v) } }
	
/**
 * Returns this; or an empty immutable ordered map, if this is `null`.
 *
 * @since 5.0
 */
@Pure
@Cached
public fun <K, V> ImmutableMap<K, V>?.orEmpty(): ImmutableMap<K, V> = this ?: persistentMapOf()

/**
 * Returns this; or an empty persistent ordered map, if this is `null`.
 *
 * @since 5.0
 */
@Pure
@Cached
public fun <K, V> PersistentMap<K, V>?.orEmpty(): PersistentMap<K, V> = this ?: persistentMapOf()

/**
 * Returns this; or an empty immutable hash map, if this is `null`.
 *
 * @since 5.0
 */
@Pure
@Cached
public fun <K, V> ImmutableMap<K, V>?.orEmptyHashMap(): ImmutableMap<K, V> = this ?: persistentHashMapOf()


/**
 * Returns this; or an empty persistent hash map, if this is `null`.
 *
 * @since 5.0
 */
@Pure
@Cached
public fun <K, V> PersistentMap<K, V>?.orEmptyHashMap(): PersistentMap<K, V> = this ?: persistentHashMapOf()
