/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.bunches

import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentMapOf

/**
 * Defines the [PersistentOrderedBunch] class.
 *
 */
public class PersistentOrderedBunch<K, V> internal constructor(
	keyExtractor: (V) -> K,
	map: PersistentMap<K, V> = persistentMapOf()
) : AbstractPersistentBunch<K, V>(keyExtractor, map), PersistentBunch<K, V> {
	
	override fun getEmptyPersistentMap(): PersistentMap<K, V> = persistentMapOf()
	
	override fun wrap(map: PersistentMap<K, V>): AbstractPersistentBunch<K, V> =
		PersistentOrderedBunch(keyExtractor, map)
	
	override fun builder(): PersistentBunch.Builder<K, V> = Builder(this)
}
