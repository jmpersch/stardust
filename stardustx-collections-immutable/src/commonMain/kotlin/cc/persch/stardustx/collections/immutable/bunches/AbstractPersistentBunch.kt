/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.bunches

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.pipe
import cc.persch.stardustx.collections.bunches.*
import cc.persch.stardustx.collections.immutable.maps.__putIfAbsent
import cc.persch.stardustx.collections.immutable.maps.__remove
import cc.persch.stardustx.collections.iterators.UnmodifiableIteratorView
import cc.persch.stardustx.collections.iterators.asImmutable
import cc.persch.stardustx.collections.sets.UnmodifiableSetView
import cc.persch.stardustx.collections.sets.UnmodifiableSetWrapper
import kotlinx.collections.immutable.*
import kotlinx.serialization.Transient

/**
 * Defines the [AbstractPersistentBunch] class.
 *
 */
public sealed class AbstractPersistentBunch<K, V>(
	override val keyExtractor: (V) -> K,
	override val map: PersistentMap<K, V>
) : AbstractBunch<K, V>(), PersistentBunch<K, V> {
	
	override val keys: ImmutableSet<K>
		get() = map.keys

	override fun iterator(): UnmodifiableIteratorView<V> = map.values.iterator().asImmutable()
	
	@Pure
	protected abstract fun getEmptyPersistentMap(): PersistentMap<K, V>
	
	@Pure
	protected abstract fun wrap(map: PersistentMap<K, V>): AbstractPersistentBunch<K, V>
	
	private inline fun mutating(block: () -> PersistentMap<K, V>): PersistentBunch<K, V> =
		block() pipe { if(it === map) this else wrap(it) }
	
	override fun put(value: V): PersistentBunch<K, V> = mutating { map.put(keyExtractor(value), value) }
	
	override fun putIfAbsent(value: V): PersistentBunch<K, V> = mutating {
		
		keyExtractor(value) pipe { key -> if(map.containsKey(key)) map else map.put(key, value) }
	}
	
	override fun putAll(values: Iterable<V>) : PersistentBunch<K, V> = mutating { map.mutate { map ->
		
		values.forEach { map.put(keyExtractor(it), it) }
	}}
	
	override fun putAllAbsent(values: Iterable<V>): PersistentBunch<K, V> = mutating { map.mutate { map ->
		
		values.forEach { keyExtractor(it) pipe { key -> if(!map.containsKey(key)) map.put(key, it) } }
	}}
	
	override fun removeKey(key: K): PersistentBunch<K, V> = mutating { map.remove(key) }
	
	override fun removeValue(value: V): PersistentBunch<K, V> = mutating { map.remove(keyExtractor(value), value) }
	
	override fun removeAllKeys(keys: Iterable<K>): PersistentBunch<K, V> = mutating {
		
		map.mutate { keys.forEach(it::remove) }
	}
	
	override fun removeAllValues(values: Iterable<V>): PersistentBunch<K, V> = mutating { map.mutate { map ->
		
		values.forEach { map.__remove(keyExtractor(it), it) }
	}}
	
	override fun removeAll(predicate: (V) -> Boolean): PersistentBunch<K, V> = mutating { map.mutate { map ->
		
		val iterator = map.values.iterator()
		
		while(iterator.hasNext()) {
			
			val element = iterator.next()
			
			if(predicate(element))
				iterator.remove()
		}
	}}
	
	override fun retainAllKeys(keys: Iterable<K>): PersistentBunch<K, V> = mutating {
		
		map.mutate { it.keys.retainAll(keys.toSet()) }
	}
	
	override fun retainAllValues(values: Iterable<V>): PersistentBunch<K, V> = mutating {
		
		map.mutate { it.values.retainAll(values.toSet()) }
	}
	
	override fun clear(): PersistentBunch<K, V> = if(isEmpty()) this else emptyPersistentBunchOf(keyExtractor)
	
	/**
	 * Defines a [AbstractPersistentBunch] builder.
	 */
	public class Builder<K, V>(
		private val sourceBunch: AbstractPersistentBunch<K, V>,
	) : PersistentBunch.Builder<K, V> {
		
		override val keyExtractor: (V) -> K = sourceBunch.keyExtractor
		
		private val targetBuilder = sourceBunch.map.builder()
		
		override val size: Int
			get() = targetBuilder.size
		
		@Transient
		private var _keys: UnmodifiableSetWrapper<K>? = null
		
		override val keys: UnmodifiableSetView<K>
			get() = this::_keys.getOrInitialize { UnmodifiableSetWrapper(targetBuilder.keys) }
		
		override fun isEmpty(): Boolean = targetBuilder.isEmpty()
		
		override fun clear(): Unit = targetBuilder.clear()
		
		override fun iterator(): MutableIterator<V> = targetBuilder.values.iterator()
		
		override fun put(value: V): V? = targetBuilder.put(keyExtractor(value), value)
		
		override fun putIfAbsent(value: V): V? = targetBuilder.__putIfAbsent(keyExtractor(value), value)
		
		override fun removeKey(key: K): Boolean = targetBuilder.remove(key) != null
		
		override fun removeValue(value: V): Boolean = targetBuilder.__remove(keyExtractor(value), value)
		
		override fun removeByKeyOf(value: V): Boolean = removeKey(keyExtractor(value))
		
		override fun get(key: K): V? = targetBuilder.get(key)
		
		override fun containsKey(key: K): Boolean = targetBuilder.containsKey(key)
		
		override fun containsValue(value: V): Boolean = targetBuilder.containsValue(value)
		
		override fun build(): PersistentBunch<K, V> {
			
			val sourceBunch = sourceBunch
			
			val targetMap = targetBuilder.build()
			
			return if(targetMap === sourceBunch.map) sourceBunch
			else sourceBunch.wrap(targetMap)
		}
		
		// region MutableSet
		
		override fun add(element: V): Boolean = put(element) !== element
		
		override fun addAll(elements: Collection<V>): Boolean = putAll(elements)
		
		override fun remove(element: V): Boolean = removeValue(element)
		
		override fun removeAll(elements: Collection<V>): Boolean = removeAllValues(elements)
		
		override fun retainAll(elements: Collection<V>): Boolean = retainAllValues(elements)
		
		// endregion /MutableSet/
	}
}
