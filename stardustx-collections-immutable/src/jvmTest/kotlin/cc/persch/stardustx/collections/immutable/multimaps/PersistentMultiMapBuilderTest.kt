/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.collections.immutable.multimaps

import cc.persch.stardust.performing
import cc.persch.stardustx.collections.multimaps.MultiMap
import cc.persch.stardustx.collections.multimaps.buildMutableMultiMap
import cc.persch.stardustx.collections.multimaps.emptyMultiMap
import cc.persch.stardustx.collections.multimaps.mutableMultiMapOf
import kotlinx.collections.immutable.persistentSetOf
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class PersistentMultiMapBuilderTest {
	
	@Test
	fun testPut1() {
		
		val m = mutableMultiMapOf("Foo" to "Bar")
		val p = buildPersistentMultiMap { put("Foo", "Bar") }
		
		assertEquals(m.size, p.size)
		assertTrue(p.isNotEmpty())
		assertEquals<MultiMap<*, *>>(m, p)
	}
	
	@Test
	fun testPut2() {
		
		val m = buildMutableMultiMap { putAll("Foo", listOf("Bar", "Baz")) }
		val p = buildPersistentMultiMap {
			
			put("Foo", "Bar")
			put("Foo", "Baz")
		}
		
		assertEquals(m.size, p.size)
		assertEquals<MultiMap<*, *>>(m, p)
	}
	
	@Test
	fun testPutAll() {
		
		val key = "Foo"
		val values = listOf("Bar", "Baz", "Bax")
		
		val m = buildMutableMultiMap { putAll(key, values) }
		val p = buildPersistentMultiMap { putAll(key, values) }
		
		assertEquals(m.size, p.size)
		assertEquals<MultiMap<*, *>>(m, p)
	}
	
	@Test
	fun testMutableIteratorRemove1() {
		
		val m = buildMutableMultiMap { put("Bax", "Baz") }
		
		val p = buildPersistentMultiMap {
			
			put("Foo", "Bar")
			put("Bax", "Baz")
			
			iterator() performing {
				
				next()
				remove()
			}
		}
		
		assertEquals(m.size, p.size)
		assertEquals<MultiMap<*, *>>(m, p)
	}
	
	@Test
	fun testMutableIteratorRemove2() {
		
		val p = buildPersistentMultiMap {
			
			put("Foo", "Bar")
			put("Bax", "Baz")
			
			iterator() performing {
				
				next()
				remove()
				
				next()
				remove()
			}
		}
		
		assertTrue(p.isEmpty())
		assertEquals(emptyMultiMap(), p)
	}
	
	@Test
	fun testMutableIteratorExchangeValue() {
		
		val m = buildMutableMultiMap {
			
			put("Foo", "Exchanged Bar")
			put("Bax", "Exchanged Baz")
		}
		
		val p = buildPersistentMultiMap {
			
			put("Foo", "Bar")
			put("Bax", "Baz")
			
			iterator() performing {
				
				next().setValue(persistentSetOf("Exchanged Bar"))
				next().setValue(persistentSetOf("Exchanged Baz"))
			}
		}
		
		assertEquals(m.size, p.size)
		assertEquals<MultiMap<*, *>>(m, p)
	}
}
