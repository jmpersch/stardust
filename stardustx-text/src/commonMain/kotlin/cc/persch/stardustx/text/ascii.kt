/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text

import cc.persch.stardust.annotations.Pure

/**
 * Checks, whether a character is an ASCII letter (`a`–`z` or `A`–`Z`).
 *
 * @since 1.0
 */
@Pure
public fun Char.isAsciiLetter(): Boolean = this in 'a'..'z' || this in 'A'..'Z'

/**
 * Checks, whether a character is an ASCII digit (`0`–`9`).
 *
 * @since 1.0
 */
@Pure
public fun Char.isAsciiDigit(): Boolean = this in '0'..'9'

/**
 * Checks, whether a character is an ASCII hex digit (`0`–`9` and `A`–`F` resp. `a`–`f`).
 *
 * @since 1.0
 */
@Pure
public fun Char.isAsciiHexDigit(): Boolean = isAsciiDigit() || this in 'a'..'f' || this in 'A'..'F'

/**
 * Checks, whether a character is an ASCII letter or digit (`a–z`, `A–Z` or `0–9`).
 *
 * @since 1.0
 */
@Pure
public fun Char.isAsciiLetterOrDigit(): Boolean = isAsciiLetter() || isAsciiDigit()
