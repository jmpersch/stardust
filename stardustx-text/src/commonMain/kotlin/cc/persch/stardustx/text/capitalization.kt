/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.buildString
import cc.persch.stardust.text.localization.Locale
import cc.persch.stardust.text.localization.Locales
import cc.persch.stardust.text.subSequence

/**
 * Applies the given [transformer] to the first character of this sequence.
 * If this sequence is empty, an empty string is returned.
 *
 * @since 5.0
 */
@Pure
internal inline fun CharSequence.replaceFirstChar(transformer: (Char) -> CharSequence): String {
	
	if(isEmpty())
		return ""
	
	val transformedFirstChar = transformer(this@replaceFirstChar.get(0))
	
	if(length == 1)
		return transformedFirstChar.toString()
	
	return buildString(transformedFirstChar.length + length - 1) {
		
		append(transformedFirstChar)
		append(this@replaceFirstChar.subSequence(1))
	}
}

///**
// * Decapitalizes a string using the given locale, e.g. `"String"` becomes `"string"`.
// * The default locale is [Locales.default].
// *
// * Info: This function replaces the deprecated pendant of Kotlin's Standard Library.
// *
// * @since 5.0
// */
//@Pure
//public expect fun CharSequence.decapitalize(locale: Locale = Locales.default): String

///**
// * Capitalizes a string using the given locale, e.g. `"string"` becomes `"String"`.
// * The default locale is [Locales.default].
// *
// * Info: For the upper case character, the [titlecase] function is used.
// *
// * Info: This function replaces the deprecated pendant of Kotlin's Standard Library.
// *
// * @since 5.0
// */
//@Pure
//public expect fun CharSequence.capitalize(locale: Locale = Locales.default): String
