/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text.scanner

import cc.persch.stardust.getSimpleName
import cc.persch.stardust.require

/**
 * Defines a default [TextScanner] implementation.
 *
 * @since 1.0
 */
public open class DefaultTextScanner(override val input: CharSequence): TextScanner {
	
	override var index: Int = 0
		set(value) {
			
			require("value", value in 0..length)
			
			field = value
		}
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(other == null || this::class != other::class) return false
		
		other as DefaultTextScanner
		
		if(input != other.input) return false
		if(index != other.index) return false
		
		return true
	}
	
	override fun hashCode(): Int {
		
		var result = input.hashCode()
		result = 31 * result + index
		return result
	}
	
	override fun toString(): String = "${this::class.getSimpleName()}{length=$length, index=$index}"
}
