/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text.scanner

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.parsing.PatternSyntaxException

@Pure
internal expect fun compileRegex(pattern: String, options: Set<RegexOption>): Regex

@Pure
internal expect fun compileInverseRegex(pattern: String, options: Set<RegexOption>): Regex

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Lex
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates an [Lex] using the given [pattern].
 *
 * @throws PatternSyntaxException The given [pattern] is invalid.
 * @since 4.0
 */
@Pure
public fun lexOf(pattern: String): Lex = Lex(pattern)

/**
 * Creates an [Lex] using the given [pattern] and [options].
 *
 * @throws PatternSyntaxException The given [pattern] is invalid.
 * @since 4.0
 */
@Pure
public fun lexOf(pattern: String, options: Set<RegexOption>): Lex = Lex(pattern, options)

/**
 * Creates an [Lex] using the given [pattern] and [options].
 *
 * @throws PatternSyntaxException The given [pattern] is invalid.
 * @since 4.0
 */
@Pure
public fun lexOf(pattern: String, vararg options: RegexOption): Lex = Lex(pattern, options.toSet())

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// InverseLex
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates an [InverseLex] using the given [pattern].
 *
 * @throws PatternSyntaxException The given [pattern] is invalid.
 * @since 4.0
 */
@Pure
public fun inverseLexOf(pattern: String): InverseLex = InverseLex(pattern)

/**
 * Creates an [InverseLex] using the given [pattern] and [options].
 *
 * @throws PatternSyntaxException The given [pattern] is invalid.
 * @since 4.0
 */
@Pure
public fun inverseLexOf(pattern: String, options: Set<RegexOption>): InverseLex = InverseLex(pattern, options)

/**
 * Creates an [InverseLex] using the given [pattern] and [options].
 *
 * @throws PatternSyntaxException The given [pattern] is invalid.
 * @since 4.0
 */
@Pure
public fun inverseLexOf(pattern: String, vararg options: RegexOption): InverseLex =
	inverseLexOf(pattern, options.toSet())
