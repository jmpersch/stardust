/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text.scanner

import cc.persch.stardust.makeStringOf
import cc.persch.stardust.parsing.PatternSyntaxException

/**
 * Defines a lexical expression, using the [Regex].
 * 
 * @since 1.0
 *
 * @constructor Creates a new instance of the [Lex] class.
 * @param regex The [regex][Regex].
 * 
 * @property regex Gets the [regex][Regex].
 */
public class Lex private constructor(public val regex: Regex)
{
	/**
	 * Creates a new instance of the [Lex] class.
	 *
	 * @param pattern The pattern.
	 * @param options The options.
	 * @throws PatternSyntaxException The given [pattern] is invalid.
	 */
	public constructor(pattern: String, options: Set<RegexOption> = emptySet()):
		this(compileRegex(pattern, options))
	
	override fun toString(): String = makeStringOf(this, Lex::regex)
}
