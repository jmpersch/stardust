/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text.scanner

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.getSimpleName

/**
 * Defines the [Capture] class.
 *
 * @since 1.1
 *
 * @constructor Creates a new instance of the class [Capture].
 * @param scanner The [TextScanner].
 * @param matchResult The [MatchResult].
 * @param index The index of the match.
 */
public class Capture(
	private val scanner: TextScanner,
	private val matchResult: MatchResult,
	private val index: Int
) {
	public val firstIndex: Int = index + matchResult.range.first
	
	public val lastIndex: Int = index + matchResult.range.last
	
	public val groupCount: Int
		get() = matchResult.groupValues.size
	
	public val groupValues: List<String>
		get() = matchResult.groupValues
	
	public val group: MatchGroupCollection
		get() = matchResult.groups
	
	public val allValue: String
		get() = groupValues[0]
	
	public val allGroup: MatchGroup?
		get() = group[0]
	
	/**
	 * Peeks the next character in the input sequence after this match.
	 *
	 * @return The next character in the input sequence after this match; or `null` if the end is reached.
	 */
	@Pure
	public fun lookAhead(): Char? {
		
		val peekIndex = this.lastIndex + 1
		val length = this.scanner.input.length
		
		return if(peekIndex < length) this.scanner.input.get(peekIndex) else null
	}
	
	/**
	 * Checks, whether the next character in the input sequence after this match equals the specified character.
	 *
	 * @param c The expected next character.
	 * @return `true`, if the next character in the input sequence after this match equals [c].
	 */
	@Pure
	public fun lookAhead(c: Char): Boolean {
		
		val next = this.lookAhead()
		
		return next != null && next == c
	}
	
	/**
	 * Looks back to the previous character in the input sequence before this match.
	 *
	 * @return The previous character in the input sequence before this match; or `null` if there is none.
	 */
	@Pure
	public fun lookBack(): Char? {
		
		val prevIndex = this.firstIndex - 1
		
		return if(prevIndex >= 0) this.scanner.input.get(prevIndex) else null
	}
	
	/**
	 * Checks, whether the previous character in the input sequence before this match equals the specified character.
	 *
	 * @param c The expected previous character.
	 * @return The previous character in the input sequence before this match equals [c].
	 */
	@Pure
	public fun lookBack(c: Char): Boolean {
		
		val prev = this.lookBack()
		
		return prev != null && prev == c
	}
	
	/**
	 * Gets this [Capture] as a [MatchResult].
	 *
	 * @return This [Capture] as a [MatchResult].
	 */
	@Pure
	public fun toMatchResult(): MatchResult = matchResult
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(other == null || this::class != other::class) return false
		
		other as Capture
		
		if(scanner !== other.scanner) return false
		if(matchResult !== other.matchResult) return false
		if(index != other.index) return false
		
		return true
	}
	
	override fun hashCode(): Int {
		
		var result = scanner.hashCode()
		result = 31 * result + matchResult.hashCode()
		result = 31 * result + index
		return result
	}
	
	override fun toString(): String = "${this::class.getSimpleName()}{${this.groupCount} group(s)}"
}
