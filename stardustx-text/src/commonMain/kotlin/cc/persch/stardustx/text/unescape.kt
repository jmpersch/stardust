/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.QuoteEscapingOption
import cc.persch.stardust.text.QuoteEscapingOption.*
import cc.persch.stardustx.text.scanner.DefaultTextScanner
import cc.persch.stardustx.text.scanner.TextScanner
import cc.persch.stardustx.text.scanner.lexOf

/**
 * Unquotes a character sequence.
 *
 * The escaping option for quotes is detected on the first character of the sequence: single quote, double quote; or
 * no escaping, if the sequence does not start with a quote character.
 * 
 * @since 5.0
 */
@Pure
public fun CharSequence.unquote(): String {
	
	val scanner = DefaultTextScanner(this)
	
	val escapeOption = when {
		
		scanner.doesFetch('"') -> ESCAPE_DOUBLE_QUOTES
		scanner.doesFetch('\'') -> ESCAPE_SINGLE_QUOTES
		else -> ESCAPE_NONE
	}
	
	return unescape(scanner, escapeOption, trimLastQuote = true)
}

/**
 * Unescapes a character sequence.
 *
 * @since 4.0
 */
@Pure
public fun CharSequence.unescape(quoteEscapingOption: QuoteEscapingOption = ESCAPE_NONE): String {
	
	return unescape(DefaultTextScanner(this), quoteEscapingOption, trimLastQuote = false)
}

private val UNICODE_PATTERN = lexOf("""[0-9a-fA-F]{4}""")

@Pure
private fun CharSequence.unescape(
	scanner: TextScanner,
	escapeOption: QuoteEscapingOption,
	trimLastQuote: Boolean
) : String {
	
	val out = StringBuilder(this.length)
	
	var lastWasEscaped = false
	
	while(scanner.hasNext()) {
		
		when(val c = scanner.next()) {
			
			'\\' -> {
				
				lastWasEscaped = true
				
				if(!scanner.hasNext()) out.append("\\")
				else when(val next = scanner.next()) {
					
					'\\' -> out.append(next)
					
					'"' -> if(escapeOption == ESCAPE_DOUBLE_QUOTES) out.append(next) else out.append("\\\"")
					
					'\'' -> if(escapeOption == ESCAPE_SINGLE_QUOTES) out.append(next) else out.append("\\'")
					
					't' -> out.append('\t')
					
					'r' -> out.append('\r')
					
					'n' -> out.append('\n')
					
					'u' -> {
						
						val unicode1 = scanner.fetch(UNICODE_PATTERN)
						
						if(unicode1 != null) {
							
							out.append(unicode1.allValue.toInt(16).toChar())
							
							// Matches a combined unicode specification, e.g. `\u1234u5678`.
							if(scanner.doesFetch('u')) {
								
								val unicode2 = scanner.fetch(UNICODE_PATTERN)
								
								if(unicode2 != null)
									out.append(unicode2.allValue.toInt(16).toChar())
								else
									out.append("\\u")
							}
						}
						else
							out.append("\\u")
					}
					
					else -> out.append('\\').append(next)
				}
			}
			
			else -> {
				
				lastWasEscaped = false
				
				out.append(c)
			}
		}
	}
	
	if(trimLastQuote && !lastWasEscaped && out.isNotEmpty()
		&& (escapeOption == ESCAPE_DOUBLE_QUOTES && out.last() == '"'
			|| escapeOption == ESCAPE_SINGLE_QUOTES && out.last() == '\'')
	) out.setLength(out.length - 1)
	
	return out.toString()
}
