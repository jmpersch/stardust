/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text.scanner

import cc.persch.stardust.*
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.compareTo
import cc.persch.stardust.text.textSequence

/**
 * Defines the interface for a text scanner.
 *
 * @since 1.0
 * @see TextScanner
 * @see DefaultTextScanner
 */
public interface TextScanner {
	
	/**
	 * Gets the input sequence.
	 */
	public val input: CharSequence
	
	/**
	 * Gets the length of the input sequence.
	 *
	 * This method simply calls `this.input.length`.
	 */
	public val length: Int
		get() = this.input.length
	
	/**
	 * Gets or sets the index.
	 *
	 * @throws IllegalArgumentException The index is negative or greater than [length].
	 */
	public var index: Int
	
	/**
	 * Gets the character that the call of [next] has returned.
	 *
	 * @throws IllegalStateException First move ([next], [next] or [moveNext]) required.
	 */
	public val char: Char
		get() {
			
			val index = this.index
			
			if(index < 1)
				error("No character available. Move to first character required.")
			
			return this.get(index - 1)
		}
	
	/**
	 * Adds a value to the index and returns the new index.
	 *
	 * @param value The value to add to the index (can be negative for subtraction).
	 * @return The new index.
	 * @throws IllegalArgumentException The addition of the specified [value] leads to an invalid index.
	 */
	public fun addIndex(value: Int): Int {
		
		val next = this.index + value
		
		require("next", next in 0..this.length) {
			"The value to add leads to an invalid index.\n\n" +
				"Current index: $index\n" +
				"Value to add: $value\n" +
				"New index: $next\n" +
				"Length of input: $length"
		}
		
		this.index = next
		
		return next
	}
	
	/**
	 * Adds a value to the index, but returns the old index.
	 *
	 * @param value The value to add to the index (can be negative for subtraction).
	 * @return The old index.
	 * @throws IllegalArgumentException The addition of the specified [value] leads to an invalid index.
	 */
	public fun postAddIndex(value: Int): Int {
		
		val current = this.index
		
		this.addIndex(value)
		
		return current
	}
	
	/**
	 * Increments the index, but returns the old value.
	 *
	 * @return The index before incrementation.
	 * @throws IllegalArgumentException The addition of the specified [value] leads to an invalid index.
	 */
	public fun postIncIndex(): Int = postAddIndex(1)
	
	/**
	 * Increments the index and returns the new value.
	 *
	 * @return The incremented index.
	 * @throws IllegalArgumentException The addition of the specified [value] leads to an invalid index.
	 */
	public fun incIndex(): Int = addIndex(1)
	
	/**
	 * Decrements the index, but returns the old value.
	 *
	 * @return The index before incrementation.
	 */
	public fun postDecIndex(): Int = postAddIndex(-1)
	
	/**
	 * Decrements the index and returns the new value.
	 *
	 * @return The incremented index.
	 * @throws IllegalArgumentException The addition of the specified [value] leads to an invalid index.
	 */
	public fun decIndex(): Int = addIndex(-1)
	
	/**
	 * Gets the character at the given [index].
	 *
	 * This call is equal to `this.getInput().charAt(index)`.
	 *
	 * @throws IndexOutOfBoundsException The given [index] is out of bounds.
	 */
	@Pure
	public operator fun get(index: Int): Char = input.get(index)
	
	/**
	 * Gets a value, indicating whether there are more characters in the input sequence.
	 *
	 * @return `true`, if there are more characters in the input sequence; otherwise, `false`.
	 */
	@Pure
	public operator fun hasNext(): Boolean = index < length
	
	/**
	 * Gets a value, indicating whether there are at least *n* characters in the input sequence.
	 *
	 * @param count The count of characters to get.
	 * @return `true`, if there are at least [count] more characters.
	 * @throws IllegalArgumentException The count is negative or there are no next [count] characters.
	 */
	@Pure
	public fun hasNext(count: Int): Boolean {
		
		require("count", count >= 0)
		
		return index + count < length
	}
	
	/**
	 * Increments index.
	 *
	 * @return `true`, if the move was successful; otherwise, `false`
	 * @see hasNext
	 * @see next
	 */
	public fun moveNext(): Boolean {
		
		if(!hasNext())
			return false
		
		incIndex()
		
		return true
	}
	
	/**
	 * Increments the index by the given [count].
	 *
	 * @return `true`, if the move was successful; otherwise, `false`
	 * @throws IllegalArgumentException The count is negative.
	 * @see hasNext
	 * @see next
	 */
	public fun moveNext(count: Int): Boolean {
		
		if(!hasNext(count))
			return false
		
		addIndex(count)
		
		return true
	}
	
	/**
	 * Gets the next character and increments the index.
	 *
	 * @return The next character.
	 * @throws ScannerException There is no next character.
	 * @see hasNext
	 * @see moveNext
	 */
	@Pure
	public operator fun next(): Char {
		
		if(!hasNext())
			throw ScannerException("There is no next character.")
		
		return input[postIncIndex()]
	}
	
	/**
	 * Gets the next *n* characters.
	 *
	 * @param count The count of characters to get.
	 * @return The next [count] characters.
	 * @throws ScannerException There are no next [count] characters.
	 */
	@Pure
	public fun next(count: Int): String {
		
		if(!hasNext(count)) {
			
			throw ScannerException(
				"There ${if(count == 1) "is" else "are"} no next $count character${if(count == 1) "" else "s"}."
			)
		}
		
		val origIndex = index
		val nextIndex = origIndex + count
		
		index = nextIndex
		
		return input.subSequence(origIndex, nextIndex).toString()
	}
	
	/**
	 * Fetches a [CharSequence].
	 *
	 * @param value The value to fetch.
	 * @return `true`, if it did fetch; otherwise, `false`.
	 */
	@Pure
	public fun doesFetch(value: CharSequence): Boolean {
		
		if(!doesLookAhead(value))
			return false
		
		addIndex(value.length)
		
		return true
	}
	
	/**
	 * Fetches a `char`.
	 *
	 * @param c The character to fetch.
	 * @return `true`, if it did fetch; otherwise, `false`.
	 */
	@Pure
	public fun doesFetch(c: Char): Boolean {
		
		if(!doesLookAhead(c))
			return false
		
		incIndex()
		
		return true
	}
	
	/**
	 * Peeks the next character.
	 *
	 * @return The next character; or `null`, if there is none.
	 */
	@Pure
	public fun lookAhead(): Char? = if(hasNext()) input.get(index) else null
	
	/**
	 * Checks, whether the peeked character equals the specified character.
	 *
	 * @param c The character to check.
	 * @param ignoreCase `true` to ignore the character case on comparison.
	 * @return `true`, if the peeked character equals [c]; otherwise, `false`.
	 */
	@Pure
	public fun doesLookAhead(c: Char, ignoreCase: Boolean = false): Boolean = lookAhead()?.compareTo(c, ignoreCase) == 0
	
	/**
	 * Checks, whether the peeked character equals the specified sequence.
	 *
	 * @param value The sequence to check.
	 * @param ignoreCase `true` to ignore the character case on comparison.
	 * @return `true`, if the peeked character equals [value]; otherwise, `false`.
	 */
	@Pure
	public fun doesLookAhead(value: CharSequence, ignoreCase: Boolean = false): Boolean =
		this.hasNext() && input.regionMatches(index, value.toString(), 0, value.length, ignoreCase)
	
	/**
	 * Peeks the next characters.
	 *
	 * @param count The count of chars to peek.
	 * @return The next *count* characters; or `null`, if the current index is less than the count.
	 * @throws IllegalArgumentException [count] is less than `1`.
	 */
	@Pure
	public fun lookAhead(count: Int): String? {
		
		if(!hasNext(count))
			return null
		
		val index = this.index
		
		return input.subSequence(index, index + count).toString()
	}
	
	/**
	 * Gets the previous character.
	 *
	 * @param count The count of chars to peek.
	 * @return The next character; or `null`, if the current index is less than the count.
	 * @throws IllegalArgumentException [count] is less than `1`.
	 */
	@Pure
	public fun lookBack(count: Int): String? {
		
		require("count", count >= 0)
		
		val index = this.index
		
		return if(count <= index) input.subSequence(index - count, index).toString() else null
	}
	
	/**
	 * Gets the previous character.
	 *
	 * @return The next character; or `null`, if the index is zero.
	 */
	@Pure
	public fun lookBack(): Char? {
		
		val index = this.index
		
		return if(index > 0) input.get(index - 1) else null
	}
	
	
	/**
	 * Tries to look back the specified character.
	 *
	 * @param c The character to check.
	 * @param ignoreCase `true` to ignore the character case on comparison.
	 * @return `true`, if the previous character is [c]; otherwise, `false`.
	 */
	@Pure
	public fun doesLookBack(c: Char, ignoreCase: Boolean = false): Boolean = lookBack()?.compareTo(c, ignoreCase) == 0
	
	/**
	 * Checks, whether the looked back sequence equals the specified sequence.
	 *
	 * @param value The sequence to check.
	 * @param ignoreCase `true` to ignore the character case on comparison.
	 * @return `true`, if the peeked character equals [value]; otherwise, `false`.
	 */
	@Pure
	public fun doesLookBack(value: CharSequence, ignoreCase: Boolean = false): Boolean {
		
		val length = value.length
		val index = this.index
		
		return index > length && this.input.regionMatches(index - length, value, 0, length, ignoreCase)
	}
	
	/**
	 * Gets the chars from the current index up to the end.
	 *
	 * @return The chars from the current index up to the end.
	 * @throws ScannerException There are no more characters. (Check with [hasNext].)
	 */
	@Pure
	public fun remaining(): String {
		
		if(!hasNext())
			throw ScannerException("There are no more characters.")
		
		val rest = input.subSequence(index, length).toString()
		
		index = length
		
		return rest
	}
	
	/**
	 * Overrides up to [maxCount] whitespaces (determined by [Char.isWhitespace]). Use `null` for no limit.
	 *
	 * @throws IllegalArgumentException [maxCount] is negative.
	 */
	public fun overrideWhitespaces(maxCount: Int? = null) {
		
		require("count", maxCount == null || maxCount >= 0)
		
		var i = 0
		
		while(maxCount == null || i++ < maxCount) {
			
			if(lookAhead()?.isWhitespace() == true) moveNext()
			else break
		}
	}
	
	/**
	 * Fetches a text that matches to the specified [Lex].
	 *
	 * @param lex The [Lex] to fetch.
	 * @return The [Capture]; or `null`, if the pattern was not found.
	 */
	@Pure
	public fun fetch(lex: Lex): Capture? {
		
		val capture = lookAhead(lex) ?: return null
		
		index = capture.lastIndex + 1
		
		return capture
	}
	
	/**
	 * Fetches a text that matches to the specified [Lex], and returns, whether it as fetched or not.
	 *
	 * @param lex The [Lex] to fetch.
	 * @return `true`, if it has fetched; otherwise, `false`.
	 */
	@Pure
	public fun doesFetch(lex: Lex): Boolean = fetch(lex) != null
	
	/**
	 * Fetches a text that matches to the specified [Lex].
	 *
	 * @param lex The [Lex] to fetch.
	 * @return The [Capture]; or `null`, if the pattern was not found.
	 */
	@Pure
	public fun lookAhead(lex: Lex): Capture? {
		
		val startIndex = index
		
		val seq = input.textSequence(startIndex, length)
		
		val matchResult = lex.regex.find(seq) ?: return null
		
		return Capture(this, matchResult, startIndex)
	}
	
	/**
	 * Fetches a text that matches to the specified [Lex].
	 *
	 * @param lex The [Lex] to fetch.
	 * @return The [Capture]; or `null`, if the pattern was not found.
	 */
	@Pure
	public fun lookBack(lex: InverseLex): Capture? {
		
		val startIndex = index
		
		val seq = input.textSequence(0, startIndex)
		
		val matchResult = lex.regex.find(seq) ?: return null
		
		return Capture(this, matchResult, startIndex)
	}
	
	/**
	 * Resets the scanner.
	 */
	public fun reset() {
		
		index = 0
	}
}
