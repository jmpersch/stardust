/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.repeat
import cc.persch.stardustx.text.FillSide.LEFT
import cc.persch.stardustx.text.FillSide.RIGHT

/**
 * Defines the fill side options.
 *
 * @since 1.0
 */
public enum class FillSide {
	/**
	 * Fill to the left side.
	 */
	LEFT,
	
	/**
	 * Fill to the right side.
	 */
	RIGHT
}

/**
 * Fills a string to the left or to the right side.
 *
 *
 * Example:
 *
 * ```
 * "foo".fill(TextUtils.FillSide.Left,  5, 'o'); // --> "oofoo"
 * "oof".fill(TextUtils.FillSide.Right, 5, 'o'); // --> "oofoo"
 * ```
 *
 * @param side The [side to fill][FillSide].
 * @param size The size to fill up.
 * @param fill The character to fill with.
 * @return The filled string.
 * @see fillLeft
 * @see fillRight
 *
 * @since 1.0
 */
@Pure
public fun CharSequence.fill(side: FillSide, size: Int, fill: Char): String {
	
	val seqLength = length
	
	if(seqLength >= size)
		return toString()
	
	val filler = fill.repeat(size - seqLength)
	
	return StringBuilder(seqLength + size).apply {
		
		if(side == LEFT)
			append(filler)
		
		append(this@fill)
		
		if(side == RIGHT)
			append(filler)
		
	}.toString()
}

/**
 * Fills a string to the left side.
 *
 *
 * Example:
 *
 * ```
 * "foo".fillLeft(5, 'o'); // --> "oofoo"
 * ```
 *
 * @param size The size to fill up.
 * @param fill The character to fill with.
 * @return The filled string.
 * @see fill
 *
 * @since 1.0
 */
@Pure
public fun CharSequence.fillLeft(size: Int, fill: Char): String = fill(LEFT, size, fill)

/**
 * Fills a string to the right side.
 *
 *
 * Example:
 *
 * ```
 * "oof".fillLeft(5, 'o'); // --> "oofoo"
 * ```
 *
 * @param size The size to fill up.
 * @param fill The character to fill with.
 * @return The filled string.
 * @see fill
 *
 * @since 1.0
 */
@Pure
public fun CharSequence.fillRight(size: Int, fill: Char): String = fill(RIGHT, size, fill)
