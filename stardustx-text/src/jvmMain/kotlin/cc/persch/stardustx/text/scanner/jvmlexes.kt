/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text.scanner

import cc.persch.stardust.annotations.Pure
import java.util.regex.PatternSyntaxException
import kotlin.text.RegexOption.COMMENTS

/**
 * @throws PatternSyntaxException The given [pattern] is invalid.
 */
@Suppress("RegExpUnnecessaryNonCapturingGroup")
@Pure
internal actual fun compileRegex(pattern: String, options: Set<RegexOption>) =
	Regex("""^(?:$pattern${if(COMMENTS in options) "\n" else ""})""", options)

/**
 * @throws PatternSyntaxException The given [pattern] is invalid.
 */
@Suppress("RegExpUnnecessaryNonCapturingGroup")
@Pure
internal actual fun compileInverseRegex(pattern: String, options: Set<RegexOption>) =
	Regex("""(?:$pattern${if(COMMENTS in options) "\n" else ""})$""", options)
