/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.localization.Locales
import java.util.*

/**
 * Decapitalizes a string using the given locale, e.g. `"String"` becomes `"string"`.
 * The default locale is [Locales.default].
 *
 * Info: This function replaces the deprecated pendant of Kotlin's Standard Library.
 *
 * @since 5.0
 */
@Pure
public fun CharSequence.decapitalize(locale: Locale): String = replaceFirstChar { it.lowercase(locale) }

/**
 * Capitalizes a string using the given locale, e.g. `"string"` becomes `"String"`.
 * The default locale is [Locales.default].
 *
 * Info: For the upper case character, the [titlecase] function is used.
 *
 * Info: This function replaces the deprecated pendant of Kotlin's Standard Library.
 *
 * @since 5.0
 */
@Pure
public fun CharSequence.capitalize(locale: Locale): String = replaceFirstChar {
	
	if(it.isLowerCase()) it.titlecase(locale)
	else it.toString()
}
