/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.test

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.ConcludeUInt
import cc.persch.stardust.conclude.FailureUInt
import cc.persch.stardust.conclude.SuccessUInt
import cc.persch.stardust.getQualifiedName
import cc.persch.stardust.pipe
import cc.persch.stardust.piping
import kotlin.jvm.JvmName
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

/**
 * Asserts that this [ConcludeUInt] is a [SuccessUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertIsSuccess
 */
public fun ConcludeUInt<*>.expectSuccess(message: String? = null): UInt = assertIsSuccess(this, message)

/**
 * Asserts that the given [result] is a [SuccessUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see expectSuccess
 */
public fun assertIsSuccess(result: ConcludeUInt<*>, message: String? = null): UInt {
	
	if(result is FailureUInt<*>)
		throw AssertionError("Expected Success${if(message == null) "" else ": $message"}\nUInt was: $result")
	
	return (result as SuccessUInt).value
}

/**
 * Asserts that the given [actualConclude] is equal to [expectedConclude].
 *
 * Use this function, if the given concludes are of different error types;
 * otherwise, it’s recommended to use [assertEquals].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertConcludeNotEquals
 */
public fun assertConcludeEquals(
	expectedConclude: ConcludeUInt<*>,
	actualConclude: ConcludeUInt<*>,
	message: String? = null
) : Unit = assertEquals(expectedConclude, actualConclude, message)

/**
 * Asserts that the given [actualConclude] is *not* equal to [expectedConclude].
 *
 * Use this function, if the given concludes are of different error types;
 * otherwise, it’s recommended to use [assertNotEquals].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertConcludeEquals
 */
public fun assertConcludeNotEquals(
	expectedConclude: ConcludeUInt<*>,
	actualConclude: ConcludeUInt<*>,
	message: String? = null
) : Unit = assertNotEquals(expectedConclude, actualConclude, message)

/**
 * Asserts that the given [actualConclude] is a [SuccessUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun assertSuccessEquals(
	expectedValue: UInt,
	actualConclude: ConcludeUInt<*>,
	message: String? = null
) : UInt {
	
	val actualValue = assertIsSuccess(actualConclude, message)
	
	assertEquals(expectedValue, actualValue, message)
	
	return actualValue
}

/**
 * Asserts that the given [actualConclude] is a [SuccessUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun assertSuccessNotEquals(
	expectedValue: UInt,
	actualConclude: ConcludeUInt<*>,
	message: String? = null
) : UInt {
	
	val actualValue = assertIsSuccess(actualConclude, message)
	
	assertNotEquals(expectedValue, actualValue, message)
	
	return actualValue
}

/**
 * Asserts that this [ConcludeUInt] is a [FailureUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
@JvmName("__assertIsFailure")
public fun <Error> ConcludeUInt<Error>.assertIsFailure(message: String? = null): Error = assertIsFailure(this, message)

/**
 * Asserts that the given [result] is a [FailureUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error> assertIsFailure(result: ConcludeUInt<Error>, message: String? = null): Error {
	
	if(result is SuccessUInt)
		throw AssertionError("Expected Failure${if(message == null) "" else ": $message"}\nUInt was: $result")
	
	return (result as FailureUInt<Error>).reason
}

/**
 * Asserts that the given [result] is a [FailureUInt] containing a reason of the [Error] class.
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public inline fun <reified Error: Any> assertIsFailureOf(
	result: ConcludeUInt<*>,
	message: String? = null
) : Error = assertIsFailureOf(Error::class, result, message)

/**
 * Asserts that the given [result] is a [FailureUInt] containing a reason of the given [reasonClass].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error: Any> assertIsFailureOf(
	reasonClass: KClass<Error>,
	result: ConcludeUInt<*>,
	message: String? = null
) : Error {
	
	if(result is SuccessUInt) {
		
		throw AssertionError(
			"${message.prefix()}Expected Failure of class ${reasonClass.getQualifiedName()} but was Success."
		)
	}
	
	val reason = (result as FailureUInt<*>).reason
	
	@Suppress("UNCHECKED_CAST")
	if(reasonClass.isInstance(reason))
		return reason as Error
	
	throw AssertionError(
		"${message.prefix()}Expected Failure of class ${reasonClass.getQualifiedName()} " +
		"but was ${reason?.pipe{it::class.getQualifiedName()} ?: "null"}"
	)
}

/**
 * Asserts that the given [actualConclude] is a [FailureUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error> assertFailureEquals(
	expectedError: Error,
	actualConclude: ConcludeUInt<Error>,
	message: String? = null
) : Error {
	
	val actualError = assertIsFailure(actualConclude, message)
	
	assertEquals(expectedError, actualError, message)
	
	return actualError
}

/**
 * Asserts that the given [actualConclude] is a [FailureUInt].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error> assertFailureNotEquals(
	expectedError: Error,
	actualConclude: ConcludeUInt<Error>,
	message: String? = null
) : Error {
	
	val actualError = assertIsFailure(actualConclude, message)
	
	assertNotEquals(expectedError, actualError, message)
	
	return actualError
}

@Pure
private fun String?.prefix() = this?.piping { "$this\n" } ?: ""
