/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.test

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.ConcludeFloat
import cc.persch.stardust.conclude.FailureFloat
import cc.persch.stardust.conclude.SuccessFloat
import cc.persch.stardust.getQualifiedName
import cc.persch.stardust.pipe
import cc.persch.stardust.piping
import kotlin.jvm.JvmName
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

/**
 * Asserts that this [ConcludeFloat] is a [SuccessFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertIsSuccess
 */
public fun ConcludeFloat<*>.expectSuccess(message: String? = null): Float = assertIsSuccess(this, message)

/**
 * Asserts that the given [result] is a [SuccessFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see expectSuccess
 */
public fun assertIsSuccess(result: ConcludeFloat<*>, message: String? = null): Float {
	
	if(result is FailureFloat<*>)
		throw AssertionError("Expected Success${if(message == null) "" else ": $message"}\nFloat was: $result")
	
	return (result as SuccessFloat).value
}

/**
 * Asserts that the given [actualConclude] is equal to [expectedConclude].
 *
 * Use this function, if the given concludes are of different error types;
 * otherwise, it’s recommended to use [assertEquals].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertConcludeNotEquals
 */
public fun assertConcludeEquals(
	expectedConclude: ConcludeFloat<*>,
	actualConclude: ConcludeFloat<*>,
	message: String? = null
) : Unit = assertEquals(expectedConclude, actualConclude, message)

/**
 * Asserts that the given [actualConclude] is *not* equal to [expectedConclude].
 *
 * Use this function, if the given concludes are of different error types;
 * otherwise, it’s recommended to use [assertNotEquals].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertConcludeEquals
 */
public fun assertConcludeNotEquals(
	expectedConclude: ConcludeFloat<*>,
	actualConclude: ConcludeFloat<*>,
	message: String? = null
) : Unit = assertNotEquals(expectedConclude, actualConclude, message)

/**
 * Asserts that the given [actualConclude] is a [SuccessFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun assertSuccessEquals(
	expectedValue: Float,
	actualConclude: ConcludeFloat<*>,
	message: String? = null
) : Float {
	
	val actualValue = assertIsSuccess(actualConclude, message)
	
	assertEquals(expectedValue, actualValue, message)
	
	return actualValue
}

/**
 * Asserts that the given [actualConclude] is a [SuccessFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun assertSuccessNotEquals(
	expectedValue: Float,
	actualConclude: ConcludeFloat<*>,
	message: String? = null
) : Float {
	
	val actualValue = assertIsSuccess(actualConclude, message)
	
	assertNotEquals(expectedValue, actualValue, message)
	
	return actualValue
}

/**
 * Asserts that this [ConcludeFloat] is a [FailureFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
@JvmName("__assertIsFailure")
public fun <Error> ConcludeFloat<Error>.assertIsFailure(message: String? = null): Error = assertIsFailure(this, message)

/**
 * Asserts that the given [result] is a [FailureFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error> assertIsFailure(result: ConcludeFloat<Error>, message: String? = null): Error {
	
	if(result is SuccessFloat)
		throw AssertionError("Expected Failure${if(message == null) "" else ": $message"}\nFloat was: $result")
	
	return (result as FailureFloat<Error>).reason
}

/**
 * Asserts that the given [result] is a [FailureFloat] containing a reason of the [Error] class.
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public inline fun <reified Error: Any> assertIsFailureOf(
	result: ConcludeFloat<*>,
	message: String? = null
) : Error = assertIsFailureOf(Error::class, result, message)

/**
 * Asserts that the given [result] is a [FailureFloat] containing a reason of the given [reasonClass].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error: Any> assertIsFailureOf(
	reasonClass: KClass<Error>,
	result: ConcludeFloat<*>,
	message: String? = null
) : Error {
	
	if(result is SuccessFloat) {
		
		throw AssertionError(
			"${message.prefix()}Expected Failure of class ${reasonClass.getQualifiedName()} but was Success."
		)
	}
	
	val reason = (result as FailureFloat<*>).reason
	
	@Suppress("UNCHECKED_CAST")
	if(reasonClass.isInstance(reason))
		return reason as Error
	
	throw AssertionError(
		"${message.prefix()}Expected Failure of class ${reasonClass.getQualifiedName()} " +
		"but was ${reason?.pipe{it::class.getQualifiedName()} ?: "null"}"
	)
}

/**
 * Asserts that the given [actualConclude] is a [FailureFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error> assertFailureEquals(
	expectedError: Error,
	actualConclude: ConcludeFloat<Error>,
	message: String? = null
) : Error {
	
	val actualError = assertIsFailure(actualConclude, message)
	
	assertEquals(expectedError, actualError, message)
	
	return actualError
}

/**
 * Asserts that the given [actualConclude] is a [FailureFloat].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
public fun <Error> assertFailureNotEquals(
	expectedError: Error,
	actualConclude: ConcludeFloat<Error>,
	message: String? = null
) : Error {
	
	val actualError = assertIsFailure(actualConclude, message)
	
	assertNotEquals(expectedError, actualError, message)
	
	return actualError
}

@Pure
private fun String?.prefix() = this?.piping { "$this\n" } ?: ""
