/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

plugins {
	
	kotlin("multiplatform")
	kotlin("plugin.serialization")
	id("maven-publish")
}

val stardustVersion: String by project
val kotlinxSerializationVersion: String by project
val jvmTargetVersion: String by project

val gitLabRegistryProjectId: String by project

allprojects {
	
	apply(plugin = "org.jetbrains.kotlin.multiplatform")
	apply(plugin = "org.jetbrains.kotlin.plugin.serialization")
	apply(plugin = "maven-publish")
	
	group = "cc.persch"
	version = stardustVersion
	
	repositories {
		
		mavenCentral()
	}
	
	kotlin {
		
		if(project.name != "stardusty")
			explicitApi()
		
		jvmToolchain(jvmTargetVersion.toInt())
		
		jvm {
			
			testRuns.named("test") {
				
				executionTask.configure {
					
					useJUnitPlatform()
				}
			}
		}
		
		js {
			
			browser()
		}
		
		@Suppress("unused")
		sourceSets {
			
			val commonMain by getting {
				
				dependencies {
					
					if(project != rootProject)
						implementation(rootProject)
					
					api("org.jetbrains.kotlinx:kotlinx-serialization-core:$kotlinxSerializationVersion")
				}
			}
			
			val commonTest by getting {
				
				dependencies {
					
					implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationVersion")
					
					implementation(kotlin("test"))
					
					implementation(project(":stardustx-test"))
				}
			}
			
			val jsMain by getting
			val jsTest by getting
			
			val jvmMain by getting
			
			val jvmTest by getting {
				
				dependencies {
					
					implementation(kotlin("test-junit5"))
				}
			}
		}
		
		// https://kotlinlang.org/docs/reference/mpp-dsl-reference.html#language-settings
		// https://kotlinlang.org/docs/reference/opt-in-requirements.html
		sourceSets.all {
			
			languageSettings {
				
				optIn("kotlin.contracts.ExperimentalContracts")
				optIn("kotlin.ExperimentalStdlibApi")
				optIn("kotlin.experimental.ExperimentalTypeInference")
				optIn("kotlin.uuid.ExperimentalUuidApi")
			}
		}
		
		compilerOptions {
			
			freeCompilerArgs.add("-Xcontext-receivers")
			freeCompilerArgs.add("-Xexpect-actual-classes")
			freeCompilerArgs.add("-Xmulti-dollar-interpolation")
			freeCompilerArgs.add("-Xnon-local-break-continue")
			
			extraWarnings = true
			freeCompilerArgs.add("-Xsuppress-warning=CONTEXT_RECEIVERS_DEPRECATED")
			freeCompilerArgs.add("-Xsuppress-warning=REDUNDANT_VISIBILITY_MODIFIER")
			freeCompilerArgs.add("-Xsuppress-warning=UNUSED_ANONYMOUS_PARAMETER")
		}
	}
	
	publishing {
		
		publications.withType<MavenPublication> {
			
			pom {
				
				url = "https://persch.cc/stardust"
				inceptionYear = "2008"
				
				licenses {
					
					license {
						
						name = "Apache-2.0"
						url = "https://www.apache.org/licenses/LICENSE-2.0.txt"
					}
				}
				
				developers {
					
					developer {
						
						id = "jmpersch"
						name = "Jan Martin Persch"
						url = "https://persch.cc/"
					}
				}
				
				scm {
					
					connection = "scm:git:git@gitlab.com:jmpersch/stardust.git"
					developerConnection = "scm:git:git@gitlab.com:jmpersch/stardust.git"
					url = "https://gitlab.com/jmpersch/stardust/-/tree/master"
				}
			}
		}
		
		repositories {
			
			maven {
				
				url = uri("https://gitlab.com/api/v4/projects/$gitLabRegistryProjectId/packages/maven")
				name = "GitLab"
				
				credentials(HttpHeaderCredentials::class) {
					
					name = "Deploy-Token"
					value = findProperty("gitLabJmperschRegistryPublishToken") as String?
				}
				
				authentication {
					
					create("header", HttpHeaderAuthentication::class)
				}
			}
		}
	}
}

publishing.publications.withType<MavenPublication> {
	
	pom {
		
		name = "Stardust"
		
		description = "Stardust is a powerful extension to Kotlin's standard API."
	}
}
