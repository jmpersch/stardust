/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import java.math.BigInteger

/**
 * Creates an ordinal string from this [BigInteger], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun BigInteger.toOrdinalString(): String = toString() + when(abs().mod(BigInteger.valueOf(10)).toInt()) {
	
	1 -> "st"
	2 -> "nd"
	3 -> "rd"
	else -> "th"
}
