/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.localization

import cc.persch.stardust.annotations.Cached
import java.util.*
import java.util.Locale.Category
import java.util.Locale.Category.DISPLAY
import java.util.Locale.Category.FORMAT

/**
 * This object contains all available [languages][Locale].
 * 
 * *Generated on: OpenJDK Runtime Environment 21.0.2+13 (64-bit)*
 *
 * @since 1.0
 * @see Locales.getAvailableLocales
 */
@Suppress("unused")
public actual object Locales  {
	
	/**
	 * Gets the default [locale][Locale] for this instance of the Java Virtual Machine.
	 * On set, it also sets the locales for the [DISPLAY][Category.DISPLAY] and [FORMAT][Category.FORMAT] categories.
	 *
	 * @see java.util.Locale.getDefault
	 * @see getDefault
	 * @see setDefault
	 * @see java.util.PropertyPermission
	 */
	public actual var default: Locale
		get() = Locale.getDefault()
		set(value) = Locale.setDefault(value)
	
	/**
	 * Gets or sets the default [locale][Locale] for the [DISPLAY][Category.DISPLAY] category.
	 *
	 * @see java.util.Locale.getDefault
	 * @see getDefault
	 * @see setDefault
	 * @see java.util.PropertyPermission
	 */
	public actual var defaultDisplay: Locale
		get() = Locale.getDefault(DISPLAY)
		set(value) = Locale.setDefault(DISPLAY, value)
	
	/**
	 * Gets or sets the default [locale][Locale] for the [FORMAT][Category.FORMAT] category.
	 *
	 * @see java.util.Locale.getDefault
	 * @see getDefault
	 * @see setDefault
	 * @see java.util.PropertyPermission
	 */
	public actual var defaultFormat: Locale
		get() = Locale.getDefault(FORMAT)
		set(value) = Locale.setDefault(FORMAT, value)
	
	/**
     * Gets the current value of the default [locale][Locale] for the specified [category][Category]
     * for this instance of the Java Virtual Machine.
     *
     * @param category The specified category to get the default locale
     * @return The default locale for the specified Category for this instance of the Java Virtual Machine
	 * @see default
     * @see setDefault
	 * @see java.util.PropertyPermission
     */
	public fun getDefault(category: Category): Locale = Locale.getDefault(category)
	
	/**
	 * Sets the default [locale][Locale] for the specified [category][Category].
	 *
	 * @param category The category to set the default locale.
	 * @param newLocale The new locale.
	 * @see getDefault
	 * @see default
	 * @see java.util.PropertyPermission
	 */
	public fun setDefault(category: Category, newLocale: Locale): Unit = Locale.setDefault(category, newLocale)
	
	/**
	 * Sets the default [locales][Locale] for this instance of the Java Virtual Machine and for the
	 * [DISPLAY][Category.DISPLAY] and [FORMAT][Category.FORMAT] categories.
	 *
	 * @param locales The new locales, where the display locale is also set for the Virtual Machine.
	 * @see getDefault
	 * @see default
	 * @see java.util.PropertyPermission
	 */
	public fun setDefault(locales: LocalePair): Unit = this.setDefault(locales.display, locales.format)
	
	/**
	 * Sets the default [locales][Locale] for this instance of the Java Virtual Machine and for the
	 * [DISPLAY][Category.DISPLAY] and [FORMAT][Category.FORMAT] categories.
	 *
	 * @param displayLocale The new Virtual Machine and display locale.
	 * @param formatLocale The new format locale.
	 * @see getDefault
	 * @see default
	 * @see java.util.PropertyPermission
	 */
	public fun setDefault(displayLocale: Locale, formatLocale: Locale) {
		
		default = displayLocale
		defaultFormat = formatLocale
	}
	
	private val cachedAvailableLocales by lazy {
		
		buildMap { Locale.getAvailableLocales().forEach { put(it.toString(), it) } }
	}
	
	/**
	 * Gets map containing all available locales.
	 */
	@Cached
	public fun getAvailableLocales(): Map<String, Locale> = cachedAvailableLocales
	
	/**
	 * Gets the invariant [locale][Locale] [en].
	 * @see en
	 */
	public actual val invariant: Locale by lazy { en }
	
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * Afrikaans
	 */
	public val af: Locale get() = Locale.of("af")
	
	/**
	 * Afrikaans (Namibia)
	 */
	public val af_NA: Locale get() = Locale.of("af", "NA")
	
	/**
	 * Afrikaans (Latin, South Africa)
	 */
	public val af_ZA: Locale get() = Locale.of("af", "ZA")
	
	/**
	 * Aghem
	 */
	public val agq: Locale get() = Locale.of("agq")
	
	/**
	 * Aghem (Latin, Cameroon)
	 */
	public val agq_CM: Locale get() = Locale.of("agq", "CM")
	
	/**
	 * Akan
	 */
	public val ak: Locale get() = Locale.of("ak")
	
	/**
	 * Akan (Latin, Ghana)
	 */
	public val ak_GH: Locale get() = Locale.of("ak", "GH")
	
	/**
	 * Amharic
	 */
	public val am: Locale get() = Locale.of("am")
	
	/**
	 * Amharic (Ethiopic, Ethiopia)
	 */
	public val am_ET: Locale get() = Locale.of("am", "ET")
	
	/**
	 * Obolo
	 */
	public val ann: Locale get() = Locale.of("ann")
	
	/**
	 * Obolo (Nigeria)
	 */
	public val ann_NG: Locale get() = Locale.of("ann", "NG")
	
	/**
	 * Arabic
	 */
	public val ar: Locale get() = Locale.of("ar")
	
	/**
	 * Arabic (world)
	 */
	public val ar_001: Locale get() = Locale.of("ar", "001")
	
	/**
	 * Arabic (United Arab Emirates)
	 */
	public val ar_AE: Locale get() = Locale.of("ar", "AE")
	
	/**
	 * Arabic (Bahrain)
	 */
	public val ar_BH: Locale get() = Locale.of("ar", "BH")
	
	/**
	 * Arabic (Djibouti)
	 */
	public val ar_DJ: Locale get() = Locale.of("ar", "DJ")
	
	/**
	 * Arabic (Algeria)
	 */
	public val ar_DZ: Locale get() = Locale.of("ar", "DZ")
	
	/**
	 * Arabic (Egypt)
	 */
	public val ar_EG: Locale get() = Locale.of("ar", "EG")
	
	/**
	 * Arabic (Western Sahara)
	 */
	public val ar_EH: Locale get() = Locale.of("ar", "EH")
	
	/**
	 * Arabic (Eritrea)
	 */
	public val ar_ER: Locale get() = Locale.of("ar", "ER")
	
	/**
	 * Arabic (Israel)
	 */
	public val ar_IL: Locale get() = Locale.of("ar", "IL")
	
	/**
	 * Arabic (Iraq)
	 */
	public val ar_IQ: Locale get() = Locale.of("ar", "IQ")
	
	/**
	 * Arabic (Jordan)
	 */
	public val ar_JO: Locale get() = Locale.of("ar", "JO")
	
	/**
	 * Arabic (Comoros)
	 */
	public val ar_KM: Locale get() = Locale.of("ar", "KM")
	
	/**
	 * Arabic (Kuwait)
	 */
	public val ar_KW: Locale get() = Locale.of("ar", "KW")
	
	/**
	 * Arabic (Lebanon)
	 */
	public val ar_LB: Locale get() = Locale.of("ar", "LB")
	
	/**
	 * Arabic (Libya)
	 */
	public val ar_LY: Locale get() = Locale.of("ar", "LY")
	
	/**
	 * Arabic (Morocco)
	 */
	public val ar_MA: Locale get() = Locale.of("ar", "MA")
	
	/**
	 * Arabic (Mauritania)
	 */
	public val ar_MR: Locale get() = Locale.of("ar", "MR")
	
	/**
	 * Arabic (Oman)
	 */
	public val ar_OM: Locale get() = Locale.of("ar", "OM")
	
	/**
	 * Arabic (Palestinian Territories)
	 */
	public val ar_PS: Locale get() = Locale.of("ar", "PS")
	
	/**
	 * Arabic (Qatar)
	 */
	public val ar_QA: Locale get() = Locale.of("ar", "QA")
	
	/**
	 * Arabic (Saudi Arabia)
	 */
	public val ar_SA: Locale get() = Locale.of("ar", "SA")
	
	/**
	 * Arabic (Sudan)
	 */
	public val ar_SD: Locale get() = Locale.of("ar", "SD")
	
	/**
	 * Arabic (Somalia)
	 */
	public val ar_SO: Locale get() = Locale.of("ar", "SO")
	
	/**
	 * Arabic (South Sudan)
	 */
	public val ar_SS: Locale get() = Locale.of("ar", "SS")
	
	/**
	 * Arabic (Syria)
	 */
	public val ar_SY: Locale get() = Locale.of("ar", "SY")
	
	/**
	 * Arabic (Chad)
	 */
	public val ar_TD: Locale get() = Locale.of("ar", "TD")
	
	/**
	 * Arabic (Tunisia)
	 */
	public val ar_TN: Locale get() = Locale.of("ar", "TN")
	
	/**
	 * Arabic (Yemen)
	 */
	public val ar_YE: Locale get() = Locale.of("ar", "YE")
	
	/**
	 * Assamese
	 */
	public val `as`: Locale get() = Locale.of("as")
	
	/**
	 * Assamese (India)
	 */
	public val as_IN: Locale get() = Locale.of("as", "IN")
	
	/**
	 * Asu
	 */
	public val asa: Locale get() = Locale.of("asa")
	
	/**
	 * Asu (Latin, Tanzania)
	 */
	public val asa_TZ: Locale get() = Locale.of("asa", "TZ")
	
	/**
	 * Asturian
	 */
	public val ast: Locale get() = Locale.of("ast")
	
	/**
	 * Asturian (Latin, Spain)
	 */
	public val ast_ES: Locale get() = Locale.of("ast", "ES")
	
	/**
	 * Azerbaijani
	 */
	public val az: Locale get() = Locale.of("az")
	
	/**
	 * Azerbaijani (Azerbaijan)
	 */
	public val az_AZ: Locale get() = Locale.of("az", "AZ")
	
	/**
	 * Basaa
	 */
	public val bas: Locale get() = Locale.of("bas")
	
	/**
	 * Basaa (Latin, Cameroon)
	 */
	public val bas_CM: Locale get() = Locale.of("bas", "CM")
	
	/**
	 * Belarusian
	 */
	public val be: Locale get() = Locale.of("be")
	
	/**
	 * Belarusian (Taraskievica orthography)
	 */
	public val be_TARASK: Locale get() = Locale.of("be", "", "TARASK")
	
	/**
	 * Belarusian (Cyrillic, Belarus)
	 */
	public val be_BY: Locale get() = Locale.of("be", "BY")
	
	/**
	 * Bemba
	 */
	public val bem: Locale get() = Locale.of("bem")
	
	/**
	 * Bemba (Latin, Zambia)
	 */
	public val bem_ZM: Locale get() = Locale.of("bem", "ZM")
	
	/**
	 * Bena
	 */
	public val bez: Locale get() = Locale.of("bez")
	
	/**
	 * Bena (Latin, Tanzania)
	 */
	public val bez_TZ: Locale get() = Locale.of("bez", "TZ")
	
	/**
	 * Bulgarian
	 */
	public val bg: Locale get() = Locale.of("bg")
	
	/**
	 * Bulgarian (Cyrillic, Bulgaria)
	 */
	public val bg_BG: Locale get() = Locale.of("bg", "BG")
	
	/**
	 * Haryanvi
	 */
	public val bgc: Locale get() = Locale.of("bgc")
	
	/**
	 * Haryanvi (India)
	 */
	public val bgc_IN: Locale get() = Locale.of("bgc", "IN")
	
	/**
	 * Bhojpuri
	 */
	public val bho: Locale get() = Locale.of("bho")
	
	/**
	 * Bhojpuri (India)
	 */
	public val bho_IN: Locale get() = Locale.of("bho", "IN")
	
	/**
	 * Bambara
	 */
	public val bm: Locale get() = Locale.of("bm")
	
	/**
	 * Bambara (Latin, Mali)
	 */
	public val bm_ML: Locale get() = Locale.of("bm", "ML")
	
	/**
	 * Bangla
	 */
	public val bn: Locale get() = Locale.of("bn")
	
	/**
	 * Bangla (Bangla, Bangladesh)
	 */
	public val bn_BD: Locale get() = Locale.of("bn", "BD")
	
	/**
	 * Bangla (India)
	 */
	public val bn_IN: Locale get() = Locale.of("bn", "IN")
	
	/**
	 * Tibetan
	 */
	public val bo: Locale get() = Locale.of("bo")
	
	/**
	 * Tibetan (Tibetan, China)
	 */
	public val bo_CN: Locale get() = Locale.of("bo", "CN")
	
	/**
	 * Tibetan (India)
	 */
	public val bo_IN: Locale get() = Locale.of("bo", "IN")
	
	/**
	 * Breton
	 */
	public val br: Locale get() = Locale.of("br")
	
	/**
	 * Breton (Latin, France)
	 */
	public val br_FR: Locale get() = Locale.of("br", "FR")
	
	/**
	 * Bodo
	 */
	public val brx: Locale get() = Locale.of("brx")
	
	/**
	 * Bodo (India)
	 */
	public val brx_IN: Locale get() = Locale.of("brx", "IN")
	
	/**
	 * Bosnian (Latin)
	 */
	public val bs: Locale get() = Locale.of("bs")
	
	/**
	 * Bosnian (Bosnia & Herzegovina)
	 */
	public val bs_BA: Locale get() = Locale.of("bs", "BA")
	
	/**
	 * Catalan
	 */
	public val ca: Locale get() = Locale.of("ca")
	
	/**
	 * Catalan (Andorra)
	 */
	public val ca_AD: Locale get() = Locale.of("ca", "AD")
	
	/**
	 * Catalan (Spain)
	 */
	public val ca_ES: Locale get() = Locale.of("ca", "ES")
	
	/**
	 * Catalan (Spain, Valencian)
	 */
	public val ca_ES_VALENCIA: Locale get() = Locale.of("ca", "ES", "VALENCIA")
	
	/**
	 * Catalan (France)
	 */
	public val ca_FR: Locale get() = Locale.of("ca", "FR")
	
	/**
	 * Catalan (Italy)
	 */
	public val ca_IT: Locale get() = Locale.of("ca", "IT")
	
	/**
	 * Chakma
	 */
	public val ccp: Locale get() = Locale.of("ccp")
	
	/**
	 * Chakma (Chakma, Bangladesh)
	 */
	public val ccp_BD: Locale get() = Locale.of("ccp", "BD")
	
	/**
	 * Chakma (India)
	 */
	public val ccp_IN: Locale get() = Locale.of("ccp", "IN")
	
	/**
	 * Chechen
	 */
	public val ce: Locale get() = Locale.of("ce")
	
	/**
	 * Chechen (Russia)
	 */
	public val ce_RU: Locale get() = Locale.of("ce", "RU")
	
	/**
	 * Cebuano
	 */
	public val ceb: Locale get() = Locale.of("ceb")
	
	/**
	 * Cebuano (Philippines)
	 */
	public val ceb_PH: Locale get() = Locale.of("ceb", "PH")
	
	/**
	 * Chiga
	 */
	public val cgg: Locale get() = Locale.of("cgg")
	
	/**
	 * Chiga (Uganda)
	 */
	public val cgg_UG: Locale get() = Locale.of("cgg", "UG")
	
	/**
	 * Cherokee
	 */
	public val chr: Locale get() = Locale.of("chr")
	
	/**
	 * Cherokee (United States)
	 */
	public val chr_US: Locale get() = Locale.of("chr", "US")
	
	/**
	 * Central Kurdish
	 */
	public val ckb: Locale get() = Locale.of("ckb")
	
	/**
	 * Central Kurdish (Arabic, Iraq)
	 */
	public val ckb_IQ: Locale get() = Locale.of("ckb", "IQ")
	
	/**
	 * Central Kurdish (Iran)
	 */
	public val ckb_IR: Locale get() = Locale.of("ckb", "IR")
	
	/**
	 * Czech
	 */
	public val cs: Locale get() = Locale.of("cs")
	
	/**
	 * Czech (Czechia)
	 */
	public val cs_CZ: Locale get() = Locale.of("cs", "CZ")
	
	/**
	 * Chuvash
	 */
	public val cv: Locale get() = Locale.of("cv")
	
	/**
	 * Chuvash (Cyrillic, Russia)
	 */
	public val cv_RU: Locale get() = Locale.of("cv", "RU")
	
	/**
	 * Welsh
	 */
	public val cy: Locale get() = Locale.of("cy")
	
	/**
	 * Welsh (Latin, United Kingdom)
	 */
	public val cy_GB: Locale get() = Locale.of("cy", "GB")
	
	/**
	 * Danish
	 */
	public val da: Locale get() = Locale.of("da")
	
	/**
	 * Danish (Latin, Denmark)
	 */
	public val da_DK: Locale get() = Locale.of("da", "DK")
	
	/**
	 * Danish (Greenland)
	 */
	public val da_GL: Locale get() = Locale.of("da", "GL")
	
	/**
	 * Taita
	 */
	public val dav: Locale get() = Locale.of("dav")
	
	/**
	 * Taita (Kenya)
	 */
	public val dav_KE: Locale get() = Locale.of("dav", "KE")
	
	/**
	 * German
	 */
	public val de: Locale get() = Locale.of("de")
	
	/**
	 * German (Austria)
	 */
	public val de_AT: Locale get() = Locale.of("de", "AT")
	
	/**
	 * German (Belgium)
	 */
	public val de_BE: Locale get() = Locale.of("de", "BE")
	
	/**
	 * German (Switzerland)
	 */
	public val de_CH: Locale get() = Locale.of("de", "CH")
	
	/**
	 * German (Latin, Germany)
	 */
	public val de_DE: Locale get() = Locale.of("de", "DE")
	
	/**
	 * German (Italy)
	 */
	public val de_IT: Locale get() = Locale.of("de", "IT")
	
	/**
	 * German (Liechtenstein)
	 */
	public val de_LI: Locale get() = Locale.of("de", "LI")
	
	/**
	 * German (Luxembourg)
	 */
	public val de_LU: Locale get() = Locale.of("de", "LU")
	
	/**
	 * Zarma
	 */
	public val dje: Locale get() = Locale.of("dje")
	
	/**
	 * Zarma (Niger)
	 */
	public val dje_NE: Locale get() = Locale.of("dje", "NE")
	
	/**
	 * Dogri
	 */
	public val doi: Locale get() = Locale.of("doi")
	
	/**
	 * Dogri (India)
	 */
	public val doi_IN: Locale get() = Locale.of("doi", "IN")
	
	/**
	 * Lower Sorbian
	 */
	public val dsb: Locale get() = Locale.of("dsb")
	
	/**
	 * Lower Sorbian (Germany)
	 */
	public val dsb_DE: Locale get() = Locale.of("dsb", "DE")
	
	/**
	 * Duala
	 */
	public val dua: Locale get() = Locale.of("dua")
	
	/**
	 * Duala (Cameroon)
	 */
	public val dua_CM: Locale get() = Locale.of("dua", "CM")
	
	/**
	 * Jola-Fonyi
	 */
	public val dyo: Locale get() = Locale.of("dyo")
	
	/**
	 * Jola-Fonyi (Senegal)
	 */
	public val dyo_SN: Locale get() = Locale.of("dyo", "SN")
	
	/**
	 * Dzongkha
	 */
	public val dz: Locale get() = Locale.of("dz")
	
	/**
	 * Dzongkha (Bhutan)
	 */
	public val dz_BT: Locale get() = Locale.of("dz", "BT")
	
	/**
	 * Embu
	 */
	public val ebu: Locale get() = Locale.of("ebu")
	
	/**
	 * Embu (Kenya)
	 */
	public val ebu_KE: Locale get() = Locale.of("ebu", "KE")
	
	/**
	 * Ewe
	 */
	public val ee: Locale get() = Locale.of("ee")
	
	/**
	 * Ewe (Latin, Ghana)
	 */
	public val ee_GH: Locale get() = Locale.of("ee", "GH")
	
	/**
	 * Ewe (Togo)
	 */
	public val ee_TG: Locale get() = Locale.of("ee", "TG")
	
	/**
	 * Greek
	 */
	public val el: Locale get() = Locale.of("el")
	
	/**
	 * Greek (Polytonic)
	 */
	public val el_POLYTON: Locale get() = Locale.of("el", "", "POLYTON")
	
	/**
	 * Greek (Cyprus)
	 */
	public val el_CY: Locale get() = Locale.of("el", "CY")
	
	/**
	 * Greek (Greek, Greece)
	 */
	public val el_GR: Locale get() = Locale.of("el", "GR")
	
	/**
	 * English
	 */
	public val en: Locale get() = Locale.of("en")
	
	/**
	 * English (world)
	 */
	public val en_001: Locale get() = Locale.of("en", "001")
	
	/**
	 * English (Europe)
	 */
	public val en_150: Locale get() = Locale.of("en", "150")
	
	/**
	 * English (United Arab Emirates)
	 */
	public val en_AE: Locale get() = Locale.of("en", "AE")
	
	/**
	 * English (Antigua & Barbuda)
	 */
	public val en_AG: Locale get() = Locale.of("en", "AG")
	
	/**
	 * English (Anguilla)
	 */
	public val en_AI: Locale get() = Locale.of("en", "AI")
	
	/**
	 * English (American Samoa)
	 */
	public val en_AS: Locale get() = Locale.of("en", "AS")
	
	/**
	 * English (Austria)
	 */
	public val en_AT: Locale get() = Locale.of("en", "AT")
	
	/**
	 * English (Australia)
	 */
	public val en_AU: Locale get() = Locale.of("en", "AU")
	
	/**
	 * English (Barbados)
	 */
	public val en_BB: Locale get() = Locale.of("en", "BB")
	
	/**
	 * English (Belgium)
	 */
	public val en_BE: Locale get() = Locale.of("en", "BE")
	
	/**
	 * English (Burundi)
	 */
	public val en_BI: Locale get() = Locale.of("en", "BI")
	
	/**
	 * English (Bermuda)
	 */
	public val en_BM: Locale get() = Locale.of("en", "BM")
	
	/**
	 * English (Bahamas)
	 */
	public val en_BS: Locale get() = Locale.of("en", "BS")
	
	/**
	 * English (Botswana)
	 */
	public val en_BW: Locale get() = Locale.of("en", "BW")
	
	/**
	 * English (Belize)
	 */
	public val en_BZ: Locale get() = Locale.of("en", "BZ")
	
	/**
	 * English (Canada)
	 */
	public val en_CA: Locale get() = Locale.of("en", "CA")
	
	/**
	 * English (Cocos (Keeling) Islands)
	 */
	public val en_CC: Locale get() = Locale.of("en", "CC")
	
	/**
	 * English (Switzerland)
	 */
	public val en_CH: Locale get() = Locale.of("en", "CH")
	
	/**
	 * English (Cook Islands)
	 */
	public val en_CK: Locale get() = Locale.of("en", "CK")
	
	/**
	 * English (Cameroon)
	 */
	public val en_CM: Locale get() = Locale.of("en", "CM")
	
	/**
	 * English (Christmas Island)
	 */
	public val en_CX: Locale get() = Locale.of("en", "CX")
	
	/**
	 * English (Cyprus)
	 */
	public val en_CY: Locale get() = Locale.of("en", "CY")
	
	/**
	 * English (Germany)
	 */
	public val en_DE: Locale get() = Locale.of("en", "DE")
	
	/**
	 * English (Diego Garcia)
	 */
	public val en_DG: Locale get() = Locale.of("en", "DG")
	
	/**
	 * English (Denmark)
	 */
	public val en_DK: Locale get() = Locale.of("en", "DK")
	
	/**
	 * English (Dominica)
	 */
	public val en_DM: Locale get() = Locale.of("en", "DM")
	
	/**
	 * English (Eritrea)
	 */
	public val en_ER: Locale get() = Locale.of("en", "ER")
	
	/**
	 * English (Finland)
	 */
	public val en_FI: Locale get() = Locale.of("en", "FI")
	
	/**
	 * English (Fiji)
	 */
	public val en_FJ: Locale get() = Locale.of("en", "FJ")
	
	/**
	 * English (Falkland Islands)
	 */
	public val en_FK: Locale get() = Locale.of("en", "FK")
	
	/**
	 * English (Micronesia)
	 */
	public val en_FM: Locale get() = Locale.of("en", "FM")
	
	/**
	 * English (United Kingdom)
	 */
	public val en_GB: Locale get() = Locale.of("en", "GB")
	
	/**
	 * English (Grenada)
	 */
	public val en_GD: Locale get() = Locale.of("en", "GD")
	
	/**
	 * English (Guernsey)
	 */
	public val en_GG: Locale get() = Locale.of("en", "GG")
	
	/**
	 * English (Ghana)
	 */
	public val en_GH: Locale get() = Locale.of("en", "GH")
	
	/**
	 * English (Gibraltar)
	 */
	public val en_GI: Locale get() = Locale.of("en", "GI")
	
	/**
	 * English (Gambia)
	 */
	public val en_GM: Locale get() = Locale.of("en", "GM")
	
	/**
	 * English (Guam)
	 */
	public val en_GU: Locale get() = Locale.of("en", "GU")
	
	/**
	 * English (Guyana)
	 */
	public val en_GY: Locale get() = Locale.of("en", "GY")
	
	/**
	 * English (Hong Kong SAR China)
	 */
	public val en_HK: Locale get() = Locale.of("en", "HK")
	
	/**
	 * English (Ireland)
	 */
	public val en_IE: Locale get() = Locale.of("en", "IE")
	
	/**
	 * English (Israel)
	 */
	public val en_IL: Locale get() = Locale.of("en", "IL")
	
	/**
	 * English (Isle of Man)
	 */
	public val en_IM: Locale get() = Locale.of("en", "IM")
	
	/**
	 * English (India)
	 */
	public val en_IN: Locale get() = Locale.of("en", "IN")
	
	/**
	 * English (British Indian Ocean Territory)
	 */
	public val en_IO: Locale get() = Locale.of("en", "IO")
	
	/**
	 * English (Jersey)
	 */
	public val en_JE: Locale get() = Locale.of("en", "JE")
	
	/**
	 * English (Jamaica)
	 */
	public val en_JM: Locale get() = Locale.of("en", "JM")
	
	/**
	 * English (Kenya)
	 */
	public val en_KE: Locale get() = Locale.of("en", "KE")
	
	/**
	 * English (Kiribati)
	 */
	public val en_KI: Locale get() = Locale.of("en", "KI")
	
	/**
	 * English (St. Kitts & Nevis)
	 */
	public val en_KN: Locale get() = Locale.of("en", "KN")
	
	/**
	 * English (Cayman Islands)
	 */
	public val en_KY: Locale get() = Locale.of("en", "KY")
	
	/**
	 * English (St. Lucia)
	 */
	public val en_LC: Locale get() = Locale.of("en", "LC")
	
	/**
	 * English (Liberia)
	 */
	public val en_LR: Locale get() = Locale.of("en", "LR")
	
	/**
	 * English (Lesotho)
	 */
	public val en_LS: Locale get() = Locale.of("en", "LS")
	
	/**
	 * English (Madagascar)
	 */
	public val en_MG: Locale get() = Locale.of("en", "MG")
	
	/**
	 * English (Marshall Islands)
	 */
	public val en_MH: Locale get() = Locale.of("en", "MH")
	
	/**
	 * English (Macao SAR China)
	 */
	public val en_MO: Locale get() = Locale.of("en", "MO")
	
	/**
	 * English (Northern Mariana Islands)
	 */
	public val en_MP: Locale get() = Locale.of("en", "MP")
	
	/**
	 * English (Montserrat)
	 */
	public val en_MS: Locale get() = Locale.of("en", "MS")
	
	/**
	 * English (Malta)
	 */
	public val en_MT: Locale get() = Locale.of("en", "MT")
	
	/**
	 * English (Mauritius)
	 */
	public val en_MU: Locale get() = Locale.of("en", "MU")
	
	/**
	 * English (Maldives)
	 */
	public val en_MV: Locale get() = Locale.of("en", "MV")
	
	/**
	 * English (Malawi)
	 */
	public val en_MW: Locale get() = Locale.of("en", "MW")
	
	/**
	 * English (Malaysia)
	 */
	public val en_MY: Locale get() = Locale.of("en", "MY")
	
	/**
	 * English (Namibia)
	 */
	public val en_NA: Locale get() = Locale.of("en", "NA")
	
	/**
	 * English (Norfolk Island)
	 */
	public val en_NF: Locale get() = Locale.of("en", "NF")
	
	/**
	 * English (Nigeria)
	 */
	public val en_NG: Locale get() = Locale.of("en", "NG")
	
	/**
	 * English (Netherlands)
	 */
	public val en_NL: Locale get() = Locale.of("en", "NL")
	
	/**
	 * English (Nauru)
	 */
	public val en_NR: Locale get() = Locale.of("en", "NR")
	
	/**
	 * English (Niue)
	 */
	public val en_NU: Locale get() = Locale.of("en", "NU")
	
	/**
	 * English (New Zealand)
	 */
	public val en_NZ: Locale get() = Locale.of("en", "NZ")
	
	/**
	 * English (Papua New Guinea)
	 */
	public val en_PG: Locale get() = Locale.of("en", "PG")
	
	/**
	 * English (Philippines)
	 */
	public val en_PH: Locale get() = Locale.of("en", "PH")
	
	/**
	 * English (Pakistan)
	 */
	public val en_PK: Locale get() = Locale.of("en", "PK")
	
	/**
	 * English (Pitcairn Islands)
	 */
	public val en_PN: Locale get() = Locale.of("en", "PN")
	
	/**
	 * English (Puerto Rico)
	 */
	public val en_PR: Locale get() = Locale.of("en", "PR")
	
	/**
	 * English (Palau)
	 */
	public val en_PW: Locale get() = Locale.of("en", "PW")
	
	/**
	 * English (Rwanda)
	 */
	public val en_RW: Locale get() = Locale.of("en", "RW")
	
	/**
	 * English (Solomon Islands)
	 */
	public val en_SB: Locale get() = Locale.of("en", "SB")
	
	/**
	 * English (Seychelles)
	 */
	public val en_SC: Locale get() = Locale.of("en", "SC")
	
	/**
	 * English (Sudan)
	 */
	public val en_SD: Locale get() = Locale.of("en", "SD")
	
	/**
	 * English (Sweden)
	 */
	public val en_SE: Locale get() = Locale.of("en", "SE")
	
	/**
	 * English (Singapore)
	 */
	public val en_SG: Locale get() = Locale.of("en", "SG")
	
	/**
	 * English (St. Helena)
	 */
	public val en_SH: Locale get() = Locale.of("en", "SH")
	
	/**
	 * English (Slovenia)
	 */
	public val en_SI: Locale get() = Locale.of("en", "SI")
	
	/**
	 * English (Sierra Leone)
	 */
	public val en_SL: Locale get() = Locale.of("en", "SL")
	
	/**
	 * English (South Sudan)
	 */
	public val en_SS: Locale get() = Locale.of("en", "SS")
	
	/**
	 * English (Sint Maarten)
	 */
	public val en_SX: Locale get() = Locale.of("en", "SX")
	
	/**
	 * English (Eswatini)
	 */
	public val en_SZ: Locale get() = Locale.of("en", "SZ")
	
	/**
	 * English (Turks & Caicos Islands)
	 */
	public val en_TC: Locale get() = Locale.of("en", "TC")
	
	/**
	 * English (Tokelau)
	 */
	public val en_TK: Locale get() = Locale.of("en", "TK")
	
	/**
	 * English (Tonga)
	 */
	public val en_TO: Locale get() = Locale.of("en", "TO")
	
	/**
	 * English (Trinidad & Tobago)
	 */
	public val en_TT: Locale get() = Locale.of("en", "TT")
	
	/**
	 * English (Tuvalu)
	 */
	public val en_TV: Locale get() = Locale.of("en", "TV")
	
	/**
	 * English (Tanzania)
	 */
	public val en_TZ: Locale get() = Locale.of("en", "TZ")
	
	/**
	 * English (Uganda)
	 */
	public val en_UG: Locale get() = Locale.of("en", "UG")
	
	/**
	 * English (U.S. Outlying Islands)
	 */
	public val en_UM: Locale get() = Locale.of("en", "UM")
	
	/**
	 * English (United States)
	 */
	public val en_US: Locale get() = Locale.of("en", "US")
	
	/**
	 * English (United States, Computer)
	 */
	public val en_US_POSIX: Locale get() = Locale.of("en", "US", "POSIX")
	
	/**
	 * English (St. Vincent & Grenadines)
	 */
	public val en_VC: Locale get() = Locale.of("en", "VC")
	
	/**
	 * English (British Virgin Islands)
	 */
	public val en_VG: Locale get() = Locale.of("en", "VG")
	
	/**
	 * English (U.S. Virgin Islands)
	 */
	public val en_VI: Locale get() = Locale.of("en", "VI")
	
	/**
	 * English (Vanuatu)
	 */
	public val en_VU: Locale get() = Locale.of("en", "VU")
	
	/**
	 * English (Samoa)
	 */
	public val en_WS: Locale get() = Locale.of("en", "WS")
	
	/**
	 * English (South Africa)
	 */
	public val en_ZA: Locale get() = Locale.of("en", "ZA")
	
	/**
	 * English (Zambia)
	 */
	public val en_ZM: Locale get() = Locale.of("en", "ZM")
	
	/**
	 * English (Zimbabwe)
	 */
	public val en_ZW: Locale get() = Locale.of("en", "ZW")
	
	/**
	 * Esperanto
	 */
	public val eo: Locale get() = Locale.of("eo")
	
	/**
	 * Esperanto (world)
	 */
	public val eo_001: Locale get() = Locale.of("eo", "001")
	
	/**
	 * Spanish
	 */
	public val es: Locale get() = Locale.of("es")
	
	/**
	 * Spanish (Latin America)
	 */
	public val es_419: Locale get() = Locale.of("es", "419")
	
	/**
	 * Spanish (Argentina)
	 */
	public val es_AR: Locale get() = Locale.of("es", "AR")
	
	/**
	 * Spanish (Bolivia)
	 */
	public val es_BO: Locale get() = Locale.of("es", "BO")
	
	/**
	 * Spanish (Brazil)
	 */
	public val es_BR: Locale get() = Locale.of("es", "BR")
	
	/**
	 * Spanish (Belize)
	 */
	public val es_BZ: Locale get() = Locale.of("es", "BZ")
	
	/**
	 * Spanish (Chile)
	 */
	public val es_CL: Locale get() = Locale.of("es", "CL")
	
	/**
	 * Spanish (Colombia)
	 */
	public val es_CO: Locale get() = Locale.of("es", "CO")
	
	/**
	 * Spanish (Costa Rica)
	 */
	public val es_CR: Locale get() = Locale.of("es", "CR")
	
	/**
	 * Spanish (Cuba)
	 */
	public val es_CU: Locale get() = Locale.of("es", "CU")
	
	/**
	 * Spanish (Dominican Republic)
	 */
	public val es_DO: Locale get() = Locale.of("es", "DO")
	
	/**
	 * Spanish (Ceuta & Melilla)
	 */
	public val es_EA: Locale get() = Locale.of("es", "EA")
	
	/**
	 * Spanish (Ecuador)
	 */
	public val es_EC: Locale get() = Locale.of("es", "EC")
	
	/**
	 * Spanish (Spain)
	 */
	public val es_ES: Locale get() = Locale.of("es", "ES")
	
	/**
	 * Spanish (Equatorial Guinea)
	 */
	public val es_GQ: Locale get() = Locale.of("es", "GQ")
	
	/**
	 * Spanish (Guatemala)
	 */
	public val es_GT: Locale get() = Locale.of("es", "GT")
	
	/**
	 * Spanish (Honduras)
	 */
	public val es_HN: Locale get() = Locale.of("es", "HN")
	
	/**
	 * Spanish (Canary Islands)
	 */
	public val es_IC: Locale get() = Locale.of("es", "IC")
	
	/**
	 * Spanish (Mexico)
	 */
	public val es_MX: Locale get() = Locale.of("es", "MX")
	
	/**
	 * Spanish (Nicaragua)
	 */
	public val es_NI: Locale get() = Locale.of("es", "NI")
	
	/**
	 * Spanish (Panama)
	 */
	public val es_PA: Locale get() = Locale.of("es", "PA")
	
	/**
	 * Spanish (Peru)
	 */
	public val es_PE: Locale get() = Locale.of("es", "PE")
	
	/**
	 * Spanish (Philippines)
	 */
	public val es_PH: Locale get() = Locale.of("es", "PH")
	
	/**
	 * Spanish (Puerto Rico)
	 */
	public val es_PR: Locale get() = Locale.of("es", "PR")
	
	/**
	 * Spanish (Paraguay)
	 */
	public val es_PY: Locale get() = Locale.of("es", "PY")
	
	/**
	 * Spanish (El Salvador)
	 */
	public val es_SV: Locale get() = Locale.of("es", "SV")
	
	/**
	 * Spanish (United States)
	 */
	public val es_US: Locale get() = Locale.of("es", "US")
	
	/**
	 * Spanish (Uruguay)
	 */
	public val es_UY: Locale get() = Locale.of("es", "UY")
	
	/**
	 * Spanish (Venezuela)
	 */
	public val es_VE: Locale get() = Locale.of("es", "VE")
	
	/**
	 * Estonian
	 */
	public val et: Locale get() = Locale.of("et")
	
	/**
	 * Estonian (Estonia)
	 */
	public val et_EE: Locale get() = Locale.of("et", "EE")
	
	/**
	 * Basque
	 */
	public val eu: Locale get() = Locale.of("eu")
	
	/**
	 * Basque (Latin, Spain)
	 */
	public val eu_ES: Locale get() = Locale.of("eu", "ES")
	
	/**
	 * Ewondo
	 */
	public val ewo: Locale get() = Locale.of("ewo")
	
	/**
	 * Ewondo (Latin, Cameroon)
	 */
	public val ewo_CM: Locale get() = Locale.of("ewo", "CM")
	
	/**
	 * Persian
	 */
	public val fa: Locale get() = Locale.of("fa")
	
	/**
	 * Persian (Afghanistan)
	 */
	public val fa_AF: Locale get() = Locale.of("fa", "AF")
	
	/**
	 * Persian (Arabic, Iran)
	 */
	public val fa_IR: Locale get() = Locale.of("fa", "IR")
	
	/**
	 * Fula (Adlam)
	 */
	public val ff: Locale get() = Locale.of("ff")
	
	/**
	 * Fula (Adlam, Burkina Faso)
	 */
	public val ff_BF: Locale get() = Locale.of("ff", "BF")
	
	/**
	 * Fula (Latin, Cameroon)
	 */
	public val ff_CM: Locale get() = Locale.of("ff", "CM")
	
	/**
	 * Fula (Latin, Ghana)
	 */
	public val ff_GH: Locale get() = Locale.of("ff", "GH")
	
	/**
	 * Fula (Latin, Gambia)
	 */
	public val ff_GM: Locale get() = Locale.of("ff", "GM")
	
	/**
	 * Fula (Guinea)
	 */
	public val ff_GN: Locale get() = Locale.of("ff", "GN")
	
	/**
	 * Fula (Adlam, Guinea-Bissau)
	 */
	public val ff_GW: Locale get() = Locale.of("ff", "GW")
	
	/**
	 * Fula (Adlam, Liberia)
	 */
	public val ff_LR: Locale get() = Locale.of("ff", "LR")
	
	/**
	 * Fula (Latin, Mauritania)
	 */
	public val ff_MR: Locale get() = Locale.of("ff", "MR")
	
	/**
	 * Fula (Adlam, Niger)
	 */
	public val ff_NE: Locale get() = Locale.of("ff", "NE")
	
	/**
	 * Fula (Latin, Nigeria)
	 */
	public val ff_NG: Locale get() = Locale.of("ff", "NG")
	
	/**
	 * Fula (Latin, Sierra Leone)
	 */
	public val ff_SL: Locale get() = Locale.of("ff", "SL")
	
	/**
	 * Fula (Senegal)
	 */
	public val ff_SN: Locale get() = Locale.of("ff", "SN")
	
	/**
	 * Finnish
	 */
	public val fi: Locale get() = Locale.of("fi")
	
	/**
	 * Finnish (Latin, Finland)
	 */
	public val fi_FI: Locale get() = Locale.of("fi", "FI")
	
	/**
	 * Filipino
	 */
	public val fil: Locale get() = Locale.of("fil")
	
	/**
	 * Filipino (Latin, Philippines)
	 */
	public val fil_PH: Locale get() = Locale.of("fil", "PH")
	
	/**
	 * Faroese
	 */
	public val fo: Locale get() = Locale.of("fo")
	
	/**
	 * Faroese (Denmark)
	 */
	public val fo_DK: Locale get() = Locale.of("fo", "DK")
	
	/**
	 * Faroese (Latin, Faroe Islands)
	 */
	public val fo_FO: Locale get() = Locale.of("fo", "FO")
	
	/**
	 * French
	 */
	public val fr: Locale get() = Locale.of("fr")
	
	/**
	 * French (Belgium)
	 */
	public val fr_BE: Locale get() = Locale.of("fr", "BE")
	
	/**
	 * French (Burkina Faso)
	 */
	public val fr_BF: Locale get() = Locale.of("fr", "BF")
	
	/**
	 * French (Burundi)
	 */
	public val fr_BI: Locale get() = Locale.of("fr", "BI")
	
	/**
	 * French (Benin)
	 */
	public val fr_BJ: Locale get() = Locale.of("fr", "BJ")
	
	/**
	 * French (St. Barthélemy)
	 */
	public val fr_BL: Locale get() = Locale.of("fr", "BL")
	
	/**
	 * French (Canada)
	 */
	public val fr_CA: Locale get() = Locale.of("fr", "CA")
	
	/**
	 * French (Congo - Kinshasa)
	 */
	public val fr_CD: Locale get() = Locale.of("fr", "CD")
	
	/**
	 * French (Central African Republic)
	 */
	public val fr_CF: Locale get() = Locale.of("fr", "CF")
	
	/**
	 * French (Congo - Brazzaville)
	 */
	public val fr_CG: Locale get() = Locale.of("fr", "CG")
	
	/**
	 * French (Switzerland)
	 */
	public val fr_CH: Locale get() = Locale.of("fr", "CH")
	
	/**
	 * French (Côte d’Ivoire)
	 */
	public val fr_CI: Locale get() = Locale.of("fr", "CI")
	
	/**
	 * French (Cameroon)
	 */
	public val fr_CM: Locale get() = Locale.of("fr", "CM")
	
	/**
	 * French (Djibouti)
	 */
	public val fr_DJ: Locale get() = Locale.of("fr", "DJ")
	
	/**
	 * French (Algeria)
	 */
	public val fr_DZ: Locale get() = Locale.of("fr", "DZ")
	
	/**
	 * French (Latin, France)
	 */
	public val fr_FR: Locale get() = Locale.of("fr", "FR")
	
	/**
	 * French (Gabon)
	 */
	public val fr_GA: Locale get() = Locale.of("fr", "GA")
	
	/**
	 * French (French Guiana)
	 */
	public val fr_GF: Locale get() = Locale.of("fr", "GF")
	
	/**
	 * French (Guinea)
	 */
	public val fr_GN: Locale get() = Locale.of("fr", "GN")
	
	/**
	 * French (Guadeloupe)
	 */
	public val fr_GP: Locale get() = Locale.of("fr", "GP")
	
	/**
	 * French (Equatorial Guinea)
	 */
	public val fr_GQ: Locale get() = Locale.of("fr", "GQ")
	
	/**
	 * French (Haiti)
	 */
	public val fr_HT: Locale get() = Locale.of("fr", "HT")
	
	/**
	 * French (Comoros)
	 */
	public val fr_KM: Locale get() = Locale.of("fr", "KM")
	
	/**
	 * French (Luxembourg)
	 */
	public val fr_LU: Locale get() = Locale.of("fr", "LU")
	
	/**
	 * French (Morocco)
	 */
	public val fr_MA: Locale get() = Locale.of("fr", "MA")
	
	/**
	 * French (Monaco)
	 */
	public val fr_MC: Locale get() = Locale.of("fr", "MC")
	
	/**
	 * French (St. Martin)
	 */
	public val fr_MF: Locale get() = Locale.of("fr", "MF")
	
	/**
	 * French (Madagascar)
	 */
	public val fr_MG: Locale get() = Locale.of("fr", "MG")
	
	/**
	 * French (Mali)
	 */
	public val fr_ML: Locale get() = Locale.of("fr", "ML")
	
	/**
	 * French (Martinique)
	 */
	public val fr_MQ: Locale get() = Locale.of("fr", "MQ")
	
	/**
	 * French (Mauritania)
	 */
	public val fr_MR: Locale get() = Locale.of("fr", "MR")
	
	/**
	 * French (Mauritius)
	 */
	public val fr_MU: Locale get() = Locale.of("fr", "MU")
	
	/**
	 * French (New Caledonia)
	 */
	public val fr_NC: Locale get() = Locale.of("fr", "NC")
	
	/**
	 * French (Niger)
	 */
	public val fr_NE: Locale get() = Locale.of("fr", "NE")
	
	/**
	 * French (French Polynesia)
	 */
	public val fr_PF: Locale get() = Locale.of("fr", "PF")
	
	/**
	 * French (St. Pierre & Miquelon)
	 */
	public val fr_PM: Locale get() = Locale.of("fr", "PM")
	
	/**
	 * French (Réunion)
	 */
	public val fr_RE: Locale get() = Locale.of("fr", "RE")
	
	/**
	 * French (Rwanda)
	 */
	public val fr_RW: Locale get() = Locale.of("fr", "RW")
	
	/**
	 * French (Seychelles)
	 */
	public val fr_SC: Locale get() = Locale.of("fr", "SC")
	
	/**
	 * French (Senegal)
	 */
	public val fr_SN: Locale get() = Locale.of("fr", "SN")
	
	/**
	 * French (Syria)
	 */
	public val fr_SY: Locale get() = Locale.of("fr", "SY")
	
	/**
	 * French (Chad)
	 */
	public val fr_TD: Locale get() = Locale.of("fr", "TD")
	
	/**
	 * French (Togo)
	 */
	public val fr_TG: Locale get() = Locale.of("fr", "TG")
	
	/**
	 * French (Tunisia)
	 */
	public val fr_TN: Locale get() = Locale.of("fr", "TN")
	
	/**
	 * French (Vanuatu)
	 */
	public val fr_VU: Locale get() = Locale.of("fr", "VU")
	
	/**
	 * French (Wallis & Futuna)
	 */
	public val fr_WF: Locale get() = Locale.of("fr", "WF")
	
	/**
	 * French (Mayotte)
	 */
	public val fr_YT: Locale get() = Locale.of("fr", "YT")
	
	/**
	 * Northern Frisian
	 */
	public val frr: Locale get() = Locale.of("frr")
	
	/**
	 * Northern Frisian (Latin, Germany)
	 */
	public val frr_DE: Locale get() = Locale.of("frr", "DE")
	
	/**
	 * Friulian
	 */
	public val fur: Locale get() = Locale.of("fur")
	
	/**
	 * Friulian (Latin, Italy)
	 */
	public val fur_IT: Locale get() = Locale.of("fur", "IT")
	
	/**
	 * Western Frisian
	 */
	public val fy: Locale get() = Locale.of("fy")
	
	/**
	 * Western Frisian (Netherlands)
	 */
	public val fy_NL: Locale get() = Locale.of("fy", "NL")
	
	/**
	 * Irish
	 */
	public val ga: Locale get() = Locale.of("ga")
	
	/**
	 * Irish (United Kingdom)
	 */
	public val ga_GB: Locale get() = Locale.of("ga", "GB")
	
	/**
	 * Irish (Ireland)
	 */
	public val ga_IE: Locale get() = Locale.of("ga", "IE")
	
	/**
	 * Scottish Gaelic
	 */
	public val gd: Locale get() = Locale.of("gd")
	
	/**
	 * Scottish Gaelic (United Kingdom)
	 */
	public val gd_GB: Locale get() = Locale.of("gd", "GB")
	
	/**
	 * Galician
	 */
	public val gl: Locale get() = Locale.of("gl")
	
	/**
	 * Galician (Latin, Spain)
	 */
	public val gl_ES: Locale get() = Locale.of("gl", "ES")
	
	/**
	 * Swiss German
	 */
	public val gsw: Locale get() = Locale.of("gsw")
	
	/**
	 * Swiss German (Switzerland)
	 */
	public val gsw_CH: Locale get() = Locale.of("gsw", "CH")
	
	/**
	 * Swiss German (France)
	 */
	public val gsw_FR: Locale get() = Locale.of("gsw", "FR")
	
	/**
	 * Swiss German (Liechtenstein)
	 */
	public val gsw_LI: Locale get() = Locale.of("gsw", "LI")
	
	/**
	 * Gujarati
	 */
	public val gu: Locale get() = Locale.of("gu")
	
	/**
	 * Gujarati (Gujarati, India)
	 */
	public val gu_IN: Locale get() = Locale.of("gu", "IN")
	
	/**
	 * Gusii
	 */
	public val guz: Locale get() = Locale.of("guz")
	
	/**
	 * Gusii (Latin, Kenya)
	 */
	public val guz_KE: Locale get() = Locale.of("guz", "KE")
	
	/**
	 * Manx
	 */
	public val gv: Locale get() = Locale.of("gv")
	
	/**
	 * Manx (Isle of Man)
	 */
	public val gv_IM: Locale get() = Locale.of("gv", "IM")
	
	/**
	 * Hausa
	 */
	public val ha: Locale get() = Locale.of("ha")
	
	/**
	 * Hausa (Ghana)
	 */
	public val ha_GH: Locale get() = Locale.of("ha", "GH")
	
	/**
	 * Hausa (Niger)
	 */
	public val ha_NE: Locale get() = Locale.of("ha", "NE")
	
	/**
	 * Hausa (Nigeria)
	 */
	public val ha_NG: Locale get() = Locale.of("ha", "NG")
	
	/**
	 * Hawaiian
	 */
	public val haw: Locale get() = Locale.of("haw")
	
	/**
	 * Hawaiian (Latin, United States)
	 */
	public val haw_US: Locale get() = Locale.of("haw", "US")
	
	/**
	 * Hebrew
	 */
	public val he: Locale get() = Locale.of("he")
	
	/**
	 * Hebrew (Hebrew, Israel)
	 */
	public val he_IL: Locale get() = Locale.of("he", "IL")
	
	/**
	 * Hindi
	 */
	public val hi: Locale get() = Locale.of("hi")
	
	/**
	 * Hindi (Latin, India)
	 */
	public val hi_IN: Locale get() = Locale.of("hi", "IN")
	
	/**
	 * Croatian
	 */
	public val hr: Locale get() = Locale.of("hr")
	
	/**
	 * Croatian (Bosnia & Herzegovina)
	 */
	public val hr_BA: Locale get() = Locale.of("hr", "BA")
	
	/**
	 * Croatian (Latin, Croatia)
	 */
	public val hr_HR: Locale get() = Locale.of("hr", "HR")
	
	/**
	 * Upper Sorbian
	 */
	public val hsb: Locale get() = Locale.of("hsb")
	
	/**
	 * Upper Sorbian (Germany)
	 */
	public val hsb_DE: Locale get() = Locale.of("hsb", "DE")
	
	/**
	 * Hungarian
	 */
	public val hu: Locale get() = Locale.of("hu")
	
	/**
	 * Hungarian (Hungary)
	 */
	public val hu_HU: Locale get() = Locale.of("hu", "HU")
	
	/**
	 * Armenian
	 */
	public val hy: Locale get() = Locale.of("hy")
	
	/**
	 * Armenian (Armenian, Armenia)
	 */
	public val hy_AM: Locale get() = Locale.of("hy", "AM")
	
	/**
	 * Interlingua
	 */
	public val ia: Locale get() = Locale.of("ia")
	
	/**
	 * Interlingua (world)
	 */
	public val ia_001: Locale get() = Locale.of("ia", "001")
	
	/**
	 * Indonesian
	 */
	public val id: Locale get() = Locale.of("id")
	
	/**
	 * Indonesian (Indonesia)
	 */
	public val id_ID: Locale get() = Locale.of("id", "ID")
	
	/**
	 * Igbo
	 */
	public val ig: Locale get() = Locale.of("ig")
	
	/**
	 * Igbo (Nigeria)
	 */
	public val ig_NG: Locale get() = Locale.of("ig", "NG")
	
	/**
	 * Sichuan Yi
	 */
	public val ii: Locale get() = Locale.of("ii")
	
	/**
	 * Sichuan Yi (China)
	 */
	public val ii_CN: Locale get() = Locale.of("ii", "CN")
	
	/**
	 * Icelandic
	 */
	public val `is`: Locale get() = Locale.of("is")
	
	/**
	 * Icelandic (Latin, Iceland)
	 */
	public val is_IS: Locale get() = Locale.of("is", "IS")
	
	/**
	 * Italian
	 */
	public val it: Locale get() = Locale.of("it")
	
	/**
	 * Italian (Switzerland)
	 */
	public val it_CH: Locale get() = Locale.of("it", "CH")
	
	/**
	 * Italian (Italy)
	 */
	public val it_IT: Locale get() = Locale.of("it", "IT")
	
	/**
	 * Italian (San Marino)
	 */
	public val it_SM: Locale get() = Locale.of("it", "SM")
	
	/**
	 * Italian (Vatican City)
	 */
	public val it_VA: Locale get() = Locale.of("it", "VA")
	
	/**
	 * Japanese
	 */
	public val ja: Locale get() = Locale.of("ja")
	
	/**
	 * Japanese (Japanese, Japan)
	 */
	public val ja_JP: Locale get() = Locale.of("ja", "JP")
	
	/**
	 * Japanese (Japan, JP, Japanese Calendar)
	 */
	public val ja_JP_JP: Locale get() = Locale.of("ja", "JP", "JP")
	
	/**
	 * Ngomba
	 */
	public val jgo: Locale get() = Locale.of("jgo")
	
	/**
	 * Ngomba (Cameroon)
	 */
	public val jgo_CM: Locale get() = Locale.of("jgo", "CM")
	
	/**
	 * Machame
	 */
	public val jmc: Locale get() = Locale.of("jmc")
	
	/**
	 * Machame (Tanzania)
	 */
	public val jmc_TZ: Locale get() = Locale.of("jmc", "TZ")
	
	/**
	 * Javanese
	 */
	public val jv: Locale get() = Locale.of("jv")
	
	/**
	 * Javanese (Latin, Indonesia)
	 */
	public val jv_ID: Locale get() = Locale.of("jv", "ID")
	
	/**
	 * Georgian
	 */
	public val ka: Locale get() = Locale.of("ka")
	
	/**
	 * Georgian (Georgia)
	 */
	public val ka_GE: Locale get() = Locale.of("ka", "GE")
	
	/**
	 * Kabyle
	 */
	public val kab: Locale get() = Locale.of("kab")
	
	/**
	 * Kabyle (Latin, Algeria)
	 */
	public val kab_DZ: Locale get() = Locale.of("kab", "DZ")
	
	/**
	 * Kamba
	 */
	public val kam: Locale get() = Locale.of("kam")
	
	/**
	 * Kamba (Kenya)
	 */
	public val kam_KE: Locale get() = Locale.of("kam", "KE")
	
	/**
	 * Makonde
	 */
	public val kde: Locale get() = Locale.of("kde")
	
	/**
	 * Makonde (Latin, Tanzania)
	 */
	public val kde_TZ: Locale get() = Locale.of("kde", "TZ")
	
	/**
	 * Kabuverdianu
	 */
	public val kea: Locale get() = Locale.of("kea")
	
	/**
	 * Kabuverdianu (Cape Verde)
	 */
	public val kea_CV: Locale get() = Locale.of("kea", "CV")
	
	/**
	 * Kaingang
	 */
	public val kgp: Locale get() = Locale.of("kgp")
	
	/**
	 * Kaingang (Latin, Brazil)
	 */
	public val kgp_BR: Locale get() = Locale.of("kgp", "BR")
	
	/**
	 * Koyra Chiini
	 */
	public val khq: Locale get() = Locale.of("khq")
	
	/**
	 * Koyra Chiini (Mali)
	 */
	public val khq_ML: Locale get() = Locale.of("khq", "ML")
	
	/**
	 * Kikuyu
	 */
	public val ki: Locale get() = Locale.of("ki")
	
	/**
	 * Kikuyu (Kenya)
	 */
	public val ki_KE: Locale get() = Locale.of("ki", "KE")
	
	/**
	 * Kazakh
	 */
	public val kk: Locale get() = Locale.of("kk")
	
	/**
	 * Kazakh (Kazakhstan)
	 */
	public val kk_KZ: Locale get() = Locale.of("kk", "KZ")
	
	/**
	 * Kako
	 */
	public val kkj: Locale get() = Locale.of("kkj")
	
	/**
	 * Kako (Latin, Cameroon)
	 */
	public val kkj_CM: Locale get() = Locale.of("kkj", "CM")
	
	/**
	 * Kalaallisut
	 */
	public val kl: Locale get() = Locale.of("kl")
	
	/**
	 * Kalaallisut (Latin, Greenland)
	 */
	public val kl_GL: Locale get() = Locale.of("kl", "GL")
	
	/**
	 * Kalenjin
	 */
	public val kln: Locale get() = Locale.of("kln")
	
	/**
	 * Kalenjin (Kenya)
	 */
	public val kln_KE: Locale get() = Locale.of("kln", "KE")
	
	/**
	 * Khmer
	 */
	public val km: Locale get() = Locale.of("km")
	
	/**
	 * Khmer (Khmer, Cambodia)
	 */
	public val km_KH: Locale get() = Locale.of("km", "KH")
	
	/**
	 * Kannada
	 */
	public val kn: Locale get() = Locale.of("kn")
	
	/**
	 * Kannada (India)
	 */
	public val kn_IN: Locale get() = Locale.of("kn", "IN")
	
	/**
	 * Korean
	 */
	public val ko: Locale get() = Locale.of("ko")
	
	/**
	 * Korean (North Korea)
	 */
	public val ko_KP: Locale get() = Locale.of("ko", "KP")
	
	/**
	 * Korean (Korean, South Korea)
	 */
	public val ko_KR: Locale get() = Locale.of("ko", "KR")
	
	/**
	 * Konkani
	 */
	public val kok: Locale get() = Locale.of("kok")
	
	/**
	 * Konkani (Devanagari, India)
	 */
	public val kok_IN: Locale get() = Locale.of("kok", "IN")
	
	/**
	 * Kashmiri (Arabic)
	 */
	public val ks: Locale get() = Locale.of("ks")
	
	/**
	 * Kashmiri (Devanagari, India)
	 */
	public val ks_IN: Locale get() = Locale.of("ks", "IN")
	
	/**
	 * Shambala
	 */
	public val ksb: Locale get() = Locale.of("ksb")
	
	/**
	 * Shambala (Tanzania)
	 */
	public val ksb_TZ: Locale get() = Locale.of("ksb", "TZ")
	
	/**
	 * Bafia
	 */
	public val ksf: Locale get() = Locale.of("ksf")
	
	/**
	 * Bafia (Cameroon)
	 */
	public val ksf_CM: Locale get() = Locale.of("ksf", "CM")
	
	/**
	 * Colognian
	 */
	public val ksh: Locale get() = Locale.of("ksh")
	
	/**
	 * Colognian (Germany)
	 */
	public val ksh_DE: Locale get() = Locale.of("ksh", "DE")
	
	/**
	 * Kurdish
	 */
	public val ku: Locale get() = Locale.of("ku")
	
	/**
	 * Kurdish (Türkiye)
	 */
	public val ku_TR: Locale get() = Locale.of("ku", "TR")
	
	/**
	 * Cornish
	 */
	public val kw: Locale get() = Locale.of("kw")
	
	/**
	 * Cornish (Latin, United Kingdom)
	 */
	public val kw_GB: Locale get() = Locale.of("kw", "GB")
	
	/**
	 * Kyrgyz
	 */
	public val ky: Locale get() = Locale.of("ky")
	
	/**
	 * Kyrgyz (Kyrgyzstan)
	 */
	public val ky_KG: Locale get() = Locale.of("ky", "KG")
	
	/**
	 * Langi
	 */
	public val lag: Locale get() = Locale.of("lag")
	
	/**
	 * Langi (Tanzania)
	 */
	public val lag_TZ: Locale get() = Locale.of("lag", "TZ")
	
	/**
	 * Luxembourgish
	 */
	public val lb: Locale get() = Locale.of("lb")
	
	/**
	 * Luxembourgish (Latin, Luxembourg)
	 */
	public val lb_LU: Locale get() = Locale.of("lb", "LU")
	
	/**
	 * Ganda
	 */
	public val lg: Locale get() = Locale.of("lg")
	
	/**
	 * Ganda (Uganda)
	 */
	public val lg_UG: Locale get() = Locale.of("lg", "UG")
	
	/**
	 * Lakota
	 */
	public val lkt: Locale get() = Locale.of("lkt")
	
	/**
	 * Lakota (United States)
	 */
	public val lkt_US: Locale get() = Locale.of("lkt", "US")
	
	/**
	 * Lingala
	 */
	public val ln: Locale get() = Locale.of("ln")
	
	/**
	 * Lingala (Angola)
	 */
	public val ln_AO: Locale get() = Locale.of("ln", "AO")
	
	/**
	 * Lingala (Congo - Kinshasa)
	 */
	public val ln_CD: Locale get() = Locale.of("ln", "CD")
	
	/**
	 * Lingala (Central African Republic)
	 */
	public val ln_CF: Locale get() = Locale.of("ln", "CF")
	
	/**
	 * Lingala (Congo - Brazzaville)
	 */
	public val ln_CG: Locale get() = Locale.of("ln", "CG")
	
	/**
	 * Lao
	 */
	public val lo: Locale get() = Locale.of("lo")
	
	/**
	 * Lao (Laos)
	 */
	public val lo_LA: Locale get() = Locale.of("lo", "LA")
	
	/**
	 * Northern Luri
	 */
	public val lrc: Locale get() = Locale.of("lrc")
	
	/**
	 * Northern Luri (Iraq)
	 */
	public val lrc_IQ: Locale get() = Locale.of("lrc", "IQ")
	
	/**
	 * Northern Luri (Iran)
	 */
	public val lrc_IR: Locale get() = Locale.of("lrc", "IR")
	
	/**
	 * Lithuanian
	 */
	public val lt: Locale get() = Locale.of("lt")
	
	/**
	 * Lithuanian (Lithuania)
	 */
	public val lt_LT: Locale get() = Locale.of("lt", "LT")
	
	/**
	 * Luba-Katanga
	 */
	public val lu: Locale get() = Locale.of("lu")
	
	/**
	 * Luba-Katanga (Latin, Congo - Kinshasa)
	 */
	public val lu_CD: Locale get() = Locale.of("lu", "CD")
	
	/**
	 * Luo
	 */
	public val luo: Locale get() = Locale.of("luo")
	
	/**
	 * Luo (Latin, Kenya)
	 */
	public val luo_KE: Locale get() = Locale.of("luo", "KE")
	
	/**
	 * Luyia
	 */
	public val luy: Locale get() = Locale.of("luy")
	
	/**
	 * Luyia (Latin, Kenya)
	 */
	public val luy_KE: Locale get() = Locale.of("luy", "KE")
	
	/**
	 * Latvian
	 */
	public val lv: Locale get() = Locale.of("lv")
	
	/**
	 * Latvian (Latin, Latvia)
	 */
	public val lv_LV: Locale get() = Locale.of("lv", "LV")
	
	/**
	 * Maithili
	 */
	public val mai: Locale get() = Locale.of("mai")
	
	/**
	 * Maithili (Devanagari, India)
	 */
	public val mai_IN: Locale get() = Locale.of("mai", "IN")
	
	/**
	 * Masai
	 */
	public val mas: Locale get() = Locale.of("mas")
	
	/**
	 * Masai (Kenya)
	 */
	public val mas_KE: Locale get() = Locale.of("mas", "KE")
	
	/**
	 * Masai (Tanzania)
	 */
	public val mas_TZ: Locale get() = Locale.of("mas", "TZ")
	
	/**
	 * Moksha
	 */
	public val mdf: Locale get() = Locale.of("mdf")
	
	/**
	 * Moksha (Russia)
	 */
	public val mdf_RU: Locale get() = Locale.of("mdf", "RU")
	
	/**
	 * Meru
	 */
	public val mer: Locale get() = Locale.of("mer")
	
	/**
	 * Meru (Kenya)
	 */
	public val mer_KE: Locale get() = Locale.of("mer", "KE")
	
	/**
	 * Morisyen
	 */
	public val mfe: Locale get() = Locale.of("mfe")
	
	/**
	 * Morisyen (Mauritius)
	 */
	public val mfe_MU: Locale get() = Locale.of("mfe", "MU")
	
	/**
	 * Malagasy
	 */
	public val mg: Locale get() = Locale.of("mg")
	
	/**
	 * Malagasy (Latin, Madagascar)
	 */
	public val mg_MG: Locale get() = Locale.of("mg", "MG")
	
	/**
	 * Makhuwa-Meetto
	 */
	public val mgh: Locale get() = Locale.of("mgh")
	
	/**
	 * Makhuwa-Meetto (Mozambique)
	 */
	public val mgh_MZ: Locale get() = Locale.of("mgh", "MZ")
	
	/**
	 * Metaʼ
	 */
	public val mgo: Locale get() = Locale.of("mgo")
	
	/**
	 * Metaʼ (Cameroon)
	 */
	public val mgo_CM: Locale get() = Locale.of("mgo", "CM")
	
	/**
	 * Māori
	 */
	public val mi: Locale get() = Locale.of("mi")
	
	/**
	 * Māori (New Zealand)
	 */
	public val mi_NZ: Locale get() = Locale.of("mi", "NZ")
	
	/**
	 * Macedonian
	 */
	public val mk: Locale get() = Locale.of("mk")
	
	/**
	 * Macedonian (North Macedonia)
	 */
	public val mk_MK: Locale get() = Locale.of("mk", "MK")
	
	/**
	 * Malayalam
	 */
	public val ml: Locale get() = Locale.of("ml")
	
	/**
	 * Malayalam (India)
	 */
	public val ml_IN: Locale get() = Locale.of("ml", "IN")
	
	/**
	 * Mongolian
	 */
	public val mn: Locale get() = Locale.of("mn")
	
	/**
	 * Mongolian (Mongolia)
	 */
	public val mn_MN: Locale get() = Locale.of("mn", "MN")
	
	/**
	 * Manipuri
	 */
	public val mni: Locale get() = Locale.of("mni")
	
	/**
	 * Manipuri (Bangla, India)
	 */
	public val mni_IN: Locale get() = Locale.of("mni", "IN")
	
	/**
	 * Marathi
	 */
	public val mr: Locale get() = Locale.of("mr")
	
	/**
	 * Marathi (India)
	 */
	public val mr_IN: Locale get() = Locale.of("mr", "IN")
	
	/**
	 * Malay
	 */
	public val ms: Locale get() = Locale.of("ms")
	
	/**
	 * Malay (Brunei)
	 */
	public val ms_BN: Locale get() = Locale.of("ms", "BN")
	
	/**
	 * Malay (Indonesia)
	 */
	public val ms_ID: Locale get() = Locale.of("ms", "ID")
	
	/**
	 * Malay (Malaysia)
	 */
	public val ms_MY: Locale get() = Locale.of("ms", "MY")
	
	/**
	 * Malay (Singapore)
	 */
	public val ms_SG: Locale get() = Locale.of("ms", "SG")
	
	/**
	 * Maltese
	 */
	public val mt: Locale get() = Locale.of("mt")
	
	/**
	 * Maltese (Latin, Malta)
	 */
	public val mt_MT: Locale get() = Locale.of("mt", "MT")
	
	/**
	 * Mundang
	 */
	public val mua: Locale get() = Locale.of("mua")
	
	/**
	 * Mundang (Latin, Cameroon)
	 */
	public val mua_CM: Locale get() = Locale.of("mua", "CM")
	
	/**
	 * Burmese
	 */
	public val my: Locale get() = Locale.of("my")
	
	/**
	 * Burmese (Myanmar, Myanmar (Burma))
	 */
	public val my_MM: Locale get() = Locale.of("my", "MM")
	
	/**
	 * Mazanderani
	 */
	public val mzn: Locale get() = Locale.of("mzn")
	
	/**
	 * Mazanderani (Iran)
	 */
	public val mzn_IR: Locale get() = Locale.of("mzn", "IR")
	
	/**
	 * Nama
	 */
	public val naq: Locale get() = Locale.of("naq")
	
	/**
	 * Nama (Latin, Namibia)
	 */
	public val naq_NA: Locale get() = Locale.of("naq", "NA")
	
	/**
	 * Norwegian Bokmål
	 */
	public val nb: Locale get() = Locale.of("nb")
	
	/**
	 * Norwegian Bokmål (Norway)
	 */
	public val nb_NO: Locale get() = Locale.of("nb", "NO")
	
	/**
	 * Norwegian Bokmål (Svalbard & Jan Mayen)
	 */
	public val nb_SJ: Locale get() = Locale.of("nb", "SJ")
	
	/**
	 * North Ndebele
	 */
	public val nd: Locale get() = Locale.of("nd")
	
	/**
	 * North Ndebele (Latin, Zimbabwe)
	 */
	public val nd_ZW: Locale get() = Locale.of("nd", "ZW")
	
	/**
	 * Low German
	 */
	public val nds: Locale get() = Locale.of("nds")
	
	/**
	 * Low German (Germany)
	 */
	public val nds_DE: Locale get() = Locale.of("nds", "DE")
	
	/**
	 * Low German (Netherlands)
	 */
	public val nds_NL: Locale get() = Locale.of("nds", "NL")
	
	/**
	 * Nepali
	 */
	public val ne: Locale get() = Locale.of("ne")
	
	/**
	 * Nepali (India)
	 */
	public val ne_IN: Locale get() = Locale.of("ne", "IN")
	
	/**
	 * Nepali (Devanagari, Nepal)
	 */
	public val ne_NP: Locale get() = Locale.of("ne", "NP")
	
	/**
	 * Dutch
	 */
	public val nl: Locale get() = Locale.of("nl")
	
	/**
	 * Dutch (Aruba)
	 */
	public val nl_AW: Locale get() = Locale.of("nl", "AW")
	
	/**
	 * Dutch (Belgium)
	 */
	public val nl_BE: Locale get() = Locale.of("nl", "BE")
	
	/**
	 * Dutch (Caribbean Netherlands)
	 */
	public val nl_BQ: Locale get() = Locale.of("nl", "BQ")
	
	/**
	 * Dutch (Curaçao)
	 */
	public val nl_CW: Locale get() = Locale.of("nl", "CW")
	
	/**
	 * Dutch (Netherlands)
	 */
	public val nl_NL: Locale get() = Locale.of("nl", "NL")
	
	/**
	 * Dutch (Suriname)
	 */
	public val nl_SR: Locale get() = Locale.of("nl", "SR")
	
	/**
	 * Dutch (Sint Maarten)
	 */
	public val nl_SX: Locale get() = Locale.of("nl", "SX")
	
	/**
	 * Kwasio
	 */
	public val nmg: Locale get() = Locale.of("nmg")
	
	/**
	 * Kwasio (Cameroon)
	 */
	public val nmg_CM: Locale get() = Locale.of("nmg", "CM")
	
	/**
	 * Norwegian Nynorsk
	 */
	public val nn: Locale get() = Locale.of("nn")
	
	/**
	 * Norwegian Nynorsk (Latin, Norway)
	 */
	public val nn_NO: Locale get() = Locale.of("nn", "NO")
	
	/**
	 * Ngiemboon
	 */
	public val nnh: Locale get() = Locale.of("nnh")
	
	/**
	 * Ngiemboon (Cameroon)
	 */
	public val nnh_CM: Locale get() = Locale.of("nnh", "CM")
	
	/**
	 * Norwegian
	 */
	public val no: Locale get() = Locale.of("no")
	
	/**
	 * Norwegian (Latin, Norway)
	 */
	public val no_NO: Locale get() = Locale.of("no", "NO")
	
	/**
	 * Norwegian (Norway, Nynorsk)
	 */
	public val no_NO_NY: Locale get() = Locale.of("no", "NO", "NY")
	
	/**
	 * Nuer
	 */
	public val nus: Locale get() = Locale.of("nus")
	
	/**
	 * Nuer (Latin, South Sudan)
	 */
	public val nus_SS: Locale get() = Locale.of("nus", "SS")
	
	/**
	 * Nyankole
	 */
	public val nyn: Locale get() = Locale.of("nyn")
	
	/**
	 * Nyankole (Uganda)
	 */
	public val nyn_UG: Locale get() = Locale.of("nyn", "UG")
	
	/**
	 * Occitan
	 */
	public val oc: Locale get() = Locale.of("oc")
	
	/**
	 * Occitan (Spain)
	 */
	public val oc_ES: Locale get() = Locale.of("oc", "ES")
	
	/**
	 * Occitan (France)
	 */
	public val oc_FR: Locale get() = Locale.of("oc", "FR")
	
	/**
	 * Oromo
	 */
	public val om: Locale get() = Locale.of("om")
	
	/**
	 * Oromo (Latin, Ethiopia)
	 */
	public val om_ET: Locale get() = Locale.of("om", "ET")
	
	/**
	 * Oromo (Kenya)
	 */
	public val om_KE: Locale get() = Locale.of("om", "KE")
	
	/**
	 * Odia
	 */
	public val or: Locale get() = Locale.of("or")
	
	/**
	 * Odia (India)
	 */
	public val or_IN: Locale get() = Locale.of("or", "IN")
	
	/**
	 * Ossetic
	 */
	public val os: Locale get() = Locale.of("os")
	
	/**
	 * Ossetic (Cyrillic, Georgia)
	 */
	public val os_GE: Locale get() = Locale.of("os", "GE")
	
	/**
	 * Ossetic (Russia)
	 */
	public val os_RU: Locale get() = Locale.of("os", "RU")
	
	/**
	 * Punjabi (Gurmukhi)
	 */
	public val pa: Locale get() = Locale.of("pa")
	
	/**
	 * Punjabi (Gurmukhi, India)
	 */
	public val pa_IN: Locale get() = Locale.of("pa", "IN")
	
	/**
	 * Punjabi (Pakistan)
	 */
	public val pa_PK: Locale get() = Locale.of("pa", "PK")
	
	/**
	 * Nigerian Pidgin
	 */
	public val pcm: Locale get() = Locale.of("pcm")
	
	/**
	 * Nigerian Pidgin (Latin, Nigeria)
	 */
	public val pcm_NG: Locale get() = Locale.of("pcm", "NG")
	
	/**
	 * Pijin
	 */
	public val pis: Locale get() = Locale.of("pis")
	
	/**
	 * Pijin (Latin, Solomon Islands)
	 */
	public val pis_SB: Locale get() = Locale.of("pis", "SB")
	
	/**
	 * Polish
	 */
	public val pl: Locale get() = Locale.of("pl")
	
	/**
	 * Polish (Poland)
	 */
	public val pl_PL: Locale get() = Locale.of("pl", "PL")
	
	/**
	 * Pashto
	 */
	public val ps: Locale get() = Locale.of("ps")
	
	/**
	 * Pashto (Arabic, Afghanistan)
	 */
	public val ps_AF: Locale get() = Locale.of("ps", "AF")
	
	/**
	 * Pashto (Pakistan)
	 */
	public val ps_PK: Locale get() = Locale.of("ps", "PK")
	
	/**
	 * Portuguese
	 */
	public val pt: Locale get() = Locale.of("pt")
	
	/**
	 * Portuguese (Angola)
	 */
	public val pt_AO: Locale get() = Locale.of("pt", "AO")
	
	/**
	 * Portuguese (Latin, Brazil)
	 */
	public val pt_BR: Locale get() = Locale.of("pt", "BR")
	
	/**
	 * Portuguese (Switzerland)
	 */
	public val pt_CH: Locale get() = Locale.of("pt", "CH")
	
	/**
	 * Portuguese (Cape Verde)
	 */
	public val pt_CV: Locale get() = Locale.of("pt", "CV")
	
	/**
	 * Portuguese (Equatorial Guinea)
	 */
	public val pt_GQ: Locale get() = Locale.of("pt", "GQ")
	
	/**
	 * Portuguese (Guinea-Bissau)
	 */
	public val pt_GW: Locale get() = Locale.of("pt", "GW")
	
	/**
	 * Portuguese (Luxembourg)
	 */
	public val pt_LU: Locale get() = Locale.of("pt", "LU")
	
	/**
	 * Portuguese (Macao SAR China)
	 */
	public val pt_MO: Locale get() = Locale.of("pt", "MO")
	
	/**
	 * Portuguese (Mozambique)
	 */
	public val pt_MZ: Locale get() = Locale.of("pt", "MZ")
	
	/**
	 * Portuguese (Portugal)
	 */
	public val pt_PT: Locale get() = Locale.of("pt", "PT")
	
	/**
	 * Portuguese (São Tomé & Príncipe)
	 */
	public val pt_ST: Locale get() = Locale.of("pt", "ST")
	
	/**
	 * Portuguese (Timor-Leste)
	 */
	public val pt_TL: Locale get() = Locale.of("pt", "TL")
	
	/**
	 * Quechua
	 */
	public val qu: Locale get() = Locale.of("qu")
	
	/**
	 * Quechua (Bolivia)
	 */
	public val qu_BO: Locale get() = Locale.of("qu", "BO")
	
	/**
	 * Quechua (Ecuador)
	 */
	public val qu_EC: Locale get() = Locale.of("qu", "EC")
	
	/**
	 * Quechua (Peru)
	 */
	public val qu_PE: Locale get() = Locale.of("qu", "PE")
	
	/**
	 * Rajasthani
	 */
	public val raj: Locale get() = Locale.of("raj")
	
	/**
	 * Rajasthani (Devanagari, India)
	 */
	public val raj_IN: Locale get() = Locale.of("raj", "IN")
	
	/**
	 * Romansh
	 */
	public val rm: Locale get() = Locale.of("rm")
	
	/**
	 * Romansh (Latin, Switzerland)
	 */
	public val rm_CH: Locale get() = Locale.of("rm", "CH")
	
	/**
	 * Rundi
	 */
	public val rn: Locale get() = Locale.of("rn")
	
	/**
	 * Rundi (Latin, Burundi)
	 */
	public val rn_BI: Locale get() = Locale.of("rn", "BI")
	
	/**
	 * Romanian
	 */
	public val ro: Locale get() = Locale.of("ro")
	
	/**
	 * Romanian (Moldova)
	 */
	public val ro_MD: Locale get() = Locale.of("ro", "MD")
	
	/**
	 * Romanian (Romania)
	 */
	public val ro_RO: Locale get() = Locale.of("ro", "RO")
	
	/**
	 * Rombo
	 */
	public val rof: Locale get() = Locale.of("rof")
	
	/**
	 * Rombo (Latin, Tanzania)
	 */
	public val rof_TZ: Locale get() = Locale.of("rof", "TZ")
	
	/**
	 * Russian
	 */
	public val ru: Locale get() = Locale.of("ru")
	
	/**
	 * Russian (Belarus)
	 */
	public val ru_BY: Locale get() = Locale.of("ru", "BY")
	
	/**
	 * Russian (Kyrgyzstan)
	 */
	public val ru_KG: Locale get() = Locale.of("ru", "KG")
	
	/**
	 * Russian (Kazakhstan)
	 */
	public val ru_KZ: Locale get() = Locale.of("ru", "KZ")
	
	/**
	 * Russian (Moldova)
	 */
	public val ru_MD: Locale get() = Locale.of("ru", "MD")
	
	/**
	 * Russian (Russia)
	 */
	public val ru_RU: Locale get() = Locale.of("ru", "RU")
	
	/**
	 * Russian (Ukraine)
	 */
	public val ru_UA: Locale get() = Locale.of("ru", "UA")
	
	/**
	 * Kinyarwanda
	 */
	public val rw: Locale get() = Locale.of("rw")
	
	/**
	 * Kinyarwanda (Latin, Rwanda)
	 */
	public val rw_RW: Locale get() = Locale.of("rw", "RW")
	
	/**
	 * Rwa
	 */
	public val rwk: Locale get() = Locale.of("rwk")
	
	/**
	 * Rwa (Latin, Tanzania)
	 */
	public val rwk_TZ: Locale get() = Locale.of("rwk", "TZ")
	
	/**
	 * Sanskrit
	 */
	public val sa: Locale get() = Locale.of("sa")
	
	/**
	 * Sanskrit (Devanagari, India)
	 */
	public val sa_IN: Locale get() = Locale.of("sa", "IN")
	
	/**
	 * Yakut
	 */
	public val sah: Locale get() = Locale.of("sah")
	
	/**
	 * Yakut (Cyrillic, Russia)
	 */
	public val sah_RU: Locale get() = Locale.of("sah", "RU")
	
	/**
	 * Samburu
	 */
	public val saq: Locale get() = Locale.of("saq")
	
	/**
	 * Samburu (Kenya)
	 */
	public val saq_KE: Locale get() = Locale.of("saq", "KE")
	
	/**
	 * Santali
	 */
	public val sat: Locale get() = Locale.of("sat")
	
	/**
	 * Santali (India)
	 */
	public val sat_IN: Locale get() = Locale.of("sat", "IN")
	
	/**
	 * Sangu
	 */
	public val sbp: Locale get() = Locale.of("sbp")
	
	/**
	 * Sangu (Latin, Tanzania)
	 */
	public val sbp_TZ: Locale get() = Locale.of("sbp", "TZ")
	
	/**
	 * Sardinian
	 */
	public val sc: Locale get() = Locale.of("sc")
	
	/**
	 * Sardinian (Italy)
	 */
	public val sc_IT: Locale get() = Locale.of("sc", "IT")
	
	/**
	 * Sindhi (Arabic)
	 */
	public val sd: Locale get() = Locale.of("sd")
	
	/**
	 * Sindhi (India)
	 */
	public val sd_IN: Locale get() = Locale.of("sd", "IN")
	
	/**
	 * Sindhi (Arabic, Pakistan)
	 */
	public val sd_PK: Locale get() = Locale.of("sd", "PK")
	
	/**
	 * Northern Sami
	 */
	public val se: Locale get() = Locale.of("se")
	
	/**
	 * Northern Sami (Finland)
	 */
	public val se_FI: Locale get() = Locale.of("se", "FI")
	
	/**
	 * Northern Sami (Latin, Norway)
	 */
	public val se_NO: Locale get() = Locale.of("se", "NO")
	
	/**
	 * Northern Sami (Sweden)
	 */
	public val se_SE: Locale get() = Locale.of("se", "SE")
	
	/**
	 * Sena
	 */
	public val seh: Locale get() = Locale.of("seh")
	
	/**
	 * Sena (Mozambique)
	 */
	public val seh_MZ: Locale get() = Locale.of("seh", "MZ")
	
	/**
	 * Koyraboro Senni
	 */
	public val ses: Locale get() = Locale.of("ses")
	
	/**
	 * Koyraboro Senni (Mali)
	 */
	public val ses_ML: Locale get() = Locale.of("ses", "ML")
	
	/**
	 * Sango
	 */
	public val sg: Locale get() = Locale.of("sg")
	
	/**
	 * Sango (Latin, Central African Republic)
	 */
	public val sg_CF: Locale get() = Locale.of("sg", "CF")
	
	/**
	 * Tachelhit (Tifinagh)
	 */
	public val shi: Locale get() = Locale.of("shi")
	
	/**
	 * Tachelhit (Tifinagh, Morocco)
	 */
	public val shi_MA: Locale get() = Locale.of("shi", "MA")
	
	/**
	 * Sinhala
	 */
	public val si: Locale get() = Locale.of("si")
	
	/**
	 * Sinhala (Sinhala, Sri Lanka)
	 */
	public val si_LK: Locale get() = Locale.of("si", "LK")
	
	/**
	 * Slovak
	 */
	public val sk: Locale get() = Locale.of("sk")
	
	/**
	 * Slovak (Slovakia)
	 */
	public val sk_SK: Locale get() = Locale.of("sk", "SK")
	
	/**
	 * Slovenian
	 */
	public val sl: Locale get() = Locale.of("sl")
	
	/**
	 * Slovenian (Slovenia)
	 */
	public val sl_SI: Locale get() = Locale.of("sl", "SI")
	
	/**
	 * Inari Sami
	 */
	public val smn: Locale get() = Locale.of("smn")
	
	/**
	 * Inari Sami (Latin, Finland)
	 */
	public val smn_FI: Locale get() = Locale.of("smn", "FI")
	
	/**
	 * Skolt Sami
	 */
	public val sms: Locale get() = Locale.of("sms")
	
	/**
	 * Skolt Sami (Finland)
	 */
	public val sms_FI: Locale get() = Locale.of("sms", "FI")
	
	/**
	 * Shona
	 */
	public val sn: Locale get() = Locale.of("sn")
	
	/**
	 * Shona (Zimbabwe)
	 */
	public val sn_ZW: Locale get() = Locale.of("sn", "ZW")
	
	/**
	 * Somali
	 */
	public val so: Locale get() = Locale.of("so")
	
	/**
	 * Somali (Djibouti)
	 */
	public val so_DJ: Locale get() = Locale.of("so", "DJ")
	
	/**
	 * Somali (Ethiopia)
	 */
	public val so_ET: Locale get() = Locale.of("so", "ET")
	
	/**
	 * Somali (Kenya)
	 */
	public val so_KE: Locale get() = Locale.of("so", "KE")
	
	/**
	 * Somali (Latin, Somalia)
	 */
	public val so_SO: Locale get() = Locale.of("so", "SO")
	
	/**
	 * Albanian
	 */
	public val sq: Locale get() = Locale.of("sq")
	
	/**
	 * Albanian (Albania)
	 */
	public val sq_AL: Locale get() = Locale.of("sq", "AL")
	
	/**
	 * Albanian (North Macedonia)
	 */
	public val sq_MK: Locale get() = Locale.of("sq", "MK")
	
	/**
	 * Albanian (Kosovo)
	 */
	public val sq_XK: Locale get() = Locale.of("sq", "XK")
	
	/**
	 * Serbian (Cyrillic)
	 */
	public val sr: Locale get() = Locale.of("sr")
	
	/**
	 * Serbian (Cyrillic, Bosnia & Herzegovina)
	 */
	public val sr_BA: Locale get() = Locale.of("sr", "BA")
	
	/**
	 * Serbian (Serbia and Montenegro)
	 */
	public val sr_CS: Locale get() = Locale.of("sr", "CS")
	
	/**
	 * Serbian (Cyrillic, Montenegro)
	 */
	public val sr_ME: Locale get() = Locale.of("sr", "ME")
	
	/**
	 * Serbian (Serbia)
	 */
	public val sr_RS: Locale get() = Locale.of("sr", "RS")
	
	/**
	 * Serbian (Cyrillic, Kosovo)
	 */
	public val sr_XK: Locale get() = Locale.of("sr", "XK")
	
	/**
	 * Sundanese (Latin)
	 */
	public val su: Locale get() = Locale.of("su")
	
	/**
	 * Sundanese (Latin, Indonesia)
	 */
	public val su_ID: Locale get() = Locale.of("su", "ID")
	
	/**
	 * Swedish
	 */
	public val sv: Locale get() = Locale.of("sv")
	
	/**
	 * Swedish (Åland Islands)
	 */
	public val sv_AX: Locale get() = Locale.of("sv", "AX")
	
	/**
	 * Swedish (Finland)
	 */
	public val sv_FI: Locale get() = Locale.of("sv", "FI")
	
	/**
	 * Swedish (Sweden)
	 */
	public val sv_SE: Locale get() = Locale.of("sv", "SE")
	
	/**
	 * Swahili
	 */
	public val sw: Locale get() = Locale.of("sw")
	
	/**
	 * Swahili (Congo - Kinshasa)
	 */
	public val sw_CD: Locale get() = Locale.of("sw", "CD")
	
	/**
	 * Swahili (Kenya)
	 */
	public val sw_KE: Locale get() = Locale.of("sw", "KE")
	
	/**
	 * Swahili (Latin, Tanzania)
	 */
	public val sw_TZ: Locale get() = Locale.of("sw", "TZ")
	
	/**
	 * Swahili (Uganda)
	 */
	public val sw_UG: Locale get() = Locale.of("sw", "UG")
	
	/**
	 * Tamil
	 */
	public val ta: Locale get() = Locale.of("ta")
	
	/**
	 * Tamil (Tamil, India)
	 */
	public val ta_IN: Locale get() = Locale.of("ta", "IN")
	
	/**
	 * Tamil (Sri Lanka)
	 */
	public val ta_LK: Locale get() = Locale.of("ta", "LK")
	
	/**
	 * Tamil (Malaysia)
	 */
	public val ta_MY: Locale get() = Locale.of("ta", "MY")
	
	/**
	 * Tamil (Singapore)
	 */
	public val ta_SG: Locale get() = Locale.of("ta", "SG")
	
	/**
	 * Telugu
	 */
	public val te: Locale get() = Locale.of("te")
	
	/**
	 * Telugu (India)
	 */
	public val te_IN: Locale get() = Locale.of("te", "IN")
	
	/**
	 * Teso
	 */
	public val teo: Locale get() = Locale.of("teo")
	
	/**
	 * Teso (Kenya)
	 */
	public val teo_KE: Locale get() = Locale.of("teo", "KE")
	
	/**
	 * Teso (Uganda)
	 */
	public val teo_UG: Locale get() = Locale.of("teo", "UG")
	
	/**
	 * Tajik
	 */
	public val tg: Locale get() = Locale.of("tg")
	
	/**
	 * Tajik (Tajikistan)
	 */
	public val tg_TJ: Locale get() = Locale.of("tg", "TJ")
	
	/**
	 * Thai
	 */
	public val th: Locale get() = Locale.of("th")
	
	/**
	 * Thai (Thai, Thailand)
	 */
	public val th_TH: Locale get() = Locale.of("th", "TH")
	
	/**
	 * Thai (Thailand, TH, Thai Digits)
	 */
	public val th_TH_TH: Locale get() = Locale.of("th", "TH", "TH")
	
	/**
	 * Tigrinya
	 */
	public val ti: Locale get() = Locale.of("ti")
	
	/**
	 * Tigrinya (Eritrea)
	 */
	public val ti_ER: Locale get() = Locale.of("ti", "ER")
	
	/**
	 * Tigrinya (Ethiopia)
	 */
	public val ti_ET: Locale get() = Locale.of("ti", "ET")
	
	/**
	 * Turkmen
	 */
	public val tk: Locale get() = Locale.of("tk")
	
	/**
	 * Turkmen (Latin, Turkmenistan)
	 */
	public val tk_TM: Locale get() = Locale.of("tk", "TM")
	
	/**
	 * Tongan
	 */
	public val to: Locale get() = Locale.of("to")
	
	/**
	 * Tongan (Latin, Tonga)
	 */
	public val to_TO: Locale get() = Locale.of("to", "TO")
	
	/**
	 * Toki Pona
	 */
	public val tok: Locale get() = Locale.of("tok")
	
	/**
	 * Toki Pona (Latin, world)
	 */
	public val tok_001: Locale get() = Locale.of("tok", "001")
	
	/**
	 * Turkish
	 */
	public val tr: Locale get() = Locale.of("tr")
	
	/**
	 * Turkish (Cyprus)
	 */
	public val tr_CY: Locale get() = Locale.of("tr", "CY")
	
	/**
	 * Turkish (Türkiye)
	 */
	public val tr_TR: Locale get() = Locale.of("tr", "TR")
	
	/**
	 * Tatar
	 */
	public val tt: Locale get() = Locale.of("tt")
	
	/**
	 * Tatar (Russia)
	 */
	public val tt_RU: Locale get() = Locale.of("tt", "RU")
	
	/**
	 * Tasawaq
	 */
	public val twq: Locale get() = Locale.of("twq")
	
	/**
	 * Tasawaq (Niger)
	 */
	public val twq_NE: Locale get() = Locale.of("twq", "NE")
	
	/**
	 * Central Atlas Tamazight
	 */
	public val tzm: Locale get() = Locale.of("tzm")
	
	/**
	 * Central Atlas Tamazight (Latin, Morocco)
	 */
	public val tzm_MA: Locale get() = Locale.of("tzm", "MA")
	
	/**
	 * Uyghur
	 */
	public val ug: Locale get() = Locale.of("ug")
	
	/**
	 * Uyghur (China)
	 */
	public val ug_CN: Locale get() = Locale.of("ug", "CN")
	
	/**
	 * Ukrainian
	 */
	public val uk: Locale get() = Locale.of("uk")
	
	/**
	 * Ukrainian (Cyrillic, Ukraine)
	 */
	public val uk_UA: Locale get() = Locale.of("uk", "UA")
	
	/**
	 * Urdu
	 */
	public val ur: Locale get() = Locale.of("ur")
	
	/**
	 * Urdu (India)
	 */
	public val ur_IN: Locale get() = Locale.of("ur", "IN")
	
	/**
	 * Urdu (Arabic, Pakistan)
	 */
	public val ur_PK: Locale get() = Locale.of("ur", "PK")
	
	/**
	 * Uzbek
	 */
	public val uz: Locale get() = Locale.of("uz")
	
	/**
	 * Uzbek (Afghanistan)
	 */
	public val uz_AF: Locale get() = Locale.of("uz", "AF")
	
	/**
	 * Uzbek (Latin, Uzbekistan)
	 */
	public val uz_UZ: Locale get() = Locale.of("uz", "UZ")
	
	/**
	 * Vai (Vai)
	 */
	public val vai: Locale get() = Locale.of("vai")
	
	/**
	 * Vai (Vai, Liberia)
	 */
	public val vai_LR: Locale get() = Locale.of("vai", "LR")
	
	/**
	 * Vietnamese
	 */
	public val vi: Locale get() = Locale.of("vi")
	
	/**
	 * Vietnamese (Vietnam)
	 */
	public val vi_VN: Locale get() = Locale.of("vi", "VN")
	
	/**
	 * Vunjo
	 */
	public val vun: Locale get() = Locale.of("vun")
	
	/**
	 * Vunjo (Tanzania)
	 */
	public val vun_TZ: Locale get() = Locale.of("vun", "TZ")
	
	/**
	 * Walser
	 */
	public val wae: Locale get() = Locale.of("wae")
	
	/**
	 * Walser (Latin, Switzerland)
	 */
	public val wae_CH: Locale get() = Locale.of("wae", "CH")
	
	/**
	 * Wolof
	 */
	public val wo: Locale get() = Locale.of("wo")
	
	/**
	 * Wolof (Senegal)
	 */
	public val wo_SN: Locale get() = Locale.of("wo", "SN")
	
	/**
	 * Xhosa
	 */
	public val xh: Locale get() = Locale.of("xh")
	
	/**
	 * Xhosa (South Africa)
	 */
	public val xh_ZA: Locale get() = Locale.of("xh", "ZA")
	
	/**
	 * Soga
	 */
	public val xog: Locale get() = Locale.of("xog")
	
	/**
	 * Soga (Latin, Uganda)
	 */
	public val xog_UG: Locale get() = Locale.of("xog", "UG")
	
	/**
	 * Yangben
	 */
	public val yav: Locale get() = Locale.of("yav")
	
	/**
	 * Yangben (Latin, Cameroon)
	 */
	public val yav_CM: Locale get() = Locale.of("yav", "CM")
	
	/**
	 * Yiddish
	 */
	public val yi: Locale get() = Locale.of("yi")
	
	/**
	 * Yiddish (Hebrew, world)
	 */
	public val yi_001: Locale get() = Locale.of("yi", "001")
	
	/**
	 * Yoruba
	 */
	public val yo: Locale get() = Locale.of("yo")
	
	/**
	 * Yoruba (Benin)
	 */
	public val yo_BJ: Locale get() = Locale.of("yo", "BJ")
	
	/**
	 * Yoruba (Nigeria)
	 */
	public val yo_NG: Locale get() = Locale.of("yo", "NG")
	
	/**
	 * Nheengatu
	 */
	public val yrl: Locale get() = Locale.of("yrl")
	
	/**
	 * Nheengatu (Brazil)
	 */
	public val yrl_BR: Locale get() = Locale.of("yrl", "BR")
	
	/**
	 * Nheengatu (Colombia)
	 */
	public val yrl_CO: Locale get() = Locale.of("yrl", "CO")
	
	/**
	 * Nheengatu (Venezuela)
	 */
	public val yrl_VE: Locale get() = Locale.of("yrl", "VE")
	
	/**
	 * Cantonese (Simplified)
	 */
	public val yue: Locale get() = Locale.of("yue")
	
	/**
	 * Cantonese (Simplified, China)
	 */
	public val yue_CN: Locale get() = Locale.of("yue", "CN")
	
	/**
	 * Cantonese (Hong Kong SAR China)
	 */
	public val yue_HK: Locale get() = Locale.of("yue", "HK")
	
	/**
	 * Standard Moroccan Tamazight
	 */
	public val zgh: Locale get() = Locale.of("zgh")
	
	/**
	 * Standard Moroccan Tamazight (Tifinagh, Morocco)
	 */
	public val zgh_MA: Locale get() = Locale.of("zgh", "MA")
	
	/**
	 * Chinese (Simplified)
	 */
	public val zh: Locale get() = Locale.of("zh")
	
	/**
	 * Chinese (China)
	 */
	public val zh_CN: Locale get() = Locale.of("zh", "CN")
	
	/**
	 * Chinese (Simplified, Hong Kong SAR China)
	 */
	public val zh_HK: Locale get() = Locale.of("zh", "HK")
	
	/**
	 * Chinese (Macao SAR China)
	 */
	public val zh_MO: Locale get() = Locale.of("zh", "MO")
	
	/**
	 * Chinese (Simplified, Singapore)
	 */
	public val zh_SG: Locale get() = Locale.of("zh", "SG")
	
	/**
	 * Chinese (Taiwan)
	 */
	public val zh_TW: Locale get() = Locale.of("zh", "TW")
	
	/**
	 * Zulu
	 */
	public val zu: Locale get() = Locale.of("zu")
	
	/**
	 * Zulu (South Africa)
	 */
	public val zu_ZA: Locale get() = Locale.of("zu", "ZA")
}
