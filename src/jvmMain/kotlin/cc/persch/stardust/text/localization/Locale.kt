/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.localization

import cc.persch.stardust.annotations.Pure

public actual typealias Locale = java.util.Locale

@Pure
internal actual fun __localeOf(language: String, country: String, variant: String): Locale =
	Locale.of(language, country, variant)

@Pure
internal actual fun __localeOf(language: String, country: String): Locale = __localeOf(language, country, "")

@Pure
internal actual fun __localeOf(language: String): Locale = __localeOf(language, "", "")

internal actual val Locale.__language: String
	get() = language

internal actual val Locale.__country: String
	get() = country

internal actual val Locale.__variant: String
	get() = variant
