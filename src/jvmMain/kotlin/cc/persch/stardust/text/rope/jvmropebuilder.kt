/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.pipe
import cc.persch.stardust.text.localization.Locale
import cc.persch.stardust.text.localization.LocalePair
import java.math.BigDecimal
import java.math.BigInteger
import java.text.Format
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PLURAL for BigInteger and BigDecimal
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Pure
private fun BigInteger.toPluralValue() = if(abs() == BigInteger.ONE) 1.0 else 0.0

@Pure
private fun BigDecimal.toPluralValue() = if(abs() == BigDecimal.ONE) 1.0 else 0.0

/**
 * Adds [plural], if [count] is unequal to `-1` and `1`.
 */
public fun RopeBuilder.plural(count: BigInteger, plural: CharSequence): Unit = plural(count, "", plural)

/**
 * Adds [plural], if [count] is unequal to `-1` and `1`.
 */
public fun RopeBuilder.plural(count: BigDecimal, plural: CharSequence): Unit = plural(count, "", plural)

/**
 * Adds [singular], if [count] is `-1` or `1`; otherwise, [plural].
 */
public fun RopeBuilder.plural(count: BigInteger, singular: CharSequence, plural: CharSequence): Unit =
	plural(count.toPluralValue(), singular, plural)

/**
 * Adds [singular], if [count] is `-1` or `1`; otherwise, [plural].
 */
public fun RopeBuilder.plural(count: BigDecimal, singular: CharSequence, plural: CharSequence): Unit =
	plural(count.toPluralValue(), singular, plural)



/**
 * Adds the result of [pluralBuilder], if [count] is unequal to `-1` and `1`.
 */
public fun RopeBuilder.plural(count: BigInteger, pluralBuilder: RopeBuilder.() -> Unit): Unit =
	plural(count.toPluralValue(), pluralBuilder)

/**
 * Adds the result of [pluralBuilder], if [count] is unequal to `-1` and `1`.
 */
public fun RopeBuilder.plural(count: BigDecimal, pluralBuilder: RopeBuilder.() -> Unit): Unit =
	plural(count.toPluralValue(), pluralBuilder)


/**
 * Adds the result of [singularBuilder], if [count] is `-1` or `1`; otherwise, the result of [pluralBuilder].
 */
public fun RopeBuilder.plural(
	count: BigInteger,
	singularBuilder: RopeBuilder.() -> Unit,
	pluralBuilder: RopeBuilder.() -> Unit
) : Unit = plural(count.toPluralValue(), singularBuilder, pluralBuilder)

/**
 * Adds the result of [singularBuilder], if [count] is `-1` or `1`; otherwise, the result of [pluralBuilder].
 */
public fun RopeBuilder.plural(
	count: BigDecimal,
	singularBuilder: RopeBuilder.() -> Unit,
	pluralBuilder: RopeBuilder.() -> Unit
) : Unit = plural(count.toPluralValue(), singularBuilder, pluralBuilder)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FORMATS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Formats the [number] using the [format].
 */
public fun RopeBuilder.format(number: Number, format: Format): Unit = append(format.format(number))

/**
 * Formats the [temporal accessor][temporal] using the [format].
 */
public fun RopeBuilder.format(temporal: TemporalAccessor, formatter: DateTimeFormatter): Unit =
	append(formatter.format(temporal))

/**
 * Creates a common format factory environment using the [format locale][LocalePair.format]
 * of the given [LocalePair][locales].
 *
 * Example:
 *
 * ```
 * rope {
 *
 *     formatterOf(locales) {
 *
 *         +format(value, formats.numbers.fullFixed(2))
 *     }
 * }
 * ```
 */
@Pure
@TypeSafeBuilder
public inline fun RopeBuilder.formatterOf(
	locales: LocalePair,
	formatter: Formatter.() -> Unit
) : Unit = formatter(locales.format, formatter)


/**
 * Creates a common format factory environment using the given [locale].
 *
 * Example:
 *
 * ```
 * rope {
 * 
 *     formatter(Locales.en_US) {
 *     
 *         +format(value, formats.numbers.fullFixed(2))
 *     }
 * }
 * ```
 */
@Suppress("UnusedReceiverParameter")
@Pure
@TypeSafeBuilder
public inline fun RopeBuilder.formatter(
	locale: Locale,
	formatter: Formatter.() -> Unit
) : Unit = Formatter(locale) pipe formatter
