/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.formats.CommonFormatFactory
import cc.persch.stardust.text.localization.Locale
import cc.persch.stardust.text.localization.LocalePair

/**
 * Defines a format factory provider for a given format locale.
 *
 * @since 4.0
 * 
 * @constructor Creates a formatter factory provider using the given [locale].
 *
 * @property formats Gets a provider for factories of common formats.
 */
public open class Formatter(locale: Locale) {
	
	public val formats: CommonFormatFactory = CommonFormatFactory(locale)
	
	public companion object {
		
		/**
		 * Creates a formatter factory provider using the [format locale][LocalePair.format]
		 * of the given [LocalePair][locales].
		 */
		@Pure
		public fun of(locales: LocalePair): Formatter = Formatter(locales.format)
	}
}
