/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.formats

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.formats.HexadecimalFormat.Companion.getHexadecimalInstance
import cc.persch.stardust.text.formats.HexadecimalFormat.Option.LOWER_CASE
import cc.persch.stardust.text.formats.HexadecimalFormat.Option.UPPER_CASE
import cc.persch.stardust.text.localization.Locales
import java.math.BigDecimal
import java.math.BigInteger
import java.text.FieldPosition
import java.text.NumberFormat
import java.text.ParsePosition
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.roundToLong
import kotlin.text.RegexOption.IGNORE_CASE

/**
 * Defines a format for hexadecimal output of numbers. The hexadecimal format output resp. parse input value is treated
 * as unsigned.
 *
 * Examples:
 *
 * ```
 * 47110815  → "2ceda9f"
 * -47110815 → "fffffffffd312561"
 * ```
 *
 * @since 1.0
 *
 * @constructor Creates a new instance with the specified [option].
 *
 *   *Note: Better use the cached instances via [getHexadecimalInstance].*
 */
public class HexadecimalFormat(public val option: Option): NumberFormat() {
	
	/**
	 * Defines the available hexadecimal format options.
	 */
	public enum class Option {
		
		/**
		 * Lower case.
		 */
		LOWER_CASE {
			
			@Pure
			override fun transform(value: String) = value
		},
		
		/**
		 * Upper case.
		 */
		UPPER_CASE {
			
			@Pure
			override fun transform(value: String) = value.uppercase(Locales.invariant)
		};
		
		@Pure
		internal abstract fun transform(value: String): String
	}
	
	@Pure
	override fun format(number: Double, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		this.format(number.roundToLong(), toAppendTo, pos)
	
	@Pure
	override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
		
		if(obj !is Number)
			return toAppendTo
		
		if(obj is BigInteger)
			return toAppendTo.append(option.transform(obj.toString(16)))
		if(obj is BigDecimal)
			return toAppendTo.append(option.transform(obj.toBigInteger().toString(16)))
		
		val value = when(obj) {
			
			is Byte -> java.lang.Byte.toUnsignedLong(obj)
			is Short -> java.lang.Short.toUnsignedLong(obj)
			is Int, is AtomicInteger -> Integer.toUnsignedLong(obj.toInt())
			else -> obj.toLong()
		}
		
		return toAppendTo.append(option.transform(value.toULong().toString(16)))
	}
	
	@Pure
	override fun format(number: Long, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		this.format(number as Any, toAppendTo, pos)
	
	@Pure
	override fun parse(source: String, parsePosition: ParsePosition): Number? {
		
		val result = HEX_REGEX.find(source, parsePosition.index) ?: return null
		
		val value = java.lang.Long.parseUnsignedLong(result.value, 16)
		
		parsePosition.index = parsePosition.index + result.value.length
		
		return value
	}
	
	public companion object {
		
		private val HEX_REGEX by lazy { Regex("""\A[0-9a-f]+""", IGNORE_CASE) }
		
		private val LOWER_HEX_FORMAT = HexadecimalFormat(LOWER_CASE)
		private val UPPER_HEX_FORMAT = HexadecimalFormat(UPPER_CASE)
		
		/**
		 * Gets hexadecimal formats using the given hexadecimal format.
		 */
		@Cached
		@Pure
		public fun getHexadecimalInstance(option: Option = LOWER_CASE): HexadecimalFormat =
			if(option === LOWER_CASE) LOWER_HEX_FORMAT else UPPER_HEX_FORMAT
	}
}
