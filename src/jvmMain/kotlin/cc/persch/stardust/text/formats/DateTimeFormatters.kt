/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.formats

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.localization.Locales
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder

/**
 * Defines functions to create a [DateTimeFormatter].
 *
 * @since 1.0
 * @see DateTimeFormatter
 */
public object DateTimeFormatters {
	
	private val TIME: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("HH:mm:ss", Locales.invariant)
	}
	
	private val TIME_MS: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("HH:mm:ss.SSS", Locales.invariant)
	}
	
	private val INVARIANT_DATETIME: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss O", Locales.invariant)
	}
	
	private val INVARIANT_DATETIME_MS: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss.SSS O", Locales.invariant)
	}
	
	private val SORTABLE_DATETIME: DateTimeFormatter by lazy {
		
		DateTimeFormatterBuilder()
			.parseCaseInsensitive()
			.appendInstant(0)
			.toFormatter(Locales.invariant)
	}
	
	private val SORTABLE_DATETIME_MS: DateTimeFormatter by lazy {
		
		DateTimeFormatterBuilder()
			.parseCaseInsensitive()
			.appendInstant(3)
			.toFormatter(Locales.invariant)
	}
	
	private val INVARIANT_DATE: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("uuuu-MM-dd", Locales.invariant)
	}
	
	private val SORTABLE_DATE: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("uuuu-MM-dd'T00:00:00Z'", Locales.invariant)
	}
	
	private val SORTABLE_DATE_MS: DateTimeFormatter by lazy {
		
		DateTimeFormatter.ofPattern("uuuu-MM-dd'T00:00:00.000Z'", Locales.invariant)
	}
	
	/**
	 * Time format, e.g. `"21:04:28"`.
	 *
	 * @param showMillis `true` adds milliseconds.
	 * @return The format.
	 */
	@Cached
	@Pure
	public fun time(showMillis: Boolean = false): DateTimeFormatter = if(showMillis) TIME_MS else TIME
	
	/**
	 * Invariant format, e.g. `"2013-03-23 21:04:28:827"`.
	 *
	 * @param showMillis `true` adds milliseconds.
	 * @return The format.
	 */
	@Cached
	@Pure
	public fun invariantDateTime(showMillis: Boolean = false): DateTimeFormatter =
		if(showMillis) INVARIANT_DATETIME_MS else INVARIANT_DATETIME
	
	/**
	 * Sortable format, e.g. `"2013-03-23T21:04:28:827Z"`.
	 *
	 * @param showMillis `true` adds milliseconds.
	 * @return The format.
	 */
	@Cached
	@Pure
	public fun sortableDateTime(showMillis: Boolean = false): DateTimeFormatter =
		if(showMillis) SORTABLE_DATETIME_MS else SORTABLE_DATETIME
	
	/**
	 * Invariant format, e.g. `"2013-03-23"`.
	 *
	 * @return The format.
	 */
	@Cached
	@Pure
	public fun invariantDate(): DateTimeFormatter = INVARIANT_DATE
	
	/**
	 * Sortable format, e.g. `"2013-03-23T00:00:00"`.
	 *
	 * @param showMillis `true` adds milliseconds.
	 * @return The format.
	 */
	@Cached
	@Pure
	public fun sortableDate(showMillis: Boolean = false): DateTimeFormatter =
		if(showMillis) SORTABLE_DATE_MS else SORTABLE_DATE
}
