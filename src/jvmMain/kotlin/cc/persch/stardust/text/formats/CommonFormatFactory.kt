/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.formats

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.formats.HexadecimalFormat.Option.LOWER_CASE
import cc.persch.stardust.text.localization.LocalePair
import cc.persch.stardust.text.localization.Locales
import cc.persch.stardust.annotations.threading.ThreadSafe
import java.text.DecimalFormat
import java.text.NumberFormat
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

/**
 * Defines a format provider for common (well-known) formats provided by Stardust and Java's API
 * for a certain [format locale][formatLocale].
 *
 * The created formats are cached and re-used on same format definition.
 *
 * @property formatLocale The format locale that is used by this factory.
 *
 */
@ThreadSafe
public open class CommonFormatFactory(public val formatLocale: Locale = Locales.defaultFormat) {
	
	/**
	 * Gets common number formats
	 */
	public val numbers: CommonNumberFormats = CommonNumberFormats()
	
	/**
	 * Gets common datetime formats
	 */
	public val dateTimes: CommonDateTimeFormats = CommonDateTimeFormats()
	
	/**
	 * @param locales The locales containing the [format locale][LocalePair.format] to be used by this factory.
	 */
	public constructor(locales: LocalePair): this(locales.format)
	
	@ThreadSafe
	public inner class CommonNumberFormats internal constructor() {
		
		/**
		 * Binary format
		 */
		public val binary: BinaryFormat get() = BinaryFormat
		
		/**
		 * Plain number format
		 */
		public val plainNumber: PlainNumberFormat get() = PlainNumberFormat
		
		/**
		 * Full dynamic format, e.g. `"12,345.67"`.
		 * 
		 * @see NumberFormats.fullDynamic
		 */
		@Pure
		public fun fullDynamic(maxDecimalDigits: Int): DecimalFormat =
			NumberFormats.fullDynamic(maxDecimalDigits, formatLocale)
		
		/**
		 * Full fixed format, e.g. `"12,345.670"`.
		 * 
		 * @see NumberFormats.fullFixed
		 */
		@Pure
		public fun fullFixed(decimalDigits: Int): DecimalFormat =
			NumberFormats.fullFixed(decimalDigits, formatLocale)
		
		/**
		 * Generic fixed format, e.g. `"12345.670"`.
		 *
		 * @see NumberFormats.genericDynamic
		 */
		@Pure
		public fun genericDynamic(maxDecimalDigits: Int): DecimalFormat =
			NumberFormats.genericDynamic(maxDecimalDigits, formatLocale)
		
		/**
		 * Generic fixed format, e.g. `"12345.670"`.
		 *
		 * @see NumberFormats.genericFixed
		 */
		@Pure
		public fun genericFixed(decimalDigits: Int): DecimalFormat =
			NumberFormats.genericFixed(decimalDigits, formatLocale)
		
		/**
		 * Hexadecimal formats using the given hexadecimal format.
		 * 
		 * @see HexadecimalFormat.getHexadecimalInstance
		 */
		@Cached
		@Pure
		public fun hexadecimal(option: HexadecimalFormat.Option = LOWER_CASE): HexadecimalFormat =
			HexadecimalFormat.getHexadecimalInstance(option)
		
		/**
		 * Currency number format
		 * 
		 * @see NumberFormat.getCurrencyInstance
		 */
		public val currency: NumberFormat by lazy { NumberFormat.getCurrencyInstance(formatLocale) }
		
		/**
		 * Default number format
		 *
		 * @see NumberFormat.getInstance
		 */
		public val default: NumberFormat by lazy { NumberFormat.getInstance(formatLocale) }
		
		/**
		 * Integer number format
		 *
		 * @see NumberFormat.getIntegerInstance
		 */
		public val integer: NumberFormat by lazy { NumberFormat.getIntegerInstance(formatLocale) }
		
		/**
		 * Number format
		 *
		 * @see NumberFormat.getNumberInstance
		 */
		public val number: NumberFormat by lazy { NumberFormat.getNumberInstance(formatLocale) }
		
		/**
		 * Percent number format
		 *
		 * @see NumberFormat.getPercentInstance
		 */
		public val percent: NumberFormat by lazy { NumberFormat.getPercentInstance(formatLocale) }
	}
	
	@ThreadSafe
	public inner class CommonDateTimeFormats internal constructor() {
		
		/**
		 * Time format, e.g. `"21:04:28"`.
		 * 
		 * @see DateTimeFormatters.time
		 */
		@Cached
		@Pure
		public fun time(showMillis: Boolean = false): DateTimeFormatter = DateTimeFormatters.time(showMillis)
		
		/**
		 * Invariant format, e.g. `"2013-03-23"`.
		 *
		 * @see DateTimeFormatters.invariantDate
		 */
		@Cached
		@Pure
		public fun invariantDate(): DateTimeFormatter = DateTimeFormatters.invariantDate()
		
		/**
		 * Invariant format, e.g. `"2013-03-23 21:04:28:827"`.
		 * 
		 * @see DateTimeFormatters.invariantDateTime
		 */
		@Cached
		@Pure
		public fun invariantDateTime(showMillis: Boolean = false): DateTimeFormatter =
			DateTimeFormatters.invariantDateTime(showMillis)
		
		/**
		 * Sortable format, e.g. `"2013-03-23T00:00:00"`.
		 *
		 * @see DateTimeFormatters.sortableDate
		 */
		@Cached
		@Pure
		public fun sortableDate(showMillis: Boolean = false): DateTimeFormatter =
			DateTimeFormatters.sortableDate(showMillis)
		
		/**
		 * Sortable format, e.g. `"2013-03-23T21:04:28:827Z"`.
		 *
		 * @see DateTimeFormatters.sortableDateTime
		 */
		@Cached
		@Pure
		public fun sortableDateTime(showMillis: Boolean = false): DateTimeFormatter =
			DateTimeFormatters.sortableDateTime(showMillis)
		
		/**
		 * Localized date format using the given [dateStyle].
		 *
		 * @see DateTimeFormatter.ofLocalizedDate
		 */
		@Pure
		public fun ofLocalizedDate(dateStyle: FormatStyle): DateTimeFormatter =
			DateTimeFormatter.ofLocalizedDate(dateStyle).withLocale(formatLocale)
		
		/**
		 * Localized datetime format using the given [dateTimeStyle].
		 *
		 * @see DateTimeFormatter.ofLocalizedDateTime
		 */
		@Pure
		public fun ofLocalizedDateTime(dateTimeStyle: FormatStyle): DateTimeFormatter =
			DateTimeFormatter.ofLocalizedDateTime(dateTimeStyle).withLocale(formatLocale)
		
		/**
		 * Localized datetime format using the given [dateStyle] and [timeStyle].
		 *
		 * @see DateTimeFormatter.ofLocalizedDateTime
		 */
		@Pure
		public fun ofLocalizedDateTime(dateStyle: FormatStyle, timeStyle: FormatStyle): DateTimeFormatter =
			DateTimeFormatter.ofLocalizedDateTime(dateStyle, timeStyle).withLocale(formatLocale)
		
		/**
		 * Localized datetime format using the given [timeStyle].
		 *
		 * @see DateTimeFormatter.ofLocalizedTime
		 */
		@Pure
		public fun ofLocalizedTime(timeStyle: FormatStyle): DateTimeFormatter =
			DateTimeFormatter.ofLocalizedTime(timeStyle).withLocale(formatLocale)
		
		/**
		 * Localized datetime format using the given pattern.
		 *
		 * @see DateTimeFormatter.ofPattern
		 */
		@Pure
		public fun ofLocalizedTime(pattern: String): DateTimeFormatter =
			DateTimeFormatter.ofPattern(pattern, formatLocale)
	}
}
