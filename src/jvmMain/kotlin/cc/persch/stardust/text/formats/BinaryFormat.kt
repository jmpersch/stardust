/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package cc.persch.stardust.text.formats

import cc.persch.stardust.annotations.Pure
import java.math.BigDecimal
import java.math.BigInteger
import java.text.FieldPosition
import java.text.NumberFormat
import java.text.ParsePosition
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong
import java.lang.Byte as JByte
import java.lang.Short as JShort
import java.lang.Integer as JInt
import java.lang.Long as JLong

/**
 * Formats and parses binary string representations.
 *
 * ## Formatting
 *
 * Formats a given value as a binary string representation,
 * e.g., an [Int] of `514229` is formatted as `"1111101100010110101"`.
 *
 * [AtomicInteger], [AtomicLong], [BigInteger], [BigDecimal], as well as Kotlin's unsigned data types,
 * [UByte], [UShort], [UInt], and [ULong], are also supported.
 *
 * For [Float] and [Double], the [raw bits representation][Double.toRawBits] is used.
 * For [BigDecimal], the underlying [BigInteger][BigDecimal.toBigIntegerExact] is used.
 *
 * ## Parsing
 *
 * Parses a binary string into a [Long] or a [BigInteger], depending on the input length.
 *
 * Examples:
 *
 * * `"10001110110000001001010110111000001001011111"` will produce a [Long] of `9809862296159`
 * * `"1111111111111111111111111111111111111111111111111111111111111111"` will produce a [Long] of `-1`
 * * `"11011000011100111001011000000100011100000001010110010100100010001101000000101010101100001"`
 *   will produce a [BigInteger] of `523347633027360537213687137`
 *
 * @since 4.0
 */
public object BinaryFormat: NumberFormat() {
	
	private val ZERO_POSITION = FieldPosition(0)
	
	private fun readResolve(): Any = BinaryFormat
	
	/** @since 6.0 */
	public fun format(number: Boolean): String = formatAny(number)
	/** @since 6.0 */
	public fun format(number: Byte): String = formatAny(number)
	/** @since 6.0 */
	public fun format(number: Short): String = formatAny(number)
	/** @since 6.0 */
	public fun format(number: Int): String = formatAny(number)
	
	/** @since 6.0 */
	public fun format(number: UByte): String = formatAny(number)
	/** @since 6.0 */
	public fun format(number: UShort): String = formatAny(number)
	/** @since 6.0 */
	public fun format(number: UInt): String = formatAny(number)
	/** @since 6.0 */
	public fun format(number: ULong): String = formatAny(number)
	
	/** @since 6.0 */
	public fun format(number: Float): String = formatAny(number)
	
	private fun formatAny(number: Any): String = format(number, StringBuffer(), ZERO_POSITION).toString()
	
	
	/** @since 6.0 */
	public fun format(number: Boolean, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	
	/** @since 6.0 */
	public fun format(number: Byte, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	public fun format(number: Short, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	public fun format(number: Int, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	override fun format(number: Long, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	
	/** @since 6.0 */
	public fun format(number: UByte, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	public fun format(number: UShort, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	public fun format(number: UInt, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	public fun format(number: ULong, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	
	/** @since 6.0 */
	public fun format(number: Float, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/** @since 6.0 */
	override fun format(number: Double, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	
	/** @since 6.0 */
	public fun format(number: BigInteger, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	/** @since 6.0 */
	public fun format(number: BigDecimal, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		format(number as Any, toAppendTo, pos)
	
	/**
	 * Formats the given [obj] as a binary string representation,
	 * e.g., an [Int] of `514229` is formatted as `"1111101100010110101"`.
	 *
	 * [AtomicInteger], [AtomicLong], [BigInteger], [BigDecimal], as well as Kotlin's unsigned data types,
	 * [UByte], [UShort], [UInt], and [ULong], are also supported.
	 *
	 * For [Float] and [Double], the [raw bits representation][Double.toRawBits] is used.
	 * For [BigDecimal], the underlying [BigInteger][BigDecimal.toBigIntegerExact] is used.
	 *
	 * @throws ArithmeticException A [BigDecimal] is given and its fractional part is nonzero.
	 */
	override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
		
		val value = when(obj) {
			
			is Boolean -> if(obj) "1" else "0"
			is Byte -> JInt.toBinaryString(JByte.toUnsignedInt(obj))
			is Short -> JInt.toBinaryString(JShort.toUnsignedInt(obj))
			is Int -> JInt.toBinaryString(obj)
			is Long -> JLong.toBinaryString(obj)
			
			is UByte -> JInt.toBinaryString(obj.toInt())
			is UShort -> JInt.toBinaryString(obj.toInt())
			is UInt -> JInt.toBinaryString(obj.toInt())
			is ULong -> JLong.toBinaryString(obj.toLong())
			
			is Float -> JInt.toUnsignedLong(obj.toRawBits())
			is Double -> obj.toRawBits()
			
			is AtomicInteger -> JInt.toBinaryString(obj.toInt())
			is AtomicLong -> obj.toLong()
			
			is BigInteger -> obj.toString(2)
			is BigDecimal -> obj.toBigIntegerExact().toString(2)
			
			else -> obj.toString()
		}
		
		return toAppendTo.append(value)
	}
	
	/**
	 * Parses a binary string into a [Long] or a [BigInteger], depending on the input length.
	 *
	 * Examples:
	 *
	 * * `"10001110110000001001010110111000001001011111"` will produce a [Long] of `9809862296159`
	 * * `"1111111111111111111111111111111111111111111111111111111111111111"` will produce a [Long] of `-1`
	 * * `"11011000011100111001011000000100011100000001010110010100100010001101000000101010101100001"`
	 *   will produce a [BigInteger] of `523347633027360537213687137`
	 */
	@Pure
	override fun parse(source: String, parsePosition: ParsePosition): Number? {
		
		val matchValue = BIN_REGEX.find(source, parsePosition.index)?.value ?: return null
		val bitCount = matchValue.length
		
		parsePosition.index += bitCount
		
		return when {
			
			bitCount <= 64 -> JLong.parseUnsignedLong(matchValue, 2)
			else -> BigInteger(matchValue, 2)
		}
	}
	
	private val BIN_REGEX by lazy { Regex("""\A[01]+""") }
}
