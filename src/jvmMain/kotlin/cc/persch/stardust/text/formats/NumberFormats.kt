/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.formats

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.require
import cc.persch.stardust.text.localization.Locales
import cc.persch.stardust.text.repeat
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Defines formats for numbers.
 *
 * @since 1.0
 */
public object NumberFormats {
	
	@Pure
	private fun createFormat(format: String, locale: Locale) = DecimalFormat(format, DecimalFormatSymbols(locale))
	
	/**
	 * Generic fixed format, e.g. `"12345.670"`.
	 *
	 * @param decimalDigits Number of decimal digits.
	 * @param locale The locale.
	 * @return The format.
	 * @throws IllegalArgumentException [decimalDigits] is less than zero.
	 */
	@Pure
	public fun genericFixed(decimalDigits: Int, locale: Locale = Locales.defaultFormat): DecimalFormat {
		
		require("decimalDigits", decimalDigits >= 0)
		
		var format = "#"
		
		if(decimalDigits > 0)
			format += "." + '0'.repeat(decimalDigits)
		
		return createFormat(format, locale)
	}
	
	/**
	 * Full fixed format, e.g. `"12,345.670"`.
	 *
	 * @param decimalDigits Number of decimal digits.
	 * @param locale The locale.
	 * @return The format.
	 * @throws IllegalArgumentException [decimalDigits] is less than zero.
	 */
	@Pure
	public fun fullFixed(decimalDigits: Int, locale: Locale = Locales.defaultFormat): DecimalFormat {
		
		require("decimalDigits", decimalDigits >= 0)
		
		var format = "#,##0"
		
		if(decimalDigits > 0)
			format += "." + '0'.repeat(decimalDigits)
		
		return createFormat(format, locale)
	}
	
	/**
	 * Generic fixed format, e.g. `"12345.670"`.
	 *
	 * @param maxDecimalDigits Maximal number of decimal digits Number of decimal digits.
	 * @param locale The locale.
	 * @return The format.
	 * @throws IllegalArgumentException [maxDecimalDigits] is less than zero.
	 */
	@Pure
	public fun genericDynamic(maxDecimalDigits: Int, locale: Locale = Locales.defaultFormat): DecimalFormat {
		
		require("maxDecimalDigits", maxDecimalDigits >= 0)
		
		var format = "#"
		
		if(maxDecimalDigits > 0)
			format += "." + '#'.repeat(maxDecimalDigits)
		
		return createFormat(format, locale)
	}
	
	/**
	 * Full dynamic format, e.g. `"12,345.67"`.
	 *
	 * @param maxDecimalDigits Maximal number of decimal digits Number of decimal digits.
	 * @param locale The locale.
	 * @return The format.
	 * @throws IllegalArgumentException [maxDecimalDigits] is less than zero.
	 */
	@Pure
	public fun fullDynamic(maxDecimalDigits: Int, locale: Locale = Locales.defaultFormat): DecimalFormat {
		
		require("maxDecimalDigits", maxDecimalDigits >= 0)
		
		var format = "#,##0"
		
		if(maxDecimalDigits > 0)
			format += "." + '#'.repeat(maxDecimalDigits)
		
		return createFormat(format, locale)
	}
	
	/**
	 * Hexadecimal format, e.g. `"12abff"`.
	 *
	 * @param option A [format option][HexadecimalFormat.Option].
	 * @return The format.
	 */
	@Cached
	@Pure
	public fun hexadecimal(option: HexadecimalFormat.Option): HexadecimalFormat =
		HexadecimalFormat.getHexadecimalInstance(option)
}
