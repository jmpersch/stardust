/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.formats

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.unfulfilled
import cc.persch.stardust.text.formats.PlainNumberFormat.parse
import java.math.BigDecimal
import java.math.BigInteger
import java.text.FieldPosition
import java.text.NumberFormat
import java.text.ParsePosition
import java.util.regex.Pattern

/**
 * Defines a plain number format that formats and parses a number "as is", like `"12345"` or `"12345.6789"`,
 * without digit grouping and exponential representation.
 * Thus, a big `double` or [BigDecimal] value may get a very long string.
 *
 * The [parse method][parse] returns a [BigDecimal], no matter how big the parsed number is.
 *
 * @since 1.0
 */
public object PlainNumberFormat: NumberFormat() {
	
	private fun readResolve(): Any = PlainNumberFormat
	
	override fun format(number: Double, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		this.format(BigDecimal(number), toAppendTo, pos)
	
	override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer = when(obj) {
		
		!is Number -> unfulfilled("obj", "The object must be a `Number`.")
		is Double, is Float -> toAppendTo.append(BigDecimal(obj.toDouble()).toPlainString())
		is BigInteger -> toAppendTo.append(obj)
		is BigDecimal -> toAppendTo.append(obj.toPlainString())
		else -> toAppendTo.append(obj.toLong())
	}
	
	override fun format(number: Long, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer =
		toAppendTo.append(number)
	
	private val NUM_LEX by lazy { Pattern.compile("""^\d+(?:\.\d+)?""") }
	
	@Pure
	override fun parse(source: String, parsePosition: ParsePosition): BigDecimal? {
		
		val m = NUM_LEX.matcher(source.subSequence(parsePosition.index, source.length))
		
		if(!m.find())
			return null
		
		val value = BigDecimal(m.group())
		
		parsePosition.index += m.group().length
		
		return value
	}
}
