/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*
import cc.persch.stardust.text.quote
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import java.math.RoundingMode

// BigInteger //////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Parses this string as a [BigInteger].
 *
 * @return [Failure] if this string is not a valid representation of a number.
 * @since 5.1
 */
@Pure
public fun String.parseBigInteger(): Conclude<BigInteger, NumberFormatFault> =
	try { successOf(BigInteger(this)) }
	catch(_: NumberFormatException) { failureOf(NumberFormatFault(this)) }

// BigDecimal //////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Defines a [BigDecimal] parsing fault.
 *
 * @see parseBigDecimal
 * @since 6.0
 */
public sealed class BigDecimalParsingFault: Fault() {
	
	/**
	 * The given input does not represent a valid representation of a [BigDecimal].
	 *
	 * @see parseBigDecimal
	 * @since 6.0
	 */
	public data class IllegalNumberFormat(
		val input: String,
		override val cause: Any? = null
	) : BigDecimalParsingFault() {
		
		override fun createMessage(): String = "Invalid number format: ${input.quote()}"
	}
	
	/**
	 * Illegal rounding condition.
	 * This error occurs, if rounding is needed, but the rounding mode is [RoundingMode.UNNECESSARY]
	 *
	 * @see parseBigDecimal
	 * @since 6.0
	 */
	public data class IllegalRoundingCondition(
		val mathContext: MathContext,
		val input: String,
		override val cause: Any? = null
	) : BigDecimalParsingFault() {
		
		override fun createMessage(): String =
			"Illegal rounding condition for math context with precision of ${mathContext.precision} " +
			"and rounding mode ${mathContext.roundingMode}: ${input.quote()}"
	}
}

/**
 * Parses this string as a [BigDecimal].
 *
 * @return [Failure] if this string is not a valid representation of a number.
 * @since 5.1
 */
@Pure
public fun String.parseBigDecimal(): Conclude<BigDecimal, BigDecimalParsingFault.IllegalNumberFormat> =
	try { successOf(BigDecimal(this)) }
	catch(_: NumberFormatException) { failureOf(BigDecimalParsingFault.IllegalNumberFormat(this)) }

/**
 * Parses this string as a [BigDecimal] using the given [mathContext].
 *
 * @return [Failure] if this string is not a valid representation of a number;
 *   or if rounding is needed, but the rounding mode is [RoundingMode.UNNECESSARY].
 * @since 5.1
 */
@Pure
public fun String.parseBigDecimal(mathContext: MathContext): Conclude<BigDecimal, BigDecimalParsingFault> =
	try { successOf(BigDecimal(this, mathContext)) }
	catch(_: NumberFormatException) { failureOf(BigDecimalParsingFault.IllegalNumberFormat(this)) }
	catch(_: ArithmeticException) { failureOf(BigDecimalParsingFault.IllegalRoundingCondition(mathContext, this)) }
