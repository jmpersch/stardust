/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardust.conclude.orNull
import java.util.Optional

/**
 * Converts this [Conclude] into an [Optional]. If [Success]'s value is `null` or this is [Failure], an [Optional]
 * with absent value is returned; otherwise, an [Optional] with the [Success.value] is returned.
 *
 * @since 5.0
 */
@Pure
public fun <Result: Any> Conclude<Result?, *>.toOptional(): Optional<Result> = Optional.ofNullable(orNull())
