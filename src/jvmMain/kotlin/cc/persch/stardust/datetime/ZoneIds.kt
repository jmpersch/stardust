/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("unused")

package cc.persch.stardust.datetime

import cc.persch.stardust.annotations.Cached
import java.time.ZoneId
import java.util.*

/**
 * Contains all available [time zones][ZoneId].
 * 
 * *Generated on: OpenJDK Runtime Environment 21.0.2+13 (64-bit)*
 *
 * @since 4.0
 * @see ZoneId.getAvailableZoneIds
 */
public object ZoneIds {
	
	/**
	 * Gets the system default [ZoneId].
	 *
	 * @see ZoneId.systemDefault
	 * @see TimeZone.getDefault
	 */
	public val systemDefault: ZoneId
		get() = ZoneId.systemDefault()
	
	private val cachedAvailableZoneIds by lazy { ZoneId.getAvailableZoneIds().toSet() }
	
	/**
	 * Gets a set containing all [available time zone identifiers][ZoneId.getAvailableZoneIds].
	 */
	@Cached
	public fun getAvailableZoneIds(): Set<String> = cachedAvailableZoneIds

	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * Africa/Abidjan: Greenwich Mean Time
	 */
	public val africa_abidjan: ZoneId by lazy { ZoneId.of("Africa/Abidjan") }
	
	/**
	 * Africa/Accra: Ghana Mean Time
	 */
	public val africa_accra: ZoneId by lazy { ZoneId.of("Africa/Accra") }
	
	/**
	 * Africa/Addis_Ababa: Eastern Africa Time
	 */
	public val africa_addisababa: ZoneId by lazy { ZoneId.of("Africa/Addis_Ababa") }
	
	/**
	 * Africa/Algiers: Central European Time
	 */
	public val africa_algiers: ZoneId by lazy { ZoneId.of("Africa/Algiers") }
	
	/**
	 * Africa/Asmara: Eastern Africa Time
	 */
	public val africa_asmara: ZoneId by lazy { ZoneId.of("Africa/Asmara") }
	
	/**
	 * Africa/Asmera: Eastern Africa Time
	 */
	public val africa_asmera: ZoneId by lazy { ZoneId.of("Africa/Asmera") }
	
	/**
	 * Africa/Bamako: Greenwich Mean Time
	 */
	public val africa_bamako: ZoneId by lazy { ZoneId.of("Africa/Bamako") }
	
	/**
	 * Africa/Bangui: West Africa Time
	 */
	public val africa_bangui: ZoneId by lazy { ZoneId.of("Africa/Bangui") }
	
	/**
	 * Africa/Banjul: Greenwich Mean Time
	 */
	public val africa_banjul: ZoneId by lazy { ZoneId.of("Africa/Banjul") }
	
	/**
	 * Africa/Bissau: Greenwich Mean Time
	 */
	public val africa_bissau: ZoneId by lazy { ZoneId.of("Africa/Bissau") }
	
	/**
	 * Africa/Blantyre: Central Africa Time
	 */
	public val africa_blantyre: ZoneId by lazy { ZoneId.of("Africa/Blantyre") }
	
	/**
	 * Africa/Brazzaville: West Africa Time
	 */
	public val africa_brazzaville: ZoneId by lazy { ZoneId.of("Africa/Brazzaville") }
	
	/**
	 * Africa/Bujumbura: Central Africa Time
	 */
	public val africa_bujumbura: ZoneId by lazy { ZoneId.of("Africa/Bujumbura") }
	
	/**
	 * Africa/Cairo: Eastern European Time
	 */
	public val africa_cairo: ZoneId by lazy { ZoneId.of("Africa/Cairo") }
	
	/**
	 * Africa/Casablanca: Western European Time
	 */
	public val africa_casablanca: ZoneId by lazy { ZoneId.of("Africa/Casablanca") }
	
	/**
	 * Africa/Ceuta: Central European Time
	 */
	public val africa_ceuta: ZoneId by lazy { ZoneId.of("Africa/Ceuta") }
	
	/**
	 * Africa/Conakry: Greenwich Mean Time
	 */
	public val africa_conakry: ZoneId by lazy { ZoneId.of("Africa/Conakry") }
	
	/**
	 * Africa/Dakar: Greenwich Mean Time
	 */
	public val africa_dakar: ZoneId by lazy { ZoneId.of("Africa/Dakar") }
	
	/**
	 * Africa/Dar_es_Salaam: Eastern Africa Time
	 */
	public val africa_daressalaam: ZoneId by lazy { ZoneId.of("Africa/Dar_es_Salaam") }
	
	/**
	 * Africa/Djibouti: Eastern Africa Time
	 */
	public val africa_djibouti: ZoneId by lazy { ZoneId.of("Africa/Djibouti") }
	
	/**
	 * Africa/Douala: West Africa Time
	 */
	public val africa_douala: ZoneId by lazy { ZoneId.of("Africa/Douala") }
	
	/**
	 * Africa/El_Aaiun: Western European Time
	 */
	public val africa_elaaiun: ZoneId by lazy { ZoneId.of("Africa/El_Aaiun") }
	
	/**
	 * Africa/Freetown: Greenwich Mean Time
	 */
	public val africa_freetown: ZoneId by lazy { ZoneId.of("Africa/Freetown") }
	
	/**
	 * Africa/Gaborone: Central Africa Time
	 */
	public val africa_gaborone: ZoneId by lazy { ZoneId.of("Africa/Gaborone") }
	
	/**
	 * Africa/Harare: Central Africa Time
	 */
	public val africa_harare: ZoneId by lazy { ZoneId.of("Africa/Harare") }
	
	/**
	 * Africa/Johannesburg: South Africa Time
	 */
	public val africa_johannesburg: ZoneId by lazy { ZoneId.of("Africa/Johannesburg") }
	
	/**
	 * Africa/Juba: Central Africa Time
	 */
	public val africa_juba: ZoneId by lazy { ZoneId.of("Africa/Juba") }
	
	/**
	 * Africa/Kampala: Eastern Africa Time
	 */
	public val africa_kampala: ZoneId by lazy { ZoneId.of("Africa/Kampala") }
	
	/**
	 * Africa/Khartoum: Central Africa Time
	 */
	public val africa_khartoum: ZoneId by lazy { ZoneId.of("Africa/Khartoum") }
	
	/**
	 * Africa/Kigali: Central Africa Time
	 */
	public val africa_kigali: ZoneId by lazy { ZoneId.of("Africa/Kigali") }
	
	/**
	 * Africa/Kinshasa: West Africa Time
	 */
	public val africa_kinshasa: ZoneId by lazy { ZoneId.of("Africa/Kinshasa") }
	
	/**
	 * Africa/Lagos: West Africa Time
	 */
	public val africa_lagos: ZoneId by lazy { ZoneId.of("Africa/Lagos") }
	
	/**
	 * Africa/Libreville: West Africa Time
	 */
	public val africa_libreville: ZoneId by lazy { ZoneId.of("Africa/Libreville") }
	
	/**
	 * Africa/Lome: Greenwich Mean Time
	 */
	public val africa_lome: ZoneId by lazy { ZoneId.of("Africa/Lome") }
	
	/**
	 * Africa/Luanda: West Africa Time
	 */
	public val africa_luanda: ZoneId by lazy { ZoneId.of("Africa/Luanda") }
	
	/**
	 * Africa/Lubumbashi: Central Africa Time
	 */
	public val africa_lubumbashi: ZoneId by lazy { ZoneId.of("Africa/Lubumbashi") }
	
	/**
	 * Africa/Lusaka: Central Africa Time
	 */
	public val africa_lusaka: ZoneId by lazy { ZoneId.of("Africa/Lusaka") }
	
	/**
	 * Africa/Malabo: West Africa Time
	 */
	public val africa_malabo: ZoneId by lazy { ZoneId.of("Africa/Malabo") }
	
	/**
	 * Africa/Maputo: Central Africa Time
	 */
	public val africa_maputo: ZoneId by lazy { ZoneId.of("Africa/Maputo") }
	
	/**
	 * Africa/Maseru: South Africa Time
	 */
	public val africa_maseru: ZoneId by lazy { ZoneId.of("Africa/Maseru") }
	
	/**
	 * Africa/Mbabane: South Africa Time
	 */
	public val africa_mbabane: ZoneId by lazy { ZoneId.of("Africa/Mbabane") }
	
	/**
	 * Africa/Mogadishu: Eastern Africa Time
	 */
	public val africa_mogadishu: ZoneId by lazy { ZoneId.of("Africa/Mogadishu") }
	
	/**
	 * Africa/Monrovia: Greenwich Mean Time
	 */
	public val africa_monrovia: ZoneId by lazy { ZoneId.of("Africa/Monrovia") }
	
	/**
	 * Africa/Nairobi: Eastern Africa Time
	 */
	public val africa_nairobi: ZoneId by lazy { ZoneId.of("Africa/Nairobi") }
	
	/**
	 * Africa/Ndjamena: West Africa Time
	 */
	public val africa_ndjamena: ZoneId by lazy { ZoneId.of("Africa/Ndjamena") }
	
	/**
	 * Africa/Niamey: West Africa Time
	 */
	public val africa_niamey: ZoneId by lazy { ZoneId.of("Africa/Niamey") }
	
	/**
	 * Africa/Nouakchott: Greenwich Mean Time
	 */
	public val africa_nouakchott: ZoneId by lazy { ZoneId.of("Africa/Nouakchott") }
	
	/**
	 * Africa/Ouagadougou: Greenwich Mean Time
	 */
	public val africa_ouagadougou: ZoneId by lazy { ZoneId.of("Africa/Ouagadougou") }
	
	/**
	 * Africa/Porto-Novo: West Africa Time
	 */
	public val africa_portonovo: ZoneId by lazy { ZoneId.of("Africa/Porto-Novo") }
	
	/**
	 * Africa/Sao_Tome: Greenwich Mean Time
	 */
	public val africa_saotome: ZoneId by lazy { ZoneId.of("Africa/Sao_Tome") }
	
	/**
	 * Africa/Timbuktu: Greenwich Mean Time
	 */
	public val africa_timbuktu: ZoneId by lazy { ZoneId.of("Africa/Timbuktu") }
	
	/**
	 * Africa/Tripoli: Eastern European Time
	 */
	public val africa_tripoli: ZoneId by lazy { ZoneId.of("Africa/Tripoli") }
	
	/**
	 * Africa/Tunis: Central European Time
	 */
	public val africa_tunis: ZoneId by lazy { ZoneId.of("Africa/Tunis") }
	
	/**
	 * Africa/Windhoek: Central African Time
	 */
	public val africa_windhoek: ZoneId by lazy { ZoneId.of("Africa/Windhoek") }
	
	/**
	 * America/Adak: Hawaii-Aleutian Time
	 */
	public val america_adak: ZoneId by lazy { ZoneId.of("America/Adak") }
	
	/**
	 * America/Anchorage: Alaska Time
	 */
	public val america_anchorage: ZoneId by lazy { ZoneId.of("America/Anchorage") }
	
	/**
	 * America/Anguilla: Atlantic Time
	 */
	public val america_anguilla: ZoneId by lazy { ZoneId.of("America/Anguilla") }
	
	/**
	 * America/Antigua: Atlantic Time
	 */
	public val america_antigua: ZoneId by lazy { ZoneId.of("America/Antigua") }
	
	/**
	 * America/Araguaina: Brasilia Time
	 */
	public val america_araguaina: ZoneId by lazy { ZoneId.of("America/Araguaina") }
	
	/**
	 * America/Argentina/Buenos_Aires: Argentina Time
	 */
	public val america_argentina_buenosaires: ZoneId by lazy { ZoneId.of("America/Argentina/Buenos_Aires") }
	
	/**
	 * America/Argentina/Catamarca: Argentina Time
	 */
	public val america_argentina_catamarca: ZoneId by lazy { ZoneId.of("America/Argentina/Catamarca") }
	
	/**
	 * America/Argentina/ComodRivadavia: Argentina Time
	 */
	public val america_argentina_comodRivadavia: ZoneId by lazy { ZoneId.of("America/Argentina/ComodRivadavia") }
	
	/**
	 * America/Argentina/Cordoba: Argentina Time
	 */
	public val america_argentina_cordoba: ZoneId by lazy { ZoneId.of("America/Argentina/Cordoba") }
	
	/**
	 * America/Argentina/Jujuy: Argentina Time
	 */
	public val america_argentina_jujuy: ZoneId by lazy { ZoneId.of("America/Argentina/Jujuy") }
	
	/**
	 * America/Argentina/La_Rioja: Argentina Time
	 */
	public val america_argentina_larioja: ZoneId by lazy { ZoneId.of("America/Argentina/La_Rioja") }
	
	/**
	 * America/Argentina/Mendoza: Argentina Time
	 */
	public val america_argentina_mendoza: ZoneId by lazy { ZoneId.of("America/Argentina/Mendoza") }
	
	/**
	 * America/Argentina/Rio_Gallegos: Argentina Time
	 */
	public val america_argentina_riogallegos: ZoneId by lazy { ZoneId.of("America/Argentina/Rio_Gallegos") }
	
	/**
	 * America/Argentina/Salta: Argentina Time
	 */
	public val america_argentina_salta: ZoneId by lazy { ZoneId.of("America/Argentina/Salta") }
	
	/**
	 * America/Argentina/San_Juan: Argentina Time
	 */
	public val america_argentina_sanjuan: ZoneId by lazy { ZoneId.of("America/Argentina/San_Juan") }
	
	/**
	 * America/Argentina/San_Luis: Argentina Time
	 */
	public val america_argentina_sanluis: ZoneId by lazy { ZoneId.of("America/Argentina/San_Luis") }
	
	/**
	 * America/Argentina/Tucuman: Argentina Time
	 */
	public val america_argentina_tucuman: ZoneId by lazy { ZoneId.of("America/Argentina/Tucuman") }
	
	/**
	 * America/Argentina/Ushuaia: Argentina Time
	 */
	public val america_argentina_ushuaia: ZoneId by lazy { ZoneId.of("America/Argentina/Ushuaia") }
	
	/**
	 * America/Aruba: Atlantic Time
	 */
	public val america_aruba: ZoneId by lazy { ZoneId.of("America/Aruba") }
	
	/**
	 * America/Asuncion: Paraguay Time
	 */
	public val america_asuncion: ZoneId by lazy { ZoneId.of("America/Asuncion") }
	
	/**
	 * America/Atikokan: Eastern Time
	 */
	public val america_atikokan: ZoneId by lazy { ZoneId.of("America/Atikokan") }
	
	/**
	 * America/Atka: Hawaii-Aleutian Time
	 */
	public val america_atka: ZoneId by lazy { ZoneId.of("America/Atka") }
	
	/**
	 * America/Bahia: Brasilia Time
	 */
	public val america_bahia: ZoneId by lazy { ZoneId.of("America/Bahia") }
	
	/**
	 * America/Bahia_Banderas: Central Time
	 */
	public val america_bahiabanderas: ZoneId by lazy { ZoneId.of("America/Bahia_Banderas") }
	
	/**
	 * America/Barbados: Atlantic Time
	 */
	public val america_barbados: ZoneId by lazy { ZoneId.of("America/Barbados") }
	
	/**
	 * America/Belem: Brasilia Time
	 */
	public val america_belem: ZoneId by lazy { ZoneId.of("America/Belem") }
	
	/**
	 * America/Belize: Central Time
	 */
	public val america_belize: ZoneId by lazy { ZoneId.of("America/Belize") }
	
	/**
	 * America/Blanc-Sablon: Atlantic Time
	 */
	public val america_blancsablon: ZoneId by lazy { ZoneId.of("America/Blanc-Sablon") }
	
	/**
	 * America/Boa_Vista: Amazon Time
	 */
	public val america_boavista: ZoneId by lazy { ZoneId.of("America/Boa_Vista") }
	
	/**
	 * America/Bogota: Colombia Time
	 */
	public val america_bogota: ZoneId by lazy { ZoneId.of("America/Bogota") }
	
	/**
	 * America/Boise: Mountain Time
	 */
	public val america_boise: ZoneId by lazy { ZoneId.of("America/Boise") }
	
	/**
	 * America/Buenos_Aires: Argentina Time
	 */
	public val america_buenosaires: ZoneId by lazy { ZoneId.of("America/Buenos_Aires") }
	
	/**
	 * America/Cambridge_Bay: Mountain Time
	 */
	public val america_cambridgebay: ZoneId by lazy { ZoneId.of("America/Cambridge_Bay") }
	
	/**
	 * America/Campo_Grande: Amazon Time
	 */
	public val america_campogrande: ZoneId by lazy { ZoneId.of("America/Campo_Grande") }
	
	/**
	 * America/Cancun: Eastern Time
	 */
	public val america_cancun: ZoneId by lazy { ZoneId.of("America/Cancun") }
	
	/**
	 * America/Caracas: Venezuela Time
	 */
	public val america_caracas: ZoneId by lazy { ZoneId.of("America/Caracas") }
	
	/**
	 * America/Catamarca: Argentina Time
	 */
	public val america_catamarca: ZoneId by lazy { ZoneId.of("America/Catamarca") }
	
	/**
	 * America/Cayenne: French Guiana Time
	 */
	public val america_cayenne: ZoneId by lazy { ZoneId.of("America/Cayenne") }
	
	/**
	 * America/Cayman: Eastern Time
	 */
	public val america_cayman: ZoneId by lazy { ZoneId.of("America/Cayman") }
	
	/**
	 * America/Chicago: Central Time
	 */
	public val america_chicago: ZoneId by lazy { ZoneId.of("America/Chicago") }
	
	/**
	 * America/Chihuahua: Central Time
	 */
	public val america_chihuahua: ZoneId by lazy { ZoneId.of("America/Chihuahua") }
	
	/**
	 * America/Ciudad_Juarez: Mountain Time
	 */
	public val america_ciudadjuarez: ZoneId by lazy { ZoneId.of("America/Ciudad_Juarez") }
	
	/**
	 * America/Coral_Harbour: Eastern Time
	 */
	public val america_coralharbour: ZoneId by lazy { ZoneId.of("America/Coral_Harbour") }
	
	/**
	 * America/Cordoba: Argentina Time
	 */
	public val america_cordoba: ZoneId by lazy { ZoneId.of("America/Cordoba") }
	
	/**
	 * America/Costa_Rica: Central Time
	 */
	public val america_costarica: ZoneId by lazy { ZoneId.of("America/Costa_Rica") }
	
	/**
	 * America/Creston: Mountain Time
	 */
	public val america_creston: ZoneId by lazy { ZoneId.of("America/Creston") }
	
	/**
	 * America/Cuiaba: Amazon Time
	 */
	public val america_cuiaba: ZoneId by lazy { ZoneId.of("America/Cuiaba") }
	
	/**
	 * America/Curacao: Atlantic Time
	 */
	public val america_curacao: ZoneId by lazy { ZoneId.of("America/Curacao") }
	
	/**
	 * America/Danmarkshavn: Greenwich Mean Time
	 */
	public val america_danmarkshavn: ZoneId by lazy { ZoneId.of("America/Danmarkshavn") }
	
	/**
	 * America/Dawson: Mountain Time
	 */
	public val america_dawson: ZoneId by lazy { ZoneId.of("America/Dawson") }
	
	/**
	 * America/Dawson_Creek: Mountain Time
	 */
	public val america_dawsoncreek: ZoneId by lazy { ZoneId.of("America/Dawson_Creek") }
	
	/**
	 * America/Denver: Mountain Time
	 */
	public val america_denver: ZoneId by lazy { ZoneId.of("America/Denver") }
	
	/**
	 * America/Detroit: Eastern Time
	 */
	public val america_detroit: ZoneId by lazy { ZoneId.of("America/Detroit") }
	
	/**
	 * America/Dominica: Atlantic Time
	 */
	public val america_dominica: ZoneId by lazy { ZoneId.of("America/Dominica") }
	
	/**
	 * America/Edmonton: Mountain Time
	 */
	public val america_edmonton: ZoneId by lazy { ZoneId.of("America/Edmonton") }
	
	/**
	 * America/Eirunepe: Acre Time
	 */
	public val america_eirunepe: ZoneId by lazy { ZoneId.of("America/Eirunepe") }
	
	/**
	 * America/El_Salvador: Central Time
	 */
	public val america_elsalvador: ZoneId by lazy { ZoneId.of("America/El_Salvador") }
	
	/**
	 * America/Ensenada: Pacific Time
	 */
	public val america_ensenada: ZoneId by lazy { ZoneId.of("America/Ensenada") }
	
	/**
	 * America/Fort_Nelson: Mountain Time
	 */
	public val america_fortnelson: ZoneId by lazy { ZoneId.of("America/Fort_Nelson") }
	
	/**
	 * America/Fort_Wayne: Eastern Time
	 */
	public val america_fortwayne: ZoneId by lazy { ZoneId.of("America/Fort_Wayne") }
	
	/**
	 * America/Fortaleza: Brasilia Time
	 */
	public val america_fortaleza: ZoneId by lazy { ZoneId.of("America/Fortaleza") }
	
	/**
	 * America/Glace_Bay: Atlantic Time
	 */
	public val america_glacebay: ZoneId by lazy { ZoneId.of("America/Glace_Bay") }
	
	/**
	 * America/Godthab: West Greenland Time
	 */
	public val america_godthab: ZoneId by lazy { ZoneId.of("America/Godthab") }
	
	/**
	 * America/Goose_Bay: Atlantic Time
	 */
	public val america_goosebay: ZoneId by lazy { ZoneId.of("America/Goose_Bay") }
	
	/**
	 * America/Grand_Turk: Eastern Time
	 */
	public val america_grandturk: ZoneId by lazy { ZoneId.of("America/Grand_Turk") }
	
	/**
	 * America/Grenada: Atlantic Time
	 */
	public val america_grenada: ZoneId by lazy { ZoneId.of("America/Grenada") }
	
	/**
	 * America/Guadeloupe: Atlantic Time
	 */
	public val america_guadeloupe: ZoneId by lazy { ZoneId.of("America/Guadeloupe") }
	
	/**
	 * America/Guatemala: Central Time
	 */
	public val america_guatemala: ZoneId by lazy { ZoneId.of("America/Guatemala") }
	
	/**
	 * America/Guayaquil: Ecuador Time
	 */
	public val america_guayaquil: ZoneId by lazy { ZoneId.of("America/Guayaquil") }
	
	/**
	 * America/Guyana: Guyana Time
	 */
	public val america_guyana: ZoneId by lazy { ZoneId.of("America/Guyana") }
	
	/**
	 * America/Halifax: Atlantic Time
	 */
	public val america_halifax: ZoneId by lazy { ZoneId.of("America/Halifax") }
	
	/**
	 * America/Havana: Cuba Time
	 */
	public val america_havana: ZoneId by lazy { ZoneId.of("America/Havana") }
	
	/**
	 * America/Hermosillo: Mexican Pacific Time
	 */
	public val america_hermosillo: ZoneId by lazy { ZoneId.of("America/Hermosillo") }
	
	/**
	 * America/Indiana/Indianapolis: Eastern Time
	 */
	public val america_indiana_indianapolis: ZoneId by lazy { ZoneId.of("America/Indiana/Indianapolis") }
	
	/**
	 * America/Indiana/Knox: Central Time
	 */
	public val america_indiana_knox: ZoneId by lazy { ZoneId.of("America/Indiana/Knox") }
	
	/**
	 * America/Indiana/Marengo: Eastern Time
	 */
	public val america_indiana_marengo: ZoneId by lazy { ZoneId.of("America/Indiana/Marengo") }
	
	/**
	 * America/Indiana/Petersburg: Eastern Time
	 */
	public val america_indiana_petersburg: ZoneId by lazy { ZoneId.of("America/Indiana/Petersburg") }
	
	/**
	 * America/Indiana/Tell_City: Central Time
	 */
	public val america_indiana_tellcity: ZoneId by lazy { ZoneId.of("America/Indiana/Tell_City") }
	
	/**
	 * America/Indiana/Vevay: Eastern Time
	 */
	public val america_indiana_vevay: ZoneId by lazy { ZoneId.of("America/Indiana/Vevay") }
	
	/**
	 * America/Indiana/Vincennes: Eastern Time
	 */
	public val america_indiana_vincennes: ZoneId by lazy { ZoneId.of("America/Indiana/Vincennes") }
	
	/**
	 * America/Indiana/Winamac: Eastern Time
	 */
	public val america_indiana_winamac: ZoneId by lazy { ZoneId.of("America/Indiana/Winamac") }
	
	/**
	 * America/Indianapolis: Eastern Time
	 */
	public val america_indianapolis: ZoneId by lazy { ZoneId.of("America/Indianapolis") }
	
	/**
	 * America/Inuvik: Mountain Time
	 */
	public val america_inuvik: ZoneId by lazy { ZoneId.of("America/Inuvik") }
	
	/**
	 * America/Iqaluit: Eastern Time
	 */
	public val america_iqaluit: ZoneId by lazy { ZoneId.of("America/Iqaluit") }
	
	/**
	 * America/Jamaica: Eastern Time
	 */
	public val america_jamaica: ZoneId by lazy { ZoneId.of("America/Jamaica") }
	
	/**
	 * America/Jujuy: Argentina Time
	 */
	public val america_jujuy: ZoneId by lazy { ZoneId.of("America/Jujuy") }
	
	/**
	 * America/Juneau: Alaska Time
	 */
	public val america_juneau: ZoneId by lazy { ZoneId.of("America/Juneau") }
	
	/**
	 * America/Kentucky/Louisville: Eastern Time
	 */
	public val america_kentucky_louisville: ZoneId by lazy { ZoneId.of("America/Kentucky/Louisville") }
	
	/**
	 * America/Kentucky/Monticello: Eastern Time
	 */
	public val america_kentucky_monticello: ZoneId by lazy { ZoneId.of("America/Kentucky/Monticello") }
	
	/**
	 * America/Knox_IN: Central Time
	 */
	public val america_knoxin: ZoneId by lazy { ZoneId.of("America/Knox_IN") }
	
	/**
	 * America/Kralendijk: Atlantic Time
	 */
	public val america_kralendijk: ZoneId by lazy { ZoneId.of("America/Kralendijk") }
	
	/**
	 * America/La_Paz: Bolivia Time
	 */
	public val america_lapaz: ZoneId by lazy { ZoneId.of("America/La_Paz") }
	
	/**
	 * America/Lima: Peru Time
	 */
	public val america_lima: ZoneId by lazy { ZoneId.of("America/Lima") }
	
	/**
	 * America/Los_Angeles: Pacific Time
	 */
	public val america_losangeles: ZoneId by lazy { ZoneId.of("America/Los_Angeles") }
	
	/**
	 * America/Louisville: Eastern Time
	 */
	public val america_louisville: ZoneId by lazy { ZoneId.of("America/Louisville") }
	
	/**
	 * America/Lower_Princes: Atlantic Time
	 */
	public val america_lowerprinces: ZoneId by lazy { ZoneId.of("America/Lower_Princes") }
	
	/**
	 * America/Maceio: Brasilia Time
	 */
	public val america_maceio: ZoneId by lazy { ZoneId.of("America/Maceio") }
	
	/**
	 * America/Managua: Central Time
	 */
	public val america_managua: ZoneId by lazy { ZoneId.of("America/Managua") }
	
	/**
	 * America/Manaus: Amazon Time
	 */
	public val america_manaus: ZoneId by lazy { ZoneId.of("America/Manaus") }
	
	/**
	 * America/Marigot: Atlantic Time
	 */
	public val america_marigot: ZoneId by lazy { ZoneId.of("America/Marigot") }
	
	/**
	 * America/Martinique: Atlantic Time
	 */
	public val america_martinique: ZoneId by lazy { ZoneId.of("America/Martinique") }
	
	/**
	 * America/Matamoros: Central Time
	 */
	public val america_matamoros: ZoneId by lazy { ZoneId.of("America/Matamoros") }
	
	/**
	 * America/Mazatlan: Mexican Pacific Time
	 */
	public val america_mazatlan: ZoneId by lazy { ZoneId.of("America/Mazatlan") }
	
	/**
	 * America/Mendoza: Argentina Time
	 */
	public val america_mendoza: ZoneId by lazy { ZoneId.of("America/Mendoza") }
	
	/**
	 * America/Menominee: Central Time
	 */
	public val america_menominee: ZoneId by lazy { ZoneId.of("America/Menominee") }
	
	/**
	 * America/Merida: Central Time
	 */
	public val america_merida: ZoneId by lazy { ZoneId.of("America/Merida") }
	
	/**
	 * America/Metlakatla: Alaska Time
	 */
	public val america_metlakatla: ZoneId by lazy { ZoneId.of("America/Metlakatla") }
	
	/**
	 * America/Mexico_City: Central Time
	 */
	public val america_mexicocity: ZoneId by lazy { ZoneId.of("America/Mexico_City") }
	
	/**
	 * America/Miquelon: St. Pierre & Miquelon Time
	 */
	public val america_miquelon: ZoneId by lazy { ZoneId.of("America/Miquelon") }
	
	/**
	 * America/Moncton: Atlantic Time
	 */
	public val america_moncton: ZoneId by lazy { ZoneId.of("America/Moncton") }
	
	/**
	 * America/Monterrey: Central Time
	 */
	public val america_monterrey: ZoneId by lazy { ZoneId.of("America/Monterrey") }
	
	/**
	 * America/Montevideo: Uruguay Time
	 */
	public val america_montevideo: ZoneId by lazy { ZoneId.of("America/Montevideo") }
	
	/**
	 * America/Montreal: Eastern Time
	 */
	public val america_montreal: ZoneId by lazy { ZoneId.of("America/Montreal") }
	
	/**
	 * America/Montserrat: Atlantic Time
	 */
	public val america_montserrat: ZoneId by lazy { ZoneId.of("America/Montserrat") }
	
	/**
	 * America/Nassau: Eastern Time
	 */
	public val america_nassau: ZoneId by lazy { ZoneId.of("America/Nassau") }
	
	/**
	 * America/New_York: Eastern Time
	 */
	public val america_newyork: ZoneId by lazy { ZoneId.of("America/New_York") }
	
	/**
	 * America/Nipigon: Eastern Time
	 */
	public val america_nipigon: ZoneId by lazy { ZoneId.of("America/Nipigon") }
	
	/**
	 * America/Nome: Alaska Time
	 */
	public val america_nome: ZoneId by lazy { ZoneId.of("America/Nome") }
	
	/**
	 * America/Noronha: Fernando de Noronha Time
	 */
	public val america_noronha: ZoneId by lazy { ZoneId.of("America/Noronha") }
	
	/**
	 * America/North_Dakota/Beulah: Central Time
	 */
	public val america_northdakota_beulah: ZoneId by lazy { ZoneId.of("America/North_Dakota/Beulah") }
	
	/**
	 * America/North_Dakota/Center: Central Time
	 */
	public val america_northdakota_center: ZoneId by lazy { ZoneId.of("America/North_Dakota/Center") }
	
	/**
	 * America/North_Dakota/New_Salem: Central Time
	 */
	public val america_northdakota_newsalem: ZoneId by lazy { ZoneId.of("America/North_Dakota/New_Salem") }
	
	/**
	 * America/Nuuk: West Greenland Time
	 */
	public val america_nuuk: ZoneId by lazy { ZoneId.of("America/Nuuk") }
	
	/**
	 * America/Ojinaga: Central Time
	 */
	public val america_ojinaga: ZoneId by lazy { ZoneId.of("America/Ojinaga") }
	
	/**
	 * America/Panama: Eastern Time
	 */
	public val america_panama: ZoneId by lazy { ZoneId.of("America/Panama") }
	
	/**
	 * America/Pangnirtung: Eastern Time
	 */
	public val america_pangnirtung: ZoneId by lazy { ZoneId.of("America/Pangnirtung") }
	
	/**
	 * America/Paramaribo: Suriname Time
	 */
	public val america_paramaribo: ZoneId by lazy { ZoneId.of("America/Paramaribo") }
	
	/**
	 * America/Phoenix: Mountain Time
	 */
	public val america_phoenix: ZoneId by lazy { ZoneId.of("America/Phoenix") }
	
	/**
	 * America/Port-au-Prince: Eastern Time
	 */
	public val america_portauprince: ZoneId by lazy { ZoneId.of("America/Port-au-Prince") }
	
	/**
	 * America/Port_of_Spain: Atlantic Time
	 */
	public val america_portofspain: ZoneId by lazy { ZoneId.of("America/Port_of_Spain") }
	
	/**
	 * America/Porto_Acre: Acre Time
	 */
	public val america_portoacre: ZoneId by lazy { ZoneId.of("America/Porto_Acre") }
	
	/**
	 * America/Porto_Velho: Amazon Time
	 */
	public val america_portovelho: ZoneId by lazy { ZoneId.of("America/Porto_Velho") }
	
	/**
	 * America/Puerto_Rico: Atlantic Time
	 */
	public val america_puertorico: ZoneId by lazy { ZoneId.of("America/Puerto_Rico") }
	
	/**
	 * America/Punta_Arenas: Punta Arenas Time
	 */
	public val america_puntaarenas: ZoneId by lazy { ZoneId.of("America/Punta_Arenas") }
	
	/**
	 * America/Rainy_River: Central Time
	 */
	public val america_rainyriver: ZoneId by lazy { ZoneId.of("America/Rainy_River") }
	
	/**
	 * America/Rankin_Inlet: Central Time
	 */
	public val america_rankininlet: ZoneId by lazy { ZoneId.of("America/Rankin_Inlet") }
	
	/**
	 * America/Recife: Brasilia Time
	 */
	public val america_recife: ZoneId by lazy { ZoneId.of("America/Recife") }
	
	/**
	 * America/Regina: Central Time
	 */
	public val america_regina: ZoneId by lazy { ZoneId.of("America/Regina") }
	
	/**
	 * America/Resolute: Central Time
	 */
	public val america_resolute: ZoneId by lazy { ZoneId.of("America/Resolute") }
	
	/**
	 * America/Rio_Branco: Acre Time
	 */
	public val america_riobranco: ZoneId by lazy { ZoneId.of("America/Rio_Branco") }
	
	/**
	 * America/Rosario: Argentina Time
	 */
	public val america_rosario: ZoneId by lazy { ZoneId.of("America/Rosario") }
	
	/**
	 * America/Santa_Isabel: Northwest Mexico Time
	 */
	public val america_santaisabel: ZoneId by lazy { ZoneId.of("America/Santa_Isabel") }
	
	/**
	 * America/Santarem: Brasilia Time
	 */
	public val america_santarem: ZoneId by lazy { ZoneId.of("America/Santarem") }
	
	/**
	 * America/Santiago: Chile Time
	 */
	public val america_santiago: ZoneId by lazy { ZoneId.of("America/Santiago") }
	
	/**
	 * America/Santo_Domingo: Atlantic Time
	 */
	public val america_santodomingo: ZoneId by lazy { ZoneId.of("America/Santo_Domingo") }
	
	/**
	 * America/Sao_Paulo: Brasilia Time
	 */
	public val america_saopaulo: ZoneId by lazy { ZoneId.of("America/Sao_Paulo") }
	
	/**
	 * America/Scoresbysund: East Greenland Time
	 */
	public val america_scoresbysund: ZoneId by lazy { ZoneId.of("America/Scoresbysund") }
	
	/**
	 * America/Shiprock: Mountain Time
	 */
	public val america_shiprock: ZoneId by lazy { ZoneId.of("America/Shiprock") }
	
	/**
	 * America/Sitka: Alaska Time
	 */
	public val america_sitka: ZoneId by lazy { ZoneId.of("America/Sitka") }
	
	/**
	 * America/St_Barthelemy: Atlantic Time
	 */
	public val america_stbarthelemy: ZoneId by lazy { ZoneId.of("America/St_Barthelemy") }
	
	/**
	 * America/St_Johns: Newfoundland Time
	 */
	public val america_stjohns: ZoneId by lazy { ZoneId.of("America/St_Johns") }
	
	/**
	 * America/St_Kitts: Atlantic Time
	 */
	public val america_stkitts: ZoneId by lazy { ZoneId.of("America/St_Kitts") }
	
	/**
	 * America/St_Lucia: Atlantic Time
	 */
	public val america_stlucia: ZoneId by lazy { ZoneId.of("America/St_Lucia") }
	
	/**
	 * America/St_Thomas: Atlantic Time
	 */
	public val america_stthomas: ZoneId by lazy { ZoneId.of("America/St_Thomas") }
	
	/**
	 * America/St_Vincent: Atlantic Time
	 */
	public val america_stvincent: ZoneId by lazy { ZoneId.of("America/St_Vincent") }
	
	/**
	 * America/Swift_Current: Central Time
	 */
	public val america_swiftcurrent: ZoneId by lazy { ZoneId.of("America/Swift_Current") }
	
	/**
	 * America/Tegucigalpa: Central Time
	 */
	public val america_tegucigalpa: ZoneId by lazy { ZoneId.of("America/Tegucigalpa") }
	
	/**
	 * America/Thule: Atlantic Time
	 */
	public val america_thule: ZoneId by lazy { ZoneId.of("America/Thule") }
	
	/**
	 * America/Thunder_Bay: Eastern Time
	 */
	public val america_thunderbay: ZoneId by lazy { ZoneId.of("America/Thunder_Bay") }
	
	/**
	 * America/Tijuana: Pacific Time
	 */
	public val america_tijuana: ZoneId by lazy { ZoneId.of("America/Tijuana") }
	
	/**
	 * America/Toronto: Eastern Time
	 */
	public val america_toronto: ZoneId by lazy { ZoneId.of("America/Toronto") }
	
	/**
	 * America/Tortola: Atlantic Time
	 */
	public val america_tortola: ZoneId by lazy { ZoneId.of("America/Tortola") }
	
	/**
	 * America/Vancouver: Pacific Time
	 */
	public val america_vancouver: ZoneId by lazy { ZoneId.of("America/Vancouver") }
	
	/**
	 * America/Virgin: Atlantic Time
	 */
	public val america_virgin: ZoneId by lazy { ZoneId.of("America/Virgin") }
	
	/**
	 * America/Whitehorse: Mountain Time
	 */
	public val america_whitehorse: ZoneId by lazy { ZoneId.of("America/Whitehorse") }
	
	/**
	 * America/Winnipeg: Central Time
	 */
	public val america_winnipeg: ZoneId by lazy { ZoneId.of("America/Winnipeg") }
	
	/**
	 * America/Yakutat: Alaska Time
	 */
	public val america_yakutat: ZoneId by lazy { ZoneId.of("America/Yakutat") }
	
	/**
	 * America/Yellowknife: Mountain Time
	 */
	public val america_yellowknife: ZoneId by lazy { ZoneId.of("America/Yellowknife") }
	
	/**
	 * Antarctica/Casey: Australian Western Time
	 */
	public val antarctica_casey: ZoneId by lazy { ZoneId.of("Antarctica/Casey") }
	
	/**
	 * Antarctica/Davis: Davis Time
	 */
	public val antarctica_davis: ZoneId by lazy { ZoneId.of("Antarctica/Davis") }
	
	/**
	 * Antarctica/DumontDUrville: Dumont-d'Urville Time
	 */
	public val antarctica_dumontDUrville: ZoneId by lazy { ZoneId.of("Antarctica/DumontDUrville") }
	
	/**
	 * Antarctica/Macquarie: Eastern Australia Time
	 */
	public val antarctica_macquarie: ZoneId by lazy { ZoneId.of("Antarctica/Macquarie") }
	
	/**
	 * Antarctica/Mawson: Mawson Time
	 */
	public val antarctica_mawson: ZoneId by lazy { ZoneId.of("Antarctica/Mawson") }
	
	/**
	 * Antarctica/McMurdo: New Zealand Time
	 */
	public val antarctica_mcMurdo: ZoneId by lazy { ZoneId.of("Antarctica/McMurdo") }
	
	/**
	 * Antarctica/Palmer: Chile Time
	 */
	public val antarctica_palmer: ZoneId by lazy { ZoneId.of("Antarctica/Palmer") }
	
	/**
	 * Antarctica/Rothera: Rothera Time
	 */
	public val antarctica_rothera: ZoneId by lazy { ZoneId.of("Antarctica/Rothera") }
	
	/**
	 * Antarctica/South_Pole: New Zealand Time
	 */
	public val antarctica_southpole: ZoneId by lazy { ZoneId.of("Antarctica/South_Pole") }
	
	/**
	 * Antarctica/Syowa: Syowa Time
	 */
	public val antarctica_syowa: ZoneId by lazy { ZoneId.of("Antarctica/Syowa") }
	
	/**
	 * Antarctica/Troll: Troll Time
	 */
	public val antarctica_troll: ZoneId by lazy { ZoneId.of("Antarctica/Troll") }
	
	/**
	 * Antarctica/Vostok: Vostok Time
	 */
	public val antarctica_vostok: ZoneId by lazy { ZoneId.of("Antarctica/Vostok") }
	
	/**
	 * Arctic/Longyearbyen: Central European Time
	 */
	public val arctic_longyearbyen: ZoneId by lazy { ZoneId.of("Arctic/Longyearbyen") }
	
	/**
	 * Asia/Aden: Arabian Time
	 */
	public val asia_aden: ZoneId by lazy { ZoneId.of("Asia/Aden") }
	
	/**
	 * Asia/Almaty: Alma-Ata Time
	 */
	public val asia_almaty: ZoneId by lazy { ZoneId.of("Asia/Almaty") }
	
	/**
	 * Asia/Amman: Eastern European Time
	 */
	public val asia_amman: ZoneId by lazy { ZoneId.of("Asia/Amman") }
	
	/**
	 * Asia/Anadyr: Anadyr Time
	 */
	public val asia_anadyr: ZoneId by lazy { ZoneId.of("Asia/Anadyr") }
	
	/**
	 * Asia/Aqtau: Aqtau Time
	 */
	public val asia_aqtau: ZoneId by lazy { ZoneId.of("Asia/Aqtau") }
	
	/**
	 * Asia/Aqtobe: Aqtobe Time
	 */
	public val asia_aqtobe: ZoneId by lazy { ZoneId.of("Asia/Aqtobe") }
	
	/**
	 * Asia/Ashgabat: Turkmenistan Time
	 */
	public val asia_ashgabat: ZoneId by lazy { ZoneId.of("Asia/Ashgabat") }
	
	/**
	 * Asia/Ashkhabad: Turkmenistan Time
	 */
	public val asia_ashkhabad: ZoneId by lazy { ZoneId.of("Asia/Ashkhabad") }
	
	/**
	 * Asia/Atyrau: Atyrau Time
	 */
	public val asia_atyrau: ZoneId by lazy { ZoneId.of("Asia/Atyrau") }
	
	/**
	 * Asia/Baghdad: Arabian Time
	 */
	public val asia_baghdad: ZoneId by lazy { ZoneId.of("Asia/Baghdad") }
	
	/**
	 * Asia/Bahrain: Arabian Time
	 */
	public val asia_bahrain: ZoneId by lazy { ZoneId.of("Asia/Bahrain") }
	
	/**
	 * Asia/Baku: Azerbaijan Time
	 */
	public val asia_baku: ZoneId by lazy { ZoneId.of("Asia/Baku") }
	
	/**
	 * Asia/Bangkok: Indochina Time
	 */
	public val asia_bangkok: ZoneId by lazy { ZoneId.of("Asia/Bangkok") }
	
	/**
	 * Asia/Barnaul: Barnaul Time
	 */
	public val asia_barnaul: ZoneId by lazy { ZoneId.of("Asia/Barnaul") }
	
	/**
	 * Asia/Beirut: Eastern European Time
	 */
	public val asia_beirut: ZoneId by lazy { ZoneId.of("Asia/Beirut") }
	
	/**
	 * Asia/Bishkek: Kirgizstan Time
	 */
	public val asia_bishkek: ZoneId by lazy { ZoneId.of("Asia/Bishkek") }
	
	/**
	 * Asia/Brunei: Brunei Time
	 */
	public val asia_brunei: ZoneId by lazy { ZoneId.of("Asia/Brunei") }
	
	/**
	 * Asia/Calcutta: India Time
	 */
	public val asia_calcutta: ZoneId by lazy { ZoneId.of("Asia/Calcutta") }
	
	/**
	 * Asia/Chita: Yakutsk Time
	 */
	public val asia_chita: ZoneId by lazy { ZoneId.of("Asia/Chita") }
	
	/**
	 * Asia/Choibalsan: Ulaanbaatar Time
	 */
	public val asia_choibalsan: ZoneId by lazy { ZoneId.of("Asia/Choibalsan") }
	
	/**
	 * Asia/Chongqing: China Time
	 */
	public val asia_chongqing: ZoneId by lazy { ZoneId.of("Asia/Chongqing") }
	
	/**
	 * Asia/Chungking: China Time
	 */
	public val asia_chungking: ZoneId by lazy { ZoneId.of("Asia/Chungking") }
	
	/**
	 * Asia/Colombo: India Time
	 */
	public val asia_colombo: ZoneId by lazy { ZoneId.of("Asia/Colombo") }
	
	/**
	 * Asia/Dacca: Bangladesh Time
	 */
	public val asia_dacca: ZoneId by lazy { ZoneId.of("Asia/Dacca") }
	
	/**
	 * Asia/Damascus: Eastern European Time
	 */
	public val asia_damascus: ZoneId by lazy { ZoneId.of("Asia/Damascus") }
	
	/**
	 * Asia/Dhaka: Bangladesh Time
	 */
	public val asia_dhaka: ZoneId by lazy { ZoneId.of("Asia/Dhaka") }
	
	/**
	 * Asia/Dili: Timor-Leste Time
	 */
	public val asia_dili: ZoneId by lazy { ZoneId.of("Asia/Dili") }
	
	/**
	 * Asia/Dubai: Gulf Time
	 */
	public val asia_dubai: ZoneId by lazy { ZoneId.of("Asia/Dubai") }
	
	/**
	 * Asia/Dushanbe: Tajikistan Time
	 */
	public val asia_dushanbe: ZoneId by lazy { ZoneId.of("Asia/Dushanbe") }
	
	/**
	 * Asia/Famagusta: Eastern European Time
	 */
	public val asia_famagusta: ZoneId by lazy { ZoneId.of("Asia/Famagusta") }
	
	/**
	 * Asia/Gaza: Eastern European Time
	 */
	public val asia_gaza: ZoneId by lazy { ZoneId.of("Asia/Gaza") }
	
	/**
	 * Asia/Harbin: China Time
	 */
	public val asia_harbin: ZoneId by lazy { ZoneId.of("Asia/Harbin") }
	
	/**
	 * Asia/Hebron: Eastern European Time
	 */
	public val asia_hebron: ZoneId by lazy { ZoneId.of("Asia/Hebron") }
	
	/**
	 * Asia/Ho_Chi_Minh: Indochina Time
	 */
	public val asia_hochiminh: ZoneId by lazy { ZoneId.of("Asia/Ho_Chi_Minh") }
	
	/**
	 * Asia/Hong_Kong: Hong Kong Time
	 */
	public val asia_hongkong: ZoneId by lazy { ZoneId.of("Asia/Hong_Kong") }
	
	/**
	 * Asia/Hovd: Hovd Time
	 */
	public val asia_hovd: ZoneId by lazy { ZoneId.of("Asia/Hovd") }
	
	/**
	 * Asia/Irkutsk: Irkutsk Time
	 */
	public val asia_irkutsk: ZoneId by lazy { ZoneId.of("Asia/Irkutsk") }
	
	/**
	 * Asia/Istanbul: Turkey Time
	 */
	public val asia_istanbul: ZoneId by lazy { ZoneId.of("Asia/Istanbul") }
	
	/**
	 * Asia/Jakarta: West Indonesia Time
	 */
	public val asia_jakarta: ZoneId by lazy { ZoneId.of("Asia/Jakarta") }
	
	/**
	 * Asia/Jayapura: East Indonesia Time
	 */
	public val asia_jayapura: ZoneId by lazy { ZoneId.of("Asia/Jayapura") }
	
	/**
	 * Asia/Jerusalem: Israel Time
	 */
	public val asia_jerusalem: ZoneId by lazy { ZoneId.of("Asia/Jerusalem") }
	
	/**
	 * Asia/Kabul: Afghanistan Time
	 */
	public val asia_kabul: ZoneId by lazy { ZoneId.of("Asia/Kabul") }
	
	/**
	 * Asia/Kamchatka: Petropavlovsk-Kamchatski Time
	 */
	public val asia_kamchatka: ZoneId by lazy { ZoneId.of("Asia/Kamchatka") }
	
	/**
	 * Asia/Karachi: Pakistan Time
	 */
	public val asia_karachi: ZoneId by lazy { ZoneId.of("Asia/Karachi") }
	
	/**
	 * Asia/Kashgar: Xinjiang Time
	 */
	public val asia_kashgar: ZoneId by lazy { ZoneId.of("Asia/Kashgar") }
	
	/**
	 * Asia/Kathmandu: Nepal Time
	 */
	public val asia_kathmandu: ZoneId by lazy { ZoneId.of("Asia/Kathmandu") }
	
	/**
	 * Asia/Katmandu: Nepal Time
	 */
	public val asia_katmandu: ZoneId by lazy { ZoneId.of("Asia/Katmandu") }
	
	/**
	 * Asia/Khandyga: Yakutsk Time
	 */
	public val asia_khandyga: ZoneId by lazy { ZoneId.of("Asia/Khandyga") }
	
	/**
	 * Asia/Kolkata: India Time
	 */
	public val asia_kolkata: ZoneId by lazy { ZoneId.of("Asia/Kolkata") }
	
	/**
	 * Asia/Krasnoyarsk: Krasnoyarsk Time
	 */
	public val asia_krasnoyarsk: ZoneId by lazy { ZoneId.of("Asia/Krasnoyarsk") }
	
	/**
	 * Asia/Kuala_Lumpur: Malaysia Time
	 */
	public val asia_kualalumpur: ZoneId by lazy { ZoneId.of("Asia/Kuala_Lumpur") }
	
	/**
	 * Asia/Kuching: Malaysia Time
	 */
	public val asia_kuching: ZoneId by lazy { ZoneId.of("Asia/Kuching") }
	
	/**
	 * Asia/Kuwait: Arabian Time
	 */
	public val asia_kuwait: ZoneId by lazy { ZoneId.of("Asia/Kuwait") }
	
	/**
	 * Asia/Macao: China Time
	 */
	public val asia_macao: ZoneId by lazy { ZoneId.of("Asia/Macao") }
	
	/**
	 * Asia/Macau: China Time
	 */
	public val asia_macau: ZoneId by lazy { ZoneId.of("Asia/Macau") }
	
	/**
	 * Asia/Magadan: Magadan Time
	 */
	public val asia_magadan: ZoneId by lazy { ZoneId.of("Asia/Magadan") }
	
	/**
	 * Asia/Makassar: Central Indonesia Time
	 */
	public val asia_makassar: ZoneId by lazy { ZoneId.of("Asia/Makassar") }
	
	/**
	 * Asia/Manila: Philippine Time
	 */
	public val asia_manila: ZoneId by lazy { ZoneId.of("Asia/Manila") }
	
	/**
	 * Asia/Muscat: Gulf Time
	 */
	public val asia_muscat: ZoneId by lazy { ZoneId.of("Asia/Muscat") }
	
	/**
	 * Asia/Nicosia: Eastern European Time
	 */
	public val asia_nicosia: ZoneId by lazy { ZoneId.of("Asia/Nicosia") }
	
	/**
	 * Asia/Novokuznetsk: Krasnoyarsk Time
	 */
	public val asia_novokuznetsk: ZoneId by lazy { ZoneId.of("Asia/Novokuznetsk") }
	
	/**
	 * Asia/Novosibirsk: Novosibirsk Time
	 */
	public val asia_novosibirsk: ZoneId by lazy { ZoneId.of("Asia/Novosibirsk") }
	
	/**
	 * Asia/Omsk: Omsk Time
	 */
	public val asia_omsk: ZoneId by lazy { ZoneId.of("Asia/Omsk") }
	
	/**
	 * Asia/Oral: Oral Time
	 */
	public val asia_oral: ZoneId by lazy { ZoneId.of("Asia/Oral") }
	
	/**
	 * Asia/Phnom_Penh: Indochina Time
	 */
	public val asia_phnompenh: ZoneId by lazy { ZoneId.of("Asia/Phnom_Penh") }
	
	/**
	 * Asia/Pontianak: West Indonesia Time
	 */
	public val asia_pontianak: ZoneId by lazy { ZoneId.of("Asia/Pontianak") }
	
	/**
	 * Asia/Pyongyang: Korean Time
	 */
	public val asia_pyongyang: ZoneId by lazy { ZoneId.of("Asia/Pyongyang") }
	
	/**
	 * Asia/Qatar: Arabian Time
	 */
	public val asia_qatar: ZoneId by lazy { ZoneId.of("Asia/Qatar") }
	
	/**
	 * Asia/Qostanay: Kostanay Time
	 */
	public val asia_qostanay: ZoneId by lazy { ZoneId.of("Asia/Qostanay") }
	
	/**
	 * Asia/Qyzylorda: Qyzylorda Time
	 */
	public val asia_qyzylorda: ZoneId by lazy { ZoneId.of("Asia/Qyzylorda") }
	
	/**
	 * Asia/Rangoon: Myanmar Time
	 */
	public val asia_rangoon: ZoneId by lazy { ZoneId.of("Asia/Rangoon") }
	
	/**
	 * Asia/Riyadh: Arabian Time
	 */
	public val asia_riyadh: ZoneId by lazy { ZoneId.of("Asia/Riyadh") }
	
	/**
	 * Asia/Saigon: Indochina Time
	 */
	public val asia_saigon: ZoneId by lazy { ZoneId.of("Asia/Saigon") }
	
	/**
	 * Asia/Sakhalin: Sakhalin Time
	 */
	public val asia_sakhalin: ZoneId by lazy { ZoneId.of("Asia/Sakhalin") }
	
	/**
	 * Asia/Samarkand: Uzbekistan Time
	 */
	public val asia_samarkand: ZoneId by lazy { ZoneId.of("Asia/Samarkand") }
	
	/**
	 * Asia/Seoul: Korean Time
	 */
	public val asia_seoul: ZoneId by lazy { ZoneId.of("Asia/Seoul") }
	
	/**
	 * Asia/Shanghai: China Time
	 */
	public val asia_shanghai: ZoneId by lazy { ZoneId.of("Asia/Shanghai") }
	
	/**
	 * Asia/Singapore: Singapore Time
	 */
	public val asia_singapore: ZoneId by lazy { ZoneId.of("Asia/Singapore") }
	
	/**
	 * Asia/Srednekolymsk: Srednekolymsk Time
	 */
	public val asia_srednekolymsk: ZoneId by lazy { ZoneId.of("Asia/Srednekolymsk") }
	
	/**
	 * Asia/Taipei: Taipei Time
	 */
	public val asia_taipei: ZoneId by lazy { ZoneId.of("Asia/Taipei") }
	
	/**
	 * Asia/Tashkent: Uzbekistan Time
	 */
	public val asia_tashkent: ZoneId by lazy { ZoneId.of("Asia/Tashkent") }
	
	/**
	 * Asia/Tbilisi: Georgia Time
	 */
	public val asia_tbilisi: ZoneId by lazy { ZoneId.of("Asia/Tbilisi") }
	
	/**
	 * Asia/Tehran: Iran Time
	 */
	public val asia_tehran: ZoneId by lazy { ZoneId.of("Asia/Tehran") }
	
	/**
	 * Asia/Tel_Aviv: Israel Time
	 */
	public val asia_telaviv: ZoneId by lazy { ZoneId.of("Asia/Tel_Aviv") }
	
	/**
	 * Asia/Thimbu: Bhutan Time
	 */
	public val asia_thimbu: ZoneId by lazy { ZoneId.of("Asia/Thimbu") }
	
	/**
	 * Asia/Thimphu: Bhutan Time
	 */
	public val asia_thimphu: ZoneId by lazy { ZoneId.of("Asia/Thimphu") }
	
	/**
	 * Asia/Tokyo: Japan Time
	 */
	public val asia_tokyo: ZoneId by lazy { ZoneId.of("Asia/Tokyo") }
	
	/**
	 * Asia/Tomsk: Tomsk Time
	 */
	public val asia_tomsk: ZoneId by lazy { ZoneId.of("Asia/Tomsk") }
	
	/**
	 * Asia/Ujung_Pandang: Central Indonesia Time
	 */
	public val asia_ujungpandang: ZoneId by lazy { ZoneId.of("Asia/Ujung_Pandang") }
	
	/**
	 * Asia/Ulaanbaatar: Ulaanbaatar Time
	 */
	public val asia_ulaanbaatar: ZoneId by lazy { ZoneId.of("Asia/Ulaanbaatar") }
	
	/**
	 * Asia/Ulan_Bator: Ulaanbaatar Time
	 */
	public val asia_ulanbator: ZoneId by lazy { ZoneId.of("Asia/Ulan_Bator") }
	
	/**
	 * Asia/Urumqi: Xinjiang Time
	 */
	public val asia_urumqi: ZoneId by lazy { ZoneId.of("Asia/Urumqi") }
	
	/**
	 * Asia/Ust-Nera: Vladivostok Time
	 */
	public val asia_ustnera: ZoneId by lazy { ZoneId.of("Asia/Ust-Nera") }
	
	/**
	 * Asia/Vientiane: Indochina Time
	 */
	public val asia_vientiane: ZoneId by lazy { ZoneId.of("Asia/Vientiane") }
	
	/**
	 * Asia/Vladivostok: Vladivostok Time
	 */
	public val asia_vladivostok: ZoneId by lazy { ZoneId.of("Asia/Vladivostok") }
	
	/**
	 * Asia/Yakutsk: Yakutsk Time
	 */
	public val asia_yakutsk: ZoneId by lazy { ZoneId.of("Asia/Yakutsk") }
	
	/**
	 * Asia/Yangon: Myanmar Time
	 */
	public val asia_yangon: ZoneId by lazy { ZoneId.of("Asia/Yangon") }
	
	/**
	 * Asia/Yekaterinburg: Yekaterinburg Time
	 */
	public val asia_yekaterinburg: ZoneId by lazy { ZoneId.of("Asia/Yekaterinburg") }
	
	/**
	 * Asia/Yerevan: Armenia Time
	 */
	public val asia_yerevan: ZoneId by lazy { ZoneId.of("Asia/Yerevan") }
	
	/**
	 * Atlantic/Azores: Azores Time
	 */
	public val atlantic_azores: ZoneId by lazy { ZoneId.of("Atlantic/Azores") }
	
	/**
	 * Atlantic/Bermuda: Atlantic Time
	 */
	public val atlantic_bermuda: ZoneId by lazy { ZoneId.of("Atlantic/Bermuda") }
	
	/**
	 * Atlantic/Canary: Western European Time
	 */
	public val atlantic_canary: ZoneId by lazy { ZoneId.of("Atlantic/Canary") }
	
	/**
	 * Atlantic/Cape_Verde: Cape Verde Time
	 */
	public val atlantic_capeverde: ZoneId by lazy { ZoneId.of("Atlantic/Cape_Verde") }
	
	/**
	 * Atlantic/Faeroe: Western European Time
	 */
	public val atlantic_faeroe: ZoneId by lazy { ZoneId.of("Atlantic/Faeroe") }
	
	/**
	 * Atlantic/Faroe: Western European Time
	 */
	public val atlantic_faroe: ZoneId by lazy { ZoneId.of("Atlantic/Faroe") }
	
	/**
	 * Atlantic/Jan_Mayen: Central European Time
	 */
	public val atlantic_janmayen: ZoneId by lazy { ZoneId.of("Atlantic/Jan_Mayen") }
	
	/**
	 * Atlantic/Madeira: Western European Time
	 */
	public val atlantic_madeira: ZoneId by lazy { ZoneId.of("Atlantic/Madeira") }
	
	/**
	 * Atlantic/Reykjavik: Greenwich Mean Time
	 */
	public val atlantic_reykjavik: ZoneId by lazy { ZoneId.of("Atlantic/Reykjavik") }
	
	/**
	 * Atlantic/South_Georgia: South Georgia Time
	 */
	public val atlantic_southgeorgia: ZoneId by lazy { ZoneId.of("Atlantic/South_Georgia") }
	
	/**
	 * Atlantic/St_Helena: Greenwich Mean Time
	 */
	public val atlantic_sthelena: ZoneId by lazy { ZoneId.of("Atlantic/St_Helena") }
	
	/**
	 * Atlantic/Stanley: Falkland Islands Time
	 */
	public val atlantic_stanley: ZoneId by lazy { ZoneId.of("Atlantic/Stanley") }
	
	/**
	 * Australia/ACT: Eastern Australia Time
	 */
	public val australia_act: ZoneId by lazy { ZoneId.of("Australia/ACT") }
	
	/**
	 * Australia/Adelaide: Central Australia Time
	 */
	public val australia_adelaide: ZoneId by lazy { ZoneId.of("Australia/Adelaide") }
	
	/**
	 * Australia/Brisbane: Eastern Australia Time
	 */
	public val australia_brisbane: ZoneId by lazy { ZoneId.of("Australia/Brisbane") }
	
	/**
	 * Australia/Broken_Hill: Central Australia Time
	 */
	public val australia_brokenhill: ZoneId by lazy { ZoneId.of("Australia/Broken_Hill") }
	
	/**
	 * Australia/Canberra: Eastern Australia Time
	 */
	public val australia_canberra: ZoneId by lazy { ZoneId.of("Australia/Canberra") }
	
	/**
	 * Australia/Currie: Eastern Australia Time
	 */
	public val australia_currie: ZoneId by lazy { ZoneId.of("Australia/Currie") }
	
	/**
	 * Australia/Darwin: Central Australia Time
	 */
	public val australia_darwin: ZoneId by lazy { ZoneId.of("Australia/Darwin") }
	
	/**
	 * Australia/Eucla: Australian Central Western Time
	 */
	public val australia_eucla: ZoneId by lazy { ZoneId.of("Australia/Eucla") }
	
	/**
	 * Australia/Hobart: Eastern Australia Time
	 */
	public val australia_hobart: ZoneId by lazy { ZoneId.of("Australia/Hobart") }
	
	/**
	 * Australia/LHI: Lord Howe Time
	 */
	public val australia_lhi: ZoneId by lazy { ZoneId.of("Australia/LHI") }
	
	/**
	 * Australia/Lindeman: Eastern Australia Time
	 */
	public val australia_lindeman: ZoneId by lazy { ZoneId.of("Australia/Lindeman") }
	
	/**
	 * Australia/Lord_Howe: Lord Howe Time
	 */
	public val australia_lordhowe: ZoneId by lazy { ZoneId.of("Australia/Lord_Howe") }
	
	/**
	 * Australia/Melbourne: Eastern Australia Time
	 */
	public val australia_melbourne: ZoneId by lazy { ZoneId.of("Australia/Melbourne") }
	
	/**
	 * Australia/NSW: Eastern Australia Time
	 */
	public val australia_nsw: ZoneId by lazy { ZoneId.of("Australia/NSW") }
	
	/**
	 * Australia/North: Central Australia Time
	 */
	public val australia_north: ZoneId by lazy { ZoneId.of("Australia/North") }
	
	/**
	 * Australia/Perth: Western Australia Time
	 */
	public val australia_perth: ZoneId by lazy { ZoneId.of("Australia/Perth") }
	
	/**
	 * Australia/Queensland: Eastern Australia Time
	 */
	public val australia_queensland: ZoneId by lazy { ZoneId.of("Australia/Queensland") }
	
	/**
	 * Australia/South: Central Australia Time
	 */
	public val australia_south: ZoneId by lazy { ZoneId.of("Australia/South") }
	
	/**
	 * Australia/Sydney: Eastern Australia Time
	 */
	public val australia_sydney: ZoneId by lazy { ZoneId.of("Australia/Sydney") }
	
	/**
	 * Australia/Tasmania: Eastern Australia Time
	 */
	public val australia_tasmania: ZoneId by lazy { ZoneId.of("Australia/Tasmania") }
	
	/**
	 * Australia/Victoria: Eastern Australia Time
	 */
	public val australia_victoria: ZoneId by lazy { ZoneId.of("Australia/Victoria") }
	
	/**
	 * Australia/West: Western Australia Time
	 */
	public val australia_west: ZoneId by lazy { ZoneId.of("Australia/West") }
	
	/**
	 * Australia/Yancowinna: Central Australia Time
	 */
	public val australia_yancowinna: ZoneId by lazy { ZoneId.of("Australia/Yancowinna") }
	
	/**
	 * Brazil/Acre: Acre Time
	 */
	public val brazil_acre: ZoneId by lazy { ZoneId.of("Brazil/Acre") }
	
	/**
	 * Brazil/DeNoronha: Fernando de Noronha Time
	 */
	public val brazil_deNoronha: ZoneId by lazy { ZoneId.of("Brazil/DeNoronha") }
	
	/**
	 * Brazil/East: Brasilia Time
	 */
	public val brazil_east: ZoneId by lazy { ZoneId.of("Brazil/East") }
	
	/**
	 * Brazil/West: Amazon Time
	 */
	public val brazil_west: ZoneId by lazy { ZoneId.of("Brazil/West") }
	
	/**
	 * CET: Central European Time
	 */
	public val cet: ZoneId by lazy { ZoneId.of("CET") }
	
	/**
	 * CST6CDT: Central Time
	 */
	public val cst6cdt: ZoneId by lazy { ZoneId.of("CST6CDT") }
	
	/**
	 * Canada/Atlantic: Atlantic Time
	 */
	public val canada_atlantic: ZoneId by lazy { ZoneId.of("Canada/Atlantic") }
	
	/**
	 * Canada/Central: Central Time
	 */
	public val canada_central: ZoneId by lazy { ZoneId.of("Canada/Central") }
	
	/**
	 * Canada/Eastern: Eastern Time
	 */
	public val canada_eastern: ZoneId by lazy { ZoneId.of("Canada/Eastern") }
	
	/**
	 * Canada/Mountain: Mountain Time
	 */
	public val canada_mountain: ZoneId by lazy { ZoneId.of("Canada/Mountain") }
	
	/**
	 * Canada/Newfoundland: Newfoundland Time
	 */
	public val canada_newfoundland: ZoneId by lazy { ZoneId.of("Canada/Newfoundland") }
	
	/**
	 * Canada/Pacific: Pacific Time
	 */
	public val canada_pacific: ZoneId by lazy { ZoneId.of("Canada/Pacific") }
	
	/**
	 * Canada/Saskatchewan: Central Time
	 */
	public val canada_saskatchewan: ZoneId by lazy { ZoneId.of("Canada/Saskatchewan") }
	
	/**
	 * Canada/Yukon: Mountain Time
	 */
	public val canada_yukon: ZoneId by lazy { ZoneId.of("Canada/Yukon") }
	
	/**
	 * Chile/Continental: Chile Time
	 */
	public val chile_continental: ZoneId by lazy { ZoneId.of("Chile/Continental") }
	
	/**
	 * Chile/EasterIsland: Easter Island Time
	 */
	public val chile_easterIsland: ZoneId by lazy { ZoneId.of("Chile/EasterIsland") }
	
	/**
	 * Cuba: Cuba Time
	 */
	public val cuba: ZoneId by lazy { ZoneId.of("Cuba") }
	
	/**
	 * EET: Eastern European Time
	 */
	public val eet: ZoneId by lazy { ZoneId.of("EET") }
	
	/**
	 * EST5EDT: Eastern Time
	 */
	public val est5edt: ZoneId by lazy { ZoneId.of("EST5EDT") }
	
	/**
	 * Egypt: Eastern European Time
	 */
	public val egypt: ZoneId by lazy { ZoneId.of("Egypt") }
	
	/**
	 * Eire: Irish Time
	 */
	public val eire: ZoneId by lazy { ZoneId.of("Eire") }
	
	/**
	 * Etc/GMT: Greenwich Mean Time (POSIX-conform offset)
	 */
	public val etc_gmt: ZoneId by lazy { ZoneId.of("Etc/GMT") }
	
	/**
	 * Etc/GMT+0: Greenwich Mean Time (POSIX-conform offset)
	 */
	public val etc_gmt_plus0: ZoneId by lazy { ZoneId.of("Etc/GMT+0") }
	
	/**
	 * Etc/GMT+1: GMT-01:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus1: ZoneId by lazy { ZoneId.of("Etc/GMT+1") }
	
	/**
	 * Etc/GMT+10: GMT-10:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus10: ZoneId by lazy { ZoneId.of("Etc/GMT+10") }
	
	/**
	 * Etc/GMT+11: GMT-11:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus11: ZoneId by lazy { ZoneId.of("Etc/GMT+11") }
	
	/**
	 * Etc/GMT+12: GMT-12:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus12: ZoneId by lazy { ZoneId.of("Etc/GMT+12") }
	
	/**
	 * Etc/GMT+2: GMT-02:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus2: ZoneId by lazy { ZoneId.of("Etc/GMT+2") }
	
	/**
	 * Etc/GMT+3: GMT-03:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus3: ZoneId by lazy { ZoneId.of("Etc/GMT+3") }
	
	/**
	 * Etc/GMT+4: GMT-04:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus4: ZoneId by lazy { ZoneId.of("Etc/GMT+4") }
	
	/**
	 * Etc/GMT+5: GMT-05:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus5: ZoneId by lazy { ZoneId.of("Etc/GMT+5") }
	
	/**
	 * Etc/GMT+6: GMT-06:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus6: ZoneId by lazy { ZoneId.of("Etc/GMT+6") }
	
	/**
	 * Etc/GMT+7: GMT-07:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus7: ZoneId by lazy { ZoneId.of("Etc/GMT+7") }
	
	/**
	 * Etc/GMT+8: GMT-08:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus8: ZoneId by lazy { ZoneId.of("Etc/GMT+8") }
	
	/**
	 * Etc/GMT+9: GMT-09:00 (POSIX-conform offset)
	 */
	public val etc_gmt_plus9: ZoneId by lazy { ZoneId.of("Etc/GMT+9") }
	
	/**
	 * Etc/GMT-0: Greenwich Mean Time (POSIX-conform offset)
	 */
	public val etc_gmt_0: ZoneId by lazy { ZoneId.of("Etc/GMT-0") }
	
	/**
	 * Etc/GMT-1: GMT+01:00 (POSIX-conform offset)
	 */
	public val etc_gmt_1: ZoneId by lazy { ZoneId.of("Etc/GMT-1") }
	
	/**
	 * Etc/GMT-10: GMT+10:00 (POSIX-conform offset)
	 */
	public val etc_gmt_10: ZoneId by lazy { ZoneId.of("Etc/GMT-10") }
	
	/**
	 * Etc/GMT-11: GMT+11:00 (POSIX-conform offset)
	 */
	public val etc_gmt_11: ZoneId by lazy { ZoneId.of("Etc/GMT-11") }
	
	/**
	 * Etc/GMT-12: GMT+12:00 (POSIX-conform offset)
	 */
	public val etc_gmt_12: ZoneId by lazy { ZoneId.of("Etc/GMT-12") }
	
	/**
	 * Etc/GMT-13: GMT+13:00 (POSIX-conform offset)
	 */
	public val etc_gmt_13: ZoneId by lazy { ZoneId.of("Etc/GMT-13") }
	
	/**
	 * Etc/GMT-14: GMT+14:00 (POSIX-conform offset)
	 */
	public val etc_gmt_14: ZoneId by lazy { ZoneId.of("Etc/GMT-14") }
	
	/**
	 * Etc/GMT-2: GMT+02:00 (POSIX-conform offset)
	 */
	public val etc_gmt_2: ZoneId by lazy { ZoneId.of("Etc/GMT-2") }
	
	/**
	 * Etc/GMT-3: GMT+03:00 (POSIX-conform offset)
	 */
	public val etc_gmt_3: ZoneId by lazy { ZoneId.of("Etc/GMT-3") }
	
	/**
	 * Etc/GMT-4: GMT+04:00 (POSIX-conform offset)
	 */
	public val etc_gmt_4: ZoneId by lazy { ZoneId.of("Etc/GMT-4") }
	
	/**
	 * Etc/GMT-5: GMT+05:00 (POSIX-conform offset)
	 */
	public val etc_gmt_5: ZoneId by lazy { ZoneId.of("Etc/GMT-5") }
	
	/**
	 * Etc/GMT-6: GMT+06:00 (POSIX-conform offset)
	 */
	public val etc_gmt_6: ZoneId by lazy { ZoneId.of("Etc/GMT-6") }
	
	/**
	 * Etc/GMT-7: GMT+07:00 (POSIX-conform offset)
	 */
	public val etc_gmt_7: ZoneId by lazy { ZoneId.of("Etc/GMT-7") }
	
	/**
	 * Etc/GMT-8: GMT+08:00 (POSIX-conform offset)
	 */
	public val etc_gmt_8: ZoneId by lazy { ZoneId.of("Etc/GMT-8") }
	
	/**
	 * Etc/GMT-9: GMT+09:00 (POSIX-conform offset)
	 */
	public val etc_gmt_9: ZoneId by lazy { ZoneId.of("Etc/GMT-9") }
	
	/**
	 * Etc/GMT0: Greenwich Mean Time (POSIX-conform offset)
	 */
	public val etc_gmt0: ZoneId by lazy { ZoneId.of("Etc/GMT0") }
	
	/**
	 * Etc/Greenwich: Greenwich Mean Time (POSIX-conform offset)
	 */
	public val etc_greenwich: ZoneId by lazy { ZoneId.of("Etc/Greenwich") }
	
	/**
	 * Etc/UCT: Coordinated Universal Time (POSIX-conform offset)
	 */
	public val etc_uct: ZoneId by lazy { ZoneId.of("Etc/UCT") }
	
	/**
	 * Etc/UTC: Coordinated Universal Time (POSIX-conform offset)
	 */
	public val etc_utc: ZoneId by lazy { ZoneId.of("Etc/UTC") }
	
	/**
	 * Etc/Universal: Coordinated Universal Time (POSIX-conform offset)
	 */
	public val etc_universal: ZoneId by lazy { ZoneId.of("Etc/Universal") }
	
	/**
	 * Etc/Zulu: Coordinated Universal Time (POSIX-conform offset)
	 */
	public val etc_zulu: ZoneId by lazy { ZoneId.of("Etc/Zulu") }
	
	/**
	 * Europe/Amsterdam: Central European Time
	 */
	public val europe_amsterdam: ZoneId by lazy { ZoneId.of("Europe/Amsterdam") }
	
	/**
	 * Europe/Andorra: Central European Time
	 */
	public val europe_andorra: ZoneId by lazy { ZoneId.of("Europe/Andorra") }
	
	/**
	 * Europe/Astrakhan: Astrakhan Time
	 */
	public val europe_astrakhan: ZoneId by lazy { ZoneId.of("Europe/Astrakhan") }
	
	/**
	 * Europe/Athens: Eastern European Time
	 */
	public val europe_athens: ZoneId by lazy { ZoneId.of("Europe/Athens") }
	
	/**
	 * Europe/Belfast: British Time
	 */
	public val europe_belfast: ZoneId by lazy { ZoneId.of("Europe/Belfast") }
	
	/**
	 * Europe/Belgrade: Central European Time
	 */
	public val europe_belgrade: ZoneId by lazy { ZoneId.of("Europe/Belgrade") }
	
	/**
	 * Europe/Berlin: Central European Time
	 */
	public val europe_berlin: ZoneId by lazy { ZoneId.of("Europe/Berlin") }
	
	/**
	 * Europe/Bratislava: Central European Time
	 */
	public val europe_bratislava: ZoneId by lazy { ZoneId.of("Europe/Bratislava") }
	
	/**
	 * Europe/Brussels: Central European Time
	 */
	public val europe_brussels: ZoneId by lazy { ZoneId.of("Europe/Brussels") }
	
	/**
	 * Europe/Bucharest: Eastern European Time
	 */
	public val europe_bucharest: ZoneId by lazy { ZoneId.of("Europe/Bucharest") }
	
	/**
	 * Europe/Budapest: Central European Time
	 */
	public val europe_budapest: ZoneId by lazy { ZoneId.of("Europe/Budapest") }
	
	/**
	 * Europe/Busingen: Central European Time
	 */
	public val europe_busingen: ZoneId by lazy { ZoneId.of("Europe/Busingen") }
	
	/**
	 * Europe/Chisinau: Eastern European Time
	 */
	public val europe_chisinau: ZoneId by lazy { ZoneId.of("Europe/Chisinau") }
	
	/**
	 * Europe/Copenhagen: Central European Time
	 */
	public val europe_copenhagen: ZoneId by lazy { ZoneId.of("Europe/Copenhagen") }
	
	/**
	 * Europe/Dublin: Irish Time
	 */
	public val europe_dublin: ZoneId by lazy { ZoneId.of("Europe/Dublin") }
	
	/**
	 * Europe/Gibraltar: Central European Time
	 */
	public val europe_gibraltar: ZoneId by lazy { ZoneId.of("Europe/Gibraltar") }
	
	/**
	 * Europe/Guernsey: British Time
	 */
	public val europe_guernsey: ZoneId by lazy { ZoneId.of("Europe/Guernsey") }
	
	/**
	 * Europe/Helsinki: Eastern European Time
	 */
	public val europe_helsinki: ZoneId by lazy { ZoneId.of("Europe/Helsinki") }
	
	/**
	 * Europe/Isle_of_Man: British Time
	 */
	public val europe_isleofman: ZoneId by lazy { ZoneId.of("Europe/Isle_of_Man") }
	
	/**
	 * Europe/Istanbul: Turkey Time
	 */
	public val europe_istanbul: ZoneId by lazy { ZoneId.of("Europe/Istanbul") }
	
	/**
	 * Europe/Jersey: British Time
	 */
	public val europe_jersey: ZoneId by lazy { ZoneId.of("Europe/Jersey") }
	
	/**
	 * Europe/Kaliningrad: Eastern European Time
	 */
	public val europe_kaliningrad: ZoneId by lazy { ZoneId.of("Europe/Kaliningrad") }
	
	/**
	 * Europe/Kiev: Eastern European Time
	 */
	public val europe_kiev: ZoneId by lazy { ZoneId.of("Europe/Kiev") }
	
	/**
	 * Europe/Kirov: Moscow Time
	 */
	public val europe_kirov: ZoneId by lazy { ZoneId.of("Europe/Kirov") }
	
	/**
	 * Europe/Kyiv: Eastern European Time
	 */
	public val europe_kyiv: ZoneId by lazy { ZoneId.of("Europe/Kyiv") }
	
	/**
	 * Europe/Lisbon: Western European Time
	 */
	public val europe_lisbon: ZoneId by lazy { ZoneId.of("Europe/Lisbon") }
	
	/**
	 * Europe/Ljubljana: Central European Time
	 */
	public val europe_ljubljana: ZoneId by lazy { ZoneId.of("Europe/Ljubljana") }
	
	/**
	 * Europe/London: British Time
	 */
	public val europe_london: ZoneId by lazy { ZoneId.of("Europe/London") }
	
	/**
	 * Europe/Luxembourg: Central European Time
	 */
	public val europe_luxembourg: ZoneId by lazy { ZoneId.of("Europe/Luxembourg") }
	
	/**
	 * Europe/Madrid: Central European Time
	 */
	public val europe_madrid: ZoneId by lazy { ZoneId.of("Europe/Madrid") }
	
	/**
	 * Europe/Malta: Central European Time
	 */
	public val europe_malta: ZoneId by lazy { ZoneId.of("Europe/Malta") }
	
	/**
	 * Europe/Mariehamn: Eastern European Time
	 */
	public val europe_mariehamn: ZoneId by lazy { ZoneId.of("Europe/Mariehamn") }
	
	/**
	 * Europe/Minsk: Moscow Time
	 */
	public val europe_minsk: ZoneId by lazy { ZoneId.of("Europe/Minsk") }
	
	/**
	 * Europe/Monaco: Central European Time
	 */
	public val europe_monaco: ZoneId by lazy { ZoneId.of("Europe/Monaco") }
	
	/**
	 * Europe/Moscow: Moscow Time
	 */
	public val europe_moscow: ZoneId by lazy { ZoneId.of("Europe/Moscow") }
	
	/**
	 * Europe/Nicosia: Eastern European Time
	 */
	public val europe_nicosia: ZoneId by lazy { ZoneId.of("Europe/Nicosia") }
	
	/**
	 * Europe/Oslo: Central European Time
	 */
	public val europe_oslo: ZoneId by lazy { ZoneId.of("Europe/Oslo") }
	
	/**
	 * Europe/Paris: Central European Time
	 */
	public val europe_paris: ZoneId by lazy { ZoneId.of("Europe/Paris") }
	
	/**
	 * Europe/Podgorica: Central European Time
	 */
	public val europe_podgorica: ZoneId by lazy { ZoneId.of("Europe/Podgorica") }
	
	/**
	 * Europe/Prague: Central European Time
	 */
	public val europe_prague: ZoneId by lazy { ZoneId.of("Europe/Prague") }
	
	/**
	 * Europe/Riga: Eastern European Time
	 */
	public val europe_riga: ZoneId by lazy { ZoneId.of("Europe/Riga") }
	
	/**
	 * Europe/Rome: Central European Time
	 */
	public val europe_rome: ZoneId by lazy { ZoneId.of("Europe/Rome") }
	
	/**
	 * Europe/Samara: Samara Time
	 */
	public val europe_samara: ZoneId by lazy { ZoneId.of("Europe/Samara") }
	
	/**
	 * Europe/San_Marino: Central European Time
	 */
	public val europe_sanmarino: ZoneId by lazy { ZoneId.of("Europe/San_Marino") }
	
	/**
	 * Europe/Sarajevo: Central European Time
	 */
	public val europe_sarajevo: ZoneId by lazy { ZoneId.of("Europe/Sarajevo") }
	
	/**
	 * Europe/Saratov: Saratov Time
	 */
	public val europe_saratov: ZoneId by lazy { ZoneId.of("Europe/Saratov") }
	
	/**
	 * Europe/Simferopol: Moscow Time
	 */
	public val europe_simferopol: ZoneId by lazy { ZoneId.of("Europe/Simferopol") }
	
	/**
	 * Europe/Skopje: Central European Time
	 */
	public val europe_skopje: ZoneId by lazy { ZoneId.of("Europe/Skopje") }
	
	/**
	 * Europe/Sofia: Eastern European Time
	 */
	public val europe_sofia: ZoneId by lazy { ZoneId.of("Europe/Sofia") }
	
	/**
	 * Europe/Stockholm: Central European Time
	 */
	public val europe_stockholm: ZoneId by lazy { ZoneId.of("Europe/Stockholm") }
	
	/**
	 * Europe/Tallinn: Eastern European Time
	 */
	public val europe_tallinn: ZoneId by lazy { ZoneId.of("Europe/Tallinn") }
	
	/**
	 * Europe/Tirane: Central European Time
	 */
	public val europe_tirane: ZoneId by lazy { ZoneId.of("Europe/Tirane") }
	
	/**
	 * Europe/Tiraspol: Eastern European Time
	 */
	public val europe_tiraspol: ZoneId by lazy { ZoneId.of("Europe/Tiraspol") }
	
	/**
	 * Europe/Ulyanovsk: Ulyanovsk Time
	 */
	public val europe_ulyanovsk: ZoneId by lazy { ZoneId.of("Europe/Ulyanovsk") }
	
	/**
	 * Europe/Uzhgorod: Eastern European Time
	 */
	public val europe_uzhgorod: ZoneId by lazy { ZoneId.of("Europe/Uzhgorod") }
	
	/**
	 * Europe/Vaduz: Central European Time
	 */
	public val europe_vaduz: ZoneId by lazy { ZoneId.of("Europe/Vaduz") }
	
	/**
	 * Europe/Vatican: Central European Time
	 */
	public val europe_vatican: ZoneId by lazy { ZoneId.of("Europe/Vatican") }
	
	/**
	 * Europe/Vienna: Central European Time
	 */
	public val europe_vienna: ZoneId by lazy { ZoneId.of("Europe/Vienna") }
	
	/**
	 * Europe/Vilnius: Eastern European Time
	 */
	public val europe_vilnius: ZoneId by lazy { ZoneId.of("Europe/Vilnius") }
	
	/**
	 * Europe/Volgograd: Volgograd Time
	 */
	public val europe_volgograd: ZoneId by lazy { ZoneId.of("Europe/Volgograd") }
	
	/**
	 * Europe/Warsaw: Central European Time
	 */
	public val europe_warsaw: ZoneId by lazy { ZoneId.of("Europe/Warsaw") }
	
	/**
	 * Europe/Zagreb: Central European Time
	 */
	public val europe_zagreb: ZoneId by lazy { ZoneId.of("Europe/Zagreb") }
	
	/**
	 * Europe/Zaporozhye: Eastern European Time
	 */
	public val europe_zaporozhye: ZoneId by lazy { ZoneId.of("Europe/Zaporozhye") }
	
	/**
	 * Europe/Zurich: Central European Time
	 */
	public val europe_zurich: ZoneId by lazy { ZoneId.of("Europe/Zurich") }
	
	/**
	 * GB: British Time
	 */
	public val gb: ZoneId by lazy { ZoneId.of("GB") }
	
	/**
	 * GB-Eire: British Time
	 */
	public val gbeire: ZoneId by lazy { ZoneId.of("GB-Eire") }
	
	/**
	 * GMT: Greenwich Mean Time
	 */
	public val gmt: ZoneId by lazy { ZoneId.of("GMT") }
	
	/**
	 * GMT0: Greenwich Mean Time
	 */
	public val gmt0: ZoneId by lazy { ZoneId.of("GMT0") }
	
	/**
	 * Greenwich: Greenwich Mean Time
	 */
	public val greenwich: ZoneId by lazy { ZoneId.of("Greenwich") }
	
	/**
	 * Hongkong: Hong Kong Time
	 */
	public val hongkong: ZoneId by lazy { ZoneId.of("Hongkong") }
	
	/**
	 * Iceland: Greenwich Mean Time
	 */
	public val iceland: ZoneId by lazy { ZoneId.of("Iceland") }
	
	/**
	 * Indian/Antananarivo: Eastern Africa Time
	 */
	public val indian_antananarivo: ZoneId by lazy { ZoneId.of("Indian/Antananarivo") }
	
	/**
	 * Indian/Chagos: Indian Ocean Territory Time
	 */
	public val indian_chagos: ZoneId by lazy { ZoneId.of("Indian/Chagos") }
	
	/**
	 * Indian/Christmas: Christmas Island Time
	 */
	public val indian_christmas: ZoneId by lazy { ZoneId.of("Indian/Christmas") }
	
	/**
	 * Indian/Cocos: Cocos Islands Time
	 */
	public val indian_cocos: ZoneId by lazy { ZoneId.of("Indian/Cocos") }
	
	/**
	 * Indian/Comoro: Eastern Africa Time
	 */
	public val indian_comoro: ZoneId by lazy { ZoneId.of("Indian/Comoro") }
	
	/**
	 * Indian/Kerguelen: French Southern & Antarctic Lands Time
	 */
	public val indian_kerguelen: ZoneId by lazy { ZoneId.of("Indian/Kerguelen") }
	
	/**
	 * Indian/Mahe: Seychelles Time
	 */
	public val indian_mahe: ZoneId by lazy { ZoneId.of("Indian/Mahe") }
	
	/**
	 * Indian/Maldives: Maldives Time
	 */
	public val indian_maldives: ZoneId by lazy { ZoneId.of("Indian/Maldives") }
	
	/**
	 * Indian/Mauritius: Mauritius Time
	 */
	public val indian_mauritius: ZoneId by lazy { ZoneId.of("Indian/Mauritius") }
	
	/**
	 * Indian/Mayotte: Eastern Africa Time
	 */
	public val indian_mayotte: ZoneId by lazy { ZoneId.of("Indian/Mayotte") }
	
	/**
	 * Indian/Reunion: Reunion Time
	 */
	public val indian_reunion: ZoneId by lazy { ZoneId.of("Indian/Reunion") }
	
	/**
	 * Iran: Iran Time
	 */
	public val iran: ZoneId by lazy { ZoneId.of("Iran") }
	
	/**
	 * Israel: Israel Time
	 */
	public val israel: ZoneId by lazy { ZoneId.of("Israel") }
	
	/**
	 * Jamaica: Eastern Time
	 */
	public val jamaica: ZoneId by lazy { ZoneId.of("Jamaica") }
	
	/**
	 * Japan: Japan Time
	 */
	public val japan: ZoneId by lazy { ZoneId.of("Japan") }
	
	/**
	 * Kwajalein: Marshall Islands Time
	 */
	public val kwajalein: ZoneId by lazy { ZoneId.of("Kwajalein") }
	
	/**
	 * Libya: Eastern European Time
	 */
	public val libya: ZoneId by lazy { ZoneId.of("Libya") }
	
	/**
	 * MET: Middle Europe Time
	 */
	public val met: ZoneId by lazy { ZoneId.of("MET") }
	
	/**
	 * MST7MDT: Mountain Time
	 */
	public val mst7mdt: ZoneId by lazy { ZoneId.of("MST7MDT") }
	
	/**
	 * Mexico/BajaNorte: Pacific Time
	 */
	public val mexico_bajaNorte: ZoneId by lazy { ZoneId.of("Mexico/BajaNorte") }
	
	/**
	 * Mexico/BajaSur: Mexican Pacific Time
	 */
	public val mexico_bajaSur: ZoneId by lazy { ZoneId.of("Mexico/BajaSur") }
	
	/**
	 * Mexico/General: Central Time
	 */
	public val mexico_general: ZoneId by lazy { ZoneId.of("Mexico/General") }
	
	/**
	 * NZ: New Zealand Time
	 */
	public val nz: ZoneId by lazy { ZoneId.of("NZ") }
	
	/**
	 * NZ-CHAT: Chatham Time
	 */
	public val nzchat: ZoneId by lazy { ZoneId.of("NZ-CHAT") }
	
	/**
	 * Navajo: Mountain Time
	 */
	public val navajo: ZoneId by lazy { ZoneId.of("Navajo") }
	
	/**
	 * PRC: China Time
	 */
	public val prc: ZoneId by lazy { ZoneId.of("PRC") }
	
	/**
	 * PST8PDT: Pacific Time
	 */
	public val pst8pdt: ZoneId by lazy { ZoneId.of("PST8PDT") }
	
	/**
	 * Pacific/Apia: Apia Time
	 */
	public val pacific_apia: ZoneId by lazy { ZoneId.of("Pacific/Apia") }
	
	/**
	 * Pacific/Auckland: New Zealand Time
	 */
	public val pacific_auckland: ZoneId by lazy { ZoneId.of("Pacific/Auckland") }
	
	/**
	 * Pacific/Bougainville: Bougainville Time
	 */
	public val pacific_bougainville: ZoneId by lazy { ZoneId.of("Pacific/Bougainville") }
	
	/**
	 * Pacific/Chatham: Chatham Time
	 */
	public val pacific_chatham: ZoneId by lazy { ZoneId.of("Pacific/Chatham") }
	
	/**
	 * Pacific/Chuuk: Chuuk Time
	 */
	public val pacific_chuuk: ZoneId by lazy { ZoneId.of("Pacific/Chuuk") }
	
	/**
	 * Pacific/Easter: Easter Island Time
	 */
	public val pacific_easter: ZoneId by lazy { ZoneId.of("Pacific/Easter") }
	
	/**
	 * Pacific/Efate: Vanuatu Time
	 */
	public val pacific_efate: ZoneId by lazy { ZoneId.of("Pacific/Efate") }
	
	/**
	 * Pacific/Enderbury: Phoenix Is. Time
	 */
	public val pacific_enderbury: ZoneId by lazy { ZoneId.of("Pacific/Enderbury") }
	
	/**
	 * Pacific/Fakaofo: Tokelau Time
	 */
	public val pacific_fakaofo: ZoneId by lazy { ZoneId.of("Pacific/Fakaofo") }
	
	/**
	 * Pacific/Fiji: Fiji Time
	 */
	public val pacific_fiji: ZoneId by lazy { ZoneId.of("Pacific/Fiji") }
	
	/**
	 * Pacific/Funafuti: Tuvalu Time
	 */
	public val pacific_funafuti: ZoneId by lazy { ZoneId.of("Pacific/Funafuti") }
	
	/**
	 * Pacific/Galapagos: Galapagos Time
	 */
	public val pacific_galapagos: ZoneId by lazy { ZoneId.of("Pacific/Galapagos") }
	
	/**
	 * Pacific/Gambier: Gambier Time
	 */
	public val pacific_gambier: ZoneId by lazy { ZoneId.of("Pacific/Gambier") }
	
	/**
	 * Pacific/Guadalcanal: Solomon Is. Time
	 */
	public val pacific_guadalcanal: ZoneId by lazy { ZoneId.of("Pacific/Guadalcanal") }
	
	/**
	 * Pacific/Guam: Chamorro Time
	 */
	public val pacific_guam: ZoneId by lazy { ZoneId.of("Pacific/Guam") }
	
	/**
	 * Pacific/Honolulu: Hawaii-Aleutian Time
	 */
	public val pacific_honolulu: ZoneId by lazy { ZoneId.of("Pacific/Honolulu") }
	
	/**
	 * Pacific/Johnston: Hawaii-Aleutian Time
	 */
	public val pacific_johnston: ZoneId by lazy { ZoneId.of("Pacific/Johnston") }
	
	/**
	 * Pacific/Kanton: Phoenix Is. Time
	 */
	public val pacific_kanton: ZoneId by lazy { ZoneId.of("Pacific/Kanton") }
	
	/**
	 * Pacific/Kiritimati: Line Is. Time
	 */
	public val pacific_kiritimati: ZoneId by lazy { ZoneId.of("Pacific/Kiritimati") }
	
	/**
	 * Pacific/Kosrae: Kosrae Time
	 */
	public val pacific_kosrae: ZoneId by lazy { ZoneId.of("Pacific/Kosrae") }
	
	/**
	 * Pacific/Kwajalein: Marshall Islands Time
	 */
	public val pacific_kwajalein: ZoneId by lazy { ZoneId.of("Pacific/Kwajalein") }
	
	/**
	 * Pacific/Majuro: Marshall Islands Time
	 */
	public val pacific_majuro: ZoneId by lazy { ZoneId.of("Pacific/Majuro") }
	
	/**
	 * Pacific/Marquesas: Marquesas Time
	 */
	public val pacific_marquesas: ZoneId by lazy { ZoneId.of("Pacific/Marquesas") }
	
	/**
	 * Pacific/Midway: Samoa Time
	 */
	public val pacific_midway: ZoneId by lazy { ZoneId.of("Pacific/Midway") }
	
	/**
	 * Pacific/Nauru: Nauru Time
	 */
	public val pacific_nauru: ZoneId by lazy { ZoneId.of("Pacific/Nauru") }
	
	/**
	 * Pacific/Niue: Niue Time
	 */
	public val pacific_niue: ZoneId by lazy { ZoneId.of("Pacific/Niue") }
	
	/**
	 * Pacific/Norfolk: Norfolk Island Time
	 */
	public val pacific_norfolk: ZoneId by lazy { ZoneId.of("Pacific/Norfolk") }
	
	/**
	 * Pacific/Noumea: New Caledonia Time
	 */
	public val pacific_noumea: ZoneId by lazy { ZoneId.of("Pacific/Noumea") }
	
	/**
	 * Pacific/Pago_Pago: Samoa Time
	 */
	public val pacific_pagopago: ZoneId by lazy { ZoneId.of("Pacific/Pago_Pago") }
	
	/**
	 * Pacific/Palau: Palau Time
	 */
	public val pacific_palau: ZoneId by lazy { ZoneId.of("Pacific/Palau") }
	
	/**
	 * Pacific/Pitcairn: Pitcairn Time
	 */
	public val pacific_pitcairn: ZoneId by lazy { ZoneId.of("Pacific/Pitcairn") }
	
	/**
	 * Pacific/Pohnpei: Ponape Time
	 */
	public val pacific_pohnpei: ZoneId by lazy { ZoneId.of("Pacific/Pohnpei") }
	
	/**
	 * Pacific/Ponape: Ponape Time
	 */
	public val pacific_ponape: ZoneId by lazy { ZoneId.of("Pacific/Ponape") }
	
	/**
	 * Pacific/Port_Moresby: Papua New Guinea Time
	 */
	public val pacific_portmoresby: ZoneId by lazy { ZoneId.of("Pacific/Port_Moresby") }
	
	/**
	 * Pacific/Rarotonga: Cook Islands Time
	 */
	public val pacific_rarotonga: ZoneId by lazy { ZoneId.of("Pacific/Rarotonga") }
	
	/**
	 * Pacific/Saipan: Chamorro Time
	 */
	public val pacific_saipan: ZoneId by lazy { ZoneId.of("Pacific/Saipan") }
	
	/**
	 * Pacific/Samoa: Samoa Time
	 */
	public val pacific_samoa: ZoneId by lazy { ZoneId.of("Pacific/Samoa") }
	
	/**
	 * Pacific/Tahiti: Tahiti Time
	 */
	public val pacific_tahiti: ZoneId by lazy { ZoneId.of("Pacific/Tahiti") }
	
	/**
	 * Pacific/Tarawa: Gilbert Is. Time
	 */
	public val pacific_tarawa: ZoneId by lazy { ZoneId.of("Pacific/Tarawa") }
	
	/**
	 * Pacific/Tongatapu: Tonga Time
	 */
	public val pacific_tongatapu: ZoneId by lazy { ZoneId.of("Pacific/Tongatapu") }
	
	/**
	 * Pacific/Truk: Chuuk Time
	 */
	public val pacific_truk: ZoneId by lazy { ZoneId.of("Pacific/Truk") }
	
	/**
	 * Pacific/Wake: Wake Time
	 */
	public val pacific_wake: ZoneId by lazy { ZoneId.of("Pacific/Wake") }
	
	/**
	 * Pacific/Wallis: Wallis & Futuna Time
	 */
	public val pacific_wallis: ZoneId by lazy { ZoneId.of("Pacific/Wallis") }
	
	/**
	 * Pacific/Yap: Chuuk Time
	 */
	public val pacific_yap: ZoneId by lazy { ZoneId.of("Pacific/Yap") }
	
	/**
	 * Poland: Central European Time
	 */
	public val poland: ZoneId by lazy { ZoneId.of("Poland") }
	
	/**
	 * Portugal: Western European Time
	 */
	public val portugal: ZoneId by lazy { ZoneId.of("Portugal") }
	
	/**
	 * ROK: Korean Time
	 */
	public val rok: ZoneId by lazy { ZoneId.of("ROK") }
	
	/**
	 * Singapore: Singapore Time
	 */
	public val singapore: ZoneId by lazy { ZoneId.of("Singapore") }
	
	/**
	 * SystemV/AST4: Atlantic Time
	 */
	public val systemV_ast4: ZoneId by lazy { ZoneId.of("SystemV/AST4") }
	
	/**
	 * SystemV/AST4ADT: Atlantic Time
	 */
	public val systemV_ast4adt: ZoneId by lazy { ZoneId.of("SystemV/AST4ADT") }
	
	/**
	 * SystemV/CST6: Central Time
	 */
	public val systemV_cst6: ZoneId by lazy { ZoneId.of("SystemV/CST6") }
	
	/**
	 * SystemV/CST6CDT: Central Time
	 */
	public val systemV_cst6cdt: ZoneId by lazy { ZoneId.of("SystemV/CST6CDT") }
	
	/**
	 * SystemV/EST5: Eastern Time
	 */
	public val systemV_est5: ZoneId by lazy { ZoneId.of("SystemV/EST5") }
	
	/**
	 * SystemV/EST5EDT: Eastern Time
	 */
	public val systemV_est5edt: ZoneId by lazy { ZoneId.of("SystemV/EST5EDT") }
	
	/**
	 * SystemV/HST10: Hawaii-Aleutian Time
	 */
	public val systemV_hst10: ZoneId by lazy { ZoneId.of("SystemV/HST10") }
	
	/**
	 * SystemV/MST7: Mountain Time
	 */
	public val systemV_mst7: ZoneId by lazy { ZoneId.of("SystemV/MST7") }
	
	/**
	 * SystemV/MST7MDT: Mountain Time
	 */
	public val systemV_mst7mdt: ZoneId by lazy { ZoneId.of("SystemV/MST7MDT") }
	
	/**
	 * SystemV/PST8: Pitcairn Time
	 */
	public val systemV_pst8: ZoneId by lazy { ZoneId.of("SystemV/PST8") }
	
	/**
	 * SystemV/PST8PDT: Pacific Time
	 */
	public val systemV_pst8pdt: ZoneId by lazy { ZoneId.of("SystemV/PST8PDT") }
	
	/**
	 * SystemV/YST9: Gambier Time
	 */
	public val systemV_yst9: ZoneId by lazy { ZoneId.of("SystemV/YST9") }
	
	/**
	 * SystemV/YST9YDT: Alaska Time
	 */
	public val systemV_yst9ydt: ZoneId by lazy { ZoneId.of("SystemV/YST9YDT") }
	
	/**
	 * Turkey: Turkey Time
	 */
	public val turkey: ZoneId by lazy { ZoneId.of("Turkey") }
	
	/**
	 * UCT: Coordinated Universal Time
	 */
	public val uct: ZoneId by lazy { ZoneId.of("UCT") }
	
	/**
	 * US/Alaska: Alaska Time
	 */
	public val us_alaska: ZoneId by lazy { ZoneId.of("US/Alaska") }
	
	/**
	 * US/Aleutian: Hawaii-Aleutian Time
	 */
	public val us_aleutian: ZoneId by lazy { ZoneId.of("US/Aleutian") }
	
	/**
	 * US/Arizona: Mountain Time
	 */
	public val us_arizona: ZoneId by lazy { ZoneId.of("US/Arizona") }
	
	/**
	 * US/Central: Central Time
	 */
	public val us_central: ZoneId by lazy { ZoneId.of("US/Central") }
	
	/**
	 * US/East-Indiana: Eastern Time
	 */
	public val us_eastindiana: ZoneId by lazy { ZoneId.of("US/East-Indiana") }
	
	/**
	 * US/Eastern: Eastern Time
	 */
	public val us_eastern: ZoneId by lazy { ZoneId.of("US/Eastern") }
	
	/**
	 * US/Hawaii: Hawaii-Aleutian Time
	 */
	public val us_hawaii: ZoneId by lazy { ZoneId.of("US/Hawaii") }
	
	/**
	 * US/Indiana-Starke: Central Time
	 */
	public val us_indianastarke: ZoneId by lazy { ZoneId.of("US/Indiana-Starke") }
	
	/**
	 * US/Michigan: Eastern Time
	 */
	public val us_michigan: ZoneId by lazy { ZoneId.of("US/Michigan") }
	
	/**
	 * US/Mountain: Mountain Time
	 */
	public val us_mountain: ZoneId by lazy { ZoneId.of("US/Mountain") }
	
	/**
	 * US/Pacific: Pacific Time
	 */
	public val us_pacific: ZoneId by lazy { ZoneId.of("US/Pacific") }
	
	/**
	 * US/Samoa: Samoa Time
	 */
	public val us_samoa: ZoneId by lazy { ZoneId.of("US/Samoa") }
	
	/**
	 * UTC: Coordinated Universal Time
	 */
	public val utc: ZoneId by lazy { ZoneId.of("UTC") }
	
	/**
	 * Universal: Coordinated Universal Time
	 */
	public val universal: ZoneId by lazy { ZoneId.of("Universal") }
	
	/**
	 * W-SU: Moscow Time
	 */
	public val wsu: ZoneId by lazy { ZoneId.of("W-SU") }
	
	/**
	 * WET: Western European Time
	 */
	public val wet: ZoneId by lazy { ZoneId.of("WET") }
	
	/**
	 * Zulu: Coordinated Universal Time
	 */
	public val zulu: ZoneId by lazy { ZoneId.of("Zulu") }
}
