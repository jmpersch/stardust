/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.datetime

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.require
import java.time.DateTimeException
import java.time.chrono.IsoChronology
import java.time.temporal.ChronoField.YEAR
import java.time.temporal.TemporalAccessor
import java.time.temporal.UnsupportedTemporalTypeException
import java.time.chrono.IsoChronology.INSTANCE as CHRONO_INSTANCE

/**
 * Defines utility methods for date specifications.
 *
 * @since 4.0
 */
public object DateUtils {
	
	/**
	 * Checks if the year is a leap year.
	 *
	 * @since 4.0
	 * @see IsoChronology.isLeapYear
	 */
	@Pure
	public fun isLeapYear(year: Int): Boolean = CHRONO_INSTANCE.isLeapYear(year.toLong())
	
	/**
	 * Gets the century of the specified temporal accessor, e.g. `"21"` for the 21<sup>st</sup> century.
	 *
	 * @param accessor The [temporal accessor][TemporalAccessor].
	 * @return The century of the specified [accessor].
	 * @throws DateTimeException A value for the field cannot be obtained or
	 * the value is outside the range of valid values for the field.
	 * @throws UnsupportedTemporalTypeException The [YEAR] field is not supported by the [accessor].
	 * @throws IllegalArgumentException The year is less than zero.
	 * @throws ArithmeticException A numeric overflow occurs.
	 * @see getCentury
	 */
	@Pure
	public fun getCenturyOf(accessor: TemporalAccessor): Int = getCentury(accessor.get(YEAR))
	
	/**
	 * Gets the century of the specified year, e.g. `"21"` for the 21<sup>st</sup> century.
	 *
	 * @param year The year.
	 * @return The century.
	 * @throws IllegalArgumentException [year] is less than zero.
	 * @see getCenturyOf
	 */
	@Pure
	public fun getCentury(year: Int): Int {
		
		require("year", year >= 0)
		
		return year / 100 + 1
	}
	
	/**
	 * Gets the millennium of the specified temporal accessor, e.g. `"3"` for the 3<sup>rd</sup> millennium.
	 *
	 * @param accessor The [temporal accessor][TemporalAccessor].
	 * @return The millennium of the specified [accessor].
	 * @throws DateTimeException A value for the field cannot be obtained or
	 * the value is outside the range of valid values for the field.
	 * @throws UnsupportedTemporalTypeException The [YEAR] field is not supported by the [accessor].
	 * @throws IllegalArgumentException The year is less than zero.
	 * @throws ArithmeticException A numeric overflow occurs.
	 * @see getMillennium
	 */
	@Pure
	public fun getMillenniumOf(accessor: TemporalAccessor): Int = getMillennium(accessor.get(YEAR))
	
	/**
	 * Gets the millennium of the specified year, e.g. `"3"` for the 3<sup>rd</sup> millennium.
	 *
	 * @param year The year.
	 * @return The millennium.
	 * @throws IllegalArgumentException [year] is less than zero.
	 * @see getMillenniumOf
	 */
	@Pure
	public fun getMillennium(year: Int): Int {
		
		require("year", year >= 0)
		
		return year / 1000 + 1
	}
	
	/**
	 * Gets the year in a two-digit form, e.g. `"15"` for 2015 or `"25"` for 2925.
	 *
	 * @param accessor The [temporal accessor][TemporalAccessor].
	 * @return The year of the specified [accessor] in a two-digit form.
	 * @throws DateTimeException A value for the field cannot be obtained or
	 * the value is outside the range of valid values for the field.
	 * @throws UnsupportedTemporalTypeException The [YEAR] field is not supported by the [accessor].
	 * @throws IllegalArgumentException The year is less than zero.
	 * @throws ArithmeticException A numeric overflow occurs.
	 * @see get2DigitYear
	 */
	@Pure
	public fun get2DigitYearOf(accessor: TemporalAccessor): Int = get2DigitYear(accessor.get(YEAR))
	
	/**
	 * Gets the year in a two-digit form, e.g. `"15"` for 2015 or `"25"` for 2925.
	 *
	 * @param year The year.
	 * @return The year in a two-digit form.
	 * @throws IllegalArgumentException [year] is less than zero.
	 * @see get2DigitYearOf
	 */
	@Pure
	public fun get2DigitYear(year: Int): Int {
		
		require("year", year >= 0)
		
		return year % 100
	}
	
	/**
	 * Gets the year in a three-digit form, e.g. `"15"` for 2015 or `"925"` for 2925.
	 *
	 * @param accessor The [temporal accessor][TemporalAccessor].
	 * @return The year of the specified [accessor] in a three-digit form.
	 * @throws DateTimeException A value for the field cannot be obtained or
	 * the value is outside the range of valid values for the field.
	 * @throws UnsupportedTemporalTypeException The [YEAR] field is not supported by the [accessor].
	 * @throws IllegalArgumentException The year is less than zero.
	 * @throws ArithmeticException A numeric overflow occurs.
	 * @see get3DigitYear
	 */
	@Pure
	public fun get3DigitYearOf(accessor: TemporalAccessor): Int = get3DigitYear(accessor.get(YEAR))
	
	/**
	 * Gets the year in a three-digit form, e.g. `"15"` for 2015 or `"925"` for 2925.
	 *
	 * @param year The year.
	 * @return The year in a three-digit form.
	 * @throws IllegalArgumentException [year] is less than zero.
	 * @see get3DigitYearOf
	 */
	@Pure
	public fun get3DigitYear(year: Int): Int {
		
		require("year", year >= 0)
		
		return year % 1000
	}
}
