/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.math

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*
import cc.persch.stardust.require
import kotlin.math.roundToLong

// CONCLUDING //////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the sum of its arguments, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public infix fun Int.plusExact(other: Int): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.addExact(this, other) }

/**
 * Returns the sum of its arguments, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public infix fun Long.plusExact(other: Long): ConcludeLong<ArithmeticException> =
	catchLong<ArithmeticException> { Math.addExact(this, other) }

/**
 * Returns the difference of its arguments, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public infix fun Int.minusExact(other: Int): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.subtractExact(this, other) }

/**
 * Returns the difference of its arguments, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public infix fun Long.minusExact(other: Long): ConcludeLong<ArithmeticException> =
	catchLong<ArithmeticException> { Math.subtractExact(this, other) }

/**
 * Returns the product of its arguments, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public infix fun Int.timesExact(other: Int): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.multiplyExact(this, other) }

/**
 * Returns the product of its arguments, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public infix fun Long.timesExact(other: Long): ConcludeLong<ArithmeticException> =
	catchLong<ArithmeticException> { Math.multiplyExact(this, other) }

/**
 * Returns the negation of the argument, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Int.negateExact(): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.negateExact(this) }

/**
 * Returns the negation of the argument, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Long.negateExact(): ConcludeLong<ArithmeticException> =
	catchLong<ArithmeticException> { Math.negateExact(this) }

/**
 * Returns the increment of the argument, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Int.incExact(): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.incrementExact(this) }

/**
 * Returns the increment of the argument, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Long.incExact(): ConcludeLong<ArithmeticException> =
	catchLong<ArithmeticException> { Math.incrementExact(this) }

/**
 * Returns the decrement of the argument, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Int.decExact(): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.decrementExact(this) }

/**
 * Returns the decrement of the argument, returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Long.decExact(): ConcludeLong<ArithmeticException> =
	catchLong<ArithmeticException> { Math.decrementExact(this) }


/**
 * Returns the value of the argument as [Int], returning a failure if the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Long.toIntExact(): ConcludeInt<ArithmeticException> =
	catchInt<ArithmeticException> { Math.toIntExact(this) }

/**
 * Returns the value of the argument as [Long], returning a failure if this value is infinite, NaN,
 * or the result overflows.
 *
 * @since 4.0
 */
@Pure
public fun Double.toLongExact(): ConcludeLong<ArithmeticException> = catchLong<ArithmeticException> {
	
	if(isInfinite() || isNaN() || this !in Long.MIN_VALUE.toDouble()..Long.MAX_VALUE.toDouble())
		throw ArithmeticException("long integer overflow")
	
	toLong()
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the sum of its arguments, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public infix fun Int.plusExact(other: Int): Int = Math.addExact(this, other)

/**
 * Returns the sum of its arguments, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public infix fun Long.plusExact(other: Long): Long = Math.addExact(this, other)

/**
 * Returns the difference of its arguments, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public infix fun Int.minusExact(other: Int): Int = Math.subtractExact(this, other)

/**
 * Returns the difference of its arguments, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public infix fun Long.minusExact(other: Long): Long = Math.subtractExact(this, other)

/**
 * Returns the product of its arguments, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public infix fun Int.timesExact(other: Int): Int = Math.multiplyExact(this, other)

/**
 * Returns the product of its arguments, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public infix fun Long.timesExact(other: Long): Long = Math.multiplyExact(this, other)

/**
 * Returns the negation of the argument, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Int.negateExact(): Int = Math.negateExact(this)

/**
 * Returns the negation of the argument, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Long.negateExact(): Long = Math.negateExact(this)

/**
 * Returns the increment of the argument, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Int.incExact(): Int = Math.incrementExact(this)

/**
 * Returns the increment of the argument, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Long.incExact(): Long = Math.incrementExact(this)

/**
 * Returns the decrement of the argument, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Int.decExact(): Int = Math.decrementExact(this)

/**
 * Returns the decrement of the argument, returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Long.decExact(): Long = Math.decrementExact(this)


/**
 * Returns the value of the argument as [Int], returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Long.toIntExact(): Int = Math.toIntExact(this)

/**
 * Returns the value of the argument as [Long], returning a failure if the result overflows.
 *
 * @throws ArithmeticException The result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Double.toLongExact(): Long {
	
	require("this", this in Long.MIN_VALUE.toDouble()..Long.MAX_VALUE.toDouble()) { "long integer overflow" }
	
	return toLong()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Even/Odd
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Checks whether a number is even.
 *
 * @return `true` if this value is even, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Byte.isEven(): Boolean = this % 2 == 0

/**
 * Checks whether a number is even.
 *
 * @return `true` if this value is even, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Short.isEven(): Boolean = this % 2 == 0

/**
 * Checks whether a number is even.
 *
 * @return `true` if this value is even, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Int.isEven(): Boolean = this % 2 == 0

/**
 * Checks whether a number is even.
 *
 * @return `true` if this value is even, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Long.isEven(): Boolean = this % 2 == 0L

/**
 * Checks whether a number is even.
 *
 * @return `true` if this value is even, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Float.isEven(): Boolean = this % 2 == 0f

/**
 * Checks whether a number is even.
 *
 * @return `true` if this value is even, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Double.isEven(): Boolean = this % 2 == 0.0

/**
 * Checks whether a number is odd.
 *
 * @return `true` if this value is odd, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Byte.isOdd(): Boolean = this % 2 != 0

/**
 * Checks whether a number is odd.
 *
 * @return `true` if this value is odd, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Short.isOdd(): Boolean = this % 2 != 0

/**
 * Checks whether a number is odd.
 *
 * @return `true` if this value is odd, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Int.isOdd(): Boolean = this % 2 != 0

/**
 * Checks whether a number is odd.
 *
 * @return `true` if this value is odd, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Long.isOdd(): Boolean = this % 2 != 0L

/**
 * Checks whether a number is odd.
 *
 * @return `true` if this value is odd, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Float.isOdd(): Boolean = this % 2 != 0f

/**
 * Checks whether a number is odd.
 *
 * @return `true` if this value is odd, otherwise `false`.
 * @since 4.0
 */
@Pure
public fun Double.isOdd(): Boolean = this % 2 != 0.0

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Rounding
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Rounds a [Double] value to a [Long] in the "half up" mode.
 *
 * Examples:
 *
 * |Input|Result|
 * |----:|-----:|
 * |  1.1|     1|
 * |  1.5|     2|
 * |  1.6|     2|
 * |  2.5|     3|
 *
 * @return [FailureLong] if the value is `NaN` or infinite; or the result overflows.
 * @since 4.0
 */
@Pure
public fun Double.roundHalfUpExact(): ConcludeLong<ArithmeticException> =
	(this + if(this < 0.0) -0.5 else 0.5).toLongExact()

/**
 * Rounds a [Double] value to a [Long] in the "half up" mode.
 *
 * Examples:
 *
 * |Input|Result|
 * |----:|-----:|
 * |  1.1|     1|
 * |  1.5|     2|
 * |  1.6|     2|
 * |  2.5|     3|
 *
 * @throws IllegalArgumentException This value is `NaN` or infinite; or the result overflows.
 * @since 6.0
 */
context(EnclosingContext<ArithmeticException>)
@Pure
public fun Double.roundHalfUpExact(): Long = (this + if(this < 0.0) -0.5 else 0.5).toLongExact()
