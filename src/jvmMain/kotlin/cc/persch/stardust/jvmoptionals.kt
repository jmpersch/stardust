/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardust.conclude.Failure
import cc.persch.stardust.conclude.asSuccess
import java.util.*
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract
import kotlin.jvm.optionals.getOrDefault
import kotlin.jvm.optionals.getOrElse
import kotlin.jvm.optionals.getOrNull


/**
 * Handle this [Optional] using error propagation. The given [errorHandler] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): Optional<Bar> {
 *
 *     val foo = getsSomethingOptional() or { return it }
 *     //    ↖ Foo         ↖ Optional<Foo>            ↖ Nil
 *
 *     return doSomethingWith(foo).asSure()
 *     //                  ↖ Bar       ↖ Sure<Bar>
 * }
 * ```
 *
 * @since 5.0
 * @see orNull
 * @see orDefault
 * @see orDefault
 * @see orThrow
 */
@Pure
public inline infix fun <T: Any> Optional<out T>.or(
	errorHandler: (Optional<Nothing>) -> Nothing
) : T {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(isPresent)
		return get()
	
	@Suppress("UNCHECKED_CAST")
	errorHandler(this as Optional<Nothing>)
}

/**
 * Returns the value, if it is present; otherwise, `null` is returned.
 *
 * @since 5.0
 * @see or
 * @see orDefault
 * @see orThrow
 */
@Pure
public fun <T: Any> Optional<out T>.orNull(): T? = getOrNull()

/**
 * Returns the value, if it is present; otherwise, [defaultValue] is returned.
 *
 * @since 5.0
 * @see or
 * @see orNull
 * @see orThrow
 */
@Pure
public infix fun <T: Any> Optional<out T>.orDefault(defaultValue: T): T = getOrDefault(defaultValue)

/**
 * Returns the value, if it is present; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 5.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orThrow
 */
@Pure
public inline infix fun <T: Any> Optional<out T>.orElse(
	elseSupplier: () -> T
) : T {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return getOrElse(elseSupplier)
}

/**
 * Returns the value, if it is present; otherwise, an [IllegalAbsentValueException] is thrown.
 *
 * @since 5.0
 * @throws IllegalAbsentValueException If this is [Optional].
 * @see or
 * @see orNull
 * @see orDefault
 */
@Pure
public fun <T: Any> Optional<out T>.orThrow(): T = orThrow { "Panicked on absent value." }

/**
 * Returns the value, if it is present; otherwise, an [IllegalAbsentValueException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 5.0
 * @throws IllegalAbsentValueException If this is [Optional].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orDefault
 */
@Pure
public inline infix fun <T: Any> Optional<out T>.orThrow(
	lazyMessage: () -> String
) : T {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return getOrElse { throw IllegalAbsentValueException(lazyMessage()) }
}

// To Conclude /////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts this [Optional] into a [Conclude] using a custom error supplier.
 *
 * @since 5.0
 */
@Pure
public inline fun <T: Any, Error> Optional<out T>.toConclude(
	errorSupplier: () -> Error
) : Conclude<T, Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return orElse(null)?.asSuccess() ?: Failure(errorSupplier())
}
