/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import java.nio.file.Path

// PATH ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides a special overload of the sequence operator for [Path].
 *
 * Calling the generic sequence operator on a [Path] instance would yield an unexpected result,
 * because [Path] is an iterable of itself.
 *
 * @since 5.1
 */
@Pure
public operator fun Iterable<Path>.rem(other: Iterable<Path>): Sequence<Path> =
	asPathSequence() + other.asPathSequence()

/**
 * Provides a special overload of the sequence operator for [Path].
 *
 * Calling the generic sequence operator on a [Path] instance would yield an unexpected result,
 * because [Path] is an iterable of itself.
 *
 * @since 5.1
 */
@Pure
public operator fun Sequence<Path>.rem(other: Iterable<Path>): Sequence<Path> =
	this + other.asPathSequence()

/**
 * Provides a special overload of the sequence operator for [Path].
 *
 * Calling the generic sequence operator on a [Path] instance would yield an unexpected result,
 * because [Path] is an iterable of itself.
 *
 * @since 5.1
 */
@Pure
public operator fun Iterable<Path>.rem(other: Sequence<Path>): Sequence<Path> =
	asPathSequence() + other

@Pure
private fun Iterable<Path>.asPathSequence() = if(this is Path) sequenceOf(this) else asSequence()
