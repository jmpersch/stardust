/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Unchecked
import java.io.File
import java.nio.charset.Charset
import java.time.ZoneId

/**
 * Defines environment variables.
 *
 * ***Info:** The values are not cached, because they can change during runtime.*
 *
 * *Generated on: OpenJDK Runtime Environment 17.0.7+7 (64-bit)*
 *
 * @since 1.0
 */
@Suppress("unused")
public actual object Environment {
	
	/**
	 * System property: `file.encoding`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val file_encoding: Charset
		get() {
			
			return Charset.forName(System.getProperty("file.encoding"))
		}
	
	/**
	 * System property: `file.separator`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val file_separator: String
		get() {
			
			return System.getProperty("file.separator")
		}
	
	/**
	 * System property: `java.home`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val java_home: File
		get() {
			
			return File(System.getProperty("java.home"))
		}
	
	/**
	 * System property: `java.runtime.name`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val java_runtime_name: String
		get() {
			
			return System.getProperty("java.runtime.name")
		}
	
	/**
	 * System property: `java.runtime.version`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val java_runtime_version: String
		get() {
			
			return System.getProperty("java.runtime.version")
		}
	
	/**
	 * System property: `java.version`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val java_version: String
		get() {
			
			return System.getProperty("java.version")
		}
	
	/**
	 * System property: `java.vm.version`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val java_vm_version: String
		get() {
			
			return System.getProperty("java.vm.version")
		}
	
	/**
	 * System property: `line.separator`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public actual val line_separator: String
		get() {
			
			return System.getProperty("line.separator")
		}
	
	/**
	 * System property: `os.name`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val os_name: String
		get() {
			
			return System.getProperty("os.name")
		}
	
	/**
	 * System property: `os.version`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val os_version: String
		get() {
			
			return System.getProperty("os.version")
		}
	
	/**
	 * System property: `path.separator`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val path_separator: String
		get() {
			
			return System.getProperty("path.separator")
		}
	
	/**
	 * Processor architecture
	 *
	 * @property bits The bit number (*32* or *64*) of the CPU architecture.
	 *
	 * @see sun_arch_data_model
	 */
	public enum class Architecture(@param:Unchecked public val bits: Int) {
		
		/**
		 * 32-bit
		 */
		X32(32),
		
		/**
		 * 64-bit
		 */
		X64(64)
	}
	
	/**
	 * System property: `sun.arch.data.model`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val sun_arch_data_model: Architecture?
		get() {
			
			return when(System.getProperty("sun.arch.data.model")) {
				
				"32" -> Architecture.X32
				"64" -> Architecture.X64
				else -> null
			}
		}
	
	/**
	 * System property: `user.dir`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val user_dir: File
		get() {
			
			return File(System.getProperty("user.dir"))
		}
	
	/**
	 * System property: `user.home`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val user_home: File
		get() {
			
			return File(System.getProperty("user.home"))
		}
	
	/**
	 * System property: `user.language`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val user_language: String
		get() {
			
			return System.getProperty("user.language")
		}
	
	/**
	 * System property: `user.name`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val user_name: String
		get() {
			
			return System.getProperty("user.name")
		}
	
	/**
	 * System property: `user.timezone`
	 *
	 * @throws SecurityException If a security manager exists and its
	 *   `checkPropertyAccess` method doesn't allow
	 *   access to the specified system property.
	 */
	public val user_timezone: ZoneId
		get() {
			
			return ZoneId.of(System.getProperty("user.timezone"))
		}
}
