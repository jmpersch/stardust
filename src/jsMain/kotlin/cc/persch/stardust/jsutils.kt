/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import kotlin.contracts.InvocationKind
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract
import kotlin.reflect.KClass

@PublishedApi
internal actual inline fun <R> ___synchronized(lock: Any, block: () -> R): R {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return block()
}

internal actual fun ___printErrorLn(message: String?) = println(message)

/**
 * Gets the qualified name of this [class][KClass] or `"<local class or anonymous object>"` if undefined.
 *
 * @since 5.0
 */
@Pure
public actual fun KClass<*>.getQualifiedName(): String =
	toString() pipe { if (it.startsWith("class ")) it.substring(6) else it }

/**
 * Gets the simple name of this [class][KClass] or `"<local class or anonymous object>"` if undefined.
 *
 * @since 5.0
 */
@Pure
public actual fun KClass<*>.getSimpleName(): String = toString() pipe {
	
	if (it.startsWith("class ")) with(it.lastIndexOf('.', 6)) { it.substring(if (this >= 0) this else 6) }
	else it
}
