/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*

public actual typealias PatternSyntaxException = Exception

/**
 * Parses the given [pattern] as a [Regex].
 *
 * Returns a [Failure] containing a [PatternSyntaxException],
 * if the given [pattern] is invalid.
 *
 * @since 5.0
 */
@Pure
public actual fun parseRegex(pattern: String): Conclude<Regex, Exception> =
	try { Regex(pattern).asSuccess() }
	catch(e: Exception) { failureOf(e) }

/**
 * Parses the given [pattern] as a [Regex] using the given [option].
 *
 * Returns a [Failure] containing a [PatternSyntaxException],
 * if the given [pattern] is invalid.
 *
 * @since 5.0
 */
@Pure
public actual fun parseRegex(pattern: String, option: RegexOption): Conclude<Regex, Exception> =
	try { Regex(pattern, option).asSuccess() }
	catch(e: Exception) { failureOf(e) }

/**
 * Parses the given [pattern] as a [Regex] using the given [options].
 *
 * Returns a [Failure] containing a [PatternSyntaxException],
 * if the given [pattern] is invalid.
 *
 * @since 5.0
 */
@Pure
public actual fun parseRegex(pattern: String, vararg options: RegexOption): Conclude<Regex, Exception> =
	parseRegex(pattern, options.toSet())

/**
 * Parses the given [pattern] as a [Regex] using the given [options].
 *
 * Returns a [Failure] containing a [PatternSyntaxException],
 * if the given [pattern] is invalid.
 *
 * @since 5.0
 */
@Pure
public actual fun parseRegex(pattern: String, options: Set<RegexOption>): Conclude<Regex, Exception> =
	try { Regex(pattern, options).asSuccess() }
	catch(e: Exception) { failureOf(e) }
