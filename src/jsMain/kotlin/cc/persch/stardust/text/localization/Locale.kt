/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.localization

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.rope.rope
import kotlinx.serialization.Serializable

/**
 * Defines a locale by a language, country, and variant.
 *
 * @since 4.0
 */
@Serializable
public actual class Locale private constructor(
	public val language: String,
	public val country: String,
	public val variant: String
) {
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(other == null || this::class.js != other::class.js) return false
		
		other as Locale
		
		if(language != other.language) return false
		if(country != other.country) return false
		if(variant != other.variant) return false
		
		return true
	}
	
	override fun hashCode(): Int {
		
		var result = language.hashCode()
		result = 31 * result + country.hashCode()
		result = 31 * result + variant.hashCode()
		return result
	}
	
	override fun toString(): String = rope { 
		
		+language
		
		if(country.isNotEmpty())
			+"-$country"
			
		if(variant.isNotEmpty())
			+"-$variant"
	}
	
	public companion object {
		
		@Pure
		public fun of(language: String, country: String, variant: String): Locale =
			Locale(language, country, variant)
		
		@Pure
		public fun of(language: String, country: String): Locale = Locale(language, country, "")
		
		@Pure
		public fun of(language: String): Locale = Locale(language, "", "")
	}
}

@Pure
internal actual fun __localeOf(language: String, country: String, variant: String): Locale =
	Locale.of(language, country, variant)

@Pure
internal actual fun __localeOf(language: String, country: String): Locale = __localeOf(language, country, "")

@Pure
internal actual fun __localeOf(language: String): Locale = __localeOf(language, "", "")

internal actual val Locale.__language: String
	get() = language

internal actual val Locale.__country: String
	get() = country

internal actual val Locale.__variant: String
	get() = variant
