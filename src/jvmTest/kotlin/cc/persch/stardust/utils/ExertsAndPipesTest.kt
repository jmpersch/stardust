/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.utils

import cc.persch.stardust.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.*

/**
 * Defines tests for exerts and pipes.
 *
 * @since 4.0
 */
class ExertsAndPipesTest
{
	private var delegateWasCalled = false
	
	private fun assertDelegateWasCalled() = assertTrue(this.delegateWasCalled, "The delegate was not called!")
	private fun assertDelegateWasNotCalled() = assertFalse(this.delegateWasCalled, "The delegate was called!")
	
	private val delegate = { this.delegateWasCalled = true }
	
	private val strPredicate: (String) -> Boolean = { it == "true" }
	
	private val strExertDelegate: (String) -> Unit = { this.delegateWasCalled = true }
	
	private val strPipeDelegate: (String) -> String = { this.delegateWasCalled = true; "true" }
	
	@BeforeEach
	fun resetDelegateWasCalled() { this.delegateWasCalled = false }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// exertIf
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun exertIfBooleanTest1()
	{
		assertNotNull(exertIf({ true }) { delegate() })
		assertDelegateWasCalled()
	}
	
	@Test
	fun exertIfBooleanTest2()
	{
		assertNull(exertIf({ false }) { delegate() })
		assertDelegateWasNotCalled()
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// pipeIf/pipeIfNot
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun pipeIfTest1()
	{
		assertEquals("true", "true".pipeIf(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasCalled()
	}
	
	@Test
	fun pipeIfTest2()
	{
		assertEquals(null, "false".pipeIf(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun pipeIfNotTest1()
	{
		assertEquals(null, "true".pipeIfNot(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun pipeIfNotTest2()
	{
		assertEquals("true", "false".pipeIfNot(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasCalled()
	}
	
	@Test
	fun pipingIfTest1()
	{
		assertEquals("true", "true".pipingIf(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasCalled()
	}
	
	@Test
	fun pipingIfTest2()
	{
		assertEquals(null, "false".pipingIf(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun pipingIfNotTest1()
	{
		assertEquals(null, "true".pipingIfNot(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun pipingIfNotTest2()
	{
		assertEquals("true", "false".pipingIfNot(this.strPredicate, this.strPipeDelegate))
		assertDelegateWasCalled()
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// exertIf/exertIfNot
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun exertIfTest1()
	{
		assertEquals("true", "true".exertIf(this.strPredicate, this.strExertDelegate))
		assertDelegateWasCalled()
	}
	
	@Test
	fun exertIfTest2()
	{
		assertEquals(null, "false".exertIf(this.strPredicate, this.strExertDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun exertIfNotTest1()
	{
		assertEquals(null, "true".exertIfNot(this.strPredicate, this.strExertDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun exertIfNotTest2()
	{
		assertEquals("false", "false".exertIfNot(this.strPredicate, this.strExertDelegate))
		assertDelegateWasCalled()
	}
	
	@Test
	fun exertingIfTest1()
	{
		assertEquals("true", "true".exertingIf(this.strPredicate, this.strExertDelegate))
		assertDelegateWasCalled()
	}
	
	@Test
	fun exertingIfTest2()
	{
		assertEquals(null, "false".exertingIf(this.strPredicate, this.strExertDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun exertingIfNotTest1()
	{
		assertEquals(null, "true".exertingIfNot(this.strPredicate, this.strExertDelegate))
		assertDelegateWasNotCalled()
	}
	
	@Test
	fun exertingIfNotTest2()
	{
		assertEquals("false", "false".exertingIfNot(this.strPredicate, this.strExertDelegate))
		assertDelegateWasCalled()
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// pipeIf
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun pipeIfBooleanTest1()
	{
		assertNotNull(pipeIf({ true }) { delegate() })
		assertDelegateWasCalled()
	}
	
	@Test
	fun pipeIfBooleanTest2()
	{
		assertNull(pipeIf({ false }) { delegate() })
		assertDelegateWasNotCalled()
	}
}
