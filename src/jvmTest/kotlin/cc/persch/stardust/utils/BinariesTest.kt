/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.utils

import cc.persch.stardust.containsAllBitsOf
import cc.persch.stardust.containsAnyBitOf
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/**
 * Defines the [BinariesTest] class.
 *
 * @see 4.0
 */
class BinariesTest
{
	@Test
	fun containsAllBitsOfTest1()
	{
		assertTrue(3.containsAllBitsOf(2))
	}
	
	@Test
	fun containsAllBitsOfTest2()
	{
		assertFalse(4.containsAllBitsOf(2))
	}
	
	@Test
	fun containsAnyBitOfTest1()
	{
		assertTrue(2.containsAnyBitOf(10))
	}
	
	@Test
	fun containsAnyBitOfTest2()
	{
		assertFalse(4.containsAnyBitOf(10))
	}
}
