/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.utils

import cc.persch.stardust.*
import org.junit.jupiter.api.Test
import kotlin.test.*

/**
 * Defines utility tests.
 *
 * @since 4.0
 */
class UtilsTest
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// To String
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test
	fun toStringTest1()
	{
		assertEquals("Foo", "Foo".toStringStrict())
	}
	
	@Test
	fun toStringTest2()
	{
		assertEquals("", null.toStringOrEmpty())
	}
	
	@Test
	fun toStringTest3()
	{
		assertEquals("Foo", "Foo".toStringOrEmpty())
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Compare Nullables
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test
	fun compareNullablesTest1()
	{
		assertEquals(0, compareNullables(null, null))
	}
	
	@Test
	fun compareNullablesTest2()
	{
		assertEquals(-1, compareNullables(null, 1))
	}
	
	@Test
	fun compareNullablesTest3()
	{
		assertEquals(1.compareTo(1), compareNullables(1, 1))
	}
	
	@Test
	fun compareNullablesTest4()
	{
		assertEquals(1, compareNullables(1, null))
	}
	
	@Test
	fun compareNullablesTest5()
	{
		assertEquals(1.compareTo(2), compareNullables(1, 2))
	}
	
	@Test
	fun compareNullablesTest6()
	{
		assertEquals(2.compareTo(1), compareNullables(2, 1))
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Try-or-Null
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun tryOrNullTest1()
	{
		assertEquals("Foo", `try`<_, Exception> { "Foo" })
	}
	
	@Test
	fun tryOrNullTest2()
	{
		@Suppress("UNREACHABLE_CODE")
		assertEquals(null, `try`<Any?, Exception> { error("This must not fail.") })
	}
	
	@Test
	fun tryOrNullTest3()
	{
		assertEquals("Foo", `try`(IllegalStateException::class) { "Foo" })
	}
	
	@Test
	fun tryOrNullTest4()
	{
		@Suppress("UNREACHABLE_CODE")
		assertEquals(null, `try`<Any?>(IllegalStateException::class) { error("This must not fail") })
	}
	
	@Test
	fun tryOrNullTest5()
	{
		assertFailsWith(IllegalArgumentException::class) { `try`<Any?>(IllegalStateException::class) { unfulfilled() } }
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Not
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test
	fun notTest1()
	{
		assertTrue(not { false }())
	}
	
	@Test
	fun notTest2()
	{
		assertFalse(not { true }())
	}
	
	@Test
	fun notTest3()
	{
		assertTrue(not<Boolean> { it }(false))
	}
	
	@Test
	fun notTest4()
	{
		assertFalse(not<Boolean> { it }(true))
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Try-Cast
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test
	fun tryCastToTest1()
	{
		assertNull("String".tryCastTo(Int::class))
	}

	@Test
	fun tryCastToTest2()
	{
		assertEquals("String", "String".tryCastTo(CharSequence::class))
	}
	@Test
	fun tryCastToTest3()
	{
		assertNull("String".tryCastTo<Int>())
	}

	@Test
	fun tryCastToTest4()
	{
		assertEquals("String", "String".tryCastTo<CharSequence>())
	}
}
