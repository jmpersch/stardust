/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.utils

import java.nio.ByteBuffer
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

class ByteArrayBufferTest {
	
	@Test
	fun testBytes() {
		
		val nbytes = ByteArray(2) { 255.toByte() }
		val fbytes = ByteArray(2) { 255.toByte() }
		
//		println("nbytes =" + nbytes.asList())
//		println("fbytes = " + fbytes.asList())
		
		val nbb = ByteBuffer.wrap(nbytes)
		val fbb = ByteArrayBuffer(fbytes)
		
		val src = byteArrayOf(255.toByte(), 0x22, 0x33, 255.toByte())
		
//		println("src = " + src.asList())
		
		nbb.put(src, 1, 2)
		fbb.putBytes(src, 1, 2)
		
//		println("nbytes =" + nbytes.asList())
//		println("fbytes = " + fbytes.asList())
		
		assertContentEquals(nbytes, fbytes)
		
		nbb.position(0)
		fbb.position = 0
		
		val ndst = ByteArray(4) { 255.toByte() }
		val fdst = ByteArray(4) { 255.toByte() }
		
		nbb.get(ndst, 1, 2)
		fbb.getBytes(fdst, 1, 2)
		
//		println("ndst = " + ndst.asList())
//		println("fdst = " + fdst.asList())
		
		assertContentEquals(ndst, fdst)
	}
	
	@Test
	fun testByte() {
		
		val nbytes = ByteArray(1) { 255.toByte() }
		val fbytes = ByteArray(1) { 255.toByte() }
		
		val nbb = ByteBuffer.wrap(nbytes)
		val fbb = ByteArrayBuffer(fbytes)
		
		nbb.put(0x11)
		fbb.putByte(0x11)
		
//		println(nbytes.asList())
//		println(fbytes.asList())
		
		assertContentEquals(nbytes, fbytes)
		
		nbb.position(0)
		fbb.position = 0
		
		val nval = nbb.get()
		val fval = fbb.getByte()
		
//		println(nval)
//		println(fval)
		
		assertEquals(nval, fval)
	}
	
	@Test
	fun testShort() {
		
		val nbytes = ByteArray(2) { 255.toByte() }
		val fbytes = ByteArray(2) { 255.toByte() }
		
		val nbb = ByteBuffer.wrap(nbytes)
		val fbb = ByteArrayBuffer(fbytes)
		
		nbb.putShort(0x11_22)
		fbb.putShort(0x11_22)
		
//		println(nbytes.asList())
//		println(fbytes.asList())
		
		assertContentEquals(nbytes, fbytes)
		
		nbb.position(0)
		fbb.position = 0
		
		val nval = nbb.getShort()
		val fval = fbb.getShort()
		
//		println(nval)
//		println(fval)
		
		assertEquals(nval, fval)
	}
	
	@Test
	fun testInt() {
		
		val nbytes = ByteArray(4) { 255.toByte() }
		val fbytes = ByteArray(4) { 255.toByte() }
		
		val nbb = ByteBuffer.wrap(nbytes)
		val fbb = ByteArrayBuffer(fbytes)
		
		nbb.putInt(0x11_22_33_44)
		fbb.putInt(0x11_22_33_44)
		
//		println(nbytes.asList())
//		println(fbytes.asList())
		
		assertContentEquals(nbytes, fbytes)
		
		nbb.position(0)
		fbb.position = 0
		
		val nval = nbb.getInt()
		val fval = fbb.getInt()
		
//		println(nval)
//		println(fval)
		
		assertEquals(nval, fval)
	}
	
	@Test
	fun testLong() {
		
		val nbytes = ByteArray(8) { 255.toByte() }
		val fbytes = ByteArray(8) { 255.toByte() }
		
		val nbb = ByteBuffer.wrap(nbytes)
		val fbb = ByteArrayBuffer(fbytes)
		
		nbb.putLong(0x11_22_33_44_55_66_77_88L)
		fbb.putLong(0x11_22_33_44_55_66_77_88L)
		
//		println(nbytes.asList())
//		println(fbytes.asList())
		
		assertContentEquals(nbytes, fbytes)
		
		nbb.position(0)
		fbb.position = 0
		
		val nval = nbb.getLong()
		val fval = fbb.getLong()
		
//		println(nval)
//		println(fval)
		
		assertEquals(nval, fval)
	}
}
