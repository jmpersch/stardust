/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.assertIsFailure
import cc.persch.stardust.assertSuccessEquals
import cc.persch.stardust.parsing.EnumParsingOption.IGNORE_CASE
import cc.persch.stardust.parsing.EnumParsingOption.CASE_SENSITIVE
import cc.persch.stardust.parsing.EnumParsingTest.Foo.BAR
import org.junit.jupiter.api.Test

class EnumParsingTest {
	
	enum class Foo { BAR }
	
	@Test
	fun `parse enum value case-sensitive success`() {
		
		assertSuccessEquals(BAR, "BAR".parseEnumValue<Foo>(CASE_SENSITIVE))
	}
	
	@Test
	fun `parse enum value case-sensitive failure`() {
		
		assertIsFailure("bar".parseEnumValue<Foo>(CASE_SENSITIVE))
	}
	
	@Test
	fun `parse enum value case-insensitive`() {
		
		assertSuccessEquals(BAR, "bar".parseEnumValue<Foo>(IGNORE_CASE))
	}
	
	@Test
	fun `parse enum value case-insensitive failure`() {
		
		assertIsFailure("Barr".parseEnumValue<Foo>(IGNORE_CASE))
	}
}
