/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.assertIsFailure
import cc.persch.stardust.assertIsFailureOf
import cc.persch.stardust.assertSuccessEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import java.math.MathContext.DECIMAL32
import java.math.RoundingMode.UNNECESSARY

class NumberParsingTest {
	
	// BYTE ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse byte successful`() {
		
		assertSuccessEquals(Byte.MAX_VALUE, "${Byte.MAX_VALUE}".parseByte().box())
	}
	
	@Test
	fun `parse byte failure`() {
		
		assertIsFailure("${Byte.MAX_VALUE + 1}".parseByte().box())
	}
	
	// SHORT ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse short successful`() {
		
		assertSuccessEquals(Short.MAX_VALUE, "${Short.MAX_VALUE}".parseShort().box())
	}
	
	@Test
	fun `parse short failure`() {
		
		assertIsFailure("${Short.MAX_VALUE + 1}".parseShort().box())
	}
	
	// INT /////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse int successful`() {
		
		assertSuccessEquals(Int.MAX_VALUE, "${Int.MAX_VALUE}".parseInt().box())
	}
	
	@Test
	fun `parse int failure`() {
		
		assertIsFailure("${Int.MAX_VALUE + 1L}".parseInt().box())
	}
	
	// LONG ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse long successful`() {
		
		assertSuccessEquals(Long.MAX_VALUE, "${Long.MAX_VALUE}".parseLong().box())
	}
	
	@Test
	fun `parse long failure`() {
		
		assertIsFailure("${BigDecimal(Long.MAX_VALUE).inc()}".parseLong().box())
	}
	
	// UBYTE ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse ubyte successful`() {
		
		assertSuccessEquals(UByte.MAX_VALUE, "${UByte.MAX_VALUE}".parseUByte().box())
	}
	
	@Test
	fun `parse ubyte failure`() {
		
		assertIsFailure("${UByte.MAX_VALUE + 1U}".parseUByte().box())
	}
	
	// USHORT //////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse ushort successful`() {
		
		assertSuccessEquals(UShort.MAX_VALUE, "${UShort.MAX_VALUE}".parseUShort().box())
	}
	
	@Test
	fun `parse ushort failure`() {
		
		assertIsFailure("${UShort.MAX_VALUE + 1U}".parseUShort().box())
	}
	
	// UINT ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse uint successful`() {
		
		assertSuccessEquals(UInt.MAX_VALUE, "${UInt.MAX_VALUE}".parseUInt().box())
	}
	
	@Test
	fun `parse uint failure`() {
		
		assertIsFailure("${UInt.MAX_VALUE + 1UL}".parseUInt().box())
	}
	
	// ULONG ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse ulong successful`() {
		
		assertSuccessEquals(ULong.MAX_VALUE, "${ULong.MAX_VALUE}".parseULong().box())
	}
	
	@Test
	fun `parse ulong failure`() {
		
		assertIsFailure("?!".parseULong().box())
	}
	
	// FLOAT ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse float successful`() {
		
		assertSuccessEquals(123.456f, "123.456".parseFloat().box())
	}
	
	@Test
	fun `parse float failure`() {
		
		assertIsFailure("?!".parseFloat().box())
	}
	
	// DOUBLE //////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse double successful`() {
		
		assertSuccessEquals(123.456, "123.456".parseDouble().box())
	}
	
	@Test
	fun `parse double failure`() {
		
		assertIsFailure("?!".parseDouble().box())
	}
	
	// BIG INTEGER /////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse BigInteger successful`() {
		
		assertSuccessEquals(BigInteger.valueOf(12345), "12345".parseBigInteger())
	}
	
	@Test
	fun `parse BigInteger failure`() {
		
		assertIsFailure("?!".parseBigInteger())
	}
	
	// BIG DECIMAL /////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun `parse BigDecimal successful 1`() {
		
		assertSuccessEquals(BigDecimal(12345), "12345".parseBigDecimal())
	}
	
	@Test
	fun `parse BigDecimal successful 2`() {
		
		assertSuccessEquals(BigDecimal(12345), "12345".parseBigDecimal(DECIMAL32))
	}
	
	@Test
	fun `parse BigDecimal failure 1`() {
		
		assertIsFailure("?!".parseBigDecimal())
	}
	
	@Test
	fun `parse BigDecimal failure 2`() {
		
		assertIsFailureOf<BigDecimalParsingFault.IllegalNumberFormat>("?!".parseBigDecimal(DECIMAL32))
	}
	
	@Test
	fun `parse BigDecimal failure 3`() {
		
		assertIsFailureOf<BigDecimalParsingFault.IllegalRoundingCondition>(
			"123.456".parseBigDecimal(MathContext(2, UNNECESSARY))
		)
	}
}
