/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.assertIsFailure
import cc.persch.stardust.assertSuccessEquals
import cc.persch.stardust.parsing.BooleanParsingOption.*
import cc.persch.stardustx.test.assertIsFailureOf
import org.junit.jupiter.api.Test

class BooleanParsingTest {
	
	@Test
	fun testParseBooleanCaseSensitive1() {
		
		assertSuccessEquals(true, "true".parseBoolean(CASE_SENSITIVE).box())
	}
	
	@Test
	fun testParseBooleanCaseSensitive2() {
		
		assertSuccessEquals(false, "false".parseBoolean(CASE_SENSITIVE).box())
	}
	
	@Test
	fun testParseBooleanCaseSensitiveFails() {
		
		assertIsFailure("True".parseBoolean(CASE_SENSITIVE).box())
	}
	
	@Test
	fun testParseBooleanCaseInsensitive1() {
		
		assertSuccessEquals(true, "true".parseBoolean(IGNORE_CASE).box())
		assertSuccessEquals(true, "True".parseBoolean(IGNORE_CASE).box())
	}
	
	@Test
	fun testParseBooleanCaseInsensitive2() {
		
		assertSuccessEquals(false, "false".parseBoolean(IGNORE_CASE).box())
		assertSuccessEquals(false, "False".parseBoolean(IGNORE_CASE).box())
	}
	
	@Test
	fun testParseBooleanCaseInsensitiveFails() {
		
		assertIsFailure("?!".parseBoolean(IGNORE_CASE).box())
	}
	
	@Test
	fun testParseBooleanLenient1() {
		
		assertSuccessEquals(true, "true".parseBoolean(LENIENT).box())
		assertSuccessEquals(true, "True".parseBoolean(LENIENT).box())
		assertSuccessEquals(true, "1".parseBoolean(LENIENT).box())
	}
	
	@Test
	fun testParseBooleanLenient2() {
		
		assertSuccessEquals(false, "false".parseBoolean(LENIENT).box())
		assertSuccessEquals(false, "False".parseBoolean(LENIENT).box())
		assertSuccessEquals(false, "0".parseBoolean(LENIENT).box())
	}
	
	@Test
	fun testParseBooleanLenientFails() {
		
		assertIsFailure("?!".parseBoolean(LENIENT).box())
	}
}
