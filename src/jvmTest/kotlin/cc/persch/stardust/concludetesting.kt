/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardust.conclude.Failure
import cc.persch.stardust.conclude.Success
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

/**
 * Asserts that this [Conclude] is a [Success].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Result> Conclude<Result, *>.expectSuccess(message: String? = null): Result = assertIsSuccess(this, message)

/**
 * Asserts that the given [result] is a [Success].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Result> assertIsSuccess(result: Conclude<Result, *>, message: String? = null): Result {
	
	if(result is Failure<*>)
		throw AssertionError("Expected Success${if(message == null) "" else ": $message"}\nResult was: $result")
	
	return (result as Success<Result>).value
}

/**
 * Asserts that the given [actualConclude] is equal to [expectedConclude].
 *
 * Use this function, if the given concludes are of different error types;
 * otherwise, it’s recommended to use [assertEquals].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 * 
 * @see assertConcludeNotEquals
 */
fun <Result> assertConcludeEquals(
	expectedConclude: Conclude<Result, *>,
	actualConclude: Conclude<Result, *>,
	message: String? = null
) : Unit = assertEquals(expectedConclude, actualConclude, message)

/**
 * Asserts that the given [actualConclude] is *not* equal to [expectedConclude].
 *
 * Use this function, if the given concludes are of different error types;
 * otherwise, it’s recommended to use [assertNotEquals].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 *
 * @see assertConcludeEquals
 */
fun <Result> assertConcludeNotEquals(
	expectedConclude: Conclude<Result, *>,
	actualConclude: Conclude<Result, *>,
	message: String? = null
) : Unit = assertNotEquals(expectedConclude, actualConclude, message)

/**
 * Asserts that the given [actualConclude] is a [Success].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Result> assertSuccessEquals(
	expectedValue: Result,
	actualConclude: Conclude<Result, *>,
	message: String? = null
) : Result {
	
	val actualValue = assertIsSuccess(actualConclude, message)
	
	assertEquals(expectedValue, actualValue, message)
	
	return actualValue
}

/**
 * Asserts that the given [actualConclude] is a [Success].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Result> assertSuccessNotEquals(
	expectedValue: Result,
	actualConclude: Conclude<Result, *>,
	message: String? = null
) : Result {
	
	val actualValue = assertIsSuccess(actualConclude, message)
	
	assertNotEquals(expectedValue, actualValue, message)
	
	return actualValue
}

/**
 * Asserts that this [Conclude] is a [Failure].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
@JvmName("__assertIsFailure")
fun <Error> Conclude<*, Error>.assertIsFailure(message: String? = null): Error = assertIsFailure(this, message)

/**
 * Asserts that the given [result] is a [Failure].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Error> assertIsFailure(result: Conclude<*, Error>, message: String? = null): Error {
	
	if(result is Success<*>)
		throw AssertionError("Expected Failure${if(message == null) "" else ": $message"}\nResult was: $result")
	
	return (result as Failure<Error>).reason
}

/**
 * Asserts that the given [result] is a [Failure] containing a reason of the [Error] class.
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
inline fun <reified Error: Any> assertIsFailureOf(
	result: Conclude<*, *>,
	message: String? = null
) : Error = assertIsFailureOf(Error::class, result, message)

/**
 * Asserts that the given [result] is a [Failure] containing a reason of the given [reasonClass].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Error: Any> assertIsFailureOf(
	reasonClass: KClass<Error>,
	result: Conclude<*, *>,
	message: String? = null
) : Error {
	
	if(result is Success<*>) {
		
		throw AssertionError(
			"${message.prefix()}Expected Failure of class ${reasonClass.getQualifiedName()} but was Success."
		)
	}
	
	val reason = (result as Failure<*>).reason
	
	@Suppress("UNCHECKED_CAST")
	if(reasonClass.isInstance(reason))
		return reason as Error
	
	throw AssertionError(
		"${message.prefix()}Expected Failure of class ${reasonClass.getQualifiedName()} " +
		"but was ${reason?.pipe{it::class.getQualifiedName()} ?: "null"}"
	)
}

/**
 * Asserts that the given [actualConclude] is a [Failure].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Error> assertFailureEquals(
	expectedError: Error,
	actualConclude: Conclude<*, Error>,
	message: String? = null
) : Error {
	
	val actualError = assertIsFailure(actualConclude, message)
	
	assertEquals(expectedError, actualError, message)
	
	return actualError
}

/**
 * Asserts that the given [actualConclude] is a [Failure].
 *
 * If the assertion fails, the given [message] is used in the failure message.
 */
fun <Error> assertFailureNotEquals(
	expectedError: Error,
	actualConclude: Conclude<*, Error>,
	message: String? = null
) : Error {
	
	val actualError = assertIsFailure(actualConclude, message)
	
	assertNotEquals(expectedError, actualError, message)
	
	return actualError
}

@Pure
private fun String?.prefix() = this?.piping { "$this\n" } ?: ""
