/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.parsing.parseDouble
import cc.persch.stardust.perform
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertIs

class ConcludeDoubleTest {
	
	@Test
	fun `orThrow on success`() {
		
		assertEquals(123.456, successDoubleOf(123.456).orThrow())
	}
	
	@Test
	fun `orThrow on failure`() {
		
		assertFailsWith(IllegalConclusionException::class) { failureDoubleOf(123).orThrow() }
	}
	
	@Test
	fun `failure propagation on success`() {
		
		assertEquals(123.456, successDoubleOf(123.456).propagateFailure())
	}
	
	@Test
	fun `failure propagation on failure`() {
		
		assertEquals(-123.456, failureDoubleOf("Error").propagateFailure())
	}
	
	@Test
	fun `catch success`() {
		
		catchDouble<Exception> { "123.456".toDouble() } perform {
			
			assertIs<SuccessDouble>(it, "`SuccessDouble` expected")
			
			assertEquals(123.456, it.value)
		}
	}
	
	@Test
	fun `catch failure`() {
		
		catchDouble<NumberFormatException> { "?!".parseDouble() or { throw NumberFormatException() } } perform {
			
			assertIs<FailureDouble<*>>(it, "`FailureDouble` expected")
		}
	}
	
	@Test
	fun `catch rethrow`() {
		
		assertFailsWith<NumberFormatException> {
			
			`catch`<_, OtherException> { "?!".parseDouble() or { throw NumberFormatException() } }
		}
	}
	
	private companion object {
		
		@Pure
		private fun ConcludeDouble<String>.propagateFailure(): Double = orDefault(-123.456)
		
		private class OtherException private constructor(): RuntimeException()
	}
}
