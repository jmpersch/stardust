/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.perform
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertIs

class ConcludeTest {
	
	@Test
	fun `orThrow on success`() {
		
		assertEquals("123", successOf("123").orThrow())
	}
	
	@Test
	fun `orThrow on failure`() {
		
		assertFailsWith(IllegalConclusionException::class) { failureOf(123).orThrow() }
	}
	
	@Test
	fun `failure propagation on success`() {
		
		assertEquals("Success", successOf("Success").propagateFailure())
	}
	
	@Test
	fun `failure propagation on failure`() {
		
		assertEquals("Failure", failureOf("Error").propagateFailure())
	}
	
	@Test
	fun `catch success`() {
		
		`catch`<_, Exception> { "123".toInt() } perform {
			
			assertIs<Success<*>>(it, "`Success` expected")
			
			assertEquals(123, it.value)
		}
	}
	
	@Test
	fun `catch failure`() {
		
		`catch`<_, NumberFormatException> { "?!".toInt() } perform {
			
			assertIs<Failure<*>>(it, "`Failure` expected")
		}
	}
	
	@Test
	fun `catch rethrow`() {
		
		assertFailsWith<NumberFormatException> { `catch`<_, OtherException> { "?!".toInt() } }
	}
	
	private companion object {
		
		@Pure
		private fun Conclude<String, String>.propagateFailure(): String = orDefault("Failure")
		
		private class OtherException private constructor(): RuntimeException()
	}
}
