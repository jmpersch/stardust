/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import org.junit.jupiter.api.Test
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.fail

/**
 * Defines tests for exerts and pipes.
 *
 * @since 4.0
 */
class PreconditionsTest
{
	val customMessage = "Custom message in Unit test."
	
	fun assertSucceeds(block: () -> Unit)
	{
		try
		{
			block()
		}
		catch(e: IllegalArgumentException)
		{
			fail("Unexpected exception thrown: ${e::class} with message \"${e.message}\"")
		}
	}
	
	fun assertFailsWithMessage(expectedMessage: String, block: () -> Unit) =
			assertFailsWithMessage(IllegalArgumentException::class, "$expectedMessage\nParameter: foo", block)
	
	inline fun <reified E: Exception> assertFailsWithMessage(
		exceptionClass: KClass<E>,
		expectedMessage: String,
		block: () -> Unit
	) {
		try
		{
			block()
			
			fail("`require` did not throw an exception.")
		}
		catch(e: Exception)
		{
			assertNotNull(e as? E, "Wrong exception class thrown.\nExpected: $exceptionClass\nCurrent: ${e::class}")
			
			assertEquals(expectedMessage, e.message)
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Require
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun requireSucceedsTest1()
	{
		assertSucceeds { require("foo", true) }
	}
	
	@Test
	fun requireSucceedsTest2()
	{
		assertSucceeds { require("foo", true) { customMessage } }
	}
	
	@Test
	fun requireFailsTest1()
	{
		assertFailsWithMessage(DEFAULT_INVALID_ARGUMENT_EXCEPTION_MESSAGE) { require("foo", false) }
	}
	
	@Test
	fun requireFailsTest2()
	{
		assertFailsWithMessage(customMessage) { require("foo", false) { customMessage } }
	}
	
	@Test
	fun requireNotNullSucceeds1()
	{
		assertSucceeds { requireNotNull("foo", "Foo") }
	}
	
	@Test
	fun requireNotNullSucceeds2()
	{
		assertSucceeds { requireNotNull("foo", "Foo") { customMessage } }
	}
	
	@Suppress("CAST_NEVER_SUCCEEDS")
	@Test
	fun requireNotNullFails1()
	{
		assertFailsWithMessage("The specified argument is null.") { requireNotNull("foo", null as? String) }
	}
	
	@Suppress("CAST_NEVER_SUCCEEDS")
	@Test
	fun requireNotNullFails2()
	{
		assertFailsWithMessage(customMessage) {
			requireNotNull("foo", null as? String) { customMessage }
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Unfulfilled
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun unfulfilledTest1()
	{
		assertFailsWith(IllegalArgumentException::class) { unfulfilled() }
	}
	
	@Test
	fun unfulfilledTest2()
	{
		assertFailsWith(IllegalArgumentException::class) { unfulfilled(RuntimeException()) }
	}
	
	@Test
	fun unfulfilledTest3()
	{
		assertFailsWithMessage(DEFAULT_INVALID_ARGUMENT_EXCEPTION_MESSAGE) { unfulfilled("foo") }
	}
	
	@Test
	fun unfulfilledTest4()
	{
		assertFailsWithMessage(DEFAULT_INVALID_ARGUMENT_EXCEPTION_MESSAGE) { unfulfilled("foo", RuntimeException()) }
	}
	
	@Test
	fun unfulfilledTest5()
	{
		assertFailsWithMessage(customMessage) { unfulfilled("foo", customMessage) }
	}
	
	@Test
	fun unfulfilledTest6()
	{
		assertFailsWithMessage(customMessage) { unfulfilled("foo", customMessage, RuntimeException()) }
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Kill
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun killTest1()
	{
		assertFailsWith(IllegalStateException::class) { throw IllegalStateException() }
	}
	
	@Test
	fun killTest2()
	{
		assertFailsWith(IllegalStateException::class) { throw IllegalStateException() }
	}
	
	@Test
	fun killTest3()
	{
		assertFailsWithMessage(IllegalStateException::class, customMessage) { throw IllegalStateException(customMessage) }
	}
	
	@Test
	fun killTest4()
	{
		assertFailsWithMessage(IllegalStateException::class, customMessage) {
			
			throw IllegalStateException(customMessage, RuntimeException())
		}
	}
}
