/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.result

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract

/**
 * Handle this [Result] using error propagation. The given [errorHandle] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): Result<Bar> {
 *
 *     val foo = getSomeResult() or { return it }
 *     //    ↖ Foo         ↖ Result<Foo>      ↖ Result<Nothing>
 *
 *     return Result.success(doSomethingWith(foo))
 *     //                ↖ Result<Bar>   ↖ Bar
 * }
 * ```
 *
 * @since 5.0
 * @see orElse
 * @see orDefault
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <T> Result<T>.or(
	errorHandle: (Result<Nothing>) -> Nothing
) : T {
	
	contract { callsInPlace(errorHandle, AT_MOST_ONCE) }
	
	if(isFailure) {
		
		@Suppress("UNCHECKED_CAST")
		errorHandle(this as Result<Nothing>)
	}
	
	return getOrThrow()
}

/**
 * Returns the value, if this represents success; otherwise, `null` is returned.
 *
 * @since 5.0
 * @see or
 * @see orDefault
 * @see orThrow
 */
@Pure
public fun <T> Result<T>.orNull(): T? = getOrNull()

/**
 * Returns the value, if this represents success; otherwise, [defaultValue] is returned.
 *
 * @since 5.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orThrow
 */
@Pure
public infix fun <T> Result<T>.orDefault(defaultValue: T): T = getOrDefault(defaultValue)

/**
 * Returns the value, if this represents success; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 5.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orThrow
 */
@Pure
public inline infix fun <T> Result<T>.orElse(
	elseSupplier: (exception: Throwable) -> T
) : T {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return getOrElse(elseSupplier)
}

/**
 * Returns the value, if this represents success; otherwise, the encapsulated exception is thrown.
 *
 * @since 5.0
 * @throws IllegalResultException If this represents a failure.
 * @see or
 * @see orNull
 * @see orDefault
 */
@Pure
public fun <T> Result<T>.orThrow(): T = getOrElse {
	
	throw IllegalResultException(it.message ?: "Panicked on failure", it)
}

/**
 * Returns the value, if this represents success; otherwise, an [IllegalResultException] with the message of
 * [lazyMessage] and the encapsulated exception as cause is thrown.
 *
 * @since 5.0
 * @throws IllegalResultException If this represents a failure.
 * @see or
 * @see orNull
 * @see orDefault
 * @see orDefault
 */
@Pure
public inline infix fun <T> Result<T>.orThrow(
	lazyMessage: () -> String
) : T {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return getOrElse { throw IllegalResultException(lazyMessage(), it) }
}

// To Conclude /////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts this [kotlin.Result] into a [Conclude].
 *
 * @since 5.0
 * @see Conclude.toResult
 */
@Pure
public fun <T> Result<T>.toConclude(): Conclude<T, Throwable> = fold(::successOf, ::failureOf)
