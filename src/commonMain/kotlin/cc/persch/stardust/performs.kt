/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("unused")

package cc.persch.stardust

import cc.persch.stardust.annotations.ShouldUse
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Invokes the specified [action] with this value as its receiver.
 *
 * @since 4.3
 * @see perform
 * @see exert
 * @see exerting
 * @see piping
 * @see pipe
 */
@ShouldUse
public inline infix fun <T> T.performing(action: T.() -> Unit) {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	action(this)
}

/**
 *  Invokes the [action] with this value as its argument.
 *
 * @since 4.3
 * @see performing
 * @see exert
 * @see exerting
 * @see pipe
 * @see piping
 */
public inline infix fun <T> T.perform(action: (T) -> Unit) {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	action(this)
}

/**
 * Invokes the [action] if the [predicate] is `true` and returns `true`, if the action was performed.
 *
 * @since 4.3
 */
@ShouldUse
public inline fun <T, R> T.performIf(predicate: (T) -> Boolean, action: (T) -> R): Boolean {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	if(!predicate(this))
		return false
	
	action(this)
	
	return true
}

/**
 * Invokes the [action] if the [predicate] is `false` and returns `true`, if the action was performed.
 *
 * @since 4.3
 */
@ShouldUse
public inline fun <T, R> T.performIfNot(predicate: (T) -> Boolean, action: (T) -> R): Boolean {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	if(predicate(this))
		return false
	
	action(this)
	
	return true
}

/**
 * Invokes the [action] as receiver if the [predicate] is `true` and returns `true`, if the action was performed.
 *
 * @since 4.3
 */
@ShouldUse
public inline fun <T, R> T.performingIf(predicate: T.() -> Boolean, action: T.() -> R): Boolean {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	if(!predicate(this))
		return false
	
	action(this)
	
	return true
}

/**
 * Invokes the [action] as receiver if the [predicate] is `false` and returns `true`, if the action was performed.
 *
 * @since 4.3
 */
@ShouldUse
public inline fun <T, R> T.performingIfNot(predicate: T.() -> Boolean, action: T.() -> R): Boolean {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	if(predicate(this))
		return false
	
	action(this)
	
	return true
}
