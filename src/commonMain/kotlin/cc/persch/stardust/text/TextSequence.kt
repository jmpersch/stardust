/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure

/**
 * Defines a real character sequence ideal to use for short-living sub-sequences of character sequences.
 *
 * Many classes that implement the interface [CharSequence] return a new [String] on the call
 * of the method [CharSequence.subSequence(…)][CharSequence.subSequence], like [String.subSequence] does.
 * The result is an unnecessary usage of memory due to a copy of the internal character array.
 * This nullifies the expected advantage of a sub-sequence in comparison to a substring.
 *
 * This class provides a *real* sub-sequence for [CharSequence] objects. The call of
 * [subSequence(…)][TextSequence.subSequence] creates a new instance of this class
 * and does *not* create a copy of the wrapped sequence, thus no unnecessary memory will be allocated.
 *
 * @since 3.0
 * @see CharSequence.textSequence
 */
public interface TextSequence: CharSequence, Comparable<CharSequence>, Iterable<Char> {
	
	@Pure
	override operator fun compareTo(other: CharSequence): Int = compareTo(other, false)
	
	/**
	 * Compares two [CharSequences][CharSequence].
	 *
	 * @param o The [CharSequence] to compare with.
	 * @param ignoreCase `true` to ignore the character case on comparison.
	 * @return The comparison result.
	 */
	@Pure
	public fun compareTo(o: CharSequence, ignoreCase: Boolean = false): Int = compareCharSequences(this, o, ignoreCase)
	
	/**
	 * Creates a real sub-sequence of the wrapped [CharSequence] without allocating additional memory.
	 *
	 * @param range The range of the sub-sequence.
	 * @return The sub-sequence of the specified range.
	 * @throws IllegalArgumentException [range] is invalid.
	 */
	@Pure
	public fun textSequence(range: IntRange): TextSequence = textSequence(range.first, range.last)
	
	/**
	 * Creates a real sub-sequence of the wrapped [CharSequence] without allocating additional memory.
	 *
	 * @param from The start index.
	 * @return The sub-sequence beginning at the specified index.
	 * @throws IllegalArgumentException [from] is out of range.
	 */
	@Pure
	public fun textSequence(from: Int): TextSequence = textSequence(from, this.length)
	
	/**
	 * Creates a real sub-sequence of the wrapped [CharSequence] without allocating additional memory.
	 *
	 * @param from The start index.
	 * @param to The end index (excluding).
	 * @return A real sub-sequence of the specified range.
	 * @throws IllegalArgumentException [from] or [to] is out of range.
	 */
	@Pure
	public fun textSequence(from: Int, to: Int): TextSequence
	
	/**
	 * Creates a real sub-sequence of the wrapped [CharSequence] without allocating additional memory.
	 *
	 * *Info: Better use [textSequence].*
	 *
	 * @param startIndex The start index.
	 * @param endIndex The end index (excluding).
	 * @return A real sub-sequence of the specified range.
	 * @throws IllegalArgumentException [startIndex] or [endIndex] is out of range.
	 * @see textSequence
	 */
	@Pure
	public override fun subSequence(startIndex: Int, endIndex: Int): TextSequence =
		textSequence(startIndex, endIndex)
}
