/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.QuoteEscapingOption.ESCAPE_DOUBLE_QUOTES
import cc.persch.stardust.text.QuoteEscapingOption.ESCAPE_SINGLE_QUOTES

/**
 * Defines quoting options.
 *
 * @since 5.0
 * @see CharSequence.quote
 * @see Char.quote
 */
public enum class QuotingOption {
	
	/**
	 * Use single quotes: `'`
	 */
	USE_SINGLE_QUOTES,
	
	/**
	 * Use double quotes: `"`
	 */
	USE_DOUBLE_QUOTES;
	
	/**
	 * Converts this value into a [QuoteEscapingOption].
	 */
	@Pure
	public fun toQuoteEscapingOption(): QuoteEscapingOption = when(this) {
		
		USE_SINGLE_QUOTES -> ESCAPE_SINGLE_QUOTES
		
		USE_DOUBLE_QUOTES -> ESCAPE_DOUBLE_QUOTES
	}
}
