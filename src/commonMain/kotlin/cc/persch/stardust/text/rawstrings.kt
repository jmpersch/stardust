/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("PackageDirectoryMismatch")

package cc.persch.stardust.text.rawstrings

/**
 * Triple quote (`"""`) to use in a raw string.
 *
 * Example: `"""Foo${TRIPPLE_QUOTE}Bar"""` instead of `"""Foo${"\"\"\""}Bar"""`
 */
public const val TRIPPLE_QUOTE: String = "\"\"\""

/**
 * Dollar character (`$`) to use in a raw string.
 *
 * Example: `"""Foo${DOLLAR}Bar"""` instead of `"""Foo${'$'}Bar"""`
 */
public const val DOLLAR: Char = '$'

/**
 * Tab character (`\t`) to use in a raw string.
 *
 * Example: `"""Foo${TAB}Bar"""` instead of `"""Foo${'\t'}Bar"""`
 */
public const val TAB: Char = '\t'

/**
 * New-line character (`\n`) to use in a raw string.
 *
 * Example: `"""Foo${NEWLINE}Bar"""` instead of `"""Foo${'\n'}Bar"""`
 */
public const val NEWLINE: Char = '\n'
