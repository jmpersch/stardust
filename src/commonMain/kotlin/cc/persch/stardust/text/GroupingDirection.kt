/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.text.rope.RopeBuilder

/**
 * Defines grouping directions.
 *
 * Example:
 *
 * ```text
 * Input: "1234567890"
 * Group site: 4
 *
 * Leftwards: "12 3456 7890"
 *             <----------|
 *
 * Rightwards: "1234 5678 90"
 *              |---------->
 * ```
 *
 * @since 2.0
 * @see CharSequence.group
 * @see StringBuilder.appendGrouped
 * @see RopeBuilder.group
 */
public enum class GroupingDirection {
	
	/**
	 * Leftwards
	 *
	 * Example:
	 * 
	 * ```text
	 * "12 3456 7890"
	 *  <----------|
	 *  ```
	 */
	LEFTWARDS,
	
	/**
	 * Rightwards
	 * 
	 * Example:
	 * 
	 * ```text
	 * "1234 5678 90"
	 *  |---------->
	 * ```
	 */
	RIGHTWARDS
}
