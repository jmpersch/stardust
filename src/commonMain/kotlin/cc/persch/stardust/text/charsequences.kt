/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import kotlin.contracts.contract

/**
 * Gets the subsequence beginning at the given [startIndex] up to the [length][CharSequence.length]
 * of this character sequence.
 *
 * This is a convenience function for `charSeq.subSequence(startIndex, charSeq.length)`.
 *
 * @since 6.0
 */
@Pure
public fun CharSequence.subSequence(startIndex: Int): CharSequence = subSequence(startIndex, length)

/**
 * Normalizes the line breaks of this character sequence. That means that `\r\n` and `\r` are replaced by `\n`.
 *
 *
 * Example:
 * ```
 * "Foo\r\r\rBar\r\n\r\n\r\nFoo\n\n\nBar" → "Foo\n\n\nBar\n\n\nFoo\n\n\nBar"
 * ```
 *
 * @since 1.0
 */
@Pure
public fun CharSequence.normalizeLineBreaks(): String {
	
	val length = this.length
	
	val sb = StringBuilder(length)
	
	var i = 0
	
	while(i < length) {
		
		val c = this.get(i)
		
		if(c == '\r') {
			
			if(i < length - 1) {
				
				val n = this.get(i + 1)
				
				if(n == '\n') {
					
					sb.append('\n')
					
					i += 2
					
					continue
				}
			}
			
			sb.append('\n')
		}
		else
			sb.append(c)
		
		i++
	}
	
	return sb.toString()
}

/**
 * Computes a hash code from the content of this [CharSequence].
 *
 * @since 1.0
 * @see String.contentEquals
 * @see compareTo
 */
@Pure
public fun CharSequence.contentHashCode(): Int {
	
	var hash = 0
	
	for(c in this)
		hash = hash * 31 + c.code
	
	return hash
}

/**
 * Compares this character sequence to another one by comparing the code points of both sequences.
 *
 * *Info: If your class already implements a method with this name, you can't call this extension method. In this case,
 * you can use the [compareCharSequences] function.*
 *
 * @param other Another [CharSequence].
 * @param ignoreCase `true` to ignore the character case on comparison.
 *   The invariant locale is used for character conversion.
 *
 * @since 1.0
 * @see compareCharSequences
 */
@Pure
public fun CharSequence.compareTo(other: CharSequence, ignoreCase: Boolean = false): Int =
	compareCharSequences(this, other, ignoreCase)

/**
 * Compares this [CharSequence] to another one by comparing the code points of both sequences.
 *
 * *Note: Better use the [compareTo] extension method if applicable.*
 *
 * @param other Another [CharSequence].
 * @param ignoreCase `true` to ignore the character case on comparison.
 *   The invariant locale is used for character conversion.
 *
 * @since 1.0
 * @see compareTo
 */
@Pure
public fun compareCharSequences(
	sequence: CharSequence,
	other: CharSequence,
	ignoreCase: Boolean = false
) : Int {
	
	val ownIterator = sequence.iterator()
	val otherIterator = other.iterator()
	
	while(ownIterator.hasNext() && otherIterator.hasNext()) {
		
		val c1 = ownIterator.next()
		val c2 = otherIterator.next()
		
		val cmp = c1.compareTo(c2, ignoreCase)
		
		if(cmp != 0)
			return cmp
	}
	
	return when {
		
		ownIterator.hasNext() -> 1
		
		otherIterator.hasNext() -> -1
		
		else -> 0
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Null, Empty, or Whitespace Checks
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Checks whether a [CharSequence] (e.g. a [String]) is empty, or consists exclusively of whitespaces.
 *
 * @return `true` if the string is `null`, empty, or consists exclusively of whitespaces; otherwise, `false`.
 *
 * @since 1.0
 * @see isEmptyOrBlank
 */
@Pure
public fun CharSequence?.isNullOrEmptyOrBlank(): Boolean {
	
	contract { returns(false) implies (this@isNullOrEmptyOrBlank != null) }
	
	return this == null || isEmptyOrBlank()
}

/**
 * Checks whether a [CharSequence] (e.g. a [String]) is empty, or consists exclusively of whitespaces.
 *
 * @return `true` if the string is empty, or consists exclusively of whitespaces; otherwise, `false`.
 *
 * @since 1.0
 * @see CharSequence.isEmpty
 * @see CharSequence.isBlank
 */
@Pure
public fun CharSequence.isEmptyOrBlank(): Boolean = isEmpty() || isBlank()

/**
 * Checks whether this character sequence contains at least one whitespace.
 *
 * @return `true` if the string contains at least one whitespace; otherwise, `false`.
 *
 * @since 1.0
 */
@Pure
public fun CharSequence.containsWhitespace(): Boolean = any(Char::isWhitespace)
