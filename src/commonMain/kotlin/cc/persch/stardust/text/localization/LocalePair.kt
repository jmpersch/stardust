/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.localization

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.pipe

/**
 * Defines a pair of locales consisting of a [display] locale and a [format] locale.
 * The default value for the [format] locale is [Locales.defaultFormat].
 *
 * @since 4.0
 */
@ThreadSafe
public data class LocalePair(
	val display: Locale,
	val format: Locale = Locales.defaultFormat
) {
	public companion object {
		
		/**
		 * Defines [invariant locales][Locales.invariant] for display and format.
		 *
		 * @see Locales.invariant
		 */
		public val invariant: LocalePair = Locales.invariant pipe { LocalePair(it, it) }
		
		/**
		 * Gets a [LocalePair] with the [default display][Locales.defaultDisplay] and
		 * [default format][Locales.defaultFormat] locales.
		 *
		 * *Info: The value is not cached, because the default locales can be altered during runtime.*
		 *
		 * @see Locales.default
		 * @see Locales.defaultDisplay
		 * @see Locales.defaultFormat
		 */
		@Pure
		public fun default(): LocalePair = LocalePair(Locales.defaultDisplay, Locales.defaultFormat)
	}
}
