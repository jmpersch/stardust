/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.localization

/**
 * This object contains all available [languages][Locale].
 *
 * @since 1.0
 */
public expect object Locales {
	
	/**
	 * Gets the default [locale][Locale].
	 * On set, it also sets the locale for the [DISPLAY][defaultDisplay] and [FORMAT][defaultFormat] categories.
	 */
	public var default: Locale
	
	/**
	 * Gets or sets the default [locale][Locale] for the DISPLAY category.
	 */
	public var defaultDisplay: Locale
	
	/**
	 * Gets or sets the default [locale][Locale] for the FORMAT category.
	 */
	public var defaultFormat: Locale
	
	/**
	 * Gets the invariant [locale][Locale].
	 */
	public val invariant: Locale
}
