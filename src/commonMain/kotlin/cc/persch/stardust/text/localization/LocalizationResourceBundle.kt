/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.localization

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.__synchronized
import cc.persch.stardust.annotations.threading.RaceCritical
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.perform

/**
 * Defines the base class for a localization resource bundle.
 *
 * **Important: *An implementation of this class must be thread-safe!***
 *
 * @since 4.0
 *
 * @constructor Creates a new instance using the specified [resource factories][resourceFactories].
 *   The first parameter is the [factory for the invariant locale][invariantResourceFactory].
 *
 *   *Info: It is recommended to use the secondary constructor using pairs of locales and factories.*
 *
 *   Example:
 *
 *   ```
 *   object YourResourceBundle: LocalizationResourceBundle<YourResource>(
 *       Locales.en    to ::YourResourceEn,
 *       Locales.en_GB to ::YourResourceEnGB,
 *       Locales.de    to ::YourResourceDe
 *   )
 *   ```
 */
@ThreadSafe
public abstract class LocalizationResourceBundle<R: LocalizationResource>(
	private val invariantResourceFactory: LocalizationResourceFactory<R>,
	resourceFactories: Iterable<LocalizationResourceFactory<R>>
) {
	/**
	 * Creates a new instance with the [defined pairs][resourceFactories] of [locale][Locale] and factory delegate that
	 * are converted into [resource factories][LocalizationResourceFactory].
	 * The [first parameter][invariantResourceFactory] is the pair for the invariant locale factory.
	 *
	 * Example:
	 *
	 * ```
	 * object YourResourceBundle: LocalizationResourceBundle<YourResource>(
	 *     Locales.en    to ::YourResourceEn,
	 *     Locales.en_GB to ::YourResourceEnGB,
	 *     Locales.de    to ::YourResourceDe
	 * )
	 * ```
	 */
	public constructor(
		invariantResourceFactory: Pair<Locale, (LocalePair) -> R>,
		vararg resourceFactories: Pair<Locale, (LocalePair) -> R>
	) : this(
		LocalizationResourceFactory(invariantResourceFactory.first, invariantResourceFactory.second),
		resourceFactories.map { (locale, factory) -> LocalizationResourceFactory(locale, factory) }
	)
	
	private val resourceFactories = buildMap {
		invariantResourceFactory perform { put(it.displayLocale, it) }
		resourceFactories.forEach { put(it.displayLocale, it) }
	}
	
	@RaceCritical
	private val resourceCache = mutableMapOf<LocalePair, R>()
	
	/**
	 * Gets the resource of the specified [display locale][displayLocale]. The [format locale][formatLocale]
	 * is used in the resource itself for creating formats.
	 * If no resource is found for the display locale, the invariant resource is returned.
	 */
	@Pure
	@Cached
	public open operator fun get(
		displayLocale: Locale = Locales.defaultDisplay,
		formatLocale: Locale = Locales.defaultFormat
	) : R = get(LocalePair(displayLocale, formatLocale))
	
	/**
	 * Gets the resource of a locale, where the display locale of the given [locale specification][locales] is used.
	 * The format locale is used in the resource itself for creating formats.
	 * If no resource is found for the display locale, the invariant resource is returned.
	 */
	@Pure
	@Cached
	public open operator fun get(
		locales: LocalePair = LocalePair.default()
	) : R = getResource(locales)
	
	@Pure
	@Cached
	private fun getResource(locales: LocalePair): R {
		
		val resource = __synchronized(resourceCache) { resourceCache[locales] }
		
		if(resource != null)
			return resource
		
		val displayLocale = locales.display
		
		val resourceFactory = resourceFactories.get(displayLocale)
		
		if(resourceFactory != null)
			return resourceFactory.createResource(locales)
		
		val country = displayLocale.__country   // US
		
		if(country.isBlank())
			return invariantResourceFactory.createResource(locales)
		
		val language = displayLocale.__language // en
		val variant = displayLocale.__variant   // POSIX
		
		val newDisplayLocale = if(variant.isBlank()) __localeOf(language) else __localeOf(language, country)
		
		return getResource(LocalePair(newDisplayLocale, locales.format))
	}
	
	private fun LocalizationResourceFactory<R>.createResource(locales: LocalePair): R {
		
		val resource = createLocalizationResource(locales.format)
		
		__synchronized(resourceCache) { resourceCache[locales] = resource }
		
		return resource
	}
}
