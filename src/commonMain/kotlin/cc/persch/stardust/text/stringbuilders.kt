/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.*
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Provides a string builder as type-safe builder to create a string using the given [action].
 *
 * @since 4.0
 */
@TypeSafeBuilder
@Pure
public inline fun buildString(action: StringBuilder.() -> Unit): String {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	return StringBuilder().exert(action).toString()
}

/**
 * Provides a string builder as type-safe builder to create a string of a certain [capacity] using the given [action].
 *
 * @since 4.0
 */
@TypeSafeBuilder
@Pure
public inline fun buildString(capacity: Int, action: StringBuilder.() -> Unit): String {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	return StringBuilder(capacity).exert(action).toString()
}

/**
 * Appends the given [value] the given [times].
 *
 * @since 5.1
 */
public fun StringBuilder.appendRepeated(value: Char, times: Int): StringBuilder {
	
	repeat(times) { append(value) }
	
	return this
}

/**
 * Appends the given [value] the given [times].
 *
 * @since 5.1
 */
public fun StringBuilder.appendRepeated(value: CharSequence, times: Int): StringBuilder {
	
	repeat(times) { append(value) }
	
	return this
}
