/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.Unchecked
import cc.persch.stardust.require
import cc.persch.stardust.text.QuoteEscapingOption.*
import cc.persch.stardust.text.QuotingOption.USE_DOUBLE_QUOTES
import cc.persch.stardust.text.QuotingOption.USE_SINGLE_QUOTES

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Quoted and Unquoted Fragments
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a quoted fragment with caret indicator.
 *
 *
 * Example:
 * ```text
 * ...st\ttext\nfor\\\"something\"."
 *              ^
 * ```
 *
 * @param index The index for the caret.
 * @param maxLength The maximum length of the fragment.
 * @return The fragment.
 * @throws IllegalArgumentException [index] is invalid or [maxLength] is less then `2`.
 *
 * @since 1.0
 */
@Pure
public fun CharSequence.toQuotedFragment(index: Int, maxLength: Int): String {
	
	var idx = index
	val length = length
	
	require("index", idx in 0..length)
	require("maxLength", maxLength > 1)
	
	if(length < 1)
		return ""
	
	if(idx == length)
		idx--
	
	val halfOfMaxLength = maxLength / 2
	
	var startIndex = idx - (maxLength - halfOfMaxLength)
	var endIndex = idx + maxLength
	
	if(startIndex < 0 && endIndex < length)
		endIndex -= startIndex
	
	if(endIndex > length && startIndex > 0)
		startIndex += length - endIndex
	
	startIndex = 0.coerceAtLeast(startIndex)
	endIndex = endIndex.coerceAtMost(length)
	
	val buff = StringBuilder((endIndex - startIndex) * 2 + 16)
	
	buff.append(if(startIndex > 3) "..." else "\"")
	
	var leftFragment: CharSequence = textSequence(startIndex, idx)
	val unquotedLength = leftFragment.length
	
	leftFragment = leftFragment.escape()
	val quotedLength = leftFragment.length
	
	buff.append(leftFragment)
		.append(textSequence(idx, endIndex).escape())
	
	buff.append(if(endIndex < length - 3) "..." else "\"")
	
	buff.append('\n')
		.append(' '.repeat(idx + (quotedLength - unquotedLength) + (if(startIndex > 3) 3 else 1) - startIndex))
		.append('^')
	
	return buff.toString()
}

/**
 * Creates an unquoted fragment with caret as indicator.
 *
 * Example:
 * ```text
 * st    text for something.
 *            ^
 * ```
 *
 * @param index The index for the caret.
 * @param maxLength The maximum length of the fragment.
 * @return The fragment.
 * @throws IllegalArgumentException [index] is invalid or [maxLength] is less then `2`.
 *
 * @since 4.0
 */
@Pure
public fun CharSequence.toUnquotedFragment(index: Int, maxLength: Int): String {
	
	require("index", index in indices)
	require("maxLength", maxLength > 1)
	
	val startIndex = findLeftLineBreakForUnquotedFragment(index, this)
	val endIndex = findRightLineBreakForUnquotedFragment(index, this)
	
	val length = endIndex - startIndex
	
	val buff = StringBuilder(length * 2 + 16)
	
	var leftFragment = substring(startIndex, index)
	val unquotedLength = leftFragment.length
	
	leftFragment = leftFragment.replace("\t", "    ")
	val quotedLength = leftFragment.length
	
	buff.append(leftFragment)
		.append(substring(index, endIndex).replace("\t", "    "))
	
	var caretIndex = index + (quotedLength - unquotedLength) - startIndex
	
	if(buff.length > maxLength) {
		
		val halfOfMaxLen = maxLength / 2
		
		if(caretIndex >= halfOfMaxLen) {
			
			val diff = halfOfMaxLen - (buff.length - caretIndex).coerceAtMost(halfOfMaxLen) / 2
			
			buff.deleteRange(0, caretIndex - diff)
			
			caretIndex = diff
		}
		
		if(buff.length > maxLength)
			buff.setLength(maxLength)
	}
	
	return buff
		.append('\n')
		.append(' '.repeat(caretIndex))
		.append('^')
		.toString()
}

@Pure
private fun findLeftLineBreakForUnquotedFragment(@Unchecked i: Int, text: CharSequence): Int {
	
	var x = i - 1
	
	while(x >= 0) {
		
		if(text.get(x).isLineBreak())
			break
		x--
	}
	
	return x + 1
}

@Pure
private fun findRightLineBreakForUnquotedFragment(@Unchecked i: Int, text: CharSequence): Int {
	
	var x = i
	
	while(x < text.length) {
		
		if(text.get(x).isLineBreak())
			break
		x++
	}
	
	return x
}

/**
 * Checks, whether this character is one of `'\n'` or `'\r'`.
 *
 * @since 4.0
 */
@Pure
private fun Char.isLineBreak() = this == '\n' || this == '\r'

/**
 * Checks, whether this character sequence is one of `"\n"` or `"\r"`, `"\r\n"`.
 *
 * @since 4.0
 */
@Pure
private fun CharSequence.isLineBreak() = length == 1 && this[0].isLineBreak() || "\r\n".contentEquals(this)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Escaping of Strings and Chars
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Quotes a character sequence.
 *
 * @since 5.0
 * @see CharSequence.escape
 */
@Pure
public fun CharSequence.quote(
	quotingOption: QuotingOption = USE_DOUBLE_QUOTES,
	maxLength: Int? = null
) : String = buildString(length + 16) {
	
	appendQuoted(this@quote, quotingOption, maxLength)
}

/**
 * Escapes a character sequence.
 *
 * @since 4.0
 * @see CharSequence.quote
 */
@Pure
public fun CharSequence.escape(
	quoteEscapingOption: QuoteEscapingOption = ESCAPE_NONE,
	maxLength: Int? = null
) : String = buildString(length + 16) {
	
	appendEscaped(this@escape, quoteEscapingOption, maxLength)
}

/**
 * Escapes a character sequence.
 *
 * @since 5.1
 * @throws IllegalArgumentException [maxLength] is not `null` and not greater than zero.
 * @see CharSequence.quote
 */
@Pure
public fun StringBuilder.appendQuoted(
	text: CharSequence,
	quotingOption: QuotingOption = USE_DOUBLE_QUOTES,
	maxLength: Int? = null
) {
	val quote = if(quotingOption == USE_DOUBLE_QUOTES) '"' else '\''
	
	append(quote)
	
	val quoteEscapingOption = quotingOption.toQuoteEscapingOption()
	
	appendEscaped(text, quoteEscapingOption, maxLength)
	
	append(quote)
}

/**
 * Escapes a character sequence.
 *
 * @since 5.1
 * @throws IllegalArgumentException [maxLength] is not `null` and not greater than zero.
 * @see CharSequence.quote
 */
@Pure
public fun StringBuilder.appendEscaped(
	text: CharSequence,
	quoteEscapingOption: QuoteEscapingOption = ESCAPE_NONE,
	maxLength: Int? = null
) {
	require("maxLength", maxLength == null || maxLength > 0)
	
	val subSequence =
		if(maxLength == null) text
		else text.textSequence(0, kotlin.math.min(text.length, maxLength))
	
	for(element in subSequence)
		appendEscapedChar(element, quoteEscapingOption)
	
	if(maxLength != null && text.length > maxLength)
		append("...")
}

/**
 * Quotes a character.
 *
 * Note: The unescaping util can be found in the "stardustx-text" library.
 *
 * @since 5.0
 * @see Char.escape
 */
@Pure
public fun Char.quote(quotingOption: QuotingOption = USE_SINGLE_QUOTES): String = buildString {
	
	val quote = if(quotingOption == USE_SINGLE_QUOTES) '\'' else '"'
	
	append(quote)
	
	appendEscapedChar(this@quote, quotingOption.toQuoteEscapingOption())
	
	append(quote)
}

/**
 * Escapes a character.
 *
 * Note: The unescaping util can be found in the "stardustx-text" library.
 *
 * @since 4.0
 * @see Char.quote
 */
@Pure
public fun Char.escape(quoteEscapingOption: QuoteEscapingOption = ESCAPE_NONE): String = buildString {
	
	appendEscapedChar(this@escape, quoteEscapingOption)
}

@Pure
private fun StringBuilder.appendEscapedChar(c: Char, quoteEscapingOption: QuoteEscapingOption) {
	
	when(c) {
		
		' ' -> append(" ")
		
		'\\' -> append("\\\\")
		
		'"' -> append(if(quoteEscapingOption == ESCAPE_DOUBLE_QUOTES) "\\\"" else "\"")
		
		'\'' -> append(if(quoteEscapingOption == ESCAPE_SINGLE_QUOTES) "\\'" else "'")
		
		'\t' -> append("\\t")
		
		'\r' -> append("\\r")
		
		'\n' -> append("\\n")
		
		// Left-to-right/right-to-left marks
		// http://en.wikipedia.org/wiki/Bi-directional_text
		'\u200e', // Left to right mark
		'\u202a', // Left to right embedding
		'\u202d', // Left to right override
		'\u200f', // Right to left mark
		'\u202b', // Right to left embedding
		'\u202e', // Right to left override
		'\u202c', // Pop directional formatting
			
			// Separators
			// http://en.wikipedia.org/wiki/Unicode_control_characters
		'\u2028', // Line separator
		'\u2029', // Paragraph separator
			
			// Language tags
			//			case 0xe0001: // Language tag character
			//			case 0xe0065: // Tag small letter e
			//			case 0xe006e: // Tag small letter n
			//			case 0xe002d: // Tag hyphen-minus
			//			case 0xe0075: // Tag small letter u
			//			case 0xe0073: // Tag small letter s
			
			// Interlinear annotation
		'\ufff9', '\ufffa', '\ufffb' -> appendUnicode(c) // Unicode sequence, e.g. "\u1234"
		
		else -> {
			if(c.__isIsoControl()  // Control sequence, e.g. '\0'
				|| c.isWhitespace()
				|| c in '\ufe00'..'\ufe0f'
			) appendUnicode(c)
			else append(c) // Unicode sequence, e.g. '\u1234'
		}
	}
}

private fun StringBuilder.appendUnicode(c: Char) {
	
	val hex = c.code.toString(16)
	
	append("\\u")
	
	repeat(4 - hex.length) { append('0') }
	
	append(hex)
}
