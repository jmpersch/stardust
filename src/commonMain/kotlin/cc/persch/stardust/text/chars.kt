/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.exert
import cc.persch.stardust.unfulfilled

@Pure
internal expect fun Char.__isIsoControl(): Boolean // = (this in '\u0000'..'\u001F') || (this in '\u007F'..'\u009F')

/**
 * Compares this character to another one.
 *
 * @param other Another character.
 * @param ignoreCase `true` to ignore the character case on comparison.
 *   The invariant local is used for character conversion.
 *
 * @since 1.0
 * @see compareTo
 */
@Pure
public fun Char.compareTo(other: Char, ignoreCase: Boolean): Int =
	if(ignoreCase) lowercaseChar().compareTo(other.lowercaseChar())
	else compareTo(other)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Repeat characters
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Repeats this character [n][count]-times.
 *
 * @param count The count of repeating.
 * @return A string containing this [count]-times.
 * @throws IllegalArgumentException [count] less than zero.
 *
 * @since 1.0
 * @see kotlin.text.repeat
 */
@Pure
public fun Char.repeat(count: Int): String = when {
	
	count == 0 -> ""
	
	count == 1 -> toString()
	
	count < 0 -> unfulfilled("count")
	
	else -> CharArray(count).exert { it.fill(this) }.concatToString()
}
