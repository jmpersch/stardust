/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.GroupingDirection.*

/**
 * Separates a string into groups, e.g., `"123-456-789".`
 *
 * @param direction The grouping direction.
 * @param groupSize The count of characters of the group.
 * @param separator The group-separator.
 *
 * @since 2.0
 */
@Pure
public fun CharSequence.group(direction: GroupingDirection, groupSize: Int, separator: String): String =
	buildString { appendGrouped(this@group, direction, groupSize, separator) }

/**
 * Separates a string into groups, e.g., `"123-456-789".`
 *
 * @param direction The grouping direction.
 * @param groupSizes The count of characters of the groups.
 * @param separator The group-separator.
 * @return The [this@group], separated in groups.
 *
 * @since 2.0
 */
@Pure
public fun CharSequence.group(direction: GroupingDirection, groupSizes: Iterable<Int>, separator: String): String =
	buildString { appendGrouped(this@group, direction, groupSizes, separator) }

/**
 * Separates a string into groups, e.g., `"123-456-789".`
 *
 * @param direction The grouping direction.
 * @param groupSize The count of characters of the group.
 * @param separator The group-separator.
 *
 * @since 5.1
 */
public fun StringBuilder.appendGrouped(
	text: CharSequence,
	direction: GroupingDirection,
	groupSize: Int,
	separator: String
) : StringBuilder = when(direction) {
	
	RIGHTWARDS -> groupRightwards(text, groupSize, separator)
	LEFTWARDS -> groupLeftwards(text, groupSize, separator)
}

/**
 * Separates a string into groups, e.g. `"123-456-789".`
 *
 * @param direction The grouping direction.
 * @param groupSizes The count of characters of the groups.
 * @param separator The group-separator.
 * @return The [this@group], separated in groups.
 *
 * @since 5.1
 */
public fun StringBuilder.appendGrouped(
	text: CharSequence,
	direction: GroupingDirection,
	groupSizes: Iterable<Int>,
	separator: String
) : StringBuilder = when(direction) {
	
	RIGHTWARDS -> groupRightwards(text, groupSizes, separator)
	LEFTWARDS -> groupLeftwards(text, groupSizes, separator)
}

private fun StringBuilder.groupRightwards(value: CharSequence, groupSize: Int, separator: String): StringBuilder {
	
	val length = value.length
	
	if(length == 0)
		return this
	
	if(groupSize < 1) {
		
		append(value)
		
		return this
	}
	
	var index = 0
	
	while(index < length) {
		
		if(index > 0)
			append(separator)
		
		append(value, index, (index + groupSize).coerceAtMost(length))
		
		index += groupSize
	}
	
	return this
}

private fun StringBuilder.groupRightwards(
	value: CharSequence,
	groupSizes: Iterable<Int>,
	separator: String
) : StringBuilder {
	
	val length = value.length
	
	if(length == 0)
		return this
	
	var index = 0
	
	for(groupSize in groupSizes) {
		
		if(index > 0)
			append(separator)
		
		if(groupSize < 1)
			continue
		
		val nextIndex = (index + groupSize).coerceAtMost(length)
		
		append(value, index, nextIndex)
		
		index = nextIndex
		
		if(index >= length)
			break
	}
	
	if(index < length) {
		
		if(index > 0)
			append(separator)
		
		append(value, index, length)
	}
	
	return this
}

private fun StringBuilder.groupLeftwards(value: CharSequence, groupSize: Int, separator: String): StringBuilder {
	
	val length = value.length
	
	if(length == 0)
		return this
	
	if(groupSize < 1) {
		
		append(value)
		
		return this
	}
	
	val first = length % groupSize
	
	if(first > 0)
		append(value, 0, first)
	
	var index = first
	
	while(index < length) {
		
		if(index > 0)
			append(separator)
		
		append(value, index, (index + groupSize).coerceAtMost(length))
		
		index += groupSize
	}
	
	return this
}

private fun StringBuilder.groupLeftwards(value: CharSequence, groupSizes: Iterable<Int>, separator: String): StringBuilder {
	
	val length = value.length
	
	if(length == 0)
		return this
	
	val sequences = mutableListOf<CharSequence>()
	
	var index = length
	
	for(groupSize in groupSizes) {
		
		if(groupSize < 1) {
			
			sequences.add("")
			
			continue
		}
		
		val nextIndex = (index - groupSize).coerceAtLeast(0)
		
		sequences.add(value.textSequence(nextIndex, index))
		
		index = nextIndex
		
		if(index <= 0)
			break
	}
	
	if(index > 0)
		sequences.add(value.textSequence(0, index))
	
	val lastSequenceIndex = sequences.lastIndex
	
	for(sequenceIndex in sequences.indices.reversed()) {
		
		if(sequenceIndex < lastSequenceIndex)
			append(separator)

		append(sequences.get(sequenceIndex))
	}
	
	return this
}
