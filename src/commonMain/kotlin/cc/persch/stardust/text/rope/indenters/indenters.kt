/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope.indenters

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.getSimpleName
import cc.persch.stardust.perform
import cc.persch.stardust.piping
import cc.persch.stardust.text.escape
import cc.persch.stardust.text.quote
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption.NO_BODY
import cc.persch.stardust.text.rope.indenters.BodyOption.WITH_BODY
import cc.persch.stardust.text.rope.indenters.BracketOption.BRACES
import cc.persch.stardust.text.rope.indenters.BracketOption.BRACKETS
import cc.persch.stardust.text.rope.indenters.SpaceOption.NO_SPACE
import cc.persch.stardust.text.rope.indenters.SpaceOption.WITH_SPACE
import cc.persch.stardust.text.rope.rope
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract
import kotlin.reflect.KClass
import kotlin.reflect.KProperty0
import kotlin.reflect.KProperty1

/**
 * Return this [CharSequence]. If it is `null`, the simple name of the given [class] is returned.
 *
 * @since 5.0
 */
@Pure
public infix fun CharSequence?.orSimpleNameOf(`class`: KClass<*>): String =
	if(this != null) "$this" else `class`.getSimpleName()

/**
 * Indents this [Sequence].
 * 
 * @since 5.0
 */
public fun Sequence<*>.ropeIndented(
	builder: RopeBuilder,
	name: CharSequence?,
	bodyOption: BodyOption
) : Unit = asIterable().ropeIndented(builder, name, bodyOption)

/**
 * Indents this [Iterable].
 * 
 * @since 5.0
 */
public fun Iterable<*>.ropeIndented(
	builder: RopeBuilder,
	name: CharSequence?,
	bodyOption: BodyOption
) : Unit = builder.indentIterable(this, name, null, bodyOption)

/**
 * Indents this [Map].
 *
 * @since 5.0
 */
public fun Map<*, *>.ropeIndented(
	builder: RopeBuilder,
	name: CharSequence?,
	bodyOption: BodyOption
) : Unit = builder.indentMap(this, name, bodyOption)

/**
 * Returns an indented string without body. This is a convenience function for `toIndentedString(bodyOption = false)`.
 *
 * @since 5.0
 */
@Pure
public fun RopeIndentable.toIndentedStringWithoutBody(): String = toIndentedString(NO_BODY)

/**
 * Returns an indented string of this [Sequence].
 *
 * @since 5.0
 */
@Pure
public fun Sequence<*>.toIndentedString(prefix: CharSequence? = null): String = asIterable().toIndentedString(prefix)

/**
 * Returns an indented string of this [Iterable].
 *
 * @since 5.0
 */
@Pure
public fun Iterable<*>.toIndentedString(prefix: CharSequence? = null): String =
	rope { indentIterable(this@toIndentedString, prefix) { indentAny(it) } }

/**
 * Returns an indented string of this [Map].
 *
 * @since 5.0
 */
@Pure
public fun Map<*, *>.toIndentedString(prefix: CharSequence? = null): String =
	rope { indentMap(this@toIndentedString, prefix) }

/**
 * Indents a property, e.g., `indentProperty(User::familyName)`.
 *
 * @since 5.0
 */
public fun RopeBuilder.indentProperty(property: KProperty0<*>): Unit =
	indentProperty(property.get(), property.name)

/**
 * Indents a property as a string. This is a convenient function for `indentProperty(property) { it.toString() }`.
 *
 * @since 5.0
 */
public fun RopeBuilder.indentPropertyAsString(property: KProperty0<*>): Unit =
	indentProperty(property) { it.toString() }

/**
 * Indents a property using the [converter].
 *
 * @since 5.0
 */
public inline fun <T> RopeBuilder.indentProperty(property: KProperty0<T>, converter: (T) -> Any?) {
	
	contract { callsInPlace(converter, EXACTLY_ONCE) }
	
	indentProperty(converter(property.get()), property.name)
}

/**
 * Indents a [value] using [property]'s name.
 *
 * @since 5.0
 */
public fun RopeBuilder.indentProperty(
	value: Any?,
	property: KProperty1<*, *>,
	propertySeparator: CharSequence = ": "
) : Unit = indentAny(value, property.name, propertySeparator)

/**
 * Indents a [value] using [name].
 *
 * @since 5.0
 */
public fun RopeBuilder.indentProperty(
	value: Any?,
	name: CharSequence?,
	propertySeparator: CharSequence = ": "
) : Unit = indentAny(value, name, propertySeparator)

/**
 * Indents the [builder] using the [property]'s name.
 *
 * @since 5.0
 */
public fun RopeBuilder.indentProperty(
	property: KProperty1<*, *>,
	propertySeparator: CharSequence = ": ",
	builder: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(builder, EXACTLY_ONCE) }
	
	indentProperty(property.name, propertySeparator, builder)
}

public fun <T, V> RopeBuilder.indentProperty(
	property: KProperty1<T, V>,
	owner: T,
	propertySeparator: CharSequence = ": "
) : Unit = indentProperty(property.get(owner), property.name, propertySeparator)

/**
 * Indents the [builder] using the [name].
 *
 * @since 5.0
 */
public inline fun RopeBuilder.indentProperty(
	name: CharSequence,
	propertySeparator: CharSequence = ": ",
	builder: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(builder, EXACTLY_ONCE) }
	
	-"$name"
	
	if(name.isNotEmpty())
		+"$propertySeparator"
	
	builder()
}

/**
 * Adds a block indented with the [builder], if the [collection] is empty.
 *
 * @since 5.0
 * @see block
 */
public inline fun RopeBuilder.blockIfNonEmpty(
	collection: Iterable<*>,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	builder: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(builder, AT_MOST_ONCE) }
	
	if((collection as? Collection<*>)?.isEmpty() ?: !collection.iterator().hasNext())
		return
	
	block(bodyOption, bracketOption, spaceOption, builder)
}

/**
 * Creates an environment that uses the given [lineBreakCount].
 *
 * @since 5.0
 * @see withSingleLineBreak
 * @see withDoubleLineBreak
 */
public inline fun RopeBuilder.withLineBreakCount(lineBreakCount: Int, builder: RopeBuilder.() -> Unit) {
	
	contract { callsInPlace(builder, AT_MOST_ONCE) }
	
	if(this.lineBreakCount == lineBreakCount) {
		
		builder()
		
		return
	}
	
	val oldLineBreakCount = this.lineBreakCount
	
	this.lineBreakCount = lineBreakCount
	
	try {
		
		builder()
	}
	finally {
		
		this.lineBreakCount = oldLineBreakCount
	}
}

/**
 * Creates an environment that uses a single line break.
 *
 * @since 5.0
 * @see withLineBreakCount
 * @see withDoubleLineBreak
 */
public inline fun RopeBuilder.withSingleLineBreak(action: RopeBuilder.() -> Unit) {
	
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	withLineBreakCount(1, action)
}

/**
 * Creates an environment that uses a double line break.
 *
 * @since 5.0
 * @see withLineBreakCount
 * @see withSingleLineBreak
 */
public inline fun RopeBuilder.withDoubleLineBreak(action: RopeBuilder.() -> Unit) {
	
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	withLineBreakCount(2, action)
}

/**
 * Indents a block.
 *
 * @since 6.0
 */
public inline fun RopeBuilder.block(
	size: Int,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	block(size.toLong(), bodyOption, bracketOption, spaceOption, action)
}

/**
 * Indents a block.
 *
 * @since 6.0
 */
public inline fun RopeBuilder.block(
	size: Long,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	block("", "|$size|", if(size > 0L) bodyOption else NO_BODY, bracketOption, spaceOption, action)
}

/**
 * Indents a block.
 *
 * @since 5.0
 */
public inline fun RopeBuilder.block(
	name: CharSequence,
	size: Int,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	block(name, size.toLong(), bodyOption, bracketOption, spaceOption, action)
}

/**
 * Indents a block.
 *
 * @since 5.0
 */
public inline fun RopeBuilder.block(
	name: CharSequence,
	size: Long,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	block(name, "|$size|", if(size > 0L) bodyOption else NO_BODY, bracketOption, spaceOption, action)
}

/**
 * Indents a block.
 *
 * @since 5.0
 */
public inline fun RopeBuilder.block(
	name: CharSequence,
	nameAppendix: CharSequence,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	if(name.isNotEmpty())
		-name.toString()
	
	if(nameAppendix.isNotEmpty()) {
		
		sp()
		+nameAppendix.toString()
	}
	
	block(bodyOption, bracketOption, spaceOption, action)
}

/**
 * Indents a block.
 *
 * @since 5.0
 */
public inline fun RopeBuilder.block(
	name: CharSequence,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	appendIndent()
	
	if(name.isNotEmpty())
		+name.toString()
	
	block(bodyOption, bracketOption, spaceOption, action)
}

/**
 * Indents a block.
 *
 * @since 5.0
 */
public inline fun RopeBuilder.block(
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	action: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(action, AT_MOST_ONCE) }
	
	if(bodyOption == NO_BODY)
		return
	
	if(bracketOption == BRACES) +" {"
	else +" ["
	
	if(spaceOption == NO_SPACE)
		nl()
	
	indent(depth + 1, lineBreakCount = if(spaceOption == WITH_SPACE) 2 else 1) { action() }
	
	withSingleLineBreak {
		
		if(bracketOption == BRACES) -'}'
		else -']'
	}
}

/**
 * Indents the given [value] as a string. This is a convenient function for `indentAny(value.toString(), property)`.
 *
 * @since 5.0
 */
public fun RopeBuilder.indentAnyAsString(value: Any?, property: KProperty1<*, *>): Unit =
	indentAny(value.toString(), property)

/**
 * Indents the given [value] as a string.
 * This is a convenient function for `indentAny(value.toString(), name, propertySeparator)`.
 *
 * @since 5.0
 */
public fun RopeBuilder.indentAnyAsString(
	value: Any?, 
	name: CharSequence? = null, 
	propertySeparator: CharSequence = ": "
) : Unit = indentAny(value.toString(), name, propertySeparator)

/**
 * Indents the given [value].
 *
 * @since 5.0
 */
public fun RopeBuilder.indentAny(value: Any?, property: KProperty1<*, *>): Unit = indentAny(value, property.name)

/**
 * Indents the given [value].
 *
 * @since 5.0
 */
public fun RopeBuilder.indentAny(value: Any?, name: CharSequence? = null, propertySeparator: CharSequence = ": ") {
	
	when(value) {
		
		is RopeIndentable -> value.ropeIndented(this, name)
		
		is Map<*, *> -> indentMap(value, name)
		
		is Iterable<*> -> indentIterable(value, name)
		
		is Pair<*, *> -> indentPair(value, name)
		
		is Triple<*, *, *> -> indentTriple(value, name)
		
		else -> {
			
			val nameIsNullOrEmpty = name.isNullOrEmpty()
			
			if(!nameIsNullOrEmpty)
				-"$name$propertySeparator"
			
			val lines = value.toString().lines()
			val quote = value is CharSequence
			
			indent(this@indentAny.depth + (if(nameIsNullOrEmpty) 0 else 1)) {
				
				if(lines.size < 2) {
					
					val first = lines.first()
					val str = if(quote) first.quote() else first
					
					if(nameIsNullOrEmpty) -str
					else +str
				}
				else {
					
					if(name == null) {
						if(quote) -"\"\"\""
						else appendIndent()
					}
					
					withSingleLineBreak {
					
						if(quote && name != null)
							-"\"\"\""
						
						lines.get(0) perform {
							
							if(name != null) -it
							else +it
						}
						
						lines.subList(1, lines.size).forEach { -it }
						
						if(quote)
							-"\"\"\""
					}
				}
			}
		}
	}
}

/**
 * Indents a custom object.
 *
 * Example usage:
 *
 * ```
 * indenter.indentCustom(this, name, bodyOption) {
 *
 *     indentProperty(Foo::bar)
 *     indentProperty(Foo::baz)
 * }
 * ```
 *
 * @since 5.0
 */
public inline fun <T: Any> RopeBuilder.indentCustom(
	obj: T,
	name: CharSequence?,
	bodyOption: BodyOption,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	body: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(body, AT_MOST_ONCE) }
	
	block(name orSimpleNameOf obj::class, bodyOption, bracketOption, spaceOption, body)
}

/**
 * Indents a custom object.
 *
 * Example usage:
 *
 * ```
 * indenter.indentCustom(this, name, size, bodyOption) {
 *
 *     indentProperty(Foo::bar)
 *     indentProperty(Foo::baz)
 * }
 * ```
 *
 * @since 5.0
 */
public inline fun <T: Any> RopeBuilder.indentCustom(
	obj: T,
	name: CharSequence?,
	size: Int,
	bodyOption: BodyOption,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	body: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(body, AT_MOST_ONCE) }
	
	block(name orSimpleNameOf obj::class, size, bodyOption, bracketOption, spaceOption, body)
}

/**
 * Indents a custom object.
 *
 * Example usage:
 *
 * ```
 * indenter.indentCustom(this, name, size, bodyOption) {
 *
 *     indentProperty(Foo::bar)
 *     indentProperty(Foo::baz)
 * }
 * ```
 *
 * @since 5.0
 */
public inline fun <T: Any> RopeBuilder.indentCustom(
	obj: T,
	name: CharSequence?,
	size: Long,
	bodyOption: BodyOption,
	bracketOption: BracketOption = BRACES,
	spaceOption: SpaceOption = WITH_SPACE,
	body: RopeBuilder.() -> Unit
) {
	contract { callsInPlace(body, AT_MOST_ONCE) }
	
	block(name orSimpleNameOf obj::class, size, bodyOption, bracketOption, spaceOption, body)
}

/**
 * Indents an [Iterable]. The [itemBuilder] is called for each item in the collection.
 * The default formatter is `{ indentAny(it) }`.
 *
 * @since 5.0
 * @see indentMap
 */
public inline fun <T, I: Iterable<T>> RopeBuilder.indentIterable(
	iterable: I,
	name: CharSequence? = null,
	size: Int? = null,
	bodyOption: BodyOption = WITH_BODY,
	bracketOption: BracketOption = if(iterable is Set<*>) BRACES else BRACKETS,
	spaceOption: SpaceOption = WITH_SPACE,
	itemBuilder: RopeBuilder.(T) -> Unit = { indentAny(it) }
) {
	contract { callsInPlace(itemBuilder, UNKNOWN) }
	
	withDoubleLineBreak {
		
		val finalSize = when {
			
			size != null -> size
			iterable is Collection<*> -> iterable.size
			else -> null
		}
		
		val simpleName = name.orSimpleNameOf(iterable::class)
		
		-simpleName
		
		if(finalSize != null) {
			
			if(simpleName.isNotEmpty() && !simpleName.endsWith(' '))
				+' '
			
			+'|'
			+"$finalSize"
			+'|'
		}
		
		if(bodyOption == NO_BODY)
			return
		
		blockIfNonEmpty(
			iterable,
			bodyOption,
			bracketOption,
			spaceOption
		) {
			iterable.forEach { itemBuilder(it) }
		}
	}
}

/**
 * Indents an [Iterable]. The [entryBuilder] is called for each item in the collection.
 * The default formatter is `{ (k, v) -> indentAny(v, k.toString() piping { if(k is CharSequence) quote() else escape() }, " -> ") }`.
 *
 * @since 5.0
 * @see indentIterable
 */
public inline fun <K, V, M: Map<out K, V>> RopeBuilder.indentMap(
	map: M,
	name: CharSequence? = null,
	bodyOption: BodyOption = WITH_BODY,
	spaceOption: SpaceOption = WITH_SPACE,
	entryBuilder: RopeBuilder.(Map.Entry<K, V>) -> Unit = { (k, v) ->
		
		indentAny(v, k.toString() piping { if(k is CharSequence) quote() else escape() }, " -> ")
	}
) {
	contract { callsInPlace(entryBuilder, UNKNOWN) }
	
	indentIterable(
		map.entries,
		name orSimpleNameOf map::class,
		null,
		bodyOption,
		BRACES,
		spaceOption
	) {		
		entryBuilder(it)
	}
}

/**
 * Indents a [Pair].
 *
 * @since 5.1
 * @see indentTriple
 */
public inline fun <A, B> RopeBuilder.indentPair(
	pair: Pair<A, B>,
	name: CharSequence? = null,
	bodyOption: BodyOption = WITH_BODY,
	spaceOption: SpaceOption = WITH_SPACE,
	pairBuilder: RopeBuilder.(Pair<A, B>) -> Unit = {
		
		indentProperty(Pair<*, *>::first, it)
		indentProperty(Pair<*, *>::second, it)
	}
) {

	contract { callsInPlace(pairBuilder, AT_MOST_ONCE) }
	
	indentCustom(pair, name, bodyOption, spaceOption = spaceOption) { pairBuilder(pair) }
}

/**
 * Indents a [Triple].
 *
 * @since 5.1
 * @see indentPair
 */
public inline fun <A, B, C> RopeBuilder.indentTriple(
	triple: Triple<A, B, C>,
	name: CharSequence? = null,
	bodyOption: BodyOption = WITH_BODY,
	spaceOption: SpaceOption = WITH_SPACE,
	tripleBuilder: RopeBuilder.(Triple<A, B, C>) -> Unit = {
		
		indentProperty(Triple<*, *, *>::first, it)
		indentProperty(Triple<*, *, *>::second, it)
		indentProperty(Triple<*, *, *>::third, it)
	}
) {
	contract { callsInPlace(tripleBuilder, AT_MOST_ONCE) }
	
	indentCustom(triple, name, bodyOption, spaceOption = spaceOption) { tripleBuilder(triple) }
}
