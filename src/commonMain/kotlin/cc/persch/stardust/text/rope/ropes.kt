/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope

import cc.persch.stardust.*
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.text.rope.RopeBuilder.Companion.DEFAULT_DEPTH
import cc.persch.stardust.text.rope.RopeBuilder.Companion.DEFAULT_INDENT
import cc.persch.stardust.text.rope.RopeBuilder.Companion.DEFAULT_LINEBREAK_COUNT
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Creates a [RopeBuilder] using a depth of `0`, indent sequence of for whitespaces and a line-break count of `1`
 * using the given [block].
 *
 * @since 4.0
 */
@Pure
@TypeSafeBuilder
public inline fun rope(block: RopeBuilder.() -> Unit): String {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return rope(DEFAULT_DEPTH, DEFAULT_INDENT, DEFAULT_LINEBREAK_COUNT, block)
}

/**
 * Creates a [RopeBuilder] with the given [depth], [indent sequence][indent], [line-break count][lineBreakCount]
 * using the given [block].
 *
 * @since 4.6
 */
@Pure
@TypeSafeBuilder
public inline fun rope(
	depth: Int,
	indent: String,
	lineBreakCount: Int,
	block: RopeBuilder.() -> Unit
) : String {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return RopeBuilder(depth, indent, lineBreakCount).exert(block).toString()
}
