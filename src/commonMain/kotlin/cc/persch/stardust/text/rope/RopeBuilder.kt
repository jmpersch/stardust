/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardust.text.*
import cc.persch.stardust.text.OverflowBehavior.OVERFLOW
import cc.persch.stardust.text.TextAlign.RIGHT
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract

/**
 * Defines a string builder to comfortably create strings
 * especially for the localization of multilingual applications.
 *
 * @since 4.0
 * @see rope
 */
public class RopeBuilder(
	depth: Int = DEFAULT_DEPTH,
	public var indentWith: String = DEFAULT_INDENT,
	lineBreakCount: Int = DEFAULT_LINEBREAK_COUNT
) {
	private val buffer = StringBuilder()
	
	private var isModified = false
	
	public val length: Int
		get() = buffer.length
	
	public var depth: Int = depth.coerceAtLeast(0)
		set(value) { field = value.coerceAtLeast(0) }
	
	public var lineBreakCount: Int = lineBreakCount.coerceAtLeast(0)
		set(value) { field = value.coerceAtLeast(0) }
	
	/**
	 * The count of tailing line breaks that are automatically added to the text.
	 *
	 * @see enableTailingLineBreak
	 * @see setTailingLineBreaks
	 */
	public var tailingLineBreakCount: Int = 0
		set(value) { field = value.coerceAtLeast(0) }
	
	/**
	 * Enables prepending line breaks on indenting, even if it adds the first text to this instance.
	 * Must be placed before adding text.
	 */
	public fun enablePrependingLineBreaks() {
		
		isModified = true
	}
	
	/**
	 * Convenience function for enabling and setting the number of prepending line breaks on indenting,
	 * even if it adds the first text to this instance.
	 * Must be placed before adding text.
	 */
	public fun setPrependingLineBreaks(count: Int) {
		
		lineBreakCount = count
		enablePrependingLineBreaks()
	}
	
	/**
	 * Enables adding a tailing line break by setting the [count of tailing line breaks][tailingLineBreakCount] to `1`,
	 * if it has not already been set to a value greater than zero.
	 *
	 * @see setTailingLineBreaks
	 * @see tailingLineBreakCount
	 */
	public fun enableTailingLineBreak() {
		
		if(tailingLineBreakCount == 0)
			tailingLineBreakCount = 1
	}
	
	/**
	 * Convenience function for setting the [number of tailing line breaks][tailingLineBreakCount]
	 * to the specified [count].
	 */
	public fun setTailingLineBreaks(count: Int) {
		
		tailingLineBreakCount = count
	}
	
	@Pure
	override fun toString(): String {
		
		nl(tailingLineBreakCount)
		
		return buffer.toString()
	}
	
	/**
	 * Appends *this* value.
	 *
	 * @see withBuffer
	 */
	public operator fun CharSequence.unaryPlus() {
		
		append(this)
	}
	
	/**
	 * Appends *this* value.
	 *
	 * @see withBuffer
	 */
	public operator fun Char.unaryPlus() {
		
		append(this)
	}
	
	/**
	 * Appends *this* value with a new line prepended, if anything has been added before to the Rope format.
	 *
	 * @see appendIndented
	 */
	public operator fun CharSequence.unaryMinus() {
		
		appendIndented(this)
	}
	
	/**
	 * Appends *this* value with a new line prepended, if anything has been added before to the Rope format.
	 *
	 * @see appendIndented
	 */
	public operator fun Char.unaryMinus() {
		
		appendIndented(this)
	}
	
	/**
	 * Appends the [value] prepended with [prependLineBreaks] of line break characters.
	 */
	public fun appendIndented(value: CharSequence, prependLineBreaks: Int) {
		
		nl(prependLineBreaks)
		
		repeat(indentWith, depth)
		
		append(value)
	}
	
	/**
	 * Appends the [value] prepended with [prependLineBreaks] of line break characters.
	 */
	public fun appendIndented(value: Char, prependLineBreaks: Int) {
		
		nl(prependLineBreaks)
		
		repeat(indentWith, depth)
		
		append(value)
	}
	
	/**
	 * Appends an indent with [prependLineBreaks] of line break characters.
	 *
	 * @since 6.0
	 */
	public fun appendIndent(prependLineBreaks: Int) {
		
		nl(prependLineBreaks)
		
		repeat(indentWith, depth)
	}
	
	/**
	 * Appends the given [value].
	 *
	 * For convenience, you can use the [unary plus operator][unaryPlus], e.g. `+'X'`
	 *
	 * @see unaryPlus
	 */
	public fun append(value: Char): Unit = withBuffer { append(value) }
	
	/**
	 * Appends the given [value].
	 *
	 * For convenience, you can use the [unary plus operator][unaryPlus], e.g. `+"Hello world!"`
	 *
	 * @see unaryPlus
	 */
	public fun append(value: CharSequence): Unit = withBuffer { append(value) }
	
	private inline fun withBuffer(block: StringBuilder.() -> Unit) {
		
		contract { callsInPlace(block, EXACTLY_ONCE) }
		
		isModified = true
		
		buffer.block()
	}
	
	/**
	 * Appends given the [value] indented.
	 */
	public fun appendIndented(value: CharSequence): Unit = appendIndented(value, if(!isModified) 0 else lineBreakCount)
	
	/**
	 * Appends given the [value] indented.
	 */
	public fun appendIndented(value: Char): Unit = appendIndented(value, if(!isModified) 0 else lineBreakCount)
	
	/**
	 * Appends an indent. Has the same effect as `-""`.
	 *
	 * @since 6.0
	 */
	public fun appendIndent(): Unit = appendIndent(if(!isModified) 0 else lineBreakCount)
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// PLURAL
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// region Pre-calculated values
	
	/**
	 * Appends [plural], if [count] is unequal to `-1` and `1`.
	 */
	public fun plural(count: Byte, plural: CharSequence): Unit = this.plural(count.toDouble(), plural)
	
	/**
	 * Appends [plural], if [count] is unequal to `-1` and `1`.
	 */
	public fun plural(count: Short, plural: CharSequence): Unit = this.plural(count.toDouble(), plural)
	
	/**
	 * Appends [plural], if [count] is unequal to `-1` and `1`.
	 */
	public fun plural(count: Int, plural: CharSequence): Unit = this.plural(count.toDouble(), plural)
	
	/**
	 * Appends [plural], if [count] is unequal to `-1` and `1`.
	 */
	public fun plural(count: Long, plural: CharSequence): Unit = this.plural(count.toDouble(), plural)
	
	/**
	 * Appends [plural], if [count] is unequal to `-1` and `1`.
	 */
	public fun plural(count: Double, plural: CharSequence): Unit = this.plural(count, "", plural)
	
	
	/**
	 * Appends [singular], if [count] is `-1` or `1`; otherwise, [plural].
	 */
	public fun plural(count: Byte, singular: CharSequence, plural: CharSequence): Unit =
		this.plural(count.toDouble(), singular, plural)
	
	/**
	 * Appends [singular], if [count] is `-1` or `1`; otherwise, [plural].
	 */
	public fun plural(count: Short, singular: CharSequence, plural: CharSequence): Unit =
		this.plural(count.toDouble(), singular, plural)
	
	/**
	 * Appends [singular], if [count] is `-1` or `1`; otherwise, [plural].
	 */
	public fun plural(count: Int, singular: CharSequence, plural: CharSequence): Unit =
		this.plural(count.toDouble(), singular, plural)
	
	/**
	 * Appends [singular], if [count] is `-1` or `1`; otherwise, [plural].
	 */
	public fun plural(count: Long, singular: CharSequence, plural: CharSequence): Unit =
		this.plural(count.toDouble(), singular, plural)
	
	/**
	 * Appends [singular], if [count] is `-1` or `1`; otherwise, [plural].
	 */
	public fun plural(count: Double, singular: CharSequence, plural: CharSequence): Unit =
		this.append(if(count == 1.0 || count == -1.0) singular else plural)
	
	// endregion /Pre-calculated values/
	
	// region Builder delegate
	
	/**
	 * Appends [pluralBuilder], if [count] is unequal to `-1` and `1`.
	 */
	public inline fun plural(count: Byte, pluralBuilder: RopeBuilder.() -> Unit) {
		
		contract { callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		this.plural(count.toDouble(), pluralBuilder)
	}
	
	/**
	 * Appends [pluralBuilder], if [count] is unequal to `-1` and `1`.
	 */
	public inline fun plural(count: Short, pluralBuilder: RopeBuilder.() -> Unit) {
		
		contract { callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		this.plural(count.toDouble(), pluralBuilder)
	}
	
	/**
	 * Appends [pluralBuilder], if [count] is unequal to `-1` and `1`.
	 */
	public inline fun plural(count: Int, pluralBuilder: RopeBuilder.() -> Unit) {
		
		contract { callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		this.plural(count.toDouble(), pluralBuilder)
	}
	
	/**
	 * Appends [pluralBuilder], if [count] is unequal to `-1` and `1`.
	 */
	public inline fun plural(count: Long, pluralBuilder: RopeBuilder.() -> Unit) {
		
		contract { callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		this.plural(count.toDouble(), pluralBuilder)
	}
	
	/**
	 * Appends [pluralBuilder], if [count] is unequal to `-1` and `1`.
	 */
	public inline fun plural(count: Double, pluralBuilder: RopeBuilder.() -> Unit) {
		
		contract { callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		this.plural(count, { }, pluralBuilder)
	}
	
	
	/**
	 * Appends [singularBuilder], if [count] is `-1` or `1`; otherwise, [pluralBuilder].
	 */
	public inline fun plural(
		count: Byte,
		singularBuilder: RopeBuilder.() -> Unit,
		pluralBuilder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(singularBuilder, AT_MOST_ONCE); callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		plural(count.toDouble(), singularBuilder, pluralBuilder)
	}
	
	/**
	 * Appends [singularBuilder], if [count] is `-1` or `1`; otherwise, [pluralBuilder].
	 */
	public inline fun plural(
		count: Short,
		singularBuilder: RopeBuilder.() -> Unit,
		pluralBuilder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(singularBuilder, AT_MOST_ONCE); callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		plural(count.toDouble(), singularBuilder, pluralBuilder)
	}
	
	/**
	 * Appends [singularBuilder], if [count] is `-1` or `1`; otherwise, [pluralBuilder].
	 */
	public inline fun plural(
		count: Int,
		singularBuilder: RopeBuilder.() -> Unit,
		pluralBuilder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(singularBuilder, AT_MOST_ONCE); callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		plural(count.toDouble(), singularBuilder, pluralBuilder)
	}
	
	/**
	 * Appends [singularBuilder], if [count] is `-1` or `1`; otherwise, [pluralBuilder].
	 */
	public inline fun plural(
		count: Long,
		singularBuilder: RopeBuilder.() -> Unit,
		pluralBuilder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(singularBuilder, AT_MOST_ONCE); callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		plural(count.toDouble(), singularBuilder, pluralBuilder)
	}
	
	/**
	 * Appends [singularBuilder], if [count] is `-1` or `1`; otherwise, [pluralBuilder].
	 */
	public inline fun plural(
		count: Double,
		singularBuilder: RopeBuilder.() -> Unit,
		pluralBuilder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(singularBuilder, AT_MOST_ONCE); callsInPlace(pluralBuilder, AT_MOST_ONCE) }
		
		if(count == 1.0 || count == -1.0) singularBuilder(this) else pluralBuilder(this)
	}
	
	// endregion /Builder delegate/
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// REPEAT
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Appends the given [value] the given [times].
	 */
	public fun repeat(value: Char, times: Int): Unit = withBuffer { appendRepeated(value, (times)) }
	
	/**
	 * Appends the given [value] the given [times].
	 */
	public fun repeat(value: CharSequence, times: Int): Unit = withBuffer { appendRepeated(value, (times)) }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// LINE BREAKS AND WHITESPACES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Appends a line break.
	 */
	public fun nl(): Unit = this.append('\n')
	
	/**
	 * Appends [count] line breaks.
	 */
	public fun nl(count: Int): Unit = this.repeat('\n', count)
	
	/**
	 * Appends a white space.
	 */
	public fun sp(): Unit = this.append(' ')
	
	/**
	 * Appends [count] white spaces.
	 */
	public fun sp(count: Int): Unit = this.repeat(" ", count)
	
	/**
	 * Appends a horizontal tabulator.
	 */
	public fun tab(): Unit = this.append('\t')
	
	/**
	 * Appends [count] horizontal tabulators.
	 */
	public fun tab(count: Int): Unit = this.repeat('\t', count)
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ALIGN
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Appends text aligned right with overflow.
	 *
	 * @since 5.1
	 */
	public inline fun align(
		width: Int,
		fillChar: Char,
		builder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(builder, EXACTLY_ONCE) }
		
		align(width, RIGHT, OVERFLOW, fillChar, builder)
	}
	
	/**
	 * Appends text aligned with whitespace as fill character.
	 */
	public inline fun align(
		width: Int,
		overflowBehavior: OverflowBehavior,
		fillChar: Char = ' ',
		builder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(builder, EXACTLY_ONCE) }
		
		align(width, RIGHT, overflowBehavior, fillChar, builder)
	}
	
	/**
	 * Appends text aligned.
	 */
	public inline fun align(
		width: Int,
		align: TextAlign = RIGHT,
		overflowBehavior: OverflowBehavior = OVERFLOW,
		fillChar: Char = ' ',
		builder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(builder, EXACTLY_ONCE) }
		
		align(rope(builder), width, align, overflowBehavior, fillChar)
	}
	
	/**
	 * Appends text aligned right with overflow.
	 *
	 * @since 5.1
	 */
	public fun align(
		value: CharSequence,
		width: Int,
		fillChar: Char
	) : Unit = align(value, width, RIGHT, OVERFLOW, fillChar)
	
	/**
	 * Appends text aligned with whitespace as fill character.
	 */
	public fun align(
		value: CharSequence,
		width: Int,
		overflowBehavior: OverflowBehavior,
		fillChar: Char = ' '
	) : Unit = align(value, width, RIGHT, overflowBehavior, fillChar)
	
	/**
	 * Appends text aligned.
	 */
	public fun align(
		value: CharSequence,
		width: Int,
		align: TextAlign = RIGHT,
		overflowBehavior: OverflowBehavior = OVERFLOW,
		fillChar: Char = ' '
	) : Unit = withBuffer { appendAligned(value, width, align, overflowBehavior, fillChar) }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// GROUP
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Appends grouped text.
	 */
	public inline fun group(
		direction: GroupingDirection,
		groupSize: Int,
		separator: String,
		builder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(builder, EXACTLY_ONCE) }
		
		group(rope(builder), direction, groupSize, separator)
	}
	
	/**
	 * Appends grouped text.
	 */
	public fun group(
		value: String,
		direction: GroupingDirection,
		groupSize: Int,
		separator: String
	) : Unit = append(value.group(direction, groupSize, separator))
	
	/**
	 * Appends grouped text.
	 */
	public inline fun group(
		direction: GroupingDirection,
		groupSizes: Iterable<Int>,
		separator: String,
		builder: RopeBuilder.() -> Unit
	) : Unit = group(rope(builder), direction, groupSizes, separator)
	
	/**
	 * Appends grouped text.
	 */
	public fun group(
		value: String,
		direction: GroupingDirection,
		groupSizes: Iterable<Int>,
		separator: String
	) : Unit = withBuffer { appendGrouped(value, direction, groupSizes, separator) }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Indentation
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Creates an indentation environment.
	 *
	 * Examples:
	 *
	 * Indentation by incrementing and decrementing the depth manually:
	 *
	 * ```
	 * rope {
	 *
	 *     -"/**"
	 *     -" * Main function"
	 *     -" * "
	 *     -" * @param args The command line arguments."
	 *     -" */"
	 *     -"fun main(args: Array<String>) {"
	 *     depth++
	 *     -"println(\"Hello world!\")"
	 *     depth--
	 *     -"}"
	 * }
	 * ```
	 *
	 * Using `indent` util:
	 *
	 * ```
	 * rope {
	 *
	 *     -"/**"
	 *     -" * Main function"
	 *     -" * "
	 *     -" * @param args The command line arguments
	 *     -" */"
	 *     -"fun main(args: Array<String>) {"
	 *     nl()
	 *     indent {
	 *
	 *         -"println(\"Hello world!\")"
	 *     }
	 *     -"}"
	 * }
	 * ```
	 *
	 * Using `block` util that adds the curly braces automatically:
	 *
	 * ```
	 * rope {
	 *
	 *     -"/**"
	 *     -" * Main function"
	 *     -" * "
	 *     -" * @param args The command line arguments
	 *     -" */"
	 *     block("fun main(args: Array<String>)") {
	 *
	 *         -"println(\"Hello world!\")"
	 *     }
	 * }
	 * ```
	 *
	 * *Hint: The first call of [appendIndented] rep. the first usage of the [-][unaryMinus] operator
	 * won't prepend the [defined count of line breaks][lineBreakCount].
	 * If you want to prepend line breaks before adding text, call [enablePrependingLineBreaks].*
	 *
	 * Example:
	 *
	 * ```
	 * rope {
	 *
	 *     prependLineBreaks() // <-- Prepend predefined count of line breaks
	 *
	 *     -"/**"
	 *     -" * Main function"
	 *     -" * "
	 *     -" * @param args The command line arguments
	 *     -" */"
	 *     block("fun main(args: Array<String>)") {
	 *
	 *         -"println(\"Hello world!\")"
	 *     }
	 * }
	 * ```
	 */
	@Pure
	@TypeSafeBuilder
	public inline fun indent(
		depth: Int = this.depth + 1,
		indentWith: String = this.indentWith,
		lineBreakCount: Int = this.lineBreakCount,
		builder: RopeBuilder.() -> Unit
	) {
		contract { callsInPlace(builder, EXACTLY_ONCE) }
		
		val oldDepth = this.depth
		val oldIndentWith = this.indentWith
		val oldLineBreakCount = this.lineBreakCount
		
		this.depth = depth
		this.indentWith = indentWith
		this.lineBreakCount = lineBreakCount
		
		try {
			
			this.builder()
		}
		finally {
			
			this.depth = oldDepth
			this.indentWith = oldIndentWith
			this.lineBreakCount = oldLineBreakCount
		}
	}
	
	@PublishedApi
	internal companion object {
		
		@PublishedApi
		internal const val DEFAULT_DEPTH: Int = 0
		
		@PublishedApi
		internal const val DEFAULT_INDENT: String = "    "
		
		@PublishedApi
		internal const val DEFAULT_LINEBREAK_COUNT: Int = 1
	}
}
