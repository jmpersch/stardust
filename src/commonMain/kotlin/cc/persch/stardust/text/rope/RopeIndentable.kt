/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.BodyOption.WITH_BODY
import cc.persch.stardust.text.rope.indenters.indentCustom

/**
 * Defines that an object is indentable via a [RopeBuilder].
 * 
 * Example implementation:
 * 
 * ```
 * data class Person(val lastName: String, val firstName: String) {
 * 
 *     override fun toString() = toIndentedStringWithoutBody()
 *
 *     override fun ropeIndented(
 *         formatter: RopeFormatter,
 *         name: CharSequence?,
 *         withBody: Boolean
 *     ) = formatter.indentCustom(this, name, withBody) {
 * 
 *         indentProperty(this@Person::lastName)
 *         indentProperty(this@Person::firstName)
 *     }
 * }
 * ```
 * 
 * Example implementation using the CEH Computation Utility ([CehComputer] and [EHComputer]):
 * 
 * ```
 * data class Person(val lastName: String, val firstName: String) {
 * 
 *     override fun toString() = toIndentedStringWithoutBody()
 *
 *     override fun ropeIndented(
 *         formatter: RopeFormatter,
 *         name: CharSequence?,
 *         withBody: Boolean
 *     ) = CEHC.makeIndentedRope(this, formatter, name, withBody)
 *
 *     companion object {
 * 
 *         private val CEHC = cehComputer<Person> {
 *     
 *             +Person::lastName
 *             +Person::firstName
 *         }
 *     }
 * }
 * ```
 *
 * @since 5.0
 * @see RopeBuilder.indentCustom
 */
public interface RopeIndentable
{
	/**
	 * Indents this instance on the given [builder].
	 */
	public fun ropeIndented(builder: RopeBuilder): Unit = ropeIndented(builder, null, WITH_BODY)
	
	/**
	 * Indents this instance on the given [builder] using the given [name].
	 */
	public fun ropeIndented(builder: RopeBuilder, name: CharSequence?): Unit = ropeIndented(builder, name, WITH_BODY)
	
	/**
	 * Indents this instance on the given [builder] with the given [bodyOption].
	 */
	public fun ropeIndented(builder: RopeBuilder, bodyOption: BodyOption): Unit = ropeIndented(builder, null, bodyOption)
	
	/**
	 * Indents this instance on the given [builder], using the given [name], and with the given [bodyOption].
	 */
	public fun ropeIndented(builder: RopeBuilder, name: CharSequence?, bodyOption: BodyOption)
	
	/**
	 * Gets this instance represented as indented string.
	 */
	@Pure
	public fun toIndentedString(): String = toIndentedString(null, WITH_BODY)
	
	/**
	 * Gets this instance represented as indented string with the given [bodyOption].
	 */
	@Pure
	public fun toIndentedString(bodyOption: BodyOption): String = toIndentedString(null, bodyOption)
	
	/**
	 * Gets this instance represented as indented string using the given [name].
	 */
	@Pure
	public fun toIndentedString(name: CharSequence?): String = toIndentedString(name, WITH_BODY)
	
	/**
	 * Gets this instance represented as indented string using the given [name] with the given [bodyOption].
	 */
	@Pure
	public fun toIndentedString(name: CharSequence?, bodyOption: BodyOption): String =
		rope { ropeIndented(this, name, bodyOption) }
}
