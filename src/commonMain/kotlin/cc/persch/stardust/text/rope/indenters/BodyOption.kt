/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.rope.indenters

import cc.persch.stardust.annotations.Pure

/**
 * Defines options for body indentation.
 * 
 * @since 5.0
 */
public enum class BodyOption {
	
	WITH_BODY,
	
	NO_BODY;
	
	public companion object {
		
		@Pure
		public fun of(collection: Iterable<*>): BodyOption = when(collection) {
			
			is Collection -> of(collection)
			
			else -> withBody(collection.iterator().hasNext())
		}
		
		@Pure
		public fun of(collection: Collection<*>): BodyOption = noBody(collection.isEmpty())
		
		/**
		 * Returns [WITH_BODY] if the given [value] is `true`; otherwise, [NO_BODY]
		 */
		@Pure
		public fun withBody(value: Boolean): BodyOption = if(value) WITH_BODY else NO_BODY
		
		/**
		 * Returns [NO_BODY] if the given [value] is `true`; otherwise, [WITH_BODY]
		 */
		@Pure
		public fun noBody(value: Boolean): BodyOption = if(value) NO_BODY else WITH_BODY
		
	}
}
