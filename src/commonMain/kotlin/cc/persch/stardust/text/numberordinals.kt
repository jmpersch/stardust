/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import kotlin.math.absoluteValue

/**
 * Creates an ordinal string from this [Byte], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun Byte.toOrdinalString(): String = toLong().toOrdinalString()

/**
 * Creates an ordinal string from this [Short], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun Short.toOrdinalString(): String = toLong().toOrdinalString()

/**
 * Creates an ordinal string from this [Int], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun Int.toOrdinalString(): String = toLong().toOrdinalString()

/**
 * Creates an ordinal string from this [Long], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun Long.toOrdinalString(): String = toString() + when(absoluteValue % 10L) {
	
	1L -> "st"
	2L -> "nd"
	3L -> "rd"
	else -> "th"
}

/**
 * Creates an ordinal string from this [UByte], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun UByte.toOrdinalString(): String = toULong().toOrdinalString()

/**
 * Creates an ordinal string from this [UShort], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun UShort.toOrdinalString(): String = toULong().toOrdinalString()

/**
 * Creates an ordinal string from this [UInt], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun UInt.toOrdinalString(): String = toULong().toOrdinalString()

/**
 * Creates an ordinal string from this [ULong], e.g., `"1st"`, `"2nd"`, `"3rd"` or `"4th"`.
 *
 * @since 6.0
 */
@Pure
public fun ULong.toOrdinalString(): String = toString() + when(this % 10UL) {
	
	1UL -> "st"
	2UL -> "nd"
	3UL -> "rd"
	else -> "th"
}
