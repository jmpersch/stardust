/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.OverflowBehavior.OVERFLOW
import cc.persch.stardust.text.OverflowBehavior.TRUNCATE
import cc.persch.stardust.text.TextAlign.LEFT
import cc.persch.stardust.text.TextAlign.RIGHT

/**
 * Aligns the text.
 *
 * Example:
 *
 * ```
 * "Foo" → "    Foo"
 * ```
 * @param width The total width.
 * @param fillChar The fill char.
 * @param overflowBehavior The overflow behavior.
 */
@Pure
public fun CharSequence.align(
	width: Int,
	overflowBehavior: OverflowBehavior,
	fillChar: Char = ' '
) : String = buildString { appendAligned(this@align, width, RIGHT, overflowBehavior, fillChar) }

/**
 * Aligns the text.
 *
 * Example:
 *
 * ```
 * "Foo" → "    Foo"
 * ```
 * @param width The total width.
 * @param align The text align.
 * @param fillChar The fill char.
 * @param overflowBehavior The overflow behavior.
 * @since 1.0
 */
@Pure
public fun CharSequence.align(
	width: Int,
	align: TextAlign = RIGHT,
	overflowBehavior: OverflowBehavior = OVERFLOW,
	fillChar: Char = ' '
) : String = buildString { appendAligned(this@align, width, align, overflowBehavior, fillChar) }

/**
 * Appends text aligned
 *
 * Example:
 *
 * ```
 * "Foo" → "    Foo"
 * ```
 * @param width The total width.
 * @param fillChar The fill char.
 * @param overflowBehavior The overflow behavior.
 * @since 5.1
 */
public fun StringBuilder.appendAligned(
	value: CharSequence,
	width: Int,
	overflowBehavior: OverflowBehavior = OVERFLOW,
	fillChar: Char = ' '
) : StringBuilder = appendAligned(value, width, RIGHT, overflowBehavior, fillChar)

/**
 * Appends text aligned
 *
 * Example:
 *
 * ```
 * "Foo" → "    Foo"
 * ```
 * @param width The total width.
 * @param align The text align.
 * @param fillChar The fill char.
 * @param overflowBehavior The overflow behavior.
 * @since 5.1
 */
public fun StringBuilder.appendAligned(
	value: CharSequence,
	width: Int,
	align: TextAlign = RIGHT,
	overflowBehavior: OverflowBehavior = OVERFLOW,
	fillChar: Char = ' '
) : StringBuilder {
	
	if(value.length == width) {
		
		append(value)
		
		return this
	}
	
	if(value.length > width) {
		
		if(overflowBehavior == TRUNCATE)
			append(value, 0, width)
		else
			append(value)
		
		return this
	}
	
	val widthDiff = width - value.length
	
	when(align) {
		
		RIGHT -> {
			
			appendRepeated(fillChar, widthDiff)
			append(value)
		}
		
		LEFT -> {
			
			append(value)
			appendRepeated(fillChar, widthDiff)
		}
		
		TextAlign.CENTER -> { // CENTER
			
			val widthDiffHalf = widthDiff / 2
			val rest = widthDiff % 2
			
			appendRepeated(fillChar, widthDiffHalf)
			append(value)
			appendRepeated(fillChar, widthDiffHalf + rest)
		}
	}
	
	return this
}
