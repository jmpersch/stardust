/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.require
import cc.persch.stardust.text.particular.EmptyTextSequence
import cc.persch.stardust.text.particular.RegularTextSequence

/**
 * Creates a real sub-sequence of this character sequence.
 *
 * @param range The range for the sub-sequence.
 * @throws IllegalArgumentException [range] is invalid.
 *
 * @since 3.0
 */
@Pure
public fun CharSequence.textSequence(range: IntRange): TextSequence = textSequence(range.first, range.last)

/**
 * Creates a real sub-sequence of this character sequence.
 *
 * @param from The form-index.
 * @throws IllegalArgumentException [from] is out of range.
 *
 * @since 3.0
 */
@Pure
public fun CharSequence.textSequence(from: Int): TextSequence = textSequence(from, this.length)

/**
 * Creates a real sub-sequence of this character sequence.
 *
 * @param from The form-index.
 * @param to The to-index (excluding).
 * @throws IllegalArgumentException [from] or [to] is out of range.
 *
 * @since 3.0
 */
@Pure
public fun CharSequence.textSequence(from: Int, to: Int): TextSequence = textSequenceOf(this, from, to)

internal fun textSequenceOf(charSequence: CharSequence, from: Int, to: Int): TextSequence {
	
	require("from", from >= 0)
	require("to", to in from..charSequence.length)
	
	return when {
		
		from == to -> EmptyTextSequence
		
		charSequence is TextSequence -> charSequence.textSequence(from, to)
		
		else -> RegularTextSequence(charSequence, from, to)
	}
}
