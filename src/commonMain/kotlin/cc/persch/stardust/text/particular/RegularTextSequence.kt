/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.particular

import cc.persch.stardust.*
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.Unchecked
import cc.persch.stardust.text.TextSequence
import cc.persch.stardust.text.contentHashCode
import cc.persch.stardust.text.textSequenceOf

/**
 * Defines a real character sequence.
 *
 * Many classes that implement the interface [CharSequence] return a new [String] on the call
 * of the method [CharSequence.subSequence(…)][CharSequence.subSequence], like [String] and also [StringBuilder] do.
 * The result is an unnecessary usage of memory due to a copy of the internal character array.
 * This nullifies the expected advantage of a sub-sequence in comparison to a substring.
 *
 * This class provides a *real* sub-sequence for [CharSequence] objects. The call of
 * [subSequence(…)][RegularTextSequence.subSequence] creates a new instance of this class
 * and does *not* create a copy of the wrapped sequence, thus no unnecessary memory will be allocated.
 *
 * @since 4.0
 * @see textSequence
 *
 * @constructor Creates a new instance of the [RegularTextSequence] class.
 * @param sequence The [CharSequence] to wrap.
 * @param from The start index.
 * @param to The end index (excluding).
 * @throws IllegalArgumentException [from] or [to] is out of range.
 */
internal class RegularTextSequence(
	private val sequence: CharSequence,
	@Unchecked private val from: Int = 0,
	@Unchecked to: Int = sequence.length
) : TextSequence {
	
	override val length = to - from
	
	@Pure
	override fun get(index: Int): Char {
		
		require("index", index in 0..<this.length)
		
		return this.sequence.get(this.from + index)
	}
	
	@Pure
	override fun hashCode() = this.contentHashCode()
	
	@Pure
	override fun equals(other: Any?) = other is CharSequence && contentEquals(other)
	
	@Pure
	override fun textSequence(from: Int, to: Int): TextSequence {
		
		require("from", from >= 0)
		require("to", to in from..length)
		
		return textSequenceOf(sequence, this.from + from, this.from + to)
	}
	
	@Pure
	override fun toString(): String {
		
		val startIndex = from
		val endIndex = startIndex + length;
		
		return if(startIndex == 0 && endIndex == sequence.length) sequence.toString()
		else sequence.subSequence(startIndex, endIndex).toString()
	}
	
	@Pure
	override fun iterator(): Iterator<Char> = CharIterator(this.sequence, this.from, this.from + this.length)
	
	private inner class CharIterator(
		private val sequence: CharSequence,
		@param:Unchecked private var index: Int,
		@param:Unchecked private val to: Int
	) : Iterator<Char> {
		
		@Pure
		override operator fun hasNext() = this.index < this.to
		
		@Pure
		override operator fun next(): Char {
			
			check(this.hasNext()) { "There is no next character available." }
			
			return this.sequence[this.index++]
		}
	}
}
