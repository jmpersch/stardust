/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.text.particular

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.collections.iterators.particular.EmptyIterator
import cc.persch.stardust.require
import cc.persch.stardust.text.TextSequence
import cc.persch.stardust.text.contentHashCode

internal object EmptyTextSequence: TextSequence {
	
	override val length = 0
	
	@Pure
	override fun get(index: Int) = throw IndexOutOfBoundsException()
	
	@Pure
	override fun textSequence(from: Int, to: Int): TextSequence {
		
		require("startIndex", from == 0)
		require("endIndex", to == 0)
		
		return this
	}
	
	@Pure
	override fun hashCode() = this.contentHashCode()
	
	@Pure
	override fun equals(other: Any?) = other is CharSequence && other.length == 0
	
	@Pure
	override fun toString() = ""
	
	@Pure
	override fun iterator(): Iterator<Char> = EmptyIterator
}
