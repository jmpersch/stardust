/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.catch
import cc.persch.stardust.text.quote
import cc.persch.stardust.text.rope.rope
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

@PublishedApi
internal expect inline fun <R> ___synchronized(lock: Any, block: () -> R): R

@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
internal inline fun <R> __synchronized(lock: Any, block: () -> R): R {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return ___synchronized(lock, block)
}

internal expect fun ___printErrorLn(message: String?)

/**
 * Gets the qualified name of this [class][KClass] or `"<anonymous>"` if undefined.
 * 
 * @since 5.0
 */
@Pure
public expect fun KClass<*>.getQualifiedName(): String

/**
 * Gets the simple name of this [class][KClass] or `"<anonymous>"` if undefined.
 *
 * @since 5.0
 */
@Pure
public expect fun KClass<*>.getSimpleName(): String

/**
 * Utility function to throw an [UnreachableError].
 *
 * @see exhaustive
 * @since 6.0
 */
@Throws(UnreachableError::class)
public fun unreachable(
	message: String? = null,
	cause: Throwable? = null
) : Nothing = throw UnreachableError(message, cause)

/**
 * Utility function to throw an [ExhaustiveError].
 *
 * @see unreachable
 * @since 6.0
 */
@Throws(ExhaustiveError::class)
public fun exhaustive(
	message: String? = null,
	cause: Throwable? = null
) : Nothing = throw ExhaustiveError(message, cause)

/**
 * Returns the result of the given [block], if *this* value is negative; otherwise, *this* value is returned.
 *
 * Example usages:
 *
 * ```
 * collection.indexOf(value) ifNegative { 0 }
 *
 * collection.indexOf(value) ifNegative { return it }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline infix fun Short.ifNegative(block: (Short) -> Short): Short {
	
	contract { callsInPlace(block, AT_MOST_ONCE) }
	
	return if(this < 0) block(this) else this
}

/**
 * Returns the result of the given [block], if *this* value is negative; otherwise, *this* value is returned.
 *
 * Example usages:
 *
 * ```
 * collection.indexOf(value) ifNegative { 0 }
 *
 * collection.indexOf(value) ifNegative { return it }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline infix fun Int.ifNegative(block: (Int) -> Int): Int {
	
	contract { callsInPlace(block, AT_MOST_ONCE) }
	
	return if(this < 0) block(this) else this
}

/**
 * Returns the result of the given [block], if *this* value is negative; otherwise, *this* value is returned.
 *
 * Example usages:
 *
 * ```
 * collection.indexOf(value) ifNegative { 0 }
 *
 * collection.indexOf(value) ifNegative { return it }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline infix fun Long.ifNegative(block: (Long) -> Long): Long {
	
	contract { callsInPlace(block, AT_MOST_ONCE) }
	
	return if(this < 0) block(this) else this
}

/**
 * Creates a string representation of the given object using the given properties, e.g.,
 * `makeStringOf(foo, Foo::bar, Foo::baz)` may get `Foo{bar="Bar", baz="Baz"}`.
 *
 * @since 5.0
 */
@Pure
public fun <T: Any> makeStringOf(obj: T, vararg properties: KProperty1<T, *>): String = rope {
	
	+obj::class.getSimpleName()
	
	if(properties.isEmpty())
		return@rope
	
	+'{'
	
	for(i in properties.indices) {
		
		if(i > 0)
			+", "
		
		val property = properties[i]
		
		+property.name
		+'='
		
		property.get(obj) perform {
			
			if(it is CharSequence) +it.quote()
			else +it.toString()
		}
	}
	
	+'}'
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// To String
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the string representation of an object.
 *
 * This function corrects an issue in Kotlin’s standard API, where `toString()` is defined for `Any?`
 * and returns the string `"null"`, if the object is `null`. This issue can be hard to find, if the
 * type changes from a non-nullable to nullable type during refactoring.
 *
 * If you want to get an empty string on `null`, you can use [toStringOrEmpty].
 *
 * @since 4.5
 * @see toStringOrEmpty
 * @see toStringOrElse
 * @see toStringOrDefault
 */
@Pure
public fun Any.toStringStrict(): String = toString()

/**
 * Returns the string representation of a nullable object. If this object is `null`, an empty string is returned.
 *
 * For non-null types, use [toStringStrict].
 *
 * @since 4.5
 * @see toStringStrict
 * @see toStringOrElse
 * @see toStringOrDefault
 */
@Pure
public fun Any?.toStringOrEmpty(): String = this?.toString() ?: ""

/**
 * Returns the string representation of a nullable object. If this object is `null`, the string `"null"` is
 * returned as defined in Kotlin’s standard API.
 *
 * For non-null types, use [toStringStrict].
 *
 * @since 4.5
 * @see toStringStrict
 * @see toStringOrElse
 * @see toStringOrEmpty
 * @see toStringOrDefault
 */
@Pure
public fun Any?.toStringOrNullString(): String = toString()

/**
 * Returns the string representation of a nullable object. If this object is `null`, the [`else`] string is returned.
 *
 * For non-null types, use [toStringStrict].
 *
 * @since 4.5
 * @see toStringStrict
 * @see toStringOrEmpty
 * @see toStringOrDefault
 * @see toStringOrElse
 */
@Pure
public fun Any?.toStringOrDefault(defaultValue: String): String = this?.toString() ?: defaultValue

/**
 * Returns the string representation of a nullable object. If this object is `null`, the value of the [elseSupplier]
 * is returned.
 *
 * For non-null types, use [toStringStrict].
 *
 * @since 4.5
 * @see toStringStrict
 * @see toStringOrElse
 * @see toStringOrEmpty
 * @see toStringOrDefault
 */
@Pure
public inline fun Any?.toStringOrElse(elseSupplier: () -> String): String = this?.toString() ?: elseSupplier()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Compare Nullables
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Compares two nullable objects. If *not* inverse, a `null` reference is considered less than a non-`null` reference;
 * otherwise, a `null` reference is considered greater than a non-`null` reference.
 *
 * @since 4.0
 */
@Pure
public fun <T: Comparable<T>> compareNullables(left: T?, right: T?, inverse: Boolean = false): Int =
	compareNullables(left, right, inverse, Comparable<T>::compareTo)

/**
 * Compares two nullable objects. A `null` reference is considered less than a non-`null` reference.
 *
 * @since 5.0
 */
@Pure
public inline fun <T: Any> compareNullables(
	left: T?,
	right: T?,
	block: (T, T) -> Int
) : Int {
	
	contract { callsInPlace(block, AT_MOST_ONCE) }
	
	return compareNullables(left, right, false, block)
}

/**
 * Compares two nullable objects. If *not* inverse, a `null` reference is considered less than a non-`null` reference;
 * otherwise, a `null` reference is considered greater than a non-`null` reference
 *
 * @since 5.0
 */
@Pure
public inline fun <T: Any> compareNullables(
	left: T?,
	right: T?,
	inverse: Boolean,
	block: (T, T) -> Int
) : Int {
	
	contract { callsInPlace(block, AT_MOST_ONCE) }
	
	val result = when {
		
		left == null -> if(right == null) return 0 else -1
		
		right == null -> 1
		
		else -> block(left, right)
	}
	
	return if(inverse) result * -1 else result
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Inverts the given [predicate]: `{ !predicate() }`.
 *
 * @since 4.0
 */
@Pure
public inline fun not(crossinline predicate: () -> Boolean): () -> Boolean = { !predicate() }

/**
 * Inverts the given [predicate]: `{ !predicate(it) }`.
 *
 * @since 4.0
 */
@Pure
public inline fun <T> not(crossinline predicate: (T) -> Boolean): (T) -> Boolean = { !predicate(it) }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Try-or-Null
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the result of the given [block]. If the block throws an exception of the
 * exception type [E], `null` is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val foo: Foo? = try { parse(foo) } catch(_: IllegalStateException) { null }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val foo: Foo? = `try`<IllegalStateException> { parse(foo) }
 * ```
 *
 * With the elvis operator, you can easily specify a default value:
 *
 * ```
 * val foo: Foo = `try`<IllegalStateException> { parse(foo) } ?: Foo.default()
 * ```
 *
 * *Hint:* To gather the exception for conclusion, you can use the [catch] util:
 *
 * ```
 * val foo: Conclude<Foo, IllegalStateException> = `catch`<_, IllegalStateException> { parse(foo) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [E] type.
 * @since 6.0
 * @see catch
 */
@Pure
public inline fun <T, reified E: Exception> `try`(block: () -> T): T? {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return try { block() } catch(e: Exception) { if(e is E) null else throw e }
}

/**
 * Returns the result of the given [block]. If the block throws an exception of the given
 * [exception type to override][overrideException], `null` is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val foo: Foo? = try { parse(foo) } catch(_: IllegalStateException) { null }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val foo: Foo? = `try`(IllegalStateException::class) { parse(foo) }
 * ```
 *
 * With the elvis operator, you can easily specify a default value:
 *
 * ```
 * val foo: Foo = `try`(IllegalStateException::class) { parse(foo) } ?: Foo.default()
 * ```
 *
 * *Hint:* To gather the exception for conclusion, you can use the [catch] util:
 *
 * ```
 * val foo: Conclude<Foo, IllegalStateException> = `catch`<_, IllegalStateException> { parse(foo) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of the
 *   [overridden exception][overrideException] type.
 * @since 4.0
 * @see catch
 */
@Pure
public inline fun <T> `try`(overrideException: KClass<out Exception>, block: () -> T): T? {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return try { block() } catch(e: Exception) { if(overrideException.isInstance(e)) null else throw e }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Try Cast To
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Tries to cast this instance to the given [class]; otherwise, `null` is returned.
 *
 * @since 1.0
 */
@Suppress("UNCHECKED_CAST")
@Pure
public fun <C: Any> Any?.tryCastTo(`class`: KClass<C>): C? = if(`class`.isInstance(this)) this as C else null

/**
 * Tries to cast this instance to [C]; otherwise, `null` is returned.
 *
 * @since 4.0
 */
@Pure
public inline fun <reified C: Any> Any?.tryCastTo(): C? = this as? C
