/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.ConcludeBoolean
import cc.persch.stardust.conclude.FailureBoolean
import cc.persch.stardust.conclude.failureBooleanOf
import cc.persch.stardust.conclude.successBooleanOf
import cc.persch.stardust.parsing.BooleanParsingOption.*
import cc.persch.stardust.text.quote

/**
 * Defines case sensitivity options.
 *
 * @see parseBoolean
 * @since 6.0
 */
public enum class BooleanParsingOption {
	
	/**
	 * Case-sensitive: `"true"` and `"false"`
	 */
	CASE_SENSITIVE,
	
	/**
	 * Case-insensitive: `"true"` and `"false"` without considering the case-sensitivity of the characters.
	 * Hence, `"True"` and `"False"`, for example, are also valid strings.
	 */
	IGNORE_CASE,
	
	/**
	 * Lenient: `"true"` or `"false"` without considering the case-sensitivity
	 * and additional the literals `"1"` and `"0"`, where `"1"` means `true` and `"0"` means `false`.
	 */
	LENIENT
}

/**
 * Defines a boolean parse error.
 *
 * @since 6.0
 * @see parseBoolean
 */
public data class BooleanParseFault(public val input: String) {
	
	override fun toString(): String = "Failed parsing boolean value.\nInput: ${input.quote(maxLength = 20)}"
}

/**
 * Parses this string as a [Boolean] using the given [parsingOption]. The default [parsingOption] is [CASE_SENSITIVE].
 *
 * Returns a [FailureBoolean] containing a [NumberFormatException],
 * if this string is not a valid representation of a boolean value (`"true"` or `"false"`).
 *
 * @since 6.0
 */
@Pure
public fun String.parseBoolean(
	parsingOption: BooleanParsingOption = CASE_SENSITIVE
) : ConcludeBoolean<BooleanParseFault> {
	
	if(parsingOption == CASE_SENSITIVE) {
		
		when(this) {
			
			"true" -> return successBooleanOf(true)
			
			"false" -> return successBooleanOf(false)
		}
		
	} else {
		
		if(parsingOption == LENIENT) {
			
			when(this) {
				
				"1" -> return successBooleanOf(true)
				
				"0" -> return successBooleanOf(false)
			}
		}
		
		if("true".equals(this, ignoreCase = true))
			return successBooleanOf(true)
		
		if("false".equals(this, ignoreCase = true))
			return successBooleanOf(false)
	}
	
	return failureBooleanOf(BooleanParseFault(this))
}
