/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*
import cc.persch.stardust.text.quote

/**
 * Defines a number format fault.
 *
 * @see parseByte
 * @see parseShort
 * @see parseInt
 * @see parseLong
 * @see parseUByte
 * @see parseUShort
 * @see parseUInt
 * @see parseULong
 * @see parseFloat
 * @see parseDouble
 * @since 6.0
 */
public data class NumberFormatFault(val input: String, override val cause: Any? = null): Fault() {
	
	override fun createMessage(): String = "Invalid number format: ${input.quote()}"
}

// Signed Integers /////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Parses this string as a [Byte] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [FailureByte] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseByte(radix: Int = 10): ConcludeByte<NumberFormatFault> =
	try { toByte(radix).asSuccessByte() }
	catch(_: NumberFormatException) { failureByteOf(NumberFormatFault(this)) }

/**
 * Parses this string as a [Short] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [FailureShort] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseShort(radix: Int = 10): ConcludeShort<NumberFormatFault> =
	try { toShort(radix).asSuccessShort() }
	catch(_: NumberFormatException) { failureShortOf(NumberFormatFault(this)) }

/**
 * Parses this string as an [Int] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [FailureInt] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseInt(radix: Int = 10): ConcludeInt<NumberFormatFault> =
	try { toInt(radix).asSuccessInt() }
	catch(_: NumberFormatException) { failureIntOf(NumberFormatFault(this)) }

/**
 * Parses this string as a [Long] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [FailureLong] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseLong(radix: Int = 10): ConcludeLong<NumberFormatFault> =
	try { toLong(radix).asSuccessLong() }
	catch(_: NumberFormatException) { failureLongOf(NumberFormatFault(this)) }

// Unsigned Integers ///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Parses this string as a [UByte] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [Failure] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseUByte(radix: Int = 10): ConcludeUByte<NumberFormatFault> =
	toUByteOrNull(radix)?.asSuccessUByte() ?: failureUByteOf(NumberFormatFault(this))

/**
 * Parses this string as a [UShort] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [Failure] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseUShort(radix: Int = 10): ConcludeUShort<NumberFormatFault> =
	toUShortOrNull(radix)?.asSuccessUShort() ?: failureUShortOf(NumberFormatFault(this))

/**
 * Parses this string as a [UInt] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [Failure] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseUInt(radix: Int = 10): ConcludeUInt<NumberFormatFault> =
	toUIntOrNull(radix)?.asSuccessUInt() ?: failureUIntOf(NumberFormatFault(this))

/**
 * Parses this string as a [ULong] using the given [radix]. The default radix is `10` (decimal).
 *
 * @return [Failure] if this string is not a valid representation of a number.
 * @throws IllegalArgumentException The given [radix] is not in the range of `2..36`.
 * @since 5.0
 */
@Pure
public fun String.parseULong(radix: Int = 10): ConcludeULong<NumberFormatFault> =
	toULongOrNull(radix)?.asSuccessULong() ?: failureULongOf(NumberFormatFault(this))

// Floating Point Numbers //////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Parses this string as a [Float].
 *
 * @return [FailureFloat] if this string is not a valid representation of a number.
 * @since 5.0
 */
@Pure
public fun String.parseFloat(): ConcludeFloat<NumberFormatFault> =
	try { toFloat().asSuccessFloat() }
	catch(_: NumberFormatException) { failureFloatOf(NumberFormatFault(this)) }

/**
 * Parses this string as a [Double].
 *
 * @return [FailureDouble] if this string is not a valid representation of a number.
 * @since 5.0
 */
@Pure
public fun String.parseDouble(): ConcludeDouble<NumberFormatFault> =
	try { toDouble().asSuccessDouble() }
	catch(_: NumberFormatException) { failureDoubleOf(NumberFormatFault(this)) }
