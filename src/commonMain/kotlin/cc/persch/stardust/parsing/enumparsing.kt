/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.parsing

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.*
import cc.persch.stardust.parsing.EnumParsingOption.CASE_SENSITIVE
import cc.persch.stardust.parsing.EnumParsingOption.IGNORE_CASE
import cc.persch.stardust.text.quote
import kotlin.enums.enumEntries

/**
 * Defines options for parsing of enum values.
 *
 * @see parseEnumValue
 * @since 6.0
 */
public enum class EnumParsingOption {
	
	/**
	 * Case-sensitive parsing of the given value.
	 */
	CASE_SENSITIVE,
	
	/**
	 * Case-insensitive parsing of the given value.
	 */
	IGNORE_CASE,
}

/**
 * Defines an enum parsing fault.
 *
 * @since 5.1
 * @see parseEnumValue
 */
public data class EnumParsingFault(public val input: String, override val cause: Any? = null): Fault() {
	
	override fun createMessage(): String =
		"Failed parsing enum value: Enum constant for input value ${input.quote(maxLength = 20)} not found."
}

/**
 * Parses this string as an enum constant of the given enum type [T] using the given [parsingOption].
 * The default parsing option is [CASE_SENSITIVE].
 *
 * @return [Failure] if this string is not a valid constant of the enum type [T].
 *
 * @since 5.1
 */
@Pure
public inline fun <reified T: Enum<T>> String.parseEnumValue(
	parsingOption: EnumParsingOption = CASE_SENSITIVE
) : Conclude<T, EnumParsingFault> = when(parsingOption) {
	
	CASE_SENSITIVE ->
		try { enumValueOf<T>(this).asSuccess() }
		catch(_: IllegalArgumentException) { failureOf(EnumParsingFault(this)) }
	
	IGNORE_CASE -> enumEntries<T>()
		.find { it.name.equals(this, ignoreCase = true) }
		.orFailure { EnumParsingFault(this) }
}

