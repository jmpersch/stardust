/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.collections

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.pipe

/**
 * Computes a hash code of this collection and an [initial hash code][initialHash].
 *
 * If this collection is `null`, the result is [initialHash]; if the collection is empty,
 * the result is `initialHash * 31 + 1`.
 *
 * @since 4.0
 */
@Pure
public fun Iterable<*>?.contentHashCode(initialHash: Int): Int = contentHashCode(initialHash) { it }

/**
 * Computes a hash code of this collection.
 *
 * If this collection is `null`, the result it `0`; if the collection is empty, the result is `1`.
 *
 * @since 1.0
 */
@Pure
public fun Iterable<*>?.contentHashCode(): Int = contentHashCode { it }

/**
 * Computes a hash code of this collection, using a converter.
 *
 * If this collection is `null`, the result it `0`; if the collection is empty, the result is `1`.
 *
 * @since 4.0
 */
@Pure
public inline fun <T> Iterable<T>?.contentHashCode(converter: (T) -> Any?): Int = contentHashCode(0, converter)

/**
 * Computes a hash code of this collection, using a converter and an [initial hash code][initialHash].
 * This method is useful for cascading collection hashes.
 *
 * If this collection is `null`, the result is [initialHash]; if the collection is empty,
 * the result is `initialHash * 31 + 1`.
 *
 * @since 4.0
 */
@Pure
public inline fun <T> Iterable<T>?.contentHashCode(initialHash: Int, converter: (T) -> Any?): Int =
	contentHashCode(initialHash, converter, Any::hashCode)

/**
 * Computes a hash code of this collection, using a converter and an [initial hash code][initialHash].
 * This method is useful for cascading collection hashes.
 *
 * If this collection is `null`, the result is [initialHash]; if the collection is empty,
 * the result is `initialHash * 31 + 1`.
 *
 * @since 1.0
 */
@Pure
public inline fun <T> Iterable<T>?.contentHashCode(
	initialHash: Int,
	converter: (T) -> Any?,
	hashCodeCalculator: (Any) -> Int
) : Int {
	
	if(this == null)
		return initialHash
	
	var hash = initialHash * 31 + 1
	
	for(item in this)
		hash = hash * 31 + if(item == null) 0 else (converter(item)?.pipe { hashCodeCalculator(it) } ?: 0)
	
	return hash
}
