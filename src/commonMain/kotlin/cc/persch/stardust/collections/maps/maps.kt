/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.collections.maps

import cc.persch.stardust.annotations.Pure
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract

/**
 * Returns [this] map, if it is not `null` and not empty. Otherwise, the result of [defaultValue] is returned.
 *
 * @since 5.0
 */
@Pure
public inline fun <C, R: Map<*, *>> C?.ifNullOrEmpty(defaultValue: () -> R): R where C: R {
	
	contract { callsInPlace(defaultValue, AT_MOST_ONCE) }
	
	return if(isNullOrEmpty()) defaultValue() else this
}
