/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.collections.sets

import cc.persch.stardust.annotations.Pure

/**
 * Returns whether this set contains any element of [other].
 * That means, both collections share at least one common element.
 *
 * If [this] is empty, the result is `false`.
 * If [this] and [other] are the same instance, the result is `true`,
 * if the collection is *not* empty; otherwise `false`.
 *
 * @since 5.0
 * @see containsAll
 */
@Pure
public fun <T> Set<T>.containsAny(other: Iterable<@UnsafeVariance T>): Boolean {
	
	if(other is Set<*>)
		return containsAny(other)
	
	if(this === other)
		return isNotEmpty()
	
	if(isEmpty())
		return false
	
	for(item in other) {
		
		if(contains(item))
			return true
	}
	
	return false
}

/**
 * Returns whether this set contains any element of the [other] set.
 * That means, both collections share at least one common element.
 *
 * If one of both sets is empty, the result is `false`.
 * If [this] and [other] are the same instance, the result is `true`,
 * if the collection is *not* empty; otherwise `false`.
 *
 * @since 5.0
 * @see containsAll
 */
@Pure
public fun <T> Set<T>.containsAny(other: Set<@UnsafeVariance T>): Boolean {
	
	if(this === other)
		return isNotEmpty()
	
	if(isEmpty() ||  other.isEmpty())
		return false
	
	val iterator: Set<T>
	val container: Set<T>
	
	if(other.size < size) {
		
		iterator = other
		container = this
	}
	else {
		
		iterator = this
		container = other
	}
	
	for(item in iterator) {
		
		if(container.contains(item))
			return true
	}
	
	return false
}

/**
 * Returns whether this set contains all elements of [other].
 *
 * @since 5.0
 * @see containsAny
 */
@Pure
public fun <T> Set<T>.containsAll(other: Iterable<@UnsafeVariance T>): Boolean {
	
	if(this === other)
		return true
	
	if(other is Collection<*>) {
		
		if(other.isEmpty())
			return true
		
		if(other is Set<*> && other.size > size)
			return false
	}
	
	for(item in other) {
		
		if(!contains(item))
			return false
	}
	
	return true
}
