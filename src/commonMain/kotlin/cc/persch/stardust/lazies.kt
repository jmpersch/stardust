/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import kotlin.LazyThreadSafetyMode.*
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.contract
import kotlin.reflect.KMutableProperty0

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Lazy backing field initialization
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Realizes a lazy initialization of a property.
 *
 * Example:
 *
 * ```
 * @Transient
 * private var _value: String? = null
 *
 * override val value: String
 * 		get() = this::_value.getOrInitialize { "Hello world!" }
 * ```
 * 
 * @since 5.0
 * @see lazy
 * @see unsafeLazy
 * @see publicationSafeLazy
 * @see synchronizedLazy
 */
public inline fun <V: Any> KMutableProperty0<V?>.getOrInitialize(factory: () -> V): V {
	
	contract { callsInPlace(factory, AT_MOST_ONCE) }
	
	val x = get()
	
	if(x != null)
		return x
	
	val y = factory()
	
	set(y)
	
	return y
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Lazy Aliases
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a [Lazy] instance with [NONE] mode.
 *
 * @since 4.0
 * @see lazy
 * @see publicationSafeLazy
 * @see synchronizedLazy
 * @see getOrInitialize
 */
public fun <T> unsafeLazy(initializer: () -> T): Lazy<T> = lazy(NONE, initializer)

/**
 * Creates a [Lazy] instance with [PUBLICATION] mode.
 *
 * @since 4.0
 * @see lazy
 * @see unsafeLazy
 * @see synchronizedLazy
 * @see getOrInitialize
 */
public fun <T> publicationSafeLazy(initializer: () -> T): Lazy<T> = lazy(PUBLICATION, initializer)

/**
 * Creates a [Lazy] instance with [SYNCHRONIZED] mode.
 *
 * @since 4.0
 * @see lazy
 * @see unsafeLazy
 * @see publicationSafeLazy
 * @see getOrInitialize
 */
public fun <T> synchronizedLazy(initializer: () -> T): Lazy<T> = lazy(SYNCHRONIZED, initializer)
