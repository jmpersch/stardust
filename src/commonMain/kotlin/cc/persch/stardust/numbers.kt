/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.math.log10

/**
 * Counts the digits of an [Int], e.g. `4711.countDigits() == 4`.
 *
 * Set [includeSign] to `true` to count the sign of a negative number as additional digit,
 * e.g. `-4711.digitCount(true) = 5`.
 *
 * @since 4.0
 */
public fun Int.countDigits(includeSign: Boolean = false): Int = this.toLong().countDigits(includeSign)

/**
 * Counts the digits of a [Long], e.g. `4711L.countDigits() == 4`.
 *
 * Set [includeSign] to `true` to count the sign of a negative number as an additional digit,
 * e.g. `-4711L.digitCount(true) = 5`.
 *
 * @since 4.0
 */
public fun Long.countDigits(includeSign: Boolean = false): Int = when {
	
	this == 0L -> 1
	
	else -> floor(log10(this.absoluteValue.toDouble()) + 1.0).toInt() + if(includeSign && this < 0L) 1 else 0
}

/**
 * Counts the digits of an [UInt], e.g. `4711U.countDigits() == 4`.
 *
 * @since 4.0
 */
public fun UInt.countDigits(): Int = this.toULong().countDigits()

/**
 * Counts the digits of an [ULong], e.g. `4711UL.countDigits() == 4`.
 *
 * @since 4.0
 */
public fun ULong.countDigits(): Int = when {
	
	this == 0UL -> 1
	
	else -> floor(log10(this.toDouble()) + 1.0).toInt()
}
