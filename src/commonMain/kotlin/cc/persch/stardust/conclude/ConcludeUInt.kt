/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessUInt] or [failure][FailureUInt] of type [UInt].
 *
 * Usage example with [failure][FailureUInt] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeUInt<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ UInt         ↖ ConcludeUInt<Err>        ↖ FailureUInt<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessUInt
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessUInt
 * @see FailureUInt
 * @see successUIntOf
 * @see failureUIntOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeUInt<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeUInt] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<UInt, Error>
}


/**
 * Defines a successful [ConcludeUInt] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeUInt
 * @see FailureUInt
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessUInt @PublishedApi internal constructor(
	val value: UInt
) : ConcludeUInt<Nothing>() {
	
	/**
	 * Converts a [SuccessUInt] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<UInt> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeUInt] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeUInt
 * @see SuccessUInt
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureUInt<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeUInt<Error>() {
	
	/**
	 * Converts a [FailureUInt] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxUInt
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessUInt] instance using the given [value].
 *
 * @since 6.0
 * @see failureUIntOf
 * @see asSuccessUInt
 * @see asFailureUInt
 */
@Pure
public fun successUIntOf(value: UInt): SuccessUInt = SuccessUInt(value)

/**
 * Creates a new [SuccessUInt] instance using this value.
 *
 * @since 6.0
 * @see successUIntOf
 * @see failureUIntOf
 * @see asFailureUInt
 * @see asSuccessUInt
 */
@Pure
public fun UInt.asSuccessUInt(): SuccessUInt = SuccessUInt(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureUInt] instance using the given [reason].
 *
 * @since 6.0
 * @see successUIntOf
 * @see asSuccessUInt
 * @see asFailureUInt
 */
@Pure
public fun <Error> failureUIntOf(reason: Error): FailureUInt<Error> = FailureUInt(reason)

/**
 * Creates a new [FailureUInt] instance using this reason.
 *
 * @since 6.0
 * @see successUIntOf
 * @see failureUIntOf
 * @see asSuccessUInt
 * @see asFailureUInt
 */
@Pure
public fun <Error> Error.asFailureUInt(): FailureUInt<Error> = FailureUInt(this)

/**
 * Maps the result of this [ConcludeUInt] to another one, if it is [SuccessUInt];
 * otherwise, the [FailureUInt] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeUInt<Error>.map(
	transformer: (UInt) -> UInt
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> successUIntOf(transformer(value))
		is FailureUInt -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeUInt] to another one, if it is [SuccessUInt];
 * otherwise, the [FailureUInt] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeUInt<Error>.flatMap(
	transformer: (UInt) -> ConcludeUInt<Error>
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> transformer(value)
		is FailureUInt -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeUInt] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUInt<ErrorIn>.map(
	resultTransformer: (UInt) -> UInt,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeUInt<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> successUIntOf(resultTransformer(value))
		is FailureUInt -> failureUIntOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeUInt] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUInt<ErrorIn>.flatMap(
	resultTransformer: (UInt) -> ConcludeUInt<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUInt<ErrorOut>
) : ConcludeUInt<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> resultTransformer(value)
		is FailureUInt -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeUInt] to another one; otherwise, the [SuccessUInt] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureUInt
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUInt<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeUInt<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> this
		is FailureUInt -> failureUIntOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessUInt] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureUInt
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUInt<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeUInt<ErrorOut>
) : ConcludeUInt<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> this
		is FailureUInt -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeUInt], if it is [SuccessUInt], using the given [resultHandler].
 *
 * @returns This [ConcludeUInt].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeUInt<Error>.handle(
	resultHandler: (UInt) -> Unit
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessUInt)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeUInt], using the given handlers.
 *
 * @returns This [ConcludeUInt].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeUInt<Error>.handle(
	resultHandler: (UInt) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessUInt -> resultHandler(value)
		is FailureUInt -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeUInt], if it is [FailureUInt], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeUInt<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureUInt)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeUInt] to handle the result, if it is [SuccessUInt].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessUInt]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeUInt<*>.ifSuccess(
	resultHandler: (UInt) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessUInt)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeUInt] to handle the error, if it is [FailureUInt].
 *
 * @return `true`, if this is [FailureUInt]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeUInt<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureUInt)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureUInt] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureUInt<ErrorIn>.combineWith(
//	other: FailureUInt<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureUInt<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureUIntOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureUInt] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureUInt<Iterable<Error>>.joinWith(
//	other: FailureUInt<Iterable<Error>>
//) : FailureUInt<Iterable<Error>> = if(this === other) this else failureUIntOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeUInt<Err> {
 *
 *     //    ↙ UInt
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeUInt<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessUInt
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeUInt<Error>.or(
	failurePropagator: (FailureUInt<Error>) -> Nothing
) : UInt {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessUInt)
		return value
	
	failurePropagator(this as FailureUInt)
}

/**
 * Returns the result, if this is [SuccessUInt]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeUInt<Error>.orNull(): UInt? = if(this is SuccessUInt) value else null

/**
 * Returns the result, if this is [SuccessUInt]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeUInt<*>.orDefault(defaultValue: UInt): UInt =
	if(this is SuccessUInt) value else defaultValue

/**
 * Returns the result, if this is [SuccessUInt]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeUInt<Error>.orElse(
	elseSupplier: (Error) -> UInt
) : UInt {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> value
		is FailureUInt -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessUInt]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeUInt<Error>.orThrow(): UInt = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessUInt]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeUInt<Error>.orThrow(message: String): UInt = orThrow { message }

/**
 * Returns the result, if this is [SuccessUInt]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureUInt].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeUInt<Error>.orThrow(
	lazyMessage: () -> String
) : UInt {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUInt -> value
		
		is FailureUInt -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureUInt<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureUInt<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeUInt] from a nullable result. If this is not `null`, [SuccessUInt] is returned. If this
 * is `null`, a [FailureUInt] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successUIntOf
 * @see failureUIntOf
 * @see asSuccessUInt
 */
@Pure
public inline infix fun <Error> UInt?.orFailure(
	errorSupplier: () -> Error
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessUInt() ?: failureUIntOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessUInt]. If the block throws an exception of the specified
 * [Error] type, a [FailureUInt] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUIntOf(parse(input)) } catch(e: IllegalStateException) { failureUIntOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUInt<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUInt], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUInt<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UInt? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchUInt(
	block: () -> UInt
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUIntOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureUIntOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessUInt]. If the block throws an exception of the specified
 * [Error] type, a [FailureUInt] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUIntOf(parse(input)) } catch(e: IllegalStateException) { failureUIntOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUInt<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUInt], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUInt<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UInt? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchUInt(
	errorConverter: (E) -> Error,
	block: () -> UInt
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUIntOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureUIntOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessUInt]. If the block throws an exception of the specified
 * [Error] type, a [FailureUInt] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUIntOf(parse(input)) } catch(e: IllegalStateException) { failureUIntOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUInt(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUInt], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUInt(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UInt? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchUInt(
	catchException: KClass<Error>,
	block: () -> UInt
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUIntOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureUIntOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUIntOf(parse(input)) } catch(e: IllegalStateException) { failureUIntOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUInt(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUInt(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchUInt(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> UInt
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUIntOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureUIntOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseUInt] that returns a [ConcludeUInt]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseUInt
 * @see catchUInt
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsUInt(
	block: EnclosingContext<Error>.() -> UInt
) : UInt {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeUInt].
 *
 * Example:
 *
 * ```
 * val value: ConcludeUInt<ArithmeticException> = encloseUInt {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseUInt(block: EnclosingContext<E>.() -> UInt): ConcludeUInt<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchUInt<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeUInt].
 *
 * Example:
 *
 * ```
 * val value: ConcludeUInt<ArithmeticException> = encloseUInt(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseUInt(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> UInt
) : ConcludeUInt<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchUInt(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchUInt
 */
@Pure
public inline fun <Error, E: Exception> encloseUInt(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> UInt
) : ConcludeUInt<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchUInt(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeUInt].
 *
 * @since 6.0
 * @see Success.unboxUInt
 * @see Failure.unboxUInt
 */
@Pure
public fun <Error> Conclude<UInt, Error>.unbox(): ConcludeUInt<Error> = when(this) {
	
	is Success -> successUIntOf(value)
	is Failure -> failureUIntOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeUInt].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<UInt, Error>.unboxUInt(): ConcludeUInt<Error> = when(this) {
	
	is Success -> successUIntOf(value)
	is Failure -> failureUIntOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessUInt].
 *
 * @since 6.0
 * @see Conclude.unboxUInt
 * @see Success.unboxUInt
 * @see Failure.unboxUInt
 */
@Pure
public fun Success<UInt>.unbox(): SuccessUInt = successUIntOf(value)

/**
 * Converts a generic [Success] into a [SuccessUInt].
 *
 * @since 6.0
 * @see Conclude.unboxUInt
 * @see Failure.unboxUInt
 */
@Pure
public fun Success<UInt>.unboxUInt(): SuccessUInt = successUIntOf(value)

/**
 * Converts a generic [Failure] into a [FailureUInt].
 *
 * @since 6.0
 * @see Conclude.unboxUInt
 * @see Success.unboxUInt
 */
@Pure
public fun <Error> Failure<Error>.unboxUInt(): FailureUInt<Error> = failureUIntOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeUInt<*>>.filterSuccessUInt(): List<UInt> =
	asSequence().filterSuccessUInt().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeUInt<Error>>.filterFailureUInt(): List<Error> =
	asSequence().filterFailureUInt().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeUInt<*>>.filterSuccessUInt(): Sequence<UInt> =
	filterIsInstance<SuccessUInt>().map(SuccessUInt::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUInt<Error>>.filterFailureUInt(): Sequence<Error> =
	filterIsInstance<FailureUInt<Error>>().map(FailureUInt<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessUInt>.unwrapUInt(
) : List<UInt> = map(SuccessUInt::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureUInt<Error>>.unwrapFailureUInt(
) : List<Error> = map(FailureUInt<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessUInt>.unwrapUInt(
) : Sequence<UInt> = map(SuccessUInt::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureUInt<Error>>.unwrapFailureUInt(
) : Sequence<Error> = map(FailureUInt<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeUInt<Error>>.mapSuccessUInt(
	resultTransformer: (UInt) -> UInt
) : List<ConcludeUInt<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUInt<ErrorIn>>.mapFailureUInt(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeUInt<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUInt<ErrorIn>>.mapConcludeUInt(
	resultTransformer: (UInt) -> UInt,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeUInt<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUInt<Error>>.mapSuccessUInt(
	resultTransformer: (UInt) -> UInt
) : Sequence<ConcludeUInt<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUInt<ErrorIn>>.mapFailureUInt(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeUInt<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUInt<ErrorIn>>.mapConcludeUInt(
	resultTransformer: (UInt) -> UInt,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeUInt<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeUInt<Error>>.flatMapSuccessUInt(
	resultTransformer: (UInt) -> ConcludeUInt<Error>
) : List<ConcludeUInt<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUInt<ErrorIn>>.flatMapFailureUInt(
	errorTransformer: (ErrorIn) -> ConcludeUInt<ErrorOut>
) : List<ConcludeUInt<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUInt<ErrorIn>>.flatMapConcludeUInt(
	resultTransformer: (UInt) -> ConcludeUInt<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUInt<ErrorOut>
) : List<ConcludeUInt<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUInt<Error>>.flatMapSuccessUInt(
	resultTransformer: (UInt) -> ConcludeUInt<Error>
) : Sequence<ConcludeUInt<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUInt<ErrorIn>>.flatMapFailureUInt(
	errorTransformer: (ErrorIn) -> ConcludeUInt<ErrorOut>
) : Sequence<ConcludeUInt<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUInt<ErrorIn>>.flatMapConcludeUInt(
	resultTransformer: (UInt) -> ConcludeUInt<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUInt<ErrorOut>
) : Sequence<ConcludeUInt<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
