/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.getOrInitialize
import cc.persch.stardust.getQualifiedName
import cc.persch.stardust.pipe
import cc.persch.stardust.text.rope.rope
import kotlinx.serialization.Transient

/**
 * Defines the base class for conclusion faults.
 * Implement this class if more information about a fault, like a message and a cause, is required.
 *
 * This class is meant to be implemented as a `data class`.
 * The [fault message][message] is [created lazy][createMessage] to reduce overhead.
 * The [cause] can be of any type–it's not restricted to a [Fault] or an [Exception].
 *
 * Example implementation – here for a parsing fault:
 *
 * ```
 * data class ParsingFault(
 * 	val input: String,
 * 	val index: Int,
 * 	val description: String,
 * 	override val cause: Any? = null
 * ) : Fault() {
 *
 *     override fun createMessage(): String =
 *         "Failed parsing input at index $index: $description.\n${input.toQuotedFragment(index, 20)}"
 * }
 *
 * class Parser(val input: String) {
 *
 *     // ...
 *
 *     fun parse(): Conclude<Ast, ParsingFault> {
 *
 *         // ...
 *
 *         if(!scanner.doesFetch(','))
 *             return failureOf(ParsingFault(input, scanner.index, "Comma separator between arguments expected"))
 *
 *         // ...
 *     }
 * }
 *
 * println(Parser("foo(a b)").parse().toIndentedString())
 *
 * // Prints:
 * //
 * // Failure {
 * //
 * //     org.example.ParsingFault: Failed parsing input at index 5: Comma separator between arguments expected.
 * //     "foo(a b)"
 * //           ^
 * // }
 * ```
 *
 * **Important:** An implementation of this class must be *thread-safe!*
 *
 * @since 6.0
 */
@ThreadSafe
public abstract class Fault {
	
	/**
	 * Gets the cause of this fault; or `null` if this is the root cause or the actual cause is unknown.
	 */
	public abstract val cause: Any?
	
	/**
	 * Creates the message of this fault in a lazy manner to reduce overhead.
	 * Retrieve the created message via the [message] function.
	 *
	 * **Important:** This method must be implemented in a *thread-safe* manner.
	 *   Additionally, this method should *not* throw any exception.
	 *
	 * @return The fault message or an empty string, if no message is available.
	 * @see message
	 */
	@Pure
	protected abstract fun createMessage(): String
	
	@Transient
	private var __cachedMessage: Any? = null
	
	private fun createCachedMessage(): Any = this::__cachedMessage.getOrInitialize {
		
		try { createMessage() } catch(e: Exception) { e }
	}
	
	/**
	 * Gets the fault message. Can be an empty string.
	 *
	 * Note: The message is created by the [createMessage] function.
	 *
	 * In the case that an exception occurs during creating the message via [createMessage],
	 * an empty string is returned.
	 * To get information about the exception, use [toString].
	 */
	@Cached
	@ShouldUse
	public fun message(): String = createCachedMessage() as? String ?: ""
	
	/**
	 * Returns a short description of this fault.
	 *
	 * The result is the concatenation of:
	 *
	 * * the qualified name of the class of this object
	 * * ": " (a colon and a space; if the [message] is a non-empty string)
	 * * the [message]
	 * * "Cause: "
	 * * the qualified name of the class of the object that caused this fault
	 *
	 * In the case that an exception occurs during creating the message via [createMessage], information about
	 * the occurred exception is appended instead of the colon and the message.
	 *
	 * Note: This function is `final` to force data classes not to override this method.
	 */
	final override fun toString(): String = rope {
		
		+this@Fault::class.getQualifiedName()
		
		createCachedMessage() pipe {
			
			if(it !is String) {
				
				-"FATAL: An exception occurred while creating the message of this fault! "
				+it.toString()
			}
			else if(it.isNotEmpty()) {
				
				+": "
				+it
			}
		}
		
		cause?.pipe {
			
			-"Cause: "
			
			try {
				
				+it.toString()
			}
			catch(e: Exception) {
				
				-"FATAL: An exception occurred while creating the string representation of the failure cause! "
				+e.toString()
			}
		}
	}
}
