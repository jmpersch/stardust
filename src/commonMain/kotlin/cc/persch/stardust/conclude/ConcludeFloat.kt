/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessFloat] or [failure][FailureFloat] of type [Float].
 *
 * Usage example with [failure][FailureFloat] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeFloat<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Float       ↖ ConcludeFloat<Err>        ↖ FailureFloat<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessFloat
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessFloat
 * @see FailureFloat
 * @see successFloatOf
 * @see failureFloatOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeFloat<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeFloat] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Float, Error>
}


/**
 * Defines a successful [ConcludeFloat] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeFloat
 * @see FailureFloat
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessFloat @PublishedApi internal constructor(
	val value: Float
) : ConcludeFloat<Nothing>() {
	
	/**
	 * Converts a [SuccessFloat] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Float> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeFloat] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeFloat
 * @see SuccessFloat
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureFloat<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeFloat<Error>() {
	
	/**
	 * Converts a [FailureFloat] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxFloat
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessFloat] instance using the given [value].
 *
 * @since 6.0
 * @see failureFloatOf
 * @see asSuccessFloat
 * @see asFailureFloat
 */
@Pure
public fun successFloatOf(value: Float): SuccessFloat = SuccessFloat(value)

/**
 * Creates a new [SuccessFloat] instance using this value.
 *
 * @since 6.0
 * @see successFloatOf
 * @see failureFloatOf
 * @see asFailureFloat
 * @see asSuccessFloat
 */
@Pure
public fun Float.asSuccessFloat(): SuccessFloat = SuccessFloat(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureFloat] instance using the given [reason].
 *
 * @since 6.0
 * @see successFloatOf
 * @see asSuccessFloat
 * @see asFailureFloat
 */
@Pure
public fun <Error> failureFloatOf(reason: Error): FailureFloat<Error> = FailureFloat(reason)

/**
 * Creates a new [FailureFloat] instance using this reason.
 *
 * @since 6.0
 * @see successFloatOf
 * @see failureFloatOf
 * @see asSuccessFloat
 * @see asFailureFloat
 */
@Pure
public fun <Error> Error.asFailureFloat(): FailureFloat<Error> = FailureFloat(this)

/**
 * Maps the result of this [ConcludeFloat] to another one, if it is [SuccessFloat];
 * otherwise, the [FailureFloat] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeFloat<Error>.map(
	transformer: (Float) -> Float
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> successFloatOf(transformer(value))
		is FailureFloat -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeFloat] to another one, if it is [SuccessFloat];
 * otherwise, the [FailureFloat] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeFloat<Error>.flatMap(
	transformer: (Float) -> ConcludeFloat<Error>
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> transformer(value)
		is FailureFloat -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeFloat] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeFloat<ErrorIn>.map(
	resultTransformer: (Float) -> Float,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeFloat<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> successFloatOf(resultTransformer(value))
		is FailureFloat -> failureFloatOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeFloat] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeFloat<ErrorIn>.flatMap(
	resultTransformer: (Float) -> ConcludeFloat<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeFloat<ErrorOut>
) : ConcludeFloat<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> resultTransformer(value)
		is FailureFloat -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeFloat] to another one; otherwise, the [SuccessFloat] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureFloat
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeFloat<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeFloat<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> this
		is FailureFloat -> failureFloatOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessFloat] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureFloat
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeFloat<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeFloat<ErrorOut>
) : ConcludeFloat<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> this
		is FailureFloat -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeFloat], if it is [SuccessFloat], using the given [resultHandler].
 *
 * @returns This [ConcludeFloat].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeFloat<Error>.handle(
	resultHandler: (Float) -> Unit
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessFloat)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeFloat], using the given handlers.
 *
 * @returns This [ConcludeFloat].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeFloat<Error>.handle(
	resultHandler: (Float) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessFloat -> resultHandler(value)
		is FailureFloat -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeFloat], if it is [FailureFloat], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeFloat<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureFloat)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeFloat] to handle the result, if it is [SuccessFloat].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessFloat]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeFloat<*>.ifSuccess(
	resultHandler: (Float) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessFloat)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeFloat] to handle the error, if it is [FailureFloat].
 *
 * @return `true`, if this is [FailureFloat]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeFloat<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureFloat)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureFloat] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureFloat<ErrorIn>.combineWith(
//	other: FailureFloat<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureFloat<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureFloatOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureFloat] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureFloat<Iterable<Error>>.joinWith(
//	other: FailureFloat<Iterable<Error>>
//) : FailureFloat<Iterable<Error>> = if(this === other) this else failureFloatOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeFloat<Err> {
 *
 *     //    ↙ Float
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeFloat<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessFloat
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeFloat<Error>.or(
	failurePropagator: (FailureFloat<Error>) -> Nothing
) : Float {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessFloat)
		return value
	
	failurePropagator(this as FailureFloat)
}

/**
 * Returns the result, if this is [SuccessFloat]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeFloat<Error>.orNull(): Float? = if(this is SuccessFloat) value else null

/**
 * Returns the result, if this is [SuccessFloat]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeFloat<*>.orDefault(defaultValue: Float): Float =
	if(this is SuccessFloat) value else defaultValue

/**
 * Returns the result, if this is [SuccessFloat]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeFloat<Error>.orElse(
	elseSupplier: (Error) -> Float
) : Float {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> value
		is FailureFloat -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessFloat]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeFloat<Error>.orThrow(): Float = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessFloat]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeFloat<Error>.orThrow(message: String): Float = orThrow { message }

/**
 * Returns the result, if this is [SuccessFloat]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureFloat].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeFloat<Error>.orThrow(
	lazyMessage: () -> String
) : Float {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessFloat -> value
		
		is FailureFloat -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureFloat<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureFloat<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeFloat] from a nullable result. If this is not `null`, [SuccessFloat] is returned. If this
 * is `null`, a [FailureFloat] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successFloatOf
 * @see failureFloatOf
 * @see asSuccessFloat
 */
@Pure
public inline infix fun <Error> Float?.orFailure(
	errorSupplier: () -> Error
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessFloat() ?: failureFloatOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessFloat]. If the block throws an exception of the specified
 * [Error] type, a [FailureFloat] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successFloatOf(parse(input)) } catch(e: IllegalStateException) { failureFloatOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchFloat<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeFloat], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchFloat<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Float? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchFloat(
	block: () -> Float
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successFloatOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureFloatOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessFloat]. If the block throws an exception of the specified
 * [Error] type, a [FailureFloat] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successFloatOf(parse(input)) } catch(e: IllegalStateException) { failureFloatOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchFloat<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeFloat], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchFloat<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Float? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchFloat(
	errorConverter: (E) -> Error,
	block: () -> Float
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successFloatOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureFloatOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessFloat]. If the block throws an exception of the specified
 * [Error] type, a [FailureFloat] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successFloatOf(parse(input)) } catch(e: IllegalStateException) { failureFloatOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchFloat(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeFloat], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchFloat(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Float? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchFloat(
	catchException: KClass<Error>,
	block: () -> Float
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successFloatOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureFloatOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successFloatOf(parse(input)) } catch(e: IllegalStateException) { failureFloatOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchFloat(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchFloat(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchFloat(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Float
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successFloatOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureFloatOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseFloat] that returns a [ConcludeFloat]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseFloat
 * @see catchFloat
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsFloat(
	block: EnclosingContext<Error>.() -> Float
) : Float {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeFloat].
 *
 * Example:
 *
 * ```
 * val value: ConcludeFloat<ArithmeticException> = encloseFloat {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseFloat(block: EnclosingContext<E>.() -> Float): ConcludeFloat<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchFloat<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeFloat].
 *
 * Example:
 *
 * ```
 * val value: ConcludeFloat<ArithmeticException> = encloseFloat(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseFloat(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Float
) : ConcludeFloat<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchFloat(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchFloat
 */
@Pure
public inline fun <Error, E: Exception> encloseFloat(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Float
) : ConcludeFloat<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchFloat(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeFloat].
 *
 * @since 6.0
 * @see Success.unboxFloat
 * @see Failure.unboxFloat
 */
@Pure
public fun <Error> Conclude<Float, Error>.unbox(): ConcludeFloat<Error> = when(this) {
	
	is Success -> successFloatOf(value)
	is Failure -> failureFloatOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeFloat].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Float, Error>.unboxFloat(): ConcludeFloat<Error> = when(this) {
	
	is Success -> successFloatOf(value)
	is Failure -> failureFloatOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessFloat].
 *
 * @since 6.0
 * @see Conclude.unboxFloat
 * @see Success.unboxFloat
 * @see Failure.unboxFloat
 */
@Pure
public fun Success<Float>.unbox(): SuccessFloat = successFloatOf(value)

/**
 * Converts a generic [Success] into a [SuccessFloat].
 *
 * @since 6.0
 * @see Conclude.unboxFloat
 * @see Failure.unboxFloat
 */
@Pure
public fun Success<Float>.unboxFloat(): SuccessFloat = successFloatOf(value)

/**
 * Converts a generic [Failure] into a [FailureFloat].
 *
 * @since 6.0
 * @see Conclude.unboxFloat
 * @see Success.unboxFloat
 */
@Pure
public fun <Error> Failure<Error>.unboxFloat(): FailureFloat<Error> = failureFloatOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeFloat<*>>.filterSuccessFloat(): List<Float> =
	asSequence().filterSuccessFloat().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeFloat<Error>>.filterFailureFloat(): List<Error> =
	asSequence().filterFailureFloat().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeFloat<*>>.filterSuccessFloat(): Sequence<Float> =
	filterIsInstance<SuccessFloat>().map(SuccessFloat::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeFloat<Error>>.filterFailureFloat(): Sequence<Error> =
	filterIsInstance<FailureFloat<Error>>().map(FailureFloat<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessFloat>.unwrapFloat(
) : List<Float> = map(SuccessFloat::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureFloat<Error>>.unwrapFailureFloat(
) : List<Error> = map(FailureFloat<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessFloat>.unwrapFloat(
) : Sequence<Float> = map(SuccessFloat::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureFloat<Error>>.unwrapFailureFloat(
) : Sequence<Error> = map(FailureFloat<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeFloat<Error>>.mapSuccessFloat(
	resultTransformer: (Float) -> Float
) : List<ConcludeFloat<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeFloat<ErrorIn>>.mapFailureFloat(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeFloat<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeFloat<ErrorIn>>.mapConcludeFloat(
	resultTransformer: (Float) -> Float,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeFloat<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeFloat<Error>>.mapSuccessFloat(
	resultTransformer: (Float) -> Float
) : Sequence<ConcludeFloat<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeFloat<ErrorIn>>.mapFailureFloat(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeFloat<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeFloat<ErrorIn>>.mapConcludeFloat(
	resultTransformer: (Float) -> Float,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeFloat<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeFloat<Error>>.flatMapSuccessFloat(
	resultTransformer: (Float) -> ConcludeFloat<Error>
) : List<ConcludeFloat<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeFloat<ErrorIn>>.flatMapFailureFloat(
	errorTransformer: (ErrorIn) -> ConcludeFloat<ErrorOut>
) : List<ConcludeFloat<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeFloat<ErrorIn>>.flatMapConcludeFloat(
	resultTransformer: (Float) -> ConcludeFloat<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeFloat<ErrorOut>
) : List<ConcludeFloat<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeFloat<Error>>.flatMapSuccessFloat(
	resultTransformer: (Float) -> ConcludeFloat<Error>
) : Sequence<ConcludeFloat<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeFloat<ErrorIn>>.flatMapFailureFloat(
	errorTransformer: (ErrorIn) -> ConcludeFloat<ErrorOut>
) : Sequence<ConcludeFloat<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeFloat<ErrorIn>>.flatMapConcludeFloat(
	resultTransformer: (Float) -> ConcludeFloat<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeFloat<ErrorOut>
) : Sequence<ConcludeFloat<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
