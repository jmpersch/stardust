/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessUByte] or [failure][FailureUByte] of type [UByte].
 *
 * Usage example with [failure][FailureUByte] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeUByte<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ UByte         ↖ ConcludeUByte<Err>      ↖ FailureUByte<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessUByte
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessUByte
 * @see FailureUByte
 * @see successUByteOf
 * @see failureUByteOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeUByte<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeUByte] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<UByte, Error>
}


/**
 * Defines a successful [ConcludeUByte] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeUByte
 * @see FailureUByte
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessUByte @PublishedApi internal constructor(
	val value: UByte
) : ConcludeUByte<Nothing>() {
	
	/**
	 * Converts a [SuccessUByte] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<UByte> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeUByte] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeUByte
 * @see SuccessUByte
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureUByte<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeUByte<Error>() {
	
	/**
	 * Converts a [FailureUByte] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxUByte
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessUByte] instance using the given [value].
 *
 * @since 6.0
 * @see failureUByteOf
 * @see asSuccessUByte
 * @see asFailureUByte
 */
@Pure
public fun successUByteOf(value: UByte): SuccessUByte = SuccessUByte(value)

/**
 * Creates a new [SuccessUByte] instance using this value.
 *
 * @since 6.0
 * @see successUByteOf
 * @see failureUByteOf
 * @see asFailureUByte
 * @see asSuccessUByte
 */
@Pure
public fun UByte.asSuccessUByte(): SuccessUByte = SuccessUByte(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureUByte] instance using the given [reason].
 *
 * @since 6.0
 * @see successUByteOf
 * @see asSuccessUByte
 * @see asFailureUByte
 */
@Pure
public fun <Error> failureUByteOf(reason: Error): FailureUByte<Error> = FailureUByte(reason)

/**
 * Creates a new [FailureUByte] instance using this reason.
 *
 * @since 6.0
 * @see successUByteOf
 * @see failureUByteOf
 * @see asSuccessUByte
 * @see asFailureUByte
 */
@Pure
public fun <Error> Error.asFailureUByte(): FailureUByte<Error> = FailureUByte(this)

/**
 * Maps the result of this [ConcludeUByte] to another one, if it is [SuccessUByte];
 * otherwise, the [FailureUByte] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeUByte<Error>.map(
	transformer: (UByte) -> UByte
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> successUByteOf(transformer(value))
		is FailureUByte -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeUByte] to another one, if it is [SuccessUByte];
 * otherwise, the [FailureUByte] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeUByte<Error>.flatMap(
	transformer: (UByte) -> ConcludeUByte<Error>
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> transformer(value)
		is FailureUByte -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeUByte] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUByte<ErrorIn>.map(
	resultTransformer: (UByte) -> UByte,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeUByte<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> successUByteOf(resultTransformer(value))
		is FailureUByte -> failureUByteOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeUByte] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUByte<ErrorIn>.flatMap(
	resultTransformer: (UByte) -> ConcludeUByte<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUByte<ErrorOut>
) : ConcludeUByte<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> resultTransformer(value)
		is FailureUByte -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeUByte] to another one; otherwise, the [SuccessUByte] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureUByte
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUByte<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeUByte<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> this
		is FailureUByte -> failureUByteOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessUByte] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureUByte
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUByte<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeUByte<ErrorOut>
) : ConcludeUByte<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> this
		is FailureUByte -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeUByte], if it is [SuccessUByte], using the given [resultHandler].
 *
 * @returns This [ConcludeUByte].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeUByte<Error>.handle(
	resultHandler: (UByte) -> Unit
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessUByte)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeUByte], using the given handlers.
 *
 * @returns This [ConcludeUByte].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeUByte<Error>.handle(
	resultHandler: (UByte) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessUByte -> resultHandler(value)
		is FailureUByte -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeUByte], if it is [FailureUByte], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeUByte<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureUByte)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeUByte] to handle the result, if it is [SuccessUByte].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessUByte]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeUByte<*>.ifSuccess(
	resultHandler: (UByte) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessUByte)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeUByte] to handle the error, if it is [FailureUByte].
 *
 * @return `true`, if this is [FailureUByte]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeUByte<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureUByte)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureUByte] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureUByte<ErrorIn>.combineWith(
//	other: FailureUByte<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureUByte<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureUByteOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureUByte] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureUByte<Iterable<Error>>.joinWith(
//	other: FailureUByte<Iterable<Error>>
//) : FailureUByte<Iterable<Error>> = if(this === other) this else failureUByteOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeUByte<Err> {
 *
 *     //    ↙ UByte
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeUByte<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessUByte
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeUByte<Error>.or(
	failurePropagator: (FailureUByte<Error>) -> Nothing
) : UByte {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessUByte)
		return value
	
	failurePropagator(this as FailureUByte)
}

/**
 * Returns the result, if this is [SuccessUByte]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeUByte<Error>.orNull(): UByte? = if(this is SuccessUByte) value else null

/**
 * Returns the result, if this is [SuccessUByte]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeUByte<*>.orDefault(defaultValue: UByte): UByte =
	if(this is SuccessUByte) value else defaultValue

/**
 * Returns the result, if this is [SuccessUByte]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeUByte<Error>.orElse(
	elseSupplier: (Error) -> UByte
) : UByte {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> value
		is FailureUByte -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessUByte]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeUByte<Error>.orThrow(): UByte = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessUByte]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeUByte<Error>.orThrow(message: String): UByte = orThrow { message }

/**
 * Returns the result, if this is [SuccessUByte]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureUByte].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeUByte<Error>.orThrow(
	lazyMessage: () -> String
) : UByte {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUByte -> value
		
		is FailureUByte -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureUByte<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureUByte<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeUByte] from a nullable result. If this is not `null`, [SuccessUByte] is returned. If this
 * is `null`, a [FailureUByte] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successUByteOf
 * @see failureUByteOf
 * @see asSuccessUByte
 */
@Pure
public inline infix fun <Error> UByte?.orFailure(
	errorSupplier: () -> Error
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessUByte() ?: failureUByteOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessUByte]. If the block throws an exception of the specified
 * [Error] type, a [FailureUByte] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUByteOf(parse(input)) } catch(e: IllegalStateException) { failureUByteOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUByte<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUByte], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUByte<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UByte? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchUByte(
	block: () -> UByte
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUByteOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureUByteOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessUByte]. If the block throws an exception of the specified
 * [Error] type, a [FailureUByte] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUByteOf(parse(input)) } catch(e: IllegalStateException) { failureUByteOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUByte<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUByte], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUByte<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UByte? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchUByte(
	errorConverter: (E) -> Error,
	block: () -> UByte
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUByteOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureUByteOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessUByte]. If the block throws an exception of the specified
 * [Error] type, a [FailureUByte] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUByteOf(parse(input)) } catch(e: IllegalStateException) { failureUByteOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUByte(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUByte], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUByte(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UByte? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchUByte(
	catchException: KClass<Error>,
	block: () -> UByte
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUByteOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureUByteOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUByteOf(parse(input)) } catch(e: IllegalStateException) { failureUByteOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUByte(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUByte(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchUByte(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> UByte
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUByteOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureUByteOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseUByte] that returns a [ConcludeUByte]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseUByte
 * @see catchUByte
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsUByte(
	block: EnclosingContext<Error>.() -> UByte
) : UByte {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeUByte].
 *
 * Example:
 *
 * ```
 * val value: ConcludeUByte<ArithmeticException> = encloseUByte {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseUByte(block: EnclosingContext<E>.() -> UByte): ConcludeUByte<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchUByte<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeUByte].
 *
 * Example:
 *
 * ```
 * val value: ConcludeUByte<ArithmeticException> = encloseUByte(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseUByte(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> UByte
) : ConcludeUByte<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchUByte(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchUByte
 */
@Pure
public inline fun <Error, E: Exception> encloseUByte(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> UByte
) : ConcludeUByte<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchUByte(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeUByte].
 *
 * @since 6.0
 * @see Success.unboxUByte
 * @see Failure.unboxUByte
 */
@Pure
public fun <Error> Conclude<UByte, Error>.unbox(): ConcludeUByte<Error> = when(this) {
	
	is Success -> successUByteOf(value)
	is Failure -> failureUByteOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeUByte].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<UByte, Error>.unboxUByte(): ConcludeUByte<Error> = when(this) {
	
	is Success -> successUByteOf(value)
	is Failure -> failureUByteOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessUByte].
 *
 * @since 6.0
 * @see Conclude.unboxUByte
 * @see Success.unboxUByte
 * @see Failure.unboxUByte
 */
@Pure
public fun Success<UByte>.unbox(): SuccessUByte = successUByteOf(value)

/**
 * Converts a generic [Success] into a [SuccessUByte].
 *
 * @since 6.0
 * @see Conclude.unboxUByte
 * @see Failure.unboxUByte
 */
@Pure
public fun Success<UByte>.unboxUByte(): SuccessUByte = successUByteOf(value)

/**
 * Converts a generic [Failure] into a [FailureUByte].
 *
 * @since 6.0
 * @see Conclude.unboxUByte
 * @see Success.unboxUByte
 */
@Pure
public fun <Error> Failure<Error>.unboxUByte(): FailureUByte<Error> = failureUByteOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeUByte<*>>.filterSuccessUByte(): List<UByte> =
	asSequence().filterSuccessUByte().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeUByte<Error>>.filterFailureUByte(): List<Error> =
	asSequence().filterFailureUByte().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeUByte<*>>.filterSuccessUByte(): Sequence<UByte> =
	filterIsInstance<SuccessUByte>().map(SuccessUByte::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUByte<Error>>.filterFailureUByte(): Sequence<Error> =
	filterIsInstance<FailureUByte<Error>>().map(FailureUByte<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessUByte>.unwrapUByte(
) : List<UByte> = map(SuccessUByte::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureUByte<Error>>.unwrapFailureUByte(
) : List<Error> = map(FailureUByte<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessUByte>.unwrapUByte(
) : Sequence<UByte> = map(SuccessUByte::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureUByte<Error>>.unwrapFailureUByte(
) : Sequence<Error> = map(FailureUByte<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeUByte<Error>>.mapSuccessUByte(
	resultTransformer: (UByte) -> UByte
) : List<ConcludeUByte<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUByte<ErrorIn>>.mapFailureUByte(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeUByte<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUByte<ErrorIn>>.mapConcludeUByte(
	resultTransformer: (UByte) -> UByte,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeUByte<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUByte<Error>>.mapSuccessUByte(
	resultTransformer: (UByte) -> UByte
) : Sequence<ConcludeUByte<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUByte<ErrorIn>>.mapFailureUByte(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeUByte<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUByte<ErrorIn>>.mapConcludeUByte(
	resultTransformer: (UByte) -> UByte,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeUByte<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeUByte<Error>>.flatMapSuccessUByte(
	resultTransformer: (UByte) -> ConcludeUByte<Error>
) : List<ConcludeUByte<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUByte<ErrorIn>>.flatMapFailureUByte(
	errorTransformer: (ErrorIn) -> ConcludeUByte<ErrorOut>
) : List<ConcludeUByte<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUByte<ErrorIn>>.flatMapConcludeUByte(
	resultTransformer: (UByte) -> ConcludeUByte<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUByte<ErrorOut>
) : List<ConcludeUByte<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUByte<Error>>.flatMapSuccessUByte(
	resultTransformer: (UByte) -> ConcludeUByte<Error>
) : Sequence<ConcludeUByte<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUByte<ErrorIn>>.flatMapFailureUByte(
	errorTransformer: (ErrorIn) -> ConcludeUByte<ErrorOut>
) : Sequence<ConcludeUByte<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUByte<ErrorIn>>.flatMapConcludeUByte(
	resultTransformer: (UByte) -> ConcludeUByte<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUByte<ErrorOut>
) : Sequence<ConcludeUByte<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
