/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.getQualifiedName

/**
 * Defines an enclosing context for operations that may throw an exception.
 *
 * @since 6.0
 * @see Conclude
 * @see enclose
 * @see encloseBoolean
 * @see encloseByte
 * @see encloseShort
 * @see encloseInt
 * @see encloseLong
 * @see encloseUByte
 * @see encloseUShort
 * @see encloseUInt
 * @see encloseULong
 * @see encloseFloat
 * @see encloseDouble
 */
@ThreadSafe
public class EnclosingContext<in Error: Exception> private constructor() {
	
	override fun toString(): String = this::class.getQualifiedName()
	
	@PublishedApi
	internal companion object {
		
		@PublishedApi
		internal val INSTANCE: EnclosingContext<Exception> = EnclosingContext()
	}
}
