/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessDouble] or [failure][FailureDouble] of type [Double].
 *
 * Usage example with [failure][FailureDouble] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeDouble<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Double         ↖ ConcludeDouble<Err>    ↖ FailureDouble<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessDouble
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessDouble
 * @see FailureDouble
 * @see successDoubleOf
 * @see failureDoubleOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeDouble<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeDouble] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Double, Error>
}


/**
 * Defines a successful [ConcludeDouble] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeDouble
 * @see FailureDouble
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessDouble @PublishedApi internal constructor(
	val value: Double
) : ConcludeDouble<Nothing>() {
	
	/**
	 * Converts a [SuccessDouble] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Double> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeDouble] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeDouble
 * @see SuccessDouble
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureDouble<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeDouble<Error>() {
	
	/**
	 * Converts a [FailureDouble] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxDouble
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessDouble] instance using the given [value].
 *
 * @since 6.0
 * @see failureDoubleOf
 * @see asSuccessDouble
 * @see asFailureDouble
 */
@Pure
public fun successDoubleOf(value: Double): SuccessDouble = SuccessDouble(value)

/**
 * Creates a new [SuccessDouble] instance using this value.
 *
 * @since 6.0
 * @see successDoubleOf
 * @see failureDoubleOf
 * @see asFailureDouble
 * @see asSuccessDouble
 */
@Pure
public fun Double.asSuccessDouble(): SuccessDouble = SuccessDouble(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureDouble] instance using the given [reason].
 *
 * @since 6.0
 * @see successDoubleOf
 * @see asSuccessDouble
 * @see asFailureDouble
 */
@Pure
public fun <Error> failureDoubleOf(reason: Error): FailureDouble<Error> = FailureDouble(reason)

/**
 * Creates a new [FailureDouble] instance using this reason.
 *
 * @since 6.0
 * @see successDoubleOf
 * @see failureDoubleOf
 * @see asSuccessDouble
 * @see asFailureDouble
 */
@Pure
public fun <Error> Error.asFailureDouble(): FailureDouble<Error> = FailureDouble(this)

/**
 * Maps the result of this [ConcludeDouble] to another one, if it is [SuccessDouble];
 * otherwise, the [FailureDouble] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeDouble<Error>.map(
	transformer: (Double) -> Double
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> successDoubleOf(transformer(value))
		is FailureDouble -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeDouble] to another one, if it is [SuccessDouble];
 * otherwise, the [FailureDouble] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeDouble<Error>.flatMap(
	transformer: (Double) -> ConcludeDouble<Error>
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> transformer(value)
		is FailureDouble -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeDouble] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeDouble<ErrorIn>.map(
	resultTransformer: (Double) -> Double,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeDouble<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> successDoubleOf(resultTransformer(value))
		is FailureDouble -> failureDoubleOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeDouble] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeDouble<ErrorIn>.flatMap(
	resultTransformer: (Double) -> ConcludeDouble<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeDouble<ErrorOut>
) : ConcludeDouble<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> resultTransformer(value)
		is FailureDouble -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeDouble] to another one; otherwise, the [SuccessDouble] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureDouble
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeDouble<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeDouble<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> this
		is FailureDouble -> failureDoubleOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessDouble] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureDouble
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeDouble<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeDouble<ErrorOut>
) : ConcludeDouble<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> this
		is FailureDouble -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeDouble], if it is [SuccessDouble], using the given [resultHandler].
 *
 * @returns This [ConcludeDouble].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeDouble<Error>.handle(
	resultHandler: (Double) -> Unit
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessDouble)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeDouble], using the given handlers.
 *
 * @returns This [ConcludeDouble].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeDouble<Error>.handle(
	resultHandler: (Double) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessDouble -> resultHandler(value)
		is FailureDouble -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeDouble], if it is [FailureDouble], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeDouble<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureDouble)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeDouble] to handle the result, if it is [SuccessDouble].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessDouble]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeDouble<*>.ifSuccess(
	resultHandler: (Double) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessDouble)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeDouble] to handle the error, if it is [FailureDouble].
 *
 * @return `true`, if this is [FailureDouble]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeDouble<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureDouble)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureDouble] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureDouble<ErrorIn>.combineWith(
//	other: FailureDouble<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureDouble<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureDoubleOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureDouble] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureDouble<Iterable<Error>>.joinWith(
//	other: FailureDouble<Iterable<Error>>
//) : FailureDouble<Iterable<Error>> = if(this === other) this else failureDoubleOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeDouble<Err> {
 *
 *     //    ↙ Double
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeDouble<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessDouble
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeDouble<Error>.or(
	failurePropagator: (FailureDouble<Error>) -> Nothing
) : Double {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessDouble)
		return value
	
	failurePropagator(this as FailureDouble)
}

/**
 * Returns the result, if this is [SuccessDouble]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeDouble<Error>.orNull(): Double? = if(this is SuccessDouble) value else null

/**
 * Returns the result, if this is [SuccessDouble]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeDouble<*>.orDefault(defaultValue: Double): Double =
	if(this is SuccessDouble) value else defaultValue

/**
 * Returns the result, if this is [SuccessDouble]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeDouble<Error>.orElse(
	elseSupplier: (Error) -> Double
) : Double {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> value
		is FailureDouble -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessDouble]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeDouble<Error>.orThrow(): Double = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessDouble]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeDouble<Error>.orThrow(message: String): Double = orThrow { message }

/**
 * Returns the result, if this is [SuccessDouble]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureDouble].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeDouble<Error>.orThrow(
	lazyMessage: () -> String
) : Double {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessDouble -> value
		
		is FailureDouble -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureDouble<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureDouble<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeDouble] from a nullable result. If this is not `null`, [SuccessDouble] is returned. If this
 * is `null`, a [FailureDouble] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successDoubleOf
 * @see failureDoubleOf
 * @see asSuccessDouble
 */
@Pure
public inline infix fun <Error> Double?.orFailure(
	errorSupplier: () -> Error
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessDouble() ?: failureDoubleOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessDouble]. If the block throws an exception of the specified
 * [Error] type, a [FailureDouble] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successDoubleOf(parse(input)) } catch(e: IllegalStateException) { failureDoubleOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchDouble<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeDouble], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchDouble<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Double? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchDouble(
	block: () -> Double
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successDoubleOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureDoubleOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessDouble]. If the block throws an exception of the specified
 * [Error] type, a [FailureDouble] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successDoubleOf(parse(input)) } catch(e: IllegalStateException) { failureDoubleOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchDouble<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeDouble], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchDouble<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Double? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchDouble(
	errorConverter: (E) -> Error,
	block: () -> Double
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successDoubleOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureDoubleOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessDouble]. If the block throws an exception of the specified
 * [Error] type, a [FailureDouble] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successDoubleOf(parse(input)) } catch(e: IllegalStateException) { failureDoubleOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchDouble(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeDouble], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchDouble(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Double? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchDouble(
	catchException: KClass<Error>,
	block: () -> Double
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successDoubleOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureDoubleOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successDoubleOf(parse(input)) } catch(e: IllegalStateException) { failureDoubleOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchDouble(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchDouble(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchDouble(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Double
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successDoubleOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureDoubleOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseDouble] that returns a [ConcludeDouble]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseDouble
 * @see catchDouble
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsDouble(
	block: EnclosingContext<Error>.() -> Double
) : Double {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeDouble].
 *
 * Example:
 *
 * ```
 * val value: ConcludeDouble<ArithmeticException> = encloseDouble {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseDouble(block: EnclosingContext<E>.() -> Double): ConcludeDouble<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchDouble<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeDouble].
 *
 * Example:
 *
 * ```
 * val value: ConcludeDouble<ArithmeticException> = encloseDouble(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseDouble(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Double
) : ConcludeDouble<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchDouble(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchDouble
 */
@Pure
public inline fun <Error, E: Exception> encloseDouble(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Double
) : ConcludeDouble<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchDouble(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeDouble].
 *
 * @since 6.0
 * @see Success.unboxDouble
 * @see Failure.unboxDouble
 */
@Pure
public fun <Error> Conclude<Double, Error>.unbox(): ConcludeDouble<Error> = when(this) {
	
	is Success -> successDoubleOf(value)
	is Failure -> failureDoubleOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeDouble].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Double, Error>.unboxDouble(): ConcludeDouble<Error> = when(this) {
	
	is Success -> successDoubleOf(value)
	is Failure -> failureDoubleOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessDouble].
 *
 * @since 6.0
 * @see Conclude.unboxDouble
 * @see Success.unboxDouble
 * @see Failure.unboxDouble
 */
@Pure
public fun Success<Double>.unbox(): SuccessDouble = successDoubleOf(value)

/**
 * Converts a generic [Success] into a [SuccessDouble].
 *
 * @since 6.0
 * @see Conclude.unboxDouble
 * @see Failure.unboxDouble
 */
@Pure
public fun Success<Double>.unboxDouble(): SuccessDouble = successDoubleOf(value)

/**
 * Converts a generic [Failure] into a [FailureDouble].
 *
 * @since 6.0
 * @see Conclude.unboxDouble
 * @see Success.unboxDouble
 */
@Pure
public fun <Error> Failure<Error>.unboxDouble(): FailureDouble<Error> = failureDoubleOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeDouble<*>>.filterSuccessDouble(): List<Double> =
	asSequence().filterSuccessDouble().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeDouble<Error>>.filterFailureDouble(): List<Error> =
	asSequence().filterFailureDouble().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeDouble<*>>.filterSuccessDouble(): Sequence<Double> =
	filterIsInstance<SuccessDouble>().map(SuccessDouble::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeDouble<Error>>.filterFailureDouble(): Sequence<Error> =
	filterIsInstance<FailureDouble<Error>>().map(FailureDouble<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessDouble>.unwrapDouble(
) : List<Double> = map(SuccessDouble::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureDouble<Error>>.unwrapFailureDouble(
) : List<Error> = map(FailureDouble<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessDouble>.unwrapDouble(
) : Sequence<Double> = map(SuccessDouble::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureDouble<Error>>.unwrapFailureDouble(
) : Sequence<Error> = map(FailureDouble<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeDouble<Error>>.mapSuccessDouble(
	resultTransformer: (Double) -> Double
) : List<ConcludeDouble<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeDouble<ErrorIn>>.mapFailureDouble(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeDouble<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeDouble<ErrorIn>>.mapConcludeDouble(
	resultTransformer: (Double) -> Double,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeDouble<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeDouble<Error>>.mapSuccessDouble(
	resultTransformer: (Double) -> Double
) : Sequence<ConcludeDouble<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeDouble<ErrorIn>>.mapFailureDouble(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeDouble<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeDouble<ErrorIn>>.mapConcludeDouble(
	resultTransformer: (Double) -> Double,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeDouble<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeDouble<Error>>.flatMapSuccessDouble(
	resultTransformer: (Double) -> ConcludeDouble<Error>
) : List<ConcludeDouble<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeDouble<ErrorIn>>.flatMapFailureDouble(
	errorTransformer: (ErrorIn) -> ConcludeDouble<ErrorOut>
) : List<ConcludeDouble<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeDouble<ErrorIn>>.flatMapConcludeDouble(
	resultTransformer: (Double) -> ConcludeDouble<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeDouble<ErrorOut>
) : List<ConcludeDouble<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeDouble<Error>>.flatMapSuccessDouble(
	resultTransformer: (Double) -> ConcludeDouble<Error>
) : Sequence<ConcludeDouble<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeDouble<ErrorIn>>.flatMapFailureDouble(
	errorTransformer: (ErrorIn) -> ConcludeDouble<ErrorOut>
) : Sequence<ConcludeDouble<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeDouble<ErrorIn>>.flatMapConcludeDouble(
	resultTransformer: (Double) -> ConcludeDouble<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeDouble<ErrorOut>
) : Sequence<ConcludeDouble<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
