/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessInt] or [failure][FailureInt] of type [Int].
 *
 * Usage example with [failure][FailureInt] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeInt<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Int         ↖ ConcludeInt<Err>          ↖ FailureInt<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessInt
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessInt
 * @see FailureInt
 * @see successIntOf
 * @see failureIntOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeInt<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeInt] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Int, Error>
}


/**
 * Defines a successful [ConcludeInt] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeInt
 * @see FailureInt
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessInt @PublishedApi internal constructor(
	val value: Int
) : ConcludeInt<Nothing>() {
	
	/**
	 * Converts a [SuccessInt] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Int> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeInt] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeInt
 * @see SuccessInt
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureInt<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeInt<Error>() {
	
	/**
	 * Converts a [FailureInt] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxInt
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessInt] instance using the given [value].
 *
 * @since 6.0
 * @see failureIntOf
 * @see asSuccessInt
 * @see asFailureInt
 */
@Pure
public fun successIntOf(value: Int): SuccessInt = SuccessInt(value)

/**
 * Creates a new [SuccessInt] instance using this value.
 *
 * @since 6.0
 * @see successIntOf
 * @see failureIntOf
 * @see asFailureInt
 * @see asSuccessInt
 */
@Pure
public fun Int.asSuccessInt(): SuccessInt = SuccessInt(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureInt] instance using the given [reason].
 *
 * @since 6.0
 * @see successIntOf
 * @see asSuccessInt
 * @see asFailureInt
 */
@Pure
public fun <Error> failureIntOf(reason: Error): FailureInt<Error> = FailureInt(reason)

/**
 * Creates a new [FailureInt] instance using this reason.
 *
 * @since 6.0
 * @see successIntOf
 * @see failureIntOf
 * @see asSuccessInt
 * @see asFailureInt
 */
@Pure
public fun <Error> Error.asFailureInt(): FailureInt<Error> = FailureInt(this)

/**
 * Maps the result of this [ConcludeInt] to another one, if it is [SuccessInt];
 * otherwise, the [FailureInt] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeInt<Error>.map(
	transformer: (Int) -> Int
) : ConcludeInt<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> successIntOf(transformer(value))
		is FailureInt -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeInt] to another one, if it is [SuccessInt];
 * otherwise, the [FailureInt] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeInt<Error>.flatMap(
	transformer: (Int) -> ConcludeInt<Error>
) : ConcludeInt<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> transformer(value)
		is FailureInt -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeInt] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeInt<ErrorIn>.map(
	resultTransformer: (Int) -> Int,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeInt<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> successIntOf(resultTransformer(value))
		is FailureInt -> failureIntOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeInt] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeInt<ErrorIn>.flatMap(
	resultTransformer: (Int) -> ConcludeInt<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeInt<ErrorOut>
) : ConcludeInt<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> resultTransformer(value)
		is FailureInt -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeInt] to another one; otherwise, the [SuccessInt] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureInt
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeInt<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeInt<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> this
		is FailureInt -> failureIntOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessInt] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureInt
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeInt<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeInt<ErrorOut>
) : ConcludeInt<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> this
		is FailureInt -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeInt], if it is [SuccessInt], using the given [resultHandler].
 *
 * @returns This [ConcludeInt].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeInt<Error>.handle(
	resultHandler: (Int) -> Unit
) : ConcludeInt<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessInt)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeInt], using the given handlers.
 *
 * @returns This [ConcludeInt].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeInt<Error>.handle(
	resultHandler: (Int) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeInt<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessInt -> resultHandler(value)
		is FailureInt -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeInt], if it is [FailureInt], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeInt<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeInt<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureInt)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeInt] to handle the result, if it is [SuccessInt].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessInt]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeInt<*>.ifSuccess(
	resultHandler: (Int) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessInt)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeInt] to handle the error, if it is [FailureInt].
 *
 * @return `true`, if this is [FailureInt]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeInt<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureInt)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureInt] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureInt<ErrorIn>.combineWith(
//	other: FailureInt<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureInt<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureIntOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureInt] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureInt<Iterable<Error>>.joinWith(
//	other: FailureInt<Iterable<Error>>
//) : FailureInt<Iterable<Error>> = if(this === other) this else failureIntOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeInt<Err> {
 *
 *     //    ↙ Int
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeInt<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessInt
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeInt<Error>.or(
	failurePropagator: (FailureInt<Error>) -> Nothing
) : Int {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessInt)
		return value
	
	failurePropagator(this as FailureInt)
}

/**
 * Returns the result, if this is [SuccessInt]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeInt<Error>.orNull(): Int? = if(this is SuccessInt) value else null

/**
 * Returns the result, if this is [SuccessInt]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeInt<*>.orDefault(defaultValue: Int): Int =
	if(this is SuccessInt) value else defaultValue

/**
 * Returns the result, if this is [SuccessInt]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeInt<Error>.orElse(
	elseSupplier: (Error) -> Int
) : Int {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> value
		is FailureInt -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessInt]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeInt<Error>.orThrow(): Int = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessInt]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeInt<Error>.orThrow(message: String): Int = orThrow { message }

/**
 * Returns the result, if this is [SuccessInt]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureInt].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeInt<Error>.orThrow(
	lazyMessage: () -> String
) : Int {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessInt -> value
		
		is FailureInt -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureInt<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureInt<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeInt] from a nullable result. If this is not `null`, [SuccessInt] is returned. If this
 * is `null`, a [FailureInt] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successIntOf
 * @see failureIntOf
 * @see asSuccessInt
 */
@Pure
public inline infix fun <Error> Int?.orFailure(
	errorSupplier: () -> Error
) : ConcludeInt<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessInt() ?: failureIntOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessInt]. If the block throws an exception of the specified
 * [Error] type, a [FailureInt] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successIntOf(parse(input)) } catch(e: IllegalStateException) { failureIntOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchInt<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeInt], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchInt<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Int? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchInt(
	block: () -> Int
) : ConcludeInt<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successIntOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureIntOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessInt]. If the block throws an exception of the specified
 * [Error] type, a [FailureInt] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successIntOf(parse(input)) } catch(e: IllegalStateException) { failureIntOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchInt<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeInt], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchInt<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Int? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchInt(
	errorConverter: (E) -> Error,
	block: () -> Int
) : ConcludeInt<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successIntOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureIntOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessInt]. If the block throws an exception of the specified
 * [Error] type, a [FailureInt] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successIntOf(parse(input)) } catch(e: IllegalStateException) { failureIntOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchInt(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeInt], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchInt(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Int? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchInt(
	catchException: KClass<Error>,
	block: () -> Int
) : ConcludeInt<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successIntOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureIntOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successIntOf(parse(input)) } catch(e: IllegalStateException) { failureIntOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchInt(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchInt(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchInt(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Int
) : ConcludeInt<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successIntOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureIntOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseInt] that returns a [ConcludeInt]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseInt
 * @see catchInt
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsInt(
	block: EnclosingContext<Error>.() -> Int
) : Int {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeInt].
 *
 * Example:
 *
 * ```
 * val value: ConcludeInt<ArithmeticException> = encloseInt {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseInt(block: EnclosingContext<E>.() -> Int): ConcludeInt<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchInt<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeInt].
 *
 * Example:
 *
 * ```
 * val value: ConcludeInt<ArithmeticException> = encloseInt(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseInt(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Int
) : ConcludeInt<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchInt(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchInt
 */
@Pure
public inline fun <Error, E: Exception> encloseInt(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Int
) : ConcludeInt<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchInt(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeInt].
 *
 * @since 6.0
 * @see Success.unboxInt
 * @see Failure.unboxInt
 */
@Pure
public fun <Error> Conclude<Int, Error>.unbox(): ConcludeInt<Error> = when(this) {
	
	is Success -> successIntOf(value)
	is Failure -> failureIntOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeInt].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Int, Error>.unboxInt(): ConcludeInt<Error> = when(this) {
	
	is Success -> successIntOf(value)
	is Failure -> failureIntOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessInt].
 *
 * @since 6.0
 * @see Conclude.unboxInt
 * @see Success.unboxInt
 * @see Failure.unboxInt
 */
@Pure
public fun Success<Int>.unbox(): SuccessInt = successIntOf(value)

/**
 * Converts a generic [Success] into a [SuccessInt].
 *
 * @since 6.0
 * @see Conclude.unboxInt
 * @see Failure.unboxInt
 */
@Pure
public fun Success<Int>.unboxInt(): SuccessInt = successIntOf(value)

/**
 * Converts a generic [Failure] into a [FailureInt].
 *
 * @since 6.0
 * @see Conclude.unboxInt
 * @see Success.unboxInt
 */
@Pure
public fun <Error> Failure<Error>.unboxInt(): FailureInt<Error> = failureIntOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeInt<*>>.filterSuccessInt(): List<Int> =
	asSequence().filterSuccessInt().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeInt<Error>>.filterFailureInt(): List<Error> =
	asSequence().filterFailureInt().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeInt<*>>.filterSuccessInt(): Sequence<Int> =
	filterIsInstance<SuccessInt>().map(SuccessInt::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeInt<Error>>.filterFailureInt(): Sequence<Error> =
	filterIsInstance<FailureInt<Error>>().map(FailureInt<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessInt>.unwrapInt(
) : List<Int> = map(SuccessInt::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureInt<Error>>.unwrapFailureInt(
) : List<Error> = map(FailureInt<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessInt>.unwrapInt(
) : Sequence<Int> = map(SuccessInt::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureInt<Error>>.unwrapFailureInt(
) : Sequence<Error> = map(FailureInt<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeInt<Error>>.mapSuccessInt(
	resultTransformer: (Int) -> Int
) : List<ConcludeInt<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeInt<ErrorIn>>.mapFailureInt(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeInt<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeInt<ErrorIn>>.mapConcludeInt(
	resultTransformer: (Int) -> Int,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeInt<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeInt<Error>>.mapSuccessInt(
	resultTransformer: (Int) -> Int
) : Sequence<ConcludeInt<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeInt<ErrorIn>>.mapFailureInt(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeInt<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeInt<ErrorIn>>.mapConcludeInt(
	resultTransformer: (Int) -> Int,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeInt<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeInt<Error>>.flatMapSuccessInt(
	resultTransformer: (Int) -> ConcludeInt<Error>
) : List<ConcludeInt<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeInt<ErrorIn>>.flatMapFailureInt(
	errorTransformer: (ErrorIn) -> ConcludeInt<ErrorOut>
) : List<ConcludeInt<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeInt<ErrorIn>>.flatMapConcludeInt(
	resultTransformer: (Int) -> ConcludeInt<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeInt<ErrorOut>
) : List<ConcludeInt<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeInt<Error>>.flatMapSuccessInt(
	resultTransformer: (Int) -> ConcludeInt<Error>
) : Sequence<ConcludeInt<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeInt<ErrorIn>>.flatMapFailureInt(
	errorTransformer: (ErrorIn) -> ConcludeInt<ErrorOut>
) : Sequence<ConcludeInt<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeInt<ErrorIn>>.flatMapConcludeInt(
	resultTransformer: (Int) -> ConcludeInt<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeInt<ErrorOut>
) : Sequence<ConcludeInt<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
