/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessShort] or [failure][FailureShort] of type [Short].
 *
 * Usage example with [failure][FailureShort] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeShort<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Short         ↖ ConcludeShort<Err>      ↖ FailureShort<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessShort
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessShort
 * @see FailureShort
 * @see successShortOf
 * @see failureShortOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeShort<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeShort] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Short, Error>
}


/**
 * Defines a successful [ConcludeShort] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeShort
 * @see FailureShort
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessShort @PublishedApi internal constructor(
	val value: Short
) : ConcludeShort<Nothing>() {
	
	/**
	 * Converts a [SuccessShort] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Short> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeShort] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeShort
 * @see SuccessShort
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureShort<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeShort<Error>() {
	
	/**
	 * Converts a [FailureShort] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxShort
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessShort] instance using the given [value].
 *
 * @since 6.0
 * @see failureShortOf
 * @see asSuccessShort
 * @see asFailureShort
 */
@Pure
public fun successShortOf(value: Short): SuccessShort = SuccessShort(value)

/**
 * Creates a new [SuccessShort] instance using this value.
 *
 * @since 6.0
 * @see successShortOf
 * @see failureShortOf
 * @see asFailureShort
 * @see asSuccessShort
 */
@Pure
public fun Short.asSuccessShort(): SuccessShort = SuccessShort(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureShort] instance using the given [reason].
 *
 * @since 6.0
 * @see successShortOf
 * @see asSuccessShort
 * @see asFailureShort
 */
@Pure
public fun <Error> failureShortOf(reason: Error): FailureShort<Error> = FailureShort(reason)

/**
 * Creates a new [FailureShort] instance using this reason.
 *
 * @since 6.0
 * @see successShortOf
 * @see failureShortOf
 * @see asSuccessShort
 * @see asFailureShort
 */
@Pure
public fun <Error> Error.asFailureShort(): FailureShort<Error> = FailureShort(this)

/**
 * Maps the result of this [ConcludeShort] to another one, if it is [SuccessShort];
 * otherwise, the [FailureShort] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeShort<Error>.map(
	transformer: (Short) -> Short
) : ConcludeShort<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> successShortOf(transformer(value))
		is FailureShort -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeShort] to another one, if it is [SuccessShort];
 * otherwise, the [FailureShort] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeShort<Error>.flatMap(
	transformer: (Short) -> ConcludeShort<Error>
) : ConcludeShort<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> transformer(value)
		is FailureShort -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeShort] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeShort<ErrorIn>.map(
	resultTransformer: (Short) -> Short,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeShort<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> successShortOf(resultTransformer(value))
		is FailureShort -> failureShortOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeShort] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeShort<ErrorIn>.flatMap(
	resultTransformer: (Short) -> ConcludeShort<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeShort<ErrorOut>
) : ConcludeShort<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> resultTransformer(value)
		is FailureShort -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeShort] to another one; otherwise, the [SuccessShort] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureShort
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeShort<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeShort<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> this
		is FailureShort -> failureShortOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessShort] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureShort
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeShort<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeShort<ErrorOut>
) : ConcludeShort<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> this
		is FailureShort -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeShort], if it is [SuccessShort], using the given [resultHandler].
 *
 * @returns This [ConcludeShort].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeShort<Error>.handle(
	resultHandler: (Short) -> Unit
) : ConcludeShort<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessShort)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeShort], using the given handlers.
 *
 * @returns This [ConcludeShort].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeShort<Error>.handle(
	resultHandler: (Short) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeShort<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessShort -> resultHandler(value)
		is FailureShort -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeShort], if it is [FailureShort], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeShort<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeShort<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureShort)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeShort] to handle the result, if it is [SuccessShort].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessShort]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeShort<*>.ifSuccess(
	resultHandler: (Short) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessShort)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeShort] to handle the error, if it is [FailureShort].
 *
 * @return `true`, if this is [FailureShort]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeShort<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureShort)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureShort] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureShort<ErrorIn>.combineWith(
//	other: FailureShort<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureShort<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureShortOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureShort] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureShort<Iterable<Error>>.joinWith(
//	other: FailureShort<Iterable<Error>>
//) : FailureShort<Iterable<Error>> = if(this === other) this else failureShortOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeShort<Err> {
 *
 *     //    ↙ Short
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeShort<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessShort
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeShort<Error>.or(
	failurePropagator: (FailureShort<Error>) -> Nothing
) : Short {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessShort)
		return value
	
	failurePropagator(this as FailureShort)
}

/**
 * Returns the result, if this is [SuccessShort]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeShort<Error>.orNull(): Short? = if(this is SuccessShort) value else null

/**
 * Returns the result, if this is [SuccessShort]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeShort<*>.orDefault(defaultValue: Short): Short =
	if(this is SuccessShort) value else defaultValue

/**
 * Returns the result, if this is [SuccessShort]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeShort<Error>.orElse(
	elseSupplier: (Error) -> Short
) : Short {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> value
		is FailureShort -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessShort]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeShort<Error>.orThrow(): Short = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessShort]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeShort<Error>.orThrow(message: String): Short = orThrow { message }

/**
 * Returns the result, if this is [SuccessShort]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureShort].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeShort<Error>.orThrow(
	lazyMessage: () -> String
) : Short {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessShort -> value
		
		is FailureShort -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureShort<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureShort<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeShort] from a nullable result. If this is not `null`, [SuccessShort] is returned. If this
 * is `null`, a [FailureShort] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successShortOf
 * @see failureShortOf
 * @see asSuccessShort
 */
@Pure
public inline infix fun <Error> Short?.orFailure(
	errorSupplier: () -> Error
) : ConcludeShort<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessShort() ?: failureShortOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessShort]. If the block throws an exception of the specified
 * [Error] type, a [FailureShort] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successShortOf(parse(input)) } catch(e: IllegalStateException) { failureShortOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchShort<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeShort], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchShort<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Short? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchShort(
	block: () -> Short
) : ConcludeShort<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successShortOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureShortOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessShort]. If the block throws an exception of the specified
 * [Error] type, a [FailureShort] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successShortOf(parse(input)) } catch(e: IllegalStateException) { failureShortOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchShort<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeShort], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchShort<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Short? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchShort(
	errorConverter: (E) -> Error,
	block: () -> Short
) : ConcludeShort<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successShortOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureShortOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessShort]. If the block throws an exception of the specified
 * [Error] type, a [FailureShort] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successShortOf(parse(input)) } catch(e: IllegalStateException) { failureShortOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchShort(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeShort], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchShort(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Short? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchShort(
	catchException: KClass<Error>,
	block: () -> Short
) : ConcludeShort<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successShortOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureShortOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successShortOf(parse(input)) } catch(e: IllegalStateException) { failureShortOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchShort(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchShort(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchShort(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Short
) : ConcludeShort<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successShortOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureShortOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseShort] that returns a [ConcludeShort]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseShort
 * @see catchShort
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsShort(
	block: EnclosingContext<Error>.() -> Short
) : Short {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeShort].
 *
 * Example:
 *
 * ```
 * val value: ConcludeShort<ArithmeticException> = encloseShort {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseShort(block: EnclosingContext<E>.() -> Short): ConcludeShort<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchShort<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeShort].
 *
 * Example:
 *
 * ```
 * val value: ConcludeShort<ArithmeticException> = encloseShort(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseShort(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Short
) : ConcludeShort<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchShort(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchShort
 */
@Pure
public inline fun <Error, E: Exception> encloseShort(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Short
) : ConcludeShort<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchShort(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeShort].
 *
 * @since 6.0
 * @see Success.unboxShort
 * @see Failure.unboxShort
 */
@Pure
public fun <Error> Conclude<Short, Error>.unbox(): ConcludeShort<Error> = when(this) {
	
	is Success -> successShortOf(value)
	is Failure -> failureShortOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeShort].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Short, Error>.unboxShort(): ConcludeShort<Error> = when(this) {
	
	is Success -> successShortOf(value)
	is Failure -> failureShortOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessShort].
 *
 * @since 6.0
 * @see Conclude.unboxShort
 * @see Success.unboxShort
 * @see Failure.unboxShort
 */
@Pure
public fun Success<Short>.unbox(): SuccessShort = successShortOf(value)

/**
 * Converts a generic [Success] into a [SuccessShort].
 *
 * @since 6.0
 * @see Conclude.unboxShort
 * @see Failure.unboxShort
 */
@Pure
public fun Success<Short>.unboxShort(): SuccessShort = successShortOf(value)

/**
 * Converts a generic [Failure] into a [FailureShort].
 *
 * @since 6.0
 * @see Conclude.unboxShort
 * @see Success.unboxShort
 */
@Pure
public fun <Error> Failure<Error>.unboxShort(): FailureShort<Error> = failureShortOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeShort<*>>.filterSuccessShort(): List<Short> =
	asSequence().filterSuccessShort().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeShort<Error>>.filterFailureShort(): List<Error> =
	asSequence().filterFailureShort().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeShort<*>>.filterSuccessShort(): Sequence<Short> =
	filterIsInstance<SuccessShort>().map(SuccessShort::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeShort<Error>>.filterFailureShort(): Sequence<Error> =
	filterIsInstance<FailureShort<Error>>().map(FailureShort<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessShort>.unwrapShort(
) : List<Short> = map(SuccessShort::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureShort<Error>>.unwrapFailureShort(
) : List<Error> = map(FailureShort<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessShort>.unwrapShort(
) : Sequence<Short> = map(SuccessShort::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureShort<Error>>.unwrapFailureShort(
) : Sequence<Error> = map(FailureShort<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeShort<Error>>.mapSuccessShort(
	resultTransformer: (Short) -> Short
) : List<ConcludeShort<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeShort<ErrorIn>>.mapFailureShort(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeShort<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeShort<ErrorIn>>.mapConcludeShort(
	resultTransformer: (Short) -> Short,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeShort<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeShort<Error>>.mapSuccessShort(
	resultTransformer: (Short) -> Short
) : Sequence<ConcludeShort<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeShort<ErrorIn>>.mapFailureShort(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeShort<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeShort<ErrorIn>>.mapConcludeShort(
	resultTransformer: (Short) -> Short,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeShort<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeShort<Error>>.flatMapSuccessShort(
	resultTransformer: (Short) -> ConcludeShort<Error>
) : List<ConcludeShort<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeShort<ErrorIn>>.flatMapFailureShort(
	errorTransformer: (ErrorIn) -> ConcludeShort<ErrorOut>
) : List<ConcludeShort<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeShort<ErrorIn>>.flatMapConcludeShort(
	resultTransformer: (Short) -> ConcludeShort<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeShort<ErrorOut>
) : List<ConcludeShort<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeShort<Error>>.flatMapSuccessShort(
	resultTransformer: (Short) -> ConcludeShort<Error>
) : Sequence<ConcludeShort<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeShort<ErrorIn>>.flatMapFailureShort(
	errorTransformer: (ErrorIn) -> ConcludeShort<ErrorOut>
) : Sequence<ConcludeShort<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeShort<ErrorIn>>.flatMapConcludeShort(
	resultTransformer: (Short) -> ConcludeShort<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeShort<ErrorOut>
) : Sequence<ConcludeShort<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
