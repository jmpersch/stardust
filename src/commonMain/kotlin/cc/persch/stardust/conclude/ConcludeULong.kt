/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessULong] or [failure][FailureULong] of type [ULong].
 *
 * Usage example with [failure][FailureULong] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeULong<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ ULong         ↖ ConcludeULong<Err>      ↖ FailureULong<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessULong
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessULong
 * @see FailureULong
 * @see successULongOf
 * @see failureULongOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeULong<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeULong] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<ULong, Error>
}


/**
 * Defines a successful [ConcludeULong] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeULong
 * @see FailureULong
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessULong @PublishedApi internal constructor(
	val value: ULong
) : ConcludeULong<Nothing>() {
	
	/**
	 * Converts a [SuccessULong] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<ULong> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeULong] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeULong
 * @see SuccessULong
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureULong<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeULong<Error>() {
	
	/**
	 * Converts a [FailureULong] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxULong
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessULong] instance using the given [value].
 *
 * @since 6.0
 * @see failureULongOf
 * @see asSuccessULong
 * @see asFailureULong
 */
@Pure
public fun successULongOf(value: ULong): SuccessULong = SuccessULong(value)

/**
 * Creates a new [SuccessULong] instance using this value.
 *
 * @since 6.0
 * @see successULongOf
 * @see failureULongOf
 * @see asFailureULong
 * @see asSuccessULong
 */
@Pure
public fun ULong.asSuccessULong(): SuccessULong = SuccessULong(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureULong] instance using the given [reason].
 *
 * @since 6.0
 * @see successULongOf
 * @see asSuccessULong
 * @see asFailureULong
 */
@Pure
public fun <Error> failureULongOf(reason: Error): FailureULong<Error> = FailureULong(reason)

/**
 * Creates a new [FailureULong] instance using this reason.
 *
 * @since 6.0
 * @see successULongOf
 * @see failureULongOf
 * @see asSuccessULong
 * @see asFailureULong
 */
@Pure
public fun <Error> Error.asFailureULong(): FailureULong<Error> = FailureULong(this)

/**
 * Maps the result of this [ConcludeULong] to another one, if it is [SuccessULong];
 * otherwise, the [FailureULong] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeULong<Error>.map(
	transformer: (ULong) -> ULong
) : ConcludeULong<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> successULongOf(transformer(value))
		is FailureULong -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeULong] to another one, if it is [SuccessULong];
 * otherwise, the [FailureULong] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeULong<Error>.flatMap(
	transformer: (ULong) -> ConcludeULong<Error>
) : ConcludeULong<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> transformer(value)
		is FailureULong -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeULong] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeULong<ErrorIn>.map(
	resultTransformer: (ULong) -> ULong,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeULong<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> successULongOf(resultTransformer(value))
		is FailureULong -> failureULongOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeULong] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeULong<ErrorIn>.flatMap(
	resultTransformer: (ULong) -> ConcludeULong<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeULong<ErrorOut>
) : ConcludeULong<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> resultTransformer(value)
		is FailureULong -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeULong] to another one; otherwise, the [SuccessULong] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureULong
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeULong<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeULong<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> this
		is FailureULong -> failureULongOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessULong] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureULong
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeULong<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeULong<ErrorOut>
) : ConcludeULong<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> this
		is FailureULong -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeULong], if it is [SuccessULong], using the given [resultHandler].
 *
 * @returns This [ConcludeULong].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeULong<Error>.handle(
	resultHandler: (ULong) -> Unit
) : ConcludeULong<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessULong)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeULong], using the given handlers.
 *
 * @returns This [ConcludeULong].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeULong<Error>.handle(
	resultHandler: (ULong) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeULong<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessULong -> resultHandler(value)
		is FailureULong -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeULong], if it is [FailureULong], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeULong<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeULong<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureULong)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeULong] to handle the result, if it is [SuccessULong].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessULong]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeULong<*>.ifSuccess(
	resultHandler: (ULong) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessULong)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeULong] to handle the error, if it is [FailureULong].
 *
 * @return `true`, if this is [FailureULong]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeULong<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureULong)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureULong] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureULong<ErrorIn>.combineWith(
//	other: FailureULong<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureULong<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureULongOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureULong] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureULong<Iterable<Error>>.joinWith(
//	other: FailureULong<Iterable<Error>>
//) : FailureULong<Iterable<Error>> = if(this === other) this else failureULongOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeULong<Err> {
 *
 *     //    ↙ ULong
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeULong<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessULong
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeULong<Error>.or(
	failurePropagator: (FailureULong<Error>) -> Nothing
) : ULong {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessULong)
		return value
	
	failurePropagator(this as FailureULong)
}

/**
 * Returns the result, if this is [SuccessULong]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeULong<Error>.orNull(): ULong? = if(this is SuccessULong) value else null

/**
 * Returns the result, if this is [SuccessULong]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeULong<*>.orDefault(defaultValue: ULong): ULong =
	if(this is SuccessULong) value else defaultValue

/**
 * Returns the result, if this is [SuccessULong]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeULong<Error>.orElse(
	elseSupplier: (Error) -> ULong
) : ULong {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> value
		is FailureULong -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessULong]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeULong<Error>.orThrow(): ULong = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessULong]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeULong<Error>.orThrow(message: String): ULong = orThrow { message }

/**
 * Returns the result, if this is [SuccessULong]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureULong].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeULong<Error>.orThrow(
	lazyMessage: () -> String
) : ULong {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessULong -> value
		
		is FailureULong -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureULong<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureULong<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeULong] from a nullable result. If this is not `null`, [SuccessULong] is returned. If this
 * is `null`, a [FailureULong] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successULongOf
 * @see failureULongOf
 * @see asSuccessULong
 */
@Pure
public inline infix fun <Error> ULong?.orFailure(
	errorSupplier: () -> Error
) : ConcludeULong<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessULong() ?: failureULongOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessULong]. If the block throws an exception of the specified
 * [Error] type, a [FailureULong] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successULongOf(parse(input)) } catch(e: IllegalStateException) { failureULongOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchULong<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeULong], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchULong<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: ULong? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchULong(
	block: () -> ULong
) : ConcludeULong<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successULongOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureULongOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessULong]. If the block throws an exception of the specified
 * [Error] type, a [FailureULong] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successULongOf(parse(input)) } catch(e: IllegalStateException) { failureULongOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchULong<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeULong], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchULong<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: ULong? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchULong(
	errorConverter: (E) -> Error,
	block: () -> ULong
) : ConcludeULong<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successULongOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureULongOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessULong]. If the block throws an exception of the specified
 * [Error] type, a [FailureULong] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successULongOf(parse(input)) } catch(e: IllegalStateException) { failureULongOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchULong(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeULong], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchULong(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: ULong? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchULong(
	catchException: KClass<Error>,
	block: () -> ULong
) : ConcludeULong<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successULongOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureULongOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successULongOf(parse(input)) } catch(e: IllegalStateException) { failureULongOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchULong(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchULong(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchULong(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> ULong
) : ConcludeULong<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successULongOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureULongOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseULong] that returns a [ConcludeULong]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseULong
 * @see catchULong
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsULong(
	block: EnclosingContext<Error>.() -> ULong
) : ULong {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeULong].
 *
 * Example:
 *
 * ```
 * val value: ConcludeULong<ArithmeticException> = encloseULong {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseULong(block: EnclosingContext<E>.() -> ULong): ConcludeULong<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchULong<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeULong].
 *
 * Example:
 *
 * ```
 * val value: ConcludeULong<ArithmeticException> = encloseULong(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseULong(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> ULong
) : ConcludeULong<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchULong(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchULong
 */
@Pure
public inline fun <Error, E: Exception> encloseULong(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> ULong
) : ConcludeULong<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchULong(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeULong].
 *
 * @since 6.0
 * @see Success.unboxULong
 * @see Failure.unboxULong
 */
@Pure
public fun <Error> Conclude<ULong, Error>.unbox(): ConcludeULong<Error> = when(this) {
	
	is Success -> successULongOf(value)
	is Failure -> failureULongOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeULong].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<ULong, Error>.unboxULong(): ConcludeULong<Error> = when(this) {
	
	is Success -> successULongOf(value)
	is Failure -> failureULongOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessULong].
 *
 * @since 6.0
 * @see Conclude.unboxULong
 * @see Success.unboxULong
 * @see Failure.unboxULong
 */
@Pure
public fun Success<ULong>.unbox(): SuccessULong = successULongOf(value)

/**
 * Converts a generic [Success] into a [SuccessULong].
 *
 * @since 6.0
 * @see Conclude.unboxULong
 * @see Failure.unboxULong
 */
@Pure
public fun Success<ULong>.unboxULong(): SuccessULong = successULongOf(value)

/**
 * Converts a generic [Failure] into a [FailureULong].
 *
 * @since 6.0
 * @see Conclude.unboxULong
 * @see Success.unboxULong
 */
@Pure
public fun <Error> Failure<Error>.unboxULong(): FailureULong<Error> = failureULongOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeULong<*>>.filterSuccessULong(): List<ULong> =
	asSequence().filterSuccessULong().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeULong<Error>>.filterFailureULong(): List<Error> =
	asSequence().filterFailureULong().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeULong<*>>.filterSuccessULong(): Sequence<ULong> =
	filterIsInstance<SuccessULong>().map(SuccessULong::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeULong<Error>>.filterFailureULong(): Sequence<Error> =
	filterIsInstance<FailureULong<Error>>().map(FailureULong<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessULong>.unwrapULong(
) : List<ULong> = map(SuccessULong::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureULong<Error>>.unwrapFailureULong(
) : List<Error> = map(FailureULong<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessULong>.unwrapULong(
) : Sequence<ULong> = map(SuccessULong::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureULong<Error>>.unwrapFailureULong(
) : Sequence<Error> = map(FailureULong<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeULong<Error>>.mapSuccessULong(
	resultTransformer: (ULong) -> ULong
) : List<ConcludeULong<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeULong<ErrorIn>>.mapFailureULong(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeULong<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeULong<ErrorIn>>.mapConcludeULong(
	resultTransformer: (ULong) -> ULong,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeULong<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeULong<Error>>.mapSuccessULong(
	resultTransformer: (ULong) -> ULong
) : Sequence<ConcludeULong<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeULong<ErrorIn>>.mapFailureULong(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeULong<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeULong<ErrorIn>>.mapConcludeULong(
	resultTransformer: (ULong) -> ULong,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeULong<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeULong<Error>>.flatMapSuccessULong(
	resultTransformer: (ULong) -> ConcludeULong<Error>
) : List<ConcludeULong<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeULong<ErrorIn>>.flatMapFailureULong(
	errorTransformer: (ErrorIn) -> ConcludeULong<ErrorOut>
) : List<ConcludeULong<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeULong<ErrorIn>>.flatMapConcludeULong(
	resultTransformer: (ULong) -> ConcludeULong<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeULong<ErrorOut>
) : List<ConcludeULong<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeULong<Error>>.flatMapSuccessULong(
	resultTransformer: (ULong) -> ConcludeULong<Error>
) : Sequence<ConcludeULong<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeULong<ErrorIn>>.flatMapFailureULong(
	errorTransformer: (ErrorIn) -> ConcludeULong<ErrorOut>
) : Sequence<ConcludeULong<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeULong<ErrorIn>>.flatMapConcludeULong(
	resultTransformer: (ULong) -> ConcludeULong<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeULong<ErrorOut>
) : Sequence<ConcludeULong<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
