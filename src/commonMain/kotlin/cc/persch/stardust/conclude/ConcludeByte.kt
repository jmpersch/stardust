/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessByte] or [failure][FailureByte] of type [Byte].
 *
 * Usage example with [failure][FailureByte] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeByte<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Byte         ↖ ConcludeByte<Err>        ↖ FailureByte<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessByte
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessByte
 * @see FailureByte
 * @see successByteOf
 * @see failureByteOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeByte<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeByte] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Byte, Error>
}


/**
 * Defines a successful [ConcludeByte] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeByte
 * @see FailureByte
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessByte @PublishedApi internal constructor(
	val value: Byte
) : ConcludeByte<Nothing>() {
	
	/**
	 * Converts a [SuccessByte] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Byte> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeByte] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeByte
 * @see SuccessByte
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureByte<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeByte<Error>() {
	
	/**
	 * Converts a [FailureByte] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxByte
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessByte] instance using the given [value].
 *
 * @since 6.0
 * @see failureByteOf
 * @see asSuccessByte
 * @see asFailureByte
 */
@Pure
public fun successByteOf(value: Byte): SuccessByte = SuccessByte(value)

/**
 * Creates a new [SuccessByte] instance using this value.
 *
 * @since 6.0
 * @see successByteOf
 * @see failureByteOf
 * @see asFailureByte
 * @see asSuccessByte
 */
@Pure
public fun Byte.asSuccessByte(): SuccessByte = SuccessByte(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureByte] instance using the given [reason].
 *
 * @since 6.0
 * @see successByteOf
 * @see asSuccessByte
 * @see asFailureByte
 */
@Pure
public fun <Error> failureByteOf(reason: Error): FailureByte<Error> = FailureByte(reason)

/**
 * Creates a new [FailureByte] instance using this reason.
 *
 * @since 6.0
 * @see successByteOf
 * @see failureByteOf
 * @see asSuccessByte
 * @see asFailureByte
 */
@Pure
public fun <Error> Error.asFailureByte(): FailureByte<Error> = FailureByte(this)

/**
 * Maps the result of this [ConcludeByte] to another one, if it is [SuccessByte];
 * otherwise, the [FailureByte] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeByte<Error>.map(
	transformer: (Byte) -> Byte
) : ConcludeByte<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> successByteOf(transformer(value))
		is FailureByte -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeByte] to another one, if it is [SuccessByte];
 * otherwise, the [FailureByte] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeByte<Error>.flatMap(
	transformer: (Byte) -> ConcludeByte<Error>
) : ConcludeByte<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> transformer(value)
		is FailureByte -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeByte] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeByte<ErrorIn>.map(
	resultTransformer: (Byte) -> Byte,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeByte<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> successByteOf(resultTransformer(value))
		is FailureByte -> failureByteOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeByte] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeByte<ErrorIn>.flatMap(
	resultTransformer: (Byte) -> ConcludeByte<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeByte<ErrorOut>
) : ConcludeByte<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> resultTransformer(value)
		is FailureByte -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeByte] to another one; otherwise, the [SuccessByte] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureByte
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeByte<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeByte<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> this
		is FailureByte -> failureByteOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessByte] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureByte
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeByte<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeByte<ErrorOut>
) : ConcludeByte<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> this
		is FailureByte -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeByte], if it is [SuccessByte], using the given [resultHandler].
 *
 * @returns This [ConcludeByte].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeByte<Error>.handle(
	resultHandler: (Byte) -> Unit
) : ConcludeByte<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessByte)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeByte], using the given handlers.
 *
 * @returns This [ConcludeByte].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeByte<Error>.handle(
	resultHandler: (Byte) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeByte<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessByte -> resultHandler(value)
		is FailureByte -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeByte], if it is [FailureByte], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeByte<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeByte<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureByte)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeByte] to handle the result, if it is [SuccessByte].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessByte]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeByte<*>.ifSuccess(
	resultHandler: (Byte) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessByte)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeByte] to handle the error, if it is [FailureByte].
 *
 * @return `true`, if this is [FailureByte]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeByte<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureByte)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureByte] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureByte<ErrorIn>.combineWith(
//	other: FailureByte<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureByte<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureByteOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureByte] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureByte<Iterable<Error>>.joinWith(
//	other: FailureByte<Iterable<Error>>
//) : FailureByte<Iterable<Error>> = if(this === other) this else failureByteOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeByte<Err> {
 *
 *     //    ↙ Byte
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeByte<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessByte
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeByte<Error>.or(
	failurePropagator: (FailureByte<Error>) -> Nothing
) : Byte {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessByte)
		return value
	
	failurePropagator(this as FailureByte)
}

/**
 * Returns the result, if this is [SuccessByte]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeByte<Error>.orNull(): Byte? = if(this is SuccessByte) value else null

/**
 * Returns the result, if this is [SuccessByte]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeByte<*>.orDefault(defaultValue: Byte): Byte =
	if(this is SuccessByte) value else defaultValue

/**
 * Returns the result, if this is [SuccessByte]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeByte<Error>.orElse(
	elseSupplier: (Error) -> Byte
) : Byte {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> value
		is FailureByte -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessByte]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeByte<Error>.orThrow(): Byte = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessByte]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeByte<Error>.orThrow(message: String): Byte = orThrow { message }

/**
 * Returns the result, if this is [SuccessByte]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureByte].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeByte<Error>.orThrow(
	lazyMessage: () -> String
) : Byte {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessByte -> value
		
		is FailureByte -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureByte<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureByte<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeByte] from a nullable result. If this is not `null`, [SuccessByte] is returned. If this
 * is `null`, a [FailureByte] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successByteOf
 * @see failureByteOf
 * @see asSuccessByte
 */
@Pure
public inline infix fun <Error> Byte?.orFailure(
	errorSupplier: () -> Error
) : ConcludeByte<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessByte() ?: failureByteOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessByte]. If the block throws an exception of the specified
 * [Error] type, a [FailureByte] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successByteOf(parse(input)) } catch(e: IllegalStateException) { failureByteOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchByte<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeByte], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchByte<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Byte? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchByte(
	block: () -> Byte
) : ConcludeByte<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successByteOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureByteOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessByte]. If the block throws an exception of the specified
 * [Error] type, a [FailureByte] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successByteOf(parse(input)) } catch(e: IllegalStateException) { failureByteOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchByte<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeByte], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchByte<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Byte? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchByte(
	errorConverter: (E) -> Error,
	block: () -> Byte
) : ConcludeByte<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successByteOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureByteOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessByte]. If the block throws an exception of the specified
 * [Error] type, a [FailureByte] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successByteOf(parse(input)) } catch(e: IllegalStateException) { failureByteOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchByte(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeByte], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchByte(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Byte? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchByte(
	catchException: KClass<Error>,
	block: () -> Byte
) : ConcludeByte<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successByteOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureByteOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successByteOf(parse(input)) } catch(e: IllegalStateException) { failureByteOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchByte(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchByte(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchByte(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Byte
) : ConcludeByte<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successByteOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureByteOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseByte] that returns a [ConcludeByte]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseByte
 * @see catchByte
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsByte(
	block: EnclosingContext<Error>.() -> Byte
) : Byte {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeByte].
 *
 * Example:
 *
 * ```
 * val value: ConcludeByte<ArithmeticException> = encloseByte {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseByte(block: EnclosingContext<E>.() -> Byte): ConcludeByte<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchByte<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeByte].
 *
 * Example:
 *
 * ```
 * val value: ConcludeByte<ArithmeticException> = encloseByte(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseByte(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Byte
) : ConcludeByte<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchByte(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchByte
 */
@Pure
public inline fun <Error, E: Exception> encloseByte(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Byte
) : ConcludeByte<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchByte(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeByte].
 *
 * @since 6.0
 * @see Success.unboxByte
 * @see Failure.unboxByte
 */
@Pure
public fun <Error> Conclude<Byte, Error>.unbox(): ConcludeByte<Error> = when(this) {
	
	is Success -> successByteOf(value)
	is Failure -> failureByteOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeByte].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Byte, Error>.unboxByte(): ConcludeByte<Error> = when(this) {
	
	is Success -> successByteOf(value)
	is Failure -> failureByteOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessByte].
 *
 * @since 6.0
 * @see Conclude.unboxByte
 * @see Success.unboxByte
 * @see Failure.unboxByte
 */
@Pure
public fun Success<Byte>.unbox(): SuccessByte = successByteOf(value)

/**
 * Converts a generic [Success] into a [SuccessByte].
 *
 * @since 6.0
 * @see Conclude.unboxByte
 * @see Failure.unboxByte
 */
@Pure
public fun Success<Byte>.unboxByte(): SuccessByte = successByteOf(value)

/**
 * Converts a generic [Failure] into a [FailureByte].
 *
 * @since 6.0
 * @see Conclude.unboxByte
 * @see Success.unboxByte
 */
@Pure
public fun <Error> Failure<Error>.unboxByte(): FailureByte<Error> = failureByteOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeByte<*>>.filterSuccessByte(): List<Byte> =
	asSequence().filterSuccessByte().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeByte<Error>>.filterFailureByte(): List<Error> =
	asSequence().filterFailureByte().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeByte<*>>.filterSuccessByte(): Sequence<Byte> =
	filterIsInstance<SuccessByte>().map(SuccessByte::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeByte<Error>>.filterFailureByte(): Sequence<Error> =
	filterIsInstance<FailureByte<Error>>().map(FailureByte<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessByte>.unwrapByte(
) : List<Byte> = map(SuccessByte::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureByte<Error>>.unwrapFailureByte(
) : List<Error> = map(FailureByte<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessByte>.unwrapByte(
) : Sequence<Byte> = map(SuccessByte::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureByte<Error>>.unwrapFailureByte(
) : Sequence<Error> = map(FailureByte<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeByte<Error>>.mapSuccessByte(
	resultTransformer: (Byte) -> Byte
) : List<ConcludeByte<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeByte<ErrorIn>>.mapFailureByte(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeByte<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeByte<ErrorIn>>.mapConcludeByte(
	resultTransformer: (Byte) -> Byte,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeByte<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeByte<Error>>.mapSuccessByte(
	resultTransformer: (Byte) -> Byte
) : Sequence<ConcludeByte<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeByte<ErrorIn>>.mapFailureByte(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeByte<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeByte<ErrorIn>>.mapConcludeByte(
	resultTransformer: (Byte) -> Byte,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeByte<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeByte<Error>>.flatMapSuccessByte(
	resultTransformer: (Byte) -> ConcludeByte<Error>
) : List<ConcludeByte<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeByte<ErrorIn>>.flatMapFailureByte(
	errorTransformer: (ErrorIn) -> ConcludeByte<ErrorOut>
) : List<ConcludeByte<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeByte<ErrorIn>>.flatMapConcludeByte(
	resultTransformer: (Byte) -> ConcludeByte<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeByte<ErrorOut>
) : List<ConcludeByte<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeByte<Error>>.flatMapSuccessByte(
	resultTransformer: (Byte) -> ConcludeByte<Error>
) : Sequence<ConcludeByte<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeByte<ErrorIn>>.flatMapFailureByte(
	errorTransformer: (ErrorIn) -> ConcludeByte<ErrorOut>
) : Sequence<ConcludeByte<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeByte<ErrorIn>>.flatMapConcludeByte(
	resultTransformer: (Byte) -> ConcludeByte<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeByte<ErrorOut>
) : Sequence<ConcludeByte<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
