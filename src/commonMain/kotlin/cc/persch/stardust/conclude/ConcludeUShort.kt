/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessUShort] or [failure][FailureUShort] of type [UShort].
 *
 * Usage example with [failure][FailureUShort] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeUShort<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ UShort       ↖ ConcludeUShort<Err>      ↖ FailureUShort<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessUShort
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessUShort
 * @see FailureUShort
 * @see successUShortOf
 * @see failureUShortOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeUShort<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeUShort] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<UShort, Error>
}


/**
 * Defines a successful [ConcludeUShort] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeUShort
 * @see FailureUShort
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessUShort @PublishedApi internal constructor(
	val value: UShort
) : ConcludeUShort<Nothing>() {
	
	/**
	 * Converts a [SuccessUShort] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<UShort> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeUShort] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeUShort
 * @see SuccessUShort
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureUShort<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeUShort<Error>() {
	
	/**
	 * Converts a [FailureUShort] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxUShort
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessUShort] instance using the given [value].
 *
 * @since 6.0
 * @see failureUShortOf
 * @see asSuccessUShort
 * @see asFailureUShort
 */
@Pure
public fun successUShortOf(value: UShort): SuccessUShort = SuccessUShort(value)

/**
 * Creates a new [SuccessUShort] instance using this value.
 *
 * @since 6.0
 * @see successUShortOf
 * @see failureUShortOf
 * @see asFailureUShort
 * @see asSuccessUShort
 */
@Pure
public fun UShort.asSuccessUShort(): SuccessUShort = SuccessUShort(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureUShort] instance using the given [reason].
 *
 * @since 6.0
 * @see successUShortOf
 * @see asSuccessUShort
 * @see asFailureUShort
 */
@Pure
public fun <Error> failureUShortOf(reason: Error): FailureUShort<Error> = FailureUShort(reason)

/**
 * Creates a new [FailureUShort] instance using this reason.
 *
 * @since 6.0
 * @see successUShortOf
 * @see failureUShortOf
 * @see asSuccessUShort
 * @see asFailureUShort
 */
@Pure
public fun <Error> Error.asFailureUShort(): FailureUShort<Error> = FailureUShort(this)

/**
 * Maps the result of this [ConcludeUShort] to another one, if it is [SuccessUShort];
 * otherwise, the [FailureUShort] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeUShort<Error>.map(
	transformer: (UShort) -> UShort
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> successUShortOf(transformer(value))
		is FailureUShort -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeUShort] to another one, if it is [SuccessUShort];
 * otherwise, the [FailureUShort] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeUShort<Error>.flatMap(
	transformer: (UShort) -> ConcludeUShort<Error>
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> transformer(value)
		is FailureUShort -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeUShort] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUShort<ErrorIn>.map(
	resultTransformer: (UShort) -> UShort,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeUShort<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> successUShortOf(resultTransformer(value))
		is FailureUShort -> failureUShortOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeUShort] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUShort<ErrorIn>.flatMap(
	resultTransformer: (UShort) -> ConcludeUShort<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUShort<ErrorOut>
) : ConcludeUShort<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> resultTransformer(value)
		is FailureUShort -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeUShort] to another one; otherwise, the [SuccessUShort] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureUShort
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUShort<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeUShort<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> this
		is FailureUShort -> failureUShortOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessUShort] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureUShort
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeUShort<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeUShort<ErrorOut>
) : ConcludeUShort<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> this
		is FailureUShort -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeUShort], if it is [SuccessUShort], using the given [resultHandler].
 *
 * @returns This [ConcludeUShort].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeUShort<Error>.handle(
	resultHandler: (UShort) -> Unit
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessUShort)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeUShort], using the given handlers.
 *
 * @returns This [ConcludeUShort].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeUShort<Error>.handle(
	resultHandler: (UShort) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessUShort -> resultHandler(value)
		is FailureUShort -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeUShort], if it is [FailureUShort], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeUShort<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureUShort)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeUShort] to handle the result, if it is [SuccessUShort].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessUShort]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeUShort<*>.ifSuccess(
	resultHandler: (UShort) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessUShort)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeUShort] to handle the error, if it is [FailureUShort].
 *
 * @return `true`, if this is [FailureUShort]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeUShort<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureUShort)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureUShort] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureUShort<ErrorIn>.combineWith(
//	other: FailureUShort<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureUShort<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureUShortOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureUShort] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureUShort<Iterable<Error>>.joinWith(
//	other: FailureUShort<Iterable<Error>>
//) : FailureUShort<Iterable<Error>> = if(this === other) this else failureUShortOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeUShort<Err> {
 *
 *     //    ↙ UShort
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeUShort<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessUShort
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeUShort<Error>.or(
	failurePropagator: (FailureUShort<Error>) -> Nothing
) : UShort {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessUShort)
		return value
	
	failurePropagator(this as FailureUShort)
}

/**
 * Returns the result, if this is [SuccessUShort]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeUShort<Error>.orNull(): UShort? = if(this is SuccessUShort) value else null

/**
 * Returns the result, if this is [SuccessUShort]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeUShort<*>.orDefault(defaultValue: UShort): UShort =
	if(this is SuccessUShort) value else defaultValue

/**
 * Returns the result, if this is [SuccessUShort]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeUShort<Error>.orElse(
	elseSupplier: (Error) -> UShort
) : UShort {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> value
		is FailureUShort -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessUShort]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeUShort<Error>.orThrow(): UShort = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessUShort]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeUShort<Error>.orThrow(message: String): UShort = orThrow { message }

/**
 * Returns the result, if this is [SuccessUShort]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureUShort].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeUShort<Error>.orThrow(
	lazyMessage: () -> String
) : UShort {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessUShort -> value
		
		is FailureUShort -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureUShort<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureUShort<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeUShort] from a nullable result. If this is not `null`, [SuccessUShort] is returned. If this
 * is `null`, a [FailureUShort] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successUShortOf
 * @see failureUShortOf
 * @see asSuccessUShort
 */
@Pure
public inline infix fun <Error> UShort?.orFailure(
	errorSupplier: () -> Error
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessUShort() ?: failureUShortOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessUShort]. If the block throws an exception of the specified
 * [Error] type, a [FailureUShort] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUShortOf(parse(input)) } catch(e: IllegalStateException) { failureUShortOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUShort<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUShort], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUShort<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UShort? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchUShort(
	block: () -> UShort
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUShortOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureUShortOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessUShort]. If the block throws an exception of the specified
 * [Error] type, a [FailureUShort] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUShortOf(parse(input)) } catch(e: IllegalStateException) { failureUShortOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUShort<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUShort], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUShort<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UShort? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchUShort(
	errorConverter: (E) -> Error,
	block: () -> UShort
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUShortOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureUShortOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessUShort]. If the block throws an exception of the specified
 * [Error] type, a [FailureUShort] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUShortOf(parse(input)) } catch(e: IllegalStateException) { failureUShortOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUShort(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeUShort], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUShort(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: UShort? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchUShort(
	catchException: KClass<Error>,
	block: () -> UShort
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUShortOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureUShortOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successUShortOf(parse(input)) } catch(e: IllegalStateException) { failureUShortOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchUShort(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchUShort(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchUShort(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> UShort
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successUShortOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureUShortOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseUShort] that returns a [ConcludeUShort]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseUShort
 * @see catchUShort
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsUShort(
	block: EnclosingContext<Error>.() -> UShort
) : UShort {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeUShort].
 *
 * Example:
 *
 * ```
 * val value: ConcludeUShort<ArithmeticException> = encloseUShort {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseUShort(block: EnclosingContext<E>.() -> UShort): ConcludeUShort<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchUShort<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeUShort].
 *
 * Example:
 *
 * ```
 * val value: ConcludeUShort<ArithmeticException> = encloseUShort(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseUShort(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> UShort
) : ConcludeUShort<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchUShort(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchUShort
 */
@Pure
public inline fun <Error, E: Exception> encloseUShort(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> UShort
) : ConcludeUShort<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchUShort(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeUShort].
 *
 * @since 6.0
 * @see Success.unboxUShort
 * @see Failure.unboxUShort
 */
@Pure
public fun <Error> Conclude<UShort, Error>.unbox(): ConcludeUShort<Error> = when(this) {
	
	is Success -> successUShortOf(value)
	is Failure -> failureUShortOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeUShort].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<UShort, Error>.unboxUShort(): ConcludeUShort<Error> = when(this) {
	
	is Success -> successUShortOf(value)
	is Failure -> failureUShortOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessUShort].
 *
 * @since 6.0
 * @see Conclude.unboxUShort
 * @see Success.unboxUShort
 * @see Failure.unboxUShort
 */
@Pure
public fun Success<UShort>.unbox(): SuccessUShort = successUShortOf(value)

/**
 * Converts a generic [Success] into a [SuccessUShort].
 *
 * @since 6.0
 * @see Conclude.unboxUShort
 * @see Failure.unboxUShort
 */
@Pure
public fun Success<UShort>.unboxUShort(): SuccessUShort = successUShortOf(value)

/**
 * Converts a generic [Failure] into a [FailureUShort].
 *
 * @since 6.0
 * @see Conclude.unboxUShort
 * @see Success.unboxUShort
 */
@Pure
public fun <Error> Failure<Error>.unboxUShort(): FailureUShort<Error> = failureUShortOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeUShort<*>>.filterSuccessUShort(): List<UShort> =
	asSequence().filterSuccessUShort().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeUShort<Error>>.filterFailureUShort(): List<Error> =
	asSequence().filterFailureUShort().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeUShort<*>>.filterSuccessUShort(): Sequence<UShort> =
	filterIsInstance<SuccessUShort>().map(SuccessUShort::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUShort<Error>>.filterFailureUShort(): Sequence<Error> =
	filterIsInstance<FailureUShort<Error>>().map(FailureUShort<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessUShort>.unwrapUShort(
) : List<UShort> = map(SuccessUShort::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureUShort<Error>>.unwrapFailureUShort(
) : List<Error> = map(FailureUShort<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessUShort>.unwrapUShort(
) : Sequence<UShort> = map(SuccessUShort::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureUShort<Error>>.unwrapFailureUShort(
) : Sequence<Error> = map(FailureUShort<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeUShort<Error>>.mapSuccessUShort(
	resultTransformer: (UShort) -> UShort
) : List<ConcludeUShort<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUShort<ErrorIn>>.mapFailureUShort(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeUShort<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUShort<ErrorIn>>.mapConcludeUShort(
	resultTransformer: (UShort) -> UShort,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeUShort<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUShort<Error>>.mapSuccessUShort(
	resultTransformer: (UShort) -> UShort
) : Sequence<ConcludeUShort<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUShort<ErrorIn>>.mapFailureUShort(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeUShort<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUShort<ErrorIn>>.mapConcludeUShort(
	resultTransformer: (UShort) -> UShort,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeUShort<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeUShort<Error>>.flatMapSuccessUShort(
	resultTransformer: (UShort) -> ConcludeUShort<Error>
) : List<ConcludeUShort<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUShort<ErrorIn>>.flatMapFailureUShort(
	errorTransformer: (ErrorIn) -> ConcludeUShort<ErrorOut>
) : List<ConcludeUShort<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeUShort<ErrorIn>>.flatMapConcludeUShort(
	resultTransformer: (UShort) -> ConcludeUShort<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUShort<ErrorOut>
) : List<ConcludeUShort<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeUShort<Error>>.flatMapSuccessUShort(
	resultTransformer: (UShort) -> ConcludeUShort<Error>
) : Sequence<ConcludeUShort<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUShort<ErrorIn>>.flatMapFailureUShort(
	errorTransformer: (ErrorIn) -> ConcludeUShort<ErrorOut>
) : Sequence<ConcludeUShort<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeUShort<ErrorIn>>.flatMapConcludeUShort(
	resultTransformer: (UShort) -> ConcludeUShort<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeUShort<ErrorOut>
) : Sequence<ConcludeUShort<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
