/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessBoolean] or [failure][FailureBoolean]
 * of type [Boolean].
 *
 * Usage example with [failure][FailureBoolean] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeBoolean<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Boolean       ↖ ConcludeBoolean<Err>    ↖ FailureBoolean<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessBoolean
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessBoolean
 * @see FailureBoolean
 * @see successBooleanOf
 * @see failureBooleanOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeBoolean<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeBoolean] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Boolean, Error>
}

/**
 * Defines a successful [ConcludeBoolean] containing the resulting value.
 * Retrieve instances with values of `true` or `false` via [SuccessBoolean.TRUE]
 * or [SuccessBoolean.FALSE], respectively.
 *
 * @since 6.0
 * @see SuccessBoolean.TRUE
 * @see SuccessBoolean.FALSE
 * @see ConcludeBoolean
 * @see FailureBoolean
 */
@ThreadSafe
public class SuccessBoolean private constructor(
	public val value: Boolean
) : ConcludeBoolean<Nothing>() {
	
	/**
	 * Converts a [SuccessBoolean] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Boolean> = if(value) CACHED_BOXED_TRUE else CACHED_BOXED_FALSE
	
	override fun equals(other: Any?): Boolean {
		
		if(this === other) return true
		if(other == null || this::class != other::class) return false
		
		return value == (other as SuccessBoolean).value
	}
	
	override fun hashCode(): Int = value.hashCode()
	
	override fun toString(): String = if(value) "SuccessBoolean(true)" else "SuccessBoolean(false)"
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
	
	public companion object {
		
		private val CACHED_BOXED_TRUE = successOf(true)
		private val CACHED_BOXED_FALSE = successOf(false)
		
		/**
		 * Gets a [SuccessBoolean] instance with a [value] of `true`.
		 */
		public val TRUE: SuccessBoolean = SuccessBoolean(true)
		
		/**
		 * Gets a [SuccessBoolean] instance with a [value] of `false`.
		 */
		public val FALSE: SuccessBoolean = SuccessBoolean(false)
	}
}

/**
 * Defines a failed [ConcludeBoolean] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeBoolean
 * @see SuccessBoolean
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureBoolean<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeBoolean<Error>() {
	
	/**
	 * Converts a [FailureBoolean] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxBoolean
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessBoolean] instance using the given [value].
 *
 * @since 6.0
 * @see SuccessBoolean.TRUE
 * @see SuccessBoolean.FALSE
 * @see failureBooleanOf
 * @see asSuccessBoolean
 * @see asFailureBoolean
 */
@Cached
@Pure
public fun successBooleanOf(value: Boolean): SuccessBoolean = if(value) SuccessBoolean.TRUE else SuccessBoolean.FALSE

/**
 * Creates a new [SuccessBoolean] instance using this value.
 *
 * @since 6.0
 * @see successBooleanOf
 * @see failureBooleanOf
 * @see asFailureBoolean
 * @see asSuccessBoolean
 */
@Pure
public fun Boolean.asSuccessBoolean(): SuccessBoolean = if(this) SuccessBoolean.TRUE else SuccessBoolean.FALSE

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureBoolean] instance using the given [reason].
 *
 * @since 6.0
 * @see successBooleanOf
 * @see asSuccessBoolean
 * @see asFailureBoolean
 */
@Pure
public fun <Error> failureBooleanOf(reason: Error): FailureBoolean<Error> = FailureBoolean(reason)

/**
 * Creates a new [FailureBoolean] instance using this reason.
 *
 * @since 6.0
 * @see successBooleanOf
 * @see failureBooleanOf
 * @see asSuccessBoolean
 * @see asFailureBoolean
 */
@Pure
public fun <Error> Error.asFailureBoolean(): FailureBoolean<Error> = FailureBoolean(this)

/**
 * Maps the result of this [ConcludeBoolean] to another one, if it is [SuccessBoolean];
 * otherwise, the [FailureBoolean] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeBoolean<Error>.map(
	transformer: (Boolean) -> Boolean
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> successBooleanOf(transformer(value))
		is FailureBoolean -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeBoolean] to another one, if it is [SuccessBoolean];
 * otherwise, the [FailureBoolean] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeBoolean<Error>.flatMap(
	transformer: (Boolean) -> ConcludeBoolean<Error>
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> transformer(value)
		is FailureBoolean -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeBoolean] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeBoolean<ErrorIn>.map(
	resultTransformer: (Boolean) -> Boolean,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeBoolean<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> successBooleanOf(resultTransformer(value))
		is FailureBoolean -> failureBooleanOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeBoolean] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeBoolean<ErrorIn>.flatMap(
	resultTransformer: (Boolean) -> ConcludeBoolean<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeBoolean<ErrorOut>
) : ConcludeBoolean<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> resultTransformer(value)
		is FailureBoolean -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeBoolean] to another one; otherwise, the [SuccessBoolean] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureBoolean
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeBoolean<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeBoolean<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> this
		is FailureBoolean -> failureBooleanOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessBoolean] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureBoolean
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeBoolean<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeBoolean<ErrorOut>
) : ConcludeBoolean<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> this
		is FailureBoolean -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeBoolean], if it is [SuccessBoolean], using the given [resultHandler].
 *
 * @returns This [ConcludeBoolean].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeBoolean<Error>.handle(
	resultHandler: (Boolean) -> Unit
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessBoolean)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeBoolean], using the given handlers.
 *
 * @returns This [ConcludeBoolean].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeBoolean<Error>.handle(
	resultHandler: (Boolean) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessBoolean -> resultHandler(value)
		is FailureBoolean -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeBoolean], if it is [FailureBoolean], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeBoolean<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureBoolean)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeBoolean] to handle the result, if it is [SuccessBoolean].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessBoolean]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeBoolean<*>.ifSuccess(
	resultHandler: (Boolean) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessBoolean)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeBoolean] to handle the error, if it is [FailureBoolean].
 *
 * @return `true`, if this is [FailureBoolean]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeBoolean<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureBoolean)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureBoolean] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureBoolean<ErrorIn>.combineWith(
//	other: FailureBoolean<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureBoolean<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureBooleanOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureBoolean] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureBoolean<Iterable<Error>>.joinWith(
//	other: FailureBoolean<Iterable<Error>>
//) : FailureBoolean<Iterable<Error>> = if(this === other) this else failureBooleanOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeBoolean<Err> {
 *
 *     //    ↙ Boolean
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeBoolean<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessBoolean
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeBoolean<Error>.or(
	failurePropagator: (FailureBoolean<Error>) -> Nothing
) : Boolean {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessBoolean)
		return value
	
	failurePropagator(this as FailureBoolean)
}

/**
 * Returns the result, if this is [SuccessBoolean]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeBoolean<Error>.orNull(): Boolean? = if(this is SuccessBoolean) value else null

/**
 * Returns the result, if this is [SuccessBoolean]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeBoolean<*>.orDefault(defaultValue: Boolean): Boolean =
	if(this is SuccessBoolean) value else defaultValue

/**
 * Returns the result, if this is [SuccessBoolean]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeBoolean<Error>.orElse(
	elseSupplier: (Error) -> Boolean
) : Boolean {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> value
		is FailureBoolean -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessBoolean]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeBoolean<Error>.orThrow(): Boolean = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessBoolean]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeBoolean<Error>.orThrow(message: String): Boolean = orThrow { message }

/**
 * Returns the result, if this is [SuccessBoolean]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureBoolean].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeBoolean<Error>.orThrow(
	lazyMessage: () -> String
) : Boolean {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessBoolean -> value
		
		is FailureBoolean -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureBoolean<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureBoolean<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeBoolean] from a nullable result. If this is not `null`, [SuccessBoolean] is returned. If this
 * is `null`, a [FailureBoolean] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successBooleanOf
 * @see failureBooleanOf
 * @see asSuccessBoolean
 */
@Pure
public inline infix fun <Error> Boolean?.orFailure(
	errorSupplier: () -> Error
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessBoolean() ?: failureBooleanOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessBoolean]. If the block throws an exception of the specified
 * [Error] type, a [FailureBoolean] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successBooleanOf(parse(input)) } catch(e: IllegalStateException) { failureBooleanOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchBoolean<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeBoolean], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchBoolean<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Boolean? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchBoolean(
	block: () -> Boolean
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successBooleanOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureBooleanOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessBoolean]. If the block throws an exception of the specified
 * [Error] type, a [FailureBoolean] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successBooleanOf(parse(input)) } catch(e: IllegalStateException) { failureBooleanOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchBoolean<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeBoolean], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchBoolean<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Boolean? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchBoolean(
	errorConverter: (E) -> Error,
	block: () -> Boolean
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successBooleanOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureBooleanOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessBoolean]. If the block throws an exception of the specified
 * [Error] type, a [FailureBoolean] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successBooleanOf(parse(input)) } catch(e: IllegalStateException) { failureBooleanOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchBoolean(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeBoolean], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchBoolean(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Boolean? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchBoolean(
	catchException: KClass<Error>,
	block: () -> Boolean
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successBooleanOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureBooleanOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successBooleanOf(parse(input)) } catch(e: IllegalStateException) { failureBooleanOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchBoolean(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchBoolean(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchBoolean(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Boolean
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successBooleanOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureBooleanOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseBoolean] that returns a [ConcludeBoolean]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseBoolean
 * @see catchBoolean
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsBoolean(
	block: EnclosingContext<Error>.() -> Boolean
) : Boolean {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeBoolean].
 *
 * Example:
 *
 * ```
 * val value: ConcludeBoolean<ArithmeticException> = encloseBoolean {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseBoolean(block: EnclosingContext<E>.() -> Boolean): ConcludeBoolean<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchBoolean<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeBoolean].
 *
 * Example:
 *
 * ```
 * val value: ConcludeBoolean<ArithmeticException> = encloseBoolean(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseBoolean(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Boolean
) : ConcludeBoolean<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchBoolean(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchBoolean
 */
@Pure
public inline fun <Error, E: Exception> encloseBoolean(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Boolean
) : ConcludeBoolean<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchBoolean(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeBoolean].
 *
 * @since 6.0
 * @see Success.unboxBoolean
 * @see Failure.unboxBoolean
 */
@Pure
public fun <Error> Conclude<Boolean, Error>.unbox(): ConcludeBoolean<Error> = when(this) {
	
	is Success -> successBooleanOf(value)
	is Failure -> failureBooleanOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeBoolean].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Boolean, Error>.unboxBoolean(): ConcludeBoolean<Error> = when(this) {
	
	is Success -> successBooleanOf(value)
	is Failure -> failureBooleanOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessBoolean].
 *
 * @since 6.0
 * @see Conclude.unboxBoolean
 * @see Success.unboxBoolean
 * @see Failure.unboxBoolean
 */
@Pure
public fun Success<Boolean>.unbox(): SuccessBoolean = successBooleanOf(value)

/**
 * Converts a generic [Success] into a [SuccessBoolean].
 *
 * @since 6.0
 * @see Conclude.unboxBoolean
 * @see Failure.unboxBoolean
 */
@Pure
public fun Success<Boolean>.unboxBoolean(): SuccessBoolean = successBooleanOf(value)

/**
 * Converts a generic [Failure] into a [FailureBoolean].
 *
 * @since 6.0
 * @see Conclude.unboxBoolean
 * @see Success.unboxBoolean
 */
@Pure
public fun <Error> Failure<Error>.unboxBoolean(): FailureBoolean<Error> = failureBooleanOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeBoolean<*>>.filterSuccessBoolean(): List<Boolean> =
	asSequence().filterSuccessBoolean().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeBoolean<Error>>.filterFailureBoolean(): List<Error> =
	asSequence().filterFailureBoolean().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeBoolean<*>>.filterSuccessBoolean(): Sequence<Boolean> =
	filterIsInstance<SuccessBoolean>().map(SuccessBoolean::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeBoolean<Error>>.filterFailureBoolean(): Sequence<Error> =
	filterIsInstance<FailureBoolean<Error>>().map(FailureBoolean<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessBoolean>.unwrapBoolean(
) : List<Boolean> = map(SuccessBoolean::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureBoolean<Error>>.unwrapFailureBoolean(
) : List<Error> = map(FailureBoolean<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessBoolean>.unwrapBoolean(
) : Sequence<Boolean> = map(SuccessBoolean::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureBoolean<Error>>.unwrapFailureBoolean(
) : Sequence<Error> = map(FailureBoolean<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeBoolean<Error>>.mapSuccessBoolean(
	resultTransformer: (Boolean) -> Boolean
) : List<ConcludeBoolean<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeBoolean<ErrorIn>>.mapFailureBoolean(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeBoolean<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeBoolean<ErrorIn>>.mapConcludeBoolean(
	resultTransformer: (Boolean) -> Boolean,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeBoolean<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeBoolean<Error>>.mapSuccessBoolean(
	resultTransformer: (Boolean) -> Boolean
) : Sequence<ConcludeBoolean<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeBoolean<ErrorIn>>.mapFailureBoolean(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeBoolean<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeBoolean<ErrorIn>>.mapConcludeBoolean(
	resultTransformer: (Boolean) -> Boolean,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeBoolean<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeBoolean<Error>>.flatMapSuccessBoolean(
	resultTransformer: (Boolean) -> ConcludeBoolean<Error>
) : List<ConcludeBoolean<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeBoolean<ErrorIn>>.flatMapFailureBoolean(
	errorTransformer: (ErrorIn) -> ConcludeBoolean<ErrorOut>
) : List<ConcludeBoolean<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeBoolean<ErrorIn>>.flatMapConcludeBoolean(
	resultTransformer: (Boolean) -> ConcludeBoolean<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeBoolean<ErrorOut>
) : List<ConcludeBoolean<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeBoolean<Error>>.flatMapSuccessBoolean(
	resultTransformer: (Boolean) -> ConcludeBoolean<Error>
) : Sequence<ConcludeBoolean<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeBoolean<ErrorIn>>.flatMapFailureBoolean(
	errorTransformer: (ErrorIn) -> ConcludeBoolean<ErrorOut>
) : Sequence<ConcludeBoolean<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeBoolean<ErrorIn>>.flatMapConcludeBoolean(
	resultTransformer: (Boolean) -> ConcludeBoolean<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeBoolean<ErrorOut>
) : Sequence<ConcludeBoolean<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
