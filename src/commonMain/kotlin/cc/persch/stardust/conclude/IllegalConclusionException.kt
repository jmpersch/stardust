/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.IllegalChanceException
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.getQualifiedName
import cc.persch.stardust.text.rope.rope

/**
 * Thrown on illegal handling of a [Conclude].
 *
 * @since 5.0
 * @see Conclude.orThrow
 */
@ThreadSafe
public class IllegalConclusionException @PublishedApi internal constructor(
	message: String,
	public val reason: Any? = null,
	cause: Throwable? = null
) : IllegalChanceException(createMessage(message, reason), cause) {
	
	internal companion object {
		
		@Pure
		internal fun createMessage(message: String, reason: Any?) = rope {
			
			+message
			
			if(reason != null) {
				
				try {
					
					val reasonString = reason.toString()
					
					-"Failure reason: "
					
					if(reason !is CharSequence && reason !is Fault && reason !is Throwable)
						+"${reason::class.getQualifiedName()}: "
					
					+reasonString
				}
				catch(e: Exception) {
					
					-"FATAL: Could not create message of failure reason! "
					+e.toString()
				}
			}
		}
	}
}
