/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import cc.persch.stardust.`try`
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [success][SuccessLong] or [failure][FailureLong] of type [Long].
 *
 * Usage example with [failure][FailureLong] propagation:
 *
 * ```kotlin
 * fun foo(): ConcludeLong<Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Long         ↖ ConcludeLong<Err>        ↖ FailureLong<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessLong
 * }
 * ```
 *
 * @since 6.0
 * @see SuccessLong
 * @see FailureLong
 * @see successLongOf
 * @see failureLongOf
 */
@ThreadSafe
@MustUse
public sealed class ConcludeLong<out Error>: RopeIndentable {
	
	/**
	 * Converts a [ConcludeLong] into a generic [Conclude].
	 *
	 * @since 6.0
	 * @see Conclude.unbox
	 */
	@Pure
	public abstract fun box(): Conclude<Long, Error>
}


/**
 * Defines a successful [ConcludeLong] containing the resulting value.
 *
 * @since 6.0
 * @see ConcludeLong
 * @see FailureLong
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class SuccessLong @PublishedApi internal constructor(
	val value: Long
) : ConcludeLong<Nothing>() {
	
	/**
	 * Converts a [SuccessLong] into a generic [Success].
	 *
	 * @since 6.0
	 * @see Success.unbox
	 */
	override fun box(): Success<Long> = successOf(value)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [ConcludeLong] containing the error reason.
 *
 * @since 6.0
 * @see ConcludeLong
 * @see SuccessLong
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class FailureLong<out Error> @PublishedApi internal constructor(
	val reason: Error
) : ConcludeLong<Error>() {
	
	/**
	 * Converts a [FailureLong] into a generic [Failure].
	 *
	 * @since 6.0
	 * @see Failure.unboxLong
	 */
	override fun box(): Failure<Error> = failureOf(reason)
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [SuccessLong] instance using the given [value].
 *
 * @since 6.0
 * @see failureLongOf
 * @see asSuccessLong
 * @see asFailureLong
 */
@Pure
public fun successLongOf(value: Long): SuccessLong = SuccessLong(value)

/**
 * Creates a new [SuccessLong] instance using this value.
 *
 * @since 6.0
 * @see successLongOf
 * @see failureLongOf
 * @see asFailureLong
 * @see asSuccessLong
 */
@Pure
public fun Long.asSuccessLong(): SuccessLong = SuccessLong(this)

// GENERATED CODE // DO NOT MANIPULATE! ////////////////////////////////////////////////////////////////////////////////

/**
 * Creates a new [FailureLong] instance using the given [reason].
 *
 * @since 6.0
 * @see successLongOf
 * @see asSuccessLong
 * @see asFailureLong
 */
@Pure
public fun <Error> failureLongOf(reason: Error): FailureLong<Error> = FailureLong(reason)

/**
 * Creates a new [FailureLong] instance using this reason.
 *
 * @since 6.0
 * @see successLongOf
 * @see failureLongOf
 * @see asSuccessLong
 * @see asFailureLong
 */
@Pure
public fun <Error> Error.asFailureLong(): FailureLong<Error> = FailureLong(this)

/**
 * Maps the result of this [ConcludeLong] to another one, if it is [SuccessLong];
 * otherwise, the [FailureLong] is returned.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <Error> ConcludeLong<Error>.map(
	transformer: (Long) -> Long
) : ConcludeLong<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> successLongOf(transformer(value))
		is FailureLong -> this
	}
}

/**
 * Flat-maps the result of this [ConcludeLong] to another one, if it is [SuccessLong];
 * otherwise, the [FailureLong] is returned.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <Error> ConcludeLong<Error>.flatMap(
	transformer: (Long) -> ConcludeLong<Error>
) : ConcludeLong<Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> transformer(value)
		is FailureLong -> this
	}
}

/**
 * Maps the result or the error of this [ConcludeLong] using the given actions.
 *
 * @since 6.0
 * @see flatMap
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeLong<ErrorIn>.map(
	resultTransformer: (Long) -> Long,
	errorTransformer: (ErrorIn) -> ErrorOut
) : ConcludeLong<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> successLongOf(resultTransformer(value))
		is FailureLong -> failureLongOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [ConcludeLong] using the given actions.
 *
 * @since 6.0
 * @see map
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeLong<ErrorIn>.flatMap(
	resultTransformer: (Long) -> ConcludeLong<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeLong<ErrorOut>
) : ConcludeLong<ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> resultTransformer(value)
		is FailureLong -> errorTransformer(reason)
	}
}

/**
 * Maps the error of this [ConcludeLong] to another one; otherwise, the [SuccessLong] is returned.
 *
 * @since 6.0
 * @see flatMapError
 * @see asFailureLong
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeLong<ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : ConcludeLong<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> this
		is FailureLong -> failureLongOf(transformer(reason))
	}
}

/**
 * Flat-maps an error to another one; otherwise, the [SuccessLong] is returned.
 *
 * @since 6.0
 * @see mapError
 * @see asFailureLong
 */
@Pure
public inline fun <ErrorIn, ErrorOut> ConcludeLong<ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> ConcludeLong<ErrorOut>
) : ConcludeLong<ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> this
		is FailureLong -> transformer(reason)
	}
}

/**
 * Handles the result of this [ConcludeLong], if it is [SuccessLong], using the given [resultHandler].
 *
 * @returns This [ConcludeLong].
 * @since 6.0
 * @see handleError
 */
public inline fun <Error> ConcludeLong<Error>.handle(
	resultHandler: (Long) -> Unit
) : ConcludeLong<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is SuccessLong)
		resultHandler(value)
	
	return this
}

/**
 * Handles a result or an error, depending on this [ConcludeLong], using the given handlers.
 *
 * @returns This [ConcludeLong].
 * @since 6.0
 * @see handle
 * @see handleError
 */
public inline fun <Error> ConcludeLong<Error>.handle(
	resultHandler: (Long) -> Unit,
	errorHandler: (Error) -> Unit
) : ConcludeLong<Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is SuccessLong -> resultHandler(value)
		is FailureLong -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [ConcludeLong], if it is [FailureLong], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 6.0
 * @see handle
 */
public inline fun <Error> ConcludeLong<Error>.handleError(
	errorHandler: (Error) -> Unit
) : ConcludeLong<Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is FailureLong)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [ConcludeLong] to handle the result, if it is [SuccessLong].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [SuccessLong]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifFailure
 */
public inline fun ConcludeLong<*>.ifSuccess(
	resultHandler: (Long) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is SuccessLong)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [ConcludeLong] to handle the error, if it is [FailureLong].
 *
 * @return `true`, if this is [FailureLong]; otherwise `false.`
 * @since 6.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> ConcludeLong<Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is FailureLong)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [FailureLong] instances using the given [operation].
// *
// * @since 6.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> FailureLong<ErrorIn>.combineWith(
//	other: FailureLong<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : FailureLong<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureLongOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [FailureLong] with the error collection of another one.
// *
// * @since 6.0
// * @see combineWith
// */
//@Pure
//public fun <Error> FailureLong<Iterable<Error>>.joinWith(
//	other: FailureLong<Iterable<Error>>
//) : FailureLong<Iterable<Error>> = if(this === other) this else failureLongOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): ConcludeLong<Err> {
 *
 *     //    ↙ Long
 *     val foo = getSomethingToConclude() or { return it }
 *     //                  ↖ ConcludeLong<Err>
 *     //                                               ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ SuccessLong
 * }
 * ```
 *
 * @since 6.0
 * @see orDefault
 * @see orElse
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeLong<Error>.or(
	failurePropagator: (FailureLong<Error>) -> Nothing
) : Long {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is SuccessLong)
		return value
	
	failurePropagator(this as FailureLong)
}

/**
 * Returns the result, if this is [SuccessLong]; otherwise, `null` is returned.
 *
 * @since 6.0
 * @see or
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Error> ConcludeLong<Error>.orNull(): Long? = if(this is SuccessLong) value else null

/**
 * Returns the result, if this is [SuccessLong]; otherwise, [defaultValue] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun ConcludeLong<*>.orDefault(defaultValue: Long): Long =
	if(this is SuccessLong) value else defaultValue

/**
 * Returns the result, if this is [SuccessLong]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 6.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Error> ConcludeLong<Error>.orElse(
	elseSupplier: (Error) -> Long
) : Long {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> value
		is FailureLong -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [SuccessLong]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeLong<Error>.orThrow(): Long = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessLong]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Error> ConcludeLong<Error>.orThrow(message: String): Long = orThrow { message }

/**
 * Returns the result, if this is [SuccessLong]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 6.0
 * @throws IllegalConclusionException If this is [FailureLong].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Error> ConcludeLong<Error>.orThrow(
	lazyMessage: () -> String
) : Long {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is SuccessLong -> value
		
		is FailureLong -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureLong<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> FailureLong<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Creates a [ConcludeLong] from a nullable result. If this is not `null`, [SuccessLong] is returned. If this
 * is `null`, a [FailureLong] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successLongOf
 * @see failureLongOf
 * @see asSuccessLong
 */
@Pure
public inline infix fun <Error> Long?.orFailure(
	errorSupplier: () -> Error
) : ConcludeLong<Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccessLong() ?: failureLongOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [SuccessLong]. If the block throws an exception of the specified
 * [Error] type, a [FailureLong] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successLongOf(parse(input)) } catch(e: IllegalStateException) { failureLongOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchLong<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeLong], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchLong<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Long? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <reified Error: Exception> catchLong(
	block: () -> Long
) : ConcludeLong<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successLongOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureLongOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessLong]. If the block throws an exception of the specified
 * [Error] type, a [FailureLong] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successLongOf(parse(input)) } catch(e: IllegalStateException) { failureLongOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchLong<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeLong], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchLong<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Long? = `try`<IllegalStateException> { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Error, reified E: Exception> catchLong(
	errorConverter: (E) -> Error,
	block: () -> Long
) : ConcludeLong<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successLongOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureLongOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [SuccessLong]. If the block throws an exception of the specified
 * [Error] type, a [FailureLong] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successLongOf(parse(input)) } catch(e: IllegalStateException) { failureLongOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchLong(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [ConcludeLong], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchLong(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Long? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error: Exception> catchLong(
	catchException: KClass<Error>,
	block: () -> Long
) : ConcludeLong<Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successLongOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureLongOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val value = try { successLongOf(parse(input)) } catch(e: IllegalStateException) { failureLongOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val value = catchLong(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val value = catchLong(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val value: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Error, E: Exception> catchLong(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Long
) : ConcludeLong<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successLongOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureLongOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment that may throw an exception.
 *
 * Note: This util should be used very rarely. Better use [encloseLong] that returns a [ConcludeLong]
 * instead of throwing an exception.
 *
 * @throws Exception
 * @see encloseLong
 * @see catchLong
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <reified Error: Exception> throwsLong(
	block: EnclosingContext<Error>.() -> Long
) : Long {
	
	contract { callsInPlace(block, EXACTLY_ONCE) }
	
	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeLong].
 *
 * Example:
 *
 * ```
 * val value: ConcludeLong<ArithmeticException> = encloseLong {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <reified E: Exception> encloseLong(block: EnclosingContext<E>.() -> Long): ConcludeLong<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchLong<E> { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [ConcludeLong].
 *
 * Example:
 *
 * ```
 * val value: ConcludeLong<ArithmeticException> = encloseLong(ArithmeticException::class) {
 *
 *     value1 plusExact (value2 timesExact value3)
 * }
 * ```
 *
 * @since 6.0
 */
@Pure
public inline fun <E: Exception> encloseLong(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> Long
) : ConcludeLong<E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catchLong(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * @since 6.0
 * @see catchLong
 */
@Pure
public inline fun <Error, E: Exception> encloseLong(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Long
) : ConcludeLong<Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catchLong(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// BOXING AND UNBOXING /////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Converts a generic [Conclude] into a [ConcludeLong].
 *
 * @since 6.0
 * @see Success.unboxLong
 * @see Failure.unboxLong
 */
@Pure
public fun <Error> Conclude<Long, Error>.unbox(): ConcludeLong<Error> = when(this) {
	
	is Success -> successLongOf(value)
	is Failure -> failureLongOf(reason)
}

/**
 * Converts a generic [Conclude] into a [ConcludeLong].
 *
 * @since 6.0
 */
@Pure
public fun <Error> Conclude<Long, Error>.unboxLong(): ConcludeLong<Error> = when(this) {
	
	is Success -> successLongOf(value)
	is Failure -> failureLongOf(reason)
}

/**
 * Converts a generic [Success] into a [SuccessLong].
 *
 * @since 6.0
 * @see Conclude.unboxLong
 * @see Success.unboxLong
 * @see Failure.unboxLong
 */
@Pure
public fun Success<Long>.unbox(): SuccessLong = successLongOf(value)

/**
 * Converts a generic [Success] into a [SuccessLong].
 *
 * @since 6.0
 * @see Conclude.unboxLong
 * @see Failure.unboxLong
 */
@Pure
public fun Success<Long>.unboxLong(): SuccessLong = successLongOf(value)

/**
 * Converts a generic [Failure] into a [FailureLong].
 *
 * @since 6.0
 * @see Conclude.unboxLong
 * @see Success.unboxLong
 */
@Pure
public fun <Error> Failure<Error>.unboxLong(): FailureLong<Error> = failureLongOf(reason)

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun Iterable<ConcludeLong<*>>.filterSuccessLong(): List<Long> =
	asSequence().filterSuccessLong().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<ConcludeLong<Error>>.filterFailureLong(): List<Error> =
	asSequence().filterFailureLong().toList()


/**
 * @since 6.0
 */
@Pure
public fun Sequence<ConcludeLong<*>>.filterSuccessLong(): Sequence<Long> =
	filterIsInstance<SuccessLong>().map(SuccessLong::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeLong<Error>>.filterFailureLong(): Sequence<Error> =
	filterIsInstance<FailureLong<Error>>().map(FailureLong<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun Iterable<SuccessLong>.unwrapLong(
) : List<Long> = map(SuccessLong::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<FailureLong<Error>>.unwrapFailureLong(
) : List<Error> = map(FailureLong<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun Sequence<SuccessLong>.unwrapLong(
) : Sequence<Long> = map(SuccessLong::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<FailureLong<Error>>.unwrapFailureLong(
) : Sequence<Error> = map(FailureLong<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeLong<Error>>.mapSuccessLong(
	resultTransformer: (Long) -> Long
) : List<ConcludeLong<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeLong<ErrorIn>>.mapFailureLong(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeLong<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeLong<ErrorIn>>.mapConcludeLong(
	resultTransformer: (Long) -> Long,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<ConcludeLong<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeLong<Error>>.mapSuccessLong(
	resultTransformer: (Long) -> Long
) : Sequence<ConcludeLong<Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeLong<ErrorIn>>.mapFailureLong(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeLong<ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeLong<ErrorIn>>.mapConcludeLong(
	resultTransformer: (Long) -> Long,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<ConcludeLong<ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <Error> Iterable<ConcludeLong<Error>>.flatMapSuccessLong(
	resultTransformer: (Long) -> ConcludeLong<Error>
) : List<ConcludeLong<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeLong<ErrorIn>>.flatMapFailureLong(
	errorTransformer: (ErrorIn) -> ConcludeLong<ErrorOut>
) : List<ConcludeLong<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ErrorIn, ErrorOut> Iterable<ConcludeLong<ErrorIn>>.flatMapConcludeLong(
	resultTransformer: (Long) -> ConcludeLong<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeLong<ErrorOut>
) : List<ConcludeLong<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<ConcludeLong<Error>>.flatMapSuccessLong(
	resultTransformer: (Long) -> ConcludeLong<Error>
) : Sequence<ConcludeLong<Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeLong<ErrorIn>>.flatMapFailureLong(
	errorTransformer: (ErrorIn) -> ConcludeLong<ErrorOut>
) : Sequence<ConcludeLong<ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ErrorIn, ErrorOut> Sequence<ConcludeLong<ErrorIn>>.flatMapConcludeLong(
	resultTransformer: (Long) -> ConcludeLong<ErrorOut>,
	errorTransformer: (ErrorIn) -> ConcludeLong<ErrorOut>
) : Sequence<ConcludeLong<ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
