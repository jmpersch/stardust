/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.conclude

import cc.persch.stardust.*
import cc.persch.stardust.annotations.Cached
import cc.persch.stardust.annotations.MustUse
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.result.toConclude
import cc.persch.stardust.text.rope.RopeBuilder
import cc.persch.stardust.text.rope.RopeIndentable
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty
import kotlin.contracts.InvocationKind.*
import kotlin.contracts.contract
import kotlin.reflect.KClass

/**
 * Defines a result to be concluded that is either [Success] or [Failure].
 * 
 * Usage example with [Failure] propagation:
 * 
 * ```kotlin
 * fun foo(): Conclude<Bar, Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Foo          ↖ Conclude<Foo, Err>       ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ Success<Bar>
 * }
 * ```
 *
 * @since 5.0
 * @see Success
 * @see Failure
 * @see successOf
 * @see failureOf
 */
@ThreadSafe
@MustUse
public sealed class Conclude<out Result, out Error>: RopeIndentable

/**
 * Defines a successful [Conclude] containing the resulting value.
 *
 * @since 5.0
 * @see Conclude
 * @see Failure
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class Success<out Result> @PublishedApi internal constructor(
	val value: Result
) : Conclude<Result, Nothing>() {
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::value)
	}
}

/**
 * Defines a failed [Conclude] containing the error reason.
 *
 * @since 5.0
 * @see Conclude
 * @see Success
 */
@ThreadSafe
@ConsistentCopyVisibility
public data class Failure<out Error> @PublishedApi internal constructor(val reason: Error): Conclude<Nothing, Error>() {
	
	override fun ropeIndented(
		builder: RopeBuilder,
		name: CharSequence?,
		bodyOption: BodyOption
	) : Unit = builder.indentCustom(this, name, bodyOption) {
		
		indentProperty(::reason)
	}
}

// UTILS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

private val successUnit = Success(Unit)
private val failureUnit = Failure(Unit)

/**
 * Creates a new [Success] instance using the given [value].
 *
 * Info: There are special implementations available for primitive result types, i.e., [successIntOf].
 *
 * @since 5.0
 * @see failureOf
 * @see asSuccess
 * @see asFailure
 */
@Pure
public fun <Result> successOf(value: Result): Success<Result> = Success(value)

/**
 * Gets a cached [Success] containing [Unit].
 *
 * @since 5.0
 * @see failureOf
 * @see asSuccess
 * @see asFailure
 */
@Suppress("UNUSED_PARAMETER")
@Pure
@Cached
public fun successOf(unit: Unit): Success<Unit> = successUnit

/**
 * Creates a new [Success] instance using this value.
 *
 * Info: There are special implementations available for primitive result types, i.e., [asSuccessInt].
 *
 * @since 5.0
 * @see successOf
 * @see failureOf
 * @see asFailure
 * @see orFailure
 */
@Pure
public fun <Result> Result.asSuccess(): Success<Result> = Success(this)

/**
 * Gets a cached [Success] containing [Unit].
 *
 * @since 5.0
 * @see successOf
 * @see failureOf
 * @see asFailure
 * @see orFailure
 */
@Suppress("UnusedReceiverParameter")
@Cached
@Pure
public fun Unit.asSuccess(): Success<Unit> = successUnit

/**
 * Creates a new [Failure] instance using the given [reason].
 *
 * Info: There are special implementations available for primitive result types, i.e., [failureIntOf].
 *
 * @since 5.0
 * @see successOf
 * @see asSuccess
 * @see asFailure
 */
@Pure
public fun <Error> failureOf(reason: Error): Failure<Error> = Failure(reason)

/**
 * Creates a new [Failure] instance using [Unit] as [reason].
 *
 * @since 5.0
 * @see successOf
 * @see asSuccess
 * @see asFailure
 */
@Cached
@Pure
public fun failureOf(reason: Unit): Failure<Unit> = failureUnit

/**
 * Creates a new [Failure] instance using this reason.
 *
 * Info: There are special implementations available for primitive result types, i.e., [asFailureInt].
 *
 * @since 5.0
 * @see successOf
 * @see failureOf
 * @see asSuccess
 * @see orFailure
 */
@Pure
public fun <Error> Error.asFailure(): Failure<Error> = Failure(this)

/**
 * Creates a new [Failure] instance using [Unit] as reason.
 *
 * @since 5.0
 * @see successOf
 * @see failureOf
 * @see asSuccess
 * @see orFailure
 */
@Cached
@Pure
public fun Unit.asFailure(): Failure<Unit> = failureUnit

/**
 * Maps the result of this [Conclude] to another one, if it is [Success]; otherwise, the [Failure] is returned.
 *
 * @since 5.0
 * @see flatMap
 */
@Pure
public inline fun <ResultIn, ResultOut, Error> Conclude<ResultIn, Error>.map(
	transformer: (ResultIn) -> ResultOut
) : Conclude<ResultOut, Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return if(this is Success) successOf(transformer(value)) else this as Failure
}

/**
 * Flat-maps the result of this [Conclude] to another one, if it is [Success]; otherwise, the [Failure] is returned.
 *
 * @since 5.0
 * @see map
 */
@Pure
public inline fun <ResultIn, ResultOut, Error> Conclude<ResultIn, Error>.flatMap(
	transformer: (ResultIn) -> Conclude<ResultOut, Error>
) : Conclude<ResultOut, Error> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> transformer(value)
		is Failure -> this
	}
}

/**
 * Maps the result or the error of this [Conclude] using the given actions.
 *
 * @since 5.0
 * @see flatMap
 */
@Pure
public inline fun <ResultIn, ResultOut, ErrorIn, ErrorOut> Conclude<ResultIn, ErrorIn>.map(
	resultTransformer: (ResultIn) -> ResultOut,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Conclude<ResultOut, ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> successOf(resultTransformer(value))
		is Failure -> failureOf(errorTransformer(reason))
	}
}

/**
 * Flat-maps the result or the error of this [Conclude] using the given actions.
 *
 * @since 5.0
 * @see map
 */
@Pure
public inline fun <ResultIn, ResultOut, ErrorIn, ErrorOut> Conclude<ResultIn, ErrorIn>.flatMap(
	resultTransformer: (ResultIn) -> Conclude<ResultOut, ErrorOut>,
	errorTransformer: (ErrorIn) -> Conclude<ResultOut, ErrorOut>
) : Conclude<ResultOut, ErrorOut> {
	
	contract { callsInPlace(resultTransformer, AT_MOST_ONCE); callsInPlace(errorTransformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> resultTransformer(value)
		is Failure -> errorTransformer(reason)
	}
}

/**
 * Maps an error of this [Conclude] to another one; otherwise, the [Success] is returned.
 *
 * @since 5.0
 * @see flatMapError
 * @see orFailure
 */
@Pure
public inline fun <Result, ErrorIn, ErrorOut> Conclude<Result, ErrorIn>.mapError(
	transformer: (ErrorIn) -> ErrorOut
) : Conclude<Result, ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> this
		is Failure -> failureOf(transformer(reason))
	}
}

/**
 * Flat-maps an error this [Conclude] to another one; otherwise, the [Success] is returned.
 *
 * @since 5.0
 * @see mapError
 * @see orFailure
 */
@Pure
public inline fun <Result, ErrorIn, ErrorOut> Conclude<Result, ErrorIn>.flatMapError(
	transformer: (ErrorIn) -> Conclude<Result, ErrorOut>
) : Conclude<Result, ErrorOut> {
	
	contract { callsInPlace(transformer, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> this
		is Failure -> transformer(reason)
	}
}

/**
 * Handles the result of this [Conclude], if it is [Success], using the given [resultHandler].
 *
 * @returns This [Conclude].
 * @since 5.0
 * @see handleError
 */
public inline fun <Result, Error> Conclude<Result, Error>.handle(
	resultHandler: (Result) -> Unit
) : Conclude<Result, Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this is Success)
		resultHandler(value)
	
	return this
}

/**
 * Handles the result or the error of this [Conclude] using the given handlers.
 *
 * @returns This [Conclude].
 * @since 5.0
 * @see handleError
 */
public inline fun <Result, Error> Conclude<Result, Error>.handle(
	resultHandler: (Result) -> Unit,
	errorHandler: (Error) -> Unit
) : Conclude<Result, Error> {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE); callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	when(this) {
		
		is Success -> resultHandler(value)
		is Failure -> errorHandler(reason)
	}
	
	return this
}

/**
 * Handles the error of this [Conclude], if it is [Failure], using the given [errorHandler].
 *
 * @returns This [Conclude].
 * @since 5.0
 * @see handle
 */
public inline fun <Result, Error> Conclude<Result, Error>.handleError(
	errorHandler: (Error) -> Unit
) : Conclude<Result, Error> {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this is Failure)
		errorHandler(reason)
	
	return this
}

/**
 * Runs an action on this [Conclude] to handle the result, if it is [Success].
 *
 * Note: This method may be rarely used in some special use cases. In general, it is recommended to
 * [propagate the Failure][or] or to handle it immediately.
 *
 * @return `true`, if this is [Success]; otherwise `false.`
 * @since 5.0
 * @see or
 * @see ifFailure
 */
public inline fun <Result> Conclude<Result, *>.ifSuccess(
	resultHandler: (Result) -> Unit
) : Boolean {
	
	contract { callsInPlace(resultHandler, AT_MOST_ONCE) }
	
	if(this !is Success)
		return false
	
	resultHandler(value)
	
	return true
}

/**
 * Runs an action on this [Conclude] to handle the error, if it is [Failure].
 *
 * @return `true`, if this is [Failure]; otherwise `false.`
 * @since 5.0
 * @see or
 * @see ifSuccess
 */
public inline fun <Error> Conclude<*, Error>.ifFailure(
	errorHandler: (Error) -> Unit
) : Boolean {
	
	contract { callsInPlace(errorHandler, AT_MOST_ONCE) }
	
	if(this !is Failure)
		return false
	
	errorHandler(reason)
	
	return true
}

///**
// * Combines the errors of two [Failure] instances using the given [operation].
// *
// * @since 5.0
// * @see joinWith
// */
//@Pure
//public inline fun <ErrorIn, ErrorOut> Failure<ErrorIn>.combineWith(
//	other: Failure<ErrorIn>,
//	operation: (ErrorIn, ErrorIn) -> ErrorOut
//) : Failure<ErrorOut> {
//
//	contract { callsInPlace(operation, EXACTLY_ONCE) }
//
//	return failureOf(operation(reason, other.reason))
//}
//
///**
// * Joins the error collection of a [Failure] with the error collection of another one.
// *
// * @since 5.0
// * @see combineWith
// */
//@Pure
//public fun <Error> Failure<Iterable<Error>>.joinWith(
//	other: Failure<Iterable<Error>>
//) : Failure<Iterable<Error>> = if(this === other) this else failureOf(reason + other.reason)

/**
 * Delegate the responsibility of handling a possible error to a higher layer using error propagation.
 * The given [failurePropagator] must cancel the program flow.
 *
 * ```kotlin
 * fun foo(): Conclude<Bar, Err> {
 *
 *     val foo = getSomethingToConclude() or { return it }
 *     //    ↖ Foo         ↖ Conclude<Foo, Err>        ↖ Failure<Err>
 *
 *     return doSomethingWith(foo).asSuccess()
 *     //                  ↖ Bar        ↖ Success<Bar>
 * }
 * ```
 *
 * @since 5.0
 * @see orElse
 * @see orDefault
 * @see orNull
 * @see orThrow
 */
@Pure
public inline infix fun <Result, Error> Conclude<Result, Error>.or(
	failurePropagator: (Failure<Error>) -> Nothing
) : Result {
	
	contract { callsInPlace(failurePropagator, AT_MOST_ONCE) }
	
	if(this is Success)
		return value
	
	failurePropagator(this as Failure)
}

/**
 * Returns the result, if this is [Success]; otherwise, `null` is returned.
 *
 * @since 5.0
 * @see or
 * @see orElse
 * @see orDefault
 * @see orThrow
 */
@Pure
public fun <Result, Error> Conclude<Result, Error>.orNull(): Result? = if(this is Success) value else null

/**
 * Returns the result, if this is [Success]; otherwise, [defaultValue] is returned.
 *
 * @since 5.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public fun <Result> Conclude<Result, *>.orDefault(defaultValue: Result): Result =
	if(this is Success) value else defaultValue

/**
 * Returns the result, if this is [Success]; otherwise, the result of the given [elseSupplier] is returned.
 *
 * @since 5.0
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see orThrow
 */
@Pure
public inline infix fun <Result, Error> Conclude<Result, Error>.orElse(
	elseSupplier: (Error) -> Result
) : Result {
	
	contract { callsInPlace(elseSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> value
		is Failure -> elseSupplier(reason)
	}
}

/**
 * Returns the result, if this is [Success]; otherwise, an [IllegalConclusionException] is thrown.
 *
 * @since 5.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Result, Error> Conclude<Result, Error>.orThrow(): Result = orThrow { "Panicked on Failure." }

/**
 * Returns the result, if this is [SuccessDouble]; otherwise,
 * an [IllegalConclusionException] with the [message] is thrown.
 *
 * @since 5.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public fun <Result, Error> Conclude<Result, Error>.orThrow(message: String): Result = orThrow { message }

/**
 * Returns the result, if this is [Success]; otherwise, an [IllegalConclusionException] with the result of
 * [lazyMessage] is thrown.
 *
 * @since 5.0
 * @throws IllegalConclusionException If this is [Failure].
 * @see or
 * @see orNull
 * @see orDefault
 * @see orElse
 * @see throw
 */
@Pure
public inline infix fun <Result, Error> Conclude<Result, Error>.orThrow(
	lazyMessage: () -> String
) : Result {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> value
		
		is Failure -> throw IllegalConclusionException(lazyMessage(), reason, reason as? Throwable)
	}
}

/**
 * Throws an [IllegalConclusionException] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> Failure<Error>.`throw`() : Nothing =
	throw IllegalConclusionException("Panicked on Failure.", reason, reason as? Throwable)

/**
 * Throws an [IllegalConclusionException] with the given [message] containing this failure's [reason][Failure.reason].
 * If the reason implements the [Throwable] interface, it is also specified as [cause][Throwable.cause].
 *
 * @see orThrow
 * @since 6.0
 */
public fun <Error> Failure<Error>.`throw`(message: String) : Nothing =
	throw IllegalConclusionException(message, reason, reason as? Throwable)

/**
 * Converts this [Conclude] into a [kotlin.Result].
 * The [failure reason][Failure.reason] must be of the [Throwable] type.
 *
 * @since 5.0
 * @see Result.toConclude
 */
@Pure
public fun <T> Conclude<T, Throwable>.toResult(): Result<T> = when(this) {
	
	is Success -> Result.success(value)
	is Failure -> Result.failure(reason)
}

/**
 * Converts this [Conclude] into a [Result] using a custom error supplier.
 *
 * @since 5.0
 */
@Pure
public inline fun <T, Error> Conclude<T, Error>.toResult(
	errorSupplier: (Error) -> Throwable
) : Result<T> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return when(this) {
		
		is Success -> Result.success(value)
		is Failure -> Result.failure(errorSupplier(reason))
	}
}

/**
 * Creates a [Conclude] from a nullable result. If this is not `null`, [Success] is returned. If this
 * is `null`, a [Failure] with the reason provided by the [errorSupplier] is returned.
 *
 * If you call this function on a non-nullable receiver, it will always return [Success].
 *
 * @since 5.0
 * @see mapError
 * @see flatMapError
 * @see successOf
 * @see failureOf
 * @see asSuccess
 */
@Pure
public inline infix fun <Result: Any, Error> Result?.orFailure(
	errorSupplier: () -> Error
) : Conclude<Result, Error> {
	
	contract { callsInPlace(errorSupplier, AT_MOST_ONCE) }
	
	return this?.asSuccess() ?: failureOf(errorSupplier())
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val foo = try { successOf(parse(input)) } catch(e: IllegalStateException) { failureOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val foo = `catch`<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val foo = `catch`<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val foo: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * Info: There are special implementations available for primitive result types, i.e., [catchInt].
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 5.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Result, reified Error: Exception> `catch`(
	block: () -> Result
) : Conclude<Result, Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successOf(block())
	}
	catch(e: Exception) {
		
		if(e is Error)
			return failureOf(e)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val foo = try { successOf(parse(input)) } catch(e: IllegalStateException) { failureOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val foo = `catch`<_, IllegalStateException> { parse(input) }
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val foo = `catch`<_, IllegalStateException> { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val foo: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * Info: There are special implementations available for primitive result types, i.e., [catchInt].
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Suppress("LiftReturnOrAssignment")
@Pure
public inline fun <Result, Error, reified E: Exception> `catch`(
	errorConverter: (E) -> Error,
	block: () -> Result
) : Conclude<Result, Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successOf(block())
	}
	catch(e: Exception) {
		
		if(e is E)
			return failureOf(errorConverter(e))
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 *
 * The following code sample …
 *
 * ```
 * val foo = try { successOf(parse(input)) } catch(e: IllegalStateException) { failureOf(e) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val foo = `catch`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val foo = `catch`(IllegalStateException::class) { parse(input) } or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val foo: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * Info: There are special implementations available for primitive result types, i.e., [catchInt].
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 5.0
 * @see try
 */
@Pure
public inline fun <Result, Error: Exception> `catch`(
	catchException: KClass<Error>,
	block: () -> Result
) : Conclude<Result, Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureOf(e as Error)
		
		throw e
	}
}

/**
 * Returns the result of the given [block] as [Success]. If the block throws an exception of the specified
 * [Error] type, a [Failure] containing the exception as reason is returned; otherwise, the exception is rethrown.
 * Use the [errorConverter] to convert the caught exception into another type.
 *
 * The following code sample …
 *
 * ```
 * val foo = try { successOf(parse(input)) } catch(e: IllegalStateException) { failureOf(ParseError(input)) }
 * ```
 *
 * …  can simply be written as:
 *
 * ```
 * val foo = `catch`(IllegalStateException::class, { parse(input) }, { ParseError(input) })
 * ```
 *
 * To easily handle the [Conclude], you can use, for example, the [Failure propagation][or]:
 *
 * ```
 * val foo = `catch`(IllegalStateException::class, { parse(input) }, { ParseError(input) }) or { return it }
 * ```
 *
 * *Hint*: If you do not need information about the caught exception, you can use the [try] util:
 *
 * ```
 * val foo: Foo? = `try`(IllegalStateException::class) { parse(input) }
 * ```
 *
 * Info: There are special implementations available for primitive result types, i.e., [catchInt].
 *
 * @throws Exception If the invocation of the given [block] throws an exception that is not of [Error] type.
 * @since 6.0
 * @see try
 */
@Pure
public inline fun <Result, Error, E: Exception> `catch`(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: () -> Result
) : Conclude<Result, Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	try {
		
		return successOf(block())
	}
	catch(e: Exception) {
		
		@Suppress("UNCHECKED_CAST")
		if(catchException.isInstance(e))
			return failureOf(errorConverter(e as E))
		
		throw e
	}
}

// ENCLOSING ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an enclosing environment to use operations that may throw.
 *
 * Note: This util should be used very rarely. Better use [enclose] that returns a [Conclude].
 *
 * Info: There are special implementations available for primitive result types, i.e., [throwsInt].
 *
 * @throws Exception
 * @see enclose
 * @see catch
 * @see try
 * @since 6.0
 */
@Pure
public inline fun <Result, reified Error: Exception> throws(
	block: EnclosingContext<Error>.() -> Result
) : Result {

	contract { callsInPlace(block, EXACTLY_ONCE) }

	return EnclosingContext.INSTANCE.block()
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * Info: There are special implementations available for primitive result types, i.e., [encloseInt].
 *
 * @since 6.0
 * @see catch
 * @see try
 * @see throws
 */
@Pure
public inline fun <Result, reified Error: Exception> enclose(
	block: EnclosingContext<Error>.() -> Result
) : Conclude<Result, Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catch { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * Info: There are special implementations available for primitive result types, i.e., [encloseInt].
 *
 * @since 6.0
 * @see catch
 * @see try
 * @see throws
 */
@Pure
public inline fun <Result, reified E: Exception, Error> enclose(
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> Result
) : Conclude<Result, Error> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catch(errorConverter) { EnclosingContext.INSTANCE.block() }
}

//@Suppress("UnusedReceiverParameter")
//public inline fun <Result, reified EIn: Exception, EOut: Exception> EnclosingContext<EOut>.enclose(
//	exceptionConverter: (EIn) -> EOut,
//	block: EnclosingContext<EIn>.() -> Result
//) : Result {
//
//	contract { callsInPlace(exceptionConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
//
//	try {
//
//		return EnclosingContext.INSTANCE.block()
//	}
//	catch(e: Exception) {
//
//		if(e is EIn)
//			throw exceptionConverter(e)
//
//		throw e
//	}
//}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * Info: There are special implementations available for primitive result types, i.e., [encloseInt].
 *
 * @since 6.0
 * @see catch
 */
@Pure
public inline fun <R, E: Exception> enclose(
	catchException: KClass<E>,
	block: EnclosingContext<E>.() -> R
) : Conclude<R, E> {
	
	contract { callsInPlace(block, UNKNOWN) }
	
	return catch(catchException) { EnclosingContext.INSTANCE.block() }
}

/**
 * Provides an enclosing environment to combine multiple operations that produce results of [Conclude].
 *
 * Info: There are special implementations available for primitive result types, i.e., [encloseInt].
 *
 * @since 6.0
 * @see catch
 */
@Pure
public inline fun <R, Error, E: Exception> enclose(
	catchException: KClass<E>,
	errorConverter: (E) -> Error,
	block: EnclosingContext<E>.() -> R
) : Conclude<R, Error> {
	
	contract { callsInPlace(errorConverter, AT_MOST_ONCE); callsInPlace(block, UNKNOWN) }
	
	return catch(
		catchException,
		errorConverter
	) { EnclosingContext.INSTANCE.block() }
}

// COLLECTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////////

// FILTER

/**
 * @since 6.0
 */
@Pure
public fun <Result> Iterable<Conclude<Result, *>>.filterSuccess(): List<Result> =
	asSequence().filterSuccess().toList()

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<Conclude<*, Error>>.filterFailure(): List<Error> =
	asSequence().filterFailure().toList()


/**
 * @since 6.0
 */
@Pure
public fun <Result> Sequence<Conclude<Result, *>>.filterSuccess(): Sequence<Result> =
	filterIsInstance<Success<Result>>().map(Success<Result>::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<Conclude<*, Error>>.filterFailure(): Sequence<Error> =
	filterIsInstance<Failure<Error>>().map(Failure<Error>::reason)

// UNWRAP

/**
 * @since 6.0
 */
@Pure
public fun <Result> Iterable<Success<Result>>.unwrap(
) : List<Result> = map(Success<Result>::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Iterable<Failure<Error>>.unwrapFailure(
) : List<Error> = map(Failure<Error>::reason)


/**
 * @since 6.0
 */
@Pure
public fun <Result> Sequence<Success<Result>>.unwrap(
) : Sequence<Result> = map(Success<Result>::value)

/**
 * @since 6.0
 */
@Pure
public fun <Error> Sequence<Failure<Error>>.unwrapFailure(
) : Sequence<Error> = map(Failure<Error>::reason)

// MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <ResultIn, ResultOut, Error> Iterable<Conclude<ResultIn, Error>>.mapSuccess(
	resultTransformer: (ResultIn) -> ResultOut
) : List<Conclude<ResultOut, Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <Result, ErrorIn, ErrorOut> Iterable<Conclude<Result, ErrorIn>>.mapFailure(
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<Conclude<Result, ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ResultIn, ErrorIn, ResultOut, ErrorOut> Iterable<Conclude<ResultIn, ErrorIn>>.mapConclude(
	resultTransformer: (ResultIn) -> ResultOut,
	errorTransformer: (ErrorIn) -> ErrorOut
) : List<Conclude<ResultOut, ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <ResultIn, ResultOut, Error> Sequence<Conclude<ResultIn, Error>>.mapSuccess(
	resultTransformer: (ResultIn) -> ResultOut
) : Sequence<Conclude<ResultOut, Error>> = map { it.map(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <Result, ErrorIn, ErrorOut> Sequence<Conclude<Result, ErrorIn>>.mapFailure(
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<Conclude<Result, ErrorOut>> = map { it.mapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ResultIn, ErrorIn, ResultOut, ErrorOut> Sequence<Conclude<ResultIn, ErrorIn>>.mapConclude(
	resultTransformer: (ResultIn) -> ResultOut,
	errorTransformer: (ErrorIn) -> ErrorOut
) : Sequence<Conclude<ResultOut, ErrorOut>> = map { it.map(resultTransformer, errorTransformer) }

// FLAT MAP

/**
 * @since 6.0
 */
@Pure
public inline fun <ResultIn, ResultOut, Error> Iterable<Conclude<ResultIn, Error>>.flatMapSuccess(
	resultTransformer: (ResultIn) -> Conclude<ResultOut, Error>
) : List<Conclude<ResultOut, Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <Result, ErrorIn, ErrorOut> Iterable<Conclude<Result, ErrorIn>>.flatMapFailure(
	errorTransformer: (ErrorIn) -> Conclude<Result, ErrorOut>
) : List<Conclude<Result, ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public inline fun <ResultIn, ErrorIn, ResultOut, ErrorOut> Iterable<Conclude<ResultIn, ErrorIn>>.flatMapConclude(
	resultTransformer: (ResultIn) -> Conclude<ResultOut, ErrorOut>,
	errorTransformer: (ErrorIn) -> Conclude<ResultOut, ErrorOut>
) : List<Conclude<ResultOut, ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }


/**
 * @since 6.0
 */
@Pure
public fun <ResultIn, ResultOut, Error> Sequence<Conclude<ResultIn, Error>>.flatMapSuccess(
	resultTransformer: (ResultIn) -> Conclude<ResultOut, Error>
) : Sequence<Conclude<ResultOut, Error>> = map { it.flatMap(resultTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <Result, ErrorIn, ErrorOut> Sequence<Conclude<Result, ErrorIn>>.flatMapFailure(
	errorTransformer: (ErrorIn) -> Conclude<Result, ErrorOut>
) : Sequence<Conclude<Result, ErrorOut>> = map { it.flatMapError(errorTransformer) }

/**
 * @since 6.0
 */
@Pure
public fun <ResultIn, ErrorIn, ResultOut, ErrorOut> Sequence<Conclude<ResultIn, ErrorIn>>.flatMapConclude(
	resultTransformer: (ResultIn) -> Conclude<ResultOut, ErrorOut>,
	errorTransformer: (ErrorIn) -> Conclude<ResultOut, ErrorOut>
) : Sequence<Conclude<ResultOut, ErrorOut>> = map { it.flatMap(resultTransformer, errorTransformer) }
