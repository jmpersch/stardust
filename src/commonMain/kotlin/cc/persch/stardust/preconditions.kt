/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.text.isEmptyOrBlank
import cc.persch.stardust.text.isNullOrEmptyOrBlank
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Require
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

internal const val DEFAULT_INVALID_ARGUMENT_EXCEPTION_MESSAGE = "The specified argument is invalid."

@PublishedApi
internal const val UNDEFINED_PARAMETER_NAME: String = "<undefined parameter name>"

/**
 * Throws an [IllegalArgumentException], if the [action] returns `false` on th given [value].
 *
 * @since 4.0
 * @see unfulfilled
 */
@Pure
public inline fun <T: Any> require(parameter: String, value: T, action: (T) -> Boolean) {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	require(parameter, action(value))
}

/**
 * Throws an [IllegalArgumentException], if the required [condition] is `false`.
 *
 * @since 4.0
 * @see unfulfilled
 */
public fun require(parameter: String, condition: Boolean) {
	
	contract { returns() implies condition }
	
	require(parameter, condition) { "" }
}

/**
 * Throws an [IllegalArgumentException], if the [action] returns `false` on th given [value].
 *
 * @since 4.0
 * @see unfulfilled
 */
@Pure
public inline fun <T: Any> require(parameter: String, value: T, action: (T) -> Boolean, lazyMessage: () -> String) {
	
	contract { callsInPlace(action, EXACTLY_ONCE); callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	require(parameter, action(value), lazyMessage)
}

/**
 * Throws an [IllegalArgumentException], if the required [condition] is `false`.
 *
 * @since 4.0
 * @see unfulfilled
 */
public inline fun require(parameter: String, condition: Boolean, lazyMessage: () -> String) {
	
	contract { returns() implies condition; callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	if(!condition)
		unfulfilled(parameter, lazyMessage(), null)
}

/**
 * Throws an [IllegalArgumentException], if the required [value] is `null`.
 *
 * @since 4.0
 * @see unfulfilled
 */
public fun <T: Any> requireNotNull(parameter: String, value: T?): T {
	
	contract { returns() implies (value != null) }
	
	return requireNotNull(parameter, value) { "" }
}

/**
 * Throws an [IllegalArgumentException], if the required [value] is `null`.
 *
 * @since 4.0
 * @see unfulfilled
 */
public inline fun <T: Any> requireNotNull(parameter: String, value: T?, lazyMessage: () -> String): T {
	
	contract { returns() implies (value != null); callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	if(value != null)
		return value
	
	val message = lazyMessage()
	
	throw ArgumentNullException(
		"${if(message.isNullOrEmptyOrBlank()) "The specified argument is null." else message}\n" +
			"Parameter: ${if(parameter.isEmptyOrBlank()) UNDEFINED_PARAMETER_NAME else parameter}"
	)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Unfulfilled
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Throws an [IllegalArgumentException] unconditionally.
 *
 * @since 4.0
 * @see require
 */
public fun unfulfilled(): Nothing = throw IllegalArgumentException()

/**
 * Throws an [IllegalArgumentException] unconditionally with the given [cause].
 *
 * @since 4.0
 * @see require
 */
public fun unfulfilled(cause: Throwable): Nothing = throw IllegalArgumentException(cause)

/**
 * Throws an [IllegalArgumentException] unconditionally with the given [parameter name][parameter].
 *
 * @since 4.0
 * @see require
 */
public fun unfulfilled(parameter: String): Nothing = unfulfilled(parameter, "", null)

/**
 * Throws an [IllegalArgumentException] unconditionally with the given [parameter name][parameter] and [cause].
 *
 * @since 4.0
 * @see require
 */
public fun unfulfilled(parameter: String, cause: Throwable?): Nothing = unfulfilled(parameter, "", cause)

/**
 * Throws an [IllegalArgumentException] unconditionally with the given [parameter name][parameter], [cause],
 * and [message].
 *
 * @since 4.0
 * @see require
 */
public fun unfulfilled(parameter: String, message: String): Nothing = unfulfilled(parameter, message, null)

/**
 * Throws an [IllegalArgumentException] unconditionally with the given [parameter name][parameter], [cause],
 * and [message].
 *
 * @since 4.0
 * @see require
 */
public fun unfulfilled(parameter: String, message: String, cause: Throwable?): Nothing =
	throw IllegalArgumentException(
		"${if(message.isNullOrEmptyOrBlank()) DEFAULT_INVALID_ARGUMENT_EXCEPTION_MESSAGE else message}\n" +
		"Parameter: ${if(parameter.isEmptyOrBlank()) UNDEFINED_PARAMETER_NAME else parameter}",
		cause
	)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Lazy checks
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Throws an [IllegalStateException], if the given [Lazy] instance has not been initialized.
 *
 * @since 4.0
 * @see checkNotInitialized
 * @see check
 */
public inline fun checkInitialized(lazy: Lazy<*>, lazyMessage: () -> Any) {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	check(lazy, true, lazyMessage)
}

/**
 * Throws an [IllegalStateException], if the given [Lazy] instance has already been initialized.
 *
 * @since 4.0
 * @see checkInitialized
 * @see check
 */
public inline fun checkNotInitialized(lazy: Lazy<*>, lazyMessage: () -> Any) {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	check(lazy, false, lazyMessage)
}

/**
 * Throws an [IllegalStateException], if the [initialization state][isInitialized] of the given [Lazy] instance
 * is not equal to [isInitialized].
 *
 * @since 4.0
 * @see checkInitialized
 * @see checkNotInitialized
 */
@Suppress("LEAKED_IN_PLACE_LAMBDA", "WRONG_INVOCATION_KIND")
public inline fun check(lazy: Lazy<*>, isInitialized: Boolean, lazyMessage: () -> Any) {
	
	contract { callsInPlace(lazyMessage, AT_MOST_ONCE) }
	
	check(lazy.isInitialized() == isInitialized, lazyMessage)
}
