/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

/**
 * Provides a more efficient way to iterate over the values of a [CharRange].
 *
 * @since 6.0
 */
public inline fun CharRange.forEachValue(action: (Char) -> Unit) {
	
	for(i in this)
		action(i)
}

/**
 * Provides a more efficient way to iterate over the values of an [IntRange].
 *
 * @since 6.0
 */
public inline fun IntRange.forEachValue(action: (Int) -> Unit) {
	
	for(i in this)
		action(i)
}

/**
 * Provides a more efficient way to iterate over the values of a [LongRange].
 *
 * @since 6.0
 */
public inline fun LongRange.forEachValue(action: (Long) -> Unit) {
	
	for(i in this)
		action(i)
}
