/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure

// Flat Sequences //////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Convenience function for `sequenceOf(sequences).flatten()` to create flat sequences.
 *
 * @since 5.0
 * @see sequenceOf
 * @see Sequence.rem
 */
@Pure
public fun <T> flatSequenceOf(vararg sequences: Sequence<T>): Sequence<T> = sequences.asSequence().flatten()

/**
 * Convenience function for `sequenceOf(iterables).flatten()` to create flat sequences.
 *
 * @since 5.0
 * @see sequenceOf
 * @see Sequence.rem
 */
@Pure
public fun <T> flatSequenceOf(vararg iterables: Iterable<T>): Sequence<T> = iterables.asSequence().flatten()

// Operators ///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns a sequence containing [this] and then all elements of the [other] sequence.
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see Sequence.rem
 */
public operator fun <T> T.rem(other: Sequence<T>): Sequence<T> = sequenceOf(this) + other

/**
 * Returns a sequence containing [this] sequence and then the [other] element.
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see Sequence.rem
 * @see Sequence.plus
 */
public operator fun <T> Sequence<T>.rem(other: T): Sequence<T> = this + sequenceOf(other)

/**
 * Returns a sequence containing [this] and then all elements of the [other] collection.
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see Sequence.rem
 */
public operator fun <T> T.rem(other: Iterable<T>): Sequence<T> = sequenceOf(this) + other

/**
 * Returns a sequence containing [this] collection and then the [other] element.
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see Sequence.rem
 * @see Sequence.plus
 */
public operator fun <T> Iterable<T>.rem(other: T): Sequence<T> = asSequence() + other

/**
 * Returns a sequence containing all elements of [this] sequence and then all elements of the
 * [other] sequence.
 * 
 * In contrast to the plus operator ([Iterable.plus] or [Collection.plus]), this operator always creates a sequence.
 * 
 * Example:
 * 
 * ```
 * val a: List<String> = listOf(123) + listOf(456)
 * //                                 ↖ Collection<T>.plus(…): List<T>
 * //                                   or Iterable<T>.plus(…): List<T>
 * 
 * val b: Sequence<String> = listOf(123).asSequence() + listOf(456)
 * //                                                  ↖ Sequence<T>.plus(…): Sequence<T>
 * 
 * val c: Sequence<String> = listOf(123) % listOf(456)
 * //                                     ↖ Iterable<T>.rem(…): Sequence<T>,
 * //                                       or Sequence<T>.rem(…): Sequence<T>
 * //                                       → Always creates a sequence
 * ```
 * 
 * @since 5.0
 * @see flatSequenceOf
 * @see Sequence.plus
 */
public operator fun <T> Sequence<T>.rem(other: Sequence<T>): Sequence<T> = this + other

/**
 * Returns a sequence containing all elements of [this] sequence and then all elements of the
 * [other] collection.
 *
 * In contrast to the [Iterable.plus] operator, this operator always creates a sequence.
 *
 * Example:
 *
 * ```
 * val a: List<String> = listOf(123) + listOf(456)
 * //                                 ↖ Collection<T>.plus(…): List<T>
 * //                                   or Iterable<T>.plus(…): List<T>
 *
 * val b: Sequence<String> = listOf(123).asSequence() + listOf(456)
 * //                                                  ↖ Sequence<T>.plus(…): Sequence<T>
 *
 * val c: Sequence<String> = listOf(123) % listOf(456)
 * //                                     ↖ Iterable<T>.rem(…): Sequence<T>,
 * //                                       or Sequence<T>.rem(…): Sequence<T>
 * //                                       → Always creates a sequence
 * ```
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see Sequence.plus
 */
public operator fun <T> Sequence<T>.rem(other: Iterable<T>): Sequence<T> = this + other

/**
 * Returns a sequence containing all elements of [this] collection and then all elements of the
 * [other] sequence.
 *
 * In contrast to the [Iterable.plus] operator, this operator always creates a sequence.
 *
 * Example:
 *
 * ```
 * val a: List<String> = listOf(123) + listOf(456)
 * //                                 ↖ Collection<T>.plus(…): List<T>
 * //                                   or Iterable<T>.plus(…): List<T>
 *
 * val b: Sequence<String> = listOf(123).asSequence() + listOf(456)
 * //                                                  ↖ Sequence<T>.plus(…): Sequence<T>
 *
 * val c: Sequence<String> = listOf(123) % listOf(456)
 * //                                     ↖ Iterable<T>.rem(…): Sequence<T>,
 * //                                       or Sequence<T>.rem(…): Sequence<T>
 * //                                       → Always creates a sequence
 * ```
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see asSequence
 */
public operator fun <T> Iterable<T>.rem(other: Sequence<T>): Sequence<T> = asSequence() + other

/**
 * Returns a sequence containing all elements of [this] collection and then all elements of the
 * [other] collection.
 *
 * In contrast to the [Iterable.plus] operator, this operator always creates a sequence.
 *
 * Example:
 *
 * ```
 * val a: List<String> = listOf(123) + listOf(456)
 * //                                 ↖ Collection<T>.plus(…): List<T>
 * //                                   or Iterable<T>.plus(…): List<T>
 *
 * val b: Sequence<String> = listOf(123).asSequence() + listOf(456)
 * //                                                  ↖ Sequence<T>.plus(…): Sequence<T>
 *
 * val c: Sequence<String> = listOf(123) % listOf(456)
 * //                                     ↖ Iterable<T>.rem(…): Sequence<T>,
 * //                                       or Sequence<T>.rem(…): Sequence<T>
 * //                                       → Always creates a sequence
 * ```
 *
 * @since 5.0
 * @see flatSequenceOf
 * @see asSequence
 */
public operator fun <T> Iterable<T>.rem(other: Iterable<T>): Sequence<T> = asSequence() + other
