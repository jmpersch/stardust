/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("NOTHING_TO_INLINE")

package cc.persch.stardust.utils

/**
 * Performs an [and][Boolean.and] operation between *this* [Boolean] and the [other] one.
 *
 * This operator allows replacing this
 *
 * ```
 * changed = changed and set.add(foo)
 * ```
 *
 * by this
 *
 * ```
 * changed *= set.add(foo)
 * ```
 *
 * @since 6.0
 */
public inline operator fun Boolean.times(other: Boolean) = this and other

/**
 * Performs an [or][Boolean.or] operation between *this* [Boolean] and the [other] one.
 *
 * This operator allows replacing this
 *
 * ```
 * changed = changed or set.add(foo)
 * ```
 *
 * by this
 *
 * ```
 * changed += set.add(foo)
 * ```
 *
 * @since 6.0
 */
public inline operator fun Boolean.plus(other: Boolean) = this or other

/**
 * Performs an [xor][Boolean.xor] operation between *this* [Boolean] and the [other] one.
 *
 * This operator allows replacing this
 *
 * ```
 * changed = changed xor set.add(foo)
 * ```
 *
 * by this
 *
 * ```
 * changed %= set.add(foo)
 * ```
 *
 * @since 6.0
 */
public inline operator fun Boolean.rem(other: Boolean) = this xor other
