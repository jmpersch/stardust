/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.utils

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.ShouldUse
import cc.persch.stardust.exert
import cc.persch.stardust.require

/**
 * Defines a byte array buffer for manipulating an underlying byte array, like Java's `ByteBuffer` does,
 * but without the overhead of the native API.
 *
 * @since 6.0
 */
public class ByteArrayBuffer internal constructor(public val array: ByteArray) {
	
	/**
	 * Gets the size of the underlying [array].
	 */
	public val size: Int
		get() = array.size
	
	private var _position: Int = 0
	
	/**
	 * Gets or sets the position.
	 *
	 * @throws IllegalArgumentException The given value is out of range.
	 */
	public var position: Int
		get() = _position
		set(value) {
			
			require(value in 0..array.size)
			
			_position = value
		}
	
	/**
	 * Gets a byte at the given [index].
	 *
	 * @throws IndexOutOfBoundsException The given index is out of bounds.
	 */
	public operator fun get(index: Int): Byte = array[index]
	
	/**
	 * Sets the [value] at the given [index].
	 *
	 * @throws IndexOutOfBoundsException The given index is out of bounds.
	 */
	public operator fun set(index: Int, value: Byte) {
		
		array[index] = value
	}
	
	// region Get Values
	
	@ShouldUse
	public fun getBytes(length: Int): ByteArray {
		
		require("length", length >= 0) { "The given count is out of range." }
		
		return ByteArray(length) exert { getBytes(it, 0, length) }
	}
	
	@ShouldUse
	public fun getBytes(dst: ByteArray, length: Int): Unit = getBytes(dst, 0, length)
	
	@ShouldUse
	public fun getBytes(dst: ByteArray, offset: Int, length: Int) {
		
		require("offset", offset in dst.indices) { "The given offset is out of range." }
		require("length", length in 0..(dst.size - offset)) { "The given length is out of range." }
		
		val array = array
		val index = _position
		
		val newIndex = index + length
		
		check(newIndex <= array.size) { "Buffer overrun" }
		
		for(i in 0..<length)
			dst[offset + i] = array[index + i]
		
		_position = newIndex
	}
	
	@ShouldUse
	public fun getByte(): Byte {
		
		val array = array
		val index = _position
		
		val newIndex = index + 1
		
		check(newIndex <= array.size) { "Buffer overrun" }
		
		val value = array[index]
		
		_position = newIndex
		
		return value
	}
	
	@ShouldUse
	public fun getUShort(): UShort = getShort().toUShort()
	
	@ShouldUse
	public fun getShort(): Short {
		
		val array = array
		val index = _position
		
		val newIndex = index + 2
		
		check(newIndex <= array.size) { "Buffer overrun" }
		
		var value = (array[index].toInt() and 0xff shl 8)
		
		value = value or (array[index + 1].toInt() and 0xff)
		
		_position = newIndex
		
		return value.toShort()
	}
	
	@ShouldUse
	public fun getUInt(): UInt = getInt().toUInt()
	
	@ShouldUse
	public fun getInt(): Int {
		
		val array = array
		val index = _position
		
		val newIndex = index + 4
		
		check(newIndex <= array.size) { "Buffer overrun" }
		
		var value = (array[index].toInt() and 0xff shl 24)
		
		value = value or (array[index + 1].toInt() and 0xff shl 16)
		value = value or (array[index + 2].toInt() and 0xff shl 8)
		value = value or (array[index + 3].toInt() and 0xff)
		
		_position = newIndex
		
		return value
	}
	
	@ShouldUse
	public fun getULong(): ULong = getLong().toULong()
	
	@ShouldUse
	public fun getLong(): Long {
		
		val array = array
		val index = _position
		
		val newIndex = index + 8
		
		check(newIndex <= array.size) { "Buffer overrun" }
		
		var value = (array[index].toLong() and 0xff shl 56)
		
		value = value or (array[index + 1].toLong() and 0xff shl 48)
		value = value or (array[index + 2].toLong() and 0xff shl 40)
		value = value or (array[index + 3].toLong() and 0xff shl 32)
		value = value or (array[index + 4].toLong() and 0xff shl 24)
		value = value or (array[index + 5].toLong() and 0xff shl 16)
		value = value or (array[index + 6].toLong() and 0xff shl 8)
		value = value or (array[index + 7].toLong() and 0xff)
		
		_position = newIndex
		
		return value
	}
	
	// endregion /Get Values/
	
	// region Put Values
	
	public fun putBytes(src: ByteArray): Unit = putBytes(src, 0, src.size)
	
	public fun putBytes(src: ByteArray, length: Int): Unit = putBytes(src, 0, length)
	
	public fun putBytes(src: ByteArray, offset: Int, length: Int) {
		
		require("offset", offset in src.indices) { "The given offset is out of range." }
		require("length", length in 0..(src.size - offset)) { "The given length is out of range." }
		
		val array = array
		val index = _position
		
		val newIndex = index + length
		
		check(newIndex <= array.size) { "Buffer overflow" }
		
		for(i in 0..<length)
			array[index + i] = src[offset + i]
		
		_position = newIndex
	}
	
	public fun putByte(value: Byte) {
		
		val array = array
		val index = _position
		
		val newIndex = index + 1
		
		check(newIndex <= array.size) { "Buffer overflow" }
		
		array[index] = value
		
		_position = newIndex
	}
	
	public fun putUShort(value: UShort): Unit = putShort(value.toShort())
	
	public fun putShort(value: Short) {
		
		val array = array
		val index = _position
		
		val newIndex = index + 2
		
		check(newIndex <= array.size)
		
		array[index    ] = (value.toInt() ushr  8 and 0xff).toByte()
		array[index + 1] = (value.toInt()         and 0xff).toByte()
		
		_position = newIndex
	}
	
	public fun putUInt(value: UInt): Unit = putInt(value.toInt())
	
	public fun putInt(value: Int) {
		
		val array = array
		val index = _position
		
		val newIndex = index + 4
		
		check(newIndex <= array.size) { "Buffer overflow" }
		
		array[index    ] = (value ushr 24 and 0xff).toByte()
		array[index + 1] = (value ushr 16 and 0xff).toByte()
		array[index + 2] = (value ushr  8 and 0xff).toByte()
		array[index + 3] = (value         and 0xff).toByte()
		
		_position = newIndex
	}
	
	public fun putULong(value: ULong): Unit = putLong(value.toLong())
	
	public fun putLong(value: Long) {
		
		val array = array
		val index = _position
		
		val newIndex = index + 8
		
		check(newIndex <= array.size) { "Buffer overflow" }
		
		array[index    ] = (value ushr 56 and 0xff).toByte()
		array[index + 1] = (value ushr 48 and 0xff).toByte()
		array[index + 2] = (value ushr 40 and 0xff).toByte()
		array[index + 3] = (value ushr 32 and 0xff).toByte()
		array[index + 4] = (value ushr 24 and 0xff).toByte()
		array[index + 5] = (value ushr 16 and 0xff).toByte()
		array[index + 6] = (value ushr  8 and 0xff).toByte()
		array[index + 7] = (value         and 0xff).toByte()
		
		_position = newIndex
	}
	
	// endregion /Put Values/
	
	override fun toString(): String = "${array.asList()} @ $_position"
	
	public companion object {
		
		@Pure
		public fun wrap(array: ByteArray): ByteArrayBuffer = ByteArrayBuffer(array)
		
		@Pure
		public fun allocate(capacity: Int): ByteArrayBuffer {
			
			require("capacity", capacity > 0)
			
			return ByteArrayBuffer(ByteArray(capacity))
		}
	}
}
