/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("NOTHING_TO_INLINE")

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure

/**
 * Checks, whether this contains all bits of [other].
 *
 * ```
 * (this and other) == other
 * ```
 *
 * @return `true`, if this contains all bits of [other].
 * @since 1.0
 */
@Pure
public inline fun Byte.containsAllBitsOf(other: Byte): Boolean = (this.toInt() and other.toInt()) == other.toInt()

/**
 * Checks, whether this contains at least one bit of [other].
 *
 * ```
 * (this and other) != 0
 * ```
 *
 * @return `true`, if this contains any bit of [other].
 * @since 1.0
 */
@Pure
public inline fun Byte.containsAnyBitOf(other: Byte): Boolean = (this.toInt() and other.toInt()) != 0

/**
 * Checks, whether this contains all bits of [other].
 *
 * ```
 * (this and other) == other
 * ```
 *
 * @return `true`, if this contains all bits of [other].
 * @since 1.0
 */
@Pure
public inline fun Short.containsAllBitsOf(other: Short): Boolean = (this.toInt() and other.toInt()) == other.toInt()

/**
 * Checks, whether this contains at least one bit of [other].
 *
 * ```
 * (this and other) != 0
 * ```
 *
 * @return `true`, if this contains any bit of [other].
 * @since 1.0
 */
@Pure
public inline fun Short.containsAnyBitOf(other: Short): Boolean = (this.toInt() and other.toInt()) != 0

/**
 * Checks, whether this contains all bits of [other].
 *
 * ```
 * (this and other) == other
 * ```
 *
 * @return `true`, if this contains all bits of [other].
 * @since 1.0
 */
@Pure
public inline fun Int.containsAllBitsOf(other: Int): Boolean = (this and other) == other

/**
 * Checks, whether this contains at least one bit of [other].
 *
 * ```
 * (this and other) != 0
 * ```
 *
 * @return `true`, if this contains any bit of [other].
 * @since 1.0
 */
@Pure
public inline fun Int.containsAnyBitOf(other: Int): Boolean = (this and other) != 0


/**
 * Checks, whether this contains all bits of [other].
 *
 * ```
 * (this and other) == other
 * ```
 *
 * @return `true`, if this contains all bits of [other].
 * @since 1.0
 */
@Pure
public inline fun Long.containsAllBitsOf(other: Long): Boolean = (this and other) == other

/**
 * Checks, whether this contains at least one bit of [other].
 *
 * ```
 * (this and other) != 0L
 * ```
 *
 * @return `true`, if this contains any bit of [other].
 * @since 1.0
 */
@Pure
public inline fun Long.containsAnyBitOf(other: Long): Boolean = (this and other) != 0L
