/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.text.isNullOrEmptyOrBlank

/**
 * Thrown to indicate that a method has been passed a `null` reference as argument.
 * 
 * @since 1.0
 *
 * @constructor Creates a new instance.
 * @param message A message.
 * @param cause A cause.
 */
public open class ArgumentNullException(
	message: String? = null,
	cause: Throwable? = null
) : IllegalArgumentException(if(message.isNullOrEmptyOrBlank()) "The specified argument is null." else message, cause)
