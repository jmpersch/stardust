/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust

import cc.persch.stardust.annotations.Pure
import kotlin.contracts.InvocationKind.UNKNOWN
import kotlin.contracts.contract

/**
 * Repeats the given [action] on the elements of this progression.
 */
public inline fun IntProgression.repeat(action: (Int) -> Unit) {
	
	contract { callsInPlace(action, UNKNOWN) }
	
	for(element in this)
		action(element)
}

/**
 * Repeats the given [action] on the elements of this progression.
 */
public inline fun LongProgression.repeat(action: (Long) -> Unit) {
	
	contract { callsInPlace(action, UNKNOWN) }
	
	for(element in this)
		action(element)
}

/**
 * Gets the length of this progression.
 */
@Pure
public fun IntProgression.length(): Int = (last - first) / step + 1

/**
 * Gets the length of this progression.
 */
@Pure
public fun LongProgression.length(): Long = (last - first) / step + 1L
