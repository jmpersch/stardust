/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

@file:Suppress("unused")

package cc.persch.stardust

import cc.persch.stardust.annotations.ShouldUse
import kotlin.contracts.InvocationKind.AT_MOST_ONCE
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Invokes the specified [action] with this value as its receiver and returns this value.
 *
 *  ***Example for "exerting" vs. "piping":***
 *
 * ```
 * //                               ↙ call via receiver (here: implicit "this")
 * mutableListOf(1, 2) exerting { add(3) } perform ::println // prints "[1, 2, 3]"
 *
 * mutableListOf(1, 2) piping   { add(3) } perform ::println // prints "true"
 * ```
 * @since 4.0
 * @see exert
 * @see piping
 * @see pipe
 * @see perform
 * @see performing
 */
@ShouldUse
public inline infix fun <T> T.exerting(action: T.() -> Unit): T {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	action(this)
	
	return this
}

/**
 *  Invokes the [action] with this value as its argument and returns this value.
 *
 *  ***Example for "exert" vs. "pipe":***
 *
 * ```
 * //                            ↙ call via "it"
 * mutableListOf(1, 2) exert { it.add(3) } perform ::println // prints "[1, 2, 3]"
 * //                      ↖ returns the list
 *
 * mutableListOf(1, 2) pipe  { it.add(3) } perform ::println // prints "true"
 * //                      ↖ returns the result of `add(…)`
 * ```
 * @since 4.0
 * @see pipe
 * @see exerting
 * @see piping
 * @see perform
 * @see performing
 */
@ShouldUse
public inline infix fun <T> T.exert(action: (T) -> Unit): T {
	
	contract { callsInPlace(action, EXACTLY_ONCE) }
	
	action(this)
	
	return this
}

/**
 * Invokes the [action] if the [predicate] is `true` and returns this.
 *
 * @since 4.0
 */
@ShouldUse
public inline fun <T> T.exertIf(predicate: (T) -> Boolean, action: (T) -> Unit): T? {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	return if(predicate(this)) {
		action(this); this
	}
	else null
}

/**
 * Invokes the [action] if the [predicate] is `false` and returns this.
 *
 * @since 4.0
 */
@ShouldUse
public inline fun <T> T.exertIfNot(predicate: (T) -> Boolean, action: (T) -> Unit): T? {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	return if(!predicate(this)) { action(this); this } else null
}

/**
 * Invokes the [action] as receiver if the [predicate] is `true` and returns this.
 *
 * @since 4.0
 */
@ShouldUse
public inline fun <T> T.exertingIf(predicate: T.() -> Boolean, action: T.() -> Unit): T? {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	return if(predicate(this)) { action(this); this } else null
}

/**
 * Invokes the [action] as receiver if the [predicate] is `false` and returns this.
 *
 * @since 4.0
 */
@ShouldUse
public inline fun <T> T.exertingIfNot(predicate: T.() -> Boolean, action: T.() -> Unit): T? {
	
	contract { callsInPlace(predicate, EXACTLY_ONCE); callsInPlace(action, AT_MOST_ONCE) }
	
	return if(!predicate(this)) { action(this); this } else null
}
