/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.annotations

import kotlin.annotation.AnnotationRetention.BINARY
import kotlin.annotation.AnnotationTarget.FUNCTION

/**
 * Defines an annotation that is used for "pure" methods.
 * 
 * A method is pure, if its call makes no sense without using the returned value.
 * 
 * Usually, a pure method should not change the state of the object. An exception to this rule is, for example,
 * if the method caches the return value to reuse it on the next call.
 * 
 * @since 4.0
 * @see ShouldUse
 * @see MustUse
 */
@MustBeDocumented
@Retention(BINARY)
@Target(FUNCTION)
public annotation class Pure
