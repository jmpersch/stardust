/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardust.annotations

import kotlin.annotation.AnnotationRetention.BINARY
import kotlin.annotation.AnnotationTarget.CLASS
import kotlin.annotation.AnnotationTarget.FUNCTION

/**
 * Indicates that the return value of a method should be used. Without using the return value, the call of the method
 * is useless. But the call may change the state of the object.
 * 
 * Note: A stronger variant of this annotation is [Pure], where the call does not change the object state.
 * 
 * @since 5.0
 * @see Pure
 * @see MustUse
 */
@MustBeDocumented
@Retention(BINARY)
@Target(FUNCTION, CLASS)
public annotation class ShouldUse
