/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.collections.contentHashCode
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/**
 * Defines tests for the CEHC utility.
 *
 * @since 4.0
 */
@Suppress("ReplaceCallWithBinaryOperator", "ReplaceAssertBooleanWithAssertEquality")
class CehcBuilderTest {
	
	// ComparableClazz /////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun compareGreaterTest() {
		
		val clazz1 = ComparableClazz(2, 2)
		val clazz2 = ComparableClazz(2, 1)
		
		assertTrue(clazz1.compareTo(clazz2) > 0)
	}
	
	@Test
	fun compareEqualTest() {
		
		val clazz1 = ComparableClazz(2, 2)
		val clazz2 = ComparableClazz(2, 2)
		
		assertTrue(clazz1.compareTo(clazz2) == 0)
	}
	
	@Test
	fun compareLowerTest() {
		
		val clazz1 = ComparableClazz(2, 1)
		val clazz2 = ComparableClazz(2, 2)
		
		assertTrue(clazz1.compareTo(clazz2) < 0)
	}
	
	@Test
	fun equalTest() {
		
		val clazz1 = ComparableClazz(1, 2)
		val clazz2 = ComparableClazz(1, 2)
		
		assertTrue(clazz1 == clazz2)
	}
	
	@Test
	fun unequalTest() {
		
		val clazz1 = ComparableClazz(2, 1)
		val clazz2 = ComparableClazz(2, 0)
		
		assertFalse(clazz1 == clazz2)
	}
	
	@Test
	fun hashCodeTest() {
		
		val clazz1 = ComparableClazz(1, 2)
		
		assertEquals(
			clazz1.hashCode(),
			arrayOf(clazz1.value1, clazz1.value2).contentDeepHashCode()
		)
	}
	
	// DerivedComparableClazz //////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun derivedCompareGreaterTest() {
		
		val clazz1 = DerivedComparableClazz(2, 2, 2)
		val clazz2 = DerivedComparableClazz(2, 2, 1)
		
		assertTrue(clazz1.compareTo(clazz2) > 0)
	}
	
	@Test
	fun derivedCompareEqualTest() {
		
		val clazz1 = DerivedComparableClazz(2, 2, 2)
		val clazz2 = DerivedComparableClazz(2, 2, 2)
		
		assertTrue(clazz1.compareTo(clazz2) == 0)
	}
	
	@Test
	fun derivedCompareLowerTest() {
		
		val clazz1 = DerivedComparableClazz(2, 2, 1)
		val clazz2 = DerivedComparableClazz(2, 2, 2)
		
		assertTrue(clazz1.compareTo(clazz2) < 0)
	}
	
	@Test
	fun derivedEqualTest() {
		
		val clazz1 = DerivedComparableClazz(1, 2, 3)
		val clazz2 = DerivedComparableClazz(1, 2, 3)
		
		assertTrue(clazz1 == clazz2)
	}
	
	@Test
	fun derivedUnequalTest() {
		
		val clazz1 = DerivedComparableClazz(3, 2, 1)
		val clazz2 = DerivedComparableClazz(3, 2, 0)
		
		assertFalse(clazz1 == clazz2)
	}
	
	@Test
	fun derivedHashCodeTest() {
		
		val clazz1 = DerivedComparableClazz(1, 2, 3)
		
		assertEquals(
			clazz1.hashCode(),
			listOf(clazz1.value3).contentHashCode(arrayOf(clazz1.value1, clazz1.value2).contentHashCode())
		)
	}
}
