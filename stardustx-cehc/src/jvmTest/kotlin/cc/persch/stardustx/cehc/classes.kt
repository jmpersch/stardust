/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.annotations.Pure

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Incomparable class
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @since 4.0
 */
open class Clazz(val value1: Int, val value2: Int) {
	
	@Pure
	override fun equals(other: Any?) = EHC.computeEquality(this, other)
	
	@Pure
	override fun hashCode() = EHC.computeHashCode(this)
	
	private companion object {
		
		private val EHC = ehComputer {
			
			+Clazz::value1
			+Clazz::value2
		}
	}
}

/**
 * @since 4.0
 */
open class DerivedClazz(value1: Int, value2: Int, val value3: Int): Clazz(value1, value2) {
	@Pure
	override fun equals(other: Any?) = EHC.computeEquality(super.equals(other), this, other)
	
	@Pure
	override fun hashCode() = EHC.computeHashCode(super.hashCode(), this)
	
	private companion object {
		
		private val EHC = ehComputer {
			
			+DerivedClazz::value3
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Comparable class
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @since 4.0
 */
open class ComparableClazz(val value1: Int, val value2: Int): Comparable<ComparableClazz> {
	
	@Pure
	override fun compareTo(other: ComparableClazz) = CEHC.computeComparison(this, other)
	
	@Pure
	override fun equals(other: Any?) = CEHC.computeEquality(this, other)
	
	@Pure
	override fun hashCode() = CEHC.computeHashCode(this)
	
	private companion object {
		
		private val CEHC = cehComputer {
			
			+ComparableClazz::value1
			+ComparableClazz::value2
		}
	}
}

/**
 * @since 4.0
 */
open class DerivedComparableClazz(value1: Int, value2: Int, val value3: Int): ComparableClazz(value1, value2) {
	
	@Pure
	override fun compareTo(other: ComparableClazz) =
		CEHC.computeComparison(super.compareTo(other), this, other as? DerivedComparableClazz)
	
	@Pure
	override fun equals(other: Any?) = CEHC.computeEquality(super.equals(other), this, other)
	
	@Pure
	override fun hashCode() = CEHC.computeHashCode(super.hashCode(), this)
	
	private companion object {
		
		private val CEHC = cehComputer {
			
			+DerivedComparableClazz::value3
		}
	}
}
