/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.collections.contentHashCode
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/**
 * Defines tests for the EHC utility.
 *
 * @since 4.0
 */
@Suppress("ReplaceAssertBooleanWithAssertEquality")
class EhcBuilderTest {
	
	// Clazz ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun equalTest() {
		
		val clazz1 = Clazz(2, 1)
		val clazz2 = Clazz(2, 1)
		
		assertTrue(clazz1 == clazz2)
	}
	
	@Test
	fun unequalTest() {
		
		val clazz1 = Clazz(2, 1)
		val clazz2 = Clazz(2, 0)
		
		assertFalse(clazz1 == clazz2)
	}
	
	@Test
	fun hashCodeTest() {
		
		val clazz1 = Clazz(1, 2)
		
		assertEquals(
			clazz1.hashCode(),
			arrayOf(clazz1.value1, clazz1.value2).contentHashCode()
		)
	}
	
	// DerivedClazz ////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun derivedEqualTest() {
		
		val clazz1 = DerivedClazz(3, 2, 1)
		val clazz2 = DerivedClazz(3, 2, 1)
		
		assertTrue(clazz1 == clazz2)
	}
	
	@Test
	fun derivedUnequalTest() {
		
		val clazz1 = DerivedClazz(3, 2, 1)
		val clazz2 = DerivedClazz(3, 2, 0)
		
		assertFalse(clazz1 == clazz2)
	}
	
	@Test
	fun derivedHashCodeTest() {
		
		val clazz1 = DerivedClazz(1, 2, 3)
		
		assertEquals(
			clazz1.hashCode(),
			listOf(clazz1.value3).contentHashCode(arrayOf(clazz1.value1, clazz1.value2).contentHashCode())
		)
	}
}
