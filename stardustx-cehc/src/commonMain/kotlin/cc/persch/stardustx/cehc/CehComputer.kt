/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.*
import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.text.quote
import cc.persch.stardust.text.rope.*
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.BodyOption.WITH_BODY
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty

/**
 * Defines the [CehComputer] (**C**omparison, **E**quality, and **H**ash code computer) class.
 *
 * This class is thread-safe.
 *
 * This class is part of the CEH Computation Utility.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external objects. They must be thread-safe to one hundred percent. The best way is to use method-references.
 *
 * **Important:** Chose the right order of the specified method references.
 *   Beware: the order influences the comparison result.
 *
 * Example usage:
 *
 * ```
 * override fun compareTo(other: MyClass) = CEHC.computeComparison(this, other)
 *
 * override fun equals(other: Any?) = CEHC.computeEquality(this, other)
 *
 * override fun hashCode() = CEHC.computeHashCode(this)
 *
 * companion object {
 *
 *     private val CEHC = cehComputer {
 *
 *         +MyClass::foo
 *         +MyClass::bar
 *     }
 * }
 * ```
 *
 * @since 1.0
 * @see cehComputer
 * @see EHComputer
 *
 * @constructor Creates a new instance of the class [CehComputer].
 * @param comparators A collections that contains the comparators to use for CEH computation.
 */
@ThreadSafe
public class CehComputer<T: Any> internal constructor(comparators: Collection<Entry<T, *>>) {
	
	private val comparators = comparators.toTypedArray()
	
	/**
	 * Computes comparison for a derived class.
	 *
	 * @param preResult The pre-result.
	 * @param left An object.
	 * @param right Another object to compare with.
	 * @return [preResult], if it is not zero; otherwise, the computation result of [left] and [right].
	 */
	@Pure
	public fun computeComparison(preResult: Int, left: T?, right: T?): Int =
		if(preResult != 0) preResult
		else computeComparison(left, right)
	
	/**
	 * Computes comparison.
	 *
	 * @param left An object.
	 * @param right Another object to compare with.
	 * @return A negative integer, zero, or a positive integer as [left] is less than, equal to, or greater than *right*.<br></br>
	 *     **Info:** `null` is less than not-`null`.
	 */
	@Pure
	public fun computeComparison(left: T?, right: T?): Int {
		
		if(left == null)
			return if(right == null) 0 else -1
		if(right == null)
			return 1
		
		for(comparator in comparators) {
			
			val result = comparator.compare(left, right)
			
			if(result != 0)
				return result
		}
		
		return 0
	}
	
	/**
	 * Computes equality for a derived class.
	 *
	 * @param preResult The pre-result.
	 * @param left  An object.
	 * @param right Another object to check for equality.
	 * @return `true`, if both instances are equal; otherwise, `false`.
	 */
	@Pure
	public fun computeEquality(preResult: Boolean, left: T, right: Any?): Boolean =
		preResult && this.computeEquality(left, right)
	
	/**
	 * Computes equality.
	 *
	 * @param left  An object.
	 * @param right Another object to check for equality.
	 * @return `true`, if both instances are equal; otherwise, `false`.
	 */
	@Suppress("UNCHECKED_CAST", "DuplicatedCode")
	@Pure
	public fun computeEquality(left: T, right: Any?): Boolean {
		
		if(left === right)
			return true
		if(right == null || left::class != right::class)
			return false
		
		val other = right as T
		
		for(comparator in comparators) {
			if(!comparator.equalityComparator(comparator.extractor(left), comparator.extractor(other)))
				return false
		}
		
		return true
	}
	
	/**
	 * Computes the hash code for a derived class.
	 *
	 * @param preResult The pre-result.
	 * @param obj The object to hash.
	 * @return The computed hash code.
	 */
	@Pure
	public fun computeHashCode(preResult: Int, obj: T): Int {
		
		var hash = preResult * 31 + 1
		
		for(entry in comparators)
			hash = hash * 31 + (entry.extractor(obj)?.pipe { entry.hashCodeCalculator(it) } ?: 0)
		
		return hash
	}
	
	/**
	 * Computes the hash code.
	 *
	 * @param obj The object to hash.
	 * @return The computed hash code.
	 */
	@Pure
	public fun computeHashCode(obj: T): Int = this.computeHashCode(0, obj)
	
	/**
	 * Makes a string of the specified object, e.g. `ClassName{foo=Foo, bar=Bar}`.
	 *
	 * @param obj The object to convert to a string.
	 * @return The created string.
	 */
	@Suppress("DuplicatedCode")
	@Pure
	public fun makeString(obj: T): String {
		
		val simpleName = obj::class.getSimpleName()
		
		val strings = mutableListOf<String>()
		
		for(it in comparators) {
			
			val name = it.name
			val extracted = it.extractor(obj)
			
			val str = if(extracted is CharSequence) extracted.quote() else extracted.toString()
			
			strings.add("${if(name != null) "$name=" else ""}$str")
		}
		
		return if(strings.isEmpty()) simpleName else strings.joinToString(", ", "$simpleName{", "}")
	}
	
	/**
	 * Makes an indented string of the specified object, for example:
	 *
	 * ```
	 * ClassName {
	 *
	 *     foo: "Foo"
	 *
	 *     bar: "Bar"
	 * }
	 * ```
	 *
	 * @param obj The object to convert to a string.
	 * @return The created string.
	 * @since 5.0
	 * @see makeIndentedRope
	 */
	@Pure
	public fun makeIndentedString(obj: T, name: CharSequence? = null, bodyOption: BodyOption = WITH_BODY): String =
		rope { makeIndentedRope(obj, this, name, bodyOption) }
	
	/**
	 * Indents the string representation of the specified object, for example:
	 *
	 * ```
	 * ClassName {
	 *
	 *     foo: "Foo"
	 *
	 *     bar: "Bar"
	 * }
	 * ```
	 *
	 * @param obj The object to convert to a string.
	 * @return The created string.
	 * @since 5.0
	 * @see makeIndentedString
	 */
	@Pure
	public fun makeIndentedRope(
		obj: T,
		builder: RopeBuilder,
		name: CharSequence?,
		withBody: BodyOption
	) : Unit = builder.indentCustom(obj, name, withBody) {
		
		for(it in comparators) {
			
			indentProperty(it.extractor(obj), it.name)
		}
	}
	
	@PublishedApi
	@Pure
	internal fun asList(): List<Entry<T, *>> = comparators.asList()
	
	/**
	 * Defines an abstract comparator.
	 *
	 * @param T The type of the items to compare.
	 */
	internal class Entry<T: Any, E: Any>(
		val name: String?,
		val extractor: (T) -> E?,
		val equalityComparator: (Any?, Any?) -> Boolean,
		val hashCodeCalculator: (Any) -> Int,
		private val comparator: (E, E) -> Int,
		private val inverse: Boolean
	) {
		fun compare(left: T, right: T) = compareNullables(extractor(left), extractor(right), inverse, comparator)
	}
	
	internal companion object {
		
		internal val defaultIncomparableComparator = { l: Any?, r: Any? ->
			
			when {
				
				l == null -> if(r == null) 0 else -1
				
				r == null -> 1
				
				else -> 0
			}
		}
	}
}
