/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.annotations.threading.ThreadSafe

/**
 * Provides an equality comparison and hash code computation provider.
 *
 * @since 4.0
 * @see EQUALS
 * 
 * @property equalityComparator Get the equality comparator.
 * @property hashCodeCalculator Gets the hash code calculator.
 */
@ThreadSafe
public class EHProvider(
	public val equalityComparator: (Any?, Any?) -> Boolean,
	public val hashCodeCalculator: (Any) -> Int
)
