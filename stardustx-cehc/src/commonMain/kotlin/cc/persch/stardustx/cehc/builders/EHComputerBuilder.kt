/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc.builders

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.cehc.CehDslMarker
import cc.persch.stardustx.cehc.EHComputer
import cc.persch.stardustx.cehc.EHProvider
import cc.persch.stardustx.cehc.EQUALS
import kotlin.reflect.KProperty1

/**
 * Defines the builder-class for the [EHComputer].
 *
 * This builder-class is *not* thread-safe, but the built [EHComputer] is.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external objects. They must be 100%-ly thread-safe. The best way is to use method-references.
 *
 * Example usage:
 *
 * ```
 * override fun equals(other: Any?) = EHC.computeEquality(this, other)
 *
 * override fun hashCode() = EHC.computeHashCode(this)
 *
 * override fun toString() = EHC.makeString(this)
 *
 * companion object {
 *
 *     private val EHC = ehComputer {
 *
 *         +MyClass::foo
 *         +MyClass::bar
 *     }
 * }
 * ```
 *
 * Example for a derived class:
 *
 * ```
 * override fun equals(other: Any?) = EHC.computeEquality(super.equals(other), this, other)
 *
 * override fun hashCode() = EHC.computeHashCode(super.hashCodee(), this)
 *
 * companion object {
 *
 *     private val EHC = ehComputer {
 *
 *         +MyDerivedClass::baz
 *     }
 * }
 * ```
 *
 * *Info: You have to call the super methods explicitly (e.g. `super.equals(other)`), because Kotlin does not support
 * method references of the super class like Java does, e.g. `super::equals` (state: Kotlin version 1.8.20).*
 *
 * @since 1.0
 */
@CehDslMarker
public class EHComputerBuilder<T: Any> {
	
	private val converters = mutableListOf<EHComputer.Entry<T>>()
	
	/**
	 * Adds this extractor.
	 *
	 * @see add
	 */
	public operator fun ((T) -> Any?).unaryPlus() { this@EHComputerBuilder.add(this) }
	
	/**
	 * Adds this extractor.
	 *
	 * @see add
	 */
	public operator fun KProperty1<T, *>.unaryPlus() { this@EHComputerBuilder.add(this) }
	
	@PublishedApi
	internal fun addAll(entries: Collection<EHComputer.Entry<T>>) {
		
		entries.toCollection(converters)
	}
	
	/**
	 * Adds an extractor.
	 *
	 * @param extractor An extractor that provides a value to process.
	 * @see unaryPlus
	 */
	public fun add(
		extractor: (T) -> Any?
	) : EHComputerBuilder<T> = this.add(null, extractor, EQUALS)
	
	/**
	 * Adds an extractor.
	 *
	 * @param extractor An extractor that provides a value to process.
	 * @see unaryPlus
	 */
	public fun add(
		extractor: (T) -> Any?,
		ehProvider: EHProvider
	) : EHComputerBuilder<T> = this.add(null, extractor, ehProvider)
	
	/**
	 * Adds a property.
	 *
	 * @see unaryPlus
	 */
	public fun add(
		property: KProperty1<T, *>,
	) : EHComputerBuilder<T> = this.add(property.name, property::get, EQUALS)
	
	/**
	 * Adds a property with a custom extractor.
	 *
	 * @see unaryPlus
	 * @since 5.0
	 */
	public fun <V> add(
		property: KProperty1<T, V>,
		extractor: (V) -> Any?
	) : EHComputerBuilder<T> = this.add(property.name, { extractor(property.get(it)) }, EQUALS)
	
	/**
	 * Adds a property with an [EHProvider].
	 *
	 * @see unaryPlus
	 */
	public fun add(
		property: KProperty1<T, *>,
		ehProvider: EHProvider
	) : EHComputerBuilder<T> = this.add(property.name, property::get, ehProvider)
	
	/**
	 * Adds an extractor.
	 *
	 * @param name The name to use in [class string][EHComputer.makeString].
	 * @param extractor An extractor that provides a value to process.
	 */
	public fun add(
		name: String?,
		extractor: (T) -> Any?,
	) : EHComputerBuilder<T> = add(name, extractor, EQUALS)
	
	/**
	 * Adds an extractor.
	 *
	 * @param name The name to use in [class string][EHComputer.makeString].
	 * @param extractor An extractor that provides a value to process.
	 */
	public fun add(
		name: String?,
		extractor: (T) -> Any?,
		ehProvider: EHProvider
	) : EHComputerBuilder<T> {
		
		this.converters.add(
			EHComputer.Entry(
				name,
				extractor,
				ehProvider.equalityComparator,
				ehProvider.hashCodeCalculator
			)
		)
		
		return this
	}
	
	@Pure
	public fun build(): EHComputer<T> = EHComputer(converters)
}
