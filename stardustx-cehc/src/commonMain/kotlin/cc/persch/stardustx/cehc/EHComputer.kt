/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.pipe
import cc.persch.stardust.annotations.threading.ThreadSafe
import cc.persch.stardust.getSimpleName
import cc.persch.stardust.text.quote
import cc.persch.stardust.text.rope.*
import cc.persch.stardust.text.rope.indenters.BodyOption
import cc.persch.stardust.text.rope.indenters.BodyOption.WITH_BODY
import cc.persch.stardust.text.rope.indenters.indentCustom
import cc.persch.stardust.text.rope.indenters.indentProperty

/**
 * Defines the [EHComputer] (**E**quality and **H**ash code computer) class.
 *
 * This class is thread-safe.
 *
 * This class is part of the CEH Computation Utility.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external
 * objects. They must be thread-safe to one hundred percent. The best way is to use method references.
 *
 * Example usage:
 *
 * ```
 * override fun equals(other: Any?) = EHC.computeEquality(this, other)
 *
 * override fun hashCode() = EHC.computeHashCode(this)
 *
 * override fun toString() = EHC.makeString(this)
 *
 * companion object {
 *
 *     private val EHC = ehComputer {
 *
 *         +MyClass::foo
 *         +MyClass::bar
 *     }
 * }
 * ```
 * 
 * @since 1.0
 * @see ehComputer
 * @see CehComputer
 *
 * @constructor Creates a new instance of the class [EHComputer].
 * @param converters A collections that contains the converters to use for EH computation.
 */
@ThreadSafe
public class EHComputer<T: Any> internal constructor(converters: Collection<Entry<T>>) {
	
	private val converters = converters.toTypedArray()
	
	/**
	 * Computes equality.
	 *
	 * @param left  An object.
	 * @param right Another object to check for equality.
	 * @return `true`, if both instances are equal; otherwise, `false`.
	 */
	@Suppress("UNCHECKED_CAST", "DuplicatedCode")
	@Pure
	public fun computeEquality(left: T, right: Any?): Boolean {
		
		if(left === right)
			return true
		if(right == null || left::class != right::class)
			return false
		
		val other = right as T
		
		for(entry in converters) {
			
			if(!entry.equalityComparator(entry.extractor(left), entry.extractor(other)))
				return false
		}
		
		return true
	}
	
	/**
	 * Computes equality for a derived class.
	 *
	 * @param preResult The pre-result.
	 * @param left  An object.
	 * @param right Another object to check for equality.
	 * @return `true`, if both instances are equal; otherwise, `false`.
	 */
	@Pure
	public fun computeEquality(preResult: Boolean, left: T, right: Any?): Boolean =
		preResult && this.computeEquality(left, right)
	
	/**
	 * Computes the hash code for a derived class.
	 *
	 * @param preResult The pre-result.
	 * @param obj The object to hash.
	 * @return The computed hash code.
	 */
	@Pure
	public fun computeHashCode(preResult: Int, obj: T): Int {
		
		var hash = preResult * 31 + 1
		
		for(entry in converters)
			hash = hash * 31 + (entry.extractor(obj)?.pipe { entry.hashCodeCalculator(it) } ?: 0)
		
		return hash
	}
	
	/**
	 * Computes the hash code.
	 *
	 * @param obj The object to hash.
	 * @return The computed hash code.
	 */
	@Pure
	public fun computeHashCode(obj: T): Int = this.computeHashCode(0, obj)
	
	/**
	 * Makes a string of the specified object, e.g. `ClassName{Foo, Bar}`.
	 *
	 * @param obj The object to convert to a string.
	 * @return The created string.
	 */
	@Pure
	public fun makeString(obj: T): String {
		
		val simpleName = obj::class.getSimpleName()
		
		val strings = mutableListOf<String>()
		
		for(it in converters) {
			
			val name = it.name
			val extracted = it.extractor(obj)
			
			val str = if(extracted is CharSequence) extracted.quote() else extracted.toString()
			
			strings.add("${if(name != null) "$name=" else ""}$str")
		}
		
		return if(strings.isEmpty()) simpleName else strings.joinToString(", ", "$simpleName{", "}")
	}
	
	/**
	 * Makes an indented string of the specified object, for example:
	 *
	 * ```
	 * ClassName {
	 *
	 *     foo: "Foo"
	 *
	 *     bar: "Bar"
	 * }
	 * ```
	 *
	 * @param obj The object to convert to a string.
	 * @return The created string.
	 * @since 5.0
	 * @see makeIndentedRope
	 */
	@Pure
	public fun makeIndentedString(obj: T, name: CharSequence? = null, withBody: BodyOption = WITH_BODY): String =
		rope { makeIndentedRope(obj, this, name, withBody) }
	
	/**
	 * Indents the string representation of the specified object, for example:
	 *
	 * ```
	 * ClassName {
	 *
	 *     foo: "Foo"
	 *
	 *     bar: "Bar"
	 * }
	 * ```
	 *
	 * @param obj The object to convert to a string.
	 * @return The created string.
	 * @since 5.0
	 * @see makeIndentedString
	 */
	@Pure
	public fun makeIndentedRope(
		obj: T,
		builder: RopeBuilder,
		name: CharSequence?,
		withBody: BodyOption
	) : Unit = builder.indentCustom(obj, name, withBody) {
		
		for(it in converters) {
			
			indentProperty(it.extractor(obj), it.name)
		}
	}
	
	@PublishedApi
	@Pure
	internal fun asList(): List<Entry<T>> = converters.asList()
	
	/**
	 * Defines an [EHComputer] entry containing the name and the extractor of a certain value to process.
	 *
	 * @property name The name of the value.
	 * @property extractor The extractor that gets the value to process.
	 */
	internal class Entry<E: Any>(
		val name: String?,
		val extractor: (E) -> Any?,
		val equalityComparator: (Any?, Any?) -> Boolean,
		val hashCodeCalculator: (Any) -> Int
	)
}
