/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.TypeSafeBuilder
import cc.persch.stardustx.cehc.builders.CehComputerBuilder
import cc.persch.stardustx.cehc.builders.EHComputerBuilder
import cc.persch.stardust.exert
import cc.persch.stardust.exerting

import kotlin.annotation.AnnotationRetention.BINARY
import kotlin.annotation.AnnotationTarget.CLASS
import kotlin.contracts.InvocationKind.EXACTLY_ONCE
import kotlin.contracts.contract

/**
 * Defines the marker annotation for the CEH Computation DSL.
 *
 * @since 4.2
 */
@DslMarker
@Retention(BINARY)
@Target(CLASS)
public annotation class CehDslMarker

/**
 * Defines a builder for a CEH Computer.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external
 * objects. They must be 100-%-ly thread-safe. The best way is to use method references.
 *
 * **Important:** Chose the right order of the specified method-references. The order influences the comparison result.
 *
 * Example usage:
 *
 * ```
 * class Foo(
 *     val value1: Any,
 *     val value2: Any
 * ) : Comparable<Foo> {
 *     override fun equals(other: Any?) = CEHC.computeEquality(this, other)
 *
 *     override fun hashCode() = CEHC.computeHashCode(this)
 *
 *     override fun compareTo(other: Foo) = CEHC.computeComparison(this, other)
 *
 *     companion object {
 *
 *         private val CEHC = cehComputer {
 *
 *             +Foo::value1
 *             +Foo::value2
 *     }
 * }
 * ```
 *
 * @param extractor The extractor that provides the value to compare.
 * @since 4.0
 */
@Pure
@TypeSafeBuilder
public inline fun <T: Any> cehComputer(
	@BuilderInference extractor: CehComputerBuilder<T>.() -> Unit
) : CehComputer<T> {
	
	contract { callsInPlace(extractor, EXACTLY_ONCE) }
	
	return CehComputerBuilder<T>().exert(extractor).build()
}

/**
 * Defines a builder for a CEH Computer.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external
 * objects. They must be 100-%-ly thread-safe. The best way is to use method references.
 *
 * **Important:** Chose the right order of the specified method-references. The order influences the comparison result.
 *
 * Example usage:
 *
 * ```
 * open class Foo(
 *     val value1: Any,
 *     val value2: Any
 * ) : Comparable<Foo> {
 * 
 *     override fun equals(other: Any?) = CEHC.computeEquality(this, other)
 * 
 *     override fun hashCode() = CEHC.computeHashCode(this)
 * 
 *     override fun compareTo(other: Foo) = CEHC.computeComparison(this, other)
 * 
 *     companion object {
 *
 *         @JvmStatic
 *         protected val CEHC = cehComputer {
 *
 *             +Foo::value1
 *             +Foo::value2
 *     }
 * }
 * 
 * class Bar(
 *     value1: Any,
 *     value2: Any
 *     val value3: Any,
 *     val value4: Any
 * ) : Foo(value1, value2) {
 * 
 *     override fun equals(other: Any?) = CEHC.computeEquality(this, other)
 *
 *     override fun hashCode() = CEHC.computeHashCode(this)
 *
 *     override fun compareTo(other: Foo) =
 *         if(other is Bar) CEHC.computeComparison(this, other)
 *         else super.compareTo(other)
 * 
 *     companion object {
 *
 *         private val CEHC = cehComputer<DerivedClass, _>(Foo.CEHC) {
 *
 *             +Bar::value3
 *             +Bar::value4
 *     }
 * }
 * ```
 * @param extractor The extractor that provides the value to compare.
 * @since 5.0
 */
@Pure
@TypeSafeBuilder
public inline fun <T: S, S: Any> cehComputer(
	`super`: CehComputer<S>,
	@BuilderInference extractor: CehComputerBuilder<T>.() -> Unit
) : CehComputer<T> {
	
	contract { callsInPlace(extractor, EXACTLY_ONCE) }
	
	return CehComputerBuilder<T>().exerting {
			
		@Suppress("UNCHECKED_CAST")
		addAll(`super`.asList() as List<CehComputer.Entry<T, *>>)
		
	}.exert(extractor).build()
}

/**
 * Defines a builder for an EH Computer.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external
 * objects. They must be 100-%-ly thread-safe. The best way is to use method references.
 *
 * **Important:** Chose the right order of the specified method-references. The order influences the comparison result.
 *
 * Example usage:
 *
 * ```
 * class Foo(
 *     val value1: Any,
 *     val value2: Any
 * ) {
 *     override fun equals(other: Any?) = EHC.computeEquality(this, other)
 *
 *     override fun hashCode() = EHC.computeHashCode(this)
 *
 *     companion object {
 *
 *         private val EHC = ehComputer {
 *
 *             +Foo::value1
 *             +Foo::value2
 *     }
 * }
 * ```
 * @param extractor The extractor that provides the value to compare.
 * @since 4.0
 */
@Pure
@TypeSafeBuilder
public inline fun <T: Any> ehComputer(
	@BuilderInference extractor: EHComputerBuilder<T>.() -> Unit
) : EHComputer<T> {
	
	contract { callsInPlace(extractor, EXACTLY_ONCE) }
	
	return EHComputerBuilder<T>().exert(extractor).build()
}

/**
 * Defines a builder for an EH Computer.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external
 * objects. They must be 100-%-ly thread-safe. The best way is to use method references.
 *
 * **Important:** Chose the right order of the specified method-references. The order influences the comparison result.
 *
 * Example usage:
 *
 * ```
 * open class Foo(
 *     val value1: Any,
 *     val value2: Any
 * ) {
 *     override fun equals(other: Any?) = EHC.computeEquality(this, other)
 *
 *     override fun hashCode() = EHC.computeHashCode(this)
 *
 *     override fun compareTo(other: Foo) = EHC.computeComparison(this, other)
 *
 *     companion object {
 *
 *         @JvmStatic
 *         protected val EHC = ehComputer {
 *
 *             +Foo::value1
 *             +Foo::value2
 *     }
 * }
 *
 * class Bar(
 *     value1: Any,
 *     value2: Any
 *     val value3: Any,
 *     val value4: Any
 * ) : Foo(value1, value2) {
 *
 *     override fun equals(other: Any?) = EHC.computeEquality(this, other)
 *
 *     override fun hashCode() = EHC.computeHashCode(this)
 *
 *     override fun compareTo(other: Foo) =
 *         if(other is Bar) EHC.computeComparison(this, other)
 *         else super.compareTo(other)
 *
 *     companion object {
 *
 *         private val EHC = ehComputer<DerivedClass, _>(Foo.EHC) {
 *
 *             +Bar::value3
 *             +Bar::value4
 *     }
 * }
 * ```
 * @param extractor The extractor that provides the value to compare.
 * @since 5.0
 */
@Pure
@TypeSafeBuilder
public inline fun <T: S, S: Any> ehComputer(
	`super`: EHComputer<S>,
	@BuilderInference extractor: EHComputerBuilder<T>.() -> Unit
) : EHComputer<T> {
	
	contract { callsInPlace(extractor, EXACTLY_ONCE) }
	
	return EHComputerBuilder<T>().exerting {
			
		@Suppress("UNCHECKED_CAST")
		addAll(`super`.asList() as List<EHComputer.Entry<T>>)
		
	}.exert(extractor).build()
}

/**
 * Compares objects by equality (`==`) and creates the hash code using [Any.hashCode]
 *
 * @since 4.0
 */
public val EQUALS: EHProvider = EHProvider({ x, y -> x == y }, Any::hashCode)
