/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.cehc.builders

import cc.persch.stardust.annotations.Pure
import cc.persch.stardustx.cehc.CehComputer
import cc.persch.stardustx.cehc.CehComputer.Companion.defaultIncomparableComparator
import cc.persch.stardust.compareNullables
import cc.persch.stardust.pipe
import cc.persch.stardustx.cehc.CehDslMarker
import cc.persch.stardustx.cehc.EHProvider
import cc.persch.stardustx.cehc.EQUALS
import kotlin.reflect.KProperty1

/**
 * Defines the builder-class for the [CehComputer].
 *
 * This builder-class is *not* thread-safe, but the built [CehComputer] is.
 *
 * **Important:** Pay attention that the specified converters do not hold inner states or references to external
 * objects. They must be 100-%-ly thread-safe. The best way is to use method-references.
 *
 * **Important:** Chose the right order of the specified properties or extractors.
 * The order influences the comparison result.
 *
 * Example usage:
 *
 * ```
 * override fun compareTo(other: MyClass) = CEHC.computeComparison(this, other)
 *
 * override fun equals(other: Any?) = CEHC.computeEquality(this, other)
 *
 * override fun hashCode() = CEHC.computeHashCode(this)
 *
 * companion object {
 *
 *     private val CEHC = cehComputer {
 *
 *         +MyClass::foo
 *         +MyClass::bar
 *     }
 * }
 * ```
 *
 * Example for a derived class:
 *
 * ```
 * override fun compareTo(other: MyClass) = CEHC.computeComparison(super.compareTo(other), this, other)
 *
 * override fun equals(other: Any?) = CEHC.computeEquality(super.equals(other), this, other)
 *
 * override fun hashCode() = CEHC.computeHashCode(super.hashCodee(), this)
 *
 * companion object {
 *
 *     private val CEHC = cehComputer {
 *
 *         +MyDerivedClass::baz
 *     }
 * }
 * ```
 *
 * *Info: You have to call the super methods explicitly (e.g. `super.compareTo(other)`), because Kotlin does not support
 * method references of the super class like Java does, e.g. `super::compareTo` (state: Kotlin version 1.8.20).*
 *
 * @since 1.0
 */
@CehDslMarker
public class CehComputerBuilder<T: Any> {
	
	private val comparators = mutableListOf<CehComputer.Entry<T, *>>()
	
	public operator fun <C: Comparable<C>> ((T) -> C?).unaryPlus() { add(this) }
	
	public operator fun <C: Comparable<C>> ((T) -> C?).not() { add(this, inverse = true) }
	
	public operator fun <C> ((T) -> C?).unaryMinus() { addIncomparable(this) }
	
	public operator fun <C: Comparable<C>> KProperty1<T, C?>.unaryPlus() { add(this) }
	
	public operator fun <C: Comparable<C>> KProperty1<T, C?>.not() { add(this, inverse = true) }
	
	public operator fun <C> KProperty1<T, C?>.unaryMinus() { addIncomparable(this) }
	
	// Add /////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@PublishedApi
	internal fun addAll(entries: Collection<CehComputer.Entry<T, *>>) {
		
		entries.toCollection(comparators)
	}
	
	/**
	 * Adds an extractor.
	 *
	 * Example:
	 *
	 * ```
	 * add(Item::getName)
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for comparison, equality check and hash code generation.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> add(
		extractor: (T) -> C?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(extractor, EQUALS, inverse)
	
	/**
	 * Adds an extractor.
	 *
	 * Example:
	 *
	 * ```
	 * add(Item::getName)
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for comparison, equality check and hash code generation.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> add(
		extractor: (T) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(null as String?, extractor, ehProvider, inverse)
	
	/**
	 * Adds an [property] and uses its name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * add(Item::name)
	 *
	 * // ... is equal to this:
	 * add("name", Item::name::get)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> add(
		property: KProperty1<T, C?>,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(property, EQUALS, inverse)
	
	/**
	 * Adds an [property] and uses its name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * add(Item::name)
	 *
	 * // ... is equal to this:
	 * add("name", Item::name::get)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> add(
		property: KProperty1<T, C?>,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(property.name, property::get, ehProvider, inverse)
	
	/**
	 * Adds an [extractor] with the specified [name].
	 *
	 * Example:
	 *
	 * ```
	 * add("getName", Item::getName)
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for comparison, equality check and hash code generation.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> add(
		name: String?,
		extractor: (T) -> C?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(name, extractor, EQUALS, inverse)
	
	/**
	 * Adds an [extractor] with the specified [name].
	 *
	 * Example:
	 *
	 * ```
	 * add("getName", Item::getName)
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for comparison, equality check and hash code generation.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> add(
		name: String?,
		extractor: (T) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> {
		
		comparators.add(
			CehComputer.Entry(
				name,
				extractor,
				ehProvider.equalityComparator,
				ehProvider.hashCodeCalculator,
				::compareNullables,
				inverse
			)
		)
		
		return this
	}
	
	// Add Custom //////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom(Item::getName) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> addCustom(
		extractor: (T) -> C?,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(extractor, EQUALS, false, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom(Item::getName) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> addCustom(
		extractor: (T) -> C?,
		inverse: Boolean,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(extractor, EQUALS, inverse, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom(Item::getName, EQUALS) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> addCustom(
		extractor: (T) -> C?,
		ehProvider: EHProvider,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(null, extractor, ehProvider, false, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom(Item::getName, EQUALS) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> addCustom(
		extractor: (T) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(null, extractor, ehProvider, inverse, comparator)
	
	/**
	 * Adds an [property] with a custom comparator and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * addCustom(Item::name, MyComparator.comparator)
	 *
	 * // ... is equal to this:
	 * addCustom("name", Item::name::get, MyComparator.comparator)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C> addCustom(
		property: KProperty1<T, C?>,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = this.addCustom(property, EQUALS, false, comparator)
	
	/**
	 * Adds an [property] with a custom comparator and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * addCustom(Item::name, MyComparator.comparator)
	 *
	 * // ... is equal to this:
	 * addCustom("name", Item::name::get, MyComparator.comparator)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C> addCustom(
		property: KProperty1<T, C?>,
		inverse: Boolean,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = this.addCustom(property, EQUALS, inverse, comparator)
	
	/**
	 * Adds an [property] with a custom comparator and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * addCustom(Item::name, MyComparator.comparator)
	 *
	 * // ... is equal to this:
	 * addCustom("name", Item::name::get, MyComparator.comparator)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C> addCustom(
		property: KProperty1<T, C?>,
		ehProvider: EHProvider,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = this.addCustom(property.name, property::get, ehProvider, false, comparator)
	
	/**
	 * Adds an [property] with a custom comparator and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * addCustom(Item::name, MyComparator.comparator)
	 *
	 * // ... is equal to this:
	 * addCustom("name", Item::name::get, MyComparator.comparator)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <C> addCustom(
		property: KProperty1<T, C?>,
		ehProvider: EHProvider,
		inverse: Boolean = false,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = this.addCustom(property.name, property::get, ehProvider, inverse, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom(Item::getName) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> addCustom(
		name: String?,
		extractor: (T) -> C?,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(name, extractor, EQUALS, false, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom(Item::getName) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Comparable<C>> addCustom(
		name: String?,
		extractor: (T) -> C?,
		inverse: Boolean,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(name, extractor, EQUALS, inverse, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom("name", Item::name::get, EQUALS) { l, r -> Integer.compare(l.length(), r.length()) }
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C> addCustom(
		name: String?,
		extractor: (T) -> C?,
		ehProvider: EHProvider,
		comparator: (C?, C?) -> Int
	) : CehComputerBuilder<T> = addCustom(name, extractor, ehProvider, false, comparator)
	
	/**
	 * Adds a custom comparator.
	 *
	 * Example:
	 * ```
	 * addCustom("name", Item::name::get, EQUALS) {l, r -> compareNullables(l, r) { x, y -> Integer.compare(x.length(), y.length()) } })
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param comparator A comparator that compares the converted items.
	 * @param C The type of the item to compare.
	 * @return The instance of the current builder.
	 */
	public fun <C: Any> addCustom(
		name: String?,
		extractor: (T) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean,
		comparator: (C, C) -> Int
	) : CehComputerBuilder<T> {
		
		comparators.add(
			CehComputer.Entry(
				name,
				extractor,
				ehProvider.equalityComparator,
				ehProvider.hashCodeCalculator,
				comparator,
				inverse
			)
		)
		
		return this
	}
	
	// Add Sub-Comparer ////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Adds an extractor-comparator.
	 *
	 * Example:
	 * ```
	 * add(Item::getName, String::length)
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param subExtractor A sub-extractor. Its result is used for comparison.
	 * @param X The type of the converted item.
	 * @param C The type of the extracted comparable item.
	 * @return The result.
	 */
	public fun <X, C: Comparable<C>> add(
		extractor: (T) -> X?,
		subExtractor: (X) -> C?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(extractor, subExtractor, EQUALS, inverse)
	
	/**
	 * Adds an extractor-comparator.
	 *
	 * Example:
	 * ```
	 * add(Item::getName, String::length)
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param subExtractor A sub-extractor. Its result is used for comparison.
	 * @param X The type of the converted item.
	 * @param C The type of the extracted comparable item.
	 * @return The result.
	 */
	public fun <X, C: Comparable<C>> add(
		extractor: (T) -> X?,
		subExtractor: (X) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(null, extractor, subExtractor, ehProvider, inverse)
	
	/**
	 * Adds an [property] with a sub-extractor and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * add(Item::name, Name::getFamilyName)
	 *
	 * // ... is equal to this:
	 * add("name", Item::name::get, Name::getFamilyName)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param subExtractor A sub-extractor to compare an inner value.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <X, C: Comparable<C>> add(
		property: KProperty1<T, X?>,
		subExtractor: (X) -> C?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(property, subExtractor, EQUALS, inverse)
	
	/**
	 * Adds an [property] with a sub-extractor and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * add(Item::name, Name::getFamilyName)
	 *
	 * // ... is equal to this:
	 * add("name", Item::name::get, Name::getFamilyName)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param subExtractor A sub-extractor to compare an inner value.
	 * @param C The type of the converted item that shall be compared.
	 * @return The instance of the current builder.
	 */
	public fun <X, C: Comparable<C>> add(
		property: KProperty1<T, X?>,
		subExtractor: (X) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(property.name, property::get, subExtractor, ehProvider, inverse)
	
	/**
	 * Adds an extractor-comparator.
	 *
	 * Example:
	 * ```
	 * add(Item::getName, String::length)
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param subExtractor A sub-extractor. Its result is used for comparison.
	 * @param X The type of the converted item.
	 * @param C The type of the extracted comparable item.
	 * @return The result.
	 */
	public fun <X, C: Comparable<C>> add(
		name: String?,
		extractor: (T) -> X?,
		subExtractor: (X) -> C?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = add(name, extractor, subExtractor, EQUALS, inverse)
	
	/**
	 * Adds an extractor-comparator.
	 *
	 * Example:
	 * ```
	 * add(Item::getName, String::length)
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param subExtractor A sub-extractor. Its result is used for comparison.
	 * @param X The type of the converted item.
	 * @param C The type of the extracted comparable item.
	 * @return The result.
	 */
	public fun <X, C: Comparable<C>> add(
		name: String?,
		extractor: (T) -> X?,
		subExtractor: (X) -> C?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> {
		
		comparators.add(
			CehComputer.Entry(
				name,
				{ extractor(it)?.pipe(subExtractor) },
				ehProvider.equalityComparator,
				ehProvider.hashCodeCalculator,
				::compareNullables,
				inverse
			)
		)
		
		return this
	}
	
	// Add Incomparable ////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Adds a converter for an item that is not comparable.
	 *
	 * Example:
	 * ```
	 * addIncomparable(Item::someIncomparable)
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param X The type of the converted item.
	 * @return The result on equality check and hash code generation; or zero on comparison.
	 */
	public fun <X> addIncomparable(
		extractor: (T) -> X?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = addIncomparable(extractor, EQUALS, inverse)
	
	/**
	 * Adds a converter for an item that is not comparable.
	 *
	 * Example:
	 * ```
	 * addIncomparable(Item::someIncomparable)
	 * ```
	 *
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param X The type of the converted item.
	 * @return The result on equality check and hash code generation; or zero on comparison.
	 */
	public fun <X> addIncomparable(
		extractor: (T) -> X?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = addIncomparable(null, extractor, ehProvider, inverse)
	
	/**
	 * Adds an incomparable [property] and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * addIncomparable(Item::incomparableProperty)
	 *
	 * // ... is equal to this:
	 * addIncomparable("name", Item::incomparableProperty::get)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param X The type of the converted item.
	 * @return The instance of the current builder.
	 */
	public fun <X> addIncomparable(
		property: KProperty1<T, X?>,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = addIncomparable(property.name, property::get, EQUALS, inverse)
	
	/**
	 * Adds an incomparable [property] and uses the property's name in the string representation.
	 *
	 * Example:
	 *
	 * ```
	 * // Add a property this way ...
	 * addIncomparable(Item::incomparableProperty)
	 *
	 * // ... is equal to this:
	 * addIncomparable("name", Item::incomparableProperty::get)
	 * ```
	 *
	 * @param property A property. Its result is used for comparison, equality check and hash code generation.
	 * @param X The type of the converted item.
	 * @return The instance of the current builder.
	 */
	public fun <X> addIncomparable(
		property: KProperty1<T, X?>,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = addIncomparable(property.name, property::get, ehProvider, inverse)
	
	/**
	 * Adds a converter for an item that is not comparable.
	 *
	 * Example:
	 * ```
	 * addIncomparable(Item::someIncomparable)
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param X The type of the converted item.
	 * @return The result on equality check and hash code generation; or zero on comparison.
	 */
	public fun <X> addIncomparable(
		name: String?,
		extractor: (T) -> X?,
		inverse: Boolean = false
	) : CehComputerBuilder<T> = addIncomparable(name, extractor, EQUALS, inverse)
	
	/**
	 * Adds a converter for an item that is not comparable.
	 *
	 * Example:
	 * ```
	 * addIncomparable(Item::someIncomparable)
	 * ```
	 *
	 * @param name A name for the extractor to use in the string representation.
	 * @param extractor An extractor. Its result is used for equality check and hash code generation.
	 * @param X The type of the converted item.
	 * @return The result on equality check and hash code generation; or zero on comparison.
	 */
	public fun <X: Any> addIncomparable(
		name: String?,
		extractor: (T) -> X?,
		ehProvider: EHProvider,
		inverse: Boolean = false
	) : CehComputerBuilder<T> {
		
		comparators.add(
			CehComputer.Entry(
				name,
				extractor,
				ehProvider.equalityComparator,
				ehProvider.hashCodeCalculator,
				defaultIncomparableComparator,
				inverse
			)
		)
		
		return this
	}
	
	/**
	 * Builds the [CehComputer].
	 */
	@Pure
	public fun build(): CehComputer<T> = CehComputer(comparators)
}
