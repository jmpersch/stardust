pluginManagement {
	
	repositories {
		
		mavenCentral()
		gradlePluginPortal()
	}
	
	val kotlinVersion: String by settings
	val foojayVersion: String by settings
	
	plugins {
		
		kotlin("multiplatform") version kotlinVersion
		kotlin("plugin.serialization") version kotlinVersion
		id("org.gradle.toolchains.foojay-resolver-convention") version foojayVersion
	}
}

plugins {
	
	id("org.gradle.toolchains.foojay-resolver-convention")
}

rootProject.name = "stardust"

include("stardustx-cehc")
include("stardustx-collections")
include("stardustx-collections-immutable")
include("stardustx-datetime")
include("stardustx-test")
include("stardustx-text")
include("stardustx-uuid")
