/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardust.conclude.asSuccess
import cc.persch.stardust.conclude.failureOf
import kotlin.uuid.Uuid

/**
 * Defines the variants of an [Uuid].
 *
 * @since 1.0
 *
 * @property value The [Uuid] version value.
 */
public enum class UuidVersion(public val value: Int) {
	
	/**
	 * Version 1: MAC address and time
	 */
	V1(1),
	
	/**
	 * Version 2: DCE Security
	 */
	V2(2),
	
	/**
	 * Version 3: MD5 hash and namespace
	 */
	V3(3),
	
	/**
	 * Version 4: Random
	 */
	V4(4),
	
	/**
	 * Version 5: SHA-1 hash and namespace
	 */
	V5(5),
	
	/**
	 * Version 6: Timestamp and a counter
	 */
	V6(6),
	
	/**
	 * Version 7: Timestamp and random value
	 */
	V7(7),
	
	/**
	 * Version 8: Custom data
	 */
	V8(8);
	
	public companion object {
		
		/**
		 * Tries to get the [UuidVersion] of the specified [version number][version].
		 */
		@Pure
		public fun valueOf(
			version: Int
		) : Conclude<UuidVersion, InvalidUuidVersionFault> =
			entries.find { version == it.value }?.asSuccess() ?: failureOf(InvalidUuidVersionFault(version))
	}
}
