/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.Unchecked
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardust.conclude.asSuccess
import cc.persch.stardust.conclude.failureOf
import kotlin.uuid.Uuid

/**
 * Defines the variants of an [Uuid].
 *
 * @since 1.0
 */
public enum class UuidVariant(
	@param:Unchecked internal val unshiftedVariant: Int,
	@param:Unchecked internal val shift: Int
) {
	/**
	 * NCS
	 */
	NCS(0, 0), // 0 → 000 << 0 → 0000
	
	/**
	 * IETF
	 */
	IETF(2, 2), // 2 → 010 << 2 → 1000
	
	/**
	 * Microsoft
	 */
	MICROSOFT(6, 1), // 6 → 110 << 1 → 1100
	
	/**
	 * Reserved
	 */
	RESERVED(7, 1); // 7 → 1110 << 2 → 1110
	
	/**
	 * Gets the [Uuid] variant value.
	 */
	public val value: Int = unshiftedVariant shl shift
	
	public companion object {
		
		/**
		 * Gets the [UuidVariant] of the specified raw [variant].
		 */
		@Pure
		public fun valueOf(
			variant: Int
		) : Conclude<UuidVariant, InvalidUuidVariantFault> =
			entries.find { variant == it.value }?.asSuccess() ?: failureOf(InvalidUuidVariantFault(variant))
	}
}
