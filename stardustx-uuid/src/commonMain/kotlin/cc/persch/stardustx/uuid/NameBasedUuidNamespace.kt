/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import kotlin.uuid.Uuid

/**
 * Defines the available UUID namespaces.
 *
 * @since 1.0
 */
public enum class NameBasedUuidNamespace {
	
	/**
	 * No namespace
	 */
	NONE {
		
		/**
		 * Gets the [NIL-UUID][NIL_UUID].
		 */
		override val uuid: Uuid = NIL_UUID
	},
	
	/**
	 * DNS namespace
	 */
	DNS {
		
		/**
		 * Gets the [Uuid] for the DNS namespace.
		 */
		override val uuid: Uuid = Uuid.parse("6ba7b810-9dad-11d1-80b4-00c04fd430c8")
	},
	
	/**
	 * URL namespace
	 */
	URL {
		
		/**
		 * Gets the [Uuid] for the URL namespace.
		 */
		override val uuid: Uuid = Uuid.parse("6ba7b811-9dad-11d1-80b4-00c04fd430c8")
	},
	
	/**
	 * ISO OID namespace
	 */
	OID {
		
		/**
		 * Gets the [Uuid] for the ISO OID namespace.
		 */
		override val uuid: Uuid = Uuid.parse("6ba7b812-9dad-11d1-80b4-00c04fd430c8")
	},
	
	/**
	 * Proc.500 namespace
	 */
	X500 {
		
		/**
		 * Gets the [Uuid] for the Proc.500 namespace.
		 */
		override val uuid: Uuid = Uuid.parse("6ba7b814-9dad-11d1-80b4-00c04fd430c8")
	};
	
	/**
	 * Gets the [Uuid] for the current namespace, or the [NIL-ID][NIL_UUID] if [NONE].
	 */
	public abstract val uuid: Uuid
}
