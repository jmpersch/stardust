/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.annotations.Pure
import kotlin.uuid.Uuid

/**
 * Contains the NIL-UUID, where all bits are set to zero:
 *
 * ```
 * 00000000-0000-0000-0000-000000000000
 * ```
 *
 * (see [RFC 4122, 4.1.7. Nil UUID](http://tools.ietf.org/html/rfc4122.section-4.1.7))
 *
 * @since 1.0
 */
public val NIL_UUID: Uuid = Uuid.fromLongs(0L, 0L)

/**
 * Contains the maximum UUID, where all bits are set to one:
 *
 * ```
 * FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF
 * ```
 *
 * (see [RFC 4122, New UUID Formats, 5.4. Max UUID](https://datatracker.ietf.org/doc/html/draft-peabody-dispatch-new-uuid-format-04#name-max-uuid))
 *
 * @since 6.0
 */
public val MAX_UUID: Uuid = Uuid.fromLongs(ULong.MAX_VALUE.toLong(), ULong.MAX_VALUE.toLong())

/**
 * Gets the version information of this [Uuid].
 *
 * @since 1.0
 */
@Pure
public fun Uuid.versionInfo(): UuidVersionInfo = toLongs { mostSignificantBits, leastSignificantBits ->
	
	val uuidVersion = ((mostSignificantBits ushr 12) and 0xf).toInt()
	val uuidVariant = (leastSignificantBits ushr 60).toInt()
	
	val version = UuidVersion.entries.find { uuidVersion == it.value }
	
	val variant =
		UuidVariant.entries.find { uuidVariant ushr it.shift == it.unshiftedVariant }
	
	UuidVersionInfo(version, variant)
}
