/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.compareNullables
import kotlin.uuid.Uuid

/**
 * Defines an [Uuid] version information.
 *
 * @since 1.0
 * @see versionInfo
 *
 * @constructor Creates a new instance of the [UuidVersionInfo] class with the specified [Uuid].
 *
 * @property version Gets the [Uuid] version. Returns `null` if the version is undefined.
 * @property variant Gets the [Uuid] variant. Returns `null` if the variant is undefined.
 */
public data class UuidVersionInfo(
	val version: UuidVersion?,
	val variant: UuidVariant?
) : Comparable<UuidVersionInfo> {
	
	@Pure
	override fun compareTo(other: UuidVersionInfo): Int {
		
		val res = compareNullables(version, other.version)
		
		if(res != 0)
			return res
		
		return compareNullables(variant, other.variant)
	}
}
