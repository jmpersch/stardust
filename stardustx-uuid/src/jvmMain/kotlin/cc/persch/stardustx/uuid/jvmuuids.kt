/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.annotations.Unchecked
import cc.persch.stardust.utils.ByteArrayBuffer
import java.security.MessageDigest
import java.util.Random as JavaRandom
import kotlin.random.Random as KtRandom
import kotlin.uuid.Uuid

/**
 * Creates a named [Uuid] of version 3 or 5, respectively, based on the given namespace, name and hash algorithm.
 * The default hash algorithm is [SHA1][NameBasedUuidHashAlgorithm.SHA1], which creates a version 5 UUID.
 *
 * Implementation details, see `uuid_create_md5_from_name(…)` and `uuid_create_sha1_from_name(…)` procedures
 * in [RFC 4122, Appendix A](https://tools.ietf.org/html/rfc4122#appendix-A).
 *
 * @param namespace A namespace.
 * @param value A value.
 * @param hashAlgorithm The hash algorithm to use. The default is [SHA1][NameBasedUuidHashAlgorithm.SHA1].
 * @return The created [Uuid].
 * @throws IllegalStateException The message digest for the given [hashAlgorithm] is not available—this is impossible,
 *   so this exception should never occur.
 * @since 1.0
 */
@Pure
public fun nameBasedUuidOf(
	namespace: NameBasedUuidNamespace,
	value: String,
	hashAlgorithm: NameBasedUuidHashAlgorithm = NameBasedUuidHashAlgorithm.SHA1
) : Uuid = nameBasedUuidOf(namespace.uuid, value, hashAlgorithm)

/**
 * Creates a named [Uuid] of version 3 or 5, respectively, based on the given namespace, name and hash algorithm.
 * The default hash algorithm is [SHA1][NameBasedUuidHashAlgorithm.SHA1], which creates a version 5 UUID.
 *
 * Implementation details, see `uuid_create_md5_from_name(…)` and `uuid_create_sha1_from_name(…)` procedures
 * in [RFC 4122, Appendix A](https://tools.ietf.org/html/rfc4122#appendix-A).
 *
 * @param namespace A namespace.
 * @param value A value.
 * @param hashAlgorithm The hash algorithm to use. The default is [SHA1][NameBasedUuidHashAlgorithm.SHA1].
 * @return The created [Uuid].
 * @throws IllegalStateException The [MessageDigest] for the given [hashAlgorithm] is not available—this is impossible,
 *   so this exception should never occur.
 * @since 1.0
 */
@Pure
public fun nameBasedUuidOf(
	namespace: Uuid,
	value: String,
	hashAlgorithm: NameBasedUuidHashAlgorithm = NameBasedUuidHashAlgorithm.SHA1
) : Uuid {
	
	val valueBytes = value.toByteArray()
	
	// Init(&c): Initialize buffer
	val buffer = ByteArrayBuffer.allocate(16 + valueBytes.size)
	
	// Update(&c, &net_nsid, sizeof net_nsid): Add namespace at first
	namespace.toLongs { mostSignificantBits, leastSignificantBits ->
		
		buffer.putLong(mostSignificantBits)
		buffer.putLong(leastSignificantBits)
	}
	
	// Update(&c, name, namelen): Then add name
	buffer.putBytes(valueBytes)
	
	// Final(hash, &c): Final hash
	val hash: ByteArray = hashAlgorithm.algorithm.digest(buffer.array)
	
	return createUuid(ByteArrayBuffer.wrap(hash), hashAlgorithm.version)
}

/**
 * Creates a version 7 UUID using a time stamp and a random value.
 *
 * Implementation details, see [RFC 4122 draft, New UUID Formats](https://datatracker.ietf.org/doc/html/draft-peabody-dispatch-new-uuid-format-04).
 *
 * @since 6.0
 */
@Pure
public fun timestampBasedUuidOf(random: KtRandom): Uuid = __v7UuidOf(random.nextInt(), random.nextLong())

/**
 * Creates a version 7 UUID using a time stamp and a random value.
 *
 * Implementation details, see [RFC 4122 draft, New UUID Formats](https://datatracker.ietf.org/doc/html/draft-peabody-dispatch-new-uuid-format-04).
 *
 * @since 6.0
 */
@Pure
public fun timestampBasedUuidOf(random: JavaRandom): Uuid = __v7UuidOf(random.nextInt(), random.nextLong())

@Pure
private fun __v7UuidOf(rnd1: Int, rnd2: Long) : Uuid = __v7UuidOf(System.currentTimeMillis(), rnd1, rnd2)

@Pure
internal fun __v7UuidOf(timeStamp: Long, rnd1: Int, rnd2: Long) : Uuid {
	
	val buffer = ByteArrayBuffer.allocate(16)
	
	buffer.putLong((timeStamp shl 16) or (rnd1.toLong() and 0xffffL))
	
	buffer.putLong(rnd2)
	
	return createUuid(buffer, UuidVersion.V7)
}

@Pure
private fun createUuid(
	@Unchecked buffer: ByteArrayBuffer,
	version: UuidVersion
) : Uuid {
	
	buffer.set(6, ((buffer.get(6).toInt() and 0x0f) or (version.value shl 4)).toByte()) // Set version
	buffer.set(8, ((buffer.get(8).toInt() and 0x3f) or 0x80).toByte()) // Set IETF variant
	
	buffer.position = 0
	
	return Uuid.fromLongs(buffer.getLong(), buffer.getLong())
}
