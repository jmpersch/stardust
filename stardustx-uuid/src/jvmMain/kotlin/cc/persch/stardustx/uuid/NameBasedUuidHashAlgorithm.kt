/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.annotations.Pure
import cc.persch.stardust.conclude.Conclude
import cc.persch.stardust.conclude.asSuccess
import cc.persch.stardust.conclude.failureOf
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Contains the supported UUID versions 3 and 5 using the MD5 or SHA-1 algorithm, respectively.
 *
 * @since 1.0
 */
public enum class NameBasedUuidHashAlgorithm {
	
	/**
	 * MD5 algorithm (UUID version 3)
	 */
	MD5 {
		
		/**
		 * Gets the version [5][UuidVersion.V3].
		 *
		 * @return The number of the current version.
		 */
		override val version: UuidVersion = UuidVersion.V3
		
		/**
		 * Gets the [MessageDigest] for the current version.
		 *
		 * @return The [MessageDigest] for the current version.
		 * @throws IllegalStateException The *MD5* hash algorithm is not available—this is impossible,
		 *   so this exception should never occur.
		 */
		override val algorithm: MessageDigest get() {
			
			try {
				
				return MessageDigest.getInstance("MD5")
			}
			catch(e: NoSuchAlgorithmException) {
				
				throw NoSuchAlgorithmException("The MD5 hash algorithm is not available.", e)
			}
		}
	},
	
	/**
	 * SHA-1 algorithm (UUID version 5)
	 */
	SHA1 {
		
		/**
		 * Gets the UUID version [5][UuidVersion.V5].
		 *
		 * @return The number of the current version.
		 */
		override val version: UuidVersion = UuidVersion.V5
		
		/**
		 * Gets the [MessageDigest] for the current version.
		 *
		 * @return The [MessageDigest] for the current version.
		 * @throws NoSuchAlgorithmException The *SHA-1* hash algorithm is not available—this is impossible,
		 *   so this exception should never occur.
		 */
		override val algorithm: MessageDigest get() {
			
			try {
				
				return MessageDigest.getInstance("SHA-1")
			}
			catch(e: NoSuchAlgorithmException) {
				
				throw NoSuchAlgorithmException("The SHA-1 hash algorithm is not available.", e)
			}
		}
	};
	
	/**
	 * Gets the number of the current version.
	 *
	 * @return The number of the current version.
	 */
	public abstract val version: UuidVersion
	
	/**
	 * Gets the [MessageDigest] for the current version.
	 *
	 * @return The [MessageDigest] for the current version.
	 * @throws IllegalStateException The corresponding hash algorithm is not available—this is impossible,
	 *   so this exception should never occur.
	 */
	public abstract val algorithm: MessageDigest
	
	public companion object {

		/**
		 * Tries to get a [NameBasedUuidHashAlgorithm] of the specified raw [version].
		 */
		@Pure
		public fun valueOf(
			version: UuidVersion
		) : Conclude<NameBasedUuidHashAlgorithm, InvalidUuidVersionForNameBasedUuidHashAlgorithmFault> =
			entries.find { version == it.version }?.asSuccess()
			?: failureOf(InvalidUuidVersionForNameBasedUuidHashAlgorithmFault(version))
	}
}
