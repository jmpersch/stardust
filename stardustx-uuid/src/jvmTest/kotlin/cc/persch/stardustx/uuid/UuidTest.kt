/*
 * Copyright © 2008–2025 Jan Martin Persch <https://persch.cc/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file is part of the Stardust software library <https://gitlab.com/jmpersch/stardust>.
 */

package cc.persch.stardustx.uuid

import cc.persch.stardust.conclude.orThrow
import cc.persch.stardust.parsing.parseInt
import cc.persch.stardust.parsing.parseLong
import cc.persch.stardustx.uuid.NameBasedUuidHashAlgorithm.MD5
import cc.persch.stardustx.uuid.NameBasedUuidHashAlgorithm.SHA1
import cc.persch.stardustx.uuid.NameBasedUuidNamespace.URL
import java.nio.ByteBuffer
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.uuid.Uuid
import kotlin.uuid.toKotlinUuid

class UuidTest {
	
	@Test
	fun testNilUUID() {
		
		assertEquals(Uuid.parse("00000000-0000-0000-0000-000000000000"), NIL_UUID)
	}
	
	@Test
	fun testMaxUUID() {
		
		assertEquals(Uuid.parse("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF"), MAX_UUID)
	}
	
	@Test
	fun testNameUUID() {
		
		val namespace = URL
		val uuidValue = "Foobar"
		val valueBytes = uuidValue.toByteArray()
		val buffer: ByteBuffer = ByteBuffer.allocate(16 + valueBytes.size)
		
		namespace.uuid.toLongs { mostSignificantBits, leastSignificantBits ->
			
			buffer.putLong(mostSignificantBits)
			buffer.putLong(leastSignificantBits)
			buffer.put(valueBytes)
			
			assertEquals(
				UUID.nameUUIDFromBytes(buffer.array()).toKotlinUuid(),
				nameBasedUuidOf(URL, uuidValue, MD5)
			)
		}
	}
	
	/**
	 * [RFC 4122 draft, New UUID Formats, B.2. Example of a UUIDv7 Value](https://datatracker.ietf.org/doc/html/draft-peabody-dispatch-new-uuid-format-04#name-example-of-a-uuidv7-value).
	 */
	@Test
	fun textV7UUID() {
		
		val timeStamp = 1645557742000L
		val rnd1 = 0xCC3
		val rnd2 = 0x18C4DC0C0C07398FL
		
		val uuid = __v7UuidOf(timeStamp, rnd1, rnd2)
		
		assertEquals(Uuid.parse("017F22E2-79B0-7CC3-98C4-DC0C0C07398F"), uuid)
	}
	
	@Test
	fun testInfo() {
		
		val versionInfo = nameBasedUuidOf(URL, "Foobar", SHA1).versionInfo()
		
		assertEquals(UuidVersion.V5, versionInfo.version)
		assertEquals(UuidVariant.IETF, versionInfo.variant)
	}
}
